:- [tak].
benchmark(tak).
bresults(tak, 'data/tak.dat').
brun(tak, Threads, P, _) :-
  run_tak(Threads, P).
bthreads(tak, fast, [1, 3, 9, 27]).
bthreads(tak, medium, [1, 3, 9, 27, 81]).
bthreads(tak, slow, [1, 3, 9, 27, 81, 243]).
bthreads(tak, lgt, [1, 3, 9, 27, 81, 243]).
bthreads(tak, lgt_long, [1, 3, 9, 27, 81, 243]).
bparams(tak, fast, [(15,10,5), (21,14,7), (27,18,9)]).
bparams(tak, medium, [(15,10,5), (16,12,6), (21,14,7), (24, 16, 8), (27,18,9)]).
bparams(tak, slow, [(15,10,5), (16,12,6), (21,14,7), (24, 16, 8), (27,18,9), (30,20,10)]).
bparams(tak, lgt, [(21,14,7)]).
bparams(tak, lgt_long, [(21,14,7), (24, 16, 8), (27,18,9), (30,20,10), (33, 22, 11)]).
bdata(tak, _, _).

benchmark(tak_native).
bresults(tak_native, 'data/tak_native.dat').
brun(tak_native, Threads, P, _) :-
  run_tak_native(Threads, P).
bthreads(tak_native, fast, [1, 3, 9, 27]).
bthreads(tak_native, medium, [1, 3, 9, 27, 81]).
bthreads(tak_native, slow, [1, 3, 9, 27, 81, 243]).
bthreads(tak_native, lgt, [1, 3, 9, 27, 81, 243]).
bthreads(tak_native, lgt_long, [1, 3, 9, 27, 81, 243]).
bparams(tak_native, fast, [(15,10,5), (21,14,7), (27,18,9)]).
bparams(tak_native, medium, [(15,10,5), (16,12,6), (21,14,7), (24, 16, 8), (27,18,9)]).
bparams(tak_native, slow, [(15,10,5), (16,12,6), (21,14,7), (24, 16, 8), (27,18,9), (30,20,10)]).
bparams(tak_native, lgt, [(21,14,7)]).
bparams(tak_native, lgt_long, [(21,14,7), (24, 16, 8), (27,18,9), (30,20,10), (33, 22, 11)]).
bdata(tak_native, _, _).

:- [fib].
benchmark(fib).
bresults(fib, 'data/fib.dat').
brun(fib, Threads, P, _) :-
  run_fib(Threads, P).
bthreads(fib, _, [1, 2, 4, 8, 16]).
bparams(fib, fast, [20, 21, 22, 23]).
bparams(fib, medium, [20, 21, 22, 23, 24, 25]).
bparams(fib, slow, [20, 21, 22, 23, 24, 25, 26, 27]).
bparams(fib, lgt, [20, 21, 22, 23, 24, 25, 26, 27]).
bparams(fib, lgt_long, [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35]).
bdata(fib, _, _).

benchmark(fib_native).
bresults(fib_native, 'data/fib_native.dat').
brun(fib_native, Threads, P, _) :-
  run_fib_native(Threads, P).
bthreads(fib_native, _, [1, 2, 4, 8, 16]).
bparams(fib_native, fast, [20, 21, 22, 23]).
bparams(fib_native, medium, [20, 21, 22, 23, 24, 25]).
bparams(fib_native, slow, [20, 21, 22, 23, 24, 25, 26, 27]).
bparams(fib_native, lgt, [20, 21, 22, 23, 24, 25, 26, 27]).
bparams(fib_native, lgt_long, [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35]).
bdata(fib_native, _, _).

:- [fft].
benchmark(fft).
bresults(fft, 'data/fft.dat').
brun(fft, Threads, P, D) :-
  run_fft(Threads, P, D).
bthreads(fft, _, [1, 2, 4, 8, 16]).
bparams(fft, fast, [16384, 32768]).
bparams(fft, medium, [16384, 32768, 65536, 131072]).
bparams(fft, long, [16384, 32768, 65536, 131072, 262144, 524288]).
bparams(fft, lgt, [1024, 2048, 4096, 8192, 16384, 32768, 65536]).
bparams(fft, lgt_long, [1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288]).
bdata(fft, P, D) :-
  data_fft(P, D).

benchmark(fft_native).
bresults(fft_native, 'data/fft_native.dat').
brun(fft_native, Threads, P, D) :-
  run_fft_native(Threads, P, D).
bthreads(fft_native, _, [1, 2, 4, 8, 16]).
bparams(fft_native, fast, [16384, 32768]).
bparams(fft_native, medium, [16384, 32768, 65536, 131072]).
bparams(fft_native, long, [16384, 32768, 65536, 131072, 262144, 524288]).
bparams(fft_native, lgt, [1024, 2048, 4096, 8192, 16384, 32768, 65536]).
bparams(fft_native, lgt_long, [1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288]).
bdata(fft_native, P, D) :-
  data_fft(P, D).

:- [hanoi].
benchmark(hanoi).
bresults(hanoi, 'data/hanoi.dat').
brun(hanoi, Threads, P, _) :-
  run_hanoi(Threads, P).
bthreads(hanoi, _,  [1, 2, 4, 8, 16]).
bparams(hanoi, fast, [20, 21, 22, 23]).
bparams(hanoi, medium, [20, 21, 22, 23, 24, 25]).
bparams(hanoi, slow, [20, 21, 22, 23, 24, 25, 26, 27, 28, 29]).
bparams(hanoi, lgt, [20, 21, 22, 23, 24, 25, 26, 27]).
bparams(hanoi, lgt_long, [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]).
bdata(hanoi, _, _).

benchmark(hanoi_native).
bresults(hanoi_native, 'data/hanoi_native.dat').
brun(hanoi_native, Threads, P, _) :-
  run_hanoi_native(Threads, P).
bthreads(hanoi_native, _,  [1, 2, 4, 8, 16]).
bparams(hanoi_native, fast, [20, 21, 22, 23]).
bparams(hanoi_native, medium, [20, 21, 22, 23, 24, 25]).
bparams(hanoi_native, slow, [20, 21, 22, 23, 24, 25, 26, 27, 28, 29]).
bparams(hanoi_native, lgt, [20, 21, 22, 23, 24, 25, 26, 27]).
bparams(hanoi_native, lgt_long, [20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]).
bdata(hanoi_native, _, _).

:- [msort].
benchmark(msort).
bresults(msort, 'data/msort.dat').
brun(msort, Threads, P, D) :-
  run_msort(Threads, P, D).
bthreads(msort, _,  [1, 2, 4, 8, 16]).
bparams(msort, fast, [5000, 10000, 20000]).
bparams(msort, medium, [5000, 10000, 20000, 50000]).
bparams(msort, slow, [5000, 10000, 20000, 50000, 100000, 200000]).
bparams(msort, lgt, [5000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000]).
bparams(msort, lgt_long, [20000, 40000, 60000, 80000, 100000, 120000, 140000, 160000, 180000, 200000, 220000, 240000]).
bdata(msort, P, D) :-
  data_msort(P, D).

benchmark(msort_native).
bresults(msort_native, 'data/msort_native.dat').
brun(msort_native, Threads, P, D) :-
  run_msort_native(Threads, P, D).
bthreads(msort_native, _,  [1, 2, 4, 8, 16]).
bparams(msort_native, fast, [5000, 10000, 20000]).
bparams(msort_native, medium, [5000, 10000, 20000, 50000]).
bparams(msort_native, slow, [5000, 10000, 20000, 50000, 100000, 200000]).
bparams(msort_native, lgt, [5000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000]).
bparams(msort_native, lgt_long, [20000, 40000, 60000, 80000, 100000, 120000, 140000, 160000, 180000, 200000, 220000, 240000]).
bdata(msort_native, P, D) :-
  data_msort(P, D).

%:- [qsort].
%benchmark(qsort).
%bresults(qsort, 'data/qsort.dat').
%brun(qsort, Threads, P, D) :-
%  run_qsort(Threads, P, D).
%bthreads(qsort, _,  [1, 2, 4, 8, 16]).
%bparams(qsort, fast, [5000, 10000, 20000, 50000]).
%bparams(qsort, medium, [5000, 10000, 20000, 50000, 100000]).
%bparams(qsort, slow, [5000, 10000, 20000, 50000, 100000, 200000]).
%bparams(qsort, lgt, [5000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000, 50000]).
%bparams(qsort, lgt_long, [20000, 40000, 60000, 80000, 100000, 120000, 140000, 160000, 180000, 200000, 220000, 240000]).
%bdata(qsort, P, D) :-
%  data_qsort(P, D).

:- [primes].
benchmark(primes).
bresults(primes, 'data/primes.dat').
brun(primes, Threads, P, _) :-
  run_primes(Threads, 1, P).
bthreads(primes, lgt,  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]) :- !.
bthreads(primes, lgt_long,  [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]) :- !.
bthreads(primes, _,  [1, 2, 4, 8, 16]).
bparams(primes, fast, [5000, 10000, 20000, 30000, 40000, 60000]).
bparams(primes, medium, [5000, 10000, 20000, 30000, 40000, 60000, 80000, 100000]).
bparams(primes, slow, [5000, 10000, 20000, 30000, 40000, 60000, 80000, 100000, 200000]).
bparams(primes, lgt, [10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000]).
bparams(primes, lgt_long, [10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000, 110000, 120000, 130000, 140000, 150000]).
bdata(primes, _, _).

:- [search].
benchmark(search).
bresults(search, 'data/search.dat').
brun(search, hill_climbing, (Liters, Jug1, Jug2, MaxDepth), _) :-
  (run_hill_climbing(Liters, Jug1, Jug2, MaxDepth) -> true; true).
brun(search, depth_first, (Liters, Jug1, Jug2, MaxDepth), _) :-
  (run_depth_first(Liters, Jug1, Jug2, MaxDepth) -> true; true).
brun(search, breadth_first, (Liters, Jug1, Jug2, MaxDepth), _) :-
  (run_breadth_first(Liters, Jug1, Jug2, MaxDepth) -> true; true).
brun(search, competitive_or, (Liters, Jug1, Jug2, MaxDepth), _) :-
  (run_competitive_or(Liters, Jug1, Jug2, MaxDepth) -> true; true).
bthreads(search, _,  [hill_climbing, depth_first, breadth_first, competitive_or]).
bparams(search, lgt, [(1, 5, 9, 14), (2, 5, 9, 14), (3, 5, 9, 14), (4, 5, 9, 14), (5, 5, 9, 14),
  (6, 5, 9, 14), (7, 5, 9, 14), (8, 5, 9, 14), (9, 5, 9, 14), (10, 5, 9, 14), (11, 5, 9, 14),
    (12, 5, 9, 14), (13, 5, 9, 14), (14, 5, 9, 14)]) :- !.
bparams(search, lgt_long, [(1, 5, 9, 14), (2, 5, 9, 14), (3, 5, 9, 14), (4, 5, 9, 14), (5, 5, 9, 14),
  (6, 5, 9, 14), (7, 5, 9, 14), (8, 5, 9, 14), (9, 5, 9, 14), (10, 5, 9, 14), (11, 5, 9, 14),
    (12, 5, 9, 14), (13, 5, 9, 14), (14, 5, 9, 14)]) :- !.
bparams(search, _, [(2, 3, 10, 14), (3, 3, 10, 14), (4, 3, 10, 14), (5, 3, 10, 14),
  (6, 3, 10, 14), (7, 3, 10, 14), (8, 3, 10, 14), (9, 3, 10, 14), (10, 3, 10, 14), (11, 3, 10, 14),
  (12, 3, 10, 14), (13, 3, 10, 14), (14, 3, 10, 14), (15, 3, 10, 14), (16, 3, 10, 14), (17, 3, 10, 14),
    (18, 3, 10, 14), (19, 3, 10, 14), (20, 3, 10, 14), (21, 3, 10, 14), (22, 3, 10, 14), (23, 3, 10, 14),
      (24, 3, 10, 14)]).
bdata(search, _, _).

:- [listintersection].
benchmark(listintersection_allsame).
bresults(listintersection_allsame, 'data/listintersection_allsame.dat').
brun(listintersection_allsame, Threads, P, (Var, Goals)) :-
  run_listintersection(Threads, Var, Goals).
bthreads(listintersection_allsame, _, [findall, piped_findall]).
bparams(listintersection_allsame, _, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]).
bdata(listintersection_allsame, P, (Var, Goals)) :-
  member(X, [2500, 5000, 7500, 10000, 12500, 15000]),
  data_listintersection_allsame(1, P, X, Var, Goals).

benchmark(listintersection_alldiff).
bresults(listintersection_alldiff, 'data/listintersection_alldiff.dat').
brun(listintersection_alldiff, Threads, P, (Var, Goals)) :-
  run_listintersection(Threads, Var, Goals).
bthreads(listintersection_alldiff, _, [findall, piped_findall]).
bparams(listintersection_alldiff, _, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]).
bdata(listintersection_alldiff, P, (Var, Goals)) :-
  member(X, [2500, 5000, 7500, 10000, 12500, 15000]),
  data_listintersection_alldiff(1, P, X, Var, Goals).

benchmark(listintersection_overlap).
bresults(listintersection_overlap, 'data/listintersection_overlap.dat').
brun(listintersection_overlap, Threads, P, (Var, Goals)) :-
  run_listintersection(Threads, Var, Goals).
bthreads(listintersection_overlap, _, [findall, piped_findall]).
bparams(listintersection_overlap, _, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]).
bdata(listintersection_overlap, P, (Var, Goals)) :-
  member(X, [2500, 5000, 7500, 10000, 12500, 15000]),
  data_listintersection_overlap(1, P, X, Var, Goals).


mtbatch(Type, Count) :-
  benchmark(Name),
  bthreads(Name, Type, Threads),
  bparams(Name, Type, Params),
  mtbatch_do(Count, Name, Threads, Params),
  fail.
mtbatch(_, _).
mtbatch(Type, Count, Name) :-
  benchmark(Name),
  bthreads(Name, Type, Threads),
  bparams(Name, Type, Params),
  mtbatch_do(Count, Name, Threads, Params),
  fail.
mtbatch(_, _, _).
mtbatch(Type, Count, Name, Threads) :-
  benchmark(Name),
  bparams(Name, Type, Params),
  mtbatch_do(Count, Name, Threads, Params),
  fail.
mtbatch(_, _, _, _).

mtbatch_do(Count, Name, Threads, Params) :-
  write('---------------------------- '),
  write(Name), write(' ( average of '), write(Count), write(' runs, in seconds)'),
  write(' ----------------------------'),nl,
  write('\t\t'),
 
  % // Log
  bresults(Name, File),
  tell(File),
  write('# '),
  write(Name), write(' ( average of '), write(Count), write(' runs, in seconds)'),nl,
  write(Name),
  told,
  % -- Log

  (
    (
      member(T, Threads), 
      write(T), write('\t\t\t'),
      append(File), write('\t'), write(T), flush_output(File), told, % Log
      fail
    ;
      true
    )
  ),
  (
    member(P, Params), 
    nl,
    write(P), write('\t'), flush_output(1),
    append(File), nl, write('"'), write(P), write('"'), flush_output(File), told, % Log
    bdata(Name, P, D), % get data for these params
    member(T, Threads),
    (catch(run(Count, Name, T, P, D, Total), Error, write_error(File, Error)) ->
      ( var(Error) ->
        Avg is Total/(Count*1000),
        write(Avg), write('\t'), flush_output(1),
        append(File), write('\t'), write(Avg), flush_output(File), told % Log
      )
    ),
    fail
  ).
mtbatch_do(_, Name, _, _) :-
  nl,nl,
  bresults(Name, File), append(File), nl, flush_output(File), told. % Log

write_error(File, E) :-
  write(E), write('\t'), flush_output(1),
  append(File), write('\t'), write(E), flush_output(File), told. % Log

sum([], 0).
sum([X|T], S) :-
  sum(T, S1),
  S is X+S1.

run(Count, Name, T, P, D, Result) :-
  unixtime(Start),
  (runall(Count, Name, T, P, D) -> true; throw(error)),
  unixtime(End),
  Result is End-Start.

runall(Count, Name, T, P, D) :-
  repeat(Count),
  (
    brun(Name, T, P, D) -> true
  ; throw(failure)
  ),
  fail.
runall(_, _, _, _, _).
