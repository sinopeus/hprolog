#ifdef THREADED
static char * thread_switch = "threaded code";
#endif
#ifdef JUMP_TABLE
static char * thread_switch = "jump table code";
#endif
#ifdef PLAIN_SWITCH
static char * thread_switch = "plain switch code";
#endif

#ifdef HW_P
static char * locpc_decl = "p in hw" ;
#else
static char * locpc_decl = "p in sw" ;
#endif

#ifdef USE_GMP
static char * whichnums = "bignums with gmp" ;
#else
static char * whichnums = "only small ints" ;
#endif



static void banner(FILE *f)
{
  fprintf(f,"\n%% hProlog by Bart.Demoen@cs.kuleuven.be - release %s - 26-5-2011\n%% installed on %s\n",VERSION_STRING,install_date);
  fprintf(f,"%% Student version for Wetenschappelijke Vorming 2013-2014\n");
#ifdef MULTI_THREADED
  fprintf(f,"%% Multi-threading support by Timon Van Overveldt\n");
#endif
  //  fprintf(f,"%% Most rights reserved \n");
  fprintf(f,"%% %s, %s, %s\n\n",thread_switch,locpc_decl,whichnums);
} /* banner */

