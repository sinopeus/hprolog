\chapter{Formatted writing} \label{format}
%=========================================

\section{Introduction}
%=====================
\texttt{format/2} behaves as specified by SICStus Prolog, 
but without column boundaries and a few control sequences.

\section{Syntax and Semantics}
%============================

The general format of a control sequence is \texttt{\~{}NC}. The character \texttt{C}
determines the type of the control sequence. \texttt{N} is an optional numeric
argument. An alternative form of \texttt{N} is \texttt{*}. \texttt{*} implies
that the next argument in Arguments should be used as a numeric argument in
the control sequence. 

\paragraph{Example:}

\begin{verbatim}
          | ?- format('Hello~4cworld!', [0'x]).
\end{verbatim}

and

\begin{verbatim}
          | ?- format('Hello~*cworld!', [4,0'x]).
\end{verbatim}

both produce

\begin{verbatim}
          Helloxxxxworld!
\end{verbatim}

\subsection{Control Sequences}

The following control sequences are available.

\begin{supertabular}{lp{.8\textwidth}}

\texttt{\~{}a} & 
        The argument is an atom. The atom is printed without quoting. \\


\texttt{\~{}Nc} &
      (Print character.) The argument is a number that will be
      interpreted as a character code. \texttt{N} defaults to one and is
      interpreted as the number of times to print the character. \\


\texttt{\~{}Ne} & \\
\texttt{\~{}NE} & \\
\texttt{\~{}Nf} & \\
\texttt{\~{}Ng} & \\
\texttt{\~{}NG} &
     (Print float). The argument is a float. The float and \texttt{N} will be
     passed to the C \texttt{printf()} function as
\begin{verbatim}

                    printf("%.Ne", Arg)
                    printf("%.NE", Arg)
                    printf("%.Nf", Arg)
                    printf("%.Ng", Arg)
                    printf("%.NG", Arg)
\end{verbatim}
     respectively.

     If \texttt{N} is not supplied the action defaults to
\begin{verbatim}
                    printf("%e", Arg)
                    printf("%E", Arg)
                    printf("%f", Arg)
                    printf("%g", Arg)
                    printf("%G", Arg)
\end{verbatim}

     respectively. \\

\texttt{\~{}Nd} &
     (Print decimal.) The argument is an integer. \texttt{N} is interpreted as
     the number of digits after the decimal point. If \texttt{N} is 0 or
     missing, no decimal point will be printed. Example:

\begin{verbatim}
                    | ?- format('Hello ~1d world!', [42]).
                    Hello 4.2 world!

                    | ?- format('Hello ~d world!', [42]).
                    Hello 42 world!
\end{verbatim}
\\

\texttt{\~{}ND} &
     (Print decimal.) The argument is an integer. Identical to \texttt{\~{}Nd}
     except that , will separate groups of three digits to the left of
     the decimal point. Example:

\begin{verbatim}
                    | ?- format('Hello ~1D world!', [12345]).
                    Hello 1,234.5 world!
\end{verbatim}

\\

\texttt{\~{}Nr} & 
     (Print radix.) The argument is an integer. \texttt{N} is interpreted as a
     radix, 2 $\geq$ \texttt{N} $\leq$ 36. If N is missing the radix defaults to 8. The
     letters a-z will denote digits larger than 9. Example:

\begin{verbatim}
                    | ?- format('Hello ~2r world!', [15]).
                    Hello 1111 world!

                    | ?- format('Hello ~16r world!', [15]).
                    Hello f world!
\end{verbatim}

\\

\texttt{\~{}NR} &
     (Print radix.) The argument is an integer. Identical to \texttt{\~{}Nr} except
     that the letters A-Z will denote digits larger than 9. Example:

\begin{verbatim}
                    | ?- format('Hello ~16R world!', [15]).
                    Hello F world!
\end{verbatim}

\\

\texttt{\~{}Ns} & 
     (Print string.) The argument is a list of character codes. Exactly
     \texttt{N} characters will be printed. N defaults to the length of the
     string. Example:

\begin{verbatim}
                    | ?- format('Hello ~4s ~4s!', ["new","world"]).
                    Hello new  worl!

                    | ?- format('Hello ~s world!', ["new"]).
                    Hello new world!
\end{verbatim}

\\

\texttt{\~{}i} &
     (Ignore.) The argument, which may be of any type, is ignored.
     Example:

\begin{verbatim}
                    | ?- format('Hello ~i~s world!', ["old","new"]).
                    Hello new world!
\end{verbatim}

\\

%% \texttt{\~{}k} &
%%      (Print canonical.) The argument may be of any type. The argument
%%      will be passed to \texttt{write\_canonical/1} (see Term I/O). Example:
%% 
%% \begin{verbatim}
%%                     | ?- format('Hello ~k world!', [[a,b,c]]).
%%                     Hello .(a,.(b,.(c,[]))) world!
%% \end{verbatim}
%% 
%% \\
%% 
%% \texttt{\~{}p} &
%%      (Print.) The argument may be of any type. The argument will be
%%      passed to \texttt{print/1} (see Term I/O). Example:
%% 
%% \begin{verbatim}
%%                     | ?- assert((portray([X|Y]) :- print(cons(X,Y)))).
%%                     | ?- format('Hello ~p world!', [[a,b,c]]).
%%                     Hello cons(a,cons(b,cons(c,[]))) world!
%% \end{verbatim}
%% 
%% \\

\texttt{\~{}q} &
     (Print quoted.) The argument may be of any type. The argument will
     be passed to \texttt{writeq/1}. Example:

\begin{verbatim}
                    | ?- format('Hello ~q world!', [['A','B']]).
                    Hello ['A','B'] world!
\end{verbatim}

\\


\texttt{\~{}k} \\

\texttt{\~{}p} \\

\texttt{\~{}w} &
     (Write.) The argument may be of any type. The argument will be
     passed to \texttt{write/1}. Example:

\begin{verbatim}
                    | ?- format('Hello ~w world!', [['A','B']]).
                    Hello [A,B] world!
\end{verbatim}

\\

\texttt{\~{}@} &
     (Call.) The argument is a goal, which will be called and expected
     to print on the current output stream. If the goal performs other
     side-effects or does not succeed deterministically, the behavior
     is undefined. Example:

\begin{verbatim}
                    | ?- format('Hello ~@ world!', [write(new)]).
                    Hello new world!
\end{verbatim}

\\

\texttt{\~{}\~{}} &
     (Print tilde.) Takes no argument. Prints \~{}. Example:

\begin{verbatim}
                    | ?- format('Hello ~~ world!', []).
                    Hello ~ world!
\end{verbatim}

\\

\texttt{\~{}Nn} &
     (Print newline.) Takes no argument. Prints \texttt{N} newlines. \texttt{N} defaults
     to 1. Example:

\begin{verbatim}
                    | ?- format('Hello ~n world!', []).
                    Hello
                     world!
\end{verbatim}

\\

%% \texttt{\~{}N} &
%%      (Print Newline.) Prints a newline if not at the beginning of a
%%      line.
%% 
%% \\

\end{supertabular}

\section{Comments}
%=================

\begin{itemize}
\item Column boundaries are not implemented. 
\item The \texttt{\~{}k} and \texttt{\~{}p} control sequences use
\texttt{write/1} instead of \texttt{write\_canonical/1} respectively
\texttt{print/1} because neither predicate exists in hProlog.
\item The \texttt{\~{}N} control sequence is not supported.
\end{itemize}
