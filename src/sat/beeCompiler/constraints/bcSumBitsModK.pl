% Author: Amit Metodi
% Last Updated: 25/07/2012

:- module(bcSumBitsModK, [ ]).

%%% ------------------------- %%%
%%% add constraints to parser %%%
%%% ------------------------- %%%
:- Head=bool_array_sum_modK(Bits,K,Sum,ConstrsH-ConstrsT),
   Body=(
       !,
       integer(K),
       length(Bits,N),
       bcSumBits:sumBitsType(SumType),
       bcSumBitsModK:decomposeSumModK(Bits,N,K,SumT,SumType,ConstrsH-ConstrsT),
       bcInteger:getUnaryNumber(Sum,SumU),
       auxUnarynum:unarynumEquals(SumT,SumU)
   ),
   bParser:addConstraint(Head,Body).

%%% ------------------------- %%%
%%% decompoosition            %%%
%%% ------------------------- %%%
decomposeSumModK(Bits,N,K,Sum,SumType,ConstrsH-ConstrsT):-
    N < K,!,% base case
    auxUnarynum:unarynumNewInRange(0,N,Sum),
    weightBits:bits2wbits(Bits,WBits),
    bcSumBits:sumBitsSimplify_step1(bc(SumType,[Sum|WBits]),Constr,1),
    (Constr==none ->
        ConstrsH=ConstrsT
    ;
        ConstrsH=[Constr|ConstrsT]
    ).

decomposeSumModK(Bits,N,K,Sum,SumType,ConstrsH-ConstrsT):-
    N2 is N // 2,
    N1 is N - N2,
    auxLists:listSplit(N1,Bits,Bits1,Bits2),
    decomposeSumModK(Bits1,N1,K,Sum1,SumType,ConstrsH-Constrs1),
    decomposeSumModK(Bits2,N2,K,Sum2,SumType,Constrs1-Constrs2),

    Sum3len is min(N1,K-1)+min(N2,K-1),
    auxUnarynum:unarynumNewInRange(0,Sum3len,Sum3),
    bcUnaryAdder:uadderSimplify1st(bc(_,[Sum1, Sum2, Sum3]), Constr, _),
    (Constr==none ->
        Constrs2=Constrs3
    ;
        Constrs2=[Constr|Constrs3]
    ),

    specialCaseMod(Sum3,K,Sum,Constrs3-ConstrsT).
   
   
% where K<=|X|< 2*K
specialCaseMod(X,K,XmodK,ConstrsH-ConstrsT):-
    X=(Min,Max,Bits,_),
    %assert for this code:
    !,Min==0, Max < 2*K,!,
    K1 is K - 1,
    auxLists:listSplit(K1,Bits,Bits1,[Y|Bits2]),
    auxUnarynum:unarynumNewInRange(0,K1,XmodK),
    XmodK=(0,K1,Bits3,_),
    bcBoolElement:boolElementType(TypeMux),!,
    mux2vecs(Bits1,Bits2,[-Y,Y],Bits3,TypeMux,ConstrsH-ConstrsT).

mux2vecs([X|Xs],[Y|Ys],I,[Z|Zs],Type,[bc(Type,[I,[X,Y],Z])|ConstrsH]-ConstrsT):-!,
    mux2vecs(Xs,Ys,I,Zs,Type,ConstrsH-ConstrsT).
mux2vecs([],[],_,[],_,Constrs-Constrs):-!.
mux2vecs([],[Y|Ys],I,[Z|Zs],Type,[bc(Type,[I,[-1,Y],Z])|ConstrsH]-ConstrsT):-!,
    mux2vecs([],Ys,I,Zs,Type,ConstrsH-ConstrsT).
mux2vecs([X|Xs],[],I,[Z|Zs],Type,[bc(Type,[I,[X,-1],Z])|ConstrsH]-ConstrsT):-!,
    mux2vecs(Xs,[],I,Zs,Type,ConstrsH-ConstrsT).
mux2vecs([X|Xs],[Y|Ys],I,[],Type,[bc(Type,[I,[X,Y],-1])|ConstrsH]-ConstrsT):-!,
    mux2vecs(Xs,Ys,I,[],Type,ConstrsH-ConstrsT).
mux2vecs([X|Xs],[],I,[],Type,[bc(Type,[I,[X,-1],-1])|ConstrsH]-ConstrsT):-!,
    mux2vecs(Xs,[],I,[],Type,ConstrsH-ConstrsT).
mux2vecs([],[Y|Ys],I,[],Type,[bc(Type,[I,[-1,Y],-1])|ConstrsH]-ConstrsT):-!,
    mux2vecs([],Ys,I,[],Type,ConstrsH-ConstrsT).