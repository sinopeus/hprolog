%% Boot file: initially loaded by the hProlog
%% It's predicate boot/0 is executed by default
%% so it doesn't need to contain queries
%% boot/0 consults enough files to do complete Prolog
%% and then calls the toplevel

%% Bart Demoen Tue Jun 29 11:42:06 CEST 1999

:- sysmodule(sysh,[boot/0]).

boot :-
	sysh:load_file('dp_version.w',installation),
	sysh:load_file('dp_builtin.w',installation),
	sysh:load_file('dp_toplevel.w',installation),
 	sysh:load_file('dp_numbering.w',installation),
 	sysh:load_file('dp_lowlevel_builtins.w',installation),
 	sysh:load_file('dp_compile.w',installation),
 	sysh:load_file('dp_halread.w',installation),
 	sysh:load_file('dp_forhalread.w',installation),
 	sysh:load_file('dp_io.w',installation),
 	sysh:load_file('dp_optimize.w',installation),
 	sysh:load_file('dp_logic_engines.w',installation),
	fail.
boot :- 
	toplevel,
	fail.
boot :-
	halt(0).
