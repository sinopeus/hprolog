p(4).

t :- 	spawn([],writeln('ok'),ID),
	spawn([],writeln('ok'),ID2).
	
test :- 	spawn(Sorted,sort([1,5,8,2,3,9],Sorted),ID),
		writeln(ID),
		spawn(Sorted2,sort([5,8,25,2,1,3,5,4,9],Sorted2),ID2),
		writeln(ID2),
		receive(ID,the(Out)),
		writeln(Out),
		stop(ID),
		stop(ID2).







sorteer(Lijst,Gesorteerd) :- 
		p(P),
		divideList( Lijst, P, Delen ),
		hub(Hub),
		makeThreads(Hub,Delen,IDS),
		writeln(IDS),
		merge_delen( IDS, Gesorteerd ).

merge_delen( [], [] ).
merge_delen( [ID|RID], Out ):-
		writeln('ontvangen'),
		receive( ID, the(Deel) ),
		writeln(Deel),
		stop(ID),
		merge_delen(RID,AndereDelen),
		merge(Deel,AndereDelen,Out).

makeThreads( _, [], [] ).
makeThreads( Q, [H|R],[ID|RID] ):-
		writeln(H),
             spawn(Sorted,sort(H,Sorted),ID),
             makeThreads( Q, R, RID ).

divideList( List, P, Res ) :-
            length( List, L ),
            A is (L+P-1)//P,
            makeLists( P, A, List, Res ).

makeLists( 1, _ , List, [List] ) :- !.
makeLists( N, A, List, [H|R] ) :-
           take( List, A , H, ListOut ),
           NN is N - 1,
           makeLists( NN, A, ListOut, R ).


merge( [], X, X ) :-!.
merge( X, [], X ) :-!.
merge( [A|RA], [B|RB], [A|R] ) :-
	A < B,
	!,
	merge( RA,[B|RB],R).
merge( [A|RA], [B|RB], [B|R] ) :-
	merge( [A|RA],RB,R).

take( [], _, [], [] ).
take( List, 0, [], List ) :- !.
take( [H|R], N, [H|L1], L2 ) :-
        NN is N - 1,
        take( R, NN, L1, L2).

