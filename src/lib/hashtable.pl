% author: Tom Schrijvers
% email:  Tom.Schrijvers@cs.kuleuven.ac.be
% copyright: K.U.Leuven, 2004

% Bart Demoen 3 sept 2006
% original name of this module was chr_hashtable_store - removed chr
% stuff and tried to improve stuff; made "compatible" with B-Prolog

:- module(hashtable,[new_hashtable/1, new_hashtable/2,
		     hashtable_register/3,
		     hashtable_get/3,
		     hashtable_delete/2, hashtable_delete/3,
		     int_hashtable_register/4,
		     int_hashtable_get/4
		    ]).

%% :- use_module(library(pairlist)).
%% :- use_module(library(lists)).


default_initial_capacity(11).

nextcapacity(0    , 11).
nextcapacity(11   , 53).
nextcapacity(53   , 264).
nextcapacity(263  , 1103).
nextcapacity(1103 , 3167).
nextcapacity(3167 , 10067).
nextcapacity(10067, 30011).
nextcapacity(30011, 99991).

compute_next_capacity(Old,New) :-
	nextcapacity(A,B),
	A =< Old,
	Old < B,
	!,
	New = B.
compute_next_capacity(I,I). % because no larger prime available :-)


%%%%%%%%% new_hashtable/1 & 2 %%%%%%%%%

new_hashtable(HT) :-
	default_initial_capacity(Capacity),
	new_hashtable1(Capacity,HT).

new_hashtable(HT,Capacity) :- new_hashtable1(Capacity,HT).

new_hashtable1(Capacity,HT) :-
        functor(T1,t,Capacity),
        HT = ht(Capacity,0,Table),
        Table = T1.

%%%%%%%%% hashtable_get/3 %%%%%%%%%

hashtable_get(HT,Term,Value) :-
	hash_term1(Term,Hash),
	int_hashtable_get(HT,Hash,Term,Value).

int_hashtable_get(HT,Hash,Key,Value) :-
	HT = ht(Capacity,_,Table),
	Index is (Hash mod Capacity) + 1,
	arg(Index,Table,Bucket),
	bucket_lookup(Bucket,Key,Value).

bucket_lookup(Bucket,Key,Value) :-
	nonvar(Bucket),
	Bucket = [K=V|Bs],
	(K == Key ->
	    Value = V
	;
	    bucket_lookup(Bs,Key,Value)
	).

%%%%%%%%% hashtable_register %%%%%%%%%

hashtable_register(HT,Term,Value) :-
	hash_term1(Term,Hash),
	int_hashtable_register(HT,Hash,Term,Value).

int_hashtable_register(HT,Hash,Key,Value) :-
	HT = ht(Capacity0,Load,Table0),
	( Load ==  Capacity0 ->
	    expand_ht(HT,Capacity),
	    arg(3,HT,Table)
	;
	    Table = Table0,
	    Capacity = Capacity0
	),
	NLoad is Load + 1,
	setarg(2,HT,NLoad),
	Index is (Hash mod Capacity) + 1,
	arg(Index,Table,Bucket),
	( var(Bucket) ->
	    Bucket = [Key=Value | _]
	;
	    bucket_member(Bucket,Key,Value)
	).

bucket_member([KV|KVs],Key,Value) :-
	(var(KV) ->
	    KV = (Key = Value)
	;
	    KV = (K = _),
	    (K == Key ->
		setarg(2,KV,Value)
	    ;
		bucket_member(KVs,Key,Value)
	    )
	).

%%%%%%%%% hashtable_delete/2 & 3 %%%%%%%%%

hashtable_delete(HT,Key) :-
	hashtable_delete(HT,Key,_).

hashtable_delete(HT,Key,Value) :-
	hash_term1(Key,Hash),
	int_hashtable_delete(HT,Hash,Key,Value).

int_hashtable_delete(HT,Hash,Key,Value) :-
	HT = ht(Capacity,Load,Table),
	NLoad is Load - 1,
	setarg(2,HT,NLoad),
	Index is (Hash mod Capacity) + 1,
	arg(Index,Table,Bucket),
	bucket_delete(Bucket,Key,Value,NewBucket),
	setarg(Index,Table,NewBucket).

bucket_delete(Bucket,Key,Value,NewBucket) :-
	nonvar(Bucket),
	Bucket = [KV|Bs],
	KV = (K=V),
	(K == Key ->
	    V = Value,
	    NewBucket = Bs
	;
	    NewBucket = [K=v|NBs],
	    bucket_delete(Bs,Key,Value,NBs)
	).

%%%%%%%%%%%%%% expand_ht - doesn't work yet %%%%%%%%%%%%%%

%% expand_ht(A,B) :- expand_ht1(A,B), fail.
%% expand_ht(A,B) :- expand_ht1(A,B).

expand_ht(HT,NewCapacity) :-
	HT = ht(Capacity,_,Table),
	compute_next_capacity(Capacity,NewCapacity),
	functor(NewTable,t,NewCapacity),
	setarg(1,HT,NewCapacity),
	setarg(2,HT,0),
	setarg(3,HT,NewTable),
	Table =.. [_|KVs],
	copy_hash(KVs,HT).

copy_hash([],_).
copy_hash([KV|KVs],HT) :-
	copy_hash_l(KV,HT),
	copy_hash(KVs,HT).

copy_hash_l(KVs,HT) :-
	(var(KVs) ->
	    true
	;
	    KVs = [K=V|R],
	    hashtable_register(HT,K,V),
	    copy_hash_l(R,HT)
	).

% hash_term1(X,Y) :- hash_term(X,Y), fail.
hash_term1(X,Y) :- hash_term(X,Y).
