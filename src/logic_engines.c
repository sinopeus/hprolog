/*
  Original written by Timon Van Overveldt Febr-May 2011
  Changes by Bart Demoen
*/
  

#include "toinclude.h"

static struct engine* engines;


#ifdef MULTI_THREADED

#include "shared_memory.h"
#include "logic_engines.h"
#include "asm-generic/errno-base.h"

dlong MAX_ENGINES = 100;
dlong MAX_HUBS = 100;
dlong MAX_QUEUE_SIZE = 8192;

extern dlong execute_machine(codep initialpc, dlong first_time,
			     struct machine** setmach, 
			     struct machine_opts *mach_optsp, struct engine* engine);

extern void findall_free(int i, findall_solution_list* list, struct machine *);
extern void fatal_message(char *);
extern dlong findall_add_to_list(dlong* p1, int dolist, struct machine *machp,
				 findall_solution_list *current_findall);
extern void findall_get_solution_single(dlong* p1, short fromlist, 
					findall_solution_list* list,
					struct machine *machp);

static pthread_attr_t engine_thread_attrs;
static char* engine_id_reservations;
static dlong* engine_messages_left;
static dlong engine_ids;
// Guards the above data structures from simultaneous access
static pthread_rwlock_t engine_store_lock;

static struct hub* hubs;
static dlong* hub_id_reservations;
static dlong* hub_messages_left;
static dlong hub_ids;
// Guards the above data structures from simultaneous access
static pthread_rwlock_t hub_store_lock;

static void execute_engine(struct engine* engine, struct engine* thisengine);
static void init_engine(dlong index);
static void release_engine(struct engine* engine);
static void engine_stop_linked(struct engine* engine, struct engine* thisengine);
static void inbox_purge_messages(dlong from_engine_index, struct inbox* inbox);
static void execute_engine_thread(struct engine* engine, struct engine* thisengine);
int builtin_stop_engine(dlong engine_index, short purge, struct engine* thisengine);
static void init_hub(dlong id);


// --  Global initialization & cleanup

#define malloc_check(p,s) if (!p) fatal_message(s)
// Initialize global variables related to logic engines.
// Argument 1 mach is the top-level machine (which has engine id 0).
// Returns the root engine, belonging to the given machine.
struct engine* init_engines_hubs()
{
  int i;

  engines = calloc(MAX_ENGINES,sizeof(struct engine));
  malloc_check(engines,"MAX_ENGINES too high ?");
  engine_id_reservations = calloc(MAX_ENGINES,sizeof(char));
  malloc_check(engine_id_reservations,"MAX_ENGINES too high ?");
  engine_messages_left = calloc(MAX_ENGINES,sizeof(dlong));
  malloc_check(engine_messages_left,"MAX_ENGINES too high ?");
  hubs = calloc(MAX_HUBS,sizeof(struct hub));
  malloc_check(hubs,"MAX_HUBS too high ?");
  hub_id_reservations = calloc(MAX_HUBS,sizeof(dlong));
  malloc_check(hub_id_reservations,"MAX_HUBS too high ?");
  hub_messages_left = calloc(MAX_HUBS,sizeof(dlong));
  malloc_check(hub_messages_left,"MAX_HUBS too high ?");

  // Initialize the mutex guarding simultaneous access to the engine store.
  pthread_rwlock_init(&engine_store_lock, NULL);
  engine_id_reservations[0] = 1;
  // Initialize the engine_ids to 1, since engine id 0 is the root engine.
  engine_ids = 1;
  // Initialize the hub_ids to 0, since at first there are no existing hubs.
  hub_ids = 0;

  for (i = 0; i < MAX_ENGINES; ++i) init_engine(i);
  
  for (i = 0; i < MAX_HUBS; ++i) init_hub(i);

  return &engines[0];
} /* init_engines_hubs */


// Destroy initialize global variables related to logic engines.
void engines_cleanup()
{
  int i;

  return; // Wouter did this but why

  // Destroy the mutex guarding simultaneous access to the engine store.
  pthread_rwlock_destroy(&engine_store_lock);

  release_engine(&engines[0]);

  for (i = 0; i < MAX_ENGINES; i++)
    {
      struct engine *e = &engines[i];
      if (e->start_goal)
	{
	  free(e->start_goal);
	  e->start_goal = 0;
	}
    }

  free(engines);
  free(engine_id_reservations);
  free(engine_messages_left);
  free(hubs);
  free(hub_id_reservations);
  free(hub_messages_left);
} /* engines_cleanup */

// --  Per-engine initialization & cleanup
static void reset_inbox(struct inbox* inbox)
{
  inbox->head = NULL;
  inbox->tail = NULL;
  inbox->size = 0;
  inbox->waiting_push = 0;
  inbox->waiting_pop = 0;
} /* reset_inbox */

static void init_inbox(struct inbox* inbox)
{
  pthread_mutex_init(&inbox->mutex, NULL);
  sem_init(&inbox->new_push, 0, 0);
  sem_init(&inbox->new_pop, 0, 0);
  reset_inbox(inbox);
} /* init_inbox */

static void init_engine(dlong index)
{
  struct engine* engine = &engines[index];

  engine_messages_left[index] = 0;
  pthread_mutex_init(&engine->mutex, NULL);

  engine->start_goal = (findall_solution_list *) calloc(sizeof(findall_solution_list),1);
  engine->index = index;
  engine->parent_id = 0;
  engine->cancelled = 0;
  pthread_rwlock_init(&engine->cancel_lock, NULL);
  engine->stop_signal = NULL;

  engine->engines_head = NULL;
  engine->engines_count = 0;
  sem_init(&engine->stopped_engines, 0, 0);

  init_inbox(&engine->inbox);
} /* init_engine */

// Resets an engine for reuse
static void reset_engine(dlong index)
{
  struct engine* engine = &engines[index];
  engine->locmachp = NULL;
  engine->parent_id = 0;
  engine->cancelled = 0;
  engine->stop_signal = NULL;

  engine->engines_head = NULL;
  engine->engines_count = 0;

  reset_inbox(&engine->inbox);
} /* reset_engine */

static void init_hub(dlong index)
{
  struct hub* hub = &hubs[index];
  hub_messages_left[index] = 0;
  pthread_mutex_init(&hub->mutex, NULL);
  hub->index = index;
  hub->cancelled = 0;
  pthread_rwlock_init(&hub->cancel_lock, NULL);

  hub->engines_head = NULL;
  hub->engines_count = 0;
  sem_init(&hub->stopped_engines, 0, 0);

  init_inbox(&hub->inbox);
} /* init_hub */

// Resets a hub for reuse.
static void reset_hub(dlong index)
{
  struct hub* hub = &hubs[index];
  hub->cancelled = 0;

  hub->engines_head = NULL;
  hub->engines_count = 0;

  reset_inbox(&hub->inbox);
} /* reset_hub */

static void release_inbox(struct inbox* inbox, struct machine *locmachp)
{
  pthread_mutex_lock(&inbox->mutex);
  struct message* msg = inbox->head;
  struct message* next_msg;

  while (msg != NULL)
  {
    findall_free(0, msg->msg, locmachp);
    free(msg->msg);
    next_msg = msg->next_msg;
    free(msg);
    msg = next_msg;
  }
  inbox->head = NULL;
  inbox->tail = NULL;
  pthread_mutex_unlock(&inbox->mutex);
} /* release_inbox */

// Clean up and deallocate an hub's resources.
static void release_hub(struct hub* hub, struct machine *locmachp)
{
  if (hub == NULL)
    return;

  // Remove the hub's hubine from the hub store and release it's INDEX
  pthread_rwlock_wrlock(&hub_store_lock);
  hub_id_reservations[hub->index] = 0;
  pthread_rwlock_unlock(&hub_store_lock);

  pthread_mutex_lock(&hub->mutex);

  // Free the inbox, freeing all remaining messages first
  release_inbox(&hub->inbox,locmachp);

  pthread_mutex_unlock(&hub->mutex);
} /* release_hub */

// Clean up and deallocate an engine's resources.
// Does not deallocate a machine's resources. (see dealloc_machine in execute_machine)
// The engine has stopped executing, so no race conditions can occur.
static void release_engine(struct engine* engine)
{
  if (engine == NULL)
    return;

  // Remove the engine's engine from the engine store and release it's INDEX
  pthread_rwlock_wrlock(&engine_store_lock);
  engine_id_reservations[engine->index] = 0;
  pthread_rwlock_unlock(&engine_store_lock);

  pthread_mutex_lock(&engine->mutex);


  // Free the start goal
  if (engine->start_goal->first_chunk != NULL)
    {
      findall_free(0, engine->start_goal,engine->locmachp);
      engine->start_goal->first_chunk = NULL; // Bart - rare volgorde ?
    }

  // Free the inbox, freeing all remaining messages first
  release_inbox(&engine->inbox, engine->locmachp);

  // Signal that we stopped (if needed)
  if (engine->stop_signal != NULL)
    sem_post(engine->stop_signal);

  change_nr_of_running_threads(-1) ;
  pthread_mutex_unlock(&engine->mutex);
} /* release_engine */

// Reserves a new hub ID & returns the hub belonging to it.
// Returns NULL if no new ID could be reserved.
// This function is synchronized.
static struct hub* get_new_hub()
{
  int valid = 0;
  dlong id;

  pthread_rwlock_wrlock(&hub_store_lock);
  // If we haven't used all available hub ids,
  // then just take the next one
  if (hub_ids < MAX_HUBS-1)
  {
    id = hub_ids++;
    valid = 1;
  }
  // If we _have_ used all available hub ids,
  // then try to find one that was released.
  else
  {
    id = hub_ids;
    while (id >= 0)
    {
      // Check if the current hub id has been released,
      // AND has no messages left to be handled.
      if (!hub_id_reservations[id] && hub_messages_left[id] == 0)
      {
        valid = 1;
        break;
      }
      --id;
    }
  }
  // If we found a valid hub id,
  // reserve it for future use by builtin_new_hub
  if (valid)
    hub_id_reservations[id] = 1;
  else
    fprintf(stderr, "Maximum amount of hubs reached... (%ld)\n", MAX_HUBS);
  pthread_rwlock_unlock(&hub_store_lock);

  if (!valid)
    return NULL;
  else
    return &hubs[id];
} /* get_new_hub */

// Reserves a free engine ID & returns the engine belonging to it.
// Returns NULL if no ID could be reserved.
// This function is synchronized.
static struct engine* get_existing_engine()
{
  int valid = 0;
  dlong index;

  pthread_rwlock_wrlock(&engine_store_lock);
  // If we haven't used all available engine ids,
  // then just take the next one
  if (engine_ids < MAX_ENGINES-1)
  {
    index = engine_ids++;
    valid = 1;
  }
  // If we _have_ used all available engine ids,
  // then try to find one that was released.
  else
  {
    index = engine_ids;
    while (index > 0)
    {
      // Check if the current engine index has been released,
      // AND has no messages left to be handled.
      if (!engine_id_reservations[index] && engine_messages_left[index] == 0)
      {
        valid = 1;
        break;
      }
      --index;
    }
  }
  // If we found a valid engine id,
  // reserve it for future use by builtin_spawn
  if (valid)
    engine_id_reservations[index] = 1;
  else
    fprintf(stderr, "Maximum amount of engines reached... (%ld)\n", MAX_ENGINES);
  pthread_rwlock_unlock(&engine_store_lock);

  if( valid )
    change_nr_of_running_threads(1) ;

  if (!valid)
    return NULL;
  else
    return &engines[index];
} /* get_existing_engine */

// Returns the hub belonging to the given ID, or NULL if the given ID isn't active or is cancelled.
// This function is synchronized.
static struct hub* get_hub(dlong index)
{
  short active;
  struct hub* hub;
  int cancelled;

  if ((index < 0) || (index >= MAX_HUBS)) return(0);

  pthread_rwlock_rdlock(&hub_store_lock);
  active = hub_id_reservations[index];
  pthread_rwlock_unlock(&hub_store_lock);
  if (!active) return(NULL);

  hub = &hubs[index];
  pthread_rwlock_rdlock(&hub->cancel_lock);
  cancelled = hub->cancelled;
  pthread_rwlock_unlock(&hub->cancel_lock);
  if (cancelled) return(NULL);

  return(hub);
} /* get_hub */

// Returns the engine belonging to the given INDEX, or NULL if the given INDEX isn't active or is cancelled.
// This function is synchronized.
static struct engine* get_engine(dlong index)
{
  short active;
  struct engine* engine;
  int cancelled;

  if ((index < 0) || (index >= MAX_ENGINES)) return(0);

  pthread_rwlock_rdlock(&engine_store_lock);
  active = engine_id_reservations[index];
  pthread_rwlock_unlock(&engine_store_lock);
  if (!active) return NULL;

  engine = &engines[index];
  pthread_rwlock_rdlock(&engine->cancel_lock);
  cancelled = engine->cancelled;
  pthread_rwlock_unlock(&engine->cancel_lock);
  if (cancelled) return(NULL);

  return(engine);
} /* get_engine */

// Destructor for an engine's machine.
static void engine_destructor(struct engine* engine, struct engine* thisengine)
{
  pthread_mutex_lock(&engine->mutex);
  engine_stop_linked(engine, thisengine);
  pthread_mutex_unlock(&engine->mutex);

  release_engine(engine);
} /* engine_destructor */

// --  Engine execution stuff
// Executes a machine and after execution, deallocates & cleans it up.
static void execute_engine(struct engine* engine, struct engine* thisengine)
{
  codep engine_toplevel_goalp = entries[engine_toplevel_goal_0];

  execute_machine(engine_toplevel_goalp, 0, NULL, NULL, engine);

  engine_destructor(engine, thisengine);
} /* execute_engine */

// First function a new engine thread executes.
// Calls execute_engine(...) to execute the engine.
// Also handles auto-detaching of the thread.
static void execute_engine_thread(struct engine* engine, struct engine* thisengine)
{
  execute_engine(engine, thisengine);

  // Detach ourselves to make sure resources are released without having to join
  pthread_detach(pthread_self());
  pthread_exit(NULL);
} /* execute_engine_thread */

// Creates a new hub, returning the hub's index.
dlong builtin_new_hub()
{
  struct hub* hub = get_new_hub();
  if (hub == NULL)
  {
    fprintf(stderr, "Couldn't get a new hub...\n");
    return -1;
  }

  pthread_mutex_lock(&hub->mutex);

  reset_hub(hub->index);

  pthread_mutex_unlock(&hub->mutex);
  return hub->index;
} /* builtin_new_hub */

// Starts a new engine using the given parent engine & goal, returning the new engine's index.
dlong builtin_spawn(dlong* goal, dlong parent_id, short link, struct engine* thisengine)
{
  struct engine* engine = get_existing_engine();
  struct machine* machp;
  dlong * w ;

  if (engine == NULL) return -1;

  pthread_mutex_lock(&engine->mutex);

  reset_engine(engine->index);
  // waarom wordt die reset niet elders gedaan - bij destroy ?
  engine->parent_id = parent_id;

  // Allocate a new chunk for our findall_solution_list. The list itself is already allocated.
  if (!(w = (dlong *)malloc(DEFAULT_MSG_CHUNK_SIZE * sizeof(dlong *))))
    fatal_message("no temp space for message");
  
  *w = 0 ;
  engine->start_goal->first_chunk = engine->start_goal->current_chunk = w ;
  w++ ; engine->start_goal->top_of_chunk = w ;
  engine->start_goal->size = 0 ;
  engine->start_goal->limit_of_chunk = w + DEFAULT_MSG_CHUNK_SIZE - 1;
  engine->start_goal->barrier = 0;
  //  current_findall = engine->start_goal;

  // Copy term to chunk, the zero indicates "dont create a list"
  findall_add_to_list(goal, 0, thisengine->locmachp, engine->start_goal);


  // Link engine to parent if needed
  if (link && is_hub_id(parent_id))
  {
    struct hub* hub = get_hub(get_hub_index(parent_id));
    if (hub == NULL)
    {
      fprintf(stderr, "Couldn't get hub to attach to...\n");
      pthread_mutex_unlock(&engine->mutex);
    }
    else
    {
      engine->stop_signal = &hub->stopped_engines;
      pthread_mutex_unlock(&engine->mutex);

      struct linked_engine* link = malloc(sizeof(struct linked_engine));
      link->engine = engine;
      
      pthread_mutex_lock(&hub->mutex);
      hub->engines_count++;
      link->next = hub->engines_head;
      hub->engines_head = link;
      pthread_mutex_unlock(&hub->mutex);
    }
  }
  else if (link && is_engine_id(parent_id))
  {
    pthread_mutex_unlock(&engine->mutex);
    struct engine* parent_engine = get_engine(get_engine_index(parent_id));
    if (parent_engine == NULL)
      fprintf(stderr, "Couldn't get engine to attach to...\n");
    else
    {

      struct linked_engine* link = malloc(sizeof(struct linked_engine));
      link->engine = engine;
      
      pthread_mutex_lock(&parent_engine->mutex);
      parent_engine->engines_count++;
      link->next = parent_engine->engines_head;
      parent_engine->engines_head = link;
      pthread_mutex_unlock(&parent_engine->mutex);
    }
  }
  else
    pthread_mutex_unlock(&engine->mutex);

  { pthread_t new_thread;

    // Execute the new engine's goal in a new thread
    if (pthread_create(&new_thread, &engine_thread_attrs,
		       (void*) execute_engine_thread, (void *)engine))
      fatal_message("thread creation failed");
    
  }

  return engine->index;
} /* builtin_spawn */

// Return the size of the start goal for the engine currently executing in this thread.
dlong builtin_engine_start_goal_size(struct engine* thisengine)
{
  struct engine* engine = thisengine;
  dlong size = engine->start_goal->size;
  return size;
} /* builtin_engine_start_goal_size */


// Copies the start goal of the engine currently executing in this thread
// to the given unbound variable. The variable must be on the heap of the current engine.
// Returns 1 on success, 0 on failure. Can only be called once and fails on later calls.
int builtin_engine_start_goal(dlong* term, struct engine* thisengine)
{
  struct engine* engine = thisengine;

  if (engine->start_goal->first_chunk != NULL)
  {
    findall_get_solution_single(term, 0, engine->start_goal, engine->locmachp);

    // Free the start goal
    findall_free(0, engine->start_goal,engine->locmachp);
    engine->start_goal->first_chunk = NULL; // Bart - rare volgorde ?
    return(1);
  }
  return(0);
} /* builtin_engine_start_goal */


// Stops (cancels) a hub & all it's engines
int builtin_stop_hub(dlong hub_index, struct machine *locmachp, struct engine* thisengine)
{

  if ((hub_index < 0) || (hub_index >= MAX_HUBS)) return(0);
  // Since we stop the hub, no more of it's message will be handled or needed
  pthread_rwlock_wrlock(&engine_store_lock);
  hub_messages_left[hub_index] = 0;
  pthread_rwlock_unlock(&engine_store_lock);

  // Get the hub belonging to the given hub id
  struct hub* hub = get_hub(hub_index);
  if (hub == NULL)
    return 1;

  pthread_rwlock_rdlock(&hub->cancel_lock);
  int cancelled = hub->cancelled;
  pthread_rwlock_unlock(&hub->cancel_lock);
  if (cancelled)
    return 1;

  pthread_rwlock_wrlock(&hub->cancel_lock);
  hub->cancelled = 1;
  pthread_rwlock_unlock(&hub->cancel_lock);

  pthread_mutex_lock(&hub->mutex);
  int i;
  for (i = hub->inbox.waiting_push; i > 0; i--)
    sem_post(&hub->inbox.new_push);
  for (i = hub->inbox.waiting_pop; i > 0; i--)
    sem_post(&hub->inbox.new_pop);

  struct linked_engine* link = hub->engines_head;
  struct linked_engine* free_link;
  // Stop engines
  while (link != NULL)
  {
    free_link = link;
    if (get_hub_index(link->engine->parent_id) == hub_index)
      builtin_stop_engine(link->engine->index, 0, thisengine);
    link = link->next;
    free(free_link);
  }
  // Wait for engines to stop
  int engines_count = hub->engines_count;
  hub->engines_head = NULL;
  hub->engines_count = 0;
  pthread_mutex_unlock(&hub->mutex);
  while (engines_count > 0)
  {
    engines_count--;
    sem_wait(&hub->stopped_engines);
  }

  release_hub(hub,locmachp);

  return 1;
} /* builtin_stop_hub */

static void engine_stop_linked(struct engine* engine, struct engine* thisengine)
{
  struct linked_engine* link = engine->engines_head;
  struct linked_engine* free_link;

  while (link != NULL)
  {
    free_link = link;
    if (get_engine_index(link->engine->parent_id) == engine->index)
      builtin_stop_engine(link->engine->index, 0, thisengine);
    link = link->next;
    free(free_link);
  }
  int engines_count = engine->engines_count;
  engine->engines_head = NULL;
  engine->engines_count = 0;
} /* engine_stop_linked */

// Stops (cancels) an engine
int builtin_stop_engine(dlong engine_index, short purge, struct engine* thisengine)
{
  if ((engine_index < 0) || (engine_index >= MAX_ENGINES)) return(0);

  // Since we stop the engine, no more of it's message will be handled or needed
  pthread_rwlock_wrlock(&engine_store_lock);
  engine_messages_left[engine_index] = 0;
  pthread_rwlock_unlock(&engine_store_lock);

  // We can't stop engine #0 (the main engine)
  if (engine_index == 0) return(0);

  if (purge)
  {
    pthread_mutex_lock(&thisengine->mutex);
    inbox_purge_messages(engine_index, &thisengine->inbox);
    pthread_mutex_unlock(&thisengine->mutex);
  }

  // Get the engine belonging to the given engine id
  struct engine* engine = get_engine(engine_index);
  if (engine == NULL)
    return 1;

  pthread_rwlock_rdlock(&engine->cancel_lock);
  int cancelled = engine->cancelled;
  pthread_rwlock_unlock(&engine->cancel_lock);
  if (cancelled)
    return 1;

  pthread_rwlock_wrlock(&engine->cancel_lock);
  engine->cancelled = 1;
  pthread_rwlock_unlock(&engine->cancel_lock);

  pthread_mutex_lock(&engine->mutex);
  int i;
  for (i = engine->inbox.waiting_push; i > 0; i--)
    sem_post(&engine->inbox.new_push);
  for (i = engine->inbox.waiting_pop; i > 0; i--)
    sem_post(&engine->inbox.new_pop);

  engine_stop_linked(engine, thisengine);
  pthread_mutex_unlock(&engine->mutex);

  return(1);
} /* builtin_stop_engine */

// --  Engine messaging

// Sends a message from the current engine out to the given inbox.
// The given message gets copied and stored in the given inbox.
// The "mutex" argument will be locked & unlocked during manipulating of the inbox.
static int inbox_push_msg(dlong* term, struct inbox* inbox, 
			  pthread_mutex_t* mutex, int* cancelled,
			  pthread_rwlock_t* cancel_lock,
			  struct engine* thisengine)
{
  pthread_mutex_lock(mutex);

  // If the inbox's size limit is reached, wait for a pop before pushing
  int cancelled_val = 0;
  if (MAX_QUEUE_SIZE > 0 && inbox->waiting_push == 0 &&
      inbox->size >= MAX_QUEUE_SIZE)
  {
    inbox->waiting_pop++;
    pthread_mutex_unlock(mutex);
    sem_wait(&inbox->new_pop);
    pthread_mutex_lock(mutex);
    inbox->waiting_pop--;

    pthread_rwlock_rdlock(cancel_lock);
    cancelled_val = *cancelled;
    pthread_rwlock_unlock(cancel_lock);
  }
  if (cancelled_val)
  {
    pthread_mutex_unlock(mutex);
    return 0;
  }

  // Increase the machine's inbox by 1
  inbox->size++;

  struct message* new_msg = malloc(sizeof(struct message));
  new_msg->next_msg = NULL;
  if (inbox->head == NULL)
    inbox->head = new_msg;
  if (inbox->tail != NULL)
    inbox->tail->next_msg = new_msg;
  inbox->tail = new_msg;
  
  // Allocate a new findall_solution_list.
  // This list will be used to trick existing findall code to copy the given term.
  dlong * w ;

  new_msg->msg = (findall_solution_list *) malloc(sizeof(findall_solution_list));
  if (!(w = (dlong *)malloc(DEFAULT_MSG_CHUNK_SIZE * sizeof(dlong *))))
    fatal_message("no temp space for message");

  *w = 0 ;
  new_msg->msg->first_chunk = new_msg->msg->current_chunk = w ;
  w++ ; new_msg->msg->top_of_chunk = w ;
  new_msg->msg->size = 0 ;
  new_msg->msg->limit_of_chunk = w + DEFAULT_MSG_CHUNK_SIZE - 1;
  new_msg->msg->barrier = 0;
  //  current_findall = new_msg->msg;

  // Copy term to chunk, the zero indicates "dont create a list"
  findall_add_to_list(term, 0, thisengine->locmachp, new_msg->msg);

  // Message's sender
  new_msg->from_engine_index = thisengine->index;

  // If the machine's inbox is being waited on for a new message,
  // signal that is has now arrived.
  if (inbox->waiting_push)
    sem_post(&inbox->new_push);

  pthread_mutex_unlock(mutex);
  return 1;
} /* inbox_push_msg */

// Sends a message from the current engine out to the inbox of a given hub.
// The given message gets copied and stored in the given hub's inbox.
// Returns 0 on failure (invalid hub id), 1 on success.
int builtin_hub_send(dlong to_hub_index, dlong* term, struct engine* thisengine)
{
  // Get the hub belonging to the given hub id
  struct hub* hub = get_hub(to_hub_index);
  if (hub == NULL)
    return 0;

  // Indicate that this engine & hub have one more message left to handle
  pthread_rwlock_wrlock(&engine_store_lock);
  engine_messages_left[thisengine->index]++;
  hub_messages_left[to_hub_index]++;
  pthread_rwlock_unlock(&engine_store_lock);

  inbox_push_msg(term, &hub->inbox, &hub->mutex, &hub->cancelled, &hub->cancel_lock, thisengine);

  return 1;
} /* builtin_hub_send */

// Sends a message from the current engine out to the inbox of a given engine.
// The given message gets copied and stored in the given engine's inbox.
// Returns 0 on failure (invalid engine id), 1 on success.
int builtin_engine_send(dlong to_engine_id, dlong* term, struct engine* thisengine)
{
  // Get the engine belonging to the given engine id
  struct engine* engine = get_engine(to_engine_id);
  if (engine == NULL)
    return 0;

  // Indicate that this engine has one more message left to handle
  pthread_rwlock_wrlock(&engine_store_lock);
  engine_messages_left[thisengine->index]++;
  pthread_rwlock_unlock(&engine_store_lock);

  inbox_push_msg(term, &engine->inbox, &engine->mutex, &engine->cancelled, &engine->cancel_lock, thisengine);

  return 1;
} /* builtin_engine_send */

// Pops a message from the given inbox.
// The messages can be filtered by sender engine (from_engine_index).
// The "mutex" argument will be locked & unlocked during manipulating of the inbox.
// Return NULL on failure or the newest message on success.
static struct message* inbox_pop_msg(dlong from_engine_index,
    struct inbox* inbox, pthread_mutex_t* mutex, 
    int* cancelled, pthread_rwlock_t* cancel_lock)
{
  pthread_mutex_lock(mutex);

  struct message *msg, *prev_msg = NULL;
  int i;
  int found_valid = 0;
  int cancelled_val;

  // Keep trying to find a valid message until we find one
  while (!found_valid)
  {
    pthread_rwlock_rdlock(cancel_lock);
    cancelled_val = *cancelled;
    pthread_rwlock_unlock(cancel_lock);
    if (cancelled_val)
      break;
    
    // If this is our first run, start from head
    if (prev_msg == NULL)
      msg = inbox->head;
    // If this is not our first run, start searching from the newest message
    else
      msg = prev_msg->next_msg;
    while (msg != NULL)
    {
      // If we don't care for the message's sender, just return the first one we find
      if (from_engine_index == -1)
      {
        found_valid = 1;
        break;
      }
      // Otherwise, check if the message is from the right sender
      else if (msg->from_engine_index == from_engine_index)
      {
        found_valid = 1;
        break;
      }
      // If the message didn't match our selection, try the next one
      prev_msg = msg;
      msg = msg->next_msg;
    }

    // If we didn't find a valid message, wait for a new one and try again.
    pthread_rwlock_rdlock(cancel_lock);
    cancelled_val = *cancelled;
    pthread_rwlock_unlock(cancel_lock);
    if (cancelled_val)
      break;

    if (!found_valid)
    {
      inbox->waiting_push++;
      if (inbox->waiting_pop)
        sem_post(&inbox->new_pop);
      pthread_mutex_unlock(mutex);
      sem_wait(&inbox->new_push);
      pthread_mutex_lock(mutex);
      inbox->waiting_push--;
    }
  }
  if (cancelled_val)
  {
    pthread_mutex_unlock(mutex);
    return NULL;
  }

  // We found a valid message so rearrange the linked list of queued messages.
  inbox->size--;
  // If the found message is the head of the queue, set the new head to the next message
  if (inbox->head == msg)
    inbox->head = msg->next_msg;
  // Otherwise, just connect the previous message to the next one
  else
    prev_msg->next_msg = msg->next_msg;
  // If the found message is the tail of the queue, set the new tail to the previous message
  if (inbox->tail == msg)
    inbox->tail = prev_msg;

  // If the machine's inbox is being waited on for a message to be popped,
  // signal that is has now been popped.
  if (inbox->waiting_pop)
    sem_post(&inbox->new_pop);

  pthread_mutex_unlock(mutex);

  return msg;
} /* inbox_pop_msg */

static void inbox_purge_messages(dlong from_engine_index,struct inbox* inbox)
{
  struct message *msg, *prev_msg = NULL;
  msg = inbox->head;
  int found_valid = 1;
  int i;

  while (found_valid && msg != NULL)
  {
    found_valid = 0;
    // If we don't care for the message's sender, just return the first one we find
    if (from_engine_index == -1)
      found_valid = 1;
    // Otherwise, check if the message is from the right sender
    else if (msg->from_engine_index == from_engine_index)
      found_valid = 1;

    if (found_valid)
    {
      // We found a valid message so rearrange the linked list of queued messages.
      inbox->size--;
      // If the found message is the head of the queue, set the new head to the next message
      if (inbox->head == msg)
        inbox->head = msg->next_msg;
      // Otherwise, just connect the previous message to the next one
      else
        prev_msg->next_msg = msg->next_msg;
      // If the found message is the tail of the queue, set the new tail to the previous message
      if (inbox->tail == msg)
        inbox->tail = prev_msg;

      // If the machine's inbox is being waited on for a message to be popped,
      // signal that is has now been popped.
      if (inbox->waiting_pop)
        sem_post(&inbox->new_pop);
    }
    prev_msg = msg;
    msg = msg->next_msg;
  }
} /* inbox_purge_messages */

// Pops a message from the given hub's inbox.
// Return NULL on failure or the newest message on success.
struct message* builtin_hub_next_msg(dlong hub_id, dlong from_engine_index, struct engine *thisengine)
{
  // Get the hub belonging to the given hub id
  struct hub* hub = get_hub(hub_id);
  if (hub == NULL)
    return 0;

  struct message* result = inbox_pop_msg(from_engine_index, &hub->inbox, &hub->mutex, &hub->cancelled, &hub->cancel_lock);

  if (result == NULL) return(0);

  // Indicate that the sender engine and this hub have one less message pending
  pthread_rwlock_wrlock(&engine_store_lock);
  engine_messages_left[result->from_engine_index]--;
  hub_messages_left[hub_id]--;
  pthread_rwlock_unlock(&engine_store_lock);
  return result;
} /* builtin_hub_next_msg */

// Pops a message from the engine currently executing in this thread's inbox.
// The messages can be filtered by sender engine (from_engine_index).
// Return NULL on failure or the newest message on success.
struct message* builtin_engine_next_msg(dlong from_engine_index, struct engine* thisengine)
{
  // Use the engine currently executing in this thread as the sender
  struct engine* engine = thisengine;

  struct message* result = inbox_pop_msg(from_engine_index, &engine->inbox, &engine->mutex, &engine->cancelled, &engine->cancel_lock);

  if (result == NULL) return(0);

  // Indicate that the sender engine has one less message pending
  pthread_rwlock_wrlock(&engine_store_lock);
  engine_messages_left[result->from_engine_index]--;
  pthread_rwlock_unlock(&engine_store_lock);
  return result;
} /* builtin_engine_next_msg */

// Copies the given message to the given unbound variable.
// The unbound variable must be on the heap of the engine currently executing in this thread.
// Return 0 on failure & 1 on success
int builtin_receive(dlong* term, struct message* msg, struct engine* thisengine)
{
  findall_get_solution_single(term, 0, msg->msg, thisengine->locmachp);

  findall_free(0, msg->msg,thisengine->locmachp);
  free(msg->msg);
  free(msg);
  return 1;
} /* builtin_receive */

#else

static void init_engine(dlong index)
{
  struct engine* engine = &engines[index];
  engine->start_goal = (findall_solution_list *) calloc(sizeof(findall_solution_list),1);
} /* init_engine */


// Initialize global variables related to logic engines.
// Argument 1 mach is the top-level machine (which has engine index 0).
// Returns the root engine, belonging to the given machine.
struct engine* init_engines_hubs()
{
  engines = malloc(sizeof(struct engine));

  // Set the toplevel goal to be used by all engines

  // The first engine is the root one
  init_engine(0);

  return &engines[0];
} /* init_engines_hubs */

// Destroy initialize global variables related to logic engines.
void engines_cleanup()
{
  if (engines == NULL)
    return;
  free(engines);
} /* engines_cleanup */


#endif
