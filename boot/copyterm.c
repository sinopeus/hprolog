#include "toinclude.h"
#include "builtins.h"
#include "mach_macros.h"
#include <setjmp.h>
#include <stdlib.h>
#include <string.h>

extern dlong numb_symbs;

#define chb machp->copy_heapbarrier

extern void representation_sharing(struct machine *);

extern void print_heap(dlong arity, struct machine *machp);


/*********************************************************************/
/* statistics                                                        */
/*       not very nice but can do                                    */
/*                                                                   */
/*********************************************************************/

dlong builtin_internalstat(dlong a1, dlong a2, dlong a3, dlong a4, dlong a5,
			   struct machine *machp)
{
  dlong *p1,*p2,*p3,*p4,*p5,*q;
  dlong nr,i,zero;

  p1 = (dlong *)args(a1); deref(p1);
  p2 = (dlong *)args(a2); deref(p2);
  p3 = (dlong *)args(a3); deref(p3);
  p4 = (dlong *)args(a4); deref(p4);
  p5 = (dlong *)args(a5); deref(p4);
  nr = get_smallint(p1);

  if (nr < 0) 
    {
      zero = 1;
      nr = -nr;
    }
  else zero = 0;

  if (nr == 1) /* heap */
    {
      i = machp->end_heap - machp->begin_heap;
      if (i > MAXSMALLINT)
        build_bigint_from_out_of_range_smallint(p2,i,hreg)
      else build_smallint(p2,i);

      i = hreg - machp->begin_heap;
      if (i > MAXSMALLINT)
	build_bigint_from_out_of_range_smallint(p3,i,hreg)
      else build_smallint(p3,i);

      for (q = (machp->end_heap-1); (*q == 0); q--) ;
      i = q - machp->begin_heap + 1;
      if (i > MAXSMALLINT)
        build_bigint_from_out_of_range_smallint(p4,i,hreg)
      else build_smallint(p4,i);

      if (zero)
	{
	  dlong *p, *q;
	  p = machp->end_heap-1;
	  q = hreg+1;
	  while (p > q) *q++ = 0;
	}

      return(1);
    }

  if (nr == 2) /* ls */
    {
      i = begin_ls - machp->end_ls;
      build_smallint(p2,i);
      i = begin_ls - tos;
      build_smallint(p3,i);
      for (q = machp->end_ls; (*q == 0); q++) ;
      i = begin_ls - q;
      build_smallint(p4,i);

      if (zero)
	{
	  dlong *p, *q;
	  p = machp->end_ls;
	  q = tos-1;
	  while (p < q) *q-- = 0;
	}

      return(1);
    }

  if (nr == 3) /* trail */
    {
      i = machp->end_trail - machp->begin_trail;
      build_smallint(p2,i);
      i = loctrreg - machp->begin_trail;
      build_smallint(p3,i);
      for (q = (machp->end_trail-1); ((q >= machp->begin_trail) && (*q == 0)); q--) ;
      i = q - machp->begin_trail + 1;
      build_smallint(p4,i);
      return(1);
    }

  if (nr == 4) /* cp */
    {
      i = machp->begin_cp - machp->end_cp;
      build_smallint(p2,i);
      i = machp->begin_cp - locbreg;
      build_smallint(p3,i);
      for (q = machp->end_cp; (*q == 0); q++) ;
      i = machp->begin_cp - q;
      build_smallint(p4,i);
      return(1);
    }

  if (nr == 5) /* mmheap */
    { 
      i = machp->nr_heapgc;
      build_smallint(p2,i);
      i = machp->total_collected2;
      if (i == 0)
	{
	  i = machp->total_collected1;
	  build_smallint(p3,i);
	}
      else
	{
	  dlong *tmph;
	  *p3 = make_list(hreg);
	  tmph = hreg + 2;
	  build_smallint(hreg,i);
	  i = machp->total_collected1;
	  hreg++;
	  build_smallint(hreg,i);
	  hreg = tmph;
	}
      i = machp->nr_heapexp;
      build_smallint(p4,i);
      i = machp->time_mm;
      build_smallint(p5,i);
      return(1);
    }

  if (nr == 6) /* lsexp */
    {
      i = machp->nr_lsexp;
      build_smallint(p2,i);
      build_smallint(p3,0);
      return(1);
    }

  if (nr == 7) /* trexp */
    {
      i = machp->nr_trexp;
      build_smallint(p2,i);
      build_smallint(p3,0);
      return(1);
    }

  if (nr == 8) /* cpexp */
    {
      i = machp->nr_cpexp;
      build_smallint(p2,i);
      build_smallint(p3,0);
      return(1);
    }

  if (nr == 9) /* symbols_code */
    {
      i = number_of_symbols();
      build_smallint(p2,i);
      i = code_size();
      build_smallint(p3,i);
      return(1);
    }

  if (nr == 10) /* clean everything up to current */
    {
      /* heap */
      for (q = (machp->end_heap-1); (q >= hreg); q--) *q = 0;

      /* ls */
      {
	dlong *p;
	if (ereg > (dlong *)(((CP_P)locbreg)->tops))
	  p = (dlong *)(((CP_P)locbreg)->tops);
	else
	  p = ereg;
	for (q = machp->end_ls; (q < p); q++) *q = 0;
      }
      
      /* trail */
      for (q = (machp->end_trail-1); (q > trreg); q--) *q = 0;

      /* cp */
      for (q = machp->end_cp; (q < breg); q++) *q = 0;

      return(1);
    }

  if (nr == 11) /* print the stacks */
    {
      print_heap(0,machp);
      return(1);
    }

  if (nr == 12) /* detect struct duplicates in heap */
    {
      representation_sharing(machp);
      return(1);
    }

  if (nr == 13)
    {
      i = machp->total_repshar_time;
      build_smallint(p2,i);
      return(1);
    }

  if (nr == 14) /* shargain */
    { 
      i = machp->total_sharedgain1;
      build_smallint(p2,i);
      i = machp->total_sharedgain2;
      build_smallint(p3,i);
      return(1);
    }

  if (nr == 15) /* minorgc */
    { 
      i = machp->nr_minorheapgc;
      build_smallint(p2,i);
      return(1);
    }

  return(0);

} /* builtin_internalstat */



/*********************************************************************/
/* some info on the var-trail used during copying                    */
/*                                                                   */
/*                                                                   */
/* begin points to current block                                     */
/* top to current position to be filled in                           */
/* limit to last position to be filled in                            */
/* at begin, we put the previous bblock begin                        */
/* first block as 0 there for stopping                               */
/*                                                                   */
/*                                                                   */
/*********************************************************************/

#define CP_VAR_TRAIL_LEN 21 // must be odd

#define init_cp_var_trail() init_cp_var_trail_horse(cp_var_trail_first_block, &cp_var_trail_current_begin, &cp_var_trail_current_top, &cp_var_trail_current_limit)
static INLINE void init_cp_var_trail_horse(dlong * cp_var_trail_first_block[], dlong *** cp_var_trail_current_beginp,
					 dlong *** cp_var_trail_current_topp, dlong *** cp_var_trail_current_limitp)
{
  *cp_var_trail_current_beginp = cp_var_trail_first_block;
  *cp_var_trail_current_topp = *cp_var_trail_current_beginp + 1;
  *(*cp_var_trail_current_beginp) = 0;
  *cp_var_trail_current_limitp = *cp_var_trail_current_beginp + CP_VAR_TRAIL_LEN - 1;

} /* init_cp_var_trail_horse */

static INLINE void get_new_cp_var_trail_block(dlong *** cp_var_trail_current_beginp,
					    dlong *** cp_var_trail_current_topp, dlong *** cp_var_trail_current_limitp)
{
  dlong **tmp;

  tmp = (dlong **)malloc(CP_VAR_TRAIL_LEN*sizeof(dlong *));
  if (! tmp)
    fatal_message("not enough space to malloc copy trail");

  *tmp = (dlong *)(*cp_var_trail_current_beginp);
  *cp_var_trail_current_beginp = tmp;
  *cp_var_trail_current_topp = tmp + 1;
  *cp_var_trail_current_limitp = tmp + CP_VAR_TRAIL_LEN - 1;
} /* get_new_cp_var_trail_block() */


#define cp_var_trail(t) cp_var_trail_horse(t,cp_var_trail_current_beginp,cp_var_trail_current_topp,cp_var_trail_current_limitp)
static INLINE void cp_var_trail_horse(dlong *t, dlong *** cp_var_trail_current_beginp,
				    dlong *** cp_var_trail_current_topp, dlong *** cp_var_trail_current_limitp)
{
  if (*cp_var_trail_current_topp > *cp_var_trail_current_limitp)
    get_new_cp_var_trail_block(cp_var_trail_current_beginp,cp_var_trail_current_topp,cp_var_trail_current_limitp);

  **cp_var_trail_current_topp = (dlong *)*t;
  (*cp_var_trail_current_topp)++ ;
  **cp_var_trail_current_topp = (dlong *)t;
  (*cp_var_trail_current_topp)++ ;
} /* cp_var_trail_horse */


#define copy_untrail() copy_untrail_horse(cp_var_trail_first_block,cp_var_trail_current_begin,cp_var_trail_current_top,cp_var_trail_current_limit)
static INLINE void copy_untrail_horse(dlong * cp_var_trail_first_block[], dlong ** cp_var_trail_current_begin,
				      dlong ** cp_var_trail_current_top, dlong ** cp_var_trail_current_limit)
{
  dlong **tmp_top, **tmp_begin, *t;

  tmp_top = cp_var_trail_current_top;
  tmp_begin = cp_var_trail_current_begin;
  while (tmp_begin)
    {
      while (--tmp_top > tmp_begin)
	{
	  t = *tmp_top--;
	  *t = (dlong)(*tmp_top);
	}
      tmp_top = tmp_begin;
      tmp_begin = (dlong **)*tmp_begin;
      if (tmp_top != cp_var_trail_first_block)
	free(tmp_top);
      tmp_top = tmp_begin + CP_VAR_TRAIL_LEN;
    }
} /* copy_untrail_horse */

#define points_in_from_space(term,fromheap,lowlimit,uplimit) \
    (((fromheap == FROM_HEAP) && (lowlimit <= term) && (term <= uplimit)) || \
     ((fromheap != FROM_HEAP) && (!((lowlimit <= term) && (term <= uplimit)))))

/* quick and dirty cyclics */
/* should have a lower MAX_SHARING at first and extend on need */

#define copyterm_cyclics_init() local_trail = machp->copy_local_trail

#define copyterm_cyclics_destroy()                                      \
  {  if (MAX_SHARING != MAX_SHAR_INIT)                                  \
      { free(local_trail);                                              \
	machp->copy_local_trail = malloc(MAX_SHAR_INIT*sizeof(dlong*)); \
      }                                                                 \
  }

#define push_cyclic(t) push_cyclic_horse(t, MAX_SHARINGp, local_trailp, top_local_trailp)

static INLINE void push_cyclic_horse(dlong* t, dlong *MAX_SHARINGp,
				     dlong ***local_trailp, dlong *top_local_trailp)
{
  if (*top_local_trailp >= *MAX_SHARINGp)
  {
    dlong NEW_MAX_SHARING = *MAX_SHARINGp;
    do
    {
      NEW_MAX_SHARING = NEW_MAX_SHARING*2;
    }
    while (*top_local_trailp >= NEW_MAX_SHARING);
    *local_trailp = realloc(*local_trailp, sizeof(dlong*)*NEW_MAX_SHARING);
    if (*local_trailp == NULL)
      fatal_message("sharing overflow during findall and realloc failed");
    else
      *MAX_SHARINGp = NEW_MAX_SHARING;
  }
  (*local_trailp)[*top_local_trailp] = t;
  (*top_local_trailp)++;
} /* push_cyclic_horse */

#define finish_cyclic() \
while (top_local_trail--) \
{ \
  dlong *t = local_trail[top_local_trail]; \
  if (is_attributed(*t)) *t = make_attvar_p(t); \
  else if (is_list(*t)) *t = (dlong)local_trail[--top_local_trail]; \
  else *t = *(dlong *)(*t); \
}

#define copy_to_heap_overflow() if (*top > limit) return(1)


#define MAXC 2000
#define copy_pop() \
  if (ctop) { term = copy_stack[--ctop]; whereto = copy_stack[--ctop]; goto tail_recursion; } return(0); 
#define copy_push(term,whereto,top,cpatv)			 \
  if (ctop == MAXC) copy_term_rec(term,whereto,top, machp,cpatv,fromheap,lowlimit,uplimit,limit,cp_var_trail_current_beginp,cp_var_trail_current_topp,cp_var_trail_current_limitp,MAX_SHARINGp,local_trailp,top_local_trailp); \
else { copy_stack[ctop++] = whereto; copy_stack[ctop++] = term; }

static int copy_term_rec(dlong *term, dlong *whereto, dlong **top,
			 struct machine* machp, dlong copy_attvars,
			 dlong fromheap, dlong *lowlimit, dlong *uplimit, dlong *limit,
			 dlong *** cp_var_trail_current_beginp,
			 dlong *** cp_var_trail_current_topp, dlong *** cp_var_trail_current_limitp,
			 dlong *MAX_SHARINGp, dlong ***local_trailp, dlong *top_local_trailp)
{
  dlong *copy_stack[MAXC];
  dlong ctop = 0;

 tail_recursion:

  deref2(term,goto is_var);

  if (has_atom_tag(term))
    {
      *whereto = (dlong)term;
      copy_pop();
    }

  if (is_struct(term))
      {
	dlong *h = *top;
	dlong arity;
	term = get_struct_pointer(term);
        if ((chb) && (term < chb))
          {
            *whereto = (dlong)make_struct_p(term);
            copy_pop();
          }
 	if (is_ref(*term))
 	  {
 	    /* it is copied already */
 	    *whereto = make_struct_p(*term);
 	    copy_pop();
 	  }
	*whereto = make_struct_p(h);
	*h = *term; /* the functor/name */
 	*term = (dlong)h; /* for preserving sharing */
 	push_cyclic(term);
	arity = get_funct_arity(*h);
	whereto = h;
	*top += arity + 1;
	copy_to_heap_overflow();
	while (--arity)
	  {
	    term++; whereto++;
	    copy_push((dlong *)*term, whereto, top, copy_attvars);
	  }
	term = (dlong *)*(term+1) ; whereto++;
	goto tail_recursion;
      }

  if (is_list(term))
      {
	term = get_list_pointer(term);
	if ((chb) && (term < chb))
	  {
	    *whereto = (dlong)make_list(term);
	    copy_pop();
	  }
	if (points_in_from_space(term,fromheap,lowlimit,uplimit))
	  { /* this list pointer can point to an already copied list or not */
	    dlong *firstel;
	    dlong *secondel;
	    dlong *h;
	    firstel = (dlong *)*term;
	    secondel = (dlong *)*(term+1);
 	    if (is_list(firstel))
 	      {
 		dlong *t;
 		t = get_list_pointer(firstel);
 		if (! points_in_from_space(t,fromheap,lowlimit,uplimit))
 		  { /* we have found a previously copied list pair */
 		    *whereto = (dlong)firstel;
 		    copy_pop();
 		  }
 	      }
	    h = *top;
	    *whereto = make_list(h);
	    *top += 2;
	    copy_to_heap_overflow();
	    whereto = h;
 	    *term = make_list(whereto);
 	    *(term+1) = make_list(whereto+1);

 	    push_cyclic(firstel); push_cyclic(term);
 	    push_cyclic(secondel); push_cyclic(term+1);

	    copy_push(secondel,whereto+1,top, copy_attvars);

	    term = firstel;
	    goto tail_recursion;
	  }
 	else
 	  { /* this is an already copied heap entry
 	       it was copied to *term - so, just create a reference to it
 	    */
 	    *whereto = (dlong)term;
 	    copy_pop();
 	  }
      }

  if (is_real(term))
    {
      dlong *flp, *h;
      dlong i;
      h = *top;
      *top += SIZE_OF_FLOAT+1;
      copy_to_heap_overflow();

      flp = get_number_pointer(term) + 1;
      
      *whereto = make_number_pointer(h);
      *(h++) = REAL_HEADER;
      for (i = SIZE_OF_FLOAT; i--; )
	{ *(h++) = *flp++; }
      copy_pop();
    }

  if (is_attributed(term))
      {
	if (copy_attvars)
	  {
	    term = get_attvar_pointer(term);
	    if (points_in_from_space(term,fromheap,lowlimit,uplimit))
	      {
		dlong *h = *top;
		*top += 2;
		copy_to_heap_overflow();
		*whereto = (dlong)h; /* we must push two consecutive fields
					this cannot be guaranteed safe at whereto */
		*term = *h = make_attvar_p(h);
		push_cyclic(term);
		copy_push((dlong *)(term+1),h+1,top, copy_attvars);
	      }
	    else
	      *whereto = (dlong)term;
	    copy_pop();
	  }
	else
	  {
	    term = get_attvar_pointer(term);
            if (points_in_from_space(term,fromheap,lowlimit,uplimit))
              {
		dlong *h = whereto;
                *whereto = (dlong)h;
                *term = make_attvar_p(h);
		push_cyclic(term);
	      }
	    else
	      *whereto = (dlong)term;
	    copy_pop();
	  }
      }

  if (is_string(term))
    {
      dlong *ip, *h, len;

      h = *top;
      ip = get_string_pointer(term);
      len = add_str_len(get_string_length(term)) + 1;
      *top += len;
      copy_to_heap_overflow();

      *whereto = make_string_pointer(h);
      while (len--)
	*(h++) = *(ip++);
      copy_pop();
    }

  if (is_bigint(term))
    {
      dlong siz, onheap, pos;
      dlong *bigintp, *h;

      bigintp = get_number_pointer(term) + 1;
      onheap = bigint_onheap(*bigintp);
      siz = bigint_size(*bigintp);
      pos = bigint_positive(*bigintp);
      h = *top;
      *top += 2 + siz;
      copy_to_heap_overflow();
      *whereto = make_number_pointer(h);
      *(h++) = BIGINT_HEADER;
      *(h++) = bigint_info_onheap(pos,siz);
      bigintp += 1;
      if (!onheap)
	bigintp = (machp->excess_bigints)[(*bigintp)];
      while (siz--)
	*h++ = *bigintp++;
      copy_pop();
    }

is_var:

  if ((chb) && (term < chb))
    *whereto = (dlong)term;
  else
    if (points_in_from_space(term,fromheap,lowlimit,uplimit))
      {
	cp_var_trail(term);
	*whereto = *term = (dlong)whereto;
      }
    else *whereto = (dlong)term;
  copy_pop();

} /* copy_term_rec */


static int copy_term_rec_list(dlong *term, dlong *whereto, dlong **top, 
			      struct machine* machp, dlong copy_attvars,
			      dlong fromheap, dlong *lowlimit, dlong *uplimit, dlong *limit)
{
  dlong * cp_var_trail_first_block[CP_VAR_TRAIL_LEN];
  dlong ** cp_var_trail_current_begin = 0;
  dlong ** cp_var_trail_current_top = 0;
  dlong ** cp_var_trail_current_limit = 0;

 tail_recursion:

  deref2(term,goto is_var);

  if (has_atom_tag(term))
    { /* does not occur, but it is better to deal with it */
      *whereto = (dlong)term;
      return;
    }

  if (is_list(term))
      {
	dlong *firstel;
	dlong *secondel;
	dlong *h;
	dlong MAX_SHARING = MAX_SHAR_INIT;
	dlong **local_trail = NULL;
	dlong top_local_trail = 0;

	term = get_list_pointer(term);
	firstel = (dlong *)*term;
	secondel = (dlong *)*(term+1);
	h = *top;
	*whereto = make_list(h);
	*top += 2;
	copy_to_heap_overflow();
	whereto = h;

	if (is_smallint(firstel))
	  {
	    *whereto = (dlong)firstel;
	  }
	else
	  {
	    init_cp_var_trail();
	    copyterm_cyclics_init();
	    if (copy_term_rec(firstel,whereto,top, machp, copy_attvars, fromheap, lowlimit,uplimit,limit,&cp_var_trail_current_begin,&cp_var_trail_current_top,&cp_var_trail_current_limit,&MAX_SHARING,&local_trail,&top_local_trail)) return(1);
	    copyterm_cyclics_destroy();
	    copy_untrail() ;
	  }
	term = secondel;
	whereto++;

	goto tail_recursion;
      }

 is_var: /* this is the end of the findall list */

  if (points_in_from_space(term,fromheap,lowlimit,uplimit))
    *whereto = *term = (dlong)whereto;
  else *whereto = (dlong)term;
  return(0);

} /* copy_term_rec_list */




/*-------------------------------------------------------------------------*/
static dlong * gen_builtin_copy_term(dlong *t, struct machine *machp, dlong copy_attvars)
{
  dlong *p;
  dlong *h = hreg+1;
  dlong fromheap;
  dlong *lowlimit, *uplimit, *limit;
  dlong * cp_var_trail_first_block[CP_VAR_TRAIL_LEN];
  dlong ** cp_var_trail_current_begin = 0;
  dlong ** cp_var_trail_current_top = 0;
  dlong ** cp_var_trail_current_limit = 0;
  dlong MAX_SHARING = MAX_SHAR_INIT;
  dlong **local_trail = NULL;
  dlong top_local_trail = 0;

  init_cp_var_trail();
  copyterm_cyclics_init();
  fromheap = FROM_HEAP; lowlimit = start_heap; uplimit = h-2; limit = machp->limit_heap+100;

  if (copy_term_rec(t, h-1, &h, machp, copy_attvars, fromheap, lowlimit,uplimit,limit,&cp_var_trail_current_begin,&cp_var_trail_current_top,&cp_var_trail_current_limit,&MAX_SHARING,&local_trail,&top_local_trail))
  {
    /* deal with overflow while copying */
    finish_cyclic();
    copyterm_cyclics_destroy();
    copy_untrail();
    return(0);
  }

  p = hreg;
  hreg = h;
  finish_cyclic();
  copyterm_cyclics_destroy();
  copy_untrail();
  return(p);

} /* gen_builtin_copy_term */

/*-------------------------------------------------------------------------*/
dlong * builtin_copy_term(dlong *t, struct machine *machp)
{
  return(gen_builtin_copy_term(t,machp,1));
} /* builtin_copy_term */

/*-------------------------------------------------------------------------*/
dlong * builtin_copy_term_nat(dlong *t, struct machine *machp)
{
  dlong *res;
  res = gen_builtin_copy_term(t,machp,0);
  return(res);
} /* builtin_copy_term_nat */

/*
*    Findall copies its templates to a findall-heap.  This heap is allocated in
*    chunks of DEFAULT_CHUNK_SIZE (dlong *)entries.  Since more than one findall
*    can be active at the same time, each findall gets a number, determined by
*    the global variables nextfree; this happens in findall_init. The maximal
*    number of active findalls is MAX_FINDALLS.
* 
*    A solution list of findall is represented by its size and a pointer to the
*    beginning of the solution list and a pointer to the tail of this solution
*    list.  The size is important for copying back to the heap, to ensure that
*    there is enough space, before we start copying.  The tail is a free
*    variable.
* 
*    One solution list or template can be in more than one chunk. Chunks are
*    linked together by the first field in the chunk; this field is only needed
*    for the deallocation of the chunks, not for the copying itself.
* 
*    Everything is allocated dynamically, and freed asap.
* 
*    findall_clean should be called at the start of every toplevel. 
*    If more space is needed in a chunk than DEFAULT_CHUNK_SIZE, it is alloced.
*/

#define DEFAULT_CHUNK_SIZE 4000 /* anything > 3 should do now  - except for VEEERY BIG ints :-( */

/*
*    one invocation of findall is associated with one entry in the
*    findall_solutions array: we then call this entry active; the type of the
*    entry is findall_solution_list
*/

// See toinclude for findall_solution_list typedef

/* malloc a new chunk and link it in in the current findall */
static dlong get_more_chunk(dlong needed, findall_solution_list *current_findall)
{
  dlong * newchunk, to_alloc ;

  if (needed > DEFAULT_CHUNK_SIZE)
    to_alloc = needed + 2;
  else
    to_alloc = DEFAULT_CHUNK_SIZE;
  if (!(newchunk = (dlong *)malloc(to_alloc * sizeof(dlong *))))
    fatal_message("get_more_chunk failed");

  *newchunk = 0;
  *(current_findall->current_chunk) = (dlong)newchunk;
  current_findall->current_chunk = newchunk;
  current_findall->limit_of_chunk = newchunk + to_alloc;
  current_findall->top_of_chunk = newchunk + 1;

  return(1);

} /*get_more_chunk*/


/* $$findall_init/2

   to be called with 1 free variable
   the variable is bound to an index in the findall table
   and a heap barrier (which can be 0)
*/

static void realloc_findall(struct machine *machp)
{
  dlong i ;
  findall_solution_list *p ;
  dlong M = machp->MAX_FINDALLS;

  if (machp->findall_solutions == 0)
    {
      machp->findall_solutions = (findall_solution_list *) malloc(M*sizeof(findall_solution_list)) ;
      if (machp->findall_solutions == 0)
	fatal_message("init of findall failed") ;
      return;
    }

  machp->MAX_FINDALLS = M = 2*M;
  p = (findall_solution_list *) realloc(machp->findall_solutions,M*sizeof(findall_solution_list));
  if (! p)
    fatal_message("realloc of findall failed");
  machp->findall_solutions = p;

} /* realloc_findall */

dlong findall_init_c(dlong heapbarrier, struct machine *machp)
{
  dlong * w ;
  findall_solution_list *p ;
  dlong thisfree;
  findall_solution_list *findall_sols = machp->findall_solutions;


  /* fprintf(stderr,"findall_sols = %d  machp->nextfree = %d\n",findall_sols,machp->nextfree); */
  if ((findall_sols == 0) || (machp->nextfree == machp->MAX_FINDALLS))
    {
      realloc_findall(machp);
      findall_sols = machp->findall_solutions;
    }

  p = findall_sols + machp->nextfree ;
  thisfree = (machp->nextfree)++;
  if (!(w = (dlong *)malloc(DEFAULT_CHUNK_SIZE * sizeof(dlong *))))
	fatal_message("no temp space for findall");

  *w = 0 ;
  p->first_chunk = p->current_chunk = w ;
  w++ ; *w = (dlong)w ; p->tail = w ; /* create an undef as init of tail */
  w++ ; p->top_of_chunk = w ;
  p->size = 1 ;
  p->limit_of_chunk = w + DEFAULT_CHUNK_SIZE - 1;
  p->barrier = heapbarrier;
  return(thisfree) ;

} /* findall_init_c */


/* findall_free is called to desactivate an entry in the solution_list
   at the end of findall_get_solutions, and from findall_clean.
   Pass a non-NULL list to not use the solution_list, but use the given lsit.
*/

void findall_free(int i, findall_solution_list* list, struct machine *machp)
{ dlong * to_free, *p ;
  int set_nextfree = 0;

  if (machp == 0) return; // the machine has been deallocated already

  if (list == NULL)
  {
    set_nextfree = 1;
    list = (machp->findall_solutions + i);
  }

  p = list->first_chunk ;
  while (p != NULL)
	{ 
	  to_free = p ;
	  p = (dlong *)(*p) ;
	  free(to_free) ; 
	}
  list->tail = 0 ;
  list->size = machp->nextfree ;
  list->barrier = 0 ;

  if (set_nextfree)
    machp->nextfree = i ;

} /*findall_free*/

/* findall_clean should be called after interrupts or jumps out of the
   interpreter - or just before jumping back into it
*/

void findall_clean(struct machine *machp)
{ findall_solution_list *p ;
  dlong i ;
  dlong M = machp->MAX_FINDALLS;

	p = machp->findall_solutions ;
	if (! p) return ;
	for (i = 0 ; i < M ; i++)
		{ if (p->tail != 0)
		    findall_free(i, NULL, machp) ;
		  (machp->findall_solutions + i)->size = i+1 ;
		}
	(machp->findall_solutions + i - 1)->size = -1 ;
	(machp->findall_solutions + i - 1)->barrier = 0 ;
	machp->nextfree = 0 ;

} /* findall_clean */

/* findall_copy_to_heap does not need overflow checking - heap space is
   ensured; variables in the findall solution list can be altered without
   problem, because they are not needed afterwards anymore, so no trailing
*/

/* findall_copy_template_to_chunk
   must do: overflow checking
   trailing of variables
   returns size of copied term
   if it "fails", returns a negative number
*/

#define points_in_heap(f,a,b)  ((a <= f) && (f <= b))

static INLINE void findall_copy_overflow(dlong **h, dlong needed, findall_solution_list *current_findall)
{
  if (*h > (current_findall->limit_of_chunk - needed))
    {
      if (! get_more_chunk(needed, current_findall))
	fatal_message("not enough sapce during findall copy");
      *h = current_findall->top_of_chunk ;
    }
} /* findall_copy_overflow */

static dlong findall_copy_template_to_chunk(dlong * from, dlong * to, dlong * *h,
					    dlong * heaplowlimit, dlong *heapuplimit,
					    struct machine* machp, findall_solution_list *current_findall,
					    dlong *** cp_var_trail_current_beginp,
					    dlong *** cp_var_trail_current_topp, dlong *** cp_var_trail_current_limitp,
					    dlong *MAX_SHARINGp,
					    dlong ***local_trailp, dlong *top_local_trailp)
{
  dlong size = 0 ;

  copy_again : /* for tail recursion optimisation */

    deref(from);
    switch (tag(from))
      {
      case SIMPLE:
	*to = (dlong)from;
	return(size);
    
      case REF0:
      case REF1:
	if ((chb) && (from < chb))
	  *to = (dlong)from;
	else
	if (points_in_heap(from,heaplowlimit,heapuplimit))
	  {
	    cp_var_trail(from);
	    *to = *from = (dlong)to;
	  }
	else
	  *to = (dlong)from;
        return(size) ;

      case LIST :
	{
	  dlong *h1;
	  dlong s;
          findall_copy_overflow(h,3,current_findall);
	  h1 = *h;
	  from = get_list_pointer(from);
	  if ((chb) && (from < chb))
	    {
	      *to = make_list(from);
	      return(size);
	    }
	  *to = make_list(h1);
	  to = h1;
	  *h += 2;
	  s = findall_copy_template_to_chunk(from, to, h, heaplowlimit, heapuplimit, machp, current_findall,cp_var_trail_current_beginp,cp_var_trail_current_topp,cp_var_trail_current_limitp,MAX_SHARINGp,local_trailp,top_local_trailp);
	  if (s < 0)
	    return(-1);
	  from++; to++;
	  size += s + 2 ;
	  goto copy_again;

	}
	
      case NUMB:
	{
	  dlong i;
	  if (is_real(from))
	    {
	      from = get_number_pointer(from)+1;
	      findall_copy_overflow(h,(SIZE_OF_FLOAT+1),current_findall);
	      *to = make_number_pointer(*h);
	      **h = REAL_HEADER; (*h)++;
	      for (i = SIZE_OF_FLOAT; i--; )
		{ **h = *from++; (*h)++; }
	      return(size+(SIZE_OF_FLOAT+1));
	    }

	  if (is_bigint(from))
	    {
	      dlong retsiz, siz, onheap, pos;
	      dlong *bigintp;

	      bigintp = get_number_pointer(from) + 1;
	      onheap = bigint_onheap(*bigintp);
	      retsiz = siz = bigint_size(*bigintp);
	      pos = bigint_positive(*bigintp);
	      findall_copy_overflow(h,(siz + 3),current_findall);
	      *to = make_number_pointer(*h);
	      **h = BIGINT_HEADER; (*h)++;
	      **h = bigint_info_onheap(pos,siz); (*h)++;
	      bigintp++;
	      if (!onheap)
		bigintp = (machp->excess_bigints)[(*bigintp)];
	      while (siz--)
		{**h = *bigintp++; (*h)++; }
	      return(retsiz+2);
	    }
	}

      case STRING:
	{
	  dlong *w;
	  dlong len,l;
	  char *fromc, *toc;

	  l = len = get_string_length(from);
	  fromc = get_string_val(from);
	  findall_copy_overflow(h,heapcells_needed_for_string(len)+1,current_findall);
	  w = *h;
	  *to = make_string_pointer(w);
	  from = get_string_pointer(from);
	  *w = *from; w++;
	  toc = (char *)w;
	  while (l--)
	    *(toc++) = *(fromc++);
	  *h = w+heapcells_needed_for_string(len);
	  return(size+1+heapcells_needed_for_string(len));
	}

      case STRUCT :
	{
          dlong *h1;
          dlong s, arity;

	  from = get_struct_pointer(from);
          if ((chb) && (from < chb))
            {
              *to = make_struct_p(from);
              return(size);
            }
	  if (is_ref(*from))
	    {
	      /* it is copied already */
	      *to = make_struct_p(*from);
	      return(size);
	    }
	  arity = get_funct_arity(*from);
	  findall_copy_overflow(h,(arity+1),current_findall);
          h1 = *h;
          *to = make_struct_p(h1);
	  *h1 = *from; /* the functor */
	  *from = (dlong)h1; /* for preserving sharing */
	  push_cyclic(from);
          to = h1;
          *h += arity + 1;
          size += 1 + arity;
	  while (--arity)
	    {
	      s = findall_copy_template_to_chunk(++from, ++to, h, heaplowlimit, heapuplimit, machp, current_findall,cp_var_trail_current_beginp,cp_var_trail_current_topp,cp_var_trail_current_limitp,MAX_SHARINGp,local_trailp,top_local_trailp);
	      if (s < 0)
		return(-1);
	      size += s;
	    }
          from++; to++;
          goto copy_again;
	}

      case ATT:
	from = get_attvar_pointer(from);
	if (points_in_heap(from,heaplowlimit,heapuplimit))
	  {
	    findall_copy_overflow(h,SIZE_OF_ATT,current_findall);
	    cp_var_trail(from);
	    *to = (dlong)*h;
	    *from = **h = make_attvar_p(*h);
	    from++;
	    to = (*h) + 1;
	    *h += SIZE_OF_ATT;
	    size += SIZE_OF_ATT;
	    goto copy_again;
	  }
	*to = (dlong)from;
	return(size);

  } /* switch */
 
 return(-1) ; /* to keep compiler happy */
 
} /*findall_copy_template_to_chunk */


/*
  $$findall_add/3
  arg1 : any
  arg2 : findall index - where to add it
  arg3 : if not var, then return with fail immediately
  at arg2, the term arg1 is added to the solution list
  if findall_add/2 fails, the associated term remains unchanged
*/

dlong findall_add_to_list(dlong* p1, int dolist, struct machine *machp, findall_solution_list *current_findall);
dlong findall_add(dlong a1, dlong a2, struct machine *machp)
{
  dlong *p1, *p2;
  findall_solution_list *current_findall;
  
  p1 = (dlong *)args(a1); p2 = (dlong *)args(a2);
  deref(p1); deref(p2);

  current_findall = machp->findall_solutions + get_smallint(p2);
  if (current_findall->tail == 0)
    fatal_message("internal error 1 in findall") ;

  if (is_smallint(p1))
    {
      dlong *to, *h ;
      
      to = current_findall->top_of_chunk ;
      
      findall_copy_overflow(&to, 3,current_findall);
      
      h = to + 2 ;
      
      *to = (dlong)p1;
      
      current_findall->top_of_chunk = h ;
      {
	/* 2 because of ./2 of solution list */
	current_findall->size += 2 ;
	*(to+1) = (dlong)(to+1) ;
	/* link in new template now */
	*(dlong *)(*(current_findall->tail)) = make_list(to);
	current_findall->tail = to+1 ; /* fill in new tail */
      }
      
      return(1);
    }

  findall_add_to_list(p1, 1, machp, current_findall);
  return 1;
} /* findall_add */

/*
 * Uses a previously set current_findall.
 */
dlong findall_add_to_list(dlong* p1, int dolist, struct machine *machp, findall_solution_list *current_findall)
{
  dlong * to, *h ;
  int size ;
  dlong * cp_var_trail_first_block[CP_VAR_TRAIL_LEN];
  dlong ** cp_var_trail_current_begin = 0;
  dlong ** cp_var_trail_current_top = 0;
  dlong ** cp_var_trail_current_limit = 0;
  dlong MAX_SHARING = MAX_SHAR_INIT;
  dlong **local_trail = NULL;
  dlong top_local_trail = 0;

  to = current_findall->top_of_chunk ;
  {
    dlong i = current_findall->barrier;

    if (i)
      {
	dlong *p;
	p = get_cut_point(machp->begin_cp, i);
	chb = ((CP_P)p)->h;
      }
    else chb = 0;
  }
  findall_copy_overflow(&to, 3,current_findall);
  
  if (dolist)
    h = to + 2 ;
  else
    h = to + 1;
  
  init_cp_var_trail();
  copyterm_cyclics_init();

  size = findall_copy_template_to_chunk(p1,to,&h,start_heap,hreg, machp, current_findall,&cp_var_trail_current_begin,&cp_var_trail_current_top,&cp_var_trail_current_limit,&MAX_SHARING,&local_trail,&top_local_trail);
  finish_cyclic();
  copyterm_cyclics_destroy();
  copy_untrail() ;

  if (size < 0)
    {
      chb = 0;
      return(0);
    }

  current_findall->top_of_chunk = h ;

  if (dolist)
  {
    /* 2 because of ./2 of solution list */
    current_findall->size += size + 2 ;
    *(to+1) = (dlong)(to+1) ;
    /* link in new template now */
    *(dlong *)(*(current_findall->tail)) = make_list(to);
    current_findall->tail = to+1 ; /* fill in new tail */
  }
  else
  {
    current_findall->size += size;
  }
  chb = 0;
  return(1);

} /* findall_add_to_list */

/* $$findall_get_solutions/3
   arg1 : out : solution list of findall
   arg2 : out : tail of the list
   arg3 : in : integer = findall index

   the list at arg3 is copied to the heap and then this copy is unified with
   arg1-arg2 
*/

dlong get_findall_size(dlong cur_f, struct machine *machp)
{
  findall_solution_list *p ;
  p = machp->findall_solutions + cur_f ;
  return(p->size);
} /* get_findall_size */

void findall_get_solution_single(dlong* p1, short fromlist, findall_solution_list* list, struct machine *machp, dlong copy_attvars);
dlong findall_get_solutions(dlong a1, dlong a2, dlong cur_f, struct machine *machp, dlong copy_attvars)
{
  dlong *p1, *p2;
  findall_solution_list *p ;
  
  p = machp->findall_solutions + cur_f ;
  
  /* space has been assured already before */

  p1 = (dlong *)args(a1); deref(p1);
  findall_get_solution_single(p1, 1, p, machp, copy_attvars);

  p2 = (dlong *)args(a2); deref(p2);
  *p2 = *(p->tail); // this should always be a free var

  findall_free(cur_f, NULL, machp) ;
  return(1);
} /* findall_get_solutions */

void findall_get_solution_single(dlong* p1, short fromlist, findall_solution_list* list, struct machine *machp, dlong copy_attvars)
{
  dlong *from, *h ;
  dlong fromheap;
  dlong *lowlimit, *uplimit, *limit;

  /* space has been assured already before */

  h = hreg;
  //  init_cp_var_trail();
  from = (dlong *)*(list->first_chunk+1) ; /* deref not necessary */
  {
    dlong i = list->barrier;

    if (i)
      {
        dlong *q;
        q = get_cut_point(machp->begin_cp, i);
        chb = ((CP_P)q)->h;
      }
    else chb = 0;
  }

  fromheap = FROM_OUTSIDE; lowlimit = hreg; limit = uplimit = machp->limit_heap;
  if (fromlist)
    {
      if (copy_term_rec_list(from, p1, &h, machp,1, fromheap, lowlimit,uplimit,limit))
	goto give_fatal_message;
    }
  else
    {
      dlong * cp_var_trail_first_block[CP_VAR_TRAIL_LEN];
      dlong ** cp_var_trail_current_begin = 0;
      dlong ** cp_var_trail_current_top = 0;
      dlong ** cp_var_trail_current_limit = 0;
      dlong MAX_SHARING = MAX_SHAR_INIT;
      dlong **local_trail = NULL;
      dlong top_local_trail = 0;
      
      init_cp_var_trail();
      copyterm_cyclics_init();
      if (copy_term_rec(from, p1, &h, machp, copy_attvars, fromheap, lowlimit,uplimit,limit,&cp_var_trail_current_begin,&cp_var_trail_current_top,&cp_var_trail_current_limit,&MAX_SHARING,&local_trail,&top_local_trail)) goto give_fatal_message;
      copyterm_cyclics_destroy();
      copy_untrail() ;
    }

  hreg = h;
  chb = 0;
  return;

 give_fatal_message:
  fatal_message("ran out of heap space when copying findall solution. This should not happen...!");


} /* findall_get_solution_single */


/*********************************************************************/
/* sort/2 - implemented in C by do_sort                              */
/*  the length of the list has been computed earlier for the sake of */
/*  heap overflow testing - now no more checking is needed           */
/*********************************************************************/

/*
  merge(dest,a,la,b,lb, machp) merges in dest the la elements of a and lb
  elements of b; returns the number of different elements in dest
*/

static dlong local_compare_terms(dlong *a, dlong *b, struct machine* machp)
{
  deref(a); deref(b);

  if (is_smallint(a) && is_smallint(b))
    {
      dlong ai, bi;
      ai = get_smallint(a);
      bi = get_smallint(b);
      if (ai < bi) return(-1);
      if (ai == bi) return(0);
      return(1);
    }

  if (is_atom(a) && is_atom(b))
    {
      dlong ia, ib, i;
      ia = get_atom_index(a);
      ib = get_atom_index(b);
      if ((ia < numb_symbs) && (ib < numb_symbs))
	{
	  i = strcmp(get_atom_name(a),get_atom_name(b));
	  if (i < 0) return(-1);
	  if (i) return(1);
	  return(0);
	}
      if (ia < numb_symbs) return(-1);
      if (ib < numb_symbs) return(1);
      if (ia < ib) return(-1);
      if (ia == ib) return(0);
      return(1);
    }

  return(compare_terms(a,b, machp));
} /* local_compare_terms */

static dlong key_local_compare_terms(dlong *a, dlong *b, struct machine* machp)
{
  deref(a); deref(b);
  if (!is_struct(a) || !is_struct(b)) return(0);

  a = get_struct_pointer(a) + 1;
  b = get_struct_pointer(b) + 1;

  if (is_smallint(a) && is_smallint(b))
    {
      dlong ai, bi;
      ai = get_smallint(a);
      bi = get_smallint(b);
      if (ai < bi) return(-1);
      if (ai == bi) return(0);
      return(1);
    }

  if (is_atom(a) && is_atom(b))
    {
      dlong ia, ib, i;
      ia = get_atom_index(a);
      ib = get_atom_index(b);
      if ((ia < numb_symbs) && (ib < numb_symbs))
	{
	  i = strcmp(get_atom_name(a),get_atom_name(b));
	  if (i < 0) return(-1);
	  if (i) return(1);
	  return(0);
	}
      if (ia < numb_symbs) return(-1);
      if (ib < numb_symbs) return(1);
      if (ia < ib) return(-1);
      if (ia == ib) return(0);
      return(1);
    }

  return(compare_terms(a,b, machp));
} /* key_local_compare_terms */

static dlong key_merge(dlong *dest, dlong *a, dlong la, dlong *b, dlong lb, struct machine* machp)
{
  dlong *x = dest;
  dlong res, retres;

  retres = la + lb;

  while (la && lb)
    {
      res = key_local_compare_terms((dlong *)(*(a)), (dlong *)(*(b)), machp);
      if (res <= 0)
	{ *(dest) = *(a); a += 2; la--; }
      else
	if (res > 0)
	  { *(dest) = *(b); b += 2; lb--; }
      dest += 2;
    }

  if (la)
    {
      while (la--)
        {
          *(dest) = *(a); a += 2; dest += 2;
        }
      return(retres);
    }

  while (lb--)
    {
      *(dest) = *(b); b += 2; dest += 2;
    }

  return(retres);
} /* key_merge */

static dlong merge(dlong *dest, dlong *a, dlong la, dlong *b, dlong lb, struct machine* machp)
{
  dlong *x = dest;
  dlong res;

  while (la && lb)
    {
      res = local_compare_terms((dlong *)(*(a)), (dlong *)(*(b)), machp);
      if (res < 0)
	{ *(dest) = *(a); a += 2; la--; }
      else
	if (res > 0)
	  { *(dest) = *(b); b += 2; lb--; }
      else
	{ *(dest) = *(a); a += 2; b += 2; la--; lb--; }
      dest += 2;
    }

  if (la)
    {
      while (la--)
	{
	  *(dest) = *(a); a += 2; dest += 2;
	}
      return((dest-x)/2);
    }

  while (lb--)
    {
      *(dest) = *(b); b += 2; dest += 2;
    }

  return((dest-x)/2);
} /* merge */

/* ms1(a,b,la, machp) leaves the la sorted elements of a in a and returns the
   number of different elements
*/
static dlong ms2(dlong *, dlong *, dlong, struct machine* machp);

static dlong ms1(dlong *a, dlong *b,dlong la, struct machine* machp)
{ 
  if (la < 3)
    {
      /* la can be 1 or 2, not 0 */

      if (la == 1)
	return(1);
      la = local_compare_terms((dlong *)(*(a)),(dlong *)(*(a+2)), machp);
      if (la == 0)
	return(1);
      if (la > 0)
	{
	  dlong tmp ;
	  tmp = *(a);
	  *(a) = *(a+2);
	  *(a+2) = tmp;
	}
      return(2);
    }

  {
    dlong l1, l2;
    dlong *a1, *a2, *b1, *b2;

    l1 = la/2; l2 = la - l1;
    a1 = a; a2 = a + (l1<<1);
    b1 = b; b2 = b + (l1<<1);
    l1 = ms2(a1,b1,l1, machp);
    l2 = ms1(a2,b2,l2, machp);

    return(merge(a,b1,l1,a2,l2, machp));
  }
} /* ms1 */

/* ms2(a,b,la, machp) leaves the la sorted elements of a in b and returns the
   number of different elements
*/

static dlong ms2(dlong *a, dlong *b,dlong la, struct machine* machp)
{ 
  if (la < 3)
    {
      /* la can be 1 or 2, not 0 */

      if (la == 1)
	{
	  *(b) = *(a);
	  return(1);
	}
      la = local_compare_terms((dlong *)(*(a)),(dlong *)(*(a+2)), machp);
      if (la == 0)
	{
	  *(b) = *(a);
	  return(1);
	}
      if (la > 0)
	{
	  *(b+2) = *(a);
	  *(b) = *(a+2);
	}
      else
	{
	  *(b) = *(a);
          *(b+2) = *(a+2);
	}
      return(2);
    }

  {
    dlong l1, l2;
    dlong *a1, *a2, *b1, *b2;

    l1 = la/2; l2 = la - l1;
    a1 = a; a2 = a + (l1<<1);
    b1 = b; b2 = b + (l1<<1);
    l1 = ms1(a1,b1,l1, machp);
    l2 = ms2(a2,b2,l2, machp);

    return(merge(b,a1,l1,b2,l2, machp));
  }
} /* ms2 */

/* key_ms1(a,b,la, machp) leaves the la sorted elements of a in a and returns the
   number of different elements
*/
static dlong key_ms2(dlong *, dlong *, dlong, struct machine* machp);

static dlong key_ms1(dlong *a, dlong *b,dlong la, struct machine* machp)
{ 
  if (la < 3)
    {
      /* la can be 1 or 2, not 0 */

      if (la == 1)
	return(1);
      la = local_compare_terms((dlong *)(*(a)),(dlong *)(*(a+2)), machp);
      if (la == 0)
	return(1);
      if (la > 0)
	{
	  dlong tmp ;
	  tmp = *(a);
	  *(a) = *(a+2);
	  *(a+2) = tmp;
	}
      return(2);
    }

  {
    dlong l1, l2;
    dlong *a1, *a2, *b1, *b2;

    l1 = la/2; l2 = la - l1;
    a1 = a; a2 = a + (l1<<1);
    b1 = b; b2 = b + (l1<<1);
    l1 = key_ms2(a1,b1,l1, machp);
    l2 = key_ms1(a2,b2,l2, machp);

    return(key_merge(a,b1,l1,a2,l2, machp));
  }
} /* key_ms1 */

/* key_ms2(a,b,la, machp) leaves the la sorted elements of a in b and returns the
   number of different elements
*/

static dlong key_ms2(dlong *a, dlong *b,dlong la, struct machine* machp)
{ 
  if (la < 3)
    {
      /* la can be 1 or 2, not 0 */

      if (la == 1)
	{
	  *(b) = *(a);
	  return(1);
	}
      la = local_compare_terms((dlong *)(*(a)),(dlong *)(*(a+2)), machp);
      if (la == 0)
	{
	  *(b) = *(a);
	  return(1);
	}
      if (la > 0)
	{
	  *(b+2) = *(a);
	  *(b) = *(a+2);
	}
      else
	{
	  *(b) = *(a);
          *(b+2) = *(a+2);
	}
      return(2);
    }

  {
    dlong l1, l2;
    dlong *a1, *a2, *b1, *b2;

    l1 = la/2; l2 = la - l1;
    a1 = a; a2 = a + (l1<<1);
    b1 = b; b2 = b + (l1<<1);
    l1 = key_ms1(a1,b1,l1, machp);
    l2 = key_ms2(a2,b2,l2, machp);

    return(key_merge(b,a1,l1,b2,l2, machp));
  }
} /* key_ms2 */

dlong do_sort(dlong *from, dlong len, dlong *whereto, struct machine* machp)
{
  dlong *p = from;
  dlong l2 = len;
  dlong *q = whereto;

  deref(p);
  while (l2--)
    {
      dlong *r,*prev;
      r = p = get_list_pointer(p);
      deref3(prev,r,break);
      if (is_atom(r))
	r = prev;
      else
	underef(r);
      *q = (dlong)r;
      q += 2; p++;
      deref(p);
    }

  len = ms1(whereto, whereto+1, len, machp);

  /* fill in linking fields - duplicates have been removed allready */

  l2 = len;
  p = whereto;
  while (--l2)
    {
      p++;
      *p = make_list(p+1);
      p++;
    }
  p++;
  *p = nil_atom;
  return(len);
} /* do_sort */

dlong do_key_sort(dlong *from, dlong len, dlong *whereto, struct machine* machp)
{
  dlong *p = from;
  dlong l2 = len;
  dlong *q = whereto;

  deref(p);
  while (l2--)
    {
      dlong *r,*prev;
      r = p = get_list_pointer(p);
      deref3(prev,r,break);
      if (is_atom(r))
	r = prev;
      else
	underef(r);
      *q = (dlong)r;
      q += 2; p++;
      deref(p);
    }

  len = key_ms1(whereto, whereto+1, len, machp);

  l2 = len;
  p = whereto;
  while (--l2)
    {
      p++;
      *p = make_list(p+1);
      p++;
    }
  p++;
  *p = nil_atom;
  return(len);
} /* do_key_sort */

