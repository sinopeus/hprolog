run_hanoi(Threads, Disks) :-
	mt_move(Threads, Disks, left, middle, right).

run_hanoi_native(Threads, Disks) :-
	mt_move_native(Threads, Disks, left, middle, right).

mt_move(1, Disks, Left, Aux, Right) :- !,
	st_move(Disks, Left, Aux, Right).
mt_move(Threads, Disks, Left, Aux, Right) :-
	Threads > 1,
	Threads2 is Threads//2,
	Disks > 1,
	Disks2 is Disks - 1,
	threaded((
		mt_move(Threads2, Disks2, Left, Right, Aux),
		mt_move(Threads2, Disks2, Aux, Left, Right)
	)).

mt_move_native(1, Disks, Left, Aux, Right) :- !,
	st_move(Disks, Left, Aux, Right).
mt_move_native(Threads, Disks, Left, Aux, Right) :-
	Threads > 1,
	Threads2 is Threads//2,
	Disks > 1,
	Disks2 is Disks - 1,
	Pattern = [Left, Right, Aux],
	hub(Hub),
	spawn_link(Hub, Pattern, mt_move_native(Threads2, Disks2, Left, Right, Aux), T1),
	spawn_link(Hub, Pattern, mt_move_native(Threads2, Disks2, Aux, Left, Right), T2),
	receive(Hub, T1, the(Pattern)),
	receive(Hub, T2, the(Pattern)),
	stop(Hub).

st_move(1, _, _, _) :- !.
st_move(Disks, Left, Aux, Right) :-
	Disks > 1,
	Disks2 is Disks - 1,
	st_move(Disks2, Left, Right, Aux),
	st_move(Disks2, Aux, Left, Right).

benchmark(_, Threads, Disks, Moves) :-
	mt_move(Threads, Disks, left, middle, right, [], Moves).

mt_move(1, Disks, Left, Aux, Right, Acc, Moves) :- !,
	st_move(Disks, Left, Aux, Right, Acc, Moves).
mt_move(Threads, Disks, Left, Aux, Right, Acc, Moves) :-
	Threads > 1,
	Threads2 is Threads//2,
	Disks > 1,
	Disks2 is Disks - 1,
	threaded((
		mt_move(Threads2, Disks2, Left, Right, Aux, [Left-Right| Acc2], Moves),
		mt_move(Threads2, Disks2, Aux, Left, Right, Acc, Acc2)
	)).

st_move(1, Left, _, Right, Acc, [Left-Right| Acc]) :- !.
st_move(Disks, Left, Aux, Right, Acc, Moves) :-
	Disks > 1,
	Disks2 is Disks - 1,
	st_move(Disks2, Left, Right, Aux, [Left-Right| Acc2], Moves),
	st_move(Disks2, Aux, Left, Right, Acc, Acc2).

write_moves([]).
write_moves([Move| Moves]) :-
	write_move(Move), nl,
	write_moves(Moves).

write_move(Pole1-Pole2) :-
	write('Move a disk from '),
	writeq(Pole1),
	write(' to '),
	writeq(Pole2),
	write('.'),
	nl.
