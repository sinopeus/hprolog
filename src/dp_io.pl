% Wed Feb 12 12:49:44 CET 2003
%% Bart Demoen K.U.Leuven
%% 
%% This file contains the Prolog definitions of the IO builtins
%% not written in C, or should never be inlined

%% There are two sets of IO builtins: the first set works on the
%% current in or out stream and they have no stream argument; the second
%% set has a stream argument and works on that stream. The second set has
%% always _stream in its name. The internal type
%% of a stream is a small integer, and if you abuse that knowledge too
%% much, operations will fail with messages; sometimes what you expect
%% happens, but you should never get segmentation errors.
%% The first set is similar to the old DEC-10 predicates (but smaller).
%% The second set is a bit ad hoc, but there for making compilation of
%% HAL to hProlog more easy - it is work in progress.


:- sysmodule(sysh,
	  [
	   read/1,
	   write/1,
	   writeln/1,
	   writeq/1,
	   display/1,
	   space/0,
	   tab/1,
	   get/1,
	   skip/1,
	   nl/0,
	   seeing/1,
	   telling/1,
	   
	   nl/1,
	   write/2,
	   write_big/1,

	   read_stream/2,
	   open_stream/3,
	   close_stream/1,
	   put_stream/2,
	   write_stream/2,
	   write_stream/3,
	   all_open_streams/1

	  ]).


% only temporarily here ...
write_atomic(X) :-
	telling(_,CurrentOutStream),
	sysh:write_atomic(CurrentOutStream,X,off,  []).

% get/1 skip/1 get0/1

get(X) :-
	get0(Y),
	moreget(Y,X).
moreget(Y,X) :-
	(Y > 32 ->
	    X = Y
	;
	    (Y == -1 ->
		X = -1
	    ;
		get0(Z),
		moreget(Z,X)
	    )
	).

skip(X) :-
	get0(A),
	(A < 0 ->
	    true
	;
	    (A == X ->
		true
	    ;
		skip(X)
	    )
	).

read(X) :-
	sysh:hread(X,_).

% writeq/1

space :- put(0' ).

user_tab(N) :-
	telling(File),
	tell(stdout),
	tab(N),
	tell(File).

tab(X) :-
	( X == 1 ->
	    true
	;
	    X > 0,
	    space,
	    X1 is X -1,
	    tab(X1)
	).


writeln(X) :-
	write(X), nl.

% write/1 writeq/1 display/1

write(X) :-
	write_with_default_options(X,[]).

writeq(X) :-
	write_with_default_options(X,[quote,module]).

display(X) :-
	write_with_default_options(X,[quote,noops]).

write_with_default_options(X,L1) :-
	nb_getval('$$print_attvar',A1), (A1 == yes -> L2 = [atts|L1] ; L2 = L1),
	nb_getval('$$print_module',A2), (A2 == yes -> L3 = [(module)|L2] ; L3 = L2),
	nb_getval('$$print_depth',Depth),
	telling(_,CurrentOutStream),
	low_write(CurrentOutStream,X,L3,Depth,1200).

needsquote(V) :-
	name(V,L),
	(
	  startswithsmallletterandok(L), !, fail
	;
	  L = [_|_], allspecchars(L), !, fail
	;
	  L = [C],
	  (C = 0', ; C = 0'; ; C = 0'|),
	  !, fail
	;
	  true
	).

allspecchars([]).
allspecchars([X|R]) :-
	specchar(X), !,
	allspecchars(R).


specchar(0'@).
specchar(0'#).
specchar(0'$).
% specchar(0'%).
specchar(0'^).
specchar(0'&).
specchar(0'*).
specchar(0'+).
specchar(0'-).
specchar(0'=).
specchar(0'<).
specchar(0'>).
specchar(0'?).
specchar(0'/).
specchar(0'\\).
specchar(0'.).
specchar(0':).


startswithsmallletterandok([A|R]) :-
	0'a =< A, 0'z >= A,
	allalfanum(R).

allalfanum([]).
allalfanum([A|R]) :-
	(
	  0'a =< A, 0'z >= A
	;
	  0'A =< A, 0'Z >= A
	;
	  0'0 =< A, 0'9 >= A
	;
	  A == 0'_
	), !,
	allalfanum(R).


arity2_assoc(xfx).
arity2_assoc(xfy).
arity2_assoc(yfx).

arity1_assoc_pre(fx).
arity1_assoc_pre(fy).

arity1_assoc_post(xf).
arity1_assoc_post(yf).



%%%%%%%%%%

write(X,B) :-
	(X = stderr ->
	    write_stream(2,B)
	;
	    write(B)
	).

nl :- put(0'\n).
nl(_) :- nl.

%%%%%%%%%%

:- typedef open_type -> (read ; write ; append).

%---------------------------------------------------------------------------%
:- export pred open_stream(atom::in, open_type::in, stream:: out) is semidet.
%---------------------------------------------------------------------------%

open_stream(FileName,How,Stream) :-
	atom(FileName),
	legal_open_type(How,CodeHow),
	var(Stream),
	sysh:open_stream(FileName,CodeHow,S), % S to make sure it is not attributed
	!,
	Stream = S.
open_stream(FileName,How,Stream) :-
	sysh:error_general_builtin(open_stream(FileName,How,Stream),?,?).

%---------------------------------------------------------------------------%
:- pred legal_open_type(open_type::in, int::out) is semidet.
%---------------------------------------------------------------------------%

legal_open_type(read,0).
legal_open_type(write,1).
legal_open_type(append,2).
% legal_open_type(stringread,4).
legal_open_type(atomread,4).


%---------------------------------------------------------------------------%
:- export pred close_stream(stream::in) is semidet.
%---------------------------------------------------------------------------%


close_stream(Stream) :-
	(sysh:low_close_stream(Stream) ->
	    true
	;
	    sysh:error_general_builtin(close_stream(Stream),?,?)
	).


%---------------------------------------------------------------------------%
:- pred write_stream(stream::in, prolog_term::oo) is semidet.
%---------------------------------------------------------------------------%
% write_stream/2
% 
%---------------------------------------------------------------------------%

write_stream(Stream, Term) :- write_stream(Stream, Term, []).


%---------------------------------------------------------------------------%
:- typedef write_options = list(write_option).
:- typedef write_option = (
			    quote
			  ; print_depth(int)
			  ; atts
			  ; numbervars(int)
			  ; (module)
			    ).

%---------------------------------------------------------------------------%
:- pred write_stream(stream::in, prolog_term::oo, write_options::in) is semidet.
%---------------------------------------------------------------------------%
% write_stream/3
% 
%---------------------------------------------------------------------------%

write_stream(Stream, Term, Options) :-
	determine_print_depth(Options, Depth), % singled out because intensive
	(me(numbervars(N),Options) ->
	    numbervars(Term,N,_)
	;
	    true
	),
	(low_write(Stream,Term, Options, Depth,1200) ->
	    true
	;
	    sysh:error_general_builtin(write_stream(Stream, Term, Options),?,?)
	).

low_write(Stream,Term,Options,Depth,Wprec) :-
	(Depth < 0 ->
	    sysh:write_atomic(Stream,' ... ',off,  [])
	;
	    Depth1 is Depth - 1,
	    (var(Term) ->
		sysh:write_var(Stream,Term),
		(attvar(Term), me(atts,Options) ->
		    sysh:get_attr_lowlevel(Term,Atts),
		    (Atts == [] ->
			true
		    ;
			put_stream(Stream,0'{),
			low_write(Stream,Atts,Options, Depth1,999),
			put_stream(Stream,0'})
		    )
		;
		    true
		)
	    ;
		atom(Term) ->
		writeatom(Stream,Options,Term,brackets)
	    ;
		string(Term) ->
		(me(quote,Options) ->
		    sysh:write_atomic(Stream,Term,on, [])
		;
		    sysh:write_atomic(Stream,Term,off,  [])
		)
	    ;
		atomic(Term) ->
		(me(quote,Options) ->
		    sysh:write_atomic(Stream,Term,on, [])
		;
		    sysh:write_atomic(Stream,Term,off,  [])
		)
	    ;
		Term = [A|R] ->
		put_stream(Stream,0'[),
		low_write(Stream,A,Options,Depth1,999),
		wl(Stream,R,Options,Depth1),
		put_stream(Stream,0'])
	    ;
		Term = '$VAR'(VN) ->
		writenumbervars(Stream,VN)
	    ;
		(
		  write_operator(Stream,Term,Options,Depth1,Wprec) , !
		;
		  functor(Term,Name,Arity),
		  writeatom(Stream,Options,Name,nobrackets),
		  put_stream(Stream,0'(),
		  write_args(Stream,Arity,Term,Options,Depth1),
		  put_stream(Stream,0'))
		)
	    )
	).

determine_print_depth(Options, Depth) :-
	(me(print_depth(Depth),Options) ->
	    true
	;
	    nb_getval('$$print_depth',Depth)
	).

writeatom(Stream,Options,Name,Brackets) :-
	(me(quote,Options) ->
	    Q = quote
	;
	    Q = noquote
	),
	(
	  once(me(module,Options)),
	  add_module(Name,Mod,_),
	  Mod \== user,
	  watom(Stream,Q,Mod,Brackets), put_stream(Stream,0':),
	  fail
	;
	  true
	),
	watom(Stream,Q,Name,Brackets).


watom(Stream,_,[],_) :- !, sysh:write_atomic(Stream,[],off,  []).
watom(Stream,quote,Dot,_) :-
	name(Dot,[0'.]),
	!,
	put_stream(Stream,0''), put_stream(Stream,0'.), put_stream(Stream,0'').
watom(Stream,_, !,_) :- !, sysh:write_atomic(Stream,!,off,  []).
watom(Stream,_, (;),_) :- !, sysh:write_atomic(Stream,(;),off,  []).
watom(Stream,_, Comma,_) :-
	name(Comma, [0',]),
	!, sysh:write_atomic(Stream,Comma,off,  []).
watom(Stream,Quote,Name,Brackets) :-
	(current_op(N,_,Name), N > 0, Brackets == brackets ->
	    Op = true,
	    put_stream(Stream,0'()
	;
	    Op = false
	),
	(Quote == quote, sysh:needsquote(Name) ->
	    sysh:write_atomic(Stream,Name,on, [])
	;
	    sysh:write_atomic(Stream,Name,off,  [])
	),
	(Op == true ->
	    put_stream(Stream,0'))
	;
	    true
	).


wl(Stream,Term,Options,Depth) :-
	(var(Term) ->
	    put_stream(Stream,0'|),
	    sysh:write_var(Stream,Term)
	;
	    Term = [A|R] ->
	    put_stream(Stream,0',),
	    low_write(Stream,A,Options,Depth,999),
	    (Depth < 0 ->
		sysh:write_atomic(Stream,' | ... ',off,  [])
	    ;
		Depth1 is Depth - 1,
		wl(Stream,R,Options,Depth1)
	    )
	;
	    Term = [] ->
	    true
	;
	    put_stream(Stream,0'|),
	    low_write(Stream,Term,Options,Depth,999)
	).


newprec(xfx,Opprec,Leftprec,Rightprec) :-
	Leftprec is Opprec - 1,
	Rightprec = Leftprec.
newprec(xfy,Opprec,Leftprec,Rightprec) :-
	Leftprec is Opprec - 1,
	Rightprec = Opprec.
newprec(yfx,Opprec,Leftprec,Rightprec) :-
	Rightprec is Opprec - 1,
	Opprec = Leftprec.

newprec(fx,Opprec,NewPrec) :- NewPrec is Opprec - 1.
newprec(xf,Opprec,NewPrec) :- NewPrec is Opprec - 1.
newprec(fy,Opprec,Opprec).
newprec(yf,Opprec,Opprec).


write_operator(Stream,Op2,Options,M,Wprec) :-
	\+(me(noops,Options)),
	functor(Op2,Name,Arity),
	( Arity = 2 ->
	    current_op(Opprec,Assoc,Name),
	    sysh:arity2_assoc(Assoc),
	    arg(1,Op2,A1),
	    arg(2,Op2,A2),
	    newprec(Assoc,Opprec,Leftprec,Rightprec),
	    (Wprec < Opprec -> openbracket(Stream) ; true),
	    low_write(Stream,A1,Options,M,Leftprec), space(Stream),
	    writeatom(Stream,Options,Name,nobrackets), space(Stream),
	    low_write(Stream,A2,Options,M,Rightprec),
	    (Wprec < Opprec -> closebracket(Stream) ; true), !
	;
	    Arity = 1,
	    current_op(Opprec,Assoc,Name),
	    (sysh:arity1_assoc_pre(Assoc) ->
		arg(1,Op2,A1),
		newprec(Assoc,Opprec,NewPrec),
		(Wprec < Opprec -> openbracket(Stream) ; true),
		writeatom(Stream,Options,Name,nobrackets), space(Stream),
		low_write(Stream,A1,Options,M,NewPrec),
		(Wprec < Opprec -> closebracket(Stream) ; true)
	    ;
		sysh:arity1_assoc_post(Assoc) ->
		arg(1,Op2,A1),
		newprec(Assoc,Opprec,NewPrec),
		(Wprec < Opprec -> openbracket(Stream) ; true),
		low_write(Stream,A1,Options,M,NewPrec), space(Stream),
		writeatom(Stream,Options,Name,nobrackets),
		(Wprec < Opprec -> closebracket(Stream) ; true)
	    )
	).

space(S) :- put_stream(S,0' ).
openbracket(S) :- put_stream(S,0'().
closebracket(S) :- put_stream(S,0')).



write_args(Stream,1,Term,Options,M) :- !,
	arg(1,Term,A1),
	low_write(Stream,A1,Options,M,999).
write_args(Stream,I,Term,Options,M) :-
	J is I - 1,
	write_args(Stream,J,Term,Options,M),
	arg(I,Term,Ai),
	put_stream(Stream,0',),
	low_write(Stream,Ai,Options,M,999).


writenumbervars(Stream,-1) :- !,
	sysh:write_atomic(Stream,' _ ',off,  []).
writenumbervars(Stream,N) :-
	integer(N), N >= 0, !,
	M is N // 26,
	I is N mod 26,
	K is 0'A + I,
	put_stream(Stream,K),
	(M > 0 ->
	    sysh:write_atomic(Stream,M,off,  [])
	;
	    true
	).
writenumbervars(Stream,N) :-
	(atom(N) ->
	   sysh:write_atomic(Stream,N,off,  [])
	;
	    write_stream(Stream,N)
	).



%---------------------------------------------------------------------------%
:- pred put_stream(Stream,stream::in, int::in) is semidet.
%---------------------------------------------------------------------------%
% put_stream/2
% 
%---------------------------------------------------------------------------%

put_stream(Stream,Int) :-
	( sysh:low_put(Stream,Int) ->
	    true
	;
	    sysh:error_general_builtin(put_stream(Stream,Int),?,?)
	).


%---------------------------------------------------------------------------%
:- pred me(T,list(T)).
:- mode me(oo,oo) is nondet.
%---------------------------------------------------------------------------%
% me/2
% 
%---------------------------------------------------------------------------%

me(X,[X|_]).
me(X,[_|R]) :- me(X,R).

%---------------------------------------------------------------------------%
% Errors
%
%---------------------------------------------------------------------------%

%% open_stream:
%% arg1: not an atom - not a file that can be opened
%% arg2: illegal open type
%% arg3: not a variable
%% 
%% close_stream:
%% arg1: not a stream - not an open stream
%% 
%% get0/2:
%% arg1: not a stream - not an open stream - not a readable stream
%% arg2: not a variable
%% 
%% put/2:
%% arg1: not a stream - not an open stream - not a writeable stream
%% arg2: not a variable


%---------------------------------------------------------------------------%
:- pred read_stream(stream::in,term::out) is semidet.
%---------------------------------------------------------------------------%
% read_stream/2
% 
%---------------------------------------------------------------------------%

read_stream(Stream,Term) :-
	seeing(_,CurrentInStream),
	see(Stream), % using an extension for see/1
	(read(Term) ->
	    see(CurrentInStream)
	;
	    see(CurrentInStream),
	    fail
	).

%---------------------------------------------------------------------------%
seeing(File) :- seeing(File,_).
telling(File) :- telling(File,_).


%---------------------------------------------------------------------------%
:- pred all_open_streams(list(stream)::out) is det.
%---------------------------------------------------------------------------%
% all_open_streams/1
%---------------------------------------------------------------------------%

all_open_streams(ListOfStreams) :-
	fetchstreams(0,ListOfStreams).

fetchstreams(N,Out) :-
	(sysh:low_get_open_stream(N,Stream,FoundAt) ->
	    Out = [Stream|RestOut],
	    NextN is FoundAt + 1,
	    fetchstreams(NextN,RestOut)
	;
	    Out = []
	).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Writing out of big terms in better readable format      %%%
%%% Not standard at all and not easy to read in again       %%%
%%% Bart Demoen Wed Jul  2 13:02:18 CEST 2003               %%%
%%%  ?- X = f(Y), Y = foo(X), wc(X-Y).                      %%%
%%%  A - B                                                  %%%
%%%    where                                                %%%
%%%          B = foo(A)                                     %%%
%%%          A = f(B)                                       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



write_big(Term) :-
	subt(Term,[],_,[],Subs),
	(Subs == [] ->
	    write(Term)
	;
	    wsubs(Term,Subs)
	),
	fail.
write_big(_).

subt(Term,In,Out,Ind,Outd) :-
	(attvar(Term) ->
	    get_attr(Term,Value),
	    subt(Value,In,Out,Ind,Outd)
	;
	    (var(Term) ->
		In = Out,
		Ind = Outd
	    ;
		(\+(bigint(Term)) , (atomic(Term) ; Term = '$VAR'(_)) ->
		    In = Out,
		    Ind = Outd
		;
		    (isidin(Term,In) ->
			In = Out,
			(isidin(Term,Ind) ->
			    Ind = Outd
			;
			    Outd = [Term|Ind]
			)
		    ;
			In2 = [Term|In],
			Ind2 = Ind,
			(Term = [X|R] ->
			    subt(X,In2,In3,Ind2,Ind3),
			    subt(R,In3,Out,Ind3,Outd)
			;
			    Term =.. [_|R],
			    subtl(R,In2,Out,Ind2,Outd)
			)
		    )
		)
	    )
	).

subtl([],In,In,Ind,Ind).
subtl([X|R],In,Out,Ind,Outd) :-
	subt(X,In,In2,Ind,Ind2),
	subtl(R,In2,Out,Ind2,Outd).

isidin(T,[X|R]) :-
	(==(T,X) ->
	    true
	;
	    isidin(T,R)
	).

trsubs(Term,NewTerm,NewVarSubs) :-
	subt(Term,[],_,[],VarSubs1),
	pairvars(VarSubs1,VarSubs),
	mknewterm(Term,VarSubs,NewTerm),
	mknewvarsubs(VarSubs,VarSubs,NewVarSubs).

wsubs(Term,Subs) :-
	pairvars(Subs,VarSubs),
	mknewterm(Term,VarSubs,NewTerm),
	mknewvarsubs(VarSubs,VarSubs,NewVarSubs),
	numbervars(NewTerm+NewVarSubs,0,_),
	write(NewTerm), nl,
	write("  where"), nl,
	wlv(NewVarSubs).

wlv([]).
wlv([A|R]) :- write("        "), write(A),nl, wlv(R).



mknewvarsubs([],_,[]).
mknewvarsubs([V=Term|R],VarSubs,[V=NewTerm|NewR]) :-
	Term =.. [Name|Args],
	mknewterml(Args,VarSubs,NewArgs),
	NewTerm =.. [Name|NewArgs],
	mknewvarsubs(R,VarSubs,NewR).

pairvars([],[]).
pairvars([X|R],[_ = X|Rs]) :- pairvars(R,Rs).

mknewterm(Term,VSs,NewTerm) :-
	(attvar(Term) ->
	    get_attr(Term,Value),
	    mknewterm(Value,VSs,NewValue),
	    put_attr(Term,NewValue),
	    NewTerm = Term
	;
	    (\+(bigint(Term)), (var(Term) ; atomic(Term) ; Term = '$VAR'(_))) ->
	    NewTerm = Term
	;
	    (isinsubs(Term,VSs,V) ->
		NewTerm = V
	    ;
		(Term = [A|B] ->
		    mknewterm(A,VSs,NewA),
		    mknewterm(B,VSs,NewB),
		    NewTerm = [NewA|NewB]
		;
		    Term =.. [Name|Args],
		    mknewterml(Args,VSs,NewArgs),
		    NewTerm =.. [Name|NewArgs]
		)
	    )
	).

mknewterml([],_,[]).
mknewterml([A|R],VSs,[NA|NR]) :-
	mknewterm(A,VSs,NA),
	mknewterml(R,VSs,NR).


isinsubs(T,[V = T2|R],W) :-
	(T == T2 ->
	    W = V
	;
	    isinsubs(T,R,W)
	).


%%%%%%%%%%%%%%%%%

read_term(Stream,Term,Options) :-
	seeing(_,CurrentInStream),
	see(Stream),		% using an extension for see/1
	(lread(Term,Options) ->
	    see(CurrentInStream)
	;
	    see(CurrentInStream),
	    fail
	).

lread(Term,Options) :-
	select_options(Options,SyntaxError,Variable_names,Variables,Singletons,Layout), !,
	native_hal_read:hal_read_term(T,_,_),
	(
	  T == eof ->
	  Term = end_of_file,
	  Options = []
	;
	  T = mercury_error(String,Int) ->
	  write(String), write(' at line '), write(Int),
	  seeing(File,_),
	  write(' of file '),
	  write(File),
	  nl,
	  SyntaxError == error,
	  throw(syntax_error)
	;
	  T = hal_read_term(Dict,Sterm),
	  Dict = dict(_,Varsl),
	  sysh:mkvarlist(Varsl,Vars,VarsDict),
	  sysh:mkterm(Sterm,Term,VarsDict),
	  (var(Variable_names) ->
	      Variable_names = Vars
	  ;
	      true
	  ),
	  (var(Variables) ->
	      true
	  ;
	      true
	  ),
	  (var(Singletons) ->
	      true
	  ;
	      true
	  ),
	  (var(Layout) ->
	      true
	  ;
	      true
	  )
	),
	! .
lread(_,Options) :-
	write("Read_term failed because Options where bad: "),
	write(Options),
	nl,
	fail.


select_options(Options,SyntaxError,Variable_names,Variables,Singletons,Layout) :-
	(me(syntax_errors(SyntaxError),Options) ->
	    (SyntaxError == error ; SyntaxError == fail), ! 
	;
	    SyntaxError = error
	),
	(me(variables(Variables),Options) ->
	    var(Variables)
	;
	    Variables = none
	),
	(me(variable_names(Variable_names),Options) ->
	    var(Variable_names)
	;
	    Variable_names = none
	),
	(me(singletons(Singletons),Options) ->
	    var(Singletons)
	;
	    Singletons = none
	),
	(me(layout(Layout),Options) ->
	    var(Layout)
	        ;
	    Layout = none
	).
