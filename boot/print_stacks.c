#include "toinclude.h"
#include <stdlib.h>

/* the next four are common with the ones in gc.c */

#define LS_MARK_BIT           0x01
#define ENV_BIT               0x01
#define env_marked(p)      (ls_mark_array[startls-p] == ENV_BIT)
#define ls_marked(p)      (ls_mark_array[startls-p] == LS_MARK_BIT)

static dlong printnum = 0;

void print_tr(struct machine *machp)
{
  char buf[100];
  FILE *where;
  dlong *p,*pp,*q;
  dlong i,j;


  sprintf(buf,"TR%ld",printnum);
  where = fopen(buf,"w") ;
  if (! where)
    {
      exit_mess("could not open TR%ld",printnum);
      return;
    }

  i = 0;
  for (pp = machp->begin_trail; pp < machp->TR; pp++)
    {
      p = (dlong *)*pp;
      fprintf(where,"trail(%ld,",i);
      i++;
      switch (tag(p))
	{
	case REF0:
	case REF1:
	  if ((machp->begin_heap <= p) && (p < machp->end_heap))
	    fprintf(where,"heap_ref,%ld).\n",(dlong)(p-machp->begin_heap));
	  else
	    fprintf(where,"unknown_ref,%ld).\n",(dlong)p);
	  break;
	case LIST:
	  q = get_list_pointer(p);
	  fprintf(where,"list_ref,%ld).\n",(dlong)(q-machp->begin_heap));
	  break;
	case STRING:
	  q = get_string_pointer(p);
	  fprintf(where,"string_ref,%ld).\n",(dlong)(q-machp->begin_heap));
	  break;
	case STRUCT:
	  q = get_struct_pointer(p);
	  fprintf(where,"struct_ref,%ld).\n",(dlong)(q-machp->begin_heap));
	  break;
	case SIMPLE:
	  if (is_atom(p))
	    fprintf(where,"atom,%ld).\n",get_atom_index(p));
	  else
	    fprintf(where,"char,%ld).\n",(dlong)get_character(p));
	  break;
	case NUMB:
	  q = get_number_pointer(p);
	  if (*q == REAL_HEADER)
	    fprintf(where,"real_ref,%ld).\n",(dlong)(q-machp->begin_heap));
	  if (*q == BIGINT_HEADER)
	    fprintf(where,"bigint_ref,%ld).\n",(dlong)(q-machp->begin_heap));
	  break;
	case ATT:
	  q = get_attvar_pointer(p);
	  fprintf(where,"attvar, %ld).\n",(dlong)(q-machp->begin_heap));
	  break;
	}
    }

  fclose(where);
} /* print_tr */

void print_translationtable(dlong *translationtable, dlong len, dlong fromwhere)
{
  dlong i = 0;
  FILE *where;
  char buf[100];

  sprintf(buf,"TRANS%ld",printnum);
  where = fopen(buf,"w") ;
  if (! where)
    {
      exit_mess("could not open TRANS%ld",printnum);
      return;
    }

  while (len > 0)
    {
      fprintf(where,"trans(%ld,%ld).\n",i+fromwhere,translationtable[i]);
      i++; len--;
    }

  fclose(where);
} /* print_translationtable */


void print_cp(struct machine *machp)
{

  char buf[100];
  FILE *where;
  dlong index = 0;
  dlong i;
  dlong *st, *p, *en, *cont;

  dlong *beginls = machp->begin_ls;
  dlong *endls = machp->TOS;
  dlong *beginheap = machp->begin_heap;
  dlong *endheap = machp->H;
  dlong *begintr = machp->begin_trail;
  dlong *endtr = machp->TR;

  sprintf(buf,"CP%ld",printnum);
  where = fopen(buf,"w") ;
  if (! where)
    {
      exit_mess("could not open CP%ld",printnum);
      return;
    }

  p = st = machp->begin_cp;
  en = machp->B;
  while (p >= en)
    {
      fprintf(where,"cp(%ld,",index);
      cont = (dlong *)*p;
      if ((st >= cont) && (cont >= en))
	fprintf(where,"cp-%ld).\n",(dlong)(st-cont));
      else
	if ((beginls >= cont) && (cont >= endls))
	  fprintf(where,"env-%ld).\n",(dlong)(beginls-cont));
	else
	  if (((endtr+1) >= cont) && (cont >= begintr))
	    {if ((dlong)cont & 0x1)
		fprintf(where,"tr1-%ld).\n",(dlong)(cont-begintr));
	      else
		fprintf(where,"tr-%ld).\n",(dlong)(cont-begintr));
	    }
	  else
	    if ((endheap >= cont) && (cont >= beginheap))
	      fprintf(where,"heap-%ld).\n",(dlong)(cont-beginheap));
	    else if ((codezone <= (byte *)cont) && ((byte *)cont <= (codezone+16000000)))
	      fprintf(where,"code-%ld).\n",(dlong)((byte *)cont-codezone));
      else
	{
	  switch (tag(cont))
	    {
	    case REF0:
	    case REF1:
	      fprintf(where,"?? - ??,%lx).\n",(unsigned dlong)cont);
	      break;
	    case SIMPLE:
	      if (is_atom(cont))
		fprintf(where,"atom,%ld).\n",get_atom_index(cont));
	      else
		fprintf(where,"char,%ld).\n",(dlong)get_character(cont));
              break;
	    case NUMB:
	      {
		dlong *q = get_number_pointer(cont);
		i = q-beginheap;
		if (*q == REAL_HEADER)
		  fprintf(where,"realpointer,%ld).\n",i);
		if (*q == BIGINT_HEADER)
		  fprintf(where,"bigintpointer,%ld).\n",i);
	      }
	      break;
	    case LIST:
              i = get_list_pointer(cont)-beginheap;
              fprintf(where,"listpointer,%ld).\n",i);
              break;
	    case STRUCT:
              i = get_struct_pointer(cont)-beginheap;
              fprintf(where,"structpointer,%ld).\n",i);
              break;
	    case STRING:
	      //	      i = get_string_pointer(cont)-beginheap;
              fprintf(where,"string,%ld).\n",(dlong)cont);
              break;
	    default:
	      fprintf(where,"totally_unknown).\n");
	      break;
	    }
	}

      p--; index++;
    }


  fclose(where);

} /* print_cp */

void print_atoms(struct machine *machp)
{
  dlong i;
  char buf[100];
  FILE *where;

  sprintf(buf,"GLOBVAR%ld",printnum);
  where = fopen(buf,"w") ;
  if (! where)
    {
      exit_mess("could not open GLOBVAR%ld",printnum);
      return;
    }

  for (i = number_of_symbols(); i--; )
    {
      dlong *p;
      p = (dlong *)symtab[i].glob_val;
      if (! has_atom_tag(p))
	{
	  fprintf(where,"atom(%ld,%s,",i,symtab[i].name);
	  if (is_list(p))
	    {
	      p = get_list_pointer(p);
	      fprintf(where,"list -> %ld).\n",(dlong)(p-machp->begin_heap));
	      continue;
	    }
	  if (is_struct(p))
	    {
	      p = get_struct_pointer(p);
	      fprintf(where,"struct -> %ld).\n",(dlong)(p-machp->begin_heap));
	      continue;
	    }
	  if (is_ref(p))
	    {
	      fprintf(where,"ref -> %ld).\n",(dlong)(p-machp->begin_heap));
	      continue;
	    }
	  if (is_attributed(p))
	    {
	      p = get_attvar_pointer(p);
	      fprintf(where,"attvar -> %ld).\n",(dlong)(p-machp->begin_heap));
	      continue;
            }
	  if (is_number(p))
	    {
	      p = get_number_pointer(p);
	      if (*p == REAL_HEADER)
		fprintf(where,"real -> %ld).\n",(dlong)(p-machp->begin_heap));
	      if (*p == BIGINT_HEADER)
		fprintf(where,"bigint -> %ld).\n",(dlong)(p-machp->begin_heap));
              continue;
	    }
	}
    }
  fclose(where);

} /* print_atoms */

void print_ls(struct machine *machp)
{

  dlong index = 0;
  dlong *st, *p, *en;
  char buf[100];
  FILE *where;
  dlong *startls = machp->begin_ls;
  char *markstring;
  char *ls_mark_array = machp->ls_mark_array;
  dlong *beginls = machp->begin_ls;
  dlong *endls = machp->TOS;
  dlong *beginheap = machp->begin_heap;
  //  dlong *endheap = machp->end_heap ;
  dlong *endheap = machp->H;
  dlong *begintr = machp->begin_trail;
  dlong *endtr = machp->TR;
  dlong *begincp = machp->begin_cp;
  dlong *endcp = machp->begin_cp;

  sprintf(buf,"LS%ld",printnum);
  where = fopen(buf,"w") ;
  if (! where)
    {
      exit_mess("could not open LS%ld",printnum);
      return;
    }

  p = st = machp->begin_ls;
  en = machp->TOS;
  while (p >= en)
    {
      dlong *cont;
      dlong i;

      fprintf(where,"ls(%ld,",index);
      if (ls_marked(p))
	markstring = "marked";
      else
	if (env_marked(p))
	  markstring = "env_marked";
      else
	markstring = "not";

      cont = (dlong *)*p;
      if (is_ref(cont) && ((en <= cont) && (cont <= st)))
	fprintf(where,"env - %ld,%s).\n",(dlong)(st-cont),markstring);
      else
	if (is_ref(cont) && ((begincp >= cont) && (cont >= endcp)))
	  fprintf(where,"cp - %ld,%s).\n",(dlong)(st-cont),markstring);
	else
	  if (is_ref(cont) && ((endtr >= cont) && (cont >= begintr)))
	    fprintf(where,"tr - %ld,%s).\n",(dlong)(cont-begintr),markstring);
	  else
	    if (is_ref(cont) && ((endheap >= cont) && (cont >= beginheap)))
	      fprintf(where,"heap - %ld,%s).\n",(dlong)(cont-beginheap),markstring);
	    else if ((codezone <= (byte *)cont) && ((byte *)cont <= (codezone+16000000)))
	      fprintf(where,"code-%ld).\n",(dlong)((byte *)cont-codezone));
      else
	{
	  switch (tag(cont))
	    {
	    case REF0:
	    case REF1:
	      fprintf(where,"?? - %lx,%s).\n",(unsigned dlong)cont,markstring);
	      break;
	    case SIMPLE:
	      if (is_atom(cont))
		fprintf(where,"atom - %ld,%s).\n",get_atom_index(cont),markstring);
	      else
	      if (is_smallint(cont))
		fprintf(where,"int - %ld,%s).\n",get_smallint(cont),markstring);
	      else
		fprintf(where,"char - %ld,%s).\n",(dlong)get_character(cont),markstring);
              break;
	    case NUMB:
	      {
		dlong *q = get_number_pointer(cont);
		i = q-beginheap;
		if (*q == REAL_HEADER)
		  fprintf(where,"realpointer - %ld,%s).\n",i,markstring);
		if (*q == BIGINT_HEADER)
		  fprintf(where,"bigintpointer - %ld,%s).\n",i,markstring);
	      }
	      break;
	    case LIST:
              i = get_list_pointer(cont)-beginheap;
              fprintf(where,"listpointer - %ld,%s).\n",i,markstring);
              break;
	    case STRUCT:
              i = get_struct_pointer(cont)-beginheap;
              fprintf(where,"structpointer - %ld,%s).\n",i,markstring);
              break;
	    case STRING:
	      i = get_string_pointer(cont)-beginheap;
	      fprintf(where,"string - %ld,%s).\n",i,markstring);
	      break;
	    default:
	      fprintf(where,"totally_unknown,%s,%lx).\n",markstring, (unsigned dlong) cont);
	      break;
	    }
	}
      p--; index++;
    }

  fclose(where);
} /* print_ls */

void print_cell(FILE *where, dlong q, struct machine *machp, dlong *begin, dlong *end)
{
  /* print_cell is only ever called from Areg */
  switch (tag(q))
    {
    case REF0:
    case REF1:
      fprintf(where,"ref,%ld).\n",(dlong)((dlong *)q - begin));
      break;
    case NUMB:
      {
	dlong *r = get_number_pointer(q);
	if (*r == REAL_HEADER) 
	  fprintf(where,"realpointer,%ld).\n",(dlong)(r-begin));
	if (*r == BIGINT_HEADER) 
	  fprintf(where,"bigintpointer,%ld).\n",(dlong)(r-begin));
      }
      break;
    case SIMPLE:
      if (is_atom(q))
	fprintf(where,"atom,%s).\n",get_atom_name(q));
      else
	fprintf(where,"char,%ld).\n",(dlong)get_character(q));
      break;
    case LIST:
      fprintf(where,"list,%ld).\n",(dlong)(get_list_pointer(q)-begin));
      break;
    case STRUCT:
      {
	dlong index = get_funct_symb(q);
	dlong arity = get_funct_arity(q);
	dlong *s = get_struct_pointer(q);
	char *n;
	
	if ((0 <= index) && (index <= number_of_symbols()) &&
	    (arity == symtab[index].arity))
	  n = symtab[index].name;
	else
	  n = 0;
	if ((begin <= s) && (s <= end))
	  index = s - begin;
	else
	  index = -1;
	if ((index >= 0) && (n != 0))
	  fprintf(where,"struct/func,%ld - (%s/%ld)).\n",index,n,arity);
	else
	  if (n != 0)
	    fprintf(where,"func,%s/%ld).\n",n,arity);
	  else
	    fprintf(where,"struct,%ld).\n",index);
      }
      break;
    case ATT:
      fprintf(where,"attvar, %ld).\n",(dlong)(get_attvar_pointer(q)-begin));
      break;
    }
} /* print_cell */

extern dlong doprintls;
extern dlong doprinttr;
extern dlong doprintcp;
extern dlong doprintheap;
extern dlong doprintatoms;


void print_heap(dlong arity, struct machine *machp, dlong *translationtable, dlong *starth)
{
  dlong *p;
  dlong *end;
  dlong *begin;
  dlong i;
  char *heap_mark_array = machp->heap_mark_array;
  char buf[100];
  FILE *where;
  dlong q;

  printnum++;

  if (doprintls) print_ls(machp);
  if (doprinttr) print_tr(machp);
  if (doprintcp) print_cp(machp);
  if (doprintatoms) print_atoms(machp);
  if (translationtable) print_translationtable(translationtable,machp->H - starth, starth -machp->begin_heap);

  if (! doprintheap) return;
  end = machp->H;
  begin = p = machp->begin_heap;

  sprintf(buf,"ARGS%ld",printnum);
  where = fopen(buf,"w") ;
  if (! where)
    { 
      exit_mess("could not open ARGS%ld",printnum);
      return;
    }

  for (i = 1; i <= arity; i++)
    {
      fprintf(where,"argument(%ld,",i);
      print_cell(where,machp->Areg[i],machp,begin,end);
    }

  fclose(where);

  sprintf(buf,"HEAP%ld",printnum);
  where = fopen(buf,"w") ;
  if (! where)
    { 
      exit_mess("could not open HEAP%ld",printnum);
      return;
    }

  i = 0;
  while (p < end)
    {
      char *marked;
      if (heap_mark_array[i])
	marked = "+";
      else
	marked = "-";
      fflush(where);

      //      fprintf(where,"%lx %lx   ",p,*p);

      q = *p;
      if (q == REAL_HEADER)
	{
	  fprintf(where,"heap(%ld,%s,",i,marked);
#ifdef BITS32
	  fprintf(where,"real(%ld,%ld), %e).\n",p[1],p[2],*(dfloat *)(p+1));
#else
	  fprintf(where,"real(%ld,%ld), %Le).\n",p[1],p[2],*(dfloat *)(p+1));
#endif
	  p += 2;
	  i += 2;
	}
      else
      if (q == BIGINT_HEADER)
	{
	  dlong siz, onheap, pos;
	  char *conheap, *cpos;
	  p++; i++;
	  if (heap_mark_array[i])
	    marked = "+";
	  else
	    marked = "-";
	  siz = bigint_size(*p);
	  pos = bigint_positive(*p);
	  if (pos) cpos = "positive" ; else cpos = "negative";
	  onheap = bigint_onheap(*p);
	  if (onheap) conheap = "on_heap"; else conheap = "elsewhere";
	  fprintf(where,"heap(%ld,%s,",i-1,marked);
	  fprintf(where,"bigint, very_big(%ld,%s,%s)).\n",siz,cpos,conheap);
	  if (onheap)
	    {p += siz;i += siz;}
	  else
	    {p++; i++;}
	}
      else
	{
	  fprintf(where,"heap(%ld,%s,",i,marked);

	  switch (tag(q))
	    {
	    case REF0:
	    case REF1:
	      fprintf(where,"ref,%ld).\n",(dlong)((dlong *)q - begin));
	      break;
	    case NUMB:
	      {
		dlong *r = get_number_pointer(q);
		if (*r == REAL_HEADER)
		  fprintf(where,"realpointer,%ld).\n",(dlong)(r-begin));
		if (*r == BIGINT_HEADER)
		  fprintf(where,"bigintpointer,%ld).\n",(dlong)(r-begin));
	      }
	      break;
	    case SIMPLE:
	      if (is_atom(q))
		fprintf(where,"atom,'%s').\n",get_atom_name(q));
	      else
		if (is_char(q))
		  fprintf(where,"char,%ld).\n",(dlong)get_character(q));
		else
		  if (is_smallint(q))
		    fprintf(where,"int, %ld).\n",get_smallint(q));
		  else
		    if (is_string_header(q))
		      {
			char *pc;
			dlong l,len;
			fprintf(where,"string,\"");
			len = get_len_from_string_header(q);
			l = heapcells_needed_for_string(len);
			pc = (char *)(p+1);
			while (len--)
			  fprintf(where,"%c",*pc++);
			fprintf(where,"\").\n");
			while (l--)
			  {
			    i++;
			    fprintf(where,"heap(%ld,%s,rest,string).\n",i,marked);
			    p++;
			  }
		      }
		    else
		      if (is_smallint(q))
			{
			  dlong i = get_smallint(q);
			  fprintf(where,"int,%ld).\n",i);
			}
		      else
			/* it is a functor header */
			{
			  dlong index = get_funct_symb(q);
			  dlong arity = get_funct_arity(q);
			  char *n = symtab[index].name;
			  fprintf(where,"func,(%s/%ld)).\n",n,arity);	      
			}
	      break;

	    case LIST:
	      fprintf(where,"list,%ld).\n",(dlong)(get_list_pointer(q)-begin));
	      break;

	    case STRUCT:
	      {
		dlong *s = get_struct_pointer(q);
		dlong index;
		
		index = s - begin;
		fprintf(where,"struct,%ld).\n",index);
	      }
	      break;

	    case ATT:
	      {
		dlong *qq;
		qq = get_attvar_pointer(q);
		fprintf(where,"attvar,%ld).\n",(dlong)(qq-begin));
	      }
	      break;

	    case STRING:
	      fprintf(where,"string,%ld).\n",(dlong)(get_string_pointer(q)-begin));
	      break;
	    }
	}
      p++; i++;
    }

  fclose(where);
} /* print_heap */

