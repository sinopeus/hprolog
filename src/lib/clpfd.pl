/*  $Id: clpfd.pl,v 1.2 2011-08-20 06:27:46 bmd Exp $

    Part of SWI-Prolog

    Author:        Markus Triska
    E-mail:        triska@gmx.at
    WWW:           http://www.swi-prolog.org
    Copyright (C): 2007, 2008 Markus Triska

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    As a special exception, if you link this library with other files,
    compiled with a Free Software compiler, to produce an executable, this
    library does not by itself cause the resulting executable to be covered
    by the GNU General Public License. This exception does not however
    invalidate any other reasons why the executable file might be covered by
    the GNU General Public License.
*/

/*
A rainy holiday in Esposende - 1-9 febr 2009
Bart Demoen ports a version of the Markus Triska clpfd.pl 
to hProlog, and rewrites parts of the code for performance reasons.

Why are all the veriables just A B C ... because the initial version
was obtained by loading clpfd.pl into SWI and then doing ?- listing.

Why is there no module clpfd ?
Because hProlog doesn't do the operators from a module right at the momen :-(
*/

:- module(clpfd,[]).

:- use_module(library(lists)).

?- op(760, yfx, #<==>).
?- op(750, xfy, #==>).
?- op(750, yfx, #<==).
?- op(740, yfx, #\/).
?- op(730, yfx, #\).
?- op(720, yfx, #/\).
?- op(710,  fy, #\).
?- op(700, xfx, #>).
?- op(700, xfx, #<).
?- op(700, xfx, #>=).
?- op(700, xfx, #=<).
?- op(700, xfx, #=).
?- op(700, xfx, #\=).
?- op(700, xfx, in).
?- op(700, xfx, ins).
?- op(450, xfx, ..). % should bind more tightly than \/

:- op(760, yfx, #<==>).
:- op(750, xfy, #==>).
:- op(750, yfx, #<==).
:- op(740, yfx, #\/).
:- op(730, yfx, #\).
:- op(720, yfx, #/\).
:- op(710,  fy, #\).
:- op(700, xfx, #>).
:- op(700, xfx, #<).
:- op(700, xfx, #>=).
:- op(700, xfx, #=<).
:- op(700, xfx, #=).
:- op(700, xfx, #\=).
:- op(700, xfx, in).
:- op(700, xfx, ins).
:- op(450, xfx, ..). % should bind more tightly than \/


disable_queue :-
	b_setval('$clpfd_queue_status', disabled).

%% clpfd:do_queue :-
%% 	(   b_getval('$clpfd_queue_status', enabled)
%% 	->  (   fetch_propagator(A)
%% 	    ->  activate_propagator(A),
%% 		do_queue
%% 	    ;   true
%% 	    )
%% 	;   true
%% 	).


% rewriting this gave large gains ! Bart Demoen
% it saves a lot of trailing (for hProlog)
% the second one also a lot of local stack

%% do_queue :-
%% 	b_getval('$clpfd_queue_status', E),
%% 	( E == enabled ->
%% 	    fetch_propagator(A),
%% 	    (
%% 	      var(A) ->
%% 	      true
%% 	    ;
%% 	      A == dead ->
%% 	      do_queue
%% 	    ;
%% 	      activate_propagator(A),
%% 	      do_queue
%% 	    )
%% 	;
%% 	    true
%% 	).
%% 
%% fetch_propagator(D) :-
%% 	b_getval('$clpfd_queue', A1-D1),
%% 	(var(A1) ->
%% 	    true
%% 	;
%% 	    A1=[X|C1],
%% 	    b_setval('$clpfd_queue', C1-D1),
%% 	    arg(2, X, B),
%% 	    arg(1, B, C),
%% 	    (C==dead ->
%% 		D = dead
%% 	    ;
%% 		D=X
%% 	    )
%% 	).

do_queue :-
	b_getval('$clpfd_queue_status', E),
	(E == enabled ->
	    b_getval('$clpfd_queue', A1D1), A1D1 = A1-D1, % and a bit of heap
	    (var(A1) ->
		true
	    ;
		A1=[X|C1],
		b_setval('$clpfd_queue', C1-D1),
		arg(2, X, B),
		arg(1, B, C),
		(C == dead ->
		    do_queue
		;
		    activate_propagator(X),
		    do_queue
		)
	    )
	;
	    true
	).

pop_queue(X) :-
	b_getval('$clpfd_queue', A1-D1),
	nonvar(A1),
	A1=[X|C1],
	b_setval('$clpfd_queue', C1-D1).

enable_queue :-
	b_setval('$clpfd_queue_status', enabled),
	do_queue.

make_queue :-
	nb_setval('$clpfd_queue', A-A).

#\(A) :-
	reify(A, 0),
	do_queue.


% Bart - save local stack

activate_propagator(propagator(B, A)) :-
	arg(1, A, Dead),
	(   Dead == dead
	->  true
	;   setarg(1, A, passive),
	    functor(B, C, _),
	    (   C == rel_tuple -> % no_reactivation(C) ->
		b_setval('$clpfd_current_propagator', A),
		run_propagator(B, A),
		b_setval('$clpfd_current_propagator', [])
	    ;
		run_propagator(B, A)
	    )
	).

all_dead(fd_props(A, B, C)) :-
	all_dead_(A),
	all_dead_(B),
	all_dead_(C).

all_dead_([]).
all_dead_([propagator(_, mutable(dead, _))|A]) :-
	all_dead_(A).

all_different(A) :-
	must_be(list, A),
	B=mutable(shared, _),
	all_different(A, [], B),
	do_queue.

all_distinct(A) :-
	must_be(list, A),
	length(A, _),
	B=mutable(shared, _),
	all_distinct(A, [], B),
	do_queue.

check_domain(A) :-
	(   var(A)
	->  instantiation_error(A)
	;   is_domain(A)
	->  true
	;   domain_error(clpfd_domain, A)
	).

choice(step).
choice(enum).
choice(bisect).

% Bart
cis_geq_zero(n(A)) :-
	A>=0.
cis_geq_zero(sup).


% Bart - watsdis ?

constrain_to_integer(A) :-
	fd_get(A, B, C),
	fd_put(A, B, C).

contracting(A) :-
	must_be(list, A),
	maplist(finite_domain, A),
	contracting(A, fail, A).

%   TODO

cyclic_term(T) :- fail.
%% 	(
%% 	  integer(T) ->
%% 	  fail
%% 	;
%% 	  atom(T) ->
%% 	  fail
%% 	;
%% 	  var(T) ->
%% 	  fail
%% 	;
%% 	  arg(1,T,T1),
%% 	  arg(2,T,T2),
%% 	  (cyclic_term(T1) ; cyclic_term(T2))
%% 	).


default_domain(from_to(inf, sup)).

drep_bound(A) :-
	var(A), !,
	instantiation_error(A).
drep_bound(sup) :- !.
drep_bound(inf) :- !.
drep_bound(A) :-
	integer(A), !.

fd_var(A) :-
	get_attr(A, clpfd, _).

fd_variable(A) :-
	(   var(A)
	->  true
	;   integer(A)
	->  true
	;   type_error(integer, A)
	).

finite_domain(A) :-
	(   fd_get(A, B, _)
	->  (   domain_infimum(B, n(_)),
		domain_supremum(B, n(_))
	    ->  true
	    ;   instantiation_error(A)
	    )
	;   integer(A)
	->  true
	;   must_be(integer, A)
	).

indomain(A) :-
	label([A]).

is_bound(n(A)) :-
	integer(A).
is_bound(inf).
is_bound(sup).

is_domain(empty).
is_domain(from_to(A, B)) :-
	is_bound(A),
	is_bound(B),
	cis_leq(A, B).
is_domain(split(A, B, C)) :-
	integer(A),
	is_domain(B),
	is_domain(C),
	all_less_than(B, A),
	all_greater_than(C, A).

is_drep(A) :-
	nonvar(A),
	(integer(A) ->
	    true
	;
	    functor(A,Name,2),
	    (Name == ('..') ->
		A = ..(X,Y),
		nonvar(X), nonvar(Y),
		(integer(X) ->
		    true
		;
		    X == inf
		),
		(integer(Y) ->
		    true
		;
		    Y = sup
		)
	    ;
		Name = ('\\/'),
		A = X \/ Y,
		is_drep(X),
		is_drep(Y)
	    )
	).

is_drep1(A) :-
	var(A), !,
	instantiation_error(A).
is_drep1(A) :-
	integer(A), !.
is_drep1(..(A, B)) :- !,
	drep_bound(A),
	drep_bound(B),
	A\==sup,
	B\==inf.
is_drep1(A\/B) :- !,
	is_drep(A),
	is_drep(B).

kill(A) :-
	setarg(1, A, dead).

label(A) :-
	labeling([], A).

lex_chain(A) :-
	must_be(list(list), A),
	make_propagator(presidual(lex_chain(A)), B),
	lex_chain_(A, B).

no_reactivation(rel_tuple).


one_alive([propagator(_, A)|B]) :-
	(   arg(1, A, dead)
	->  one_alive(B)
	;   true
	).

optimisation(min(_)).
optimisation(max(_)).

order(up).
order(down).

push_queue(A) :-
	b_getval('$clpfd_queue', BAC), BAC = B-[A|C], % more heap
	b_setval('$clpfd_queue', B-C).

reinforce(A) :-
	cpf_clpfd_propagation(F),
	(   F == full
	->  true
	;   term_variables(A, B),
	    maplist_reinforce_( B),
	    do_queue
	).

maplist_reinforce_([]).
maplist_reinforce_([X|R]) :-
	reinforce_(X),
	maplist_reinforce_(R).

maplist(Goal, List) :-
	        maplist2(List, Goal).

maplist2([], _).
maplist2([Elem|Tail], Goal) :-
	call(Goal, Elem),
	maplist2(Tail, Goal).

maplist(Goal, List,List1) :-
	        maplist2(List, Goal, List1).

maplist2([], _, []).
maplist2([Elem|Tail], Goal,[Elem1|T1]) :-
	call(Goal, Elem, Elem1),
	maplist2(Tail, Goal,T1).


reinforce_(A) :-
	(   fd_var(A),
	    fd_get(A, B, C)
	->  put_full(A, B, C)
	;   true
	).

scalar_supported(#=).
scalar_supported(#\=).

selection(ff).
selection(ffc).
selection(min).
selection(max).
selection(leftmost).

trigger_once(A) :-
	trigger_prop(A),
	do_queue.

trigger_once_new(A) :-
	arg(2, A, B),
	arg(1, B, DQ),
	(
	  DQ == dead ->
	  do_queue
	;
	  DQ = queued ->
	  do_queue
	;
	  b_getval('$clpfd_current_propagator', C),
	  (C==B ->
	      do_queue
	  ;
	      setarg(1, B, queued),
	      b_getval('$clpfd_queue', BAC), BAC = B2-[A|C2], % more heap
	      b_setval('$clpfd_queue', B2-C2),
% 	      setarg(2,BAC,C2),  Bart
	      do_queue
	  )
	).


trigger_prop(A) :-
	arg(2, A, B),
	arg(1, B, DQ),
	(   DQ == dead
	->  true
	;   DQ = queued
	->  true
	;   b_getval('$clpfd_current_propagator', C),
	    ( C==B
	    ->  true
	    ;   setarg(1, B, queued),
		push_queue(A)
	    )
	).
% Bart
%% trigger_prop(A) :-
%% 	arg(2, A, B),
%% 	(   arg(1, B, dead)
%% 	->  true
%% 	;   arg(1, B, queued)
%% 	->  true
%% 	;   b_getval('$clpfd_current_propagator', C),
%% 	    C==B
%% 	->  true
%% 	;   setarg(1, B, queued),
%% 	    push_queue(A)
%% 	).

trigger_props(fd_props(A, B, C)) :-
	trigger_props_(A),
	trigger_props_(B),
	trigger_props_(C).

%% trigger_props_([]).
%% trigger_props_([A|B]) :-
%% 	trigger_prop(A),
%% 	trigger_props_(B).

trigger_props_(L) :-
	(L == [] ->
	    true
	;
	    L = [A|B],
	    trigger_prop(A),
	    trigger_props_(B)
	).

v_or_i(A) :-
	var(A), !.
v_or_i(A) :-
	integer(A).

#/\(A, B) :-
	reify(A, 1),
	reify(B, 1),
	do_queue.

#<(B, A) :-
	(   integer(A),
	    integer(B)
	->  A>=B+1
	;   clpfd_geq(A, B+1)
	).

#<==(A, B) :-
	reify(A, D),
	reify(B, C),
	myimpl(C, D),
	do_queue.

#<==>(A, B) :-
	reify(A, C),
	reify(B, C),
	do_queue.

#=(A, B) :-
	clpfd_equal(A, B).

#=<(B, A) :-
	#>=(A, B).

#==>(A, B) :-
	reify(A, C),
	reify(B, D),
	myimpl(C, D),
	do_queue.

#>(A, B) :-
	(   integer(A),
	    integer(B)
	->  A>=B+1
	;   clpfd_geq(A, B+1)
	).

#>=(A, B) :-
	clpfd_geq(A, B).

#\/(A, B) :-
	reify(A, C),
	reify(B, D),
	myor(C, D, 1),
	do_queue.

#\=(A, B) :-
	(   var(A),
	    integer(B)
	->  neq_num(A, B),
	    do_queue
	;   var(A),
	    nonvar(B),
	    B=C-D,
	    var(C),
	    (   integer(D)
	    ;   var(D)
	    )
	->  x_neq_y_plus_z(C, A, D)
	;   var(A),
	    nonvar(B),
	    B=C+D,
	    var(C),
	    (   integer(D)
	    ;   var(D)
	    )
	->  x_neq_y_plus_z(A, C, D)
	;   nonvar(A),
	    var(B),
	    A=C+D,
	    var(C),
	    (   integer(D)
	    ;   var(D)
	    )
	->  x_neq_y_plus_z(B, C, D)
	;   nonvar(A),
	    var(B),
	    A=C-D,
	    var(C),
	    (   integer(D)
	    ;   var(D)
	    )
	->  x_neq_y_plus_z(C, B, D)
	;   nonvar(A),
	    A=abs(E),
	    nonvar(E),
	    E=F-G,
	    var(F),
	    var(G),
	    integer(B)
	->  absdiff_neq_const(F, G, B)
	;   integer(A),
	    nonvar(B),
	    B=abs(E),
	    nonvar(E),
	    E=F-G,
	    var(F),
	    var(G)
	->  absdiff_neq_const(F, G, A)
	;   left_right_linsum_const(A, B, H, I, J)
	->  scalar_product(H, I, #\=, J)
	;   parse_clpfd(A, K),
	    parse_clpfd(B, L),
	    neq(K, L)
	).

all_empty_intersection([], _).
all_empty_intersection([A|D], C) :-
	(   fd_get(A, B, _)
	->  domains_intersection_(B, C, empty),
	    all_empty_intersection(D, C)
	;   all_empty_intersection(D, C)
	).

all_greater_than(empty, _).
all_greater_than(from_to(A, C), B) :-
	cis_gtn(A,B), % cis_gt(A, n(B)), Bart
	cis_gtn(C,B). % cis_gt(C, n(B)).
all_greater_than(split(A, C, D), B) :-
	A>B,
	all_greater_than(C, B),
	all_greater_than(D, B).

all_in_domain([], []).
all_in_domain([C|D], [A|E]) :-
	(   fd_get(A, B, _)
	->  domain_contains(B, C)
	;   A=:=C
	),
	all_in_domain(D, E).

all_less_than(empty, _).
all_less_than(from_to(A, C), B) :-
	cis_lt(A, n(B)),
	cis_lt(C, n(B)).
all_less_than(split(A, C, D), B) :-
	A<B,
	all_less_than(C, B),
	all_less_than(D, B).


% Bart - rewrite make small good difference

clpfd:attr_unify_hook(clpfd_attr(_, _, _, B, C), A) :-
	(
	  integer(A) ->
	  domain_contains(B, A),
	  trigger_props(C),
	  do_queue
	;
	  nonvar(A) ->
	  type_error(integer, A)
	;
	  fd_get(A, D, E),
	  domains_intersection(D, B, F),
	  append_propagators(C, E, G),
	  fd_put(A, F, G),
	  trigger_props(G),
	  do_queue
	).

%% clpfd:attr_unify_hook(clpfd_attr(_, _, _, B, C), A) :-
%% 	( nonvar(A) ->
%% 	    (integer(A) ->
%% 		true
%% 	    ;
%% 		type_error(integer, A)
%% 	    ),
%% 	    domain_contains(B, A),
%% 	    trigger_props(C),
%% 	    do_queue
%% 	;
%% 	    fd_get(A, D, E),
%% 	    domains_intersection(D, B, F),
%% 	    append_propagators(C, E, G),
%% 	    fd_put(A, F, G),
%% 	    trigger_props(G),
%% 	    do_queue
%% 	).

attribute_goal_(presidual(A), A).
attribute_goal_(pgeq(A, B), #>=(A, B)).
attribute_goal_(pplus(A, B, C), #=(A+B, C)).
attribute_goal_(pneq(A, B), #\=(A, B)).
attribute_goal_(ptimes(A, B, C), #=(A*B, C)).
attribute_goal_(absdiff_neq(A, B, C), #\=(abs(A-B), C)).
attribute_goal_(x_neq_y_plus_z(A, B, C), #\=(A, B+C)).
attribute_goal_(x_leq_y_plus_c(A, B, C), #=<(A, B+C)).
attribute_goal_(pdiv(A, B, C), #=(A/B, C)).
attribute_goal_(pexp(A, B, C), #=(A^B, C)).
attribute_goal_(pabs(B, A), #=(A, abs(B))).
attribute_goal_(pmod(A, B, C), #=(A mod B, C)).
attribute_goal_(pmax(B, C, A), #=(A, max(B, C))).
attribute_goal_(pmin(B, C, A), #=(A, min(B, C))).
attribute_goal_(scalar_product(A, B, I, K), H) :-
	A=[C|E],
	B=[D|F],
	coeff_var_term(C, D, G),
	unfold_product(E, F, G, J),
	H=..[I, J, K].
attribute_goal_(pdifferent(A, C, B, F), all_different(E)) :-
	append(A, [B|C], D),
	msort(D, E),
	setarg(1, F, processed).
attribute_goal_(pdistinct(A, C, B, F), all_distinct(E)) :-
	append(A, [B|C], D),
	msort(D, E),
	setarg(1, F, processed).
attribute_goal_(pserialized(B, C, A, D), serialized(E, F)) :-
	append(A, [B-C|D], G),
	pair_up(E, F, G).
attribute_goal_(rel_tuple(mutable(B, _), A), tuples_in([A], B)).
attribute_goal_(pzcompare(A, B, C), zcompare(A, B, C)).
attribute_goal_(reified_in(A, C, B), #<==>(in(A, D), B)) :-
	domain_to_drep(C, D).
attribute_goal_(reified_fd(A, B), #<==>(finite_domain(A), B)).
attribute_goal_(reified_neq(A, C, B, D, E), #<==>(#/\(#/\(A, B), #\=(C, D)), E)).
attribute_goal_(reified_eq(A, C, B, D, E), #<==>(#/\(#/\(A, B), #=(C, D)), E)).
attribute_goal_(reified_geq(A, C, B, D, E), #<==>(#/\(#/\(A, B), #>=(C, D)), E)).
attribute_goal_(reified_div(B, C, A, D), (#==>(#=(A, 1), #=(B/C, D)), #==>(#\=(C, 0), #=(A, 1)))).
attribute_goal_(reified_mod(B, C, A, D), (#==>(#=(A, 1), #=(B mod C, D)), #==>(#\=(C, 0), #=(A, 1)))).
attribute_goal_(por(A, B, C), #<==>(#\/(A, B), C)).
attribute_goal_(reified_and(A, B, C), #<==>(#/\(A, B), C)).
attribute_goal_(reified_or(A, B, C), #<==>(#\/(A, B), C)).
attribute_goal_(reified_not(A, B), #<==>(#\(A), B)).
attribute_goal_(pimpl(A, B), #==>(A, B)).


bound_portray(inf, inf).
bound_portray(sup, sup).
bound_portray(n(A), A).


cis_abs(inf, sup).
cis_abs(sup, sup).
cis_abs(n(B), n(A)) :-
	A is abs(B).

cis_geq(A, B) :-
	(compound(A) ->
	    (compound(B) ->
		A = n(X), B = n(Y),
		X >= Y
	    ;
		B == inf
	    )
	;
	    (A == sup ->
		true
	    ;
		B == inf
	    )
	).


% Bart
cis_gt(A,B) :-
	(compound(A) ->
	    (compound(B) ->
		A = n(X), B = n(Y),
		X > Y
	    ;
		B == inf
	    )
	;
	    A == sup,
	    B \== sup
	).

cis_gtn(n(A),X) :- A > X, !.
cis_gtn(sup,_).

cis_leq(B, A) :-
	cis_geq(A, B).

cis_lt(B, A) :-
	cis_gt(A, B).

cis_sign(sup, n(1)).
cis_sign(inf, n(-1)).
cis_sign(n(A), n(B)) :-
	(   A<0
	->  B= -1
	;   A>0
	->  B=1
	;   B=0
	).

cis_uminus(inf, sup).
cis_uminus(sup, inf).
cis_uminus(n(B), n(A)) :-
	A is -B.

clpfd_equal(A, B) :-
	(   left_right_linsum_const(A, B, C, H, D)
	->  (   C=[]
	    ->  D=:=0
	    ;   C=[F|E],
		gcd(E, F, G),
		D mod G=:=0,
		scalar_product(C, H, #=, D)
	    )
	;   v_or_i(B)
	->  parse_clpfd(A, B),
	    reinforce(B)
	;   v_or_i(A)
	->  parse_clpfd(B, A),
	    reinforce(A)
	;   parse_clpfd(A, I),
	    parse_clpfd(B, I),
	    reinforce(I)
	).

clpfd_geq(A, B) :-
	clpfd_geq_(A, B),
	reinforce(A),
	reinforce(B).

clpfd_geq_(A, B) :-
	(   var(A),
	    nonvar(B),
	    B=C-D,
	    var(C),
	    integer(D)
	->  var_leq_var_plus_const(C, A, D)
	;   var(A),
	    nonvar(B),
	    B=C+D,
	    var(C),
	    integer(D)
	->  E is -D,
	    var_leq_var_plus_const(C, A, E)
	;   nonvar(A),
	    var(B),
	    A=F+D,
	    var(F),
	    integer(D)
	->  var_leq_var_plus_const(B, F, D)
	;   nonvar(A),
	    var(B),
	    A=F-D,
	    var(F),
	    integer(D)
	->  E is -D,
	    var_leq_var_plus_const(B, F, E)
	;   nonvar(B),
	    B=H+G,
	    G==1,
	    integer(H)
	->  C is H+1,
	    clpfd_geq_(A, C)
	;   integer(A),
	    nonvar(B),
	    B=H+G,
	    G==1
	->  F is A-1,
	    clpfd_geq_(F, H)
	;   parse_clpfd(A, I),
	    parse_clpfd(B, J),
	    geq(I, J)
	).

consistency(upto_in(A), upto_in(1, A)).
consistency(upto_ground, upto_ground).

constraint_wake(pneq, ground).
constraint_wake(x_neq_y_plus_z, ground).
constraint_wake(absdiff_neq, ground).
constraint_wake(pdifferent, ground).
constraint_wake(pdistinct, ground).
constraint_wake(x_leq_y_plus_c, bounds).
constraint_wake(scalar_product, bounds).
constraint_wake(pplus, bounds).
constraint_wake(pgeq, bounds).


defaulty_to_bound(A, B) :-
	(   integer(A)
	->  B=n(A)
	;   B=A
	).

domain(A, B) :-
	(   fd_get(A, C, E)
	->  domains_intersection(B, C, D),
	    fd_put(A, D, E),
	    do_queue,
	    reinforce(A)
	;   domain_contains(B, A)
	).

domain_complement(B, C) :-
	default_domain(A),
	domain_subtract(A, B, C).

domain_contains(from_to(A, C), B) :-
	domain_contains_from(A, B),
	domain_contains_to(C, B).
domain_contains(split(B, C, D), A) :-
	(   A<B
	->  domain_contains(C, A)
	;   A>B
	->  domain_contains(D, A)
	).

domain_contains_from(inf, _).
domain_contains_from(n(A), B) :-
	A=<B.

domain_contains_to(sup, _).
domain_contains_to(n(B), A) :-
	A=<B.

domain_infimum(from_to(A, _), A).
domain_infimum(split(_, A, _), B) :-
	domain_infimum(A, B).

domain_intervals(A, B) :-
	domain_intervals(A, B, []). % phrase

domain_largest_finite(from_to(B, A), C) :-
	largest_finite(A, B, C).
domain_largest_finite(split(_, _, A), B) :-
	domain_largest_finite(A, B).

domain_negate(empty, empty).
domain_negate(from_to(A, C), from_to(D, B)) :-
	cis_uminus(A, B),
	cis_uminus(C, D).
domain_negate(split(B, C, E), split(A, F, D)) :-
	A is -B,
	domain_negate(C, D),
	domain_negate(E, F).

domain_num_elements(empty, n(0)).
domain_num_elements(from_to(B, A), D) :-
	cis_minus(A, B, C),
	cis_plus(C, n(1), D).
domain_num_elements(split(_, A, B), E) :-
	domain_num_elements(A, C),
	domain_num_elements(B, D),
	cis_plus(C, D, E).

domain_smallest_finite(from_to(A, B), C) :-
	smallest_finite(A, B, C).
domain_smallest_finite(split(_, A, _), B) :-
	domain_smallest_finite(A, B).

domain_spread(A, D) :-
	domain_smallest_finite(A, C),
	domain_largest_finite(A, B),
	cis_minus(B, C, D).

domain_subdomain(A, B) :-
	domain_subdomain(A, A, B).

domain_subdomain_fromto(empty, _).
domain_subdomain_fromto(from_to(B, D), from_to(A, C)) :-
	cis_leq(A, B),
	cis_geq(C, D).
domain_subdomain_fromto(split(_, A, C), B) :-
	domain_subdomain_fromto(A, B),
	domain_subdomain_fromto(C, B).

domain_supremum(from_to(_, A), A).
domain_supremum(split(_, _, A), B) :-
	domain_supremum(A, B).

domain_to_drep(A, H) :-
	domain_intervals(A, [B-C|G]),
	bound_portray(B, D),
	bound_portray(C, E),
	(   D==E
	->  F=D
	;   F= ..(D, E)
	),
	intervals_to_drep(G, F, H).

domains([], _).
domains([A|C], B) :-
	domain(A, B),
	domains(C, B).

% Bart - this was really worth it :-)

drep_to_domain(..(I,J),F) :-
	integer(I),
	integer(J),
	I =< J,
	!,
	F = from_to(n(I),n(J)).
drep_to_domain(A, D) :-
	(   is_drep(A)
	->  true
	;   domain_error(clpfd_domain, A)
	),
	drep_to_intervals(A, B, []), % phrase
	merge_intervals(B, C),
	intervals_to_domain(C, D).


earliest_start_time(A, C) :-
	(   fd_get(A, B, _)
	->  domain_infimum(B, C)
	;   C=n(A)
	).

exclude_list([], _).
exclude_list([A|F], C) :-
	(   fd_get(A, B, E)
	->  domain_remove(B, C, D),
	    fd_put(A, D, E)
	;   A=\=C
	),
	exclude_list(F, C).

fd_dom(A, C) :-
	(   fd_get(A, B, _)
	->  domain_to_drep(B, C)
	;   must_be(integer, A),
	    C= ..(A, A)
	).

fd_inf(A, D) :-
	(   fd_get(A, B, _)
	->  domain_infimum(B, C),
	    bound_portray(C, D)
	;   must_be(integer, A),
	    D=A
	).

fd_size(A, D) :-
	(   fd_get(A, B, _)
	->  domain_num_elements(B, C),
	    bound_portray(C, D)
	;   must_be(integer, A),
	    D=1
	).

fd_sup(A, D) :-
	(   fd_get(A, B, _)
	->  domain_supremum(B, C),
	    bound_portray(C, D)
	;   must_be(integer, A),
	    D=A
	).

ff_lt(A, D) :-
	(   fd_get(A, B, _)
	->  domain_num_elements(B, n(C))
	;   C=1
	),
	(   fd_get(D, E, _)
	->  domain_num_elements(E, n(F))
	;   F=1
	),
	C<F.

ffc_lt(A, E) :-
	(   fd_get(A, B, D)
	->  domain_num_elements(B, n(C))
	;   C=1,
	    D=[]
	),
	(   fd_get(E, F, H)
	->  domain_num_elements(F, n(G))
	;   G=1,
	    H=[]
	),
	(   C<G
	->  true
	;   C=:=G,
	    props_number(D, I),
	    props_number(H, J),
	    I>J
	).

geq(A, C) :-
	(   fd_get(A, B, I)
	->  domain_infimum(B, E),
	    (   fd_get(C, D, _)
	    ->  domain_supremum(D, F),
		(   cis_geq(E, F)
		->  true
		;   make_propagator(pgeq(A, C), G),
		    init_propagator(A, G),
		    init_propagator(C, G),
		    trigger_once(G)
		)
	    ;   domain_remove_smaller_than(B, C, H),
		fd_put(A, H, I),
		do_queue
	    )
	;   fd_get(C, D, K)
	->  domain_remove_greater_than(D, A, J),
	    fd_put(C, J, K),
	    do_queue
	;   A>=C
	).

goal_expansion(cis(B, A), D) :-
	cis_goals(A, B, C, []), % phrase
	list_goal(C, D).

goals_entail(A, B) :-
	must_be(list, A),
	\+ (   maplist(call, A),
	   #\(B),
	   term_variables(A-B, C),
	   label(C)
	   ).

in(A, B) :-
	fd_variable(A),
	drep_to_domain(B, C),
	domain(A, C).

init_propagator(A, B) :-
	(   fd_get(A, D, C)
	->  insert_propagator(B, C, E),
	    fd_put(A, D, E)
	;   true
	).

ins(A, B) :-
	must_be(list, A),
	maplist(fd_variable, A),
	drep_to_domain(B, C),
	domains(A, C).

% please steadfast ! Bart Demoen

intervals_to_domain([], Out) :- ! , Out = empty.
intervals_to_domain([A-B], Out) :- !, Out = from_to(A, B).
intervals_to_domain(A, G) :-
	length(A, B),
	C is B//2,
	length(D, C),
	append(D, E, A),
	E=[n(F)-_|_],
	H is F-1,
	intervals_to_domain(D, I),
	intervals_to_domain(E, J),
	G=split(H, I, J).

labeling(A, B) :-
	must_be(list, A),
	must_be(list, B),
	maplist(finite_domain, B),
	label(A, A, default(leftmost), default(up), default(step), [], upto_ground, B).

latest_start_time(A, C) :-
	(   fd_get(A, B, _)
	->  domain_supremum(B, C)
	;   C=n(A)
	).

lex_chain_([], _).
lex_chain_([A|C], B) :-
	lex_check_and_attach(A, B),
	lex_chain_lag(C, A),
	lex_chain_(C, B).

lex_chain_lag([], _).
lex_chain_lag([B|C], A) :-
	lex_le(A, B),
	lex_chain_lag(C, B).

lex_check_and_attach([], _).
lex_check_and_attach([A|C], B) :-
	fd_variable(A),
	(   var(A)
	->  init_propagator(A, B)
	;   true
	),
	lex_check_and_attach(C, B).

lex_le([], []).
lex_le([B|C], [A|D]) :-
	(   integer(A),
	    integer(B)
	->  A>=B
	;   clpfd_geq(A, B)
	),
	(   integer(B)
	->  (   integer(A)
	    ->  (   B=:=A
		->  lex_le(C, D)
		;   true
		)
	    ;   freeze(A, lex_le([B|C], [A|D]))
	    )
	;   freeze(B, lex_le([B|C], [A|D]))
	).

linterm_negate(vn(A, C), vn(A, B)) :-
	B is -C.

list_contains([A|C], B) :-
	(   A==B
	->  true
	;   list_contains(C, B)
	).

list_goal([], true).
list_goal([B|A], C) :-
	list_goal_(A, B, C).

list_to_disjoint_intervals([], []).
list_to_disjoint_intervals([B|A], C) :-
	list_to_disjoint_intervals(A, B, B, C).

list_to_domain(A, B) :-
	(   A==[]
	->  B=empty
	;   sort(A, C),
	    list_to_disjoint_intervals(C, D),
	    intervals_to_domain(D, B)
	).

make_propagator(A, propagator(A, mutable(passive, _))).

max_gt(A, B) :-
	bounds(A, _, C),
	bounds(B, _, D),
	C>D.

merge_intervals(A, C) :-
	keysort(A, B),
	merge_overlapping(B, C).

merge_overlapping([], []).
merge_overlapping([A-C|B], [A-D|F]) :-
	merge_remaining(B, C, D, E),
	merge_overlapping(E, F).

min_lt(A, B) :-
	bounds(A, C, _),
	bounds(B, D, _),
	C<D.

myabs(A, B) :-
	make_propagator(pabs(A, B), C),
	init_propagator(A, C),
	init_propagator(B, C),
	trigger_prop(C),
	trigger_once(C).

myimpl(A, B) :-
	make_propagator(pimpl(A, B), C),
	init_propagator(A, C),
	init_propagator(B, C),
	trigger_prop(C).

neq(A, B) :-
	make_propagator(pneq(A, B), C),
	init_propagator(A, C),
	init_propagator(B, C),
	trigger_once(C).

neq_num(A, C) :-
	(   fd_get(A, B, E)
	->  domain_remove(B, C, D),
	    fd_put(A, D, E)
	;   A=\=C
	).

parse_clpfd(A, B) :-
	(   cyclic_term(A)
	->  domain_error(clpfd_expression, A)
	;   var(A)
	->  constrain_to_integer(A),
	    B=A
	;   integer(A)
	->  B=A
	;   A=C+D
	->  parse_clpfd(C, E),
	    parse_clpfd(D, F),
	    myplus(E, F, B)
	;   power_var_num(A, G, H)
	->  #=(G^H, B)
	;   A=C*D
	->  parse_clpfd(C, E),
	    parse_clpfd(D, F),
	    mytimes(E, F, B)
	;   A=C-D
	->  parse_clpfd(C, E),
	    parse_clpfd(D, F),
	    mytimes(-1, F, I),
	    myplus(E, I, B)
	;   A= -J
	->  parse_clpfd(J, K),
	    mytimes(-1, K, B)
	;   A=max(C, D)
	->  parse_clpfd(C, E),
	    parse_clpfd(D, F),
	    mymax(E, F, B)
	;   A=min(C, D)
	->  parse_clpfd(C, E),
	    parse_clpfd(D, F),
	    mymin(E, F, B)
	;   A=C mod D
	->  parse_clpfd(C, E),
	    parse_clpfd(D, F),
	    #\=(F, 0),
	    mymod(E, F, B)
	;   A=abs(J)
	->  parse_clpfd(J, K),
	    myabs(K, B),
	    #>=(B, 0)
	;   A=C/D
	->  parse_clpfd(C, E),
	    parse_clpfd(D, F),
	    #\=(F, 0),
	    mydiv(E, F, B)
	;   A=C^D
	->  parse_clpfd(C, E),
	    parse_clpfd(D, F),
	    myexp(E, F, B)
	;   domain_error(clpfd_expression, A)
	).

portray_propagator(propagator(A, _), B) :-
	functor(A, B, _).

portray_queue(A, []) :-
	var(A), !.
portray_queue([A|C], [B|D]) :-
	portray_propagator(A, B),
	portray_queue(C, D).

props_number(fd_props(A, B, C), D) :-
	length(A, E),
	length(B, F),
	length(C, G),
	D is E+F+G.

reduce_from_others([], _).
reduce_from_others([A|F], C) :-
	(   fd_get(A, B, E)
	->  domain_subtract(B, C, D),
	    fd_put(A, D, E)
	;   true
	),
	reduce_from_others(F, C).

reify(B, A) :-
	in(A, ..(0, 1)),
	(   cyclic_term(B)
	->  domain_error(clpfd_reifiable_expression, B)
	;   var(B)
	->  A=B
	;   integer(B)
	->  A=B
	;   B=in(D, C)
	->  drep_to_domain(C, E),
	    fd_variable(D),
	    make_propagator(reified_in(D, E, A), F),
	    init_propagator(D, F),
	    init_propagator(A, F),
	    trigger_prop(F)
	;   B=finite_domain(D)
	->  fd_variable(D),
	    make_propagator(reified_fd(D, A), F),
	    init_propagator(D, F),
	    init_propagator(A, F),
	    trigger_prop(F)
	;   B= #>=(G, H)
	->  parse_reified_clpfd(G, J, I),
	    parse_reified_clpfd(H, L, K),
	    make_propagator(reified_geq(I, J, K, L, A), F),
	    init_propagator(J, F),
	    init_propagator(L, F),
	    init_propagator(A, F),
	    init_propagator(I, F),
	    init_propagator(K, F),
	    trigger_prop(F)
	;   B= #>(G, H)
	->  reify(#>=(G, H+1), A)
	;   B= #=<(G, H)
	->  reify(#>=(H, G), A)
	;   B= #<(G, H)
	->  reify(#>=(H, G+1), A)
	;   B= #=(G, H)
	->  parse_reified_clpfd(G, J, I),
	    parse_reified_clpfd(H, L, K),
	    make_propagator(reified_eq(I, J, K, L, A), F),
	    init_propagator(J, F),
	    init_propagator(L, F),
	    init_propagator(A, F),
	    init_propagator(I, F),
	    init_propagator(K, F),
	    trigger_prop(F)
	;   B= #\=(G, H)
	->  parse_reified_clpfd(G, J, I),
	    parse_reified_clpfd(H, L, K),
	    make_propagator(reified_neq(I, J, K, L, A), F),
	    init_propagator(J, F),
	    init_propagator(L, F),
	    init_propagator(A, F),
	    init_propagator(I, F),
	    init_propagator(K, F),
	    trigger_prop(F)
	;   B= #==>(G, H)
	->  reify(#\/(#\(G), H), A)
	;   B= #<==(G, H)
	->  reify(#==>(H, G), A)
	;   B= #<==>(G, H)
	->  reify(#/\(#==>(G, H), #==>(H, G)), A)
	;   B= #/\(G, H)
	->  reify(G, J),
	    reify(H, L),
	    make_propagator(reified_and(J, L, A), F),
	    init_propagator(J, F),
	    init_propagator(L, F),
	    init_propagator(A, F),
	    trigger_prop(F)
	;   B= #\/(G, H)
	->  reify(G, J),
	    reify(H, L),
	    make_propagator(reified_or(J, L, A), F),
	    init_propagator(J, F),
	    init_propagator(L, F),
	    init_propagator(A, F),
	    trigger_prop(F)
	;   B= #\(M)
	->  reify(M, N),
	    make_propagator(reified_not(N, A), F),
	    init_propagator(N, F),
	    init_propagator(A, F),
	    trigger_prop(F)
	;   domain_error(clpfd_reifiable_expression, B)
	).

remove_dist_lower([], _).
remove_dist_lower([B*A|K], E) :-
	(   fd_get(A, C, J)
	->  (   B<0
	    ->  (   domain_infimum(C, n(D))
		->  F is D-E//B,
		    domain_remove_greater_than(C, F, G)
		;   G=C
		)
	    ;   domain_supremum(C, n(H))
	    ->  I is H-E//B,
		domain_remove_smaller_than(C, I, G)
	    ;   G=C
	    ),
	    fd_put(A, G, J)
	;   true
	),
	remove_dist_lower(K, E).

remove_dist_upper([], _).
remove_dist_upper([B*A|K], E) :-
	(   fd_get(A, C, J)
	->  (   B<0
	    ->  (   domain_supremum(C, n(D))
		->  F is D+E//B,
		    domain_remove_smaller_than(C, F, G)
		;   G=C
		)
	    ;   domain_infimum(C, n(H))
	    ->  I is H+E//B,
		domain_remove_greater_than(C, I, G)
	    ;   G=C
	    ),
	    fd_put(A, G, J)
	;   true
	),
	remove_dist_upper(K, E).

remove_lower([], _).
remove_lower([C*A|H], B) :-
	(   fd_get(A, D, G)
	->  E is -B//C,
	    (   C<0
	    ->  domain_remove_greater_than(D, E, F)
	    ;   domain_remove_smaller_than(D, E, F)
	    ),
	    fd_put(A, F, G)
	;   true
	),
	remove_lower(H, B).

remove_upper([], _).
remove_upper([C*A|H], B) :-
	(   fd_get(A, D, G)
	->  E is B//C,
	    (   C<0
	    ->  domain_remove_smaller_than(D, E, F)
	    ;   domain_remove_greater_than(D, E, F)
	    ),
	    fd_put(A, F, G)
	;   true
	),
	remove_upper(H, B).

% run_propagator(F,_) :- functor(F,N,A), writeln(N/A), fail.
run_propagator(presidual(_), _).
run_propagator(pdifferent(B, C, A, _), _) :-
	(   ground(A)
	->  disable_queue,
	    exclude_fire(B, C, A),
	    enable_queue
	;   true
	).
run_propagator(pdistinct(B, C, A, _), _) :-
	(   ground(A)
	->  disable_queue,
	    exclude_fire(B, C, A),
	    enable_queue
	;   true
	).
run_propagator(check_distinct(A, C, B), _) :-
	\+ list_contains(A, B),
	\+ list_contains(C, B).
run_propagator(pneq(A, B), C) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  A=\=B,
		kill(C)
	    ;   fd_get(B, D, F),
		domain_remove(D, A, E),
		kill(C),
		fd_put(B, E, F)
	    )
	;   nonvar(B)
	->  run_propagator(pneq(B, A), C)
	;   A\==B,
	    fd_get(A, _, I, G, _),
	    fd_get(B, _, H, J, _),
	    (   cis_lt(G, H)
	    ->  kill(C)
	    ;   cis_gt(I, J)
	    ->  kill(C)
	    ;   true
	    )
	).
run_propagator(pgeq(A, B), C) :-
	(   A==B
	->  kill(C)
	;   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		A>=B
	    ;   fd_get(B, D, F),
		domain_remove_greater_than(D, A, E),
		kill(C),
		fd_put(B, E, F)
	    )
	;   nonvar(B)
	->  fd_get(A, G, I),
	    domain_remove_smaller_than(G, B, H),
	    kill(C),
	    fd_put(A, H, I)
	;   fd_get(A, G, L, J, I),
	    fd_get(B, _, K, M, _),
	    cis_geq(J, K),
	    (   cis_geq(L, M)
	    ->  kill(C)
	    ;   J==K
	    ->  kill(C),
		A=B
	    ;   cis_max(L, K, N),
		domains_intersection(G, from_to(N, J), O),
		fd_put(A, O, I),
		(   fd_get(B, Q, R, P, U)
		->  cis_min(P, J, S),
		    domains_intersection(Q, from_to(R, S), T),
		    fd_put(B, T, U)
		;   true
		)
	    )
	).
run_propagator(rel_tuple(A, B), C) :-
	arg(1, A, D),
	(   ground(B)
	->  kill(C),
	    memberchk(B, D)
	;   relation_unifiable(D, B, E, 0, I),
	    E=[_|_],
	    (   B=[F, G],
		(   ground(F)
		;   ground(G)
		)
	    ->  kill(C)
	    ;   true
	    ),
	    (   E=[H]
	    ->  kill(C),
		H=B
	    ;   I=:=0
	    ->  true
	    ;   setarg(1, A, E),
		disable_queue,
		tuple_domain(B, E),
		enable_queue
	    )
	).
run_propagator(pserialized(D, A, B, C), _) :-
	myserialized(A, B, C, D).
run_propagator(absdiff_neq(A, B, D), C) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		abs(A-B)=\=D
	    ;   kill(C),
		E is A-D,
		neq_num(B, E),
		F is D+A,
		neq_num(B, F)
	    )
	;   nonvar(B)
	->  kill(C),
	    E is D+B,
	    neq_num(A, E),
	    F is B-D,
	    neq_num(A, F)
	;   true
	).
run_propagator(x_neq_y_plus_z(A, B, C), D) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  (   nonvar(C)
		->  kill(D),
		    A=\=B+C
		;   kill(D),
		    E is A-B,
		    neq_num(C, E)
		)
	    ;   nonvar(C)
	    ->  kill(D),
		F is A-C,
		neq_num(B, F)
	    ;   true
	    )
	;   nonvar(B)
	->  (   nonvar(C)
	    ->  kill(D),
		G is B+C,
		neq_num(A, G)
	    ;   true
	    )
	;   true
	).
run_propagator(x_leq_y_plus_c(A, B, D), C) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		A=<B+D
	    ;   kill(C),
		F is A-D,
		fd_get(B, E, H),
		domain_remove_smaller_than(E, F, G),
		fd_put(B, G, H)
	    )
	;   nonvar(B)
	->  kill(C),
	    F is B+D,
	    fd_get(A, I, K),
	    domain_remove_greater_than(I, F, J),
	    fd_put(A, J, K)
	;   fd_get(B, E, _),
	    (   domain_supremum(E, n(L))
	    ->  M is L+D,
		fd_get(A, I, K),
		domain_remove_greater_than(I, M, J),
		fd_put(A, J, K)
	    ;   true
	    ),
	    (   fd_get(A, N, _),
		domain_infimum(N, n(O))
	    ->  P is O-D,
		(   fd_get(B, G, T)
		->  domain_remove_smaller_than(G, P, Q),
		    (   domain_infimum(Q, n(S)),
			domain_supremum(N, n(R)),
			R=<S+D
		    ->  kill(C)
		    ;   true
		    ),
		    fd_put(B, Q, T)
		;   true
		)
	    ;   true
	    )
	).
run_propagator(scalar_product(A, B, E, C), G) :-
	coeffs_variables_const(A, B, I, F, 0, D),
	H is C-D,
	(   E== (#\=)
	->  (   F=[]
	    ->  kill(G),
		H=\=0
	    ;   H=:=0,
		I=[1, 1, -1]
	    ->  kill(G),
		F=[K, L, J],
		x_neq_y_plus_z(J, K, L)
	    ;   I==[1, -1]
	    ->  kill(G),
		F=[K, L],
		x_neq_y_plus_z(K, L, H)
	    ;   I==[-1, 1]
	    ->  kill(G),
		F=[K, L],
		x_neq_y_plus_z(L, K, H)
	    ;   F=[M],
		I=[J]
	    ->  kill(G),
		(   J=:=1
		->  neq_num(M, H)
		;   #\=(J*M, H)
		)
	    ;   true
	    )
	;   E== (#=)
	->  (   F=[]
	    ->  kill(G),
		H=:=0
	    ;   F=[M],
		I=[J]
	    ->  kill(G),
		H mod J=:=0,
		M is H//J
	    ;   I==[1, 1]
	    ->  kill(G),
		F=[K, L],
                (   integer(H)
		->  (   fail % var(K+L)
		    ->  H is K + L
		    ;   integer(K),
			integer(L)
		    ->  K+L=:=H
		    ;   clpfd_equal(K+L, H)
		    )
		;   clpfd_equal(K+L, H)
		)
	    ;   I==[-1, 1]
	    ->  kill(G),
		F=[K, L],
		(   integer(H),
		    integer(K)
		->  (   var(L)
		    ->  L is H+K
		    ;   integer(L)
		    ->  L=:=H+K
		    ;   clpfd_equal(L, H+K)
		    )
		;   clpfd_equal(L, H+K)
		)
	    ;   I==[1, -1]
	    ->  kill(G),
		F=[K, L],
		(   integer(H),
		    integer(L)
		->  (   var(K)
		    ->  K is H+L
		    ;   integer(K)
		    ->  K=:=H+L
		    ;   clpfd_equal(K, H+L)
		    )
		;   clpfd_equal(K, H+L)
		)
	    ;   H=:=0,
		I==[1, 1, -1]
	    ->  kill(G),
		F=[K, L, J],
               (   integer(J)
               ->  (   fail % var(K+L)
                   ->  fail
                   ;   integer(K),
		       integer(L)
		   ->  K+L=:=J
		   ;   clpfd_equal(K+L, J)
		   )
	       ;   clpfd_equal(K+L, J)
	       )
	    ;   H=:=0,
		I==[1, -1, 1]
	    ->  kill(G),
		F=[K, L, J],
		(   integer(L)
		->  (   fail % var(K+J)
		    ->  fail % K+J is L
		    ;   integer(K),
			integer(J)
		    ->  K+J=:=L
		    ;   clpfd_equal(K+J, L)
		    )
		;   clpfd_equal(K+J, L)
		)
	    ;   H=:=0,
		I==[-1, 1, 1]
	    ->  kill(G),
		F=[K, L, J],
		(   integer(K)
		->  (   fail % var(L+J)
		    ->  fail % L+J is K
		    ;   integer(L),
			integer(J)
		    ->  L+J=:=K
		    ;   clpfd_equal(L+J, K)
		    )
		;   clpfd_equal(L+J, K)
		)
	    ;   sum_finite_domains(I, F, P, Q, 0, 0, N, O),
		R is H-N,
		S is O-H,
		(   P==[],
		    Q==[]
		->  between(N, O, H),
		    remove_dist_upper_lower(I, F, R, S)
		;   Q=[]
		->  H=<O,
		    remove_dist_lower(P, S)
		;   P=[]
		->  N=<H,
		    remove_dist_upper(Q, R)
		;   Q=[_],
		    P=[_]
		->  remove_lower(Q, S),
		    remove_upper(P, R)
		;   P=[_]
		->  remove_upper(P, R)
		;   Q=[_]
		->  remove_lower(Q, S)
		;   true
		)
	    )
	).
run_propagator(pplus(A, C, D), B) :-
	(   nonvar(A)
	->  (   A=:=0
	    ->  kill(B),
		C=D
	    ;   nonvar(C)
	    ->  kill(B),
		D is A+C
	    ;   nonvar(D)
	    ->  kill(B),
		C is D-A
	    ;   fd_get(D, F, I),
		fd_get(C, E, _),
		domain_shift(E, A, G),
		domains_intersection(F, G, H),
		fd_put(D, H, I),
		(   fd_get(C, K, N)
		->  J is -A,
		    domain_shift(H, J, L),
		    domains_intersection(K, L, M),
		    fd_put(C, M, N)
		;   true
		)
	    )
	;   nonvar(C)
	->  run_propagator(pplus(C, A, D), B)
	;   nonvar(D)
	->  (   A==C
	    ->  kill(B),
		D mod 2=:=0,
		A is D//2
	    ;   fd_get(A, O, _),
		fd_get(C, E, N),
		domain_negate(O, P),
		domain_shift(P, D, K),
		domains_intersection(E, K, L),
		fd_put(C, L, N),
		(   fd_get(A, R, U)
		->  domain_negate(L, Q),
		    domain_shift(Q, D, S),
		    domains_intersection(R, S, T),
		    fd_put(A, T, U)
		;   true
		)
	    )
	;   A==C
	->  kill(B),
	    (   integer(D)
	    ->  (   fail % var(2*A)
		->  fail % 2*A is D
		;   integer(A)
		->  2*A=:=D
		;   clpfd_equal(2*A, D)
		)
	    ;   clpfd_equal(2*A, D)
	    )
	;   fd_get(A, O, X, B1, U),
	    fd_get(C, E, A1, W, N),
	    fd_get(D, F, V, Z, _)
	->  cis_minus(V, W, Y),
	    cis_max(X, Y, D1),
	    cis_minus(Z, A1, C1),
	    cis_min(B1, C1, E1),
	    (   D1==X,
		E1==B1
	    ->  true
	    ;   domains_intersection(O, from_to(D1, E1), F1),
		fd_put(A, F1, U)
	    ),
	    (   fd_get(C, L, G1, I1, N1)
	    ->  cis_minus(V, E1, H1),
		cis_max(G1, H1, K1),
		cis_minus(Z, D1, J1),
		cis_min(I1, J1, L1),
		(   K1==G1,
		    L1==I1
		->  true
		;   domains_intersection(L, from_to(K1, L1), M1),
		    fd_put(C, M1, N1)
		)
	    ;   K1=C,
		L1=C
	    ),
	    (   fd_get(D, U1, O1, Q1, W1)
	    ->  cis_plus(D1, K1, P1),
		cis_max(O1, P1, S1),
		cis_plus(E1, L1, R1),
		cis_min(Q1, R1, T1),
		(   S1==O1,
		    T1==Q1
		->  true
		;   domains_intersection(U1, from_to(S1, T1), V1),
		    fd_put(D, V1, W1)
		)
	    ;   true
	    )
	;   true
	).
run_propagator(ptimes(A, B, D), C) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		D is A*B
	    ;   A=:=0
	    ->  kill(C),
		D=0
	    ;   A=:=1
	    ->  kill(C),
		D=B
	    ;   nonvar(D)
	    ->  kill(C),
		0=:=D mod A,
		B is D//A
	    ;   fd_get(B, E, _),
		fd_get(D, F, I),
		domain_expand(E, A, G),
		domains_intersection(F, G, H),
		fd_put(D, H, I),
		(   fd_get(B, J, M)
		->  domain_contract(H, A, K),
		    domains_intersection(J, K, L),
		    fd_put(B, L, M)
		;   kill(C),
		    D is A*B
		)
	    )
	;   nonvar(B)
	->  run_propagator(ptimes(B, A, D), C)
	;   nonvar(D)
	->  (   A==B
	    ->  kill(C),
		integer_kth_root(D, 2, N),
		O is -N,
		in(A, O\/N)
	    ;   fd_get(A, X, R, T, Z),
		fd_get(B, E, P, Q, _),
		min_divide(n(D), n(D), P, Q, S),
		max_divide(n(D), n(D), P, Q, U),
		cis_max(R, S, V),
		cis_min(T, U, W),
		(   V==R,
		    W==T
		->  true
		;   domains_intersection(X, from_to(V, W), Y),
		    fd_put(A, Y, Z)
		),
		(   fd_get(B, E1, B1, D1, G1)
		->  min_divide(n(D), n(D), V, W, A1),
		    max_divide(n(D), n(D), V, W, C1),
		    (   cis_leq(A1, B1),
			cis_geq(C1, D1)
		    ->  true
		    ;   domains_intersection(E1, from_to(A1, C1), F1),
			fd_put(B, F1, G1)
		    )
		;   B\==0
		->  0=:=D mod B,
		    kill(C),
		    A is D//B
		;   kill(C),
		    D=0
		)
	    ),
	    (   D=\=0
	    ->  neq_num(A, 0),
		neq_num(B, 0)
	    ;   true
	    )
	;   A==B
	->  kill(C),
	    (   integer(D)
	    ->  (   fail % var(A^2)
		->  fail % A^2 =:= D
		;   integer(A),
		    (   2>=0
		    ;   A=:= -1
		    )
		->  A^2=:=D
		;   clpfd_equal(A^2, D)
		)
	    ;   clpfd_equal(A^2, D)
	    )
	;   fd_get(A, X, R, T, L1),
	    fd_get(B, E, P, Q, _),
	    fd_get(D, F, H1, I1, _),
	    min_divide(H1, I1, P, Q, J1),
	    cis_max(R, J1, V),
	    max_divide(H1, I1, P, Q, K1),
	    cis_min(T, K1, W),
	    (   V==R,
		W==T
	    ->  true
	    ;   domains_intersection(X, from_to(V, W), Y),
		fd_put(A, Y, L1)
	    ),
	    (   fd_get(B, E1, B1, D1, G1)
	    ->  min_divide(H1, I1, R, T, M1),
		cis_max(B1, M1, A1),
		max_divide(H1, I1, R, T, N1),
		cis_min(D1, N1, C1),
		(   A1==B1,
		    C1==D1
		->  true
		;   domains_intersection(E1, from_to(A1, C1), F1),
		    fd_put(B, F1, G1)
		)
	    ;   A1=B,
		C1=B
	    ),
	    (   fd_get(D, S1, P1, R1, U1)
	    ->  min_times(V, W, A1, C1, O1),
		max_times(V, W, A1, C1, Q1),
		(   cis_leq(O1, P1),
		    cis_geq(Q1, R1)
		->  true
		;   domains_intersection(S1, from_to(O1, Q1), T1),
		    fd_put(D, T1, U1)
		)
	    ;   true
	    )
	).
run_propagator(pdiv(A, B, D), C) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		B=\=0,
		D is A//B
	    ;   fd_get(B, E, N, P, I),
		(   nonvar(D)
		->  (   D=:=0
		    ->  F is -abs(A)-1,
			G is abs(A)+1,
			domains_intersection(E, split(0, from_to(inf, n(F)), from_to(n(G), sup)), H),
			fd_put(B, H, I)
		    ;   (   sign(A)=:=sign(D)
			->  cis_sign(n(D), J),
			    cis_plus(n(D), J, K),
			    cis_slash(n(A), K, L),
			    cis_plus(L, n(1), M),
			    cis_max(M, N, F),
			    cis_slash(n(A), n(D), O),
			    cis_min(O, P, G)
			;   cis_slash(n(A), n(D), Q),
			    cis_max(Q, N, F),
			    cis_sign(n(D), R),
			    cis_plus(n(D), R, S),
			    cis_slash(n(A), S, T),
			    cis_minus(T, n(1), U),
			    cis_min(U, P, G)
			),
			(   F=N,
			    G=P
			->  true
			;   domains_intersection(E, from_to(F, G), H),
			    fd_put(B, H, I)
			)
		    )
		;   fd_get(D, E1, W, Y, G1),
		    (   A>=0,
			cis_gtn(N,0) % cis_gt(N, n(0))
		    ->  cis_slash(n(A), P, V),
			cis_max(V, W, B1),
			cis_slash(n(A), N, X),
			cis_min(X, Y, D1)
		    ;   cis_abs(n(A), Z),
			cis_uminus(Z, A1),
			cis_max(A1, W, B1),
			cis_abs(n(A), C1),
			cis_min(C1, Y, D1)
		    ),
		    (   B1=W,
			D1=Y
		    ->  true
		    ;   domains_intersection(E1, from_to(B1, D1), F1),
			fd_put(D, F1, G1)
		    )
		)
	    )
	;   nonvar(B)
	->  B=\=0,
	    (   B=:=1
	    ->  kill(C),
		A=D
	    ;   B=:= -1
	    ->  kill(C),
		(   integer(A)
		->  (   var(D)
		    ->  D is -A
		    ;   integer(D)
		    ->  D=:= -A
		    ;   clpfd_equal(D, -A)
		    )
		;   clpfd_equal(D, -A)
		)
	    ;   fd_get(A, C2, I1, O1, E2),
		(   nonvar(D)
		->  (   sign(D)=:=sign(B)
		    ->  cis_times(n(D), n(B), H1),
			cis_max(H1, I1, S1),
			cis_abs(n(D), J1),
			cis_plus(J1, n(1), K1),
			cis_abs(n(B), L1),
			cis_times(K1, L1, M1),
			cis_minus(M1, n(1), N1),
			cis_min(N1, O1, V1)
		    ;   D=:=0
		    ->  cis_abs(n(B), P1),
			cis_uminus(P1, Q1),
			cis_plus(Q1, n(1), R1),
			cis_max(R1, I1, S1),
			cis_abs(n(B), T1),
			cis_minus(T1, n(1), U1),
			cis_min(U1, O1, V1)
		    ;   cis_sign(n(D), W1),
			cis_times(W1, n(1), X1),
			cis_plus(n(D), X1, Y1),
			cis_times(Y1, n(B), Z1),
			cis_plus(Z1, n(1), A2),
			cis_max(A2, I1, S1),
			cis_times(n(D), n(B), B2),
			cis_min(B2, O1, V1)
		    ),
		    (   S1==I1,
			V1==O1
		    ->  true
		    ;   domains_intersection(C2, from_to(S1, V1), D2),
			fd_put(A, D2, E2)
		    )
		;   fd_get(D, E1, G1),
		    domain_contract_less(C2, B, F2),
		    domains_intersection(E1, F2, F1),
		    fd_put(D, F1, G1),
		    (   \+ domain_contains(F1, 0),
			fd_get(A, G2, J2)
		    ->  domain_expand_more(F1, B, H2),
			domains_intersection(G2, H2, I2),
			fd_put(A, I2, J2)
		    ;   true
		    )
		)
	    )
	;   nonvar(D)
	->  fd_get(A, C2, I1, O1, E2),
	    fd_get(B, E, N, P, I),
	    (   cis_geq(N, n(0)),
		cis_geq(I1, n(0))
	    ->  cis_times(N, n(D), K2),
		cis_max(K2, I1, S1),
		cis_plus(n(D), n(1), L2),
		cis_times(P, L2, M2),
		cis_minus(M2, n(1), N2),
		cis_min(N2, O1, V1)
	    ;   S1=I1,
		V1=O1
	    ),
	    (   S1==I1,
		V1==O1
	    ->  true
	    ;   domains_intersection(C2, from_to(S1, V1), D2),
		fd_put(A, D2, E2)
	    )
	;   A==B
	->  D=1
	;   fd_get(A, _, I1, O1, _),
	    fd_get(B, _, N, P, _),
	    fd_get(D, E1, G1),
	    cis_abs(I1, O2),
	    cis_max(O2, O1, D1),
	    cis_uminus(D1, B1),
	    domains_intersection(E1, from_to(B1, D1), P2),
	    (   cis_geq_zero(I1),
		cis_geq_zero(N)
	    ->  domain_remove_smaller_than(P2, 0, Q2)
	    ;   Q2=P2
	    ),
	    fd_put(D, Q2, G1)
	).
run_propagator(pabs(A, C), B) :-
	(   nonvar(A)
	->  kill(B),
	    C is abs(A)
	;   nonvar(C)
	->  kill(B),
	    C>=0,
	    D is -C,
	    in(A, D\/C)
	;   fd_get(A, G, J),
	    fd_get(C, E, _),
	    domain_negate(E, F),
	    domains_union(E, F, H),
	    domains_intersection(G, H, I),
	    fd_put(A, I, J),
	    (   fd_get(C, M, P)
	    ->  domain_negate(I, K),
		domains_union(I, K, L),
		domain_remove_smaller_than(L, 0, N),
		domains_intersection(M, N, O),
		fd_put(C, O, P)
	    ;   true
	    )
	).
run_propagator(pmod(A, B, D), C) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		B=\=0,
		D is A mod B
	    ;   true
	    )
	;   nonvar(B)
	->  B=\=0,
	    (   abs(B)=:=1
	    ->  kill(C),
		D=0
	    ;   fd_get(D, E, F)
	    ->  G is abs(B)-1,
		fd_get(D, E, F),
		(   B>0
		->  H=from_to(n(0), n(G))
		;   I is -G,
		    H=from_to(n(I), n(0))
		),
		domains_intersection(E, H, J),
		fd_put(D, J, F),
		(   fd_get(A, K, _),
		    domain_infimum(K, n(L))
		->  M is L mod B,
		    (   domain_contains(J, M)
		    ->  true
		    ;   neq_num(A, L)
		    )
		;   true
		),
		(   fd_get(A, N, _),
		    domain_supremum(N, n(O))
		->  P is O mod B,
		    (   domain_contains(J, P)
		    ->  true
		    ;   neq_num(A, O)
		    )
		;   true
		)
	    ;   fd_get(A, K, _),
		(   nonvar(D),
		    domain_infimum(K, n(L))
		->  (   L mod B=:=D
		    ->  true
		    ;   neq_num(A, L)
		    )
		;   true
		),
		(   nonvar(D),
		    domain_supremum(K, n(O))
		->  (   O mod B=:=D
		    ->  true
		    ;   neq_num(A, O)
		    )
		;   true
		)
	    )
	;   true
	).
run_propagator(pmax(A, B, D), C) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		D is max(A, B)
	    ;   nonvar(D)
	    ->  (   D=:=A
		->  kill(C),
		    (   integer(A),
			integer(B)
		    ->  A>=B
		    ;   clpfd_geq(A, B)
		    )
		;   D>A
		->  D=B
		;   fail
		)
	    ;   fd_get(B, K, E, F, _),
		(   cis_gtn(E,A) % cis_gt(E, n(A))
		->  D=B
		;   cis_lt(F, n(A))
		->  D=A
		;   F=n(H)
		->  fd_get(D, G, J),
		    domain_remove_greater_than(G, H, I),
		    fd_put(D, I, J)
		;   true
		)
	    )
	;   nonvar(B)
	->  run_propagator(pmax(B, A, D), C)
	;   fd_get(D, G, J)
	->  fd_get(A, _, L, M, _),
	    fd_get(B, K, E, F, _),
	    (   cis_gt(E, F)
	    ->  D=B
	    ;   cis_lt(F, L)
	    ->  D=A
	    ;   cis_max(M, F, n(H))
	    ->  domain_remove_greater_than(G, H, I),
		fd_put(D, I, J)
	    ;   true
	    )
	;   true
	).
run_propagator(pmin(A, B, D), C) :-
	(   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		D is min(A, B)
	    ;   nonvar(D)
	    ->  (   D=:=A
		->  kill(C),
		    (   integer(B),
			integer(A)
		    ->  B>=A
		    ;   clpfd_geq(B, A)
		    )
		;   D<A
		->  D=B
		;   fail
		)
	    ;   fd_get(B, K, F, E, _),
		(   cis_lt(E, n(A))
		->  D=B
		;   cis_gtn(F,A) % cis_gt(F, n(A))
		->  D=A
		;   F=n(H)
		->  fd_get(D, G, J),
		    domain_remove_smaller_than(G, H, I),
		    fd_put(D, I, J)
		;   true
		)
	    )
	;   nonvar(B)
	->  run_propagator(pmin(B, A, D), C)
	;   fd_get(D, G, J)
	->  fd_get(A, _, M, L, _),
	    fd_get(B, K, F, E, _),
	    (   cis_lt(E, F)
	    ->  D=B
	    ;   cis_gt(F, L)
	    ->  D=A
	    ;   cis_min(M, F, n(H))
	    ->  domain_remove_smaller_than(G, H, I),
		fd_put(D, I, J)
	    ;   true
	    )
	;   true
	).
run_propagator(pexp(A, D, C), B) :-
	(   A==1
	->  kill(B),
	    C=1
	;   A==0
	->  kill(B),
	    #<==>(C, #=(D, 0))
	;   D==0
	->  kill(B),
	    C=1
	;   D==1
	->  kill(B),
	    C=A
	;   nonvar(A),
	    nonvar(D)
	->  (   D>=0
	    ->  true
	    ;   A=:= -1
	    ),
	    kill(B),
	    C is A**D
	;   nonvar(C),
	    nonvar(D)
	->  integer_kth_root(C, D, E),
	    kill(B),
	    (   D mod 2=:=0
	    ->  F is -E,
		in(A, F\/E)
	    ;   A=E
	    )
	;   nonvar(D),
	    D>0
	->  (   D mod 2=:=0
	    ->  geq(C, 0)
	    ;   true
	    ),
	    (   fd_get(A, H, I, L, _),
		fd_get(C, G, Q)
	    ->  (   domain_contains(G, 0)
		->  true
		;   neq_num(A, 0)
		),
		(   domain_contains(H, 0)
		->  true
		;   neq_num(C, 0)
		),
		(   I=n(J),
		    J>=0
		->  K is J**D,
		    domain_remove_smaller_than(G, K, N),
		    (   L=n(M)
		    ->  O is M**D,
			domain_remove_greater_than(N, O, P)
		    ;   P=N
		    ),
		    fd_put(C, P, Q)
		;   true
		)
	    ;   true
	    )
	;   true
	).
run_propagator(pzcompare(D, A, B), C) :-
	(   A==B
	->  kill(C),
	    D= (=)
	;   nonvar(A)
	->  (   nonvar(B)
	    ->  kill(C),
		(   A>B
		->  D= (>)
		;   D= (<)
		)
	    ;   fd_get(B, _, E, F, _),
		(   cis_gtn(E,A) % cis_gt(E, n(A))
		->  kill(C),
		    D= (<)
		;   cis_lt(F, n(A))
		->  kill(C),
		    D= (>)
		;   true
		)
	    )
	;   nonvar(B)
	->  fd_get(A, _, G, H, _),
	    (   cis_gtn(G,B) % cis_gt(G, n(B))
	    ->  kill(C),
		D= (>)
	    ;   cis_lt(H, n(B))
	    ->  kill(C),
		D= (<)
	    ;   true
	    )
	;   fd_get(A, _, G, H, _),
	    fd_get(B, _, E, F, _),
	    (   cis_gt(G, F)
	    ->  kill(C),
		D= (>)
	    ;   cis_lt(H, E)
	    ->  kill(C),
		D= (<)
	    ;   true
	    )
	).
run_propagator(reified_in(A, C, D), B) :-
	(   integer(A)
	->  kill(B),
	    (   domain_contains(C, A)
	    ->  D=1
	    ;   D=0
	    )
	;   D==1
	->  kill(B),
	    domain(A, C)
	;   D==0
	->  kill(B),
	    domain_complement(C, E),
	    domain(A, E)
	;   fd_get(A, F, _),
	    (   domains_intersection(F, C, G)
	    ->  (   G==F
		->  kill(B),
		    D=1
		;   true
		)
	    ;   kill(B),
		D=0
	    )
	).
run_propagator(reified_fd(A, E), D) :-
	(   fd_inf(A, B),
	    B\==inf,
	    fd_sup(A, C),
	    C\==sup
	->  kill(D),
	    E=1
	;   E==0
	->  (   fd_inf(A, inf)
	    ->  true
	    ;   fd_sup(A, sup)
	    ->  true
	    ;   fail
	    )
	;   true
	).
run_propagator(reified_div(D, A, C, E), B) :-
	(   A==0
	->  kill(B),
	    C=0
	;   C==1
	->  kill(B),
	    (   integer(D),
		integer(A),
		A=\=0
	    ->  (   var(E)
		->  E is D//A
		;   integer(E)
		->  E=:=D//A
		;   clpfd_equal(E, D/A)
		)
	    ;   clpfd_equal(E, D/A)
	    )
	;   integer(A),
	    A=\=0
	->  kill(B),
	    C=1,
	    (   integer(D),
		integer(A),
		A=\=0
	    ->  (   var(E)
		->  E is D//A
		;   integer(E)
		->  E=:=D//A
		;   clpfd_equal(E, D/A)
		)
	    ;   clpfd_equal(E, D/A)
	    )
	;   fd_get(A, F, _),
	    \+ domain_contains(F, 0)
	->  kill(B),
	    C=1,
	    (   integer(D),
		integer(A),
		A=\=0
	    ->  (   var(E)
		->  E is D//A
		;   integer(E)
		->  E=:=D//A
		;   clpfd_equal(E, D/A)
		)
	    ;   clpfd_equal(E, D/A)
	    )
	;   true
	).
run_propagator(reified_mod(D, A, C, E), B) :-
	(   A==0
	->  kill(B),
	    C=0
	;   C==1
	->  kill(B),
	    (   integer(D),
		integer(A),
		A=\=0
	    ->  (   var(E)
		->  E is D mod A
		;   integer(E)
		->  E=:=D mod A
		;   clpfd_equal(E, D mod A)
		)
	    ;   clpfd_equal(E, D mod A)
	    )
	;   integer(A),
	    A=\=0
	->  kill(B),
	    C=1,
	    (   integer(D),
		integer(A),
		A=\=0
	    ->  (   var(E)
		->  E is D mod A
		;   integer(E)
		->  E=:=D mod A
		;   clpfd_equal(E, D mod A)
		)
	    ;   clpfd_equal(E, D mod A)
	    )
	;   fd_get(A, F, _),
	    \+ domain_contains(F, 0)
	->  kill(B),
	    C=1,
	    (   integer(D),
		integer(A),
		A=\=0
	    ->  (   var(E)
		->  E is D mod A
		;   integer(E)
		->  E=:=D mod A
		;   clpfd_equal(E, D mod A)
		)
	    ;   clpfd_equal(E, D mod A)
	    )
	;   true
	).
run_propagator(reified_geq(A, E, D, F, C), B) :-
	(   A==0
	->  kill(B),
	    C=0
	;   D==0
	->  kill(B),
	    C=0
	;   C==1
	->  kill(B),
	    A=1,
	    D=1,
	    (   integer(E),
		integer(F)
	    ->  E>=F
	    ;   clpfd_geq(E, F)
	    )
	;   A==1,
	    D==1
	->  (   var(C)
	    ->  (   nonvar(E)
		->  (   nonvar(F)
		    ->  kill(B),
			(   E>=F
			->  C=1
			;   C=0
			)
		    ;   fd_get(F, _, H, G, _),
			(   cis_geq(n(E), G)
			->  kill(B),
			    C=1
			;   cis_lt(n(E), H)
			->  kill(B),
			    C=0
			;   true
			)
		    )
		;   nonvar(F)
		->  fd_get(E, _, I, J, _),
		    (   cis_geq(I, n(F))
		    ->  kill(B),
			C=1
		    ;   cis_lt(J, n(F))
		    ->  kill(B),
			C=0
		    ;   true
		    )
		;   fd_get(E, _, I, J, _),
		    fd_get(F, _, H, G, _),
		    (   cis_geq(I, G)
		    ->  kill(B),
			C=1
		    ;   cis_lt(J, H)
		    ->  kill(B),
			C=0
		    ;   true
		    )
		)
	    ;   C=:=0
	    ->  kill(B),
		(   integer(F),
		    integer(E)
		->  F>=E+1
		;   clpfd_geq(F, E+1)
		)
	    ;   true
	    )
	;   true
	).
run_propagator(reified_eq(A, E, D, F, C), B) :-
	(   A==0
	->  kill(B),
	    C=0
	;   D==0
	->  kill(B),
	    C=0
	;   C==1
	->  kill(B),
	    A=1,
	    D=1,
	    E=F
	;   A==1,
	    D==1
	->  (   var(C)
	    ->  (   nonvar(E)
		->  (   nonvar(F)
		    ->  kill(B),
			(   E=:=F
			->  C=1
			;   C=0
			)
		    ;   fd_get(F, G, _),
			(   domain_contains(G, E)
			->  true
			;   kill(B),
			    C=0
			)
		    )
		;   nonvar(F)
		->  run_propagator(reified_eq(D, F, A, E, C), B)
		;   E==F
		->  kill(B),
		    C=1
		;   fd_get(E, _, H, K, _),
		    fd_get(F, _, J, I, _),
		    (   cis_gt(H, I)
		    ->  kill(B),
			C=0
		    ;   cis_gt(J, K)
		    ->  kill(B),
			C=0
		    ;   true
		    )
		)
	    ;   C=:=0
	    ->  kill(B),
		#\=(E, F)
	    ;   true
	    )
	;   true
	).
run_propagator(reified_neq(A, E, D, F, C), B) :-
	(   A==0
	->  kill(B),
	    C=0
	;   D==0
	->  kill(B),
	    C=0
	;   C==1
	->  kill(B),
	    A=1,
	    D=1,
	    #\=(E, F)
	;   A==1,
	    D==1
	->  (   var(C)
	    ->  (   nonvar(E)
		->  (   nonvar(F)
		    ->  kill(B),
			(   E=\=F
			->  C=1
			;   C=0
			)
		    ;   fd_get(F, G, _),
			(   domain_contains(G, E)
			->  true
			;   C=1
			)
		    )
		;   nonvar(F)
		->  run_propagator(reified_neq(D, F, A, E, C), B)
		;   E==F
		->  C=0
		;   fd_get(E, _, H, K, _),
		    fd_get(F, _, J, I, _),
		    (   cis_gt(H, I)
		    ->  kill(B),
			C=1
		    ;   cis_gt(J, K)
		    ->  kill(B),
			C=1
		    ;   true
		    )
		)
	    ;   C=:=0
	    ->  kill(B),
		E=F
	    ;   true
	    )
	;   true
	).
run_propagator(reified_and(B, C, A), D) :-
	(   var(A)
	->  (   nonvar(B)
	    ->  (   B=:=0
		->  A=0
		;   B=:=1
		->  A=C
		)
	    ;   nonvar(C)
	    ->  run_propagator(reified_and(C, B, A), D)
	    ;   true
	    )
	;   A=:=0
	->  (   B==1
	    ->  kill(D),
		C=0
	    ;   C==1
	    ->  kill(D),
		B=0
	    ;   true
	    )
	;   A=:=1
	->  kill(D),
	    B=1,
	    C=1
	).
run_propagator(reified_or(B, C, A), D) :-
	(   var(A)
	->  (   nonvar(B)
	    ->  (   B=:=1
		->  A=1
		;   B=:=0
		->  A=C
		)
	    ;   nonvar(C)
	    ->  run_propagator(reified_or(C, B, A), D)
	    ;   true
	    )
	;   A=:=0
	->  kill(D),
	    B=0,
	    C=0
	;   A=:=1
	->  (   B==0
	    ->  C=1
	    ;   C==0
	    ->  B=1
	    ;   true
	    )
	).
run_propagator(reified_not(A, C), B) :-
	(   A==0
	->  kill(B),
	    C=1
	;   A==1
	->  kill(B),
	    C=0
	;   C==0
	->  kill(B),
	    A=1
	;   C==1
	->  kill(B),
	    A=0
	;   true
	).
run_propagator(pimpl(A, C), B) :-
	(   nonvar(A)
	->  (   A=:=1
	    ->  kill(B),
		C=1
	    ;   kill(B)
	    )
	;   nonvar(C)
	->  (   C=:=0
	    ->  kill(B),
		A=0
	    ;   kill(B)
	    )
	;   true
	).
run_propagator(por(A, B, C), D) :-
	(   nonvar(A)
	->  (   A=:=0
	    ->  B=C
	    ;   A=:=1
	    ->  C=1
	    )
	;   nonvar(B)
	->  run_propagator(por(B, A, C), D)
	;   true
	).

serialize([], _).
serialize([B-A|D], C) :-
	cis_geq_zero(A),
	(   var(B)
	->  make_propagator(pserialized(B, A, C, D), E),
	    init_propagator(B, E),
	    trigger_prop(E)
	;   true
	),
	myserialized(A, C, D, B),
	serialize(D, [B-A|C]).

serialized(B, A) :-
	must_be(list(integer), A),
	pair_up(B, A, C),
	serialize(C, []),
	do_queue.


trigger_props(fd_props(D, B, A), C) :-
	trigger_props_(A),
	trigger_props_(B),
	(   ground(C)
	->  trigger_props_(D)
	;   true
	).

tuple_domain([], _).
tuple_domain([B|I], A) :-
	take_firsts(A, C, J),
	(   var(B)
	->  (   C=[D]
	    ->  B=D
	    ;   list_to_domain(C, F),
		fd_get(B, E, H),
		domains_intersection(E, F, G),
		fd_put(B, G, H)
	    )
	;   true
	),
	tuple_domain(I, J).

tuple_freeze(B, A) :-
	make_propagator(rel_tuple(mutable(A, _), B), C),
	tuple_freeze(B, B, C).

tuples_domain([], _).
tuples_domain([B|D], A) :-
	relation_unifiable(A, B, C, 0, _),
	(   ground(B)
	->  memberchk(B, A)
	;   tuple_domain(B, C),
	    tuple_freeze(B, C)
	),
	tuples_domain(D, A).

tuples_in(A, C) :-
	must_be(list, A),
	append(A, B),
	maplist(fd_variable, B),
	must_be(list(list(integer)), C),
	tuples_domain(A, C),
	do_queue.

vs_propagator([], _).
vs_propagator([A|C], B) :-
	init_propagator(A, B),
	vs_propagator(C, B).

absdiff_neq_const(B, C, A) :-
	(   A>=0
	->  make_propagator(absdiff_neq(B, C, A), D),
	    init_propagator(B, D),
	    init_propagator(C, D),
	    trigger_once(D)
	;   constrain_to_integer(B),
	    constrain_to_integer(C)
	).

all_collect([], A, A).
all_collect([B|D], A, E) :-
	(   member(C, A),
	    B==C
	->  all_collect(D, A, E)
	;   collect_variables(B, A, F),
	    all_collect(D, F, E)
	).

all_different([], _, _).
all_different([A|C], B, D) :-
	(   var(A)
	->  make_propagator(pdifferent(B, C, A, D), E),
	    init_propagator(A, E),
	    trigger_prop(E)
	;   exclude_fire(B, C, A)
	),
	all_different(C, [A|B], D).

all_distinct([], _, _).
all_distinct([A|C], B, D) :-
	(   var(A)
	->  make_propagator(pdistinct(B, C, A, D), E),
	    init_propagator(A, E),
	    trigger_prop(E)
	;   exclude_fire(B, C, A)
	),
	outof_reducer(B, C, A),
	all_distinct(C, [A|B], D).

append_propagators(fd_props(A, D, G), fd_props(B, E, H), fd_props(C, F, I)) :-
	append(A, B, C),
	append(D, E, F),
	append(G, H, I).


attribute_goals(A, G, M) :-
	get_attr(A, clpfd, clpfd_attr(_, _, _, F, fd_props(B, C, E))),
	append(B, C, D),
	append(D, E, H),
	domain_to_drep(F, K),
	I=G,
	(   default_domain(F),
	    one_alive(H),
	    J=I
	->  J=L
	;   I=[in(A, K)|L]
	),
	attributes_goals(H, L, M).

attributes_goals([], A, A).
attributes_goals([propagator(F, A)|K], B, L) :-
	(   arg(1, A, dead),
	    C=B
	->  C=E
	;   arg(1, A, processed),
	    D=B
	->  D=E
	;   (   functor(F, pdifferent, _)
	    ;   functor(F, pdistinct, _)
	    ),
	    arg(4, F, mutable(processed, _)),
	    G=B
	->  G=E
	;   attribute_goal_(F, J),
	    H=B
	->  setarg(1, A, processed),
	    I=H,
	    I=[J|E]
	;   B=[F|E]
	),
	attributes_goals(K, E, L).


bounds(A, C, D) :-
	(   fd_get(A, B, _)
	->  domain_infimum(B, n(C)),
	    domain_supremum(B, n(D))
	;   C=A,
	    D=C
	).

cis_div(sup, A, B) :-
	(   cis_geq_zero(A)
	->  B=sup
	;   B=inf
	).
cis_div(inf, A, B) :-
	(   cis_geq_zero(A)
	->  B=inf
	;   B=sup
	).
cis_div(n(B), A, C) :-
	cis_div_(A, B, C).

cis_div_(sup, _, n(0)).
cis_div_(inf, _, n(0)).
cis_div_(n(A), B, C) :-
	(   A=:=0
	->  (   B>=0
	    ->  C=sup
	    ;   C=inf
	    )
	;   D is B//A,
	    C=n(D)
	).

% Bart

cis_max(A,B,C) :-
	(compound(A) ->
	    (compound(B) ->
		A = n(X), B = n(Y),
		Z is max(X,Y),
		C = n(Z)
	    ;
		(B == sup ->
		    C = sup
		;
		    C = A
		)
	    )
	;
	    (A == sup ->
		C = sup
	    ;
		C = B
	    )
	).

cis_min(A,B,C) :-
	(compound(A) ->
	    (compound(B) ->
		A = n(X), B = n(Y),
		Z is min(X,Y),
		C = n(Z)
	    ;
		(B == inf ->
		    C = inf
		;
		    C = A
		)
	    )
	;
	    (A == inf ->
		C = inf
	    ;
		C = B
	    )
	).




cis_minus(inf, _, inf).
cis_minus(sup, _, sup).
cis_minus(n(B), A, C) :-
	cis_minus_(A, B, C).

cis_minus_(inf, _, sup).
cis_minus_(sup, _, inf).
cis_minus_(n(C), B, n(A)) :-
	A is B-C.


cis_plus(A,B,C) :-
	(compound(A) ->
	    (compound(B) ->
		A = n(X), B = n(Y),
		Z is X + Y,
		C = n(Z)
	    ;
		C = B
	    )
	;
	    C = A
	).

cis_slash(sup, _, sup).
cis_slash(inf, _, inf).
cis_slash(n(B), A, C) :-
	cis_slash_(A, B, C).

cis_slash_(sup, _, n(0)).
cis_slash_(inf, _, n(0)).
cis_slash_(n(C), B, n(A)) :-
	A is B//C.

cis_times(inf, A, B) :-
	(   cis_lt(A, n(0))
	->  B=sup
	;   cis_gtn(A,0) % cis_gt(A, n(0))
	->  B=inf
	;   B=n(0)
	).
cis_times(sup, A, B) :-
	(   cis_gtn(A,0) % cis_gt(A, n(0))
	->  B=sup
	;   cis_lt(A, n(0))
	->  B=inf
	;   B=n(0)
	).
cis_times(n(B), A, C) :-
	cis_times_(A, B, C).

cis_times_(inf, A, B) :-
	cis_times(inf, n(A), B).
cis_times_(sup, A, B) :-
	cis_times(sup, n(A), B).
cis_times_(n(C), B, n(A)) :-
	A is B*C.

coeff_var_term(A, C, B) :-
	(   A=:=1
	->  B=C
	;   B=A*C
	).

collect_variables(A, E, C) :-
	(   fd_get(A, _, B)
	->  term_variables(B, D),
	    C=[A|D]
	;   C=E
	).

contracting([], A, B) :-
	(   A
	->  contracting(B, fail, B)
	;   true
	).
contracting([A|D], E, F) :-
	fd_inf(A, B),
	(   \+ \+ A=B
	->  fd_sup(A, C),
	    (   \+ \+ A=C
	    ->  contracting(D, E, F)
	    ;   #\=(A, C),
		contracting(D, true, F)
	    )
	;   #\=(A, B),
	    contracting(D, true, F)
	).

delete_eq([], _, []).
delete_eq([A|D], B, C) :-
	(   A==B
	->  C=D
	;   C=[A|E],
	    delete_eq(D, B, E)
	).

domain_contract(B, A, E) :-
	(   A<0
	->  domain_negate(B, C),
	    D is abs(A)
	;   C=B,
	    D=A
	),
	domain_contract_(C, D, E).

domain_contract_(empty, _, empty).
domain_contract_(from_to(A, F), B, from_to(E, G)) :-
	(   cis_geq_zero(A)
	->  cis_plus(A, n(B), C),
	    cis_minus(C, n(1), D),
	    cis_slash(D, n(B), E)
	;   cis_slash(A, n(B), E)
	),
	(   cis_geq_zero(F)
	->  cis_slash(F, n(B), G)
	;   cis_minus(F, n(B), H),
	    cis_plus(H, n(1), I),
	    cis_slash(I, n(B), G)
	).
domain_contract_(split(A, C, D), B, J) :-
	H is A//B,
	domain_contract_(C, B, E),
	domain_contract_(D, B, F),
	domain_supremum(E, G),
	domain_infimum(F, I),
	(   cis_lt(G, n(H)),
	    cis_lt(n(H), I)
	->  J=split(H, E, F)
	;   domain_infimum(C, K),
	    domain_supremum(D, L),
	    min_divide(K, L, n(B), n(B), N),
	    max_divide(K, L, n(B), n(B), P),
	    domain_infimum(E, M),
	    domain_supremum(F, O),
	    cis_max(M, N, Q),
	    cis_min(O, P, R),
	    J=from_to(Q, R)
	).

domain_contract_less(B, A, E) :-
	(   A<0
	->  domain_negate(B, C),
	    D is abs(A)
	;   C=B,
	    D=A
	),
	domain_contract_less_(C, D, E).

domain_contract_less_(empty, _, empty).
domain_contract_less_(from_to(A, D), B, from_to(C, E)) :-
	cis_slash(A, n(B), C),
	cis_slash(D, n(B), E).
domain_contract_less_(split(A, C, D), B, J) :-
	H is A//B,
	domain_contract_less_(C, B, E),
	domain_contract_less_(D, B, F),
	domain_supremum(E, G),
	domain_infimum(F, I),
	(   cis_lt(G, n(H)),
	    cis_lt(n(H), I)
	->  J=split(H, E, F)
	;   domain_infimum(C, K),
	    domain_supremum(D, L),
	    min_divide_less(K, L, n(B), n(B), N),
	    max_divide_less(K, L, n(B), n(B), P),
	    domain_infimum(E, M),
	    domain_supremum(F, O),
	    cis_max(M, N, Q),
	    cis_min(O, P, R),
	    J=from_to(Q, R)
	).

domain_direction_element(from_to(n(B), n(C)), A, D) :-
	(   A==up
	->  between(B, C, D)
	;   between(B, C, E),
	    D is C- (E-B)
	).
domain_direction_element(split(_, B, D), A, C) :-
	(   A==up
	->  (   domain_direction_element(B, A, C)
	    ;   domain_direction_element(D, A, C)
	    )
	;   domain_direction_element(D, A, C)
	;   domain_direction_element(B, A, C)
	).

domain_expand(B, A, E) :-
	(   A<0
	->  domain_negate(B, C),
	    D is abs(A),
	    domain_expand_(C, D, E)
	;   A=:=1
	->  E=B
	;   domain_expand_(B, A, E)
	).

domain_expand_(empty, _, empty).
domain_expand_(from_to(A, D), B, from_to(C, E)) :-
	cis_times(A, n(B), C),
	cis_times(D, n(B), E).
domain_expand_(split(C, D, F), B, split(A, E, G)) :-
	A is B*C,
	domain_expand_(D, B, E),
	domain_expand_(F, B, G).

domain_expand_more(B, A, E) :-
	(   A<0
	->  domain_negate(B, C),
	    D is abs(A)
	;   C=B,
	    D=A
	),
	domain_expand_more_(C, D, E).

domain_expand_more_(empty, _, empty).
domain_expand_more_(from_to(A, F), C, from_to(E, G)) :-
	(   cis_lt(A, n(0))
	->  cis_minus(A, n(1), B),
	    cis_times(B, n(C), D),
	    cis_plus(D, n(1), E)
	;   cis_times(A, n(C), E)
	),
	(   cis_lt(F, n(0))
	->  cis_times(F, n(C), G)
	;   cis_plus(F, n(1), H),
	    cis_times(H, n(C), I),
	    cis_minus(I, n(1), G)
	).
domain_expand_more_(split(B, C, D), A, J) :-
	H is A*B,
	domain_expand_more_(C, A, E),
	domain_expand_more_(D, A, F),
	domain_supremum(E, G),
	domain_infimum(F, I),
	(   cis_lt(G, n(H)),
	    cis_lt(n(H), I)
	->  J=split(H, E, F)
	;   domain_infimum(E, K),
	    domain_supremum(F, L),
	    J=from_to(K, L)
	).

domain_intervals(split(_, A, C), B, E) :-
	domain_intervals(A, B, D),
	domain_intervals(C, D, E).
domain_intervals(empty, A, A).
domain_intervals(from_to(A, B), [A-B|C], C).

domain_remove(empty, _, empty).
domain_remove(from_to(A, B), C, D) :-
	domain_remove_(A, B, C, D).
domain_remove(split(B, D, E), A, C) :-
	(   A=:=B
	->  C=split(B, D, E)
	;   A<B
	->  domain_remove(D, A, F),
	    (   F==empty
	    ->  C=E
	    ;   C=split(B, F, E)
	    )
	;   domain_remove(E, A, G),
	    (   G==empty
	    ->  C=D
	    ;   C=split(B, D, G)
	    )
	).

domain_remove_greater_than(empty, _, empty).
domain_remove_greater_than(from_to(A, D), B, C) :-
	(   cis_gtn(A,B) % cis_gt(A, n(B))
	->  C=empty
	;   cis_min(D, n(B), E),
	    C=from_to(A, E)
	).
domain_remove_greater_than(split(A, F, C), B, E) :-
	(   A=<B
	->  domain_remove_greater_than(C, B, D),
	    (   D==empty
	    ->  E=F
	    ;   E=split(A, F, D)
	    )
	;   domain_remove_greater_than(F, B, E)
	).

domain_remove_smaller_than(empty, _, empty).
domain_remove_smaller_than(from_to(D, A), B, C) :-
	(   cis_lt(A, n(B))
	->  C=empty
	;   cis_max(D, n(B), E),
	    C=from_to(E, A)
	).
domain_remove_smaller_than(split(A, C, F), B, E) :-
	(   A>=B
	->  domain_remove_smaller_than(C, B, D),
	    (   D==empty
	    ->  E=F
	    ;   E=split(A, D, F)
	    )
	;   domain_remove_smaller_than(F, B, E)
	).

domain_shift(empty, _, empty).
domain_shift(from_to(A, D), B, from_to(C, E)) :-
	cis_plus(A, n(B), C),
	cis_plus(D, n(B), E).
domain_shift(split(B, D, F), C, split(A, E, G)) :-
	A is B+C,
	domain_shift(D, C, E),
	domain_shift(F, C, G).

domain_subdomain(from_to(_, _), B, A) :-
	domain_subdomain_fromto(A, B).
domain_subdomain(split(_, _, _), B, A) :-
	domain_subdomain_split(A, B, A).

domain_subdomain_split(empty, _, _).
domain_subdomain_split(from_to(E, A), split(B, C, F), D) :-
	(   cis_lt(A, n(B))
	->  domain_subdomain(C, C, D)
	;   cis_gtn(E,B) % cis_gt(E, n(B))
	->  domain_subdomain(F, F, D)
	).
domain_subdomain_split(split(_, B, C), A, _) :-
	domain_subdomain(A, A, B),
	domain_subdomain(A, A, C).

domain_subtract(A, B, C) :-
	domain_subtract(A, A, B, C).

domains_intersection(A, B, C) :-
	domains_intersection_(A, B, C),
	C\==empty.

domains_intersection_(empty, _, empty).
domains_intersection_(from_to(B, C), A, D) :-
	narrow(A, B, C, D).
domains_intersection_(split(G, A, C), B, E) :-
	domains_intersection_(A, B, D),
	domains_intersection_(C, B, F),
	(   D==empty
	->  E=F
	;   F==empty
	->  E=D
	;   E=split(G, D, F)
	).

domains_union(A, B, G) :-
	domain_intervals(A, C),
	domain_intervals(B, D),
	append(C, D, E),
	merge_intervals(E, F),
	intervals_to_domain(F, G).

drep_to_intervals(A, B, D) :-
	integer(A),
	C=B, !,
	C=[n(A)-n(A)|D].
drep_to_intervals(..(A, B), E, G) :-
	(   defaulty_to_bound(A, C),
	    defaulty_to_bound(B, D),
	    cis_leq(C, D),
	    F=E
	->  F=[C-D|G]
	;   E=G
	).
drep_to_intervals(A\/C, B, E) :-
	drep_to_intervals(A, B, D),
	drep_to_intervals(C, D, E).

element(D, A, E) :-
	must_be(list, A),
	length(A, B),
	numlist(1, B, C),
	maplist(twolist, C, A, F),
	tuples_in([[D, E]], F).

exclude_fire(A, C, B) :-
	exclude_list(A, B),
	exclude_list(C, B).

fd_get(A, C, D) :-
	(   get_attr(A, clpfd, B)
	->  % B=clpfd_attr(_, _, _, C, D)
	    arg(4,B,C), arg(5,B,D)
	;   var(A),
	    D=fd_props([], [], []),
	    default_domain(C)
	).

fd_put(A, B, C) :-
	cpf_clpfd_propagation(F),
	(   F == full
	->  put_full(A, B, C)
	;   put_terminating(A, B, C)
	).

find_ff([], A, A).
find_ff([A|C], B, D) :-
	(   ff_lt(A, B)
	->  find_ff(C, A, D)
	;   find_ff(C, B, D)
	).

find_ffc([], A, A).
find_ffc([A|C], B, D) :-
	(   ffc_lt(A, B)
	->  find_ffc(C, A, D)
	;   find_ffc(C, B, D)
	).

find_max([], A, A).
find_max([A|C], B, D) :-
	(   max_gt(A, B)
	->  find_max(C, A, D)
	;   find_max(C, B, D)
	).

find_min([], A, A).
find_min([A|C], B, D) :-
	(   min_lt(A, B)
	->  find_min(C, A, D)
	;   find_min(C, B, D)
	).


gcd([], A, A).
gcd([A|C], B, E) :-
	gcd_(A, B, D),
	gcd(C, D, E).

gcd_(C, A, B) :-
	(   A=:=0
	->  B=C
	;   D is C mod A,
	    gcd_(A, D, B)
	).


insert_propagator(B, A, E) :-
	A=fd_props(F, G, H),
	arg(1, B, C),
	functor(C, D, _),
	(   constraint_wake(D, ground)
	->  E=fd_props([B|F], G, H)
	;   constraint_wake(D, bounds)
	->  E=fd_props(F, [B|G], H)
	;   E=fd_props(F, G, [B|H])
	).

integer_kth_root(B, A, C) :-
	(   A mod 2=:=0
	->  B>=0
	;   true
	),
	(   B<0
	->  A mod 2=:=1,
	    integer_kroot(B, 0, B, A, C)
	;   integer_kroot(0, B, B, A, C)
	).

intervals_to_drep([], A, A).
intervals_to_drep([A-B|F], G, H) :-
	bound_portray(A, C),
	bound_portray(B, D),
	(   C==D
	->  E=C
	;   E= ..(C, D)
	),
	intervals_to_drep(F, G\/E, H).

largest_finite(sup, A, A).
largest_finite(n(A), _, n(A)).

list_goal_([], A, A).
list_goal_([C|A], B, D) :-
	list_goal_(A, (B, C), D).

mydiv(A, B, C) :-
	make_propagator(pdiv(A, B, C), D),
	init_propagator(A, D),
	init_propagator(B, D),
	init_propagator(C, D),
	trigger_once(D).

myexp(A, B, C) :-
	make_propagator(pexp(A, B, C), D),
	init_propagator(A, D),
	init_propagator(B, D),
	init_propagator(C, D),
	trigger_once(D).

mymax(A, C, B) :-
	#=<(A, B),
	#=<(C, B),
	make_propagator(pmax(A, C, B), D),
	init_propagator(A, D),
	init_propagator(C, D),
	init_propagator(B, D),
	trigger_once(D).

mymin(A, C, B) :-
	#>=(A, B),
	#>=(C, B),
	make_propagator(pmin(A, C, B), D),
	init_propagator(A, D),
	init_propagator(C, D),
	init_propagator(B, D),
	trigger_once(D).

mymod(A, B, C) :-
	make_propagator(pmod(A, B, C), D),
	init_propagator(A, D),
	init_propagator(B, D),
	init_propagator(C, D),
	trigger_once(D).

myor(A, B, C) :-
	make_propagator(por(A, B, C), D),
	init_propagator(A, D),
	init_propagator(B, D),
	init_propagator(C, D),
	trigger_prop(D).

myplus(A, B, C) :-
	make_propagator(pplus(A, B, C), D),
	init_propagator(A, D),
	init_propagator(B, D),
	init_propagator(C, D),
	trigger_once(D).

myserialized([], _, _).
myserialized([A-B|I], E, C) :-
	(   var(A)
	->  serialize_lower_bound(A, B, D, C),
	    (   var(A)
	    ->  serialize_upper_bound(A, B, D, C)
	    ;   true
	    )
	;   var(E)
	->  serialize_lower_bound(E, C, F, B),
	    (   var(E)
	    ->  serialize_upper_bound(E, C, F, B)
	    ;   true
	    )
	;   B=n(G),
	    C=n(H),
	    (   A+G=<E
	    ->  true
	    ;   E+H=<A
	    ->  true
	    ;   fail
	    )
	),
	myserialized(I, E, C).

mytimes(A, B, C) :-
	make_propagator(ptimes(A, B, C), D),
	init_propagator(A, D),
	init_propagator(B, D),
	init_propagator(C, D),
	trigger_once(D).


optimise(B, C, A) :-
	A=[D|F],
	E=extremum(none),
	(   store_extremum(B, C, D, E)
	;   E=extremum(n(H)),
	    arg(1, D, G),
	    append(F, C, I),
	    (   #=(G, H),
		labeling(I, B)
	    ;   #\=(G, H),
		optimise(B, C, A)
	    )
	).

order_dom_next(up, A, B) :-
	domain_infimum(A, n(B)).
order_dom_next(down, A, B) :-
	domain_supremum(A, n(B)).

outof_reducer(B, C, A) :-
	(   fd_get(A, D, _)
	->  append(B, C, E),
	    domain_num_elements(D, G),
	    num_subsets(E, D, 0, F, H),
	    (   cis_geq(n(F), G)
	    ->  fail
	    ;   cis_minus(G, n(1), n(F))
	    ->  reduce_from_others(H, D)
	    ;   true
	    )
	;   true
	).

pair_up([], [], []).
pair_up([A|C], [B|D], [A-n(B)|E]) :-
	pair_up(C, D, E).

parse_reified_clpfd(A, B, C) :-
	(   cyclic_term(A)
	->  domain_error(clpfd_expression, A)
	;   var(A)
	->  constrain_to_integer(A),
	    B=A,
	    C=1
	;   integer(A)
	->  B=A,
	    C=1
	;   A=D+E
	->  parse_reified_clpfd(D, F, H),
	    parse_reified_clpfd(E, G, I),
	    myplus(F, G, B),
	    #<==>(#/\(H, I), C)
	;   A=D*E
	->  parse_reified_clpfd(D, F, H),
	    parse_reified_clpfd(E, G, I),
	    mytimes(F, G, B),
	    #<==>(#/\(H, I), C)
	;   A=D-E
	->  parse_reified_clpfd(D, F, H),
	    parse_reified_clpfd(E, G, I),
	    mytimes(-1, G, J),
	    myplus(F, J, B),
	    #<==>(#/\(H, I), C)
	;   A= -K
	->  parse_reified_clpfd(K, L, C),
	    mytimes(-1, L, B)
	;   A=max(D, E)
	->  parse_reified_clpfd(D, F, H),
	    parse_reified_clpfd(E, G, I),
	    mymax(F, G, B),
	    #<==>(#/\(H, I), C)
	;   A=min(D, E)
	->  parse_reified_clpfd(D, F, H),
	    parse_reified_clpfd(E, G, I),
	    mymin(F, G, B),
	    #<==>(#/\(H, I), C)
	;   A=D mod E
	->  parse_reified_clpfd(D, F, H),
	    parse_reified_clpfd(E, G, I),
	    #<==>(#/\(H, I), M),
	    my_reified_mod(F, G, N, B),
	    #<==>(#/\(M, N), C)
	;   A=abs(K)
	->  parse_reified_clpfd(K, L, C),
	    myabs(L, B),
	    (   integer(B)
	    ->  B>=0
	    ;   clpfd_geq(B, 0)
	    )
	;   A=D/E
	->  parse_reified_clpfd(D, F, H),
	    parse_reified_clpfd(E, G, I),
	    #<==>(#/\(H, I), M),
	    my_reified_div(F, G, N, B),
	    #<==>(#/\(M, N), C)
	;   A=D^E
	->  parse_reified_clpfd(D, F, H),
	    parse_reified_clpfd(E, G, I),
	    #<==>(#/\(H, I), C),
	    myexp(F, G, B)
	;   domain_error(clpfd_expression, A)
	).


power_var_num(A, B, C) :-
	(   var(A)
	->  B=A,
	    C=1
	;   A=D*E,
	    power_var_num(D, F, H),
	    power_var_num(E, G, I),
	    F==G,
	    B=F,
	    C is H+I
	).


put_full(C, A, E) :-
	A\==empty,
	(   A=from_to(B, B)
	->  B=n(C)
	;   get_attr(C, clpfd, D)
	->  D=clpfd_attr(_, _, _, F, _),
	    put_attr(C, clpfd, clpfd_attr(no, no, no, A, E)),
	    (   F==A
	    ->  true
	    ;   trigger_props(E, C, F, A)
	    )
	;   var(C)
	->  put_attr(C, clpfd, clpfd_attr(no, no, no, A, E))
	;   true
	).

% Bart  -  als dom en props gelijk zijn - niks doen ? kan dat ?
% mogelijk dat 2 puts gedaan worden voor zelfde var
% F=yes mag dat ook F==yes zijn ?

put_terminating(C, A, H) :-
	A\==empty,
	(   A=from_to(B, B)
	->  B=n(C)  % could B be equal to sup or inf ?
	;   get_attr(C, clpfd, D)
	->  D=clpfd_attr(E, F, G, I, _),
	    % put_attr(C, clpfd, clpfd_attr(E, F, G, A, H)),
	    (   I==A
	    ->  % put_attr(C, clpfd, clpfd_attr(E, F, G, A, H))
		setarg(5,D,H)
	    ;   (   E=='.'
		->  L=yes
		;   domain_infimum(A, J),
		    domain_supremum(A, K),
		    (   compound(J), compound(K)
			% J = n(_), K=n(_)
		    ->   L=yes
		    ;   L=no
		    )
		),
		(   L==yes
		->  % put_attr(C, clpfd, clpfd_attr('.', '.', '.', A, H)),
		    setarg(4,D,A), setarg(5,D,H),
		    trigger_props(H, C, I, A)
		;   domain_infimum(I, M),
		    (   J==M
		    ->  N=E
		    ;   N=yes
		    ),
		    domain_supremum(I, O),
		    (   K==O
		    ->  P=F
		    ;   P=yes
		    ),
		    domain_spread(I, R),
		    domain_spread(A, Q),
		    (   Q==R
		    ->  S=G
		    ;   S=yes
		    ),
		    put_attr(C, clpfd, clpfd_attr(N, P, S, A, H)),
		    (   P==yes,
			F=yes
		    ->  true
		    ;   N==yes,
			E=yes
		    ->  true
		    ;   S==yes,
			G=yes
		    ->  true
		    ;   trigger_props(H, C, I, A)
		    )
		)
	    )
	;   var(C)
	->  put_attr(C, clpfd, clpfd_attr(no, no, no, A, H))
	;   true
	).


smallest_finite(inf, A, A).
smallest_finite(n(A), _, n(A)).

sum(A, B, D) :-
	must_be(list, A),
	maplist(fd_variable, A),
	must_be(callable, B),
	(   scalar_supported(B),
	    vars_plusterm(A, 0, C),
	    left_right_linsum_const(C, D, E, F, G)
	->  scalar_product(E, F, B, G)
	;   sum(A, 0, B, D)
	).

take_firsts([], [], []).
take_firsts([[A|B]|C], [A|D], [B|E]) :-
	take_firsts(C, D, E).

tighten(min, A, B) :-
	#<(A, B).
tighten(max, A, B) :-
	#>(A, B).

tuple_freeze([], _, _).
tuple_freeze([A|C], D, B) :-
	(   var(A)
	->  init_propagator(A, B),
	    trigger_prop(B)
	;   true
	),
	tuple_freeze(C, D, B).

twolist(A, B, [A, B]).

var_leq_var_plus_const(A, B, C) :-
	make_propagator(x_leq_y_plus_c(A, B, C), D),
	init_propagator(A, D),
	init_propagator(B, D),
	trigger_once(D).

vars_plusterm([], A, A).
vars_plusterm([C|A], B, D) :-
	vars_plusterm(A, B+C, D).

x_neq_y_plus_z(B, C, A) :-
	(   A==0
	->  neq(B, C)
	;   make_propagator(x_neq_y_plus_z(B, C, A), D),
	    init_propagator(B, D),
	    init_propagator(C, D),
	    init_propagator(A, D),
	    trigger_once(D)
	).

zcompare(A, B, C) :-
	(   nonvar(A)
	->  zcompare_(A, B, C)
	;   freeze(A, zcompare_(A, B, C)),
	    fd_variable(B),
	    fd_variable(C),
	    make_propagator(pzcompare(A, B, C), D),
	    init_propagator(B, D),
	    init_propagator(C, D),
	    trigger_once(D)
	).

zcompare_(=, B, A) :-
	(   integer(A)
	->  (   var(B)
	    ->  B is A
	    ;   integer(B)
	    ->  B=:=A
	    ;   clpfd_equal(B, A)
	    )
	;   clpfd_equal(B, A)
	).
zcompare_(<, B, A) :-
	(   integer(A),
	    integer(B)
	->  A>=B+1
	;   clpfd_geq(A, B+1)
	).
zcompare_(>, A, B) :-
	(   integer(A),
	    integer(B)
	->  A>=B+1
	;   clpfd_geq(A, B+1)
	).

cis_goals(A, A, C, B) :-
	var(A),
	B=C, !.
cis_goals(n(A), n(A), B, B).
cis_goals(inf, inf, A, A).
cis_goals(sup, sup, A, A).
cis_goals(sign(A), E, B, F) :-
	cis_goals(A, D, B, C),
	C=[cis_sign(D, E)|F].
cis_goals(abs(A), E, B, F) :-
	cis_goals(A, D, B, C),
	C=[cis_abs(D, E)|F].
cis_goals(-A, E, B, F) :-
	cis_goals(A, D, B, C),
	C=[cis_uminus(D, E)|F].
cis_goals(A+C, H, B, I) :-
	cis_goals(A, F, B, D),
	cis_goals(C, G, D, E),
	E=[cis_plus(F, G, H)|I].
cis_goals(A-C, H, B, I) :-
	cis_goals(A, F, B, D),
	cis_goals(C, G, D, E),
	E=[cis_minus(F, G, H)|I].
cis_goals(min(A, C), H, B, I) :-
	cis_goals(A, F, B, D),
	cis_goals(C, G, D, E),
	E=[cis_min(F, G, H)|I].
cis_goals(max(A, C), H, B, I) :-
	cis_goals(A, F, B, D),
	cis_goals(C, G, D, E),
	E=[cis_max(F, G, H)|I].
cis_goals(A*C, H, B, I) :-
	cis_goals(A, F, B, D),
	cis_goals(C, G, D, E),
	E=[cis_times(F, G, H)|I].
cis_goals(div(A, C), H, B, I) :-
	cis_goals(A, F, B, D),
	cis_goals(C, G, D, E),
	E=[cis_div(F, G, H)|I].
cis_goals(A//C, H, B, I) :-
	cis_goals(A, F, B, D),
	cis_goals(C, G, D, E),
	E=[cis_slash(F, G, H)|I].

domain_remove_(inf, A, B, C) :-
	(   A==n(B)
	->  D is B-1,
	    C=from_to(inf, n(D))
	;   cis_lt(A, n(B))
	->  C=from_to(inf, A)
	;   E is B+1,
	    D is B-1,
	    C=split(B, from_to(inf, n(D)), from_to(n(E), A))
	).
domain_remove_(n(B), A, C, D) :-
	domain_remove_upper(A, B, C, D).

domain_remove_upper(sup, A, B, C) :-
	(   A=:=B
	->  D is B+1,
	    C=from_to(n(D), sup)
	;   A>B
	->  C=from_to(n(A), sup)
	;   D is B+1,
	    E is B-1,
	    C=split(B, from_to(n(A), n(E)), from_to(n(D), sup))
	).
domain_remove_upper(n(B), A, C, D) :-
	(   A=:=B,
	    C=:=A
	->  D=empty
	;   A=:=C
	->  E is C+1,
	    D=from_to(n(E), n(B))
	;   B=:=C
	->  F is C-1,
	    D=from_to(n(A), n(F))
	;   between(A, B, C)
	->  F is C-1,
	    E is C+1,
	    D=split(C, from_to(n(A), n(F)), from_to(n(E), n(B)))
	;   D=from_to(n(A), n(B))
	).

domain_subtract(empty, _, _, empty).
domain_subtract(from_to(H, G), C, A, B) :-
	(   A==empty
	->  B=C
	;   A=from_to(D, E)
	->  (   D==E
	    ->  D=n(F),
		domain_remove(C, F, B)
	    ;   cis_gt(D, G)
	    ->  B=C
	    ;   cis_lt(E, H)
	    ->  B=C
	    ;   cis_leq(D, H)
	    ->  (   cis_geq(E, G)
		->  B=empty
		;   cis_plus(E, n(1), I),
		    B=from_to(I, G)
		)
	    ;   cis_minus(D, n(1), K),
		(   cis_lt(E, G)
		->  D=n(J),
		    cis_plus(E, n(1), L),
		    B=split(J, from_to(H, K), from_to(L, G))
		;   B=from_to(H, K)
		)
	    )
	;   A=split(J, M, N)
	->  (   cis_gt(n(J), G)
	    ->  domain_subtract(C, C, M, B)
	    ;   cis_lt(n(J), H)
	    ->  domain_subtract(C, C, N, B)
	    ;   domain_subtract(C, C, M, O),
		domain_subtract(O, O, N, B)
	    )
	).
domain_subtract(split(G, A, C), _, B, E) :-
	domain_subtract(A, A, B, D),
	domain_subtract(C, C, B, F),
	(   D==empty
	->  E=F
	;   F==empty
	->  E=D
	;   E=split(G, D, F)
	).

expr_conds(A, A, B, D) :-
	var(A),
	C=B, !,
	C=[integer(A)|D].
expr_conds(A, A, B, D) :-
	integer(A),
	C=B, !,
	C=D.
expr_conds(-A, -B, C, D) :-
	expr_conds(A, B, C, D).
expr_conds(abs(A), abs(B), C, D) :-
	expr_conds(A, B, C, D).
expr_conds(A+D, B+E, C, G) :-
	expr_conds(A, B, C, F),
	expr_conds(D, E, F, G).
expr_conds(A*D, B*E, C, G) :-
	expr_conds(A, B, C, F),
	expr_conds(D, E, F, G).
expr_conds(A-D, B-E, C, G) :-
	expr_conds(A, B, C, F),
	expr_conds(D, E, F, G).
expr_conds(A/D, B//E, C, H) :-
	expr_conds(A, B, C, F),
	expr_conds(D, E, F, G),
	G=[E=\=0|H].
expr_conds(min(A, D), min(B, E), C, G) :-
	expr_conds(A, B, C, F),
	expr_conds(D, E, F, G).
expr_conds(max(A, D), max(B, E), C, G) :-
	expr_conds(A, B, C, F),
	expr_conds(D, E, F, G).
expr_conds(A mod D, B mod E, C, H) :-
	expr_conds(A, B, C, F),
	expr_conds(D, E, F, G),
	G=[E=\=0|H].
expr_conds(A^D, B^E, C, H) :-
	expr_conds(A, B, C, F),
	expr_conds(D, E, F, G),
	G=[ (E>=0;B=:= -1)|H].

filter_linsum([], [], [], []).
filter_linsum([A|C], [B|D], E, F) :-
	(   A=:=0
	->  constrain_to_integer(B),
	    filter_linsum(C, D, E, F)
	;   E=[A|G],
	    F=[B|H],
	    filter_linsum(C, D, G, H)
	).

kill_if_isolated(A, B, C, F) :-
	append(A, B, D),
	fd_get(C, E, _),
	(   all_empty_intersection(D, E)
	->  kill(F)
	;   true
	).

lin_mul([], _, A, A).
lin_mul([vn(E, A)|G], B, C, I) :-
	F is A*B,
	D=C,
	D=[vn(E, F)|H],
	lin_mul(G, B, H, I).

list_to_disjoint_intervals([], A, B, [n(A)-n(B)]).
list_to_disjoint_intervals([A|C], D, B, E) :-
	(   A=:=B+1
	->  list_to_disjoint_intervals(C, D, A, E)
	;   E=[n(D)-n(B)|F],
	    list_to_disjoint_intervals(C, A, A, F)
	).

merge_remaining([], A, A, []).
merge_remaining([B-F|G], A, D, E) :-
	cis_plus(A, n(1), C),
	(   cis_gt(B, C)
	->  D=A,
	    E=[B-F|G]
	;   cis_max(A, F, H),
	    merge_remaining(G, H, D, E)
	).

my_reified_div(A, B, C, D) :-
	make_propagator(reified_div(A, B, C, D), E),
	init_propagator(A, E),
	init_propagator(B, E),
	init_propagator(D, E),
	init_propagator(C, E),
	trigger_once(E).

my_reified_mod(A, B, C, D) :-
	make_propagator(reified_mod(A, B, C, D), E),
	init_propagator(A, E),
	init_propagator(B, E),
	init_propagator(D, E),
	init_propagator(C, E),
	trigger_once(E).

myserialized(C, A, D, B) :-
	myserialized(A, B, C),
	myserialized(D, B, C).

narrow(empty, _, _, empty).
narrow(from_to(B, D), A, C, G) :-
	cis_max(A, B, E),
	cis_min(C, D, F),
	(   cis_gt(E, F)
	->  G=empty
	;   G=from_to(E, F)
	).
narrow(split(B, C, F), D, A, E) :-
	(   cis_lt(A, n(B))
	->  narrow(C, D, A, E)
	;   cis_gtn(D,B) % cis_gt(D, n(B))
	->  narrow(F, D, A, E)
	;   narrow(C, D, A, G),
	    narrow(F, D, A, H),
	    (   G==empty
	    ->  E=H
	    ;   H==empty
	    ->  E=G
	    ;   E=split(B, G, H)
	    )
	).

override_(default(_), A, _, user(A)).
override_(user(B), A, C, _) :-
	(   A==B
	->  domain_error(nonrepeating_labeling_options, C)
	;   domain_error(consistent_labeling_options, C)
	).

remove_dist_upper_lower([], _, _, _).
remove_dist_upper_lower([B|M], [A|N], E, I) :-
	(   fd_get(A, C, L)
	->  (   B<0
	    ->  domain_supremum(C, n(D)),
		F is D+E//B,
		domain_remove_smaller_than(C, F, G),
		domain_infimum(G, n(H)),
		J is H-I//B,
		domain_remove_greater_than(G, J, K)
	    ;   domain_infimum(C, n(H)),
		J is H+E//B,
		domain_remove_greater_than(C, J, G),
		domain_supremum(G, n(D)),
		F is D-I//B,
		domain_remove_smaller_than(G, F, K)
	    ),
	    fd_put(A, K, L)
	;   true
	),
	remove_dist_upper_lower(M, N, E, I).

scalar_product(A, B, C, D) :-
	make_propagator(scalar_product(A, B, C, D), E),
	vs_propagator(B, E),
	trigger_prop(E),
	do_queue.

select_var(leftmost, [A|B], A, B).
select_var(min, [B|A], C, D) :-
	find_min(A, B, C),
	delete_eq([B|A], C, D).
select_var(max, [B|A], C, D) :-
	find_max(A, B, C),
	delete_eq([B|A], C, D).
select_var(ff, [B|A], C, D) :-
	find_ff(A, B, C),
	delete_eq([B|A], C, D).
select_var(ffc, [B|A], C, D) :-
	find_ffc(A, B, C),
	delete_eq([B|A], C, D).

serialize_lower_bound(A, E, C, I) :-
	fd_get(A, B, L),
	domain_infimum(B, D),
	latest_start_time(C, G),
	(   cis_plus(D, E, F),
	    cis_gt(F, G)
	->  earliest_start_time(C, H),
	    cis_plus(H, I, J),
	    domain_remove_smaller_than(B, J, K),
	    fd_put(A, K, L)
	;   true
	).

serialize_upper_bound(A, I, C, E) :-
	fd_get(A, B, L),
	domain_supremum(B, G),
	earliest_start_time(C, D),
	(   cis_plus(D, E, F),
	    cis_gt(F, G)
	->  latest_start_time(C, H),
	    cis_minus(H, I, J),
	    domain_remove_greater_than(B, J, K),
	    fd_put(A, K, L)
	;   true
	).

store_extremum(A, C, B, I) :-
	duplicate_term(A-B, D-E),
	once(labeling(C, D)),
	functor(B, F, _),
	maplist(arg(1), [B, E], [H, G]),
	optimise(F, C, A, G, H, I).

sum([], B, A, C) :-
	call(A, B, C).
sum([B|C], A, E, F) :-
	#=(D, A+B),
	sum(C, D, E, F).

trigger_props(fd_props(C, D, A), B, E, F) :-
	trigger_props_(A),
	(   ground(B)
	->  trigger_props_(C),
	    trigger_props_(D)
	;   D\==[]
	->  domain_infimum(E, H),
	    domain_infimum(F, G),
	    (   G==H
	    ->  domain_supremum(E, J),
		domain_supremum(F, I),
		(   I==J
		->  true
		;   trigger_props_(D)
		)
	    ;   trigger_props_(D)
	    )
	;   true
	).

unfold_product([], [], A, A).
unfold_product([A|C], [B|D], E, G) :-
	coeff_var_term(A, B, F),
	unfold_product(C, D, E+F, G).

fd_get(A, B, D, E, C) :-
	fd_get(A, B, C),
	domain_infimum(B, D),
	domain_supremum(B, E).

integer_kroot(A, B, C, D, E) :-
	(   A=:=B
	->  C=:=A**D,
	    E=A
	;   A+1=:=B
	->  (   A**D=:=C
	    ->  E=A
	    ;   B**D=:=C
	    ->  E=B
	    ;   fail
	    )
	;   F is (A+B)//2,
	    (   F**D>C
	    ->  integer_kroot(A, F, C, D, E)
	    ;   integer_kroot(F, B, C, D, E)
	    )
	).

label([], _, _, _, A) :- !,
	(   A=upto_in(B, C)
	->  B=C
	;   true
	).
label(B, A, I, J, D) :-
	select_var(A, B, C, H),
	(   var(C)
	->  (   D=upto_in(F, L),
		fd_get(C, _, E),
		all_dead(E)
	    ->  fd_size(C, G),
		K is F*G,
		label(H, A, I, J, upto_in(K, L))
	    ;   choice_order_variable(J, I, C, H, A, D)
	    )
	;   must_be(integer, C),
	    label(H, A, I, J, D)
	).

left_right_linsum_const(A, B, N, O, P) :-
	\+ (   v_or_i(A),
	   v_or_i(B)
	   ),
	\+ (   nonvar(A),
	   A=C+D,
% 	   maplist(v_or_i, [C, D, B]),
	   v_or_i(C),
	   v_or_i(D),
	   v_or_i(B)
	   ),
	\+ (   nonvar(B),
	   B=C+D,
% 	   maplist(v_or_i, [C, D, A]),
	       v_or_i(C),
	       v_or_i(D),
	       v_or_i(A)
	   ),
	\+ (   nonvar(A),
	   A=C*D,
% 	   maplist(v_or_i, [C, D, B])
	       v_or_i(C),
	       v_or_i(D),
	       v_or_i(B)
	   ),
	\+ (   nonvar(B),
	   B=C*D,
% 	   maplist(v_or_i, [C, D, A])
	       v_or_i(C),
	       v_or_i(D),
	       v_or_i(A)
	   ),
	linsum(A, 0, R, G, F), % phrase
	linsum(B, 0, Q, E, []), % phrase
	maplist(linterm_negate, E, F),
	msort(G, H),
	H=[vn(K, J)|I],
	vns_coeffs_variables(I, J, K, L, M),
	filter_linsum(L, M, N, O),
	P is Q-R.

linsum(B, A, A, C, E) :-
	var(B),
	D=C, !,
	D=[vn(B, 1)|E].
linsum(A, C, B, E, D) :-
	integer(A), !,
	B is C+A,
	D=E.
linsum(-A, B, C, D, E) :-
	mulsum(A, -1, B, C, D, E).
linsum(A*C, D, E, B, G) :-
	integer(A),
	F=B, !,
	mulsum(C, A, D, E, F, G).
linsum(C*A, D, E, B, G) :-
	integer(A),
	F=B, !,
	mulsum(C, A, D, E, F, G).
linsum(A+D, B, F, C, H) :-
	linsum(A, B, E, C, G),
	linsum(D, E, F, G, H).
linsum(A-D, B, F, C, H) :-
	linsum(A, B, E, C, G),
	mulsum(D, -1, E, F, G, H).

max_divide(B, C, A, E, D) :-
	(   A=n(_),
	    cis_geq_zero(B),
	    cis_geq_zero(A)
	->  cis_div(C, A, D)
	;   cis_leq(A, n(0)),
	    cis_geq_zero(E)
	->  D=sup
	;   cis_div(B, A, F),
	    cis_div(B, E, G),
	    cis_max(F, G, J),
	    cis_div(C, A, H),
	    cis_div(C, E, I),
	    cis_max(H, I, K),
	    cis_max(J, K, D)
	).

max_divide_less(D, G, A, B, C) :-
	(   cis_leq(A, n(0)),
	    cis_geq_zero(B)
	->  C=sup
	;   cis_div(D, A, E),
	    cis_div(D, B, F),
	    cis_max(E, F, J),
	    cis_div(G, A, H),
	    cis_div(G, B, I),
	    cis_max(H, I, K),
	    cis_max(J, K, C)
	).

max_times(A, F, B, C, K) :-
	cis_times(A, B, D),
	cis_times(A, C, E),
	cis_max(D, E, I),
	cis_times(F, B, G),
	cis_times(F, C, H),
	cis_max(G, H, J),
	cis_max(I, J, K).

min_divide(D, J, A, C, G) :-
	(   A=n(B),
	    B>0,
	    C=n(_),
	    cis_geq_zero(D)
	->  cis_plus(D, C, E),
	    cis_minus(E, n(1), F),
	    cis_div(F, C, G)
	;   cis_leq(A, n(0)),
	    cis_geq_zero(C)
	->  G=inf
	;   cis_div(D, A, H),
	    cis_div(D, C, I),
	    cis_min(H, I, M),
	    cis_div(J, A, K),
	    cis_div(J, C, L),
	    cis_min(K, L, N),
	    cis_min(M, N, G)
	).

min_divide_less(D, G, A, B, C) :-
	(   cis_leq(A, n(0)),
	    cis_geq_zero(B)
	->  C=inf
	;   cis_div(D, A, E),
	    cis_div(D, B, F),
	    cis_min(E, F, J),
	    cis_div(G, A, H),
	    cis_div(G, B, I),
	    cis_min(H, I, K),
	    cis_min(J, K, C)
	).

min_times(A, F, B, C, K) :-
	cis_times(A, B, D),
	cis_times(A, C, E),
	cis_min(D, E, I),
	cis_times(F, B, G),
	cis_times(F, C, H),
	cis_min(G, H, J),
	cis_min(I, J, K).

num_subsets([], _, A, A, []).
num_subsets([A|E], B, D, G, H) :-
	(   fd_get(A, C, _)
	->  (   domain_subdomain(B, C)
	    ->  F is D+1,
		num_subsets(E, B, F, G, H)
	    ;   H=[A|I],
		num_subsets(E, B, D, G, I)
	    )
	;   num_subsets(E, B, D, G, H)
	).

override(A, C, B, D, E) :-
	call(A, B),
	override_(C, B, D, E).

relation_unifiable([], _, [], A, A).
relation_unifiable([A|D], B, C, F, G) :-
	(   all_in_domain(A, B)
	->  C=[A|E],
	    relation_unifiable(D, B, E, F, G)
	;   relation_unifiable(D, B, C, 1, G)
	).

vns_coeffs_variables([], A, B, [A], [B]).
vns_coeffs_variables([vn(A, D)|E], C, B, G, H) :-
	(   A==B
	->  F is C+D,
	    vns_coeffs_variables(E, F, B, G, H)
	;   G=[C|I],
	    H=[B|J],
	    vns_coeffs_variables(E, D, A, I, J)
	).

choice_order_variable(step, B, A, E, F, G) :-
	fd_get(A, C, _),
	order_dom_next(B, C, D),
	(   A=D,
	    label(E, F, B, step, G)
	;   neq_num(A, D),
	    label([A|E], F, B, step, G)
	).
choice_order_variable(enum, C, A, D, E, F) :-
	fd_get(A, B, _),
	domain_direction_element(B, C, A),
	label(D, E, C, enum, F).
choice_order_variable(bisect, I, A, G, H, J) :-
	fd_get(A, B, _),
	domain_infimum(B, n(C)),
	domain_supremum(B, n(D)),
	E is (C+D)//2,
	(   E=:=D
	->  F is E-1
	;   F=E
	),
	(   #=<(A, F),
	    label([A|G], H, I, bisect, J)
	;   #>(A, F),
	    label([A|G], H, I, bisect, J)
	).

coeffs_variables_const([], [], [], [], A, A).
coeffs_variables_const([C|I], [A|J], B, D, E, K) :-
	(   var(A)
	->  B=[C|G],
	    D=[A|H],
	    F=E
	;   F is E+C*A,
	    B=G,
	    D=H
	),
	coeffs_variables_const(I, J, G, H, F, K).

mulsum(A, D, C, B, F, I) :-
	linsum(A, 0, E, G, []), % phrase
	B is C+D*E,
	H=F,
	lin_mul(G, D, H, I).

optimise(F, H, D, A, E, B) :-
	% C is A,
	metacalled_is(A,C),
	nb_setarg(1, B, n(C)),
	duplicate_term(D-E, I-G),
	tighten(F, G, C),
	once(labeling(H, I)),
	optimise(F, H, D, G, E, B).

label([A|D], C, B, F, G, H, I, J) :-
	(   var(A)
	->  instantiation_error(A)
	;   override(selection, B, A, C, E)
	->  label(D, C, E, F, G, H, I, J)
	;   override(order, F, A, C, K)
	->  label(D, C, B, K, G, H, I, J)
	;   override(choice, G, A, C, L)
	->  label(D, C, B, F, L, H, I, J)
	;   optimisation(A)
	->  label(D, C, B, F, G, [A|H], I, J)
	;   consistency(A, K)
	->  label(D, C, B, F, G, H, K, J)
	;   domain_error(labeling_option, A)
	).
label([], _, A, B, C, D, I, E) :-
	maplist(arg(1), [A, B, C], [F, G, H]),
	(   D==[]
	->  label(E, F, G, H, I)
	;   reverse(D, J),
	    optimise(E, [F, G, H], J)
	).

sum_finite_domains([], [], [], [], A, B, A, B).
sum_finite_domains([C|Q], [A|R], H, G, F, D, S, T) :-
	fd_get(A, _, B, M, _),
	(   B=n(E)
	->  (   C<0
	    ->  I is D+C*E
	    ;   L is F+C*E
	    ),
	    G=J,
	    H=K
	;   C<0
	->  I=D,
	    G=[C*A|J],
	    H=K
	;   L=F,
	    H=[C*A|K],
	    G=J
	),
	(   M=n(N)
	->  (   C<0
	    ->  L is F+C*N
	    ;   I is D+C*N
	    ),
	    J=P,
	    K=O
	;   C<0
	->  L=F,
	    K=[C*A|O],
	    J=P
	;   I=D,
	    J=[C*A|P],
	    K=O
	),
	sum_finite_domains(Q, R, O, P, L, I, S, T).


myinit :-
	make_queue,
	nb_setval('$clpfd_current_propagator', []),
	nb_setval('$clpfd_queue_status', enabled).


?- myinit.

msort(A,B) :- sort(A,B).

must_be(_,_).

cpf_clpfd_propagation( terminating).

between(I,J,X) :-
	J >= I,
	(
	  X = I
	;
	  I1 is I + 1,
	  between(I1,J,X)
	).

% get_attr2(X,Y,Z) :- get_attr(X,Y,Z), fail.
get_attr2(X,Y,Z) :- get_attr(X,Y,Z).

duplicate_term(X,Y) :- copy_term(X,Y).
