/*
  INSTRUCTIONS FOR THE HPROLOG-CRYPTOMINISAT BINDING

  These instructions pass calls to libhplcryptominisat, which glues
  together hProlog and Cryptominisat. Note that these instructions are
  not meant to be called directly; doing so is very likely to provoke
  segfaults (ex. calling sat_get_model/1 before calling
  create_sat_instance/0 is a typical situation for this).
*/


/* 
   Creates a new Cryptominisat solver instance. Note that this will
   first delete the current instance.
 */
hcase(builtin_create_sat_instance_0)
builtin_create_sat_instance_0_label:
profile(builtin_create_sat_instance_0);
{
  if(minisat_new_solver()) {
    locpc += builtin_create_sat_instance_0_len;
    goto_more_exec;
  }
  else
    goto_fail_label;
}

/* 
   Deletes the current Cryptominisat solver instance.
 */
hcase(builtin_delete_sat_instance_0)
builtin_delete_sat_instance_0_label:
profile(builtin_delete_sat_instance_0);
{
  if(minisat_delete_solver()) {
    locpc += builtin_delete_sat_instance_0_len;
    goto_more_exec;
  }
  else
    goto_fail_label;
}

/* 
   Solves the current SAT formula.
 */
hcase(builtin_solve_sat_0)
builtin_solve_sat_0_label:
profile(builtin_solve_sat_0);
{
  if(minisat_solve()) {
    locpc += builtin_solve_sat_0_len;
    goto_more_exec;
  }
  else
    goto_fail_label;
}

/* 
   Adds a new clause to the existing instance. Clauses must be given
   in list form, with a minus used to denote negation, like so:

   sat_add_clause([X1,-X2]).
 */
hcase(builtin_add_sat_clause_1)
builtin_add_sat_clause_1_label:
profile(builtin_add_sat_clause_1);
{
  dlong areg1;
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_add_sat_clause_1));
  
  dlong *list = (dlong *)args(areg1);
  deref(list);

  dlong *head;
  while (is_list(list)) {
    list = get_list_pointer(list);
    head = (dlong *)list[0];
    deref(head); 
    
    if (! is_number(head)) 
      goto_fail_label;

    if (is_real(head)) 
      goto_fail_label;

    if (is_smallint(head)) 
      push_literal(get_smallint(head));
    else
      goto_fail_label;

    list = (dlong *)list[1];
    deref(list);
  }

  minisat_add_clause();
  locpc += builtin_add_sat_clause_1_len;
  goto_more_exec;
}

/* 
   Adds a new XOR clause to the existing instance.
   Clauses are to be passed in the same form as for add_sat_clause/1,
   but the first element should be an x, like so:

   sat_add_clause([x,X1,-X2]).
 */
hcase(builtin_add_xor_clause_1)
builtin_add_xor_clause_1_label:
profile(builtin_add_xor_clause_1);
{
  dlong areg1;
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_add_xor_clause_1));
  
  dlong *list = (dlong *)args(areg1);
  deref(list);

  dlong *head;
  while (is_list(list)) {
    list = get_list_pointer(list);
    head = (dlong *)list[0];
    deref(head); 
    
    if (! is_number(head)) 
      goto_fail_label;

    if (is_real(head)) 
      goto_fail_label;

    if (is_smallint(head)) 
      push_literal(get_smallint(head));
    else
      goto_fail_label;

    list = (dlong *)list[1];
    deref(list);
  }

  minisat_add_xorclause();
  locpc += builtin_add_xor_clause_1_len;
  goto_more_exec;
}

/* 
   sat_get_var_assignment(<int i>, X) assigns to X the value of the
   SAT variable with index i (i.e. either -(i + 1) if it equals false,
   i + 1 if it equals true.
 */
hcase(builtin_sat_get_var_assignment_2)
builtin_sat_get_var_assignment_2_label:
profile(builtin_sat_get_var_assignment_2);
{
  dlong areg1;
  dlong areg2;
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_sat_get_var_assignment_2));
  areg2 = *(areg_type *)(arg_place(locpc,2,builtin_sat_get_var_assignment_2));
  
  int var_idx = get_smallint((dlong *)args(areg1)) - 1;
  int n = minisat_nvars();
  
  dlong *p;
  p = (dlong *)args(areg2);
  deref(p);

  dlong res;
  if (var_idx < n && var_idx >= 0) {
    res = make_smallint(val(var_idx));

    if (is_ref(p)) {
      trail(p,bh,loctrreg);
      *p = res;
    }
    else {
      if (!unify_terms(p,&res,&locmach))
	goto_fail_label;
    }

  }
  else 
    goto_fail_label;
      
  locpc += builtin_sat_get_var_assignment_2_len;
  goto_more_exec;
}

/* 
   The predicate sat_get_model(X) assigns to X a list with the
   assigned values of the variables in the current SAT instance.
 */
hcase(builtin_sat_get_model_1)
builtin_sat_get_model_1_label:
profile(builtin_sat_get_model_1);
{
  dlong areg1 = *(areg_type *)(arg_place(locpc,1,builtin_sat_get_model_1));
  dlong *list = (dlong *)args(areg1);
  dlong *tail;

  deref(list);
  if (! is_ref(list)) 
    goto_fail_label;
  
  *list = make_list(lochreg);

  int n = minisat_nvars();
  int i = 0;

  while (i < n)
    {
      tail = lochreg + 2;
      *lochreg = make_smallint(val(i));
      lochreg++;
      *lochreg = make_list(tail);
      lochreg = tail;
      i++;
    }

  *(lochreg-1) = nil_atom;
  
  locpc += builtin_sat_get_model_1_len;
  goto_more_exec;
}


/* 
   The predicate sat_assign_model([H|T]) unifies the values for the
   variables in the current SAT instance with the elements of the list given as an
   argument, only going as far as the list extends (so
   sat_assign_model([]) will not unify anything, sat_assign_model([X])
   will unify X with the value of variable 1, etc.
 */
hcase(builtin_sat_assign_model_1)
builtin_sat_assign_model_1_label:
profile(builtin_sat_assign_model_1);
{
  dlong areg1 = *(areg_type *)(arg_place(locpc,1,builtin_sat_assign_model_1));

  int i = 0;
  dlong sat;
  dlong *list = (dlong *)args(areg1);
  dlong *head;
  deref(list);

  while(is_list(list)) {
    list = get_list_pointer(list);
    head = (dlong *)list[0];
    deref(head);
 
    sat = make_smallint(check_model_index(i) ? 1 : -1);
    if (is_ref(head)) {
      trail(head,bh,loctrreg);
      *head = sat;
    }
    else {
      if (!unify_terms(head,&sat,&locmach))
	goto_fail_label;
    }


    list = (dlong *)list[1];
    deref(list);
    i++;
  }


  locpc += builtin_sat_assign_model_1_len;
  goto_more_exec;
}

/* 
   The predicate sat_vars(N) unifies N with the amount of variables in
   the current SAT instance.
 */
hcase(builtin_sat_nvars_1)
builtin_sat_nvars_1_label:
profile(builtin_sat_nvars_1);
{

  dlong areg1;
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_sat_nvars_1));

  dlong *p;
  p = (dlong *)args(areg1);
  deref(p);

  dlong nvars = make_smallint(minisat_nvars());
 
  if (is_ref(p)) {
    trail(p,bh,loctrreg);
    *p = nvars;
  }
  else {
    if (!unify_terms(p,&nvars,&locmach))
      goto_fail_label;
  }

  locpc += builtin_sat_nvars_1_len;
  goto_more_exec;
}
