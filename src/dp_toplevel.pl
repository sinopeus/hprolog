%% Toplevel file: initially loaded by dp_boot.pl

%% Bart Demoen Tue Aug 29 11:42:06 CEST 1999
%% last update: very recently

:- sysmodule(sysh,[toplevel/0,
		   sread/3,
		   current_op/3
		  ]).

toplevel :-
	(init),
	catch((sysh:argv, fail ; true),B,sysh:handle_toplevel(B)),
	repeat,
	reset(catch(sysh:toplevel2,Ball,sysh:handle_toplevel(Ball)),
	      Cont,Term),
	once((Cont \== [] ; Term = [])),
	writeln("Shift/1 delimited by toplevel reset/3 - ignored"),
	fail.
toplevel :-
	halt(0).

toplevel2 :-
	seeing(In),
	see(stdin),
	prolog_flag(prompt,_,'?- '),
	hread(Query, Variables),
	see(In),
	treat_query(Query, Variables),
	fail.

handle_toplevel(Ball) :-
	(Ball == 'back to toplevel' ->
	    true
	;
	    write('Uncaught exception'(Ball))
	),
	nl,
	fail.

init :-
	has_a_definition('?-'(_)),
	pred_erase('?-'(_)),
	fail.
init :-
	cputime(T),
	nb_setval('%%prevcputime',T),
	fail.
init :-
	nb_setval('$$throw', '$$none'),
	fail.
init :-
	nb_setval('$operators',[]),
	fail.
init :-
	nb_setval('$$used_libs',[]),
	fail.
init :-
	nb_setval('$$print_depth',20),
	fail.
init :-
	nb_setval('$$print_attvar',yes),
        fail.
init :-
	nb_setval('$$print_module',no),
        fail.
init :-
	nb_setval('$inline',no),
        fail.
init :-
	nb_setval('$$fail_on_error',yes),
	fail.
init :-
	nb_setval('$$message_on_error',yes),
	fail.
init :-
	nb_setval('%%source_optimize',yes),
	fail.
init :-
	nb_setval('%%specialize',yes),
	fail.
init :-
	nb_setval('%%merge',yes),
	fail.
init :-
	nb_setval('$$current_module',user),
	fail.
init.

writelrev([],1).
writelrev([(Q,V)|H],N) :-
	writelrev(H,M),
	bindvars(V),
	write(M), write(': '), write(Q), write('. '), nl,
	N is M + 1.

bindvars([]).
bindvars([X=X|R]) :- bindvars(R).

findnth(N,L,X) :-
	L = [A|R],
	(N == 1 ->
	    X = A
	;
	    M is N - 1,
	    findnth(M,R,X)
	).

rev([],L,L).
rev([X|R],I,O) :- rev(R,[X|I],O).

inst_vars([]).
inst_vars([A=A|R]) :- inst_vars(R).

mod_adorn(Var,_,G) :-
	var(Var), !,
	G = call(Var).
mod_adorn((G1,G2),CurMod,G) :- !,
	G = (G3,G4),
	mod_adorn(G1,CurMod,G3),
	mod_adorn(G2,CurMod,G4).
mod_adorn((G1;G2),CurMod,G) :- !,
	G = (G3;G4),
	mod_adorn(G1,CurMod,G3),
	mod_adorn(G2,CurMod,G4).
mod_adorn((G1 -> G2),CurMod,G) :- !,
	G = (G3 -> G4),
	mod_adorn(G1,CurMod,G3),
	mod_adorn(G2,CurMod,G4).
mod_adorn((Mod:Goal),_,NewG) :-
	!,
	NewG = (Mod:Goal).
mod_adorn(List,_,Out) :- List = [_|_], !, Out = List.
mod_adorn(G1,Mod,G2) :-
	(atom(G1) ; compound(G1)), !,
	add_module(G1,OldMod,G),
	add_module(G3,Mod,G),
	(has_a_definition(G3), OldMod == user ->
	    G2 = G3
	;
	    G2 = G1
	).
mod_adorn(G,_,G).


treat_query(NakedQueryIn, Variables) :-
	(var(NakedQueryIn) ->
	    % NakedQuery = call(NakedQueryIn)
	    telling(Out),
	    tell(stdout),
	    write("% ... 100,000 ............ 1,000,000 years later\n"),
	    write("%\n"),
	    write("%       >> 42 << (last release gives the question)\n"),
	    fail
	;
	    NakedQuery = NakedQueryIn
	),
	module(CurMod),
	mod_adorn(NakedQuery,CurMod,Query),
	'_$savecp'(Before),
        call(Query),
	sysh:dummy,
	'_$savecp'(After),
	telling(Out),
	tell(stdout),
	nl,
	write_answer(Variables),
        tell(Out),
	(Before = After ->
	    tell(stdout), write('Yes'), tell(Out)
	;   no_more_sols
        ),
	telling(Out2),
	tell(stdout),
	nl,
	tell(Out2),
	! .
treat_query(_,_) :-
	telling(Out),
        tell(stdout),
	write(' No more !'), nl, nl,
	tell(Out).

dummy.

no_more_sols :-
	repeat,
        get0(C),
	C \== 32,
	!,
        (
	  C = 10, !
	;
	  skip_line,
	  fail
	) .

skip_line :-
	get0(C),
	( (C = 10 ; C = -1) ->
	    true
	;
	    skip_line
        ).

write_answer(L) :-
	instantvars(L,L2),
 	sysh:trsubs(L2,L3,Subs),
	all_term_variables(L3+Subs,Allvars),
	singletons(Allvars,Allvars,Singletons),
	name(UnderScore,[0'_]),
	instantiatesingletons(Singletons,'$VAR'(UnderScore)),
 	localnumbervars(L3+Subs,26,_),
	sort(L3,L4),
	write_answer2(L4),
	Subs \== [],
	nl,
	write("  where\n"),
	sysh:wlv(Subs),
	fail.
write_answer(_) :-
	% write('Yes').
	prolog_flag(prompt,_,'Yes ').

instantiatesingletons([],_).
instantiatesingletons([X|R],U) :-
	(attvar(X) ->
	    true
	;
	    X = U,
	    instantiatesingletons(R,X)
	).

singletons([],_,[]).
singletons([X|R],A,S) :-
	(twice(X,A) ->
	    singletons(R,A,S)
	;
	    S = [X|SS],
	    singletons(R,A,SS)
	).

twice(X,L) :-
	find1(X,L,R),
	find1(X,R,_).

find1(X,[Y|R],Z) :-
	(X == Y ->
	    Z = R
	;
	    find1(X,R,Z)
	).


%% write_answer(L) :-
%% 	instantvars(L,L2),
%% 	write_answer2(L2),
%%      write('Yes').

instantvars([],[]).
instantvars([Value = Name|R],Out) :-
	(var(Value) ->
	    (attvar(Value) ->
		Out = [Value = Name|NewOut]
	    ;
		Value = '$VAR'(Name),
		Out = NewOut
	    )
	;
	    Out = [Value = Name|NewOut]
	),
	instantvars(R,NewOut).

write_answer2([]) :- ! .
write_answer2([Value = Name|R]) :-
	write(Name), write(' = '),
	write_all_other_equal(R,Value,NewR),
	writeq(Value), nl,
	write_answer2(NewR).

write_all_other_equal([],_,[]).
write_all_other_equal([V1 = Name|R],Value,NewR) :-
	(V1 == Value ->
	    write(Name), write(' = '),
	    write_all_other_equal(R,Value,NewR)
	;
	    NewR = [V1 = Name|NR],
	    write_all_other_equal(R,Value,NR)
	).

idmember(Val,Name,[V = N|R]) :-
	(Val == V ->
	    Name = N
	;
	    idmember(Val,Name,R)
	).

wl([]).
wl([X|R]) :- write(X), wl(R).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reading   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%



vread_dcg(Term,Vars) :-
	native_hal_read:alt_hal_read_dcg(T,_,_),
	(
	  T == eof ->
	  Term = end_of_file,
	  Vars = []
	;
	  T = mercury_error(String,Int) ->
	  wrs(String), write(' at line '), write(Int),
	  seeing(File,_),
	  write(' of file '),
	  write(File),
	  nl, fail
	;
	  T = hal_read_term(Dict,Sterm),
	  Dict = dict(_,Varsl),
	  mkvarlist(Varsl,Vars,VarsDict),
	  mkterm(Sterm,Term,VarsDict)
	).


%%%%%%%%%%%%%%%%%%
% changes for op/3
%%%%%%%%%%%%%%%%%%

current_op(Prec, Assoc, Op) :-
	(atom(Op) ->
	    name(Op,OpIl),
	    ilist_to_string(OpIl,StringOp),
	    native_hal_read:op_table(StringOp, _, Assoc, Prec)
	;
	    string(Op) ->
	    native_hal_read:op_table(Op, _, Assoc, Prec)
	;
	    var(Op) ->
	    native_hal_read:op_table(Op, _, Assoc, Prec)
	).


groundintlist([]).
groundintlist([X|R]) :- integer(X), nonvar(R), groundintlist(R).


%% parse2(TokenList, Prec, Ans, LeftOver) :-
%% 	parse(TokenList, Prec, Ans, LeftOver),
%% 	nothingleft(LeftOver),
%% 			        ! .
%% parse2(TokenList, Prec, Ans, LeftOver) :-
%% 	parse(TokenList, Prec, Ans, LeftOver), ! .


wrs(S) :- sysh:write_atomic(S,off,[]).

hread(Term,Vars) :-
	native_hal_read:hal_read_term(T,_,_),
	(
	  T == eof ->
	  Term = end_of_file,
	  Vars = []
	;
	  T = mercury_error(String,Int) ->
	  wrs(String), write(' at line '), write(Int),
	  seeing(File,_),
	  write(' of file '),
	  write(File),
	  nl, fail
	;
	  T = hal_read_term(Dict,Sterm),
	  Dict = dict(_,Varsl),
	  mkvarlist(Varsl,Vars,VarsDict),
	  mkterm(Sterm,Term,VarsDict)
	), ! .

mkvarlist([],[],[]).
mkvarlist([SName - VarNr|R],Out1,Out2) :-
	(SName == "_" ->
	    Out2 = [VarNr = _Var|SS],
	    mkvarlist(R,Out1,SS)
	;
	    string_to_ilist(SName,IName),
	    atomic2list(Name,IName,0),
	    Out1 = [Var = Name|RR],
	    Out2 = [VarNr = Var|SS],
	    mkvarlist(R,RR,SS)
	).

mkterm(char(A,_), Term,_) :- !, int_to_char(A,Term). % atomic2list(Term,[A],2).
mkterm(atom(A,_), Term,_) :- !, string2atom(Term,A).
mkterm(integer(A,_), Term,_) :- !, Term = A.
mkterm(float(A,_), Term,_) :- !, Term = A.
mkterm(string(A,_), Term,_) :- !,
	Term = A.
mkterm(functor(2,"[|]",Args,_), Term, Vars) :- !,
	Args = [A1,A2],
	mkterm(A1,NewA1,Vars),
	mkterm(A2,NewA2,Vars),
	Term = [NewA1|NewA2].
mkterm(functor(_,NameList,Args,_), Term, Vars) :- !,
	mktermlist(Args,NewArgs,Vars),
	string2atom(Name,NameList),
	Term =.. [Name|NewArgs].
mkterm(var(N,_), Term, Vars) :-
	findvar(N,Vars,Term).

findvar(N,[Vn = V|R],Out) :-
	(N == Vn ->
	    Out = V
	;
	    findvar(N,R,Out)
	).

string2atom(Atom,String) :-
	% now String is a string or a list of ints
	% the reason (a.o.) is the occurrence of "[]" in dp_halread.hal 
	(string(String) ->
	    string_to_ilist(String,IlistS)
	;
	    nl, write('panic string2atom'), nl,
	    IlistS = String
	),
 	string_to_ilist("__M_",IlistM),
%% 	% ugly alternative for the previous line
%% 	name('_M_',M2),
%% 	name('_',M1),
%% 	head(M1,IlistM,M2),
	(substring(IlistM,IlistS,Before,After) ->
	    atomic2list(Batom,Before,2),
	    atomic2list(Aatom,After,2),
	    add_module(Atom,Aatom,Batom)
	;
	    atomic2list(Atom,IlistS,2)
	).


w(X) :- write(X), nl.

substring(Sub,String,Before,After) :-
	(head(Sub,String,Rest) ->
	    After = Rest,
	    Before = []
	;
	    String = [First|NewString],
	    Before = [First|NewBefore],
	    substring(Sub,NewString,NewBefore,After)
	).

head([],R,R).
head([X|R],[X|S],Out) :- head(R,S,Out).

mktermlist([],[],_).
mktermlist([A|RA],[B|RB],Vars) :-
	mkterm(A,B,Vars),
	mktermlist(RA,RB,Vars).

%%%%%%%%%%%%%%%

localnumbervars(Term,A,B) :-
	term_variables(Term,Vars),
	localnumbervarlist(Vars,A,B).

localnumbervarlist([],A,A).
localnumbervarlist([X|R],A,B) :-
	(attvar(X) ->
	    AA = A
	;
	    X = '$VAR'(A),
	    AA is A + 1
	),
	localnumbervarlist(R,AA,B).

