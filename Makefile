# Copyright (C) 1999 Bart Demoen  

clean:
	cd src ; make -i realclean ; cd ..
	cd doc ;  make clean ; cd ..
	cd boot ; make clean ; cd ..


all:
	make -i clean
	cd boot ; make ; cd ..
	cd src ; make all ; cd ..

install:
	cd src; $(MAKE) install; cd ..

