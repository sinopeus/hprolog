/*
Started 18 april 2007
Meant to replace most of the preprocessor PROFILING options
in machine.h and main.c

Todo
   output more precisely the call instructions and the builtins
   eliminate combinations that do not make sense (call putpvar)
   detect unused instructions
*/

:- use_module(library(lists)).

analyse_dump(_,help) :-
	!,
	writeln("Arg 1 of analyse_dump = file name that can be consulted"),
	writeln("Arg2 = list of options - currently available:"),
	writeln("         single instruction count: single"),
	writeln("         double instruction count: double"),
	writeln("         triple instruction count: triple"),
	fail.

analyse_dump(File,Options) :-
	[File],
	analyse_dump(Options).

analyse_dump(Options) :-
	member(Option,Options),
	analyse_dump1(Option),
	fail.
analyse_dump(_).

analyse_dump1(single) :- profile_single.
analyse_dump1(double) :- profile_double.
analyse_dump1(triple) :- profile_triple.


profile_triple :-
	findall(I/F, instr_exec(_,I,F), Is),
	findall(triple(I1,I2,I3),triple(Is,I1,I2,I3),Ds1),
	sort(Ds1,Ds),
	findall(F=triple(I1,I2,I3), (member(triple(I1/_,I2/_,I3/_),Ds),
				     count3(Is,I1,I2,I3,0,F)), FDs),
	sort(FDs,FDss),
	reverse(FDss,FDsss),
	writeln(FDsss),
	fail.

triple([X,Y,Z|_],X,Y,Z).
triple([_|R],X,Y,Z) :- triple(R,X,Y,Z).

count3([_],_,_,_,N,N).
count3([X/_,Y/FY,Z/FZ|R],X,Y,Z,I,O) :-
	!,
	II is I + FZ,
	count3([Y/FY,Z/FZ|R],X,Y,Z,II,O).
count3([_|R],X,Y,Z,I,O) :-
	count3(R,X,Y,Z,I,O).


profile_double :-
	findall(I/F, instr_exec(_,I,F), Is),
	findall(double(I1,I2,F)-P,double(Is,I1,I2,F,0,P),Ds1),
	sort(Ds1,Ds2),
	stripkey(Ds2,Ds2bis),
	count2(Ds2bis,Ds3),
	sort(Ds3,Ds4),
	reverse(Ds4,Ds5),
	writeln(Ds5),
	fail.

stripkey([],[]).
stripkey([A-_|R],[A|S]) :- stripkey(R,S).

count2([],[]).
count2([double(I,J,F)|R],[double(FinalF,I,J)|S]) :-
	count2(R,I,J,F,FinalF,Rest),
	count2(Rest,S).

count2([],_,_,F,F,[]).
count2(In,I,J,InF,OutF,Rest) :-
	In = [D|R],
	D = double(A,B,ABF),
	(A == I, B == J ->
	    InF1 is InF + ABF,
	    count2(R,I,J,InF1,OutF,Rest)
	;
	    Rest = In,
	    OutF = InF
	).


double([X/_,Y/F|_],X,Y,F,P,P).
double([_|R],X,Y,F,I,P) :- II is I + 1, double(R,X,Y,F,II,P).

	

profile_single :-
	findall(I, instr_exec(_,I,_), Is),
	sort(Is,Is1),
	findall((F=I),(member(I,Is1),count1(I,F)),IFs),
	sort(IFs,IFss),
	reverse(IFss,IFsss),
	writeln(IFsss),
	fail.

count1(I,F) :-
	findall(C,instr_exec(_,I,C),Cs),
	sum_list(Cs,F).
