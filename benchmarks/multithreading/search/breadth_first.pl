breadth_search(Space, State, Bound, Solution) :-
  breadth(Space, l(State), Bound, Path),
  sysh:reverse(Path, Solution).

breadth(Space, Tree, Bound, Solution) :-
  expand([], Tree, Tree2, Solved, Solution, Space, Bound),
  (	Solved == true ->
    true
  ;	breadth(Space, Tree2, Bound, Solution)
  ).

expand(Path, l(State), _, true, [State| Path], Space, _) :-
  goal_state(Space, State).
expand(Path, l(State), t(State, Subs), fail, _, Space, Bound) :-
  Bound > 0,
  bagof(l(Next), (next_state(Space, State, Next, _), \+ member_path(Next, [State| Path])), Subs).
expand(Path, t(State,Subs), t(State, Subs2), Solved, Solution, Space, Bound) :-
  expandall([State| Path], Subs, [], Subs2, Solved, Solution, Space, Bound).

expandall(_, [], [Tree| Trees], [Tree| Trees], fail, _, _, _).
expandall(Path, [Tree| Trees], Trees2, Subs2, Solved, Solution, Space, Bound) :-
  (	Bound > 0,
    Bound2 is Bound -1,
    expand(Path, Tree, Tree2, Solved2, Solution, Space, Bound2),
    (	Solved2 == true ->
      Solved = true
    ;	expandall(Path, Trees, [Tree2| Trees2], Subs2, Solved, Solution, Space, Bound)
    )
  ;	expandall(Path, Trees, Trees2, Subs2, Solved, Solution, Space, Bound)
  ).

