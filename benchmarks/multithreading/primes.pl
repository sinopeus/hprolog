run_primes(Threads, Inf, Sup) :-
  Sup > Inf,
  split(Inf, Sup, Threads, Intervals),
  spawn2(Intervals, _, Goals),
  collect(Goals).

split(Inf, Sup, N, Intervals) :-
  Width is (Sup - Inf) / N,
  split(1, N, Inf, Inf, Width, [], Intervals).

split(N, N, Inf, Current, Width, Acc, [Current-Sup| Acc]) :-
  X is Inf + Width*N,
  truncate(X, Sup),
  !.
split(I, N, Inf, Current, Width, Acc, Intervals) :-
  X is Inf + Width*I,
  truncate(X, Sup),
  Current2 is Sup + 1,
  I2 is I + 1,
  split(I2, N, Inf, Current2, Width, [Current-Sup| Acc], Intervals).

spawn2(Intervals, Primes, Goals) :-
  spawn2(Intervals, [], Primes, Goals).

spawn2([], Primes, Primes, []).
spawn2([Inf-Sup| Intervals], Acc, Primes, [(T, primes(Inf, Sup, Acc, Acc2))| Goals]) :-
  spawn(primes(Inf, Sup, Acc, Acc2), once(primes(Inf, Sup, Acc, Acc2)), T),
  spawn2(Intervals, Acc2, Primes, Goals).

collect([]).
collect([(T, primes(Inf, Sup, Acc, Primes))| Goals]) :-
  receive(T, the(primes(Inf, Sup, Acc, Primes))),
  stop(T),
  collect(Goals).

primes(N, M, Primes, Primes) :-
  N > M,
  !.
primes(N, M, Acc, Primes) :-
  (	is_prime(N) ->
    Primes = [N| Primes2]
  ;	Primes = Primes2
  ),
    N2 is N + 1,
  primes(N2, M, Acc, Primes2).

is_prime(2) :- !.
is_prime(Prime):-
  Prime > 2,
  Prime mod 2 =:= 1,
  Sqrt is sqrt(Prime),
  is_prime(3, Sqrt, Prime).

is_prime(N, Sqrt, Prime):-
  (	N > Sqrt ->
    true
  ;	Prime mod N > 0,
    N2 is N + 2,
    is_prime(N2, Sqrt, Prime)
  ).
