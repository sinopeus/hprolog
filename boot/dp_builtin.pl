% Sat Aug 28 17:19:20 CEST 1999
%% Bart Demoen K.U.Leuven
%% 
%% This file contains the Prolog definitions of the builtins not written in C,
%% or should never be inlined - or are defined in some other dp_* file
%% The ones written in C are in dp_lowlevel_builtins.pl
%% Most recent change: recently.

:- sysmodule(sysh,
	  [
	   error/2,

	   '_$savecp'/1, '_$cutto'/1,

	   readln/1,

	   dummy_call/0,

	   call_atomic/1,

	   error/2,
	   
	   term_variables/2, all_term_variables/2,

	   (op)/3,

	   det/1, info_cp/0,

	   sort/2, sort/3, keysort/2,

	   cint/4, object_reference/2, link_pred2func/3,

	   halt/0, end_of_file/0,
	   
	   del_attr/2,
	   put_attr/3, get_attr/3,
	   put_attr/2, get_attr/2,

           length/2,

	   load_files/2,

	   freeze/2, ontouch/2,

	   append/3,
	   append/2,
	   member/2,

	   mgu_variables/3,
	   mgu/3,

	   prolog_flag/3, prolog_flag/2, current_prolog_flag/2,

	   (if)/3, once/1, guard/1, (\+)/1, true/0, repeat/0, repeat/1, fail/0,
	   catch/3, throw/1,

	   intercept/3, send_signal/1,

% 	   callcc/2, continue/1,

  	   reset/3, shift/1,

  	   old_reset/3, old_shift/1,

	   reset_nocont/2, shift_nocont/1,

  	   new_reset/3, new_shift/1, '$newcont$'/3,
	   
   	   %% reset/1, shift/2,
	   call_continuation/1,
	   
	   (:)/2, (;)/2, (->)/2, (^)/2,
	   (',')/2,  % can't denote that anymore
	   
	   name/2, atom_chars/2, atom_concat/3, number_codes/2,

	   numbervars/3, numbervars/4,

	   (is)/2, metacalled_is/2,

	   shell/1,
	   
	   statistics/0, statistics/2, time/1, time/3,
	   measure/5, measure/7,

	   (@<)/2, (@=<)/2, (@>)/2, (@>=)/2, (==)/2, (===)/2, (<<<)/2, (\==)/2, (\=)/2,
	   (<)/2, (>)/2, (=<)/2, (>=)/2, (=:=)/2, (=\=)/2, (=)/2,

	   consult/1, (.)/2, compile/1, 
   	   multi_file/1, load/1,

	   setof/3, bagof/3,
	   findall/3, findall/4,
	   sharing_findall/3, sharing_findall/4,

	   (module)/1,

	   ! /0,
	   use_module/1,

	   maplist/2, maplist/3, maplist/4, maplist/5,
	   filter/3,

	   ancestors/1,

	   sleep/1,
	   flush_output/1,
	  
	   mutable/1,

	   reverse/2,

	   print_code/1,

	   between/3,
	   succ/2,

	   clause/2, clause/3,

	   built_in_predicate/1,

	   profile/1,

	   last/2
	  ]).

c(F) :-
	name(F,LF),
	remsp(LF,NewLF),
	name(mv,MV),
	append([MV,[32,0''],LF,[0'',32,0''],NewLF,[0'']],LCommand),
	name(Command,LCommand),
	shell(Command,Result),
	Result \= 0,
	writeln(compress_failed(F)),
	fail.
c(_) :- halt.

remsp([],[]).
remsp([X|R],Out) :-
	(X == 32 ->
	    remsp(R,Out)
	;
	    Out = [X|S],
	    remsp(R,S)
	).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% intercept/3 as in Ciao I hope
%% started on 29 august 2013 in Istanbul, ICLP13
%% it looks a bit like catch-throw, but there are differences
%%    the stack is not unwound
%%    the "thrown" term is not copied
%%    the handler is immediatly executed
%%    execution continues
%% first test works on Thu Aug 29 21:39:14 CEST 2013
%% after dinner with Mike
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

intercept(Goal,Ball,Handler) :-
	call(Goal),
	sysh:after_intercept,
	dummy(Ball,Handler).

dummy(_,_).

send_signal(Term) :-
	sysh:find_intercept(Ball,Handler),
	Ball = Term,
	call(Handler).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% a request by Tom Schrijvers
%% Thu Nov  1 15:17:25 CET 2012
%% if I would have to redo it, I would do it differently I think
%% Mon Nov  5 09:48:52 CET 2012
%% it is different now :-)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/*
see CW631
*/

reset_nocont(Goal,Term) :-
	call(Goal),
	sysh:after_callcc,
	sysh:asm(getpval(Term,1)).
	
shift_nocont(Term) :- sysh:unwind_stack_callcc.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

reset(Goal,Cont,Term) :-
	call(Goal),
	sysh:after_callcc,
 	sysh:asm(getpval(Cont,1)),
 	sysh:asm(getpval(Term,2)).

shift(Term) :- sysh:construct_delcont(Term).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

old_reset(Goal,Cont,Term) :- reset(Goal,Cont,Term).
	
old_shift(Term) :-
	nop_call, % to force the env not to be moved down
	sysh:nextEP(first,E,P),
	get_all_cc(Cont,E,P),
% 	(get_all_cc(Cont,E,P), fail ; get_all_cc(Cont,E,P)),
	sysh:asm(putpval(Cont,1)),
	sysh:asm(putpval(Term,2)),
	sysh:unwind_stack_callcc.

get_all_cc(call_continuation(L),E,P) :- get_all_cc1(L,E,P).
get_all_cc1(L,E,P) :-
	(points_to_callcc(P) ->
	    L = []
	;
	    L = [TB|RestCC],
	    sysh:get_one_tailbody(E,P,TB),
	    sysh:nextEP(E,NE,NP),
	    get_all_cc1(RestCC,NE,NP)
	).

call_continuation([]).
call_continuation([X|R]) :-
	call_conts(R,X).

call_conts([],X) :-
	call_tailbody1(X).
call_conts([TB|RestCC],X) :-
	call_tailbody1(X),
	call_conts(RestCC,TB).

call_tailbody1(Cont) :- sysh:call_tailbody(Cont).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% new_reset leaves the dc on the stack

new_reset(Goal,Cont,Term) :-
	indirect_call(Goal,OldCont),
	sysh:after_callcc,
	sysh:asm(getpval(Cont,2)),
	sysh:asm(getpval(Term,1)),
	b_setval('$newcont$',OldCont).
	
indirect_call(Goal,OldCont) :-
	nop_call,
%  	sysh:setEinB,
	b_getval('$newcont$',OldCont),
	sysh:current_reg(1,E),
	b_setval('$newcont$',E),
	call(Goal),
	sysh:asm(dealloc_proceed).

new_shift(Term) :- new_shift(Term,_Cont).

new_shift(Term,Cont) :-
	sysh:setEinB,
	b_getval('$newcont$',LastE),
	sysh:current_reg(1,E),
	sysh:current_reg(2,P), % really the CP
	Cont = '$newcont$'(E,P,LastE),
	sysh:end_new_shift(Term,Cont,LastE).

'$newcont$'(_E,_P,_LastE) :- sysh:alt_call_continuation.
'$newcont$'(_E,_P,_LastE) :- sysh:reset_cont, fail.

%% new_reset(Goal,Cont,Term) :-
%% 	b_getval('$newcont$',OldCont),
%% 	indirect_call(Goal),
%% 	sysh:after_callcc,
%% 	sysh:asm(getpval(Cont,2)),
%% 	sysh:asm(getpval(Term,1)),
%% 	b_setval('$newcont$',OldCont).
%% 	
%% indirect_call(Goal) :-
%% 	nop_call,
%% 	sysh:current_reg(1,E),
%% 	b_setval('$newcont$',E),
%% 	call(Goal),
%% 	sysh:asm(dealloc_proceed).
%% 
%% new_shift(Term) :- new_shift(Term,_Cont).
%% 
%% new_shift(Term,Cont) :-
%% 	sysh:setEinB,
%% 	b_getval('$newcont$',LastE),
%% 	sysh:current_reg(1,E),
%% 	sysh:current_reg(2,P), % really the CP
%% 	Cont = '$newcont$'(E,P,LastE),
%% 	sysh:end_new_shift(Term,Cont,LastE).
%% 
%% '$newcont$'(_E,_P,_LastE) :- sysh:new_call_continuation.
%% '$newcont$'(_E,_P,_LastE) :- sysh:reset_cont, fail.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% experimental for the moment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

built_in_predicate(Head) :-
	htoggles(66,Head).

clause(Mod,Head,Body) :-
	functor(Head,Name,Arity),
	concstr(clause,Name,Arity,ClauseName),
	C =.. [ClauseName,Head,Body],
	Mod:C.


clause(Head,Body) :-
	functor(Head,Name,Arity),
	concstr(clause,Name,Arity,ClauseName),
	call(ClauseName,Head,Body).

concstr(C,N,A,O) :-
	name(C,CL),
	name(N,NL),
	name(A,AL),
	append(NL,AL,NAL),
	append(CL,NAL,CNAL),
	name(O,CNAL).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

between(I,J,A) :-
	((integer(I), integer(J)) ->
	    between2(I,J,A)
	;
	    throw(error(between(I,J,A)))
	).

between2(I,J,A) :- I =< J, A = I.
between2(I,J,A) :- I < J, I1 is I + 1, between2(I1,J,A).

succ(A,B) :-
	(var(B) ->
	    integer(A),
	    B is A + 1
	;
	    integer(B),
	    A is B - 1
	).

mutable(X) :- norepshar(X).

print_code(X:Y) :- atom(X), !, add_module(G,X,Y), sysh:print_code(G).
print_code(X) :- sysh:print_code(X).


! .


ancestors(X) :-
	sysh:ancestors(Y),
	mkanc(Y,X).

mkanc([],[]).
mkanc([N,A|R],[N/A|S]) :- mkanc(R,S).


'_$savecp'(B) :- '_$savecp'(B1), B = B1.
'_$cutto'(B) :- '_$cutto'(B).

error(File,Line) :-
	write("Error in file "),
	write(File),
	write(" at line "),
	writeln(Line).

dummy_call.


% mgu_variables - first cut

mgu_variables(A,B,C) :- sysh:mgu_variables(A,B,C1), sort(C1,C).

% mgu - first cut

mgu(A,B,C) :- sysh:mgu(A,B,C).

% ontouch/2 built on the attributed variables

ontouch(X,Goal) :-
	(attvar(X) ->
	    (get_attr(X,ontouch,G2) ->
		put_attr(X,ontouch,(Goal,G2))
	    ;
		put_attr(X,ontouch,Goal)
	    )
	;
	    (var(X) ->
		put_attr(X,ontouch,Goal)
	    ;
		call(Goal)
	    )
	).

ontouch:attr_unify_hook(Goal,Y) :-
	(attvar(Y) ->
	    (get_attr(Y,ontouch,G2) ->
		/* keep ontouch alive */
		put_attr(Y,ontouch,(Goal,G2)),
		call(Goal), call(G2)
	    ;
		/* Y is bound - inherits ontouch */
		put_attr(Y,ontouch,Goal),
		call(Goal)
	    )
	;
	    call(Goal)
	).



% freeze/2 built on the attributed variables

freeze(X,Goal) :-
        (attvar(X) ->
%%             (get_attr(X,freeze,G2) ->
%%                 put_attr(X,freeze,conj__M_sysh(G2,Goal))
%%             ;
%%                 put_attr(X,freeze,Goal)
%%             )
	    sysh:add_attr(X,freeze,Goal,conj__M_sysh(_,_))
        ;
            (var(X) ->
                put_attr(X,freeze,Goal)
            ;
                call(Goal)
            )
        ).
 
%% varfreeze(X,Goal) :-
%%         (attvar(X) ->
%%             (get_attr(X,freeze,G2) ->
%%                 put_attr(X,freeze,conj__M_sysh(G2,Goal))
%%             ;
%%                 put_attr(X,freeze,Goal)
%%             )
%% 	;
%% 	    put_attr(X,freeze,Goal)
%%         ).
 
freeze:attr_unify_hook(Goal,Y) :-
        (attvar(Y) ->
            (get_attr(Y,freeze,G2) ->
                put_attr(Y,freeze,conj__M_sysh(G2,Goal))
            ;
                put_attr(Y,freeze,Goal)
            )
        ;
	    call(Goal)
        ).

conjunction__M_sysh(A,B) :- call(A), call(B).
conj__M_sysh(A,B) :- call(A), call(B).

% attributed variables


put_attr(AttVar,Name,Value) :-
	sysh:put_attr_lowlevel(AttVar,Name,Value).

put_attr(AttVar,Value) :-
	sysh:put_attr_lowlevel(AttVar,Value).

get_attr(AttVar,Value) :-
	sysh:get_attr_lowlevel(AttVar,Value).

get_attr(AttVar,Name,Value) :-
        sysh:get_attr_lowlevel(AttVar,Name,V),
	V = Value.

del_attr(AttVar,Name) :-
	sysh:get_attr_lowlevel(AttVar,Atts),
	delete_att(Atts,Name,NewAtts),
	sysh:put_attr_lowlevel(AttVar,NewAtts).

member_att(Name,Val,[N,A|R]) :-
	(Name == N ->
	    Val = A
	;
	    member_att(Name,Val,R)
	).

delete_att([],_,[]).
delete_att([X|R],N,S) :-
	(X == N ->
	    R = [_|S]
	;
	    R = [Y|T],
	    S = [X,Y|SS],
	    delete_att(T,N,SS)
	).

call_goal_list([]).
call_goal_list([G|Goals]) :- call(G), call_goal_list(Goals).


call_atomic(Goal) :-
	setflag(atomicgoal,true,Old),
	call(Goal),
	setflag(atomicgoal,Old,_),
	nb_getval(delayedgoals,L),
	b_setval(delayedgoals,[]),
	trigger(L).

setflag(F,New,Old) :-
	b_getval(F,Old),
	b_setval(F,New).

trigger([]).
trigger([UL|ULs]) :-
	trigger(ULs), % because they were in the "wrong" order
	deal_with_wakeup2(UL,true).

deal_with_wakeup_of_one(Atts,Value,Cont) :-
	call_all_attr_uhooks_hprolog(Atts,Value),
	call(Cont).

deal_with_wakeup_of_two(Atts1,Value1,Atts2,Value2,Cont) :-
	call_all_attr_uhooks_hprolog(Atts1,Value1),
	call_all_attr_uhooks_hprolog(Atts2,Value2),
	call(Cont).

deal_with_wakeup(UnificationList,Continuation) :-
%% 	nb_getval(atomicgoal,X),
%% 	(X == true ->
%% 	    b_getval(delayedgoals,Old),
%% 	    b_setval(delayedgoals,[UnificationList|Old]),
%% 	    call(Continuation)
%% 	;
%% 	    deal_with_wakeup2(UnificationList,Continuation)
%% 	).
        wake_attvars_hprolog(UnificationList),
	call(Continuation).


deal_with_wakeup2(UnificationList,Continuation) :-
	% 	    pre_wake_attvars_hprolog(UnificationList),
	wake_attvars_hprolog(UnificationList),
	call(Continuation).

pre_wake_attvars_hprolog([]).
pre_wake_attvars_hprolog(List) :-
	List = [Atts,Value|Rest],
	( Rest == [] ->
		call_all_pre_attr_uhooks_hprolog(Atts,Value)
	;
		collect_all_handlers(List,[],Handlers),
		call_all_pre_attr_uhooks_hprolog(Handlers)
	).

collect_all_handlers([],Handlers,Handlers).
collect_all_handlers([Atts,Value|Rest],Acc,Handlers) :-
	add_handlers(Atts,Value,Acc,NAcc),
	collect_all_handlers(Rest,NAcc,Handlers).

add_handlers([],_,Acc,Acc).
add_handlers([Name,AttVal|R],Value,Acc,NAcc) :-
	add_handler(Acc,Name,AttVal,Value,Acc1),
	add_handlers(R,Value,Acc1,NAcc).

add_handler([],Name,AttVal,Value,[Name-[AttVal-Value]]).
add_handler([N-PL|R],Name,AttVal,Value,Acc) :-
	( N == Name ->
		Acc = [ N - [AttVal-Value | PL] | R]
	;
		Acc = [ N-PL | AccT ],
		add_handler(R,Name,AttVal,Value,AccT)
	).

call_all_pre_attr_uhooks_hprolog([]).
call_all_pre_attr_uhooks_hprolog([Name-PL|R]) :-
	G = Name:pre_attr_unify_hook(PL),
	( has_a_definition(G) ->
		call(G)
	;
		true
	),
	call_all_pre_attr_uhooks_hprolog(R).

call_all_pre_attr_uhooks_hprolog([],_).
call_all_pre_attr_uhooks_hprolog([Name,AttVal|R],Value) :-
	G = Name:pre_attr_unify_hook([AttVal-Value]),
	( has_a_definition(G) ->
		call(G)
	;
		true
	),
	call_all_pre_attr_uhooks_hprolog(R,Value).

wake_attvars_hprolog([]).
wake_attvars_hprolog([Atts,Value|Rest]) :-
	call_all_attr_uhooks_hprolog(Atts,Value),
	wake_attvars_hprolog(Rest).

call_all_attr_uhooks_hprolog([],_).
call_all_attr_uhooks_hprolog([Name,AttVal|R],Value) :-
	(R == [] ->
	    Name:attr_unify_hook(AttVal,Value)
	;
	    Name:attr_unify_hook(AttVal,Value),
	    call_all_attr_uhooks_hprolog(R,Value)
	).

call_all_goals([]).
call_all_goals([GL|GLs]) :-
	call_goals(GL),
	call_all_goals(GLs).

call_goals([]).
call_goals([G|Gs]) :-
	call(G),
	call_goals(Gs).


%%%%%%%%%%%%%%%%%%%%%%%%%%


% check whether there are arguments that must go to argv/2
% if so, parse them and do corresponding actions
% argv/0 fails - perhaps not finitely :-)

argv :-
	get_all_args(0,Args),
	do_actions_for_args(Args).

get_all_args(N,Args) :-
	(argv(N,ArgN) ->
	    Args = [ArgN|R],
	    M is N + 1,
	    get_all_args(M,R)
	;
	    Args = []
	).

do_actions_for_args([]).
do_actions_for_args([Option|R]) :-
	do_actions_for_arg(Option,R,NewR),
	do_actions_for_args(NewR).

% at the moment only two options are understood: -l and -g

do_actions_for_arg('-l',R,NewR) :- !, load_files_until_nomore(R,NewR).
do_actions_for_arg('-g',R,NewR) :- !,
	catch(sysh:do_one_goal(R,NewR),Ball,sysh:handle_toplevel(Ball)).


load_files_until_nomore([],[]).
load_files_until_nomore([FO|R],NewR) :-
	(arg_option(FO) ->
	    NewR = [FO|R]
	;
	    % assume FO is a file
	    [FO],
	    load_files_until_nomore(R,NewR)
	).

do_one_goal([],[]).
%% do_one_goal([G|NewR],NewR) :-
%% 	telling(T),
%% 	tell('/tmp/hprologtemp'),
%% 	write(G), put(0'.), nl,
%% 	told,
%% 	tell(T),
%% 	seeing(S),
%% 	see('/tmp/hprologtemp'),
%% 	read(Term),
%% 	seen,
%% 	see(S),
%% 	call(Term).
do_one_goal([G|NewR],NewR) :-
	atom_concat(G,'. ',NG),
	open_stream(NG,atomread,S),
%%	open_stream(NG,stringread,S),  %% why not stringread ?
	read_stream(S,Term),
	close_stream(S),
	call(Term).

readln(L) :-
	get0(X),
	readln(X,L).
readln(X,L) :-
	(X < 11 ->
	    L = []
	;
	    L = [X|R],
	    get0(Y),
	    readln(Y,R)
	).


arg_option('-l').
arg_option('-g').



% C interface

cint(A,B,C,D) :- cinter(A,B,C,D).


object_reference(ObjectName,Reference) :-
	atom(ObjectName),
	cint(0,ObjectName,Ref,666), % last one not used
	Ref = Reference.

link_pred2func(Predicate,FunctionName,Reference) :-
	integer(Reference),
	cint(1,Predicate,FunctionName,Reference).


%% a :-
%% 	object_reference('/home/bmd/hprolog1.8/src/a.so',C),
%% 	link_pred2func(bla1(9),foo1,C),
%% 	link_pred2func(bla2(9,9),foo2,C),
%% 	link_pred2func(bla3(9,9,9),foo3,C).
%%   363  gcc -c a.c
%%   364  gcc -shared a.o -o a.so

module(X) :-
	(var(X) ->
	    nb_getval('$$current_module',X)
	;
	    atom(X),
	    nb_setval('$$current_module',X)
	).


all_flags([inline,fail_on_error,print_attvar,message_on_error,
	   print_module,print_depth,prompt,verbose,very_verbose,optimize,
	   specialize,merge,repshar]).


current_prolog_flag(X,Y) :-
	prolog_flag(X,Y).

prolog_flag(Flag,X) :-
	var(Flag), !,
	all_flags(AllFlags),
	member(Flag,AllFlags),
	prolog_flag(Flag,X,X).
prolog_flag(Flag,X) :-
	all_flags(AllFlags),
	member(Flag,AllFlags),
	!,
	(var(X) ->
	    prolog_flag(Flag,X,X)
	;
	    prolog_flag(Flag,_,X)
	).

prolog_flag(optimize,Current,New) :-
	!,
	nb_getval('%%source_optimize',C),
	Current = C,
	(New == Current ->
	    true
	;
	    ((New == yes ; New == no) ->
		nb_setval('%%source_optimize',New)
	    ;
		write('optimize values are yes/no'),nl, fail
	    )
	).

prolog_flag(specialize,Current,New) :-
	!,
	nb_getval('%%specialize',C),
	Current = C,
	(New == Current ->
	    true
	;
	    ((New == yes ; New == no) ->
		nb_setval('%%specialize',New)
	    ;
		write('specialize values are yes/no'),nl, fail
	    )
	).

prolog_flag(merge,Current,New) :-
	!,
	nb_getval('%%merge',C),
	Current = C,
	(New == Current ->
	    true
	;
	    ((New == yes ; New == no) ->
		nb_setval('%%merge',New)
	    ;
		write('merge values are yes/no'),nl, fail
	    )
	).

prolog_flag(message_on_error,Current,New) :-
	!,
	nb_getval('$$message_on_error',C),
	C = Current,
	(New == Current ->
	    true
	;
	    ((New == yes ; New == no) ->
		nb_setval('$$message_on_error',New)
	    ;
		write('fail_on_error values are yes/no'),nl, fail
	    )
	).
prolog_flag(inline,Current,New) :-
	!,
	nb_getval('$inline',C),
	C = Current,
	(New == Current ->
	    true
	;
	    ((New == yes ; New == no) ->
		nb_setval('$inline',New)
	    ;
		write('fail_on_error values are yes/no'),nl, fail
	    )
	).
prolog_flag(fail_on_error,Current,New) :-
	!,
	nb_getval('$$fail_on_error',C),
	C = Current,
	(New == Current ->
	    true
	;
	    ((New == yes ; New == no) ->
		nb_setval('$$fail_on_error',New)
	    ;
		write('fail_on_error values are yes/no'),nl, fail
	    )
	).
prolog_flag(print_attvar,Current,New) :-
	!,
	nb_getval('$$print_attvar',C),
	C = Current,
	(New == Current ->
	    true
	;
	    ((New == yes ; New == no) ->
		nb_setval('$$print_attvar',New)
	    ;
		write('print_attvar values are yes/no'),nl, fail
	    )
	).
prolog_flag(print_module,Current,New) :-
	!,
	nb_getval('$$print_module',C),
	C = Current,
	(New == Current ->
	    true
	;
	    ((New == yes ; New == no) ->
		nb_setval('$$print_module',New)
	    ;
		write('print_module values are yes/no'),nl, fail
	    )
	).
prolog_flag(print_depth,Current,New) :-
	!,
	nb_getval('$$print_depth',C),
	C = Current,
	integer(New),
	(New == Current ->
	    true
	;
	    New > 0,
	    nb_setval('$$print_depth',New)
	).
prolog_flag(X,Y,Z) :-
	(var(Y), var(Z) ->
	    Y == Z,
	    sysh:internal_prolog_flag(X,YZ,YZ), Y = YZ
	;
	    (var(Y) ->
		sysh:internal_prolog_flag(X,Ynew,Z), Y = Ynew
	    ;
		var(Z),
		sysh:internal_prolog_flag(X,Y,Znew), Z = Znew
	    )
	), ! .
prolog_flag(X,Y,Z) :-
	error_general_builtin(prolog_flag(X,Y,Z),prolog_flag,3).


% special treatment of unknown predicate call

error_unknown_pred(Goal) :-
        (message_on_error ->
	    user_write('Calling an unknown predicate: '),
	    functor(Goal,Name,Arity),
	    user_write(Name/Arity), user_write('  '),
	    user_write(Goal),
	    add_module(Goal,Mod,_),
	    user_write(' in_module'(Mod)),
	    user_nl
	;
	    true
	),
	fail_or_throw(error_unknown(Goal)).

% special treatment of errors during compilation that the user ignored

error_message(N,NameP,Arity) :-
	(N == 0 ->
	    Goal = void_arithmetic_in(NameP/Arity),
	    Mes = 'Void detected during compilation in arithmetic expression'
	;
	    Goal = compilation_of_clause_failed(NameP/Arity),
	    Mes = 'Compiler failed on a clause'
	),
	(message_on_error ->
	    user_write(Mes),
	    user_write(' in predicate '),
	    user_write(NameP/Arity),
	    user_nl
	;
	    true
	),
	fail_or_throw(Goal).


% the following is for dealing with errors in builtins
% you need one for each arity an inlined builtin can have

error_builtin(N,NameP,Arity) :-
	inline_builtin(Name,_,N), !,
	Goal = Name,
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,N,NameP,Arity) :-
	inline_builtin(Name,_,N), !,
	Goal =.. [Name,A1],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,A3,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2,A3],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,A3,A4,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2,A3,A4],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,A3,A4,A5,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2,A3,A4,A5],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,A3,A4,A5,A6,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2,A3,A4,A5,A6],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,A3,A4,A5,A6,A7,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2,A3,A4,A5,A6,A7],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,A3,A4,A5,A6,A7,A8,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2,A3,A4,A5,A6,A7,A8],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,A3,A4,A5,A6,A7,A8,A9,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2,A3,A4,A5,A6,A7,A8,A9],
	error_general_builtin(Goal,NameP,Arity).

error_builtin(A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,N,NameP,Arity) :-
        inline_builtin(Name,_,N), !,
        Goal =.. [Name,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10],
	error_general_builtin(Goal,NameP,Arity).



error_general_builtin(sysh:spawn(X,Y,Z),NameP,Arity) :-
	!,
	Y = (M+G),
	(
	  X == -1 -> Goal = spawn(M,G,Z)
	;
	  X == -2 -> Goal = spawn_link(M,G,Z)
	;
	  Goal = spawn_link(X,M,G,Z)
	),
	(message_on_error ->
	    user_write('Bad call to builtin predicate: '),
	    user_writeq(Goal),
	    user_nl
	;
	    true
	),
	fail_or_throw(Goal,NameP,Arity).
error_general_builtin(Goal,NameP,Arity) :-
	(message_on_error ->
	    user_write('Bad call to builtin predicate: '),
	    user_writeq(Goal),
	    user_write(' in predicate '),
	    user_write(NameP/Arity),
	    extra_info(Goal),
	    user_nl
	;
	    true
	),
	fail_or_throw(Goal,NameP,Arity).


extra_info(sysh:functor(A,B,C)) :-
	!,
	(
	  var(A), var(B), var(C) ->
	  user_write("; functor/3: not all three arguments can be variable")
	;
	  nonvar(C) ->
	  (
	    integer(C) ->
	    user_write("; functor/3: argument 3 not in correct range")
	  ;
	    user_write("; functor/3: argument 3 not an integer")
	  )
	;
	  true
	).
extra_info(_).



error_builtin_arith5(A1,A2,Operator,Name,Arity) :-
        user_write('Bad arithmetic involving ('),
        user_write(A1), user_write('  '), user_write(Operator),
	user_write('  '), user_write(A2),
	user_write(')  in predicate '),
	user_write(Name/Arity),
        user_nl,
        fail.

error_builtin_arith4(A1,Operator,Name,Arity) :-
        user_write('Bad arithmetic involving ('),
        user_write(Operator),  user_write('  '),
	user_write(A1), user_write(')  in predicate '),
	user_write(Name/Arity),
        user_nl,
        fail.

message_on_error :-
	nb_getval('$$message_on_error',M),
	M == yes.

fail_or_throw(Goal,NameP,Arity) :-
	nb_getval('$$fail_on_error',F),
	(F == yes ->
	    fail
	;
	    throw(error_builtin(Goal,in_predicate(NameP/Arity)))
	).

fail_or_throw(Goal) :-
	nb_getval('$$fail_on_error',F),
	(F == yes ->
	    fail
	;
	    throw(Goal)
	).


% some that are used in sysh

user_writeq(Term) :- write_stream(1,Term,[quote,module]).
user_write(Term) :- write_stream(1,Term).
user_put(Term) :- put_stream(1,Term).
user_nl :- put_stream(1,0'\n).
user_get0(C) :- get0_stream(0,C).


% atom_concat/3

atom_concat(A,B,C) :-
	G = atom_concat(A,B,C),
        getl(A,AL,G),
        getl(B,BL,G),
        getl(C,CL,G),
	(var(CL) ->
	    (var(AL) ->
		error_atom_concat(G)
	    ;
		(var(BL) ->
		    error_atom_concat(G)
		;
		    true
		)
	    )
	;
	    true
	),
	concat(AL,BL,CL),
	ungetl(A,AL,G),
	ungetl(B,BL,G),
	ungetl(C,CL,G).


getl(A,AL,G) :-
	(atomic(A) ->
	    atomic2list(A,AL,0)
	;
	    (var(A) ->
		true
	    ;
		error_atom_concat(G)
	    )
	).

ungetl(A,AL,G) :-
	(var(A) ->
	    atomic2list(A,AL,2)
	;
	    (atomic(A) ->
		true
	    ;
		error_atom_concat(G)
	    )
	).

error_atom_concat(G) :- user_write("Illegal call to "), user_write(G), user_nl, fail.

% op/3: to install new operators

op(Prec,Fixity,Name) :-
	integer(Prec),
	Prec >= 0,
	Prec =< 1200,
	(atom(Name) ; char(Name)),
	legalfixity(Fixity,Cat),
	!,
	name(Name,SNameIl),
	ilist_to_string(SNameIl,SName),
	nb_getval('$operators',Ops),
	nb_setval('$operators',[op_table(SName,Cat,Fixity,Prec)|Ops]).
op(Prec,Fixity,Name) :-
	write('Something wrong with call to op/3 - arguments are '),
	write((Prec,Fixity,Name)),nl.

prefix(fx).
prefix(fy).

infix(xfx).
infix(yfx).
infix(xfy).

postfix(xf).
postfix(yf).


legalfixity(Fixity,Cat) :- (prefix(Fixity), Cat = before ; infix(Fixity), Cat = after ; postfix(Fixity), Cat = after).

% once/1

once(X) :- call(X), ! .

% guard/1

guard(Goal) :-
	sysh:enter_guard(B),
	Goal,
	sysh:exit_guard(B).
	

new_call_2(Goal) :-
	sysh:classify_goal(Goal,C),
	new_call_3(C,Goal).

new_call_3(1,G) :- sysh:call(G). % not call(G) - otherwise loop
new_call_3(3,(G1,G2)) :- new_call_2(G1), new_call_2(G2).
new_call_3(4,(G1;G2)) :-
	(G1 = (I->T) ->
	    (new_call_2(I) -> new_call_2(T) ; new_call_2(G2))
	;
	    (new_call_2(G1) ; new_call_2(G2))
	).
new_call_3(5,(I->T)) :- (new_call_2(I) -> new_call_2(T)).
new_call_3(6,if(I,T,E)) :- if(I,T,E).

nc2((G1,G2)) :- !, call(G1), nc2(G2).
nc2(G) :- call(G).


% META CALL

(G1,G2) :-
% 	notbadmetagoal((G1,G2)),
 	notbadmetagoal(G1,G2),
	!,
%% 	nc2(G1),
%% 	nc2(G2).
%% 	new_call_2(G1),
%% 	new_call_2(G2).
	'_$savecp'(B),
	new_call_2(G1,B),
	new_call_2(G2,B).
(G1,G2) :-
	changevartocall((G1,G2),NewG),
	!,
        call(NewG).
(G1,G2) :-
	badmetacallmessage((G1,G2)).

(G1;G2) :-
% 	notbadmetagoal((G1;G2)),
	notbadmetagoal(G1,G2),
	!,
	'_$savecp'(B),
	(G1 = (I -> E) ->
	    (call_2(I,B) -> call_2(E,B) ; call_2(G2,B))
	;
	    (call_2(G1,B) ; call_2(G2,B))
	).
(G1;G2) :-
	changevartocall((G1;G2),NewG),
	!,
        call(NewG).
(G1;G2) :-
	badmetacallmessage((G1;G2)).

(G1->G2) :-
% 	notbadmetagoal((G1->G2)),
	notbadmetagoal(G1,G2),
        !,
	'_$savecp'(B),
	call_2((G1 -> G2),B).
(G1->G2) :-
	changevartocall((G1->G2),NewG),
	!,
        call(NewG).
(G1->G2) :-
	badmetacallmessage((G1->G2)).

(_ ^G2) :-
	call(G2).

badmetacallmessage(G) :-
	error_general_builtin(G,call,1).

A:B :-  module_call(A,B).
%% 	add_module(NewB,A,B),
%% 	call(NewB).

% call/1

notbadmetagoal(G) :-
	nonvar(G),
	notbadmetagoal2(G).

notbadmetagoal2((G1,G2)) :-
	!,
	notbadmetagoal(G1),
	notbadmetagoal(G2).
notbadmetagoal2((G1;G2)) :-
	!,
	notbadmetagoal(G1),
	notbadmetagoal(G2).
notbadmetagoal2((G1->G2)) :-
	!,
	notbadmetagoal(G1),
	notbadmetagoal(G2).
notbadmetagoal2(if(G1,G2,G3)) :-
	!,
	notbadmetagoal(G1),
	notbadmetagoal(G2),
	notbadmetagoal(G3).
notbadmetagoal2(G) :-
	sysh:nonnumber(G).


changevartocall(G,NG) :-
	var(G),
	!,
	NG = call(G).
changevartocall(G,_) :-
        number(G),
        !,
        fail.
changevartocall((G1,G2),(NG1,NG2)) :-
	!,
	changevartocall(G1,NG1),
	changevartocall(G2,NG2).
changevartocall((G1;G2),(NG1;NG2)) :-
	!,
	changevartocall(G1,NG1),
	changevartocall(G2,NG2).
changevartocall((G1->G2),(NG1->NG2)) :-
	!,
	changevartocall(G1,NG1),
	changevartocall(G2,NG2).
changevartocall(if(G1,G2,G3),if(NG1,NG2,NG3)) :-
	!,
	changevartocall(G1,NG1),
	changevartocall(G2,NG2),
	changevartocall(G3,NG3).
changevartocall(G,G).

new_call_2(Goal,B) :-
	sysh:classify_goal(Goal,C),
	new_call_3(C,Goal,B).

new_call_3(0,G,_) :- user_write(wrong_metacall(G)), user_nl, fail.
new_call_3(1,G,_) :- sysh:call(G).  % not call(G) - otherwise loop
new_call_3(2,_,B) :- cutdummy, '_$cutto'(B).
new_call_3(3,(G1,G2),B) :- new_call_2(G1,B), new_call_2(G2,B).
new_call_3(4,(G1;G2),B) :-
	(G1 = (I->T) ->
	    (new_call_2(I,B) -> new_call_2(T,B) ; new_call_2(G2,B))
	;
	    (new_call_2(G1,B) ; new_call_2(G2,B))
	).
new_call_3(5,(I->T),B) :- (new_call_2(I,B) -> new_call_2(T,B)).
new_call_3(6,if(I,T,E),B) :- if(I,T,E,B).


call_2(V,B) :-
        ( var(V) ->
	    user_write('Metacall to variable'),
	    user_nl,
	    fail
	;
	    call3(V,B)
	).

call3((G1,G2),B) :- !,
        call_2(G1,B),
        call_2(G2,B).
call3((I->T;E),B) :- !,
	(call_2(I,B) -> call_2(T,B) ; call_2(E,B)).
call3((I->T),B) :- !,
	(call_2(I,B) -> call_2(T,B)).
call3(if(I,T,E),B) :- !,
	if(I,T,E,B).
call3((G1;G2),B) :- !,
        (call_2(G1,B) ; call_2(G2,B)).
call3(call(G),_) :- !,
        call(G).
call3(!,B) :- !, cutdummy,
        '_$cutto'(B).
call3(G,_) :-
        sysh:call(G).  % this one can't be call(G) - otherwise loop

cutdummy.

% end_of_file/0

end_of_file :-
	tell(stdout),
        nl, write('see_you_later.'), nl,
        halt.


% halt/0

halt :- halt(0).

% if/3

if(A,B,C) :-
	'_$savecp'(GCP),
	if(A,B,C,GCP).

if(A,B,C,GCP) :-
	(
	'_$savecp'(CP),
	  call(A),
	  sysh:disable_cp(CP),
	  call_2(B,GCP)
	;
	  call_2(C,GCP)
	).



% name/2

name(X,Y) :- atomic2list(X,Y,0).

number_codes(Int,Ascis) :-
	(
	  integer(Int) ->
	    name(Int,Ascis)
	;
	  var(Int),
	  name(Int,Ascis),
	  integer(Int)
	).


% atomic2list/3
% arg1 = atomic
% arg2 = list of asci or chars (length 1 atoms)
% arg3 indicates how conversion must be done - it is input
%      it is the superposition of
%            bit0: 0 = arg2 = asci
%                  1 = arg2 = chars
%            bit1: 0 = arg1 = atom/int/float depending on arg2
%                  1 = arg1 = always an atom
% if the first arg is ground, arg3 is not taken into account and
% arg2 is always a list of asci :-)
% def in dp_low...

% term_variables/2

term_variables(Term,Vars) :- term_variables(Term,Vars,[]).

% all_term_variables/2

all_term_variables(Term,Vars) :- all_term_variables(Term,Vars,[]).

% numbervars/3

numbervars(Term,A,B) :-
	term_variables(Term,Vars),
	numbervarlist(Vars,A,B).

numbervarlist([],A,A).
numbervarlist([X|R],A,B) :-
	X = '$VAR'(A),
	AA is A + 1,
	numbervarlist(R,AA,B).

% numbervars/4

numbervars(Term,A,B,[singletons(true)]) :-
	all_term_variables(Term,Vars),
	sing_numbervarlist(Vars,A,B).

sing_numbervarlist([],A,A).
sing_numbervarlist([X|R],A,B) :-
	(delvar(R,X,_,Rest) ->
	    X = '$VAR'(A),
	    AA is A + 1,
	    sing_numbervarlist(Rest,AA,B)
	;
	    X = '$VAR'('_'),
	    sing_numbervarlist(R,A,B)
	).

delvar([],_,Flag,[]) :- nonvar(Flag).
delvar([A|R],X,Flag,Out) :-
	(A == X ->
	    Flag = 1,
	    delvar(R,X,Flag,Out)
	;
	    Out = [A|O],
	    delvar(R,X,Flag,O)
	).


% is/2

A is B :-
	(number(B) ->
	    A = B
	;
	    metacalled_is2(B,A)
	).

metacalled_is(B,A) :-
	(number(B) ->
	    A = B
	;
	    metacalled_is2(B,A)
	).


% it would be better to have these generated, because I always forget some

metacalled_is2(X,_) :- var(X), !, error_general_builtin(metacalled_is(free_variable),call,1).
metacalled_is2(B + C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB + CC.
metacalled_is2(B - C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB - CC.
metacalled_is2(max(B,C),A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is max(BB,CC).
metacalled_is2(min(B,C),A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is min(BB,CC).
metacalled_is2(- C,A) :- !, metacalled_is(C,CC), A is - CC.
metacalled_is2(round(C),A) :- !, metacalled_is(C,CC), A is round(CC).
metacalled_is2(floor(C),A) :- !, metacalled_is(C,CC), A is floor(CC).
metacalled_is2(ceil(C),A) :- !, metacalled_is(C,CC), A is ceil(CC).
metacalled_is2(trunc(C),A) :- !, metacalled_is(C,CC), A is trunc(CC).
metacalled_is2(msb(C),A) :- !, metacalled_is(C,CC), A is msb(CC).
metacalled_is2(~(C),A) :- !, metacalled_is(C,CC), A is ~(CC).
metacalled_is2(-(C),A) :- !, metacalled_is(C,CC), A is -(CC).
metacalled_is2(sqrt(C),A) :- !, metacalled_is(C,CC), A is sqrt(CC).
metacalled_is2(abs(C),A) :- !, metacalled_is(C,CC), A is abs(CC).
% metacalled_is2(sign(C),A) :- !, metacalled_is(C,CC), A is sign(CC).
metacalled_is2(atan(C),A) :- !, metacalled_is(C,CC), A is atan(CC).
metacalled_is2(sin(C),A) :- !, metacalled_is(C,CC), A is sin(CC).
metacalled_is2(cos(C),A) :- !, metacalled_is(C,CC), A is cos(CC).
metacalled_is2(log(C),A) :- !, metacalled_is(C,CC), A is log(CC).
metacalled_is2(exp(C),A) :- !, metacalled_is(C,CC), A is exp(CC).
metacalled_is2(+ C,A) :- !, metacalled_is(C,CC), A = CC.
metacalled_is2(\(C),A) :- !, metacalled_is(C,CC), A is \(CC).
metacalled_is2(B * C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB * CC.
metacalled_is2(B / C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB / CC.
metacalled_is2(B // C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB // CC.
metacalled_is2(B << C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB << CC.
metacalled_is2(B >> C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB >> CC.
metacalled_is2(B /\ C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB /\ CC.
metacalled_is2(B \/ C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB \/ CC.
metacalled_is2(xor(B,C),A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is xor(BB,CC).
metacalled_is2('**'(B, C),A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is '**'(BB, CC).
metacalled_is2(^(B, C),A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is '**'(BB, CC).
metacalled_is2(B mod C,A) :- !, metacalled_is(B,BB), metacalled_is(C,CC), A is BB mod CC.
metacalled_is2(len(L),N) :- !, length(L,N).
metacalled_is2(min(L),N) :- !, N is min(L).
metacalled_is2(max(L),N) :- !, N is max(L).
metacalled_is2(sum(L),N) :- !, N is sum(L).

metacalled_is2(A,B) :- number(A), !, A = B.
metacalled_is2(Term,Result) :-
	call(Term,Result),
	! .
% metacalled_is2(A,B) :- B is A.

min([X|L],Min) :- min(L,Min,X).
min([],Min,Min).
min([I|R],Min,J) :- I =< J -> min(R,Min,I) ; min(R,Min,J).

max([X|L],Max) :- max(L,Max,X).
max([],Max,Max).
max([I|R],Max,J) :- I >= J -> max(R,Max,I) ; max(R,Max,J).

sum(L,O) :- sum(L,0,O).
sum([],I,I).
sum([X|L],I,O) :- J is I + X, sum(L,J,O).



% det/1

det(Goal) :-
	'_$savecp'(B1),
	Goal,
	'_$savecp'(B2),
	(B1 == B2 ->
	    true
	;
	    info_cp(B1)
	).

info_cp :- info_cp(0).

% repeat/0

repeat.
repeat :-
	repeat.

repeat(N) :-
	N > 0,
	repeat1(N).

repeat1(_).
repeat1(N) :- N > 1, M is N - 1, repeat1(M).

% sort/2

sort(X,Y) :- internal_sort2(X,Y1), !, Y = Y1.
sort(X,_) :- iso_check_list(X), !, fail.
sort(X,Y) :- throw(attempt_to_sort_not_a_list(X,Y)).

internal_sort2(X,Y) :- sysh:internal_sort(X,Y).

% keysort/2

keysort(X,Y) :- internal_key_sort2(X,Y1), !, Y = Y1.
keysort(X,_) :- iso_check_list(X), !, fail.
keysort(X,Y) :- throw(attempt_to_keysort_not_a_list(X,Y)).

internal_key_sort2(X,Y) :- sysh:internal_key_sort(X,Y).

iso_check_list(L) :- nonvar(L), iso_check_list2(L).

iso_check_list2([]).
iso_check_list2([_|R]) :- nonvar(R), iso_check_list2(R).

% sort/3

sort(X,Y,Vars) :-
	sysh:internal_numbervars(Vars,0),
	internal_sort2(X,Y1), !,
	sysh:internal_numbervars(Vars,1),
	Y = Y1.
sort(X,_,_) :-
	iso_check_list(X),
	!,
	fail.
sort(X,Y,Vars) :-
	throw('illegal_attempt_to_sort/3'(X,Y,Vars)).

%% % sort/2
%% 
%% sort([],[]).
%% sort([X|R],O) :-
%%         sort(R,Rs),
%%         sinsert(X,Rs,O).
%% 
%% sinsert(X,[],[X]) :- ! .
%% sinsert(X,[A|R],Out) :-
%%         X == A, !,
%%         sinsert(X,R,Out).
%% sinsert(X,[A|R],Out) :-
%%         X @> A, !,
%%         Out = [A|O],
%%         sinsert(X,R,O).
%% sinsert(X,L,[X|L]).

% shell/1

shell(X) :- shell(X,Y), Y = 0.


% statistics/0 statistics/2

statistics(What,Stat) :-
	(
	  What == runtime ->
	  runtimestat(Stat)
	;
	  What == walltime ->
	  walltimestat(Stat)
	;
	  generalstat(What,Stat)
	).

runtimestat([Time,IncrTime]) :-
	% 	nb_getval('%%prevcputime',Tprev),
	cputime(Time),
	% 	nb_setval('%%prevcputime',Time),
	IncrTime = 0. % IncrTime is Time - Tprev.

walltimestat([Time,IncrTime]) :-
	%       nb_getval('%%prevcputime',Tprev),
	unixtime(Time),
	%       nb_setval('%%prevcputime',Time),
	IncrTime = 0. % IncrTime is Time - Tprev.
			
generalstat(runtime,Stat) :- runtimestat(Stat).
generalstat(heap,[HeapTot,HeapCurrentTop,HeapMax]) :- sysh:internalstat(-1,HeapTot,HeapCurrentTop,HeapMax,x).
generalstat(ls,[LsTot,LsCurrentTop,LsMax]) :- sysh:internalstat(-2,LsTot,LsCurrentTop,LsMax,x).
generalstat(trail,[TrailTot,TrailCurrentTop,TrailMax]) :- sysh:internalstat(3,TrailTot,TrailCurrentTop,TrailMax,x).
generalstat(cp,[CpTot,CpCurrentTop,CpMax]) :- sysh:internalstat(4,CpTot,CpCurrentTop,CpMax,x).
generalstat(mmheap,[NrGC,CollectedBytes,NrHexp,TimeGcHeap]) :- sysh:internalstat(5,NrGC,CollectedBytes,NrHexp,TimeGcHeap).
generalstat(lsexp,[NrLsExp,TimeLsExp]) :- sysh:internalstat(6,NrLsExp,TimeLsExp,x,x).
generalstat(trexp,[NrTrExp,TimeTrExp]) :- sysh:internalstat(7,NrTrExp,TimeTrExp,x,x).
generalstat(cpexp,[NrCpExp,TimeCpExp]) :- sysh:internalstat(8,NrCpExp,TimeCpExp,x,x).
generalstat(repshar,RepSharTime) :- sysh:internalstat(13,RepSharTime,x,x,x).
generalstat(shargain,SharGain) :- sysh:internalstat(14,I1,I2,x,x), SharGain is I1 + I2*100000000.
generalstat(minorgc,MinorGC) :- sysh:internalstat(15,MinorGC,x,x,x).
generalstat(symbols_code,[Symbols,CodeSize]) :- sysh:internalstat(9,Symbols,CodeSize,x,x).


statistics :-
	generalstat(heap,[HeapTot,HeapCurrentTop,HeapMax]),
        generalstat(ls,[LsTot,LsCurrentTop,LsMax]),
        generalstat(trail,[TrailTot,TrailCurrentTop,TrailMax]),
        generalstat(cp,[CpTot,CpCurrentTop,CpMax]),

	generalstat(mmheap,[NrGC,CollectedBytes,NrHexp,TimeGcHeap]),
        generalstat(lsexp,[NrLsExp,TimeLsExp]),
        generalstat(trexp,[NrTrExp,TimeTrExp]),
	generalstat(cpexp,[NrCpExp,TimeCpExp]),
	generalstat(repshar,RepSharTime),
	generalstat(shargain,SharGain),
	generalstat(minorgc,MinorGC),
	TimeMM is TimeGcHeap+TimeLsExp+TimeTrExp+TimeCpExp,

        generalstat(symbols_code,[Symbols,CodeSize]),

	cputime(TotCpuTime),

	report_usage('Heap                ',HeapTot,HeapCurrentTop,HeapMax),
	report_usage('Trail               ',TrailTot,TrailCurrentTop,TrailMax),
	report_usage('Local Stack         ',LsTot,LsCurrentTop,LsMax),
	report_usage('Choice Point Stack  ',CpTot,CpCurrentTop,CpMax),
	user_nl,
	report_usage('Number of symbols                : ',Symbols),
	report_usage('Code size in bytes               : ',CodeSize),
	MajorGC is NrGC - MinorGC,
	report_usage('Number of major heap gcs         : ',MajorGC),
	report_usage('Number of minor heap gcs         : ',MinorGC),
	report_usage('Heap bytes freed                 : ',CollectedBytes),
	report_usage('Heap bytes gained by repshar     : ',SharGain),
	report_usage('Number of heap expansions        : ',NrHexp),
	report_usage('Number of local stack expansions : ',NrLsExp),
	report_usage('Number of trail expansions       : ',NrTrExp),
	report_usage('Number of choice point expansions: ',NrCpExp),
	report_usage('Representation sharing time      : ',RepSharTime),
	report_usage('Time in gc and expansions        : ',TimeMM),
	report_usage('Total cputime including above    : ',TotCpuTime),
	user_nl, fail.
statistics.

report_usage(What,Current) :-
        user_write(What),
	(Current = [C1|C2] ->
	    user_write(C1), write_blancs(8,C2), user_write(C2)
	;
	    user_write(Current)
	),
	user_nl.

write_blancs(N,M) :-
	(M =< 0 ->
	    write_blancs(N)
	;
	    MM is M // 10,
	    NN is N - 1,
	    write_blancs(NN,MM)
	).

write_blancs(N) :-
	(N =< 0 ->
	    true
	;
	    NN is N - 1,
	    user_put(0'0),
	    write_blancs(NN)
	).

report_usage(What,Tot,Current,Max) :-
	user_write(What),
	user_write(': total = '), user_write(Tot),
	user_write('   current = '), user_write(Current),
	user_write('   maximal = '), user_write(Max),
	user_nl.



% time/1

%% time(G) :-
%% 	cputime(T1),
%% 	(call(G) -> true ; true),
%% 	cputime(T2),
%% 	T is T2 - T1,
%% 	write(T),
%% 	nl.

time(Goal) :-
	time(Goal, Cpu, Unix),
	write('% '), write(Cpu), write(' CPU in '), write(Unix), write(' seconds'), nl.
% time/3
time(Goal, Cpu, Unix) :-
	cputime(CpuStart),
	unixtime(UnixStart),
	(Goal -> true; true),
	cputime(CpuEnd),
	unixtime(UnixEnd),
	Cpu is (CpuEnd - CpuStart)/1000,
	Unix is (UnixEnd - UnixStart)/1000.

call1(G) :- call(G), !, fail.


% true/0
true.

% fail/0
fail :- fail.

% cutfail. for the trick with disable_cp
cutfail.
cutfail :- fail.


% comparison

A @< B :-
	A @< B.

A @=< B :-
	A @=< B.

A @> B :-
	A @> B.

A @>= B :-
	A @>= B.

A == B :-
	A == B.

===(A,B) :-
	===(A,B).

<<<(A,B) :-
	<<<(A,B).


% comparison

A < B :-
	(atomic(A), atomic(B) ->
	    A < B
	;
	    metacalled_is(B,BB),
	    metacalled_is(A,AA),
	    AA < BB
	).

A =< B :-
	(atomic(A), atomic(B) ->
	    A =< B
	;
	    metacalled_is(B,BB),
	    metacalled_is(A,AA),
	    AA =< BB
	).

A > B :-
	(atomic(A), atomic(B) ->
	    A > B
	;
	    metacalled_is(B,BB),
	    metacalled_is(A,AA),
	    AA > BB
	).

A >= B :-
	(atomic(A), atomic(B) ->
	    A >= B
	;
	    metacalled_is(B,BB),
	    metacalled_is(A,AA),
	    AA >= BB
	).


% unification

A = A.


% \+/1

\+(G) :- call(G), !, fail.
\+(_).


% =:= / 2 .

A =:= B :-
	(atomic(A), atomic(B) ->
	    A =:= B
	;
	    metacalled_is(B,BB),
	    metacalled_is(A,AA),
	    AA =:= BB
	).


% =\= / 2 .

A =\= B :-
	(atomic(A), atomic(B) ->
	    A =\= B
	;
	    metacalled_is(B,BB),
	    metacalled_is(A,AA),
	    AA =\= BB
	).


% X \== Y

X \== Y :-
	(X == Y ->
	    fail
	;   true).

% X \= Y

X \= Y :-
	(X = Y ->
	    fail
	;   true).



% consulting


%% use_module expects an absolute file name
%% it should only be called from within the loader

sysh:use_module(M,_) :- consult_once(M). % ignore the imports at runtime
sysh:use_module(M) :- consult_once(M).

use_module(M,_) :- consult_once(M). % ignore the imports at runtime
use_module(M) :- consult_once(M).


consult_once(File) :-
	nb_getval('$$used_libs',UsedLibs),
	(member(File,UsedLibs) ->
	    true
	;
	    nb_setval('$$used_libs',[File|UsedLibs]),
	    load(File,File)
	).



init_dyn_ops :-
	nb_setval('$operators',[]).


do_loading(_File,WFile) :-
	prolog_flag(verbose,X),
	(X == yes ->
	    sysh:write_atomic('loading ', off, []),
	    sysh:write_atomic(WFile,off, []),
	    sysh:write_atomic(' ...', off, []),
	    put(10),
	    cputime(T1),
	    sysh:load_file(WFile,[]),
	    cputime(T2),
	    T is T2 - T1,
	    sysh:write_atomic('loading ', off, []),
	    sysh:write_atomic(WFile, off, []),
	    sysh:write_atomic(' finished in ', off, []),
	    sysh:write_atomic(T, off, []),
	    sysh:write_atomic(' ms', off, []),
	    nl,nl
	;
	    sysh:load_file(WFile,[])
	),
	(
	  has_a_definition('?-'(_)),
	  '?-'(G),
	  (has_a_definition('?-'(_)) -> pred_erase('?-'(_)) ; true),
	  call(G),
	  fail
	;
	  true
	).


compile(File) :- [File].

strip_extension(File,Extension,Stripped) :-
	atomic2list(File,FileList,0),
	atomic2list(Extension,ExtList,0),
	( concat(StrippedList,ExtList,FileList) ->
	    atomic2list(Stripped,StrippedList,2)
	;
	    Stripped = File
        ).

% always called after the extension was stripped

give_extension(File,Extension,ExtFile) :-
	atomic2list(File,FileList,0),
	atomic2list(Extension,ExtensionList,0),
	concat(FileList,ExtensionList,ExtFileList),
	atomic2list(ExtFile,ExtFileList,2).

concat([],L,L).
concat([X|R],S,[X|T]) :- concat(R,S,T).


[F|Rest] :-
        consult(F),
        consult_list(Rest).


consult_list(L) :-
        nonvar(L),
        (L == [] ->
            true
        ;
            L = [F|Rest],
            consult(F),
            consult_list(Rest)
        ).



%% implementation of catch/3 and throw/1
%% compatible with on_exception and rais_exception in ANSI - but different
%%    order of arguments
%% Do not call any of the internal predicates at other places
%% Origin: "A 20' implementation of catch and throw"
%%                              Bart Demoen CW report nr 99 1989
%% clean_up_block implemented in hProlog

%% requires new builtins:
%%                set_scope_marker/0
%%                unwind_stack/0

%% ideally, the toplevel should be in a catch block so that throws for
%% which no catch exist, are reported

%% Wed Jun  2 14:39:01 CEST 2010
%% Removed the set_scope_marker and replaced by sysh:end_catch/1
%% sysh:unwind_stack was also adapted slightly to reflect that
%% note also that catch/3 has 3 clauses now

catch(Goal,_Catcher,_Handler) :-
	'_$savecp'(Before),
	call(Goal),
	sysh:end_catch(Before).
catch(_Goal,_Catcher,_Handler) :-
	% we come here because of backtracking, not because of throwing
	sysh:cut1fail.
catch(_Goal,Catcher,Handler) :- % is it crucial that this is the
				% last clause for catch/3 ?
	% only because of a throw can we come here, so no check is needed
	nb_getval('$$throw',Ball),
	(
 	  foo(Ball,Catcher) ->
	  nb_setval('$$throw', '$$none'),
	  call(Handler)
	;
	  sysh:unwind_stack
	).

% foo(X,X) :- dummy_call.
foo(X,X).

throw(Ball) :-
	copy_term(Ball,B),
	nb_setval('$$throw',B),
	sysh:unwind_stack.


deal_with_sigint(G) :-
	has_a_definition(user_installed_handler(_)),
	user_installed_handler(Name),
	atom(Name),
	!,
	Handler =.. [Name,G,'SIGINT'],
	call(Handler).
deal_with_sigint(G) :-
	signal_menu_interact(G).

signal_menu_interact(G) :-
	user_write('You interrupted Prolog ... why ? (h for help) '),
	prolog_flag(prompt,_,'   '),
	read_signal_response(R),
	(R = 0'a ->
	    /* abort query */
	    throw('back to toplevel')
	;
	    R = 0'c ->
	    /* continue */
	    call(G)
	;
	    R = 0't ->
	    trace,
	    call(G)
	;
	    R = 0'w ->
	    user_write(G),user_nl,
	    signal_menu_interact(G)
	;
	    R = 0'H ->
	    halt
	;
	    R = 0'b ->
	    sysh:back_trace,
	    signal_menu_interact(G)
	;
	    R = 0'p,
	    prolog_flag(prompt,_,'?- '),
	    read(Goal),
	    call(Goal),
	    fail
	;
	    /* help */
	    signal_help_menu,
	    signal_menu_interact(G)
	).

read_signal_response(R) :-
	seeing(F),
	seen,
	get0(X),
	( X = -1 -> halt; skip(10)), % Exit when end of file
	see(F),
	R = X.


signal_help_menu :-
	user_nl,
	fetch_menu_items(S),
	user_write(S),
	user_nl,
	fail.
signal_help_menu :-
	user_nl.

fetch_menu_items('a abort     - return to toplevel').
fetch_menu_items('b backtrace - show backtrace').
fetch_menu_items('t trace     - continue in trace mode').
fetch_menu_items('p Prolog    - type in a goal to be executed now').
fetch_menu_items('w write     - write interrupted goal').
fetch_menu_items('c continue  - resume interrupted goal').
fetch_menu_items('H HALT      - exit from Prolog').



/*======================================================================*/
/* findall(?Template, +Call, ?List)                                     */
/*      The first word in buffer is the size of the buffer; not used    */
/*      The second word is the pointer to the argument position where   */
/*              the current answer should be be put.                    */
/*              This location is initialized to [] before writing.      */
/*      The third word is a pointer to the next free location in buffer */
/*      The rest of buffer contains the answer (a list)                 */
/*======================================================================*/

findall(Template,Goal,List) :- findall(Template,Goal,List,[]).

sharing_findall(Template,Goal,List) :- sharing_findall(Template,Goal,List,[]).

%% sharing_findall(Template,Goal,List) :-
%% 	(check_sharing_goal(Goal) ->
%% 	    sharing_findall(Template,Goal,List,[])
%% 	;
%% 	    findall(Template,Goal,List,[])
%% 	).

check_sharing_goal(Goal) :-
	Goal =.. [_|Args],
	check_sharing_args(Args).

check_sharing_args([]).
check_sharing_args([A|R]) :-
	(var(A) ->
	    true
	;
	    ground(A)
	),
	check_sharing_args(R).

% findall/4 adapted from something written for XSB
% by Bart Demoen - Christmas period 1996
% inspiration from a very old ALP newsletter and some Portuguese people

iso_check_partial_list(L) :-
	iso_check_partial_list1(L), ! .
iso_check_partial_list(L) :-
	throw(findall-L-not_partial_list).

iso_check_partial_list1(L) :-
	(var(L) ->
	    true
	;
	    iso_check_partial_list_nonvar(L)
	).

iso_check_partial_list_nonvar([]).
iso_check_partial_list_nonvar([_|R]) :-
	iso_check_partial_list1(R).

findall(Template,Goal,List,Tail) :-
	(var(List) ->
	    true
	;
	    iso_check_partial_list(List)
	),
%% 	(
%% 	    notbadmetagoal(Goal), !
%% 	;
%% 	    throw(findall(bad_goal(Goal)))
%% 	),
	sysh:findall_init(I,0),
	(
	  % call(Goal), 
	  catch(Goal,Ball,sysh:findallhandler(Ball,I,Goal)),
	  sysh:findall_add(Template,I),
	  fail
	;
	  gc_findall_get_solutions(L,T,I),
	  List = L,
	  Tail = T
	).

sharing_findall(Template,Goal,List,Tail) :-
	(var(List) ->
	    true
	;
	    iso_check_partial_list(List)
	),
%% 	(
%% 	    notbadmetagoal(Goal), !
%% 	;
%% 	    throw(findall(bad_goal(Goal)))
%% 	),
	sysh:findall_init(I,1),
	(
	  % call(Goal), 
	    catch(Goal,Ball,sysh:findallhandler(Ball,I,Goal)),
	  sysh:findall_add(Template,I),
	  fail
	;
	  gc_findall_get_solutions(L,T,I),
	  List = L,
	  Tail = T
	).

gc_findall_get_solutions(L,T,I) :-
	sysh:findall_get_solutions(L,T,I).

findallhandler(Ball,I,Goal) :-
	notbadmetagoal(Goal), !,
	sysh:findall_cleanup(I),
	throw(Ball).
findallhandler(_,I,Goal) :-
	sysh:findall_cleanup(I),
	throw(findall(bad_goal(Goal))).

% setof/3

%% setof(Template, Goal, Set) :-
%% 	bagof(Template, Goal, Bag),
%% 	sort(Bag, Set0),
%% 	Set = Set0.

setof(Template, Goal, Bag) :-
	excess_vars(Goal, Template, FreeVars), % FreeVars is a list
				% containing the free vars of Goal
	FreeVars \== [], !,
	Key =.. ['$$'| FreeVars], % works only for short FreeVars
	findall( Key - Template, Goal, L),
% 	smash(L,_), % bind (rename) vars in Keys
 	smash([Key-1|L],_), % bind (rename) vars in Keys
	sort(L, Bags),
	!,
	pick(Bags, Key, Bag1),
	Bag = Bag1.
setof(Template, Goal, Bag) :-
	findall( Template, Goal, List),
	List \== [],
	sort(List,Bag).


% bagof/3

% the following is more right than it ever was before, but maybe not quite completely ...

bagof(Template, Goal, Bag) :-
	excess_vars(Goal, Template, FreeVars), % FreeVars is a list
				% containing the free vars of Goal
	FreeVars \== [], !,
	Key =.. ['$$'| FreeVars], % works only for short FreeVars
	findall( Key - Template, Goal, L),
	smash(L,_), % bind (rename) vars in Keys
	keysort(L, Bags),
	!,
	pick(Bags, Key, Bag).
bagof(Template, Goal, Bag) :-
	findall( Template, Goal, Bag),
	Bag \== [].

smash([],_) :- ! .
smash([Key - _|Rest],VarList) :-
	term_variables(Key,V),
	loc_append(V,_,VarList),
	smash(Rest,VarList).

%%%%%%%%%%%%%%%%%%%%%%%%

mkeysort([],[]).
mkeysort([X-Y|R],Out) :-
	mkeysort(R,RS),
	keyinsert(RS,X,Y,Out).

keyinsert([],X,Y,[X-Y]).
keyinsert([Z|R],X,Y,Out) :-
	Z = (A- _),
	( A @< X ->
	    Out = [Z|NO],
	    keyinsert(R,X,Y,NO)
	;
	    Out = [(X-Y),Z|R]
	).

excess_vars(T,_,_) :-
	var(T), !,
	error(excess_vars("goal_is_free")).
excess_vars(X^P,Y,L) :- !, excess_vars(P,(X,Y),L).
excess_vars(Goal,Template,L) :-
	term_variables(Goal,Vg),
	term_variables(Template,Vt),
	var_diff(Vg,Vt,L).

var_diff([],_,[]).
var_diff([X|R],S,Out) :-
	(member_var(X,S) ->
	    var_diff(R,S,Out)
	;
	    Out = [X|NewOut],
	    var_diff(R,S,NewOut)
	).

member_var(X, [Y|_]) :-  X == Y, ! .
member_var(X, [_|T]) :- member_var(X,T).


pick(Bags,Key,Bag) :-
	Bags = [KeyFirst-ValFirst|Bs],
	picksame(Bs,KeyFirst,BagFirst,Rest),
	(
	  Bag = [ValFirst|BagFirst],
	  Key = KeyFirst,
	  (Rest == [] ->
	      !
	  ;
	      true
	  )
	;
	  pick(Rest,Key,Bag)
	).

picksame([],_,[],[]) :- ! .
picksame(Bs,KeyFirst,BagFirst,Rest) :-
	Bs = [Key-Val|RestBs],
	(KeyFirst == Key ->
	    BagFirst = [Val|RestBagFirst],
	    picksame(RestBs,KeyFirst,RestBagFirst,Rest)
	;
	    BagFirst = [],
	    Rest = Bs
	).


%%%%%%%%

atom_chars(Atom,L) :-
        (var(Atom) ->
	    atomic2list(Atom,L,3)
	;
	    atom(Atom),
	    atomic2list(Atom,LL,0),
	    ints2chars(LL,L)
	).
	   
ints2chars([],[]).
ints2chars([I|Is],[C|Cs]) :- atomic2list(C,[I],2), ints2chars(Is,Cs).



%%%%%%%%%%

% multi_file/1: to be used as a query
% the argument is a list of clauses

multi_file(Clauses) :-
	nb_getval('$multifile',OldClauses),
	concat(OldClauses,Clauses,NewClauses),
	nb_setval('$multifile',NewClauses),
	tell('tmp_multi.pl'),
	write_clauses(NewClauses),
	close('tmp_multi.pl'),
	['tmp_multi.pl'],
	fail.
multi_file(_).

write_clauses([]) :- nl.
write_clauses([Cl|Clauses]) :-
	(
	    write(Cl), put(0'.), nl, fail
	;
	    write_clauses(Clauses)
	).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% hProlog specific predicates to go along with my odd benchmarks
%%
%% measure/5 is supposed to measure the user time (not including gctime)
%% and the garbage collection time of Goal minus Dummy
%% the Dummy can at the call of measure/4 just be true if there is no dummy loop
%% to subtract
%%
%% By Bart Demoen & Phuong-Lan Nguyen - 8 February 2001
%% Comments to bmd@cs.kuleuven.ac.be - thanks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

measure(Goal,Dummy,Utime,Gtime,hProlog) :-
	measure(Goal,Dummy,Utime,Gtime,_NGs,_GcBytes,hProlog).

measure(Goal,Dummy,Utime,Gtime,NGs,GcBytes,hProlog) :-
	generalstat(mmheap,[NrGC1,CollectedBytes1,_NrHexp1,TimeGcHeap1]),
        cputime(T1),
        call(Goal),
        cputime(T2),
	generalstat(mmheap,[NrGC2,CollectedBytes2,_NrHexp2,TimeGcHeap2]),
        cputime(T3),
	call(Dummy),
	cputime(T4),
	generalstat(mmheap,[NrGC3,CollectedBytes3,_NrHexp3,TimeGcHeap3]),
        Utime1 is T2 - T1 - (T4 - T3),
        Gtime is TimeGcHeap2 - TimeGcHeap1 - (TimeGcHeap3 - TimeGcHeap2),
	Utime is Utime1 - Gtime,
	NGs is (NrGC2-NrGC1) - (NrGC3-NrGC2),
	subtractcollectedbytes(CollectedBytes2,CollectedBytes1,Diff1),
	subtractcollectedbytes(CollectedBytes3,CollectedBytes2,Diff2),
	subtractcollectedbytes(Diff1,Diff2,GcBytes).

% only called with arg1 >= arg2
subtractcollectedbytes([A1|B1],[A2|B2],Out) :-
	!,
	B3 is B1-B2,
	(B3 < 0 ->
	    A3 is A1-A2-1,
	    (A3 == 0 ->
		Out is B3 + 100000000
	    ;
		B4 is B3 + 100000000,
		Out = [A3|B4]
	    )
	;
	    A3 is A1-A2,
	    (A3 == 0 ->
		Out = B3
	    ;
		Out = [A3|B3]
	    )
	).
subtractcollectedbytes([A1|B1],C,Out) :-
	!,
	subtractcollectedbytes([A1|B1],[0|C],Out).
subtractcollectedbytes(A,B,Out) :-
	Out is A - B.



%%%%%%%%%%%%%%%%%%%%
% loading facility - used by consult
%%%%%%%%%%%%%%%%%%%%

dep('.pl',['.w']).
dep('',['.w']).

dep_goal('.pl','.w', FilePl, FileW) :-
	sysh:init_dyn_ops,
	compile:compile(FilePl,FileW,[compact,listing]),
	sysh:init_dyn_ops.

dep_goal('.w',_, FileW, FileOrig) :-
	sysh:do_loading(FileOrig, FileW).

dep_goal('',_, FileOrig, FileW) :-
	sysh:init_dyn_ops,
	compile:compile(FileOrig, FileW,[compact,listing]),
	sysh:init_dyn_ops.





consult(File) :-
	compile:getfilename(File,_,FileA),
	load(FileA,File).

load(File,OrigFile) :-
        dep(Suffix, DepSuffs),
	strip_suffix(File,Suffix,WithSuffix,NoSuffix),
	file_exists(WithSuffix), !,
	% now we can build a list with time stamps of all files
	build_time_stamps([Suffix|DepSuffs],NoSuffix,TimeStamps),
	TimeStamps = [WithSuffix-Filesuf-Filetime|RestTimeStamps],
	decide_on_transitions(RestTimeStamps,Filetime,[WithSuffix-Filesuf],RevTransitions),
	loc_reverse(RevTransitions,[],Transitions),
	do_transitions(Transitions,OrigFile).
load(File,OrigFile) :-
	\+(file_exists(File)),
	sysh:give_extension(File,'.w',DotW),
	file_exists(DotW), !,
	sysh:do_loading(OrigFile,DotW).
load(_File,OrigFile) :-
	write(OrigFile), write(' could not be loaded with any of the extensions '),
	findall(E,(sysh:dep(E,_), E \== ''),Exts),
	write(Exts), write(' or no extension '),nl,
	fail.


do_transitions([File-Suf|R],OrigFile) :-
	do_transitions(R,File,Suf,OrigFile).

do_transitions([],File,Suf,OrigFile) :-
	dep_goal(Suf,_,File,OrigFile).
do_transitions([File2-Suf2|R],File1,Suf1,OrigFile) :-
	dep_goal(Suf1,Suf2,File1,File2),
	do_transitions(R,File2,Suf2,OrigFile).


decide_on_transitions([],_,In,In).
decide_on_transitions([File2-Filesuf2-Filetime2|R],Filetime1,In,Out) :-
	(Filetime2 =< 0 ->
	    % File2 did not exist
	    decide_on_transitions(R,Filetime1,[File2-Filesuf2|In],Out)
	;
	    (Filetime2 > Filetime1 ->
		% File2 is newer and takes over
		decide_on_transitions(R,Filetime2,[File2-Filesuf2],Out)
	    ;
		% File2 is older and from here everything needs to be redone
		push_actions(R,[File2-Filesuf2|In],Out)
	    )
	).

push_actions([],In,In).
push_actions([File-Filesuf-_Filetime|R],In,Out) :-
	push_actions(R,[File-Filesuf|In],Out).


build_time_stamps([],_,[]).
build_time_stamps([Suf|Sufs],Naked,[NakedSuf-Suf-Time|R]) :-
	sysh:give_extension(Naked,Suf,NakedSuf),
	(Suf == '.w' ->
	    sysh:file_time_stamp(NakedSuf,Time,1)
	;
	    sysh:file_time_stamp(NakedSuf,Time,0)
	),
	build_time_stamps(Sufs,Naked,R).

strip_suffix(File,Suffix,WithSuffix,NoSuffix) :-
	sysh:strip_extension(File,Suffix,NoSuffix),
	sysh:give_extension(NoSuffix,Suffix,WithSuffix).


%%%%%%%%%%%%%%%%%%%%%%%%

%% pi(3.141592653589793).
%% 
%% gcd(X,Y,Z) :-
%% 	A is X mod Y,
%% 	(A =:= 0 ->
%% 	    Z = Y
%% 	;
%% 	    gcd(Y,A,Z)
%% 	).

%%%%%%%%%%%%%%%%%%%%%%%%

load_files([],_).
load_files([File|Files],Conditions) :-
	compile:getfilename(File,_,FileName),
	(member(if(not_loaded),Conditions) ->
	    consult_once(FileName)
	;
	    [FileName]
	),
	load_files(Files,Conditions).

%%%%%%%%%%%%%%%%%%%%%%%%

member__M_sysh(_,_) :-
	sysh:asm(member_entry),
	sysh:asm(member_retry),
	sysh:asm(execute(loc_mem__M_sysh(_,_),loc_mem__M_sysh(_,_))),
	sysh:asm(end_clauses).

loc_mem(X,[X|_]).
loc_mem(X,[_|R]) :- loc_mem(X,R).

member(Element, List) :-
 	sysh:member(Element, List).
% 	loc_mem(Element, List).


%  the next one because of bootstrap reasons: the old optimizer uses sysh:mem
sysh:mem(X,Y) :- sysh:member(X,Y).

%%%%%%%%%%%%%%%%%%%%%%%%

append__M_sysh(_,_,_) :-
	sysh:asm(append_entry),
	sysh:asm(execute(loc_append__M_sysh(_,_,_),loc_append__M_sysh(_,_,_))),
	sysh:asm(end_clauses).

loc_append([],L,L).
loc_append([X|R],S,[X|T]) :- loc_append(R,S,T).

append(X,Y,Z) :- sysh:append(X,Y,Z).

append([],[]).
append([X|Xs],L) :-
	append(X,T,L),
	append(Xs,T).

%%%%%%%%%%%%%%%%%%%%%%%%

last(_,_) :-
	sysh:asm(last_entry),
	sysh:asm(execute(loc_last__M_sysh(_,_),loc_last__M_sysh(_,_))),
	sysh:asm(end_clauses).


loc_last([H|T], X) :-
	'$last1'(T, H, X).
'$last1'([], X, X).
'$last1'([H|T], _, X) :-
	'$last1'(T, H, X).
	
%%%%%%%%%%%%%%%%%%%%%%%%

reverse__M_sysh(_,_) :-
	sysh:asm(reverse_entry),
	sysh:asm(execute(loc_reverse__M_sysh(_,_,_),loc_reverse__M_sysh(_,_,_))),
	sysh:asm(end_clauses).

reverse(I,O) :- loc_reverse(I,[],O).

loc_reverse([],I,I).
loc_reverse([A|R],I,O) :- loc_reverse(R,[A|I],O).

%%%%%%%%%%%%%%%%%%%%%%%%

% length/2 - should actually be in a lib

length__M_sysh(_,_) :-
	sysh:asm(length_entry),
	sysh:asm(execute(loc_length__M_sysh(_,_),loc_length__M_sysh(_,_))),
	sysh:asm(end_clauses).

length(L,Len) :-
	L \== Len,
	(var(Len) ->
	    length(L,0,Len)
	;
	    Len >= 0,
	    length(L,0,Len), !
	).

length([],I,I).
length([_|R],I,O) :- II is I + 1, length(R,II,O).

loc_length(X,Y) :- length(X,Y).

%%%%%%%%%%%%%%%%%%%%%%%%

filter([],_,[]).
filter([X|R],G,Out) :-
	(call(G,X) ->
	    Out = [X|S],
	    filter(R,G,S)
	;
	    filter(R,G,Out)
	).

maplist(B, A) :- maplist1(A, B).
maplist(C, A, B) :- maplist1(A, B, C).
maplist(D, A, B, C) :- maplist1(A, B, C, D).
maplist(E, A, B, C, D) :- maplist1(A, B, C, D, E).

maplist1([], _).
maplist1([B|C], A) :-
	call(A, B),
	maplist1(C, A).

maplist1([], [], _).
maplist1([B|D], [C|E], A) :-
	call(A, B, C),
	maplist1(D, E, A).

maplist1([], [], [], _).
maplist1([B|E], [C|F], [D|G], A) :-
	call(A, B, C, D),
	maplist1(E, F, G, A).

maplist1([], [], [], [], _).
maplist1([B|F], [C|G], [D|H], [E|I], A) :-
	call(A, B, C, D, E),
	maplist1(F, G, H, I, A).


% -- Misc. stuff added because logic engines needed it.

sleep(Seconds) :-
	sysh:sleep(Seconds).

flush_output(Stream) :-
	sysh:flush_output(Stream).

%%%%%%%%%%%%%%%%%%%%%%%%

profile(What) :-
	        (
		  What == on ->
		  htoggles(17,_)
		;
		  What == off ->
		  htoggles(22,_)
		;
		  What = dump(File),
		  atom(File),
		  htoggles(23,File)
		).
