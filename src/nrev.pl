% :- module(nrev).

/*  The naive reverse benchmark */
% :- pragma(nod e b u g ).

t :-
	(go;go;go;go), fail.



go :-
	bench(100000).



%% nrev([],X) => X = [].
%% nrev([X|Rest],Ans) =>
%% 	nrev(Rest,L), app(L,[X],Ans).
%% 
%% app([],L1,L2) => L1 = L2.
%% app([X|L1],L2,Out) =>
%% 	Out = [X|L3],
%% 	app(L1,L2,L3).

nrev([],[]).
nrev([X|Rest],Ans):-
	nrev(Rest,L), app(L,[X],Ans).

app([],L,L).
app([X|L1],L2,[X|L3]):-
	app(L1,L2,L3).

%% app(X,Y,Z) :- append__M_sysh(X,Y,Z).

%% nrev(In,Out) :-
%% 	(In == [] ->
%% 	    Out = []
%% 	;
%% 	    In = [X|Rest],
%% 	    nrev(Rest,L), app(L,[X],Out)
%% 	).
%% 
%% 
%% app(A,L2,C) :-
%% 	(A == [] ->
%% 	    C = L2
%% 	;
%% 	    A = [X|L1],
%% 	    C = [X|L3],
%% 	    app(L1,L2,L3)
%% 	).


bench(Count):-
	statistics(runtime,[T0,_]),
	dodummy(Count),
	statistics(runtime,[T1,_]),
	dobench(Count),
	statistics(runtime,[T2,_]),
	report(Count,T0,T1,T2).

dobench(Count):-
	data(List),
	repeat(Count),
	nrev(List,_),
	fail.
dobench(_).


int((X,Y)) :- !, int(X), int(Y).
int(true) :- !.
int(H) :- clause(H,B), int(B).


dodummy(Count):-
	data(List),
	repeat(Count),
	dummy(List,_),
	fail.
dodummy(_).


dummy(_,_).

data(X):- data(X,30).

data([],0).
data([a|Y],N):-
	N > 0, 
	N1 is N-1, 
	data(Y,N1).

repeat(_N).
repeat(N):-
	N > 1, 
	N1 is N-1, 
	repeat(N1).

report(Count,T0,T1,T2) :-
	Time1 is T1-T0,
	Time2 is T2-T1,
	Time  is Time2-Time1,           /* Time spent on nreving lists */
        Lips1 is (496000.0*Count)/Time,
	Lips is round(Lips1),
	write(bench(nrev,Count,Time,Lips)),write('.'),
	nl.

