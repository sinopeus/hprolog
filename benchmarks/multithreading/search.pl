:- ['search/salt.pl'].
:- ['search/hill_climbing.pl'].
:- ['search/depth_first.pl'].
:- ['search/breadth_first.pl'].

reverse(X, Y) :-
  sysh:rev(X,[], Y).

run_hill_climbing(Liters, Jug1, Jug2, MaxDepth) :-
  initial_state(Initial),
  hill_search((Liters, Jug1, Jug2), Initial, MaxDepth, X, Y).

run_depth_first(Liters, Jug1, Jug2, MaxDepth) :-
  initial_state(Initial),
  depth_search((Liters, Jug1, Jug2), Initial, MaxDepth, X).

run_breadth_first(Liters, Jug1, Jug2, MaxDepth) :-
  initial_state(Initial),
  breadth_search((Liters, Jug1, Jug2), Initial, MaxDepth, X).

run_competitive_or(Liters, Jug1, Jug2, MaxDepth) :-
  initial_state(Initial),
  hub(Hub),
  spawn_link(Hub, (X, Y),
    (hill_search((Liters, Jug1, Jug2), Initial, MaxDepth, X, Y) -> true; fail), T1),
  spawn_link(Hub, X,
    (depth_search((Liters, Jug1, Jug2), Initial, MaxDepth, X) -> true; fail), T2),
  spawn_link(Hub, X,
    (breadth_search((Liters, Jug1, Jug2), Initial, MaxDepth, X) -> true; fail), T3),
  receive(Hub, Z),
  stop(Hub).
