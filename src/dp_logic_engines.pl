%% Logic engines related predicates.
%% Its predicate engine_toplevel/0 is executed by
%% default to start up a new logic engine.
%% engine_toplevel/0 gets an engine's start goal and
%% calls it.

%% Timon Van Oververveldt Thu Feb 17 15:01:13 CET 2011
%% later changes by Bart Demoen

:- sysmodule(sysh,[
		   spawn/3,
		   spawn_link/3,
		   spawn_link/4,
		   send/1,
		   send/2,
		   receive/1,
		   receive/2,
		   receive/3,

		   % Logtalk syntax:
		   threaded/1,

		   % Pipes
		   piped/2
		   
		  ]).

sysh:engine_toplevel :-
        sysh:engine_get_start_goal(ConcatGoal),
	Pattern + Goal = ConcatGoal,
	catch(Goal,Ball,sysh:handle_threaded_toplevel_exception(Ball)),
	send(Pattern),
	fail.
sysh:engine_toplevel :-
	sysh:low_send(_, no),
	fail.
sysh:engine_toplevel :- halt(0).

sysh:handle_threaded_toplevel_exception(Ball) :-
	sysh:low_send(_,exception(Ball)),
	halt(0).

% We must use this wrapper because engine_get_start_goal_low is unsafeheap.
sysh:engine_get_start_goal(X) :- sysh:engine_get_start_goal_low(X).

% -- Logic engines: creation of engines & messaging
is_hub_id(Id) :- 1 is Id /\ 1.

is_engine_id(Id) :- 0 is Id /\ 1. %% \+ is_hub_id(Id).

spawn_link(Pattern, Goal, Pid) :- sysh:spawn(-2, (Pattern+Goal), Pid).

spawn_link(Parent, Pattern, Goal, Pid) :- sysh:spawn(Parent, (Pattern+Goal), Pid).

spawn(Pattern, Goal, Pid) :- sysh:spawn(-1, (Pattern+Goal), Pid).

% Push message to inbox of engine Engine
send(Engine, Msg) :- sysh:low_send(Engine, the(Msg)).

% Push message to inbox of parent
send(Msg) :- send(_, Msg).

% Wrapper to ensure the arguments are in the right place when growing the heap
sysh:low_receive_wrapper(Hub, From, Msg) :- sysh:low_receive(Hub, From, Msg).

% Receive a message from:
% - Hub if hub is a hub id
% - the current engine's inbox if Hub is not a hub id
% Only receive messages from From if From is ground,
% else receive any message and unify From with the sender ID
receive(Hub, From, Msg) :-
	sysh:low_receive_wrapper(Hub, From, X),
	check_error(X),
	Msg = X. % Unification

receive(From, Msg) :- receive(From, From, Msg).

receive(Msg) :- receive(_, _, Msg).

check_error(X) :-
	(var(X) ->
	    true
	;
	    X = exception(Ball) ->
	    throw(Ball)
	;
	    true
	).


% == LOGIC ENGINES: LOGTALK SYNTAX == %
% Logtalk threaded/1 for competitive OR goals
threaded((Goal; Goals)) :-
	!,
	threaded_or((Goal; Goals)).
% Logtalk threaded/1 for independent AND goals
threaded((Goal, Goals)) :-
	!,
	threaded_and((Goal, Goals)).

% threaded/1 should also work with a single goal
threaded(Goal) :- threaded_and((Goal,true)).

% Creates a hub, calls all goals in seperate threads & fetches the first result
threaded_or(Goals) :-
	hub(Hub),
	( ( % If we succeed, stop the hub
	      goals_to_list(or, Goals, GoalsList, MarkedPatterns, Length),
	      threaded_call_goals(Hub, GoalsList, MarkedPatterns),
	      threaded_or_result(Hub, Length, MarkedPatterns)
	  ) ->
	    stop(Hub)
	; % If we fail, stop the hub, then really fail
	    stop(Hub),
	    fail
	).

% Creates a hub, calls all goals in seperate threads & fetches all results
threaded_and(Goals) :-
	hub(Hub),
	( ( % If we succeed, stop the hub
	      goals_to_list(and, Goals, GoalsList, MarkedPatterns, Length),
	      threaded_call_goals(Hub, GoalsList, MarkedPatterns),
	      threaded_and_result(Hub, Length, MarkedPatterns)
	  ) ->
	    stop(Hub)
	; % If we fail, stop the hub, then really fail
	    stop(Hub), 
	    fail
	).

% Split conjunctions or disjunctions of goals into a list of goals and a list of marked patterns.
goals_to_list(Type, Goals, GList, PList, Length) :-
	goals_to_list(Type, Goals, 1, GList, PList, Length).

% Split a disjunction of goals into a list of goals and a list of marked patterns
goals_to_list(AndOr, Goals, C, [Goal | GList], [(C, Pattern) | PList], Length) :-
	(AndOr = or, Goals = (GivenGoal ; Rest) ; AndOr = and, Goals = (GivenGoal , Rest)),
	!,
	CPP is C+1,
	( GivenGoal = (Goal -> Pattern) ->
	    true
	;
	    term_variables(GivenGoal, Pattern),
	    %Pattern = GivenGoal,
	    Goal = GivenGoal
	),
	goals_to_list(AndOr, Rest, CPP, GList, PList, Length).
% Split a final goal into a list with the goal and a list with the marked pattern
goals_to_list(_, (GivenGoal), C, [Goal], [(C, Pattern)], C) :-
	( GivenGoal = (Goal -> Pattern) ->
	    true
	;
	    term_variables(GivenGoal, Pattern),
	    %Pattern = GivenGoal,
	    Goal = GivenGoal
	).

% Call all goals in a disjunctive goal list
threaded_call_goals(_Hub, [], []).
threaded_call_goals(Hub, [Goal | Goals], [Pattern | Patterns]) :-
	spawn_link(Hub, Pattern, (Goal,!), _),
	threaded_call_goals(Hub, Goals, Patterns).

% Get the result of a competitive-OR call
threaded_or_result(Hub, NosLeft, Patterns) :-
	receive(Hub, Message), % Get a message
	( Message == no -> % Message is 'no', try again for another message
	    NosLeft > 1, % Fail as soon as we have gotten all no's without a result
	    NewNosLeft is NosLeft - 1,
	    threaded_or_result(Hub, NewNosLeft, Patterns)
	;
	    Message = the(Result), % Message is a valid result
	    memberchk(Result, Patterns) % Unify it with the right goal
	).

% Get the results of a independent-AND call
threaded_and_result(Hub, Length, Patterns) :-
	threaded_and_result_all(Hub, Length, Length, Patterns).

threaded_and_result_all(_, 0, ResultsLeft, _) :- !, ResultsLeft == 0.
threaded_and_result_all(Hub, NosLeft, ResultsLeft, Patterns) :-
	NosLeft >= ResultsLeft, % Fail as soon as we have gotten more no's then results
	receive(Hub, Message),
	( Message == no -> % Message is 'no', try again for another message
	    NewNosLeft is NosLeft - 1,
	    threaded_and_result_all(Hub, NewNosLeft, ResultsLeft, Patterns)
	;
	    Message = the(Result), % Message is a valid result
	    NewResultsLeft is ResultsLeft - 1,
	    memberchk(Result, Patterns), % Unify it with the right goal
	    threaded_and_result_all(Hub, NosLeft, NewResultsLeft, Patterns) % Get all other results
	).

% -- Pipes

piped(Goals, ID) :-
	term_variables(Goals, Vars),
	pipe_create(Goals, Vars, ID),
	pipe_results(ID, Vars).

pipe_create(Goals, Vars, End) :-
	hub(End),
	spawn_pipe_stages(End, Vars, Goals, [Head|IDs]),
	link_pipe_stages([Head | IDs], End),
	send(Head, _),
	send(Head, pipe_done).

spawn_pipe_stages(End, Vars, (Goal, Goals), [ID | IDs]) :-
	!,
	spawn_link(End, [], sysh:pipe_node(Vars, Goal), ID),
	spawn_pipe_stages(End, Vars, Goals, IDs).
spawn_pipe_stages(End, Vars, (Goal), [ID]) :-
	spawn_link(End, [], sysh:pipe_node(Vars, Goal), ID).

link_pipe_stages([Node], End) :-
	  !,
	  send(Node, End).
link_pipe_stages([Node, Next | IDs], End) :-
	send(Node, Next),
	link_pipe_stages([Next | IDs], End).

sysh:pipe_node(Vars, Goal) :-
	% Get the previous and next node in the pipe
	receive(the(Next)),
	repeat,
	receive(_, the(In)),
	(In == pipe_done -> % We shouldn't expect more results
	    send(Next, pipe_done), !, fail % Send the message to the next node
	; Vars = In
	),
	Goal, % Call our goal to bind some variables
	send(Next, Vars), % Send our newly bound variables to the next node in the pipe
	fail. % Backtrack to produce more results OR receive new variable bindings

pipe_results(End, Vars) :-
	repeat,
	receive(End, In),
	(
	  In == the(pipe_done) -> !, fail
	; the(Vars) = In
	).


% Everything needed for piped_findall - but not needed for interface

%% piped_findall(Pattern, Goals, Results) :-
%% 	term_variables(Goals, Vars),
%% 	pipe_create(Goals, Pattern+Vars, ID),
%% 	pipe_all_results(ID, Results),
%% 	stop(ID).
%% 
%% 
%% pipe_all_results(End, Results) :-
%% 	receive(End, In),
%% 	(
%% 	  In == the(pipe_done) -> Results = []
%% 	; In == no -> pipe_all_results(End, Results)
%% 	; the(Result+_) = In,
%% 	  Results = [Result|Rest],
%% 	  pipe_all_results(End, Rest)
%% 	).

% Utility ...

memberchk(Result, [Result | _]) :- !.
memberchk(Result, [_ | Patterns]) :-
	memberchk(Result, Patterns).

