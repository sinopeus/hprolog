#include "toinclude.h"
#include "builtins.h"
#include "mach_macros.h"
#include <stdlib.h>

#define MAYBESHARABLE 0
#define NOTSHARABLE (struct hentry *)-1
#define BUSY (struct hentry *)-2
#define set_maybesharable(x) x = MAYBESHARABLE
#define set_nonsharable(x) x = NOTSHARABLE
#define is_nonsharable(x) (x == NOTSHARABLE)
#define maybe_sharable(x) (x == 0)
#define is_sharable(x) ((x != 0) && (x != NOTSHARABLE) && (x != BUSY))
#define is_busy(x) (x == BUSY)
#define set_busy(x) x = BUSY

#define CYCLE 23

#define hash_a_string 11
#define hash_a_bigint 12
#define hash_a_struct 13
#define hash_a_list   14
#define hash_a_real   15

extern void deal_with_tr_overflow(struct machine *machp);

struct hentry
{
  dlong hashvalue;
  struct hentry * next;
  dlong *heapp;
} hentry;

static dlong allocated, freed, hash_fail;
static struct hentry **cached_hash = 0;
struct hentry **hashed_terms = 0;

static dlong HASHED_TERMS_SIZE;
static dlong non_null_hash_entries, hash_buckets, number_of_lists, number_of_structs,
  number_of_numbers, number_of_strings;

/*-------------------------------------------------------------------------*/
static void init_hashed_terms(dlong i)
{
  //  if (i > 200000) HASHED_TERMS_SIZE = 50021; else HASHED_TERMS_SIZE = 1003;
  HASHED_TERMS_SIZE = i/10;
  if (HASHED_TERMS_SIZE < 100000) HASHED_TERMS_SIZE = 99991;
  else
    if (HASHED_TERMS_SIZE < 200000) HASHED_TERMS_SIZE = 199999;
  else
    if (HASHED_TERMS_SIZE < 400000) HASHED_TERMS_SIZE = 399989;
  else
    if (HASHED_TERMS_SIZE < 800000) HASHED_TERMS_SIZE = 799999;
  else
    if (HASHED_TERMS_SIZE < 2000000) HASHED_TERMS_SIZE = 1999993;
  else
    HASHED_TERMS_SIZE = 2999999;

  hashed_terms = (struct hentry **)calloc(HASHED_TERMS_SIZE,sizeof(dlong));
  allocated += HASHED_TERMS_SIZE*sizeof(dlong);
  if (! hashed_terms) 
    fatal_message("Could not allocate hashed_terms ... Exiting !\n");
} /* init_hashed_terms */

/*-------------------------------------------------------------------------*/
#define chasederef(p,cp) \
  cp = 0; \
  while (is_ref(p) && (p != (dlong *)*p)) \
    { cp = p; p = (dlong *)*p; }

/*-------------------------------------------------------------------------*/
static dlong equal_bigints(dlong l, dlong *p, dlong *q)
{
  while (l--)
    if (*p++ != *q++) return(0);
  return(1);
} /* equal_bigints */

/*-------------------------------------------------------------------------*/
static dlong equal_hashed_terms_rec(dlong *p1, dlong *p2, dlong *startheap, struct machine *machp)
{
  dlong *cp1, *cp2;
  struct hentry *hp1, *hp2;
  dlong i;

  chasederef(p1,cp1);
  chasederef(p2,cp2);

  if (cp1 && cp2 && (cp1 == cp2)) return(1);
  if (tag(p1) != tag(p2)) return(0);

  if (p1 == p2) return(1); // to catch simples ...

  if (cp1 && cp2 && is_sharable(cached_hash[cp1-startheap]) && is_sharable(cached_hash[cp2-startheap]))
    return(cached_hash[cp1-startheap] == cached_hash[cp2-startheap]);

  switch (tag(p1))
    {
    case LIST:
      p1 = get_list_pointer(p1);
      p2 = get_list_pointer(p2);

      /* now do the cycle prevention */

      if (cp1 && cp2)
	{
	  uunconditional_value_trail(cp1,machp->TR,machp);
	  *cp1 = (dlong)cp2;
	}

      if (p1 == p2) return(1);
      if (! equal_hashed_terms_rec(p1,p2,startheap, machp)) return(0);
      return(equal_hashed_terms_rec((++p1),(++p2),startheap, machp));

    case NUMB:
      if (is_real(p1) && is_real(p2))
	return(get_real(p1) == get_real(p2));

      if (is_bigint(p1) && is_bigint(p2))
	{
	  p1 = get_number_pointer(p1);
	  p2 = get_number_pointer(p2);
	  if (p1[1] == p2[1]) return(equal_bigints(bigint_size(p1[1]),p1+2,p2+2));
	  return(0);
	}
      break;

    case STRING:
      {
	dlong len;
	p1 = get_string_pointer(p1);
	p2 = get_string_pointer(p2);
	if (p1 == p2) return(1);
	if (*p1 != *p2) return(0);
	len = get_len_from_string_header(*p1);
	return(equal_strings(len,(char *)(p1+1),(char *)(p2+1)));
      }

    case STRUCT:
      p1 = get_struct_pointer(p1);
      p2 = get_struct_pointer(p2);
      while (is_ref(*p1)) p1 = (dlong *)(*p1);
      while (is_ref(*p2)) p1 = (dlong *)(*p2);
      if (p1 == p2) return(1);
      if (*p1 != *p2) return(0);
      if (is_sharable(cached_hash[p1-startheap]) && is_sharable(cached_hash[p2-startheap]))
	return(cached_hash[p1-startheap] == cached_hash[p2-startheap]);


      /* now do the cycle prevention */

      uunconditional_value_trail(p1,machp->TR,machp);
      *p1 = (dlong)p2;

      i = get_funct_arity(*p2);
      while (i--)
	if (! equal_hashed_terms_rec((++p1),(++p2),startheap, machp)) return(0);
      return(1);
    }

  return(0);
} /* equal_hashed_terms_rec */


/* the following also appears in symtab.c - but I keep a duplicate here for now */

static void untrail_till(dlong there, struct machine *machp)
{
  dlong *until = machp->limit_trail + there;
  dlong *tr = machp->TR;

  while (tr > until)
    {
      dlong *q; q = (dlong *)*(--tr);
      if (q) *q = *(--tr);
    }
  machp->TR = tr;

} /* untrail_till */

static dlong equal_hashed_terms(dlong *p1, dlong *p2, dlong *startheap, struct machine *machp)
{
  dlong t;
  dlong trailtop;

  trailtop = machp->TR-machp->limit_trail;
  t = equal_hashed_terms_rec(p1, p2, startheap, machp);
  untrail_till(trailtop, machp);
  return(t);
} /* equal_hashed_terms */


#define update_bucket \
{                                              \
cached_hash[p-startheap] = ph;                 \
if (termold > termnew) ph->heapp = termnew;    \
return;                                        \
}                                              \

#define HSIZE 50000

struct hchain
{
  struct hentry * room;
  struct hchain * next;
} hchain;

static struct hchain *start_chain;
static struct hentry *current_hroom;
static dlong current_hentry;

static void free_hashed_terms()
{
  dlong i;
  free(hashed_terms);
  freed += HASHED_TERMS_SIZE*sizeof(dlong);

  while (start_chain != 0)
    {
      struct hchain *next_chain = start_chain->next;
      free(start_chain->room);
      freed += HSIZE*sizeof(struct hentry);
      free(start_chain);
      freed += sizeof(struct hchain);
      start_chain = next_chain;
    }

} /* free_hashed_terms */

static void init_chain()
{
  start_chain = malloc(sizeof(struct hchain));
  allocated += sizeof(struct hchain);

  if (! start_chain) fatal_message("malloc failed in init_chain ... Exiting\n");
  current_hroom = start_chain->room = malloc(HSIZE*sizeof(struct hentry));
  allocated += HSIZE*sizeof(struct hentry);

  if (! current_hroom) fatal_message("malloc failed in init_chain ... Exiting\n");
  start_chain->next = 0;
  current_hentry = 0;
} /* init_chain */

static INLINE struct hentry *malloc_hentry()
{
  hash_buckets++;
  if (current_hentry < HSIZE)
    return(current_hroom + (current_hentry++));

  {
    struct hchain *old_chain = start_chain;

    start_chain = malloc(sizeof(struct hchain));
    if (! start_chain) fatal_message("malloc failed in malloc_hentry ... Exiting\n");
    allocated += sizeof(struct hchain);

    current_hroom = start_chain->room = malloc(HSIZE*sizeof(struct hentry));
    if (! current_hroom) fatal_message("malloc failed in init_chain ... Exiting\n");
    allocated += HSIZE*sizeof(struct hentry);

    start_chain->next = old_chain;
    current_hentry = 1;
    return(current_hroom);
  }
} /* malloc_hentry */

static void save_info(dlong hashval, dlong *p, dlong *startheap, dlong type, struct machine *machp)
{
  dlong index;
  struct hentry *ph, *ph1;

  index = hashval % HASHED_TERMS_SIZE;
  if (index < 0) index = -index;

  ph = hashed_terms[index];
  while (ph != 0)
    {
      if (ph->hashvalue == hashval)
	{ // check whether equal
	  dlong *termold, *termnew, *p1, *p2;
	  termold = ph->heapp;
	  termnew = p;

	  if (type == hash_a_struct)
	    {
	      if (equal_hashed_terms((dlong *)make_struct_p(termold),
				     (dlong *)make_struct_p(termnew),startheap, machp))
		update_bucket;
	    }
	  else if (type == hash_a_list)
	    {
	      if (equal_hashed_terms(termold,termnew,startheap, machp)) update_bucket;
	    }
	  else if (type == hash_a_real)
	    {
	      p1 = termold; p2 = termnew;
	      if ((p1[0] == p2[0]) && (p1[1] == p2[1]) && (p1[2] == p2[2])) update_bucket;
	    }
	  else if (type == hash_a_bigint)
	    {
	      p1 = termold; p2 = termnew;
	      if ((p1[0] == p2[0]) && (p1[1] == p2[1]) &&
		equal_bigints(bigint_size(p1[1]),p1+2,p2+2)) update_bucket;
	    }
	  else if (type == hash_a_string)
	    {
	      p1 = termold; p2 = termnew;
	      if ((p1[0] == p2[0]) &&
		  equal_strings(get_len_from_string_header(p1[0]),(char *)(p1+1),(char *)(p2+1)))
		update_bucket;
	    }
	  else fatal_message("Unexpected type in save_info ... Exiting\n");
	}
      ph = ph->next;
    }

  ph1 = hashed_terms[index];
  if (ph1 == 0) non_null_hash_entries++;
  ph = malloc_hentry();
  hashed_terms[index] = ph;
  ph->hashvalue = hashval;
  ph->next = ph1;
  ph->heapp = p;
  cached_hash[p-startheap] = ph;
  return;
  
} /* save_info */

static dlong term_hash_repshar_rec(dlong *p, dlong *startheap, dlong depth, struct machine *machp)
{
  dlong *q;
  dlong h;

  /* first go to the end of the chain and then decide what to do
     if any intermediate cell is trailed, then "fail"
     if any intermediate cell is busy, return CYCLE
     if at the end it is an attvar, then "fail"
  */

  if (hash_fail) return(0);

  if (depth > 1000) { hash_fail = 1 ; return(0); }

  depth++;

  q = 0;
  while (is_ref(p))
    {
      if (is_nonsharable(cached_hash[p-startheap])) {hash_fail = 1; return(0);}
      if (is_busy(cached_hash[p-startheap])) return(CYCLE);
      q = p;
      p = (dlong *)*p;
      if (p == q) return((dlong)p);
    }

  // q points to a heap cell containing p, or == 0

  if (p == q) return((dlong)p);

  if (is_attributed(p)) {hash_fail = 1; return(0);}

  if (has_atom_tag(p))
    return(((dlong)p >> 4)*((dlong)p >> 4));

  if (q && is_nonsharable(cached_hash[q-startheap])) {hash_fail = 1; return(0);}
  if (q && is_busy(cached_hash[q-startheap])) return(CYCLE);

  if (is_bigint(p))
    {
      q = p = get_number_pointer(p);
      if (is_nonsharable(cached_hash[p-startheap])) {hash_fail = 1; return(0);}

      p++;
      if (!bigint_onheap(*p)) {hash_fail = 1; return(0); }

      if (q && (! maybe_sharable(cached_hash[q-startheap])))
        {
          struct hentry *hp;
          hp = cached_hash[q-startheap];
          return(hp->hashvalue);
        }

      h = p[0] + p[1];
      save_info(h,q,startheap,hash_a_bigint, machp);
      return(h);
    }

  if (is_string(p))
    {
      q = p = get_string_pointer(p);
      if (is_nonsharable(cached_hash[p-startheap])) {hash_fail = 1; return(0);}

      if (q && (! maybe_sharable(cached_hash[q-startheap])))
        {
          struct hentry *hp;
          hp = cached_hash[q-startheap];
          return(hp->hashvalue);
        }

      h = p[0] + p[1];
      save_info(h,q,startheap,hash_a_string, machp);
      return(h);
    }

  if (is_real(p))
    {
      q = get_number_pointer(p);
      if (is_nonsharable(cached_hash[q-startheap])) {hash_fail = 1; return(0);}

      if (q && (! maybe_sharable(cached_hash[q-startheap])))
        {
          struct hentry *hp;
          hp = cached_hash[q-startheap];
          return(hp->hashvalue);
        }

      h = get_real(p);
      save_info(h,q,startheap,hash_a_real, machp);
      return(h);
    }

  if (is_struct(p))
    {
      dlong ar;

      q = p = get_struct_pointer(p);
      if (is_nonsharable(cached_hash[q-startheap])) {hash_fail = 1; return(0);}
      if (is_busy(cached_hash[q-startheap])) return(CYCLE);

      if (! maybe_sharable(cached_hash[q-startheap]))
	{
	  struct hentry *hp;
	  hp = cached_hash[q-startheap];
	  return(hp->hashvalue);
	}
      ar = get_funct_arity(*p);

      if (get_norepshar(get_funct_symb(*p)))
	{
	  set_nonsharable(cached_hash[p-startheap]);
	  hash_fail = 1;
	  return(0);
	}

      h = *p;
      set_busy(cached_hash[q-startheap]);
      while (ar)
	{
	  p++;
	  if (is_nonsharable(cached_hash[p-startheap]))
	    { hash_fail = 1;
	      set_nonsharable(cached_hash[q-startheap]);
	      return(0);
	    }
	  h += ar*term_hash_repshar_rec(p,startheap,depth, machp);
	  if (hash_fail) 
	    { set_nonsharable(cached_hash[p-startheap]);
	      set_nonsharable(cached_hash[q-startheap]);
	      return(0);
	    }
	  ar--;
	}
      set_maybesharable(cached_hash[q-startheap]);
      save_info(h,q,startheap,hash_a_struct, machp);
      return(h);
    }

  if (is_list(p))
    {
      // q points to the cell with p, unless q == 0, but here it
      // should not be the case

      if (q == 0)
	fatal_message("term_hash_repshar_rec: q != 0 violated ... Exiting\n");

      if (is_nonsharable(cached_hash[q-startheap])) {hash_fail = 1; return(0);}
      if (is_busy(cached_hash[q-startheap])) return(CYCLE);

      if (is_sharable(cached_hash[q-startheap]))
	{
          struct hentry *hp;
          hp = cached_hash[q-startheap];
          return(hp->hashvalue);
        }
      set_busy(cached_hash[q-startheap]);
      p = get_list_pointer(p);
      h = 17;
      h += term_hash_repshar_rec(p,startheap,depth, machp);
      if (hash_fail)
	{ set_nonsharable(cached_hash[p-startheap]);
	  set_nonsharable(cached_hash[q-startheap]);
	  return(0);
	}
      h = h + 2*term_hash_repshar_rec(++p,startheap,depth, machp);
      if (hash_fail)
        { set_nonsharable(cached_hash[p-startheap]);
	  set_nonsharable(cached_hash[q-startheap]);
          return(0);
        }
      set_maybesharable(cached_hash[q-startheap]);
      save_info(h,q,startheap,hash_a_list, machp);
      return(h);
    }

  fatal_message("Big bug: term_hash_repshar_rec sees strange type ... Exiting !\n");
  return(0);
} /* term_hash_repshar_rec */

/* must be callled with a pointer to the heap */

static void term_hash_repshar(dlong *p, dlong *startheap, struct machine *machp)
{
  dlong ignore_result;
  hash_fail = 0;
  ignore_result = term_hash_repshar_rec(p,startheap,0, machp); //ignore return value
} /* term_hash_repshar */


/*-------------------------------------------------------------------------*/

static INLINE void absorb_list(dlong *p, dlong *startheap)
{
  if (is_sharable(cached_hash[p-startheap]))
    {
      struct hentry *hp;
      number_of_lists++;
      hp = cached_hash[p-startheap];
      *p = *(hp->heapp);
    }
} /* absorb_list */


static INLINE void absorb_number(dlong *p, dlong *startheap)
{
  dlong *q = get_number_pointer(*p);
  if (is_sharable(cached_hash[q-startheap]))
    {
      struct hentry *hp;
      number_of_numbers++;
      hp = cached_hash[q-startheap];
      *p = make_number_pointer((hp->heapp));
    }
} /* absorb_number */

static INLINE void absorb_string(dlong *p, dlong *startheap)
{
  dlong *q = get_string_pointer(*p);
  if (is_sharable(cached_hash[q-startheap]))
    {
      struct hentry *hp;
      number_of_strings++;
      hp = cached_hash[q-startheap];
      *p = make_string_pointer((hp->heapp));
    }
} /* absorb_string */

static INLINE void absorb_struct(dlong *p, dlong *startheap)
{
  dlong *q = get_struct_pointer(*p);
  if (is_sharable(cached_hash[q-startheap]))
    {
      struct hentry *hp;
      number_of_structs++;
      hp = cached_hash[q-startheap];
      // if (q != hp->heapp) 
	*p = make_struct_p(hp->heapp);
    }
} /* absorb_struct */

static INLINE void absorb_struct_cautiously(dlong *p, dlong *startheap, dlong *h)
{
  dlong *q = get_struct_pointer(*p);
  if ((startheap <= q) && (q < h) && is_sharable(cached_hash[q-startheap]))
    {
      number_of_structs++;
      struct hentry *hp;
      hp = cached_hash[q-startheap];
      *p = make_struct_p(hp->heapp);
    }
} /* absorb_struct_cautiously */


void representation_sharing(struct machine *machp, dlong arity)
{
  dlong *p, *q;
  dlong t1, t2, t3;
  dlong len = hreg-machp->begin_heap;

  t1 = get_cpu_time();

  allocated = freed = 0;
  cached_hash = (struct hentry **)calloc(len,sizeof(dlong));
  allocated += len*sizeof(dlong);
  hash_buckets = non_null_hash_entries = 0;

  if (! cached_hash) fatal_message("Could not allocate cached_hash ... Exiting !\n");
  
  for (p = machp->begin_trail; p < loctrreg; p++)
    {
      p++;
      q = (dlong *)*p;
      set_nonsharable(cached_hash[q-machp->begin_heap]);
    }
  
  init_hashed_terms(hreg-machp->begin_heap);
  init_chain();

  for (p = machp->begin_heap; p < hreg; p++)
    {
//       if (p == 0x84cdb80)
// 	printf("daar \n");

	if (*p == BIGINT_HEADER)
	  {
	    term_hash_repshar((dlong *)make_number_pointer(p), machp->begin_heap, machp);
	    p +=  1 + bigint_size(p[1]);
	  }
      else
	if (is_string_header(*p))
	  {
	    dlong q = *p;
	    dlong len, l;
	    term_hash_repshar((dlong *)make_string_pointer(p), machp->begin_heap, machp);
	    len = get_len_from_string_header(q);
	    l = heapcells_needed_for_string(len);
	    p += l;
	  }
      else
      if (*p == REAL_HEADER)
	{
	  term_hash_repshar((dlong *)make_number_pointer(p), machp->begin_heap, machp);
	  p += SIZE_OF_FLOAT;
	}
      else
	if ((is_functor_header(*p))
	  && (! is_atom(*p))
	  && (!is_nonsharable(cached_hash[p-machp->begin_heap])))
	term_hash_repshar((dlong *)make_struct_p(p), machp->begin_heap, machp);
      else
	if ((is_list(*p)) && (!is_nonsharable(cached_hash[p-machp->begin_heap])))
	  { // if (p == 0x84cdb84)
	    // printf("here !\n");
	    term_hash_repshar(p,machp->begin_heap, machp);
	  }
    }

  t2 = get_cpu_time();

  // now absorb stuff 

  number_of_strings = number_of_numbers = number_of_lists = number_of_structs = 0;

  for (p = machp->begin_heap; p < hreg; p++)
    {
      if (*p == BIGINT_HEADER) p +=  1 + bigint_size(p[1]);
      else if (*p == REAL_HEADER) p += SIZE_OF_FLOAT;
      else if (is_string_header(*p)) p+= heapcells_needed_for_string(get_len_from_string_header(*p));
      else if (is_real(*p) || is_bigint(*p)) absorb_number(p,machp->begin_heap);
      else if (is_string(*p)) absorb_string(p,machp->begin_heap);
      else if (is_simple(*p)) ; // might be dangerous
      else if (is_struct(*p)) absorb_struct(p,machp->begin_heap);
      else if (is_list(*p)) absorb_list(p,machp->begin_heap);
    }

  for (p = begin_ls; p > tos; p--)
    {
      if (is_struct(*p)) absorb_struct_cautiously(p,machp->begin_heap,hreg);
      else if (is_real(*p) || is_bigint(*p)) absorb_number(p,machp->begin_heap);
      else if (is_string(*p)) absorb_string(p,machp->begin_heap);
    }

  for (p = machp->begin_cp; p > locbreg; p--)
    {
      if (is_struct(*p)) absorb_struct_cautiously(p,machp->begin_heap,hreg);
      else if (is_real(*p) || is_bigint(*p)) absorb_number(p,machp->begin_heap);
      else if (is_string(*p)) absorb_string(p,machp->begin_heap);
    }

  for (p = machp->Areg; arity--; )
    {
      p++;
      if (is_struct(*p)) absorb_struct_cautiously(p,machp->begin_heap,hreg);
      else if (is_real(*p) || is_bigint(*p)) absorb_number(p,machp->begin_heap);
      else if (is_string(*p)) absorb_string(p,machp->begin_heap);
    }

  free_hashed_terms();
  free(cached_hash);
  freed += len*sizeof(dlong);

  t3 = get_cpu_time();
  machp->total_repshar_time += t3-t1;

  if (! verbose_mode) return;
  fprintf(stderr,"\nRepresentation Sharing: total time = %ld; scanning time = %ld; absorption time = %ld\n",t3-t1,t2-t1,t3-t2);
  fprintf(stderr,"                        allocated %ld; freed %ld; heap size %ld\n",
                                                 allocated, freed, len*sizeof(dlong));
  fprintf(stderr,"                        hashed_terms size %ld; #filled %ld; #buckets %ld\n",
	                                   HASHED_TERMS_SIZE,non_null_hash_entries,hash_buckets);
  fprintf(stderr,"                        #structs %ld; #lists %ld; #numbers %ld; #strings %ld\n",
                                           number_of_structs,number_of_lists,
                                           number_of_numbers,number_of_strings);

} /* representation_sharing */
