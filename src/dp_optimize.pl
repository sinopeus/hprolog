:- module(source_optimize,[source_optimize/3]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% some clause transformations
% start reimplementing on 16 May 2006
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

source_optimize(I,O,_) :-
	maplist(I,source_optimize:foo1,I1),
	prolog_flag(inline,InLine),
	(
 	  InLine == no ->
	  I1 = I2
	;
	  inline(I1,I2)
	),
	tr_test1(I2,O1),
	maplist(O1,source_optimize:foo2,O).

foo1(clause(C,_),C1) :- compile:abstract2prolog(C,C1).
foo2(C,clause(C1,[])) :- compile:prolog2abstract(C,C1).


tr_test1([],[]).
tr_test1([Clause|RestCl], Out) :-
	get_head_body(Clause,Head1,Body1),
	collect_clauses_same_pred(RestCl,Head1,RestSame,NewRestCl),
	% var(RestSame) iff only one clause for this pred
	(
	  nonvar(RestSame), two_exclusive(Head1,Body1,RestSame,NewClause) ->
	  Out = [NewClause|NewOut],
	  tr_test1(NewRestCl,NewOut)
	;
	  nonvar(RestSame), two_with_first_cut(Head1,Body1,RestSame,NewClause) ->
	  Out = [NewClause|NewOut],
	  tr_test1(NewRestCl,NewOut)
	;
	  fail, nonvar(RestSame), two_member(Head1,Body1,RestSame,Out,NewOut) ->
	  tr_test1(NewRestCl,NewOut)
	;
	  fail, nonvar(RestSame), two_append(Head1,Body1,RestSame,Out,NewOut) ->
	  tr_test1(NewRestCl,NewOut)
	;
	  nonvar(RestSame), allsimpletestcut(Head1,Body1,RestSame,NewClause) ->
	  Out = [NewClause|NewOut],
	  tr_test1(NewRestCl,NewOut)
	;
	  Out = [Clause|RestSame],
	  findopenend(RestSame,OpenEnd),
	  tr_test1(NewRestCl,OpenEnd)
	).

collect_clauses_same_pred([],_,_,[]).
collect_clauses_same_pred([C|R],Head,RestSame,Rest) :-
	get_head_body(C,Head2,_),
	(clauses_of_same_pred(Head,Head2) ->
	    RestSame = [C|RS],
	    collect_clauses_same_pred(R,Head,RS,Rest)
	;
	    Rest = [C|RC],
	    collect_clauses_same_pred(R,Head,RestSame,RC)
	).

clauses_of_same_pred(H1,H2) :-
	functor(H1,N,A),
	functor(H2,N,A).

% needs extension ...

% a(0,_,_) :- !.   % 0 can be any (small) atomic
% a(X,Y,Z) :- body2.
% to a(X,Y,Z) :- (X = 0 -> true ; body2).

two_with_first_cut(H1,B1,[Cl2|OpenEnd],NewClause) :-
	(B1 = (A, MoreB1), A == ! ; B1 == !, MoreB1 = true),
	var(OpenEnd),		% just transform two clauses
	get_head_body(Cl2,H2,B2),
	H1 =.. [_,F|Args1],
	atomic(F), % for now
	F \== [],
	alldifvars(Args1),
	H2 =.. [_|Args2],
	introdifvars([X|Args1],Args2,ExtraBody),
	functor(H1,Name,_),
	Hnew =.. [Name,X|Args1],
	NewClause = (
		      Hnew :-
 		    (X = F -> MoreB1  ; ExtraBody,B2)
		    ),
	! .


two_append(H1,B1,[Cl2|OpenEnd],NewClauses,Tail) :-
	% fail, % because disabled at the moment
	var(OpenEnd),		% just transform two clauses
	B1 = true,
	H1 =.. [Name|Args1], variant(Args1,[[],L,L]),
	get_head_body(Cl2,H2,B2),
	H2 =.. [Name|Args2], B2 =.. [Name|Args3],
	variant(Args2+Args3, [[X|R],S,[X|T]]+[R,S,T]),
	!,
	NewHead =.. [Name,A,B,C], NewBody = sysh:append(A,B,C),
	NewClauses = [(NewHead :- NewBody) | Tail].

two_member(H1,B1,[Cl2|OpenEnd],NewClauses,Tail) :-
	var(OpenEnd),		% just transform two clauses
	B1 = true,
	H1 =.. [Name|Args1], variant(Args1,[X,[X|_]]),
	get_head_body(Cl2,H2,B2),
	H2 =.. [Name|Args2], B2 =.. [Name|Args3],
	variant(Args2+Args3, [X,[_|R]]+[X,R]),
	!,
	NewHead =.. [Name,A,B], NewBody = sysh:member(A,B),
	NewClauses = [(NewHead :- NewBody) | Tail].

variant(T1,T2) :- \+(\+(v(T1,T2))).

v(T1,T2) :-
	numbervars(T1,69,N), numbervars(T2,69,N),
	T1 = T2.

% needs extension ...

% a :- test, !, b. a :- c. to a :- test -> b ; c.
% a :- test1, test2, !, b. a :- c. to a :- (test1, test2) -> b ; c.

two_exclusive(H1,B1,[Cl2|OpenEnd],NewClause) :-
	var(OpenEnd), % just transform two clauses
	get_head_body(Cl2,H2,B2),
	H1 =.. [_|Args1],
	alldifvars(Args1),
	findcut(B1,Tests,MoreBody1),
	tr_simpletests(Tests),
	H2 =.. [_|Args2],
	introdifvars(Args1,Args2,ExtraBody),
	NewClause = (
		      H1 :-
		    (Tests -> MoreBody1 ; ExtraBody,B2)
		    ),
	! .

% needs extension ...

% a :- test, b. a :- not_test, c. to a :- test -> b ; c.

two_exclusive(H1,B1,[Cl2|OpenEnd],NewClause) :-
	var(OpenEnd),		% just transform two clauses
	get_head_body(Cl2,H2,B2),
 	H1 =.. [_|Args1],
	alldifvars(Args1),
 	H2 =.. [_|Args2],
	alldifvars(Args2),
	findtest(B1,Test1,MoreBody1),
	findtest(B2,Test2,MoreBody2),
	tr_simpletest(Test1),
	tr_simpletest(Test2),
	complementarytest(Test1,Test2,Args1,Args2),
	H2 =.. [_|Args2],
	introdifvars(Args1,Args2,ExtraBody),
	NewClause = (
		      H1 :-
		    (Test1 -> MoreBody1 ; ExtraBody,MoreBody2)
		    ),
	! .


% a :- st1, !, b.   a :- st2, !, c.   a :- st3, !, d. ... to
% a :- (st1 -> b ; st2 -> c ; st3 -> d ...).

allsimpletestcut(Head1,Body1,RestSame,NewClause) :-
	findopenend(RestSame,[]),
	unifyheads(RestSame,Head1,Bodies),
	Head1 =.. [_|Args1],
	alldifvars(Args1),
	checksimpletestcut([Body1|Bodies],NewBody),
	NewClause = (Head1 :- NewBody).

unifyheads([],_,[]).
unifyheads([(H:-B)|R],H,[B|S]) :- unifyheads(R,H,S).

checksimpletestcut([B],B) :- ! .
checksimpletestcut([B|R],(Tests -> MoreBody ; RestBody)) :-
        findcut(B,Tests,MoreBody),
	tr_simpletests(Tests),
	checksimpletestcut(R,RestBody).



% auxiliaries

get_head_body(Clause,Head,Body) :-
	(Clause = (Head :- Body) ->
	    true
	;
	    Head = Clause,
	    Body = true
	).


findtest((A,B),A,B) :- !, tr_simpletest(A).
findtest(A,A,true) :- tr_simpletest(A).

findcut((A,B),Tests,Body) :-
	!,
	(A == ! ->
	    Tests = true,
	    Body = B
	;
	    Tests = (A,NTs),
	    findcut(B,NTs,Body)
	).
findcut(A,Tests,Body) :-
	A == !,
	Tests = true,
	Body = true.


introdifvars([],[],true).
introdifvars([X|R],[Y|S],Goal) :-
	Goal = (X=Y, NewGoal),
	introdifvars(R,S,NewGoal).

varisinlist(X,[Y|R]) :-
	(X == Y ->
	    true
	;
	    varisinlist(X,R)
	).

tr_simpletests((A,B)) :- !, tr_simpletest(A), tr_simpletests(B).
tr_simpletests(A) :- tr_simpletest(A).

tr_simpletest( X ) :- var(X), !, fail.
tr_simpletest( _ < _).
tr_simpletest( _ > _).
tr_simpletest( _ =< _).
tr_simpletest( _ >= _).
tr_simpletest( _ =:= _).
tr_simpletest( _ =\= _).
tr_simpletest( _ == _).
tr_simpletest(true).

tr_simpletest(var(_)).
tr_simpletest(nonvar(_)).

alldifvars(L) :-
	sort(L,S),
	same_len_var(L,S).

same_len_var([],[]).
same_len_var([V1|R],[_|S]) :- var(V1), same_len_var(R,S).


findopenend(I,O) :-
	(var(I) ->
	    I = O
	;
	    I = [_|R],
	    findopenend(R,O)
	). 

complementarytest(T1,T2,A1,A2) :-
	complementarytest2(T1,T2,A,B,X,Y),
	(var(A), var(X) ->
	    correspondingarg(A1,A2,A,X)
	;
	    A == X
	),
	(var(B), var(Y) ->
	    correspondingarg(A1,A2,B,Y)
	;
	    B == Y
	), ! .
complementarytest(T1,T2,A1,A2) :-
	complementarytest1(T1,T2,A,B),
	(var(A), var(B) ->
	    correspondingarg(A1,A2,A,B)
	;
	    A == B
	).

correspondingarg([A|As],[B|Bs],X,Y) :-
	(A == X, B == Y ->
	    true
	;
	    correspondingarg(As,Bs,X,Y)
	).

complementarytest2( A < B, X >= Y, A,B,X,Y).
complementarytest2( A < B, X  =< Y, A,B,Y,X).

complementarytest2( A > B, X  =< Y, A,B,X,Y).
complementarytest2( A > B, X  >= Y, A,B,Y,X).

complementarytest2( A =< B, X  > Y, A,B,X,Y).
complementarytest2( A =< B, X  < Y, A,B,Y,X).

complementarytest2( A >= B, X  < Y, A,B,X,Y).
complementarytest2( A >= B, X  > Y, A,B,Y,X).

complementarytest2( A =:= B, X  =\= Y, A,B,X,Y).
complementarytest2( A =:= B, X  =\= Y, A,B,Y,X).

complementarytest2( A =\= B, X =:= Y, A,B,X,Y).
complementarytest2( A =\= B, X =:= Y, A,B,Y,X).

complementarytest1(var(A), nonvar(B), A, B).
complementarytest1(nonvar(A), var(B), A, B).


maplist(   [], _,     []).
maplist([X|Xs], P, [Y|Ys]) :-
	call(P,X,Y),
	maplist(Xs,P,Ys).	

w(X) :- writeln(X).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% inlining: simple strategy
%           inline every predicate with only one clause that is smaller
%                    than a given treshhold
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


inline(ProgrIn, ProgOut) :-
	collect_single_clause_preds(ProgrIn, SingleClauses),
	(SingleClauses == [] ->
	    ProgrIn = ProgOut
	;
	    inline(ProgrIn, SingleClauses, ProgOut)
	).

inline([], _, []).
inline([Clause|RestCl], SingleClauses, ProgOut) :-
	source_optimize:get_head_body(Clause,Head1,_),
	source_optimize:collect_clauses_same_pred(RestCl,Head1,RestSame,NewRestCl),
	% var(RestSame) iff only one clause for this pred
	% but RestSame is always open ended
	(
%% 	  var(RestSame) ->
%% 	    ProgOut = [Clause|RestSame],
%% 	    inline(RestCl, SingleClauses, RestSame)
%% 	;
	    inline([Clause|RestSame],SingleClauses,ProgOut,TailProgOut),
	    inline(NewRestCl, SingleClauses, TailProgOut)
	).

inline([], _, ProgOut, TailProgOut) :- !, ProgOut = TailProgOut.
inline([Clause|RestClauses], SingleClauses, [NewClause|ProgOut], TailProgOut) :-
	inline_clauses(SingleClauses,Clause,NewClause),
	inline(RestClauses,SingleClauses,ProgOut,TailProgOut).

inline_clauses([],Cl,Cl).
inline_clauses([InLine|Is], Clause, NewClause) :-
	inline1clause(InLine,Clause,NewClause1),
	inline_clauses(Is,NewClause1,NewClause).

inline1clause(InLine, Clause, NewClause) :-
	source_optimize:get_head_body(Clause,Head,Body),
	source_optimize:get_head_body(InLine,IHead,IBody),
	inline1body(Body,IHead,IBody,NewBody),
	NewClause = (Head:-NewBody).

inline1body(Body,  IHead, IBody, NewBody) :-
	(
	  var(Body) -> NewBody = Body
	;
	  atomic(Body) -> NewBody = Body % do not bother
	;
	  control(Body,A,B,NewBody,C,D) ->
	  inline1body(A, IHead, IBody, C),
	  inline1body(B, IHead, IBody, D)
	;
	  Body = Goal,
	  (\+(\+(Goal = IHead)) ->
	      copy_term(IHead/IBody,NewIHead/NewIBody),
	      Goal = NewIHead,
	      NewBody = NewIBody
	  ;
	      NewBody = Body
	  )
	).

collect_single_clause_preds([],[]).
collect_single_clause_preds([Clause|RestCl], Out) :-
	source_optimize:get_head_body(Clause,Head1,Body1),
	source_optimize:collect_clauses_same_pred(RestCl,Head1,RestSame,NewRestCl),
	% var(RestSame) iff only one clause for this pred
	(var(RestSame), nocut(Body1) ->
	    Out = [Clause|RestSame],
	    collect_single_clause_preds(RestCl,RestSame)
	;
	    collect_single_clause_preds(NewRestCl,Out)
	).

nocut(X) :-
	(
	  var(X) -> true
	; X == ! -> fail
	; atomic(X) -> true
	;
	  control(X,A,B) -> nocut(A), nocut(B)
	;
	  true
	).

control((A,B),A,B).
control((A;B),A,B).
control((A->B),A,B).

control((A,B),A,B,(C,D),C,D).
control((A;B),A,B,(C;D),C,D).
control((A->B),A,B,(C->D),C,D).

