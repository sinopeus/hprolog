data_qsort(Size, List) :-
  gen_list_qsort(Size, List).

gen_list_qsort(0, []) :- !.
gen_list_qsort(Count, [Elem|Result]) :-
  NewCount is Count - 1,
  random(Elem),
  gen_list_qsort(NewCount, Result).

run_qsort(Threads, _, List) :-
  qsort(List, [], _, Threads).

qsort([], Sorted, Sorted, _).
qsort([Pivot| Rest], Acc, Sorted, Threads) :-
  (	Threads =:= 1 ->
    quicksort([Pivot| Rest], Acc, Sorted)
  ;	Threads2 is Threads//2,
    partition(Rest, Pivot, Smaller0, Bigger0),
    threaded((
      qsort(Smaller0, [Pivot| Bigger], Sorted, Threads2),
      qsort(Bigger0, Acc, Bigger, Threads2)
    ))
  ).

partition([], _, [], []).
partition([X| Xs], Pivot, Smalls, Bigs) :-
  (	X < Pivot ->
    Smalls = [X| Rest],
    partition(Xs, Pivot, Rest, Bigs)
  ;	Bigs = [X| Rest],
    partition(Xs, Pivot, Smalls, Rest)
  ).

quicksort([], Sorted, Sorted).
quicksort([Pivot| Rest], Acc, Sorted) :- 
  partition(Rest, Pivot, Smaller0, Bigger0),
  quicksort(Smaller0, [Pivot| Bigger], Sorted),
  quicksort(Bigger0, Acc, Bigger).
