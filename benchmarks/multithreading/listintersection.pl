% Generates a conjunction
data_listintersection_allsame(I, GoalCount, Size, Var, Goals) :-
	gen_list(Size, Size, List),
  data_listintersection(List, I, GoalCount, Size, Var, Goals).

data_listintersection(List, I, GoalCount, Size, Var, RealGoals) :-
	GoalCount1 is GoalCount-1,
  I1 is I+1,
  ( I = 1 -> Goal = member(Var, List)
  ; Goal = once(member(Var, List))
  ),
	( GoalCount1 = 0 -> RealGoals = Goal
	; RealGoals = (Goal, Goals),
	  data_listintersection(List, I1, GoalCount1, Size, Var, Goals)
	).

data_listintersection_alldiff(I, GoalCount, Size, Var, RealGoals) :-
	GoalCount1 is GoalCount-1,
  I1 is I+1,
  Size2 is Size*I,
  gen_seq_list(Size2, Size, List),
  ( I = 1 -> Goal = member(Var, List)
  ; Goal = once(member(Var, List))
  ),
	( GoalCount1 = 0 -> RealGoals = Goal
	; RealGoals = (Goal, Goals),
	  data_listintersection_alldiff(I1, GoalCount1, Size, Var, Goals)
	).

data_listintersection_overlap(I, GoalCount, Size, Var, RealGoals) :-
	GoalCount1 is GoalCount-1,
  I1 is I+1,
  Size2 is Size*I - (Size/2)*I,
  truncate(Size2, S2),
  gen_seq_list(S2, Size, List),
  ( I = 1 -> Goal = member(Var, List)
  ; Goal = once(member(Var, List))
  ),
	( GoalCount1 = 0 -> RealGoals = Goal
	; RealGoals = (Goal, Goals),
	  data_listintersection_overlap(I1, GoalCount1, Size, Var, Goals)
	).

gen_list(_, 0, []) :- !.
gen_list(Size, Left, [X|List]) :-
	Left1 is Left-1,
	random(Y),
	Z is Y*Size,
	truncate(Z, X),
	gen_list(Size, Left1, List).

gen_seq_list(_, 0, []) :- !.
gen_seq_list(X, Left, [X|List]) :-
	Left1 is Left-1,
  Y is X-1,
	gen_seq_list(Y, Left1, List).

run_listintersection(findall, Var, Goals) :-
	!,
	findall(Var, Goals, _).
run_listintersection(piped_findall, Var, Goals) :-
	!,
	piped_findall(Var, Goals, _).
