
#define dlong long

int val(int i); 
dlong minisat_new_solver();
dlong minisat_delete_solver();
void minisat_add_clause();
void minisat_add_xorclause();
void push_literal(dlong lit);
dlong minisat_solve();
int check_model_index(int index);
dlong minisat_nvars();
