%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  ____       __            _             _             
%% |  _ \ ___ / _| __ _  ___| |_ ___  _ __(_)_ __   __ _ 
%% | |_) / _ \ |_ / _` |/ __| __/ _ \| '__| | '_ \ / _` |
%% |  _ <  __/  _| (_| | (__| || (_) | |  | | | | | (_| |
%% |_| \_\___|_|  \__,_|\___|\__\___/|_|  |_|_| |_|\__, |
%%                                                 |___/ 
%%
%% author: Tom Schrijvers, K.U.Leuven
%%         Alexander Serebrenik, K.U.Leuven 
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% To Do:
%%
%%
%%     - f/a x f/a frequency counts of conjunctions of 2 simple goals
%%
%%     - frequency of particular nonvar simple goal arguments
%%
%%     - frequency of particular simpel goal argument interdependencies
%%
%%     - unfolding of small predicates used only once
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(refactoring,[commoncode/1]).

:- use_module(library(lists)).
:- use_module(library(pairlist)).

simple_goal(G) :-
	G \= (_,_),
	G \= (_;_),
	G \= (_->_),
	G \= (!).

swap([],[]).
swap([A-B|T],[B-A|NT]) :-
	swap(T,NT).

my_lookup_eq([K - V | KVs],Key,Value) :-
        ( K == Key ->
                V = Value
        ;
                my_lookup_eq(KVs,Key,Value)
        ).

my_delete_eq([], _, []).
my_delete_eq([K - V| KVs], Key, PL) :-
        ( Key = K ->
                PL = KVs
        ;
                PL = [ K - V | T ],
                my_delete_eq(KVs, Key, T)
        ).

add_pair(FA1,FA2,Table,NTable) :-
	Key = FA1-FA2,
	( my_lookup_eq(Table,Key,N) ->
		my_delete_eq(Table,Key,Table1),
		M is N + 1,
		NTable = [Key-M|Table1]
	;
		M = 1,
		NTable = [Key-M|Table]
	).

commoncode(File) :-
	readfile(File,Clauses),
	findall(C, ( member(C,Clauses), C \= (:- _) , C \= (?- _)), Cs),
	findall(B, member((_ :- B),Cs), Bodies),
	Table = [],
	commonbodies(Bodies,Table,NTable),
	swap(NTable,List),
	sort(List,SortedList),
	findall(_,(member(F-(FA1-FA2),SortedList),write(F),write('\t'),write(FA1),write('\t\t'),write(FA2),nl),_).

commonbodies([],T,T).
commonbodies([B|Bs],T1,T3) :-
	commonbody(B,T1,T2),
	commonbodies(Bs,T2,T3).

commonbody((A;B),T1,T3) :- !,
	commonbody(A,T1,T2),
	commonbody(B,T2,T3).
commonbody((A->B),T1,T3) :- !,
	commonbody(A,T1,T2),
	commonbody(B,T2,T3).
commonbody((A,B),T1,T3) :- !,
	commonconj(A,no,Last,T1,T2),
	commonconj(B,Last,_,T2,T3).
commonbody(_,T,T).

commonconj(G,Prev,Last,T,NT) :-
	( simple_goal(G) ->
		functor(G,F,A),
		FA2 = F/A,
		( Prev = yes(FA1) ->
			add_pair(FA1,FA2,T,NT)
		;
			NT = T
		),
		Last = yes(FA2)
	; G = (G1,G2) ->
		commonconj(G1,Prev,Next,T,T1),
		commonconj(G2,Next,Last,T1,NT)
	;
		commonbody(G,T,NT),
		Last = no
	).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% File Reading

readfile(File,Declarations) :-
	see(File),
	prolog_flag(verbose,X),
	( X == yes ->
		write(' * loading file '),
		write(File),
		write('...'),
		nl
	;
		true
	),
	( readcontent(Declarations) ->
		seen,
		( X == yes ->
			write('   finished loading file '),
			write(File),
			nl
		;
			true
		)
	;
		seen,
		( X == yes ->
			write('   ERROR loading file '),
			write(File),
			nl
		;
			true
		),
		fail
	).

readcontent(C) :-
	read(X),
	( X = (:- op(Prec,Fix,Op)) ->
		op(Prec,Fix,Op)
	;
		true
	),
	( X == end_of_file ->
		C = []
	;
		C = [X | Xs],
		readcontent(Xs)
	).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

