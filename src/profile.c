#include "toinclude.h"

extern long numb_symbs;
extern void write_with_quotes(char *, FILE *);
extern type_opcode next_instruction(codep);
extern void fatal_message(char *);


dlong dump_reset_profiling(char *filename)
{
  dlong current_pred;
  codep startcode_pred;
  type_opcode opc;
  dlong countlow = 0;
  dlong counthigh = 0;
  FILE *OUTFILE;

  OUTFILE = fopen(filename,"w");
  if (! OUTFILE) return(0);  

  for (current_pred = 0; current_pred < numb_symbs; current_pred++)
    {
      startcode_pred = entries[current_pred];
      if ( ! startcode_pred) continue;
      if (points_to_unknown_call(startcode_pred)) continue;
      //       if (is_protected(current_pred)) continue;

      opc = next_instruction(startcode_pred);
      if (opc != profile_instr) continue;

      countlow = *(count_type*)(arg_place(startcode_pred,1,profile_instr));
      counthigh = *(count_type*)(arg_place(startcode_pred,2,profile_instr));
      if ((! countlow) && (! counthigh)) continue;

      fprintf(OUTFILE,"current_pred(%ld,", current_pred);
      write_with_quotes(symtab[current_pred].name, OUTFILE);
      fprintf(OUTFILE,",%ld,",symtab[current_pred].arity);
      write_with_quotes(symtab[symtab[current_pred].module].name,OUTFILE);
      if (counthigh)
	fprintf(OUTFILE,",%ld%09ld).\n",counthigh,countlow);
      else
	fprintf(OUTFILE,",%ld).\n",countlow);

      while (1)
	{
	  opc = next_instruction(startcode_pred);
	  if (((countlow > 0) || (counthigh > 0)) 
	      && (opc != profile_instr) 
	      && (opc != end_clauses))
	    {
	      if (counthigh)
		fprintf(OUTFILE,"instr_exec(%ld,%s,%ld%09ld).\n",
			current_pred,instr_name[opc],counthigh,countlow);
	      else 
		fprintf(OUTFILE,"instr_exec(%ld,%s,%ld).\n",
			current_pred,instr_name[opc],countlow);
	    }
	  switch (opc)
	    {
	    case end_clauses:
	      goto end_of_this_clause;
#include "profile.h"
	      
	    default:
	      fatal_message("precise_profile_call_site - impossible opcode");
	    }
	}
    end_of_this_clause: ;
    }

  fclose(OUTFILE);
  return(1);

} /* precise_profile_call_site */
