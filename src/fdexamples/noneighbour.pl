:- use_module(library(fd)).

run :-
	Vars = [A,B,C,D,E,F,G,H],
	domain(Vars,[1,2,3,4,5,6,7,8]),
	all_different(Vars),

	'abs(X-Y)#<A'(1,A,B),
	'abs(X-Y)#<A'(1,A,C),
	'abs(X-Y)#<A'(1,A,D),
	'abs(X-Y)#<A'(1,A,E),
		      
	'abs(X-Y)#<A'(1,B,D),
	'abs(X-Y)#<A'(1,B,E),
	'abs(X-Y)#<A'(1,B,F),
		      
	'abs(X-Y)#<A'(1,C,D),
	'abs(X-Y)#<A'(1,C,G),
		      
	'abs(X-Y)#<A'(1,D,E),
	'abs(X-Y)#<A'(1,D,G),
	'abs(X-Y)#<A'(1,D,H),
		      
	'abs(X-Y)#<A'(1,E,F),
	'abs(X-Y)#<A'(1,E,G),
	'abs(X-Y)#<A'(1,E,H),
		      
	'abs(X-Y)#<A'(1,F,H),
		      
	'abs(X-Y)#<A'(1,G,H),

	labeling(Vars),
	writeln(Vars).
