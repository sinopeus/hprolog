

#include <math.h>
#include <fcntl.h>
#include "toinclude.h"
#ifdef USE_GMP
#include <gmp.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "installdir.h"
#include <sys/param.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include "builtinnumbers.h"
#include "builtins.h"
#include <signal.h>
#include <string.h>

#include "logic_engines.h"
#include "shared_memory.h"

extern void install_active_yvars(dlong *, codep, dlong *, dlong);
extern dlong *collect_active_yvars(dlong *, codep, dlong *);
extern dlong *find_intercept(struct machine *);
extern codep unwind_stack_callcc(struct machine *);
extern dlong * alt_unwind_stack_callcc(struct machine *);
extern void add_mpz_hint(mpz_t, dlong *, struct machine *);
extern dlong bigint_result(mpz_t, struct machine *);
extern dlong htoggles_2_builtin(dlong, dlong, struct machine *);
extern void deal_with_heap_overflow(dlong , struct machine *, dlong);
extern dlong * deal_with_ls_overflow(struct machine *);
extern void deal_with_cp_overflow(struct machine *);
extern void deal_with_tr_overflow(struct machine *);
extern void   precise_profile_call_site();
extern void init_codezone();
extern void init_symbol_tables();
extern void copyterm_cyclics_init();
extern dlong string_hash(dlong, char *);
extern dlong file2stream(dlong filename, dlong openmode, struct machine *machp);
extern dlong numb_symbs;

extern dlong get_float_from_number(dlong *p, dfloat *f, struct machine *machp);
extern dlong addsub_two_inumbers(dlong *p1, dlong *p2, dlong addition, struct machine *machp);
extern dlong and_two_inumbers(dlong *p1, dlong *p2, struct machine *machp);
extern dlong or_two_inumbers(dlong *p1, dlong *p2, struct machine *machp);
extern dlong xor_two_inumbers(dlong *p1, dlong *p2, struct machine *machp);
extern dlong multiply_two_inumbers(dlong *p1, dlong *p2, struct machine *machp);
extern dlong bigint_divide(dlong *p1, dlong *p2, dlong tocompute, struct machine *machp);
extern dlong float_to_bigint(dfloat f, struct machine *machp);
extern dlong shiftleft_two_inumbers(dlong *, dlong *, struct machine *);
extern dlong shiftright_two_inumbers(dlong *, dlong *, struct machine *);
extern dlong compare_two_inumbers(dlong *p1, dlong *p2, struct machine *machp, dlong *er);
extern dlong bigint_low_msb(dlong *p, struct machine *machp);
extern dlong bigint_low_abs(dlong *p, struct machine *machp, dlong makepos);
extern dlong bigint_print(FILE *f, dlong *p, struct machine* machp);
extern void make_pred_without_clauses(dlong i);
extern dlong low_write_var1(dlong *p, struct machine *machp);
extern dlong low_write_var2(dlong *p1, dlong *p2, struct machine *machp);
extern void low_write_atomic3(dlong *p1, dlong *p2, struct machine *machp, dlong doubleform);
extern dlong low_write_atomic4(dlong *p1, dlong *p2, dlong *p3, struct machine *machp, dlong doubleform);
extern dlong low_put2(dlong *p1, dlong *p2, struct machine *machp);
extern dlong low_open_stream_3(dlong *p1, dlong *p2, struct machine *machp);
extern dlong low_get_open_stream3(dlong *p1, dlong *p2, dlong *p3, struct machine *machp);
extern dlong low_close_stream1(dlong *p1, struct machine *machp);
extern dlong low_get0_1(dlong curin, struct machine *machp);
extern dlong low_current_eof0(dlong curin, struct machine *machp);
extern dlong low_get0_stream_2(dlong *str, struct machine *machp);
extern dlong low_see_1(dlong *p, struct machine *machp);
extern void low_seen_0(struct machine *machp);
extern dlong low_tell_1(dlong *p, struct machine *machp, char* type);
extern void low_told_0(struct machine *machp);
extern dlong low_close1(dlong *p, struct machine *machp);
extern dlong builtin_internalstat(dlong a1, dlong a2, dlong a3, dlong a4, dlong a5, struct machine *machp);
extern void back_trace(struct machine *machp);
extern dlong ancestors(struct machine *machp);
extern dlong term_hash_builtin(dlong *p, struct machine *machp);
extern dlong Cprolog_flag(dlong *p1, dlong *p2, dlong *p3, struct machine *machp);
extern sympointer new_symbol();
extern void print_heap(dlong arity, struct machine *machp);
extern dlong do_key_sort(dlong *from, dlong len, dlong *whereto, struct machine* machp);
extern void print_code_for_pred(dlong *p);
extern void set_no_representation_sharing(dlong *p);

dlong precise_profile_call_site_opt = 0;
dlong generational_gc = 1;
dlong overwrite_without_asking = 0;



#ifdef COUNT_INFERENCES
static dlong inferences = 0;
static pthread_mutex_t inferences_mutex = PTHREAD_MUTEX_INITIALIZER;
#define inferencesplusplus \
  do { \
    pthread_mutex_lock(&inferences_mutex); \
    inferences++; \
    pthread_mutex_unlock(&inferences_mutex); \
  } while (0);
//#define inferencesplusplus __sync_add_and_fetch(&inferences, 1)
#else
#define inferencesplusplus
#endif

dlong print_instr = 0;
dlong compress_table = 1;
dlong peep_spec = 3;

static dlong unif_count = 0;
static dlong rec_unif_count = 0;
static dlong unifintint = 0;
static dlong unifatomatom = 0;
static dlong unifdeldel = 0;
static dlong unifrealreal = 0;
static dlong unifstringstring = 0;
static dlong unifvarother = 0;
static dlong unifvarvar = 0;
static dlong uniflistlist = 0;
static dlong unifstructstruct = 0;
static dlong unifdelother = 0;
static dlong glob_min_argc,glob_max_argc;
static char **glob_argv;

#define ensure_space_for_woken_goals(locmach) \
{ \
dlong s = get_number_of_woken_goals(&locmach); \
if (s*2 + lochreg > heap_limit) \
macro_deal_with_heap_overflow(arity,s*3); \
}


#define macro_deal_with_heap_overflow(arity,minimal) \
      { deal_with_heap_overflow(arity,&locmach,minimal); }

#define macro_deal_with_tr_overflow() \
      { deal_with_tr_overflow(&locmach); }


#define macro_deal_with_cp_overflow() \
      { deal_with_cp_overflow(&locmach); }

// Check if the current engine is cancelled and if so jump to halt

#ifdef MULTI_THREADED
#define check_cancellation()		\
{ \
  int cancelled = thisengine->cancelled; \
  if (cancelled) \
    goto end_session; \
}
#else
#define check_cancellation()
#endif


static struct machine *mainmachp = NULL;
static __thread struct engine* currentengine = NULL;
static dlong exclude_list_trigger_3;
static dlong deal_with_sigint_1;
static dlong deal_with_wakeup_2;
static dlong deal_with_wakeup_of_one_3;
static dlong deal_with_wakeup_of_two_5;

dlong engine_toplevel_goal_0;

static dlong sysh_conjunction_2;

static dlong colon_2;
static dlong symbol_equal_2;
dlong nil_atom, atom_at_smaller, atom_at_bigger, atom_at_equal;
dlong atom_verbose, atom_very_verbose, atom_prompt, 
  atom_default_prompt, atom_on, true_atom, atom_repshar, atom_cut;
char *profile_output = "profilingdata.hprolog";
static FILE *profile_output_fd;
dlong atom_yes, atom_no;
dlong value_prompt;
dlong symbol_true_0;
dlong symbol_halt_0;
dlong verbose_mode = 0;
dlong very_verbose_mode = 0;
static dlong atom_dot_0, symbol_error_message_3, symbol_error_builtin_arith5, symbol_error_builtin_arith4, symbol_error_unknown_pred_1, symbol_cutfail_0;
static dlong funct_comma_2,funct_semicolon_2,funct_arrow_2,funct_if_3;
static dlong funct_continuation_3;
static dlong funct_continuation_2;
static dlong funct_call_continuation_1;

dlong symbol_dot_2;
#ifndef PLAIN_SWITCH
void *instr_labels[NR_OF_OPCODES];
#endif

#if 0
static dlong bitcount(unsigned int n)                          
{
  /* works for 32-bit numbers only    */
  /* fix last line for 64-bit numbers */
    
  register unsigned dlong tmp;
    
  tmp = n - ((n >> 1) & 033333333333)
    - ((n >> 2) & 011111111111);
  return ((tmp + (tmp >> 3)) & 030707070707) % 63;
}
#endif


static dlong * delfromdomain(dlong *l, dlong *i)
{
  /* returns 0 if no change, i.e., i not in l
     returns [] if empty --> fail
     returns integer if is only one
  */
  dlong count, j;
  dlong *res;

  l = get_struct_pointer(l) + 1;
  j = get_smallint(i);

  fprintf(stderr,"delfromdomain disabled\n"); exit(0);

//   res = make_list(h);
// 
//   while (is_list(l))
//     {
//       dlong *el;
//       l = get_list_pointer(l);
//       el = (dlong *)*l; deref(el);
//       l = l[1]; deref(l);
// 
//       if (el < i)
//         {
//           count++;
//           h[0] = (dlong)el;
//           h[1] = make_list(h+2);
//           h += 2;
// 	  continue;
//         }
// 
//       if (el == i)
// 	{
// 	  if (count == 0)
// 	    {
// 	      if (l == nil_atom) return(nil_atom);
// 	      res = l;
// 	      l = get_list_pointer(l);
// 	      el = l[0];
// 	      l = l[1];
// 	      deref(l);
// 	      if (l == nil_atom) return(el);
// 	      return(res);
// 	    }
// 	      
// 	  if (count == 1)
// 	    if (l == nil_atom) return(h[-2]);
// 	  
// 	  h[-1] = l;
// 	  mach->H = h;
// 	  return(res);
// 	}
// 
//       return(0);
//     }

  if (*(l+j) == 0) return(0);
  count = get_smallint(l[0]);
  if (count == 1) return((dlong *)nil_atom);
  if (count == 2)
    {
      l[j] = 0;
      // find the only non zero entry
      for (count = 1; count < 10; count++)
	if (l[count] != 0) return((dlong *)(l[count]));
      printf("scream delfromdomain 1\n");
    }
  // trail appropriate things - not for now
  count--; 
  l[0] = make_smallint(count);
  l[j] = 0;
  return(0);

} /* delfromdomain */


dlong compare_version(FILE *F)
{
  dlong i;
  char p[100];

  i = (dlong)fgets(p,100,F);
  i = strlen(p);
  p[i-1] = 0;
  if (strcmp(p,VERSION_STRING))
    return(0);
  return(1);
} /* compare_version */

static char needs_escape(char c)
{
  if (c < 14)
    {
      if (c == 7) return('a');
      if (c == 8) return('b');
      if (c == 9) return('t');
      if (c == 10) return('n');
      if (c == 11) return('v');
      if (c == 12) return('f');
      if (c == 13) return('r');
      return(0);
    }
  if (c == '\'') return(c);
  if (c == '\"') return(c);
  if (c == '\\') return(c);
  return(0);
} /* needs_escape */

void put_with_escape(FILE *f,char c)
{
  char c2;
  if ((c2 = needs_escape(c)))
    { fputc('\\',f); fputc(c2,f); }
  else
    fputc(c,f);
} /* put_with_escape */

static dlong call_c_function(dlong arity, dlong (*cfunction)(), dlong *args)
{
  dlong res;

  switch (arity)
    {
    case 1: 
      { dlong (*c_function)(dlong);
        c_function = cfunction;
	res = (*c_function)(args[1]);
	return(res);
      }
    case 2: 
      { dlong (*c_function)(dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2]);
	return(res);
      }
    case 3: 
      { dlong (*c_function)(dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3]);
	return(res);
      }
    case 4: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4]);
	return(res);
      }
    case 5: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5]);
	return(res);
      }
    case 6: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6]);
	return(res);
      }
    case 7: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7]);
	return(res);
      }
    case 8: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8]);
	return(res);
      }
    case 9: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9]);
	return(res);
      }
    case 10: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10]);
	return(res);
      }
    case 11: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11]);
	return(res);
      }
    case 12: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11],args[12]);
	return(res);
      }
    case 13: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11],args[12],args[13]);
	return(res);
      }
    case 14: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11],args[12],args[13],args[14]);
	return(res);
      }
    case 15: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11],args[12],args[13],args[14],args[15]);
	return(res);
      }
    case 16: 
      { dlong (*c_function)(dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong,dlong);
        c_function = cfunction;
	res = (*c_function)(args[1],args[2],args[3],args[4],args[5],args[6],args[7],args[8],args[9],args[10],args[11],args[12],args[13],args[14],args[15],args[16]);
	return(res);
      }
    }
  return(0);
} /* call_c_function */

dlong get_cpu_time()
{
  struct rusage usage;
  dlong tim;

//   {
//     int ret;
//     clockid_t cid;
//     struct timespec ts;
//     ret = pthread_getcpuclockid(pthread_self(), &cid);
//     ret = clock_gettime(cid, &ts);
//     if (ret == -1) fatal_message("clock_gettime");
//     // fprintf(stderr,"%ld %ld\n",ts.tv_sec,ts.tv_nsec);
//     tim = ts.tv_sec*1000 + ts.tv_nsec / 1000000;
//     return(tim);
//   }


  //  getrusage(RUSAGE_THREAD, &usage);
  getrusage(RUSAGE_SELF, &usage);
  tim = (usage.ru_utime.tv_sec)*1000 + (usage.ru_utime.tv_usec / 1000);
  return(tim);

} /* get_cpu_time */

dlong get_unix_time()
{
  struct timeval tv;
  dlong tim;

  gettimeofday(&tv, NULL);
  
  tim = (tv.tv_sec)*1000 + (tv.tv_usec / 1000);
  return(tim);
}

static void expand_woken_goals(struct machine *machp)
{
  dlong *p = machp->begin_woken_goals;
  dlong i = machp->nr_woken_goals;

  i = i * 2;
  machp->begin_woken_goals = (dlong *)realloc(p,i*sizeof(dlong *));
  if (! machp->begin_woken_goals)
    fatal_message("not enough space for expanding woken goals");
  machp->nr_woken_goals = i;  
} /* expand_woken_goals */


static INLINE dlong * tidy_trail(dlong *cutpoint, dlong *currentb,dlong *endtr, dlong *toph)
{
  dlong *testbh;
  dlong *currenttr;
  dlong *newtoptr;

  while (((CP_P)currentb)->b != cutpoint)
     currentb = ((CP_P)currentb)->b;

  testbh = ((CP_P)cutpoint)->h;
  currenttr = ((CP_P)currentb)->tr;
  newtoptr = currenttr = cleantr(currenttr);
  while (currenttr < endtr)
    {
      /* the toph is needed because of the damn global variables */
      if (((dlong *)currenttr[1] < testbh) || (toph <= (dlong *)currenttr[1]))
	{
	  newtoptr[0] = currenttr[0];
	  newtoptr[1] = currenttr[1];
	  newtoptr += 2;
	}
      currenttr += 2;
    }
  return(newtoptr);
} /* tidy_trail */


/*
badmetagoal: if the term considered as a goal (so , ; -> if/3 are treated specially)
has a variable or non-callable somewhere, it says true
otherwise false
*/

static dlong badmetagoal(dlong *p)
{
  deref(p);
  switch (tag(p))
    {
    case REF0:
    case REF1:
      return(1);

    case LIST:
      return(0);

    case STRUCT:
      {
	dlong i;

	p = get_struct_pointer(p);
	i = get_funct_arity(*p);
	if (i == 2)
	  {
	    /* , ; -> */
	    if ((*p == funct_comma_2) || (*p == funct_semicolon_2) ||
	        (*p == funct_arrow_2))
	      {
		if (badmetagoal(p+1)) return(1);
		if (badmetagoal(p+2)) return(1);
	      }
	    return(0);
	  }
	if (i == 3)
	  {
	    if (*p == funct_if_3)
	      {
		if (badmetagoal(p+1)) return(1);
                if (badmetagoal(p+2)) return(1);
                if (badmetagoal(p+3)) return(1);
	      }
	  }
	return(0);
      }

    case SIMPLE:
      if (is_atom(p)) return(0);
      return(1);

    case NUMB:
    case STRING:
    case ATT:
      return(1);
    }

  return(1);
} /* badmetagoal */


static dlong make_woken_goal_list(dlong i, struct machine *machp)
{
  dlong start, *h, *p;
  p = machp->begin_woken_goals;
  h = machp->H;
  start = make_list(h);
  while (i--)
    {
      *h++ = *p++;
      *h = make_list(h+1);
      h++;
      i--;
      *h++ = *p++;
      *h = make_list(h+1);
      h++;
    }
  *(h-1) = nil_atom;
  machp->H = h;
  return(start);
} /* make_woken_goal_list */


static dlong interrupt_flag = 0;

static codep treat_asynchrous_call(dlong symbol, struct machine *machp)
{
  dlong arity,i, woken;
  codep pc;

  /* construct continuation */
  arity = symtab[symbol].arity;
  if (arity == 0)
    machp->Areg[2] = make_atom(symbol);
  else
    {
      dlong *h = machp->H;
      *h = make_funct(symbol);
      h++;
      i = 1;
      while (arity--)
        *h++ = machp->Areg[i++];
      machp->Areg[2] = make_struct_p(machp->H);
      machp->H = h;
    }

  if (interrupt_flag != 0)
    { /* we have a sigint */
      interrupt_flag = 0;
      machp->Areg[1] = machp->Areg[2];
      pc = main_entrypoint(deal_with_sigint_1);
      return(pc);
    }

  /* we have a wakeup */

  i = get_number_of_woken_goals(machp);
  set_number_of_woken_goals(0,machp); /* to reset the woken goals to empty */
  if (i == 2)
    {
      dlong *a, *p;
      p = machp->begin_woken_goals;
      a = machp->Areg;
      a[3] = a[2];
      a[1] = p[0];
      a[2] = p[1];
      pc = main_entrypoint(deal_with_wakeup_of_one_3);
      return(pc);
    }

  if (i == 4)
    {
      dlong *a, *p;
      p = machp->begin_woken_goals;
      a = machp->Areg;
      a[5] = a[2];
      a[1] = p[0];
      a[2] = p[1];
      a[3] = p[2];
      a[4] = p[3];
      pc = main_entrypoint(deal_with_wakeup_of_two_5);
      return(pc);
    }

  pc = main_entrypoint(deal_with_wakeup_2);
  woken = make_woken_goal_list(i,machp);
  machp->Areg[1] = woken;

  return(pc);
} /* treat_asynchrous_call */

#define MAKE_ATOM 0
#define MAKE_INTEGER 1
#define MAKE_FLOAT 2

static dlong represents_integer(char *b)
{
  while (*b)
    {
      if ((*b < '0') || (*b > '9')) return(0);
      b++;
    }

    return(1);
  
} /* represents_integer */

static dlong represents_float(char *b)
{
  dlong seensomething = 0;

  while (*b)
    {
      if ((*b < '0') || (*b > '9'))
	{
	  if (*b != '.') return(0);
	  goto seen_dot;
	}
      seensomething = 1;
      b++;
    }

  return(0);

 seen_dot:
  if (! seensomething) return(0);
  seensomething = 0;
  b++; /* just seen . */

  while (*b)
    {
      if ((*b < '0') || (*b > '9'))
        {
          if ((*b != 'e') && (*b != 'E')) return(0);
          goto seen_e;
        }
      seensomething = 1;
      b++;
    }

  return(1);

 seen_e:
  if (! seensomething) return(0);
  seensomething = 0;
  b++; /* just seen e */

  if ((*b == '+') || (*b == '-'))
    b++;

  if (! *b) return(0);

  while (*b)
    {
      if ((*b < '0') || (*b > '9')) return(0);
      b++;
    }

  return(1);
  
} /* represents_float */

static dlong get_list_length(dlong *p, dlong maxlen)
{
  dlong len = 0;
  dlong *q;
  while (maxlen--)
    {
      deref(p);
      if (! is_list(p))
	{
	  if ((dlong)p == nil_atom)
            return(len);
	  return(-1);
	}
      p = q = get_list_pointer(p); deref(q);
      if (!is_smallint(q))
	return(-1);
      p++; deref(p); len++;
    }
  return(-1);
} /* get_list_length */

static dlong listorstring_to_Cstring(dlong *p, char *pc, dlong max, dlong *what, dlong achar)
{
  dlong len = 0;
  dlong *a;
  char *b = pc;

  if (is_string(p))
    {
      char *strp;
      dlong strlen;

      len = strlen = get_string_length(p);
      if (len >= max) 
	return(-1);
      strp = get_string_val(p);
      while (strlen--)
	*pc++ = *strp++;
      *pc = 0;
      return(len);
    }

  while (max--)
    {
      deref(p);
      if (is_list(p))
	{
	  p = a = get_list_pointer(p);
	  deref(a);
	  if (achar)
	    {
	      if (! is_atom(a)) return(-1);
	      *pc = (dlong) (*get_atom_name(a));
	    }
	  else
	    {
	      if (!is_smallint(a)) return(-1);
	      *pc = get_smallint(a);
	    }
	  pc++;
	  len++;
	  p = (dlong *)*(p+1);
	  continue;
	}

      if (! is_atom(p)) return(-1);

      if ((dlong)p != nil_atom)  return(-1);

      *pc = 0;

      if (! len)
	*what = MAKE_ATOM;
      else
	if (represents_integer(b))
	  *what = MAKE_INTEGER;
	else
	  if (represents_float(b))
	    *what = MAKE_FLOAT;
	  else
	    *what = MAKE_ATOM;

      return(len);
    }
  return(-1);
} /* listorstring_to_Cstring */


#ifdef ALIGNED_DOUBLE

INLINE dfloat get_aligned_real(dlong *where)
{
  conversion_type c;

  c.two_ints.i1 = *where;
  c.two_ints.i2 = *(where+1);
  return(c.f);
} /* get_aligned_real */

INLINE void push_aligned_real(dlong *h, dfloat f)
{
  conversion_type c;

  c.f = f;
  *h = c.two_ints.i1;
  *(h+1) = c.two_ints.i2;

} /* push_aligned_real */
#endif

static dlong wakeup(dlong *p, dlong *val, struct machine *machp)
{
  dlong *attributes, i;

  /* is called with p == attvar; q could be an attvar */
  /* both are dereffed */

  machp->shadow_limit_heap = 0; /* for waking up */

  p = get_attvar_pointer(p);
  underef(val);
  uvalue_trail(p,machp->BH,machp->TR,machp);
  *p = (dlong)val;

  i = get_number_of_woken_goals(machp);

  if (i + 2 > machp->nr_woken_goals)
    expand_woken_goals(machp);
  p++;
  *(machp->begin_woken_goals + i) = (dlong)p; /* the attvar or the attribute term */
  i++;
  *(machp->begin_woken_goals + i) = (dlong)val; /* the value p is bound to */
  i++;
  set_number_of_woken_goals(i,machp);
  return(1);
} /* wakeup */

static void expand_cyclic_stack(struct machine* machp)
{
  dlong **p;
  dlong offset;

  p = realloc(machp->unify_cyclic_stack,2*machp->MAXUNIFYCYCLICSTACK*sizeof(dlong *));
  if (! p) fatal_message("expansion unification stack failed\n");
  offset = p-machp->unify_cyclic_stack;
  machp->unify_cyclic_stack = p;
  machp->unify_cyclic_stack_top += offset;
  machp->MAXUNIFYCYCLICSTACK = 2*machp->MAXUNIFYCYCLICSTACK;
  machp->unify_cyclic_stack_limit = machp->unify_cyclic_stack+machp->MAXUNIFYCYCLICSTACK-3;
} /* expand_cyclic_stack */

#define CYCLIC_UNIF 1

#ifdef CYCLIC_UNIF
#define push_cyclic(x,y, machp) \
      { \
        if (machp->unify_cyclic_stack_top > machp->unify_cyclic_stack_limit) \
             expand_cyclic_stack(machp); \
        *machp->unify_cyclic_stack_top++ = x; \
        *machp->unify_cyclic_stack_top++ = (dlong *)y; \
      }

#define cyclic_struct_deref(p) while (is_ref(*p)) p = (dlong *)*p

#define init_cyclic_unification(machp) \
  machp->unify_cyclic_stack_top = machp->unify_cyclic_stack;\
  machp->unify_cyclic_stack_limit = machp->unify_cyclic_stack+machp->MAXUNIFYCYCLICSTACK-3; \

#define finish_cyclic_unification() \
  { \
    dlong **t; \
    t = machp->unify_cyclic_stack_top; \
    while (t > machp->unify_cyclic_stack) \
      { \
	**(t-2) = (dlong)*(t-1); \
	t -= 2; \
      } \
  }

#define smash_cyclic(p,q) *p = (dlong)q

#define adapt_cyclic(p,oldval,newval, machp) \
{ \
    push_cyclic(p,oldval, machp); \
    smash_cyclic(p,newval);\
}
#else
#define push_cyclic(x,y, machp) {}

#define cyclic_struct_deref(p) 

#define init_cyclic_unification(machp)

#define finish_cyclic_unification()

#define adapt_cyclic(p,oldval,newval, machp) {}
#endif

static dlong compare_terms_rec(dlong *p, dlong *q, struct machine* machp)
{

  deref(p); deref(q);

  if (p == q) return(0);

  switch (tag(p))
    {
    case REF0:
    case REF1:
      if (is_attributed(q))
	return(-1);
      if (! is_ref(q))
	return(1);
      if (p < q) return(-1);
      return(1);

    case LIST:
      if (is_atomic(q) || is_struct(q)) 
	return(1);
      if (is_ref(q) || is_attributed(q))
        return(-1);
      p = get_list_pointer(p);
      q = get_list_pointer(q);
      {
	dlong i;
	dlong *p1,*p2,*qq;
	p1 = (dlong *)*p;
	qq = (dlong *)*q; deref(qq);
        if ((p1 != p) && (p1 != qq))
	  adapt_cyclic(p,p1,qq, machp);
	p++;
	p2 = (dlong *)*p;
	qq = (dlong *)*(q+1); deref(qq);
        if ((p2 != p) && (p2 != qq))
          adapt_cyclic(p,p2,qq, machp);
        i = compare_terms_rec(p1,(dlong *)(*q), machp);
	if (i) return(i);
	q++;
	return(compare_terms_rec(p2,(dlong *)(*q), machp));
      }

    case STRUCT:
      if (is_atomic(q))
        return(1);
      if (is_list(q) || is_ref(q) || is_attributed(q))
	return(-1);
      p = get_struct_pointer(p); cyclic_struct_deref(p);
      q = get_struct_pointer(q); cyclic_struct_deref(q);
      if (p == q) return(0);
      if (*p == *q)
	{
	  dlong i,j;
	  i = get_funct_arity(*p);
	  adapt_cyclic(p,(dlong *)(*p),q, machp);
	  while (i--)
	    {
	      j = compare_terms_rec((dlong *)(*(++p)),(dlong *)(*(++q)), machp);
	      if (j) return(j);
	    }
	  return(0);
	}
      else
	{
	  dlong i,j;
	  i = get_funct_arity(*p);
	  j = get_funct_arity(*q);
	  if (i < j) return(-1);
	  if (i > j) return(1);
	  i = strcmp(get_funct_name(*p),get_funct_name(*q));
	  if (i < 0) return(-1);
	  return(1);  /* can't be equal */
	}
      return(0);

    case SIMPLE:
      if (is_char(p))
	{
	  if (is_char(q))
	    {
	      if ((dlong)p < (dlong)q) return(-1);
	      return(1);
	    }
	  if (is_number(q)) return(1);
	  return(-1);
	}

      if (is_smallint(p))
	{
	  if (is_smallint(q))
	    {
	      dlong pi = get_smallint(p);
	      dlong qi = get_smallint(q);
	      if (pi < qi) return(-1);
	      if (pi == qi) return(0);
	      return(1);
	    }
	  if (is_real(q)) return(1);
	  if (is_bigint(q))
	    {
	      q = get_number_pointer(q)+1;
	      if (bigint_positive(*q)) return(-1);
	      return(1);
	    }
	  return(-1);
	}

      if (!is_atom(q))
	{
	  if (is_atomic(q)) return(1);
	  return(-1);
	}

      {
	dlong ia, ib, i;
	ia = get_atom_index(p);
	ib = get_atom_index(q);
	if ((ia < numb_symbs) && (ib < numb_symbs))
	  {
	    i = strcmp(get_atom_name(p),get_atom_name(q));
	    if (i < 0) return(-1);
	    if (i) return(1);
	    return(0);
	  }
	if (ia < numb_symbs) return(-1);
	if (ib < numb_symbs) return(1);
	if (ia < ib) return(-1);
	if (ia == ib) return(0);
	return(1);
      }

    case NUMB:

      if (is_real(p))
	{
	  if (!is_real(q))
	    return(-1);
	  {
	    dfloat f1, f2;
	    f1 = get_real(p);
	    f2 = get_real(q);
	    if (f1 < f2) return(-1);
	    if (f1 > f2) return(1);
	    return(0);
	  }
	}

      if (is_real(q))
	return(1);
      if (! is_inumber(q))
	return(-1);

      /* p is bigint, q is bigint or smallint */

      if (is_smallint(q))
	{
	  p = get_number_pointer(p)+1;
	  if (bigint_positive(*p)) return(1);
	  return(-1);
	}

      {
	dlong er;
	return(compare_two_inumbers(p,q, machp,&er));
      }

    case STRING:
      if (is_number(q) || is_char(q)) return(1);
      if (! is_string(q)) return(-1);
      {
	dlong lenp, lenq, i, diff;
	char *cp, *cq;
	lenp = get_string_length(p);
	cp = get_string_val(p);
	lenq = get_string_length(q);
	cq = get_string_val(q);
	diff = lenp-lenq;
	if (lenq > lenp)
	  i = lenp;
	else
	  i = lenq;
	while (i--)
	  {
	    if (*cp < *cq) return(-1);
	    if (*cp > *cq) return(1);
	    cp++; cq++;
	    lenq--; lenp--;
	  }
	if (diff < 0) return(-1);
	if (diff) return(1);
	return(0);
      }

    case ATT:
      if (! is_attributed(q))
	return(1);
      if ((dlong)p < (dlong)q)
	return(-1);
      return(1);
    }

  return(0);

} /* compare_terms_rec */

/* order on terms:
   float < integer < char < string < atoms < structs < list < vars < attributed vars
*/

dlong compare_terms(dlong *p, dlong *q, struct machine* machp)
{
  dlong ret;

  init_cyclic_unification(machp);
  ret = compare_terms_rec(p,q, machp);
  finish_cyclic_unification();
  return(ret);
} /* compare_terms */

static dlong is_ground_rec(dlong *p, struct machine* machp)
{
  dlong *p0;

 tail_recursion:

  deref3(p0,p,return(0));
  switch (tag(p))
    {
    case REF0:
    case REF1:
      return(0);

    case LIST:
      if (p0)
	adapt_cyclic(p0,p,SIMPLE, machp);
      p = get_list_pointer(p);
      {
	if (!is_ground_rec(p, machp))
	  return(0);
	p++;
	goto tail_recursion;
      }

    case STRUCT:
      {
	dlong i;
	p = get_struct_pointer(p);
	if (*p == (dlong)p)
	  return(1);
	i = get_funct_arity(*p);
	adapt_cyclic(p,*p,p, machp);
	while (--i)
	  {
	    p++;
	    if (!is_ground_rec(p, machp))
	      return(0);
	  }
	p++;
	goto tail_recursion;
      }

    case SIMPLE:
    case NUMB:
    case STRING:
      return(1);

    case ATT:
      return(0);
    }

  return(1);

} /* is_ground_rec */

dlong is_ground(dlong *p, struct machine* machp)
{
  dlong ret;

  init_cyclic_unification(machp);
  ret = is_ground_rec(p, machp);
  finish_cyclic_unification();
  return(ret);
} /* is_ground */


#define VARLISTINITSTACKSIZE 100

static INLINE void varlist_push(dlong *p, struct machine* machp)
{
  if (machp->varliststacktop >= machp->varliststacksize)
    {
      if (machp->varliststack)
	{
	  machp->varliststacksize = machp->varliststacksize*2;
	  machp->varliststack = realloc(machp->varliststack,machp->varliststacksize*sizeof(dlong *));
	}
      else
	{
	  machp->varliststacksize = VARLISTINITSTACKSIZE;
	  machp->varliststack = malloc(machp->varliststacksize*sizeof(dlong *));
	}
      if (! machp->varliststack)
	fatal_message("not enough space for varlist stack\n");
    }
  machp->varliststack[machp->varliststacktop++] = p;
} /* varlist_push */

static void varlist_rec(dlong *p, struct machine* machp)
{

 tail_recursion:

  deref(p);
  switch (tag(p))
    {
    case REF0:
    case REF1:
      varlist_push(p, machp);
      adapt_cyclic(p,p,SIMPLE, machp);
      return;

    case LIST:
      p = get_list_pointer(p);
      {
	dlong *q1, *q2;
	q1 = (dlong *)*p;
	if (q1 != p)
	  adapt_cyclic(p,q1,SIMPLE, machp);
	p++;
	q2 = (dlong *)*p;
	if (q2 != p)
	  adapt_cyclic(p,q2,SIMPLE, machp);
	varlist_rec(q1, machp);
	p = q2;
	goto tail_recursion;
      }

    case STRUCT:
      {
	dlong i;
	p = get_struct_pointer(p);
        if (*p == (dlong)p)
	  return;
	i = get_funct_arity(*p);
	adapt_cyclic(p,*p,p, machp);
	while (--i)
	  {
	    p++;
	    varlist_rec(p, machp);
	  }
	p++;
	goto tail_recursion;
      }

    case SIMPLE:
    case NUMB:
    case STRING:
      return;

    case ATT:
      {
	dlong *q;
	q = get_attvar_pointer(p);
	varlist_push(q, machp);
	adapt_cyclic(q,p,SIMPLE, machp);
	return;
      }
    }

} /* varlist_rec */

dlong varlist_init(dlong *p, struct machine* machp)
{
  machp->varliststacktop = 0;
  init_cyclic_unification(machp);
  varlist_rec(p, machp);
  finish_cyclic_unification();
  return(machp->varliststacktop);
} /* varlist_init */

void varlist_final(dlong *h, dlong *p, struct machine* machp)
{
  dlong **t;
  dlong i = 0;
  while (machp->varliststacktop--) 
    { 
      *h = make_list(h+1);
      h++;
      *h = (dlong)(machp->varliststack[i++]);
      h++;	  
    }
  *h = (dlong)p;
} /* varlist_final */


/******************************************/
static void all_varlist_rec(dlong *p, struct machine* machp)
{

 tail_recursion:

  deref(p);
  switch (tag(p))
    {
    case REF0:
    case REF1:
      varlist_push(p, machp);
      return;

    case LIST:
      p = get_list_pointer(p);
      {
	dlong *q1, *q2;
	q1 = (dlong *)*p;
	if (q1 != p)
	  adapt_cyclic(p,q1,SIMPLE, machp);
	p++;
	q2 = (dlong *)*p;
	if (q2 != p)
	  adapt_cyclic(p,q2,SIMPLE, machp);
	all_varlist_rec(q1, machp);
	p = q2;
	goto tail_recursion;
      }

    case STRUCT:
      {
	dlong i;
	p = get_struct_pointer(p);
        if (*p == (dlong)p)
	  return;
	i = get_funct_arity(*p);
	adapt_cyclic(p,*p,p, machp);
	while (--i)
	  {
	    p++;
	    all_varlist_rec(p, machp);
	  }
	p++;
	goto tail_recursion;
      }

    case SIMPLE:
    case NUMB:
    case STRING:
      return;

    case ATT:
      {
	dlong *q;
	q = get_attvar_pointer(p);
	varlist_push(q, machp);
	p = q+1;
	goto tail_recursion; // recent change 10-7-2004
      }
    }

} /* all_varlist_rec */

dlong all_varlist_init(dlong *p, struct machine* machp)
{
  machp->varliststacktop = 0;
  init_cyclic_unification(machp);
  all_varlist_rec(p, machp);
  finish_cyclic_unification();
  return(machp->varliststacktop);
} /* all_varlist_init */

void all_varlist_final(dlong *h, dlong *p, struct machine* machp)
{
  dlong **t;
  dlong i = 0;
  while (machp->varliststacktop--) 
    { 
      *h = make_list(h+1);
      h++;
      *h = (dlong)(machp->varliststack[i++]);
      h++;	  
    }
  *h = (dlong)p;
} /* all_varlist_final */


/******************************************/

static INLINE dlong unify_terms_rec(dlong *p, dlong *q, struct machine *machp)
{
  dlong *p0;

  unif_countplusplus;

 more_unify:
  rec_unif_countplusplus;
  deref3(p0,p,goto first_is_var);
  deref2(q,goto second_is_var);

  if (tag(p) != tag(q))
    goto check_attributed_vars;

  if (is_struct(p)) goto both_struct;

  if (is_list(p)) goto both_list;

  if (has_atom_tag(p))
    {
      unifatomatomplusplus;
      return(p == q);
    }

  if (is_attributed(p))
    { /* unification of two attributed vars */
      dlong *p1, *q1;
      unifdeldelplusplus;
      if (p == q)
	return(1);
      p1 = get_attvar_pointer(p);
      q1 = get_attvar_pointer(q);
      if (p1 > q1) 
	return(wakeup(p,q,machp));
      return(wakeup(q,p,machp));
    }

  if (is_string(p))
    {
      dlong len;
      unifstringstringplusplus;
      if (! is_string(q)) return(0);
      len = get_string_length(p);
      if (len != get_string_length(q)) return(0);
      if (equal_strings(len,get_string_val(p),get_string_val(q)))
	return(1);
      return(0);
    }

  if (is_bigint(p))
    {
      dlong er;
      if (! is_bigint(q)) return(0);
      if (compare_two_inumbers(p,q, machp, &er)) return(0);
      return(1);
    }
  /* both must be real */
  {
    dfloat pi,qi;
    unifrealrealplusplus;
    if (! is_real(q)) return(0);
    pi = get_real(p);
    qi = get_real(q);
    return(pi == qi);
  }

  
 first_is_var:
    deref2(q,goto both_are_var);
    unifvarotherplusplus;
    utrail(p,machp->BH,machp->TR,machp);
    if (is_attributed(q))
      *p = (dlong)get_attvar_pointer(q);
    else
      *p = (dlong)q;
    return(1);

 second_is_var:
    unifvarotherplusplus;
    utrail(q,machp->BH,machp->TR,machp);
    if (is_attributed(p))
      *q = (dlong)get_attvar_pointer(p);
    else
      *q = (dlong)p;
    return(1);

 both_are_var:
    unifvarvarplusplus;
    if (p == q) return(1);
    if (p < q)
      {
	utrail(q,machp->BH,machp->TR,machp);
	*q = (dlong)p;
      }
    else
      {
	utrail(p,machp->BH,machp->TR,machp);
	*p = (dlong)q;
      }
    return(1);

 both_list:
    {
      dlong *p1;
      uniflistlistplusplus;
      if (p == q) return(1);
      p1 = p;
      p = get_list_pointer(p);
      if (p0)
	adapt_cyclic(p0,p1,q, machp);
      q = get_list_pointer(q);
      if (! unify_terms_rec(p,q,machp)) return(0);
      p++; q++;
      goto more_unify;
    }


 both_struct:
  {
    dlong arity;

    unifstructstructplusplus;
    p = get_struct_pointer(p);
    cyclic_struct_deref(p);
    q = get_struct_pointer(q);
    cyclic_struct_deref(q);
    if (p == q) return(1);
    {
      dlong pi;
      pi = *p;
      if (pi != *q) return(0);
      arity = get_funct_arity(pi);
      adapt_cyclic(p,(dlong *)pi,q, machp);
    }

    while (--arity)
      {
	p++; q++;
	if (*p != *q)
	  if (! unify_terms_rec((dlong *)*p,(dlong *)*q,machp)) return(0);
      }
    p++; q++;
    goto more_unify;
  }

 check_attributed_vars:
  /* only one can be attributed: if none is, fail
     none is undef */
  if (! is_attributed(p))
    {
      dlong *tmp;
      if (! is_attributed(q))
	return(0);
      tmp = p; p = q; q = tmp;
    }

  /* p is attributed - q not */
  unifdelotherplusplus;
  return(wakeup(p,q,machp));
} /* unify_terms_rec */

dlong unify_terms(dlong *p, dlong *q, struct machine *machp)
{
  dlong ret;

  if (p == q) return(1);
  if (has_atom_tag(p) && has_atom_tag(q)) return(0);
  init_cyclic_unification(machp);
  ret = unify_terms_rec(p,q,machp);
  finish_cyclic_unification();
  return(ret);
} /* unify_terms */

#define PRE_UNIFY_INLINE 1

#ifdef PRE_UNIFY_INLINE

#define pre_unify_terms(p1,p2,label) \
if (p1 == p2) goto label;                  \
if (is_ref(p1))                            \
{                                          \
  if (is_ref(p2))                          \
    {                                      \
      if (p2 > p1)                         \
	{                                  \
          underef(p1);                     \
          trail(p2,bh,loctrreg);           \
          *p2 = (dlong)p1;                 \
          goto label;                      \
	}                                  \
    }                                      \
  else underef(p2);                        \
  trail(p1,bh,loctrreg);                   \
  *p1 = (dlong)p2;                         \
  goto label;                              \
}                                          \
if (is_ref(p2))                            \
{                                          \
  underef(p1);                             \
  trail(p2,bh,loctrreg);                   \
  *p2 = (dlong)p1;                         \
  goto label;                              \
}                                          \
                                           \
if (! unify_terms(p1,p2,&locmach))         \
{                                          \
  goto_fail_label;                         \
}                                          \
goto label;

#else

#define pre_unify_terms(p1,p2,label) \
if (! unify_terms(p1,p2,&locmach))         \
{                                          \
  goto_fail_label;                         \
}                                          \
goto label;

#endif

void exit_mess(char *mess, dlong i)
{
  fprintf(stderr,"Exiting because ... ");
  fprintf(stderr,mess,i);
  fprintf(stderr,"\n");
  exit(1);
} /* exit_mess */


void fatal_message(char *mess)
{
  fprintf(stderr,"Exiting because ... %s\n",mess);
  exit(1);
} /* fatal_message */

char *mem2low = "startup failed because memory too low\n";

struct machine_opts DEFAULT_MACHINE_OPTS =
{
  1000000, 1000000, 1000000, 100000, 100, 0
};

static struct machine_opts ENGINE_MACHINE_OPTS =
{
  1000000, 1000000, 1000000, 100000, 100, 0
  // 500000, 500000, 500000, 50000, 100, 0
  // 10000,  10000,  10000,  1000,  100, 0
};

static dlong tmp_intro_representation_sharing = 0;

// Allocates and initializes a new machine using the given
// code pointer & options.
void init_machine(codep p, struct machine_opts *optsp, struct machine * machp)
{

  if (optsp == 0) optsp = &ENGINE_MACHINE_OPTS;

  if (!(machp->begin_woken_goals = (dlong *)malloc(sizeof(dlong *)*(optsp->wokengoals))))
      fatal_message(mem2low);
  machp->nr_woken_goals = optsp->wokengoals;

  if (!(machp->begin_heap = (dlong *)calloc(sizeof(dlong *)*(optsp->heap_size),1)))
      fatal_message(mem2low);
  //   fprintf(stderr,"heap = %ld\n",machp->begin_heap);
  machp->H = (machp->begin_heap);
  machp->end_heap = machp->begin_heap+optsp->heap_size-1;
  machp->shadow_limit_heap = machp->limit_heap = machp->begin_heap+optsp->initial_heap_size;
  machp->FH = 0;

#ifdef BITS32
  if ((((dlong)machp->begin_heap) & 0x80000000) ||
      (((dlong)machp->end_heap) & 0x80000000))
    {
      fprintf(stderr,"%x %x\n",(unsigned int)machp->begin_heap,(unsigned int)machp->end_heap);
      fprintf(stderr,"%ld %ld\n",(dlong)machp->begin_heap,(dlong)machp->end_heap);
      fatal_message("malloc in wrong part of memory\n");
    }
#endif


  if (!(machp->TR = machp->begin_trail = (dlong *)calloc(sizeof(dlong *)*(optsp->tr_size),1)))
      fatal_message(mem2low);
  machp->end_trail = machp->begin_trail+optsp->tr_size-1;
  machp->limit_trail = machp->end_trail - SPARE_TR;

  if (!(machp->end_ls = (dlong *)calloc(sizeof(dlong *)*(optsp->ls_size+1),1)))
      fatal_message(mem2low);
  machp->TOS = machp->E = machp->begin_ls = machp->end_ls+optsp->ls_size-1;
  machp->limit_ls = machp->end_ls + SPARE_LS;

  size_t cp_size = (optsp->cp_size);

  if (!(machp->end_cp = (dlong *)calloc(cp_size+1,sizeof(dlong *))))
      fatal_message(mem2low);

  machp->B = machp->begin_cp = machp->end_cp+cp_size;
  machp->limit_cp = machp->end_cp + SPARE_CP;

  machp->heap_mark_array = machp->ls_mark_array = 0;

  machp->MAXUNIFYCYCLICSTACK = 1000;
  if (!(machp->unify_cyclic_stack = malloc(machp->MAXUNIFYCYCLICSTACK*sizeof(dlong*))))
    fatal_message(mem2low);

  machp->findall_solutions = 0;
  machp->MAX_FINDALLS = 50;
  machp->nextfree = 0;

  machp->varliststacksize = 0;
  machp->varliststack = 0;
  machp->varliststacktop = 0;

  set_number_of_woken_goals(0,machp);

  machp->BH = machp->H;

  machp->CONTP = machp->P = p;

  machp->openfiles[0].fp = stdin;
  machp->openfiles[0].f_symbol = sym_lookup(0,"stdin",USER_MOD);
  machp->file_in_nr = 0;
  machp->openfiles[0].opentype = FILE_READ;
  machp->openfiles[0].line_number = 1;
  machp->openfiles[0].ungetnr = 0;

  machp->openfiles[1].fp = stdout;
  machp->openfiles[1].f_symbol = sym_lookup(0,"stdout",USER_MOD);
  machp->file_out_nr = 1;
  machp->openfiles[1].opentype = FILE_WRITE;

  machp->openfiles[2].fp = stderr;
  machp->openfiles[2].f_symbol = sym_lookup(0,"stderr",USER_MOD);
  machp->openfiles[2].opentype = FILE_WRITE;

  machp->nr_open_files = 3;

  machp->nr_heapgc = 0;
  machp->nr_minorheapgc = 0;
  machp->total_collected1 = 0;
  machp->total_collected2 = 0;
  machp->total_sharedgain1 = 0;
  machp->total_sharedgain2 = 0;
  machp->total_gc_time = 0;
  machp->total_repshar_time = 0;
  machp->nr_heapexp = 0;
  machp->nr_lsexp = 0;
  machp->nr_cpexp = 0;
  machp->nr_trexp = 0;
  machp->time_mm = 0;
  machp->nr_excess_bigints = 0;
  machp->excess_woken = 1; /* crucial ! */
  machp->excess_bigints = NULL;
  machp->max_excess_bigints = 0;
  machp->total_size_excess_bigints = 0;

  machp->intro_representation_sharing = tmp_intro_representation_sharing;
  machp->copy_heapbarrier = 0;
  machp->int_buffer = malloc(40);
  machp->global_charbuf = malloc(GLOBAL_CHARBUF_LEN+1);

  machp->copy_local_trail = malloc(MAX_SHAR_INIT*sizeof(dlong*));

} /* init_machine */

/*-------------------------------------------------------------------------*/
void free_excess_bigint(dlong i, struct machine *machp)
{
  dlong *p = (machp->excess_bigints)[i];
  if (!p)
    fatal_message("excess bigint invariant I violated");

  free(p);
  (machp->excess_bigints)[i] = 0;
} /*free_excess_bigint */
/*-------------------------------------------------------------------------*/
void free_excess_bigints(struct machine *machp)
{
  dlong i = machp->nr_excess_bigints;
  dlong **p = machp->excess_bigints;

  while (i--)
    {
      if (p[i])
	free(p[i]);
    }

  machp->total_size_excess_bigints = machp->nr_excess_bigints = 0;
} /*free_excess_bigints */

/*-------------------------------------------------------------------------*/
dlong alloc_new_bigint(dlong siz, struct machine *machp)
{
  dlong *p;
  dlong i;

  if (((machp->excess_woken)&0x1)==0) 
    {
      free_excess_bigints(machp);
      machp->excess_woken += 1; /* don't want to destroy the nrwokengoals */
    }

  if (machp->nr_excess_bigints >= machp->max_excess_bigints)
    {
      dlong **tmp;

      if (machp->excess_bigints)
	tmp = realloc(machp->excess_bigints,2*machp->max_excess_bigints*sizeof(dlong *));
      else
	{
	  machp->max_excess_bigints = 10;
	  machp->nr_excess_bigints = 0;
	  tmp = malloc(machp->max_excess_bigints*2*sizeof(dlong *));
	}
      if (! tmp)
	fatal_message("could not allocate excess bigint array\n");
      machp->max_excess_bigints = 2*machp->max_excess_bigints;
      machp->excess_bigints = tmp;
    }

  p = malloc(siz*sizeof(dlong));
  if (! p)
    fatal_message("could not allocate excess bigint");
  i = machp->nr_excess_bigints;
  (machp->total_size_excess_bigints) += siz;
  (machp->excess_bigints)[i] = p;
  (machp->nr_excess_bigints) = i+1;
  return(i);
} /*alloc_new_bigint */

// #define DEBUG_INFO_CALL

#ifdef DEBUG_INFO_CALL
static void debug_info_call(dlong symbol, char * instr)
{
  printf("%s %s/%d\n",instr,symtab[symbol].name,symtab[symbol].arity);
} /* debug_info_call */
#else
#define debug_info_call(a,b)
#endif


#ifdef PROFILING_DEBUG
#define profile(X) fprintf(stderr,"%d %s\n",X,instr_name[X]);
#define profile_builtin(X) fprintf(stderr,"%d %s\n",X,instr_name[X]);
#define init_profiling_instructions()
#define profile_calls_output()
#define profile_wam_output()
#define profile_open()
#endif

#ifdef NO_PROFILING
#define profile(X)
#define profile_builtin(X)
#define profile_calls_output()
#define profile_wam_output()
#define init_profiling_instructions()
#define profile_open()
#endif

#ifdef PROFILING_WAM
static  type_opcode prevopc1 = 0;
static  type_opcode prevopc2 = 0;
static  dlong opcode_count[NR_OF_OPCODES];
static  dlong opcode_count2[NR_OF_OPCODES][NR_OF_OPCODES];
static  dlong opcode_count3[NR_OF_OPCODES][NR_OF_OPCODES][NR_OF_OPCODES];

static void profile(dlong opc)
{
  opcode_count[opc] += 1;
  opcode_count2[prevopc1][opc] += 1;
  opcode_count3[prevopc2][prevopc1][opc] += 1;
  prevopc2 = prevopc1;
  prevopc1 = opc;
} /* profile */

static void profile_builtin(dlong opc)
{
  opcode_count[opc] += 1;
  opcode_count2[prevopc1][opc] += 1;
  opcode_count3[prevopc2][prevopc1][opc] += 1;
  prevopc2 = prevopc1;
  prevopc1 = opc;
} /* profile */

static void init_profiling_instructions()
{
  dlong i,j,k;
  for (i = NR_OF_OPCODES ; i--; )
    {
      opcode_count[i] = 0;
      for (j = NR_OF_OPCODES ; j-- ; )
	{
	  opcode_count2[i][j] = 0;
	  for (k = NR_OF_OPCODES ; k-- ; )
	    opcode_count3[i][j][k] = 0;
	}
    }
} /* init_profiling_instructions */

static void profile_wam_output()
{
  dlong i,j,k,l;
    for (i = 0 ; instr_name[i]; i++)
      fprintf(profile_output_fd,"single %10d %s\n",opcode_count[i],instr_name[i]);

    for (i = 0 ; instr_name[i]; i++)
      for (j = 0 ; instr_name[j]; j++)
	{
	  k = opcode_count2[i][j];
	  if (k)
	    fprintf(profile_output_fd,"double %10d %s %s\n",k,instr_name[i],instr_name[j]);
	}
    for (i = 0 ; instr_name[i]; i++)
      for (j = 0 ; instr_name[j]; j++)
	for (l = 0 ; instr_name[l]; l++)
	  {
	    k = opcode_count3[i][j][l];
	    if (k)
	      fprintf(profile_output_fd,"triple %10d %s %s %s\n",k,instr_name[i],instr_name[j],instr_name[l]);
	  }

    fprintf(profile_output_fd,"call count %10d general unify\n",unif_count);
    fprintf(profile_output_fd,"call count %10d recursive general unify\n",rec_unif_count);
    fprintf(profile_output_fd,"call count %10d unifintint\n", unifintint);
    fprintf(profile_output_fd,"call count %10d unifatomatom\n", unifatomatom);
    fprintf(profile_output_fd,"call count %10d unifdeldel\n", unifdeldel);
    fprintf(profile_output_fd,"call count %10d unifrealreal\n", unifrealreal);
    fprintf(profile_output_fd,"call count %10d unifstringstring\n", unifstringstring);
    fprintf(profile_output_fd,"call count %10d unifvarother\n", unifvarother);
    fprintf(profile_output_fd,"call count %10d unifvarvar\n", unifvarvar);
    fprintf(profile_output_fd,"call count %10d uniflistlist\n", uniflistlist);
    fprintf(profile_output_fd,"call count %10d unifstructstruct\n", unifstructstruct);
    fprintf(profile_output_fd,"call count %10d unifdelother\n", unifdelother);

} /* profile_wam_output */

#define profile_calls_output()

static void profile_open()
{
  if (profile_output)
    {
      profile_output_fd = fopen(profile_output,"w");
      if (! profile_output_fd)
	exit(0);
    }
  else
    profile_output_fd = stderr;
} /* profile_open */
#endif

#ifdef PROFILING_CALLS
static  dlong opcode_count[NR_OF_OPCODES];

#define profile(X) 

static void profile_builtin(dlong opc)
{
  opcode_count[opc] += 1;
} /* profile */

static void init_profiling_instructions()
{
  dlong i;

  for (i = NR_OF_OPCODES ; i--; )
    {
      opcode_count[i] = 0;
    }
} /* init_profiling_instructions */


static void profile_wam_output()
{
    fprintf(profile_output_fd,"call count %10d general unify\n",unif_count);
    fprintf(profile_output_fd,"call count %10d recursive general unify\n",rec_unif_count);
    fprintf(profile_output_fd,"call count %10d unifintint\n", unifintint);
    fprintf(profile_output_fd,"call count %10d unifatomatom\n", unifatomatom);
    fprintf(profile_output_fd,"call count %10d unifdeldel\n", unifdeldel);
    fprintf(profile_output_fd,"call count %10d unifrealreal\n", unifrealreal);
    fprintf(profile_output_fd,"call count %10d unifvarother\n", unifvarother);
    fprintf(profile_output_fd,"call count %10d unifvarvar\n", unifvarvar);
    fprintf(profile_output_fd,"call count %10d uniflistlist\n", uniflistlist);
    fprintf(profile_output_fd,"call count %10d unifstructstruct\n", unifstructstruct);
    fprintf(profile_output_fd,"call count %10d unifdelother\n", unifdelother);

} /* profile_wam_output */

static void profile_calls_output()
{
  dlong i,j;

  j = number_of_symbols();
    for (i = 0; i < j; i++)
      {
	if (symtab[i].callcount)
	  {
	    char *mods = symtab[symtab[i].module].name;
	    if (strcmp(mods,"sysh") && strcmp(mods,"toc_compile"))
	      fprintf(profile_output_fd,"call count %10d prolog pred: %s/%d %s\n",symtab[i].callcount,symtab[i].name,symtab[i].arity,mods);
	  }
      }
    for (i = 0 ; instr_name[i]; i++)
      {
	char *s = instr_name[i];
	if (opcode_count[i])
	{
	  if (! strncmp("builtin_",s,8))
	    s += 8;
	  fprintf(profile_output_fd,"call count %10d builtin: %s\n",opcode_count[i],s);
	}
      }
} /* profile_calls_output */

static void profile_open()
{
  if (profile_output)
    {
      profile_output_fd = fopen(profile_output,"w");
      if (! profile_output_fd)
	exit(0);
    }
  else
    profile_output_fd = stderr;
} /* profile_open */

#endif


void init_selected_symbols()
{
  atom_at_smaller = make_atom(sym_lookup(0,"<",USER_MOD)); 
  atom_at_bigger = make_atom(sym_lookup(0,">",USER_MOD)); 
  atom_at_equal = make_atom(sym_lookup(0,"=",USER_MOD)); 
  symbol_equal_2 = sym_lookup(2,"=",USER_MOD); 
  atom_repshar = make_atom(sym_lookup(0,"repshar",USER_MOD)); 
  atom_verbose = make_atom(sym_lookup(0,"verbose",USER_MOD)); 
  atom_very_verbose = make_atom(sym_lookup(0,"very_verbose",USER_MOD)); 
  atom_prompt = make_atom(sym_lookup(0,"prompt",USER_MOD)); 
  value_prompt = make_atom(sym_lookup(0,"?- ",USER_MOD)); 
  exclude_list_trigger_3 = sym_lookup(3,"exclude_list_trigger",USER_MOD); 
  deal_with_sigint_1 = sym_lookup(1,"deal_with_sigint",SYSTEM_MOD); 
  deal_with_wakeup_2 = sym_lookup(2,"deal_with_wakeup",SYSTEM_MOD); 
  deal_with_wakeup_2 = sym_lookup(2,"deal_with_wakeup",SYSTEM_MOD);
  deal_with_wakeup_of_one_3 = sym_lookup(3,"deal_with_wakeup_of_one",SYSTEM_MOD);
  deal_with_wakeup_of_two_5 = sym_lookup(5,"deal_with_wakeup_of_two",SYSTEM_MOD);

  engine_toplevel_goal_0 = sym_lookup(0, "engine_toplevel", SYSTEM_MOD);


  sysh_conjunction_2 = sym_lookup(2,"conjunction",SYSTEM_MOD);

  colon_2 = sym_lookup(2,":",USER_MOD); 
  symbol_true_0 = sym_lookup(0,"true",USER_MOD); 
  symbol_halt_0 = sym_lookup(0,"halt",USER_MOD); 
  true_atom = make_atom(symbol_true_0);
  symbol_error_unknown_pred_1 = sym_lookup(1,"error_unknown_pred",SYSTEM_MOD); 
  atom_dot_0 = make_atom(sym_lookup(0,".",USER_MOD)); 
  atom_yes = make_atom(sym_lookup(0,"yes",USER_MOD)); 
  atom_no = make_atom(sym_lookup(0,"no",USER_MOD)); 
  symbol_dot_2 = sym_lookup(2,".",USER_MOD); 
  symbol_error_message_3 = sym_lookup(3,"error_message",SYSTEM_MOD); 
  symbol_error_builtin_arith5 = sym_lookup(5,"error_builtin_arith5",SYSTEM_MOD); 
  symbol_error_builtin_arith4 = sym_lookup(4,"error_builtin_arith4",SYSTEM_MOD); 
  symbol_cutfail_0 = sym_lookup(0,"cutfail",SYSTEM_MOD); 
  atom_default_prompt = make_atom(sym_lookup(0,"   ",USER_MOD)); 
  atom_on = make_atom(sym_lookup(0,"on",USER_MOD)); 
  atom_cut = make_atom(sym_lookup(0,"!",USER_MOD)); 

  funct_comma_2 = make_funct(sym_lookup(2,",",USER_MOD));
  funct_semicolon_2 = make_funct(sym_lookup(2,";",USER_MOD));
  funct_arrow_2 = make_funct(sym_lookup(2,"->",USER_MOD));
  funct_if_3 = make_funct(sym_lookup(3,"if",USER_MOD));
  funct_continuation_3 = make_funct(sym_lookup(3,"$cont$",USER_MOD));
  funct_continuation_2 = make_funct(sym_lookup(2,"$cont$",USER_MOD));
  funct_call_continuation_1 = make_funct(sym_lookup(1,"call_continuation",USER_MOD));

} /* init_selected_symbols */


const struct machine_opts SMALLEST_MACHINE_OPTS =
{
  SPARE_HEAP + 500,
  SPARE_LS + 20,
  SPARE_CP + 20,
  SPARE_TR + 10,
  1,
  0
};

#define FREE(p) if (p) free(p)
#define FREE1(p) if (p) free(p-1)

static void dealloc_machine(struct machine * machp)
{
  //  fprintf(stderr,"machine %ld\n",(dlong)machp);
  //  FREE(machp->begin_heap);
  FREE(machp->begin_trail);
  //  FREE(machp->end_ls);
  // FREE(machp->end_cp);
  FREE(machp->begin_woken_goals);
  FREE1(machp->heap_mark_array); // heap_mark_array is offset by 1
  FREE(machp->ls_mark_array);
  FREE(machp->unify_cyclic_stack);
  FREE(machp->varliststack);
} /* dealloc_machine */


REGISTER codep locpc ASM_PC;

dlong execute_machine(codep initialpc, dlong first_time, 
		      struct machine **setmach, 
		      struct machine_opts *mach_optsp, struct engine *tmpengine)
{
#include "exec_mach_macros.h"

  register dlong *WAMS = 0; /* the unification structure pointer */

  struct machine *locmachp;
#define locmach (*locmachp)
  dlong return_code = 0;
  char *arithsymb;
  dlong *bridge = 0;
  dlong glob_arity;
  dlong glob_nrbuiltin;
  struct engine* thisengine;

  if (first_time == 1)
    {
#include "instr_labels.c"
      return 0;
    }

  locmachp = malloc(sizeof(struct machine));

  init_machine(initialpc, mach_optsp, &locmach);
  if (setmach != NULL) *setmach = &locmach;

  thisengine = tmpengine;
  thisengine->locmachp = &locmach;

  init_profiling_instructions();

  locpc = preg;

  //  goto_more_exec;
  goto try_me_else_label;


#ifdef PLAIN_SWITCH
 more_exec:

  opc = *(type_opcode *)locpc;

//    if (1) // print_instr)
//       {
// 	fprintf(stderr,"%ld %ld\n",opc,locpc);
//         fprintf(stderr,"%s\n", instr_name[opc]);
// // //       fprintf(stderr,"%ld %s %ld %ld\n",thisengine, instr_name[opc], opc, locpc);
// // //       fprintf(stderr,"%s  %lx %lx %lx %lx %lx ",instr_name[opc],locmach.TOS,
// // //  	      locmach.H,locmach.limit_ls, locmach.CONTP, locpc);
// // //       fprintf(stderr,"\n");
//       }

  switch (opc)
#endif

    {

#include "machine.h"

    }


 end_session: ;

  pthread_mutex_lock(&thisengine->mutex);

  // Pop the cleanup handler for the locmach, and execute it
  dealloc_machine(&locmach);
  thisengine->locmachp = NULL;
  if (thisengine->start_goal->first_chunk != NULL)
    findall_free(0, thisengine->start_goal, &locmach);
  
  pthread_mutex_unlock(&thisengine->mutex);

  profile_open();
  profile_calls_output();
  profile_wam_output();

  return(return_code);
} /* execute_machine */

#include "banner.h"

void sigint_handler(int signum);
void sigterm_handler(int signum);

static void dealloc_shared_memory()
{
  free(codezone);
  free_symboltables();
} /* dealloc_shared_memory */

int main(int argc, char *argv[])
{
  char *initialf = "dp_boot.w";
  char *initialg = "boot";
  char initialfile[100];
  char initialgoal[100];
  dlong i,j;
  char temp_buf[1024];
  char c;
  struct machine_opts mach_opts;
  dlong show_banner = 1;

  if (ENGINE_MACHINE_OPTS.initial_heap_size == 0 ||
      (ENGINE_MACHINE_OPTS.heap_size - ENGINE_MACHINE_OPTS.initial_heap_size) < SPARE_HEAP)
    ENGINE_MACHINE_OPTS.initial_heap_size = ENGINE_MACHINE_OPTS.heap_size - SPARE_HEAP;

  mach_opts = DEFAULT_MACHINE_OPTS;

  execute_machine(NULL, 1, NULL, NULL, NULL);
  strcpy(temp_buf,install_dir);
  strcat(temp_buf,initialf);
  initialf = temp_buf;
  glob_max_argc = -1;
  glob_min_argc = 0;


  for (i = 1; i < argc; i++)
    {
      if (*argv[i] != '-') continue;
      switch (argv[i][1])
	{
	case 'b':
	  show_banner = 0;
	  break;
	case 'f':
	  i++;
	  strcpy(initialfile,argv[i]);
	  initialf = initialfile;
	  break;
	case 'D':
	  i++;
	  strcpy(temp_buf,argv[i]);
	  strcat(temp_buf,"/dp_boot.w");
	  initialf = temp_buf;
	  strcpy(install_dir,argv[i]);
	  strcat(install_dir,"/");
	  break;
	case 'I':
	  i++;
	  strcpy(install_dir,argv[i]);
	  strcat(install_dir,"/");
	  strcpy(temp_buf,argv[i]);
	  strcat(temp_buf,"/");
	  strcat(temp_buf,"dp_boot.w");
	  initialf = temp_buf;
	  printf("initialf = %s\n",initialf);
	  break;
	case 'g':
          i++;
          strcpy(initialgoal,argv[i]);
          initialg = initialgoal;
	  break;
	case 'r':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  tmp_intro_representation_sharing = j;
	  break;
	case 'h':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  mach_opts.heap_size = j;
	  if (mach_opts.heap_size < SMALLEST_MACHINE_OPTS.heap_size)
	    mach_opts.heap_size = SMALLEST_MACHINE_OPTS.heap_size;
	  break;
	case 'i':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  mach_opts.initial_heap_size = j;
	  break;
	case 'l':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  mach_opts.ls_size = j;
	  if (mach_opts.ls_size < SMALLEST_MACHINE_OPTS.ls_size)
            mach_opts.ls_size = SMALLEST_MACHINE_OPTS.ls_size;
	  break;
	case 'c':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  mach_opts.cp_size = j;
	  if (mach_opts.cp_size < SMALLEST_MACHINE_OPTS.cp_size)
            mach_opts.cp_size = SMALLEST_MACHINE_OPTS.cp_size;
	  break;
	case 't':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  mach_opts.tr_size = j;
	  if (mach_opts.tr_size < SMALLEST_MACHINE_OPTS.tr_size)
            mach_opts.tr_size = SMALLEST_MACHINE_OPTS.tr_size;
	  break;
	case 'v':
	  sscanf(&(argv[i][2]),"%c",&c);
	  if (c == '+')
	    verbose_mode = 1;
	  else
	    if (c == '-')
	      verbose_mode = 0;
	  else
	    goto bad_option;
	  break;
	case 'p':
	  precise_profile_call_site_opt = 1;
	  break;
	case 'V':
	  sscanf(&(argv[i][2]),"%c",&c);
	  if (c == '+')
	    verbose_mode = very_verbose_mode = 1;
	  else
	    if (c == '-')
	      verbose_mode = very_verbose_mode = 0;
	  else
	    goto bad_option;
	  break;

	case 'Q':
	  profile_output = &(argv[i][2]);
	  break;

	case 'G':
	  generational_gc = 1 - generational_gc;
	  break;

	case 'F':
	  print_instr = 1;
	  break;

	case 'T':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  compress_table = j;
	  break;

	case 'S':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  peep_spec = j;
	  break;

	case 'P':
	  /* all rest is meant to be passed to the user program */
	  /* by means of the predicate argv/2                   */
	  /* we just save information so that later these args can be retrieved */
	  glob_max_argc = argc-1;
	  glob_min_argc = i+1;
	  glob_argv = argv;
	  i = argc; // to make sure it ends here
	  break;
	case 'O':
	  overwrite_without_asking = 1;
	  break;

#ifdef MULTI_THREADED

	case 'E':
	  sscanf(&(argv[i][2]),"%ld",&j);
	  MAX_ENGINES = j;
	  MAX_HUBS = j;
	  break;
#endif

	case 'H':
	help_option:
	  printf("-H      : this help\n");
	  printf("-b      : do not show banner\n");
	  printf("-cN     : size choice point stack - N = number of cells\n");
	  printf("-hN     : size heap - N = number of cells allocated\n");
	  printf("-iN     : initial heap size - N = number of cells to be used\n");
	  printf("-lN     : size local stack - N = number of cells\n");
	  printf("-tN     : size trail stack - N = number of cells\n");
	  printf("-vX     : verbose on/off - X = + or -\n");
	  printf("-VX     : very verbose on/off - X = + or -\n");
	  printf("-p      : turn precise profiling on (affects loading)\n");
	  printf("-P      : what follows is available to argv/2\n");
	  printf("-G      : don't do generational gc\n");
	  printf("-O      : silent replacing predefined system predicates - I hope you know what you are doing\n");
	  printf("-SN     : N = two bits: peephole + specialization; default = 3\n");
	  printf("-rN     : N = number; 2 = repshar+gc; 1 = repshar, 0 = none\n");
#ifdef MULTI_THREADED
	  printf("-EN     : N = maximum amount of engines & hubs\n");
#endif
	  exit(0);

	default:
// 	  /* all rest is meant to be passed to the user program */
//           /* by means of the predicate argv/2                   */
//           /* we just save information so that later these args can be retrieved */
//           glob_max_argc = argc-1;
//           glob_min_argc = i;
//           glob_argv = argv;
//           i = argc; // to make sure it ends here
//           break;


 	bad_option:
 	  printf("You used a bad option ...\n");
 	  goto help_option;
	  break;
	}
    }

  if (show_banner) banner(stderr);

  init_symbol_tables();
  init_codezone();

  init_profiling_instructions();

  if (mach_opts.initial_heap_size == 0 ||
      (mach_opts.heap_size - mach_opts.initial_heap_size) < SPARE_HEAP)
      mach_opts.initial_heap_size = mach_opts.heap_size - SPARE_HEAP;

  {
    dlong res = load_compiled_file(initialf,1);
    switch (res)
      {
      case BAD_VERSION_NUMBER:
	fprintf(stderr,"bad version number in boot file %s\n",initialf); exit(0);

      case NOTENOUGHSPACE:
	fprintf(stderr,"not enough space for boot file %s\n",initialf); exit(0);
	
      case FILENOTOPEN:
        fprintf(stderr," unable to open boot file %s\n",initialf); exit(0);
	
      default:
	if (res != ALLOK)
	  { fprintf(stderr," something wrong with boot file %s\n",initialf); exit(0); }
      }
  }


  /* mmap exclusion only on 32-bit linux - maybe no longer needed */

  {
#ifndef MACBOOKPRO
#ifdef BITS32
         mallopt(M_MMAP_THRESHOLD,512000);
         mallopt(M_MMAP_MAX,0);
#endif
#endif
  }


  {
    codep initialpc;
    struct engine *tmpengine;

    initialpc = sym_codep(0,initialg,USER_MOD);
    if (!initialpc)
    {
      fprintf(stderr,"no goal %s/0 in file %s\n",initialg,initialf);
      return(0);
    }
    // This struct will be copied by execute machine.
    {
      dlong i;
      sigset_t signal_set;
      pthread_t sig_thread;

      struct sigaction sigint_action;
      sigfillset(&sigint_action.sa_mask);
      sigint_action.sa_flags = 0;
      sigint_action.sa_handler = sigint_handler;
      sigaction(SIGINT, &sigint_action, NULL);

      struct sigaction sigterm_action;
      sigfillset(&sigterm_action.sa_mask);
      sigterm_action.sa_flags = 0;
      sigterm_action.sa_handler = sigterm_handler;
      sigaction(ENGINE_CANCEL_SIGNAL, &sigterm_action, NULL);
      //Block all signals in this and all sub-threads
      //sigfillset(&signal_set);
      //pthread_sigmask(SIG_BLOCK, &signal_set, NULL);
      //Create the signal-handler thread
      //pthread_create(&sig_thread, NULL, sigint_handler, NULL);
    
      tmpengine = currentengine = init_engines_hubs();
      // First machine sets mainmach
      i = execute_machine(initialpc, 0, &mainmachp, &mach_opts, tmpengine);

      // Resources are deallocated by execute_machine

      //Signal termination to the signal handler
      //pthread_kill(sig_thread, SIGTERM);
      //pthread_join(sig_thread, NULL);
    }

    engines_cleanup();
    dealloc_shared_memory();
    exit(0); // ???
    exit(i);
  }

} /* main */

void sigint_handler(int signum)
{
  if (mainmachp == NULL)
    fatal_message("unable to handle interrupt, sorry - it came too quick");

  mainmachp->shadow_limit_heap = 0;
  interrupt_flag = 1;
}

void sigterm_handler(int signum)
{
  if (currentengine == NULL)
    fatal_message("unable to handle interrupt, sorry - it came too quick");

  currentengine->locmachp->shadow_limit_heap = 0;
  currentengine->cancelled = 1;
}
