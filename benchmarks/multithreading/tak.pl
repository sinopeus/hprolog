run_tak(Threads, (X, Y, Z)) :-
	Threads > 0,
	tak_mt(Threads, X, Y, Z, _).

run_tak_native(Threads, (X, Y, Z)) :-
	Threads > 0,
	tak_mt_native(Threads, X, Y, Z, _).

tak_mt(1, X, Y, Z, A) :-
	!,
	tak_st(X, Y, Z, A).
tak_mt(_, X, Y, Z, A) :-
	X =< Y, !,
	Z = A.
tak_mt(Threads, X, Y, Z, A) :-
	Threads3 is Threads//3,
  %X > Y,
	X1 is X - 1,
	Y1 is Y - 1,
	Z1 is Z - 1,
	threaded((
		tak_mt(Threads3, X1, Y, Z, A1),
		tak_mt(Threads3, Y1, Z, X, A2),
		tak_mt(Threads3, Z1, X, Y, A3)
	)),
	tak_st(A1, A2, A3, A).

tak_mt_native(1, X, Y, Z, A) :-
	!,
	tak_st(X, Y, Z, A).
tak_mt_native(_, X, Y, Z, A) :-
	X =< Y, !,
	Z = A.
tak_mt_native(Threads, X, Y, Z, A) :-
	Threads3 is Threads//3,
  %X > Y,
	X1 is X - 1,
	Y1 is Y - 1,
	Z1 is Z - 1,
  hub(Hub),
	spawn_link(Hub, A1, tak_mt_native(Threads3, X1, Y, Z, A1), T1),
	spawn_link(Hub, A2, tak_mt_native(Threads3, Y1, Z, X, A2), T2),
	spawn_link(Hub, A3, tak_mt_native(Threads3, Z1, X, Y, A3), T3),
  receive(Hub, T1, the(A1)),
  receive(Hub, T2, the(A2)),
  receive(Hub, T3, the(A3)),
  stop(Hub),
	tak_st(A1, A2, A3, A).

tak_st(X, Y, Z, A) :-
	X =< Y, !,
	Z = A.
tak_st(X, Y, Z, A) :-
  %X > Y,
	X1 is X - 1,
	tak_st(X1, Y, Z, A1),
	Y1 is Y - 1,
	tak_st(Y1, Z, X, A2),
	Z1 is Z - 1,
	tak_st(Z1, X, Y, A3),
	tak_st(A1, A2, A3, A).

