/*
Thu Sep  6 21:43:15 CEST 2001

A plain and simple copying gc will be implemented, ala Bevemyr-Lindberg.
The idea is to experiment also with different options, before we'll really
commit to something and then add generations

Thu Sep 27 08:12:20 CEST 2001

Works for a couple of days - now is time to go for generational.

But first some expansions ...
Tue Oct  8 17:27:47 CEST 2001: expansions and gc seem to work properly ...

Some comments from the optimistic gc:

Most recent documented change:
Wed Jan  8 13:03:51 CET 2003       introduction NUMB tag
Thu Jan  9 19:28:58 CET 2003       introduction STRING tag
Feb 2003 and onwards               support for BIGINTS


Tue Nov 25 11:08:31 CET 2003

This collector has not been active since version 1.8.
It is time to revive it, because some things were irrepairably wrong with
the optimistic collector - related to attributed variables and because
also the bigints living outside the heap cause problems when wanting
to know the size of live data

Sun Feb 12 08:40:05 CET 2006

Going to introduce preserving segments by counting ...

Tue Apr 11 15:21:26 CEST 2006

Making real work of generational ...

Mon Aug 15 17:35:41 CEST 2011

5 years ago, that generational idea once again failed - trying again

why did it fail: since variables (selfrefs) only live on the heap, and
not on the local stack, hProlog has no "unsafe" variables
but this means that env slots older in the old generation can point to
the new generation without being trailed; similar for the *v instructions;
there are three choices:

1) have selfrefs in the ls and unsafe vars
2) conditionally trail var creation (and *v instr)
3) scan the old env also when doing gengc

neither of them is attractive - currently, we'll do 3)

Wed Aug 17 12:44:00 CEST 2011

Several tests are working now ... making it the default for trying out

Wed Aug 24 11:29:38 CEST 2011
A problem with trail entries pointing to oldgen and putting 666 in unmarked slots solved
Thu Aug 25 12:59:22 CEST 2011
A problem with multiple trail entries pointing to oldgen ...


*/

extern void fatal_message(char *);
extern char *mem2low;

#include "toinclude.h"
#include <stdlib.h>
#include <string.h>

// #define DEBUG_GC

dlong doprintls = 1;
dlong doprinttr = 1;
dlong doprintcp = 1;
dlong doprintheap = 1;
dlong doprintatoms = 1;

static   dlong *translationtable = 0;

#ifdef DEBUG_GC
#define debug_print_heap(a,b,c,d) print_heap(a,b,c,d)
#define alloc_translationtable(size) translationtable = (dlong *)calloc(size,sizeof(dlong *))
#define fill_translationtable translationtable[lnext-temp_heap] = beginp - begin_heap
#define dealloc_translationtable if (translationtable) free(translationtable)
#define sanity_check  1
#define print_scan_next_array(a,b,c) print_scan_next_array1(a,b,c)
#else
#define debug_print_heap(a,b,c,d)
#define alloc_translationtable(size)
#define fill_translationtable 
#define dealloc_translationtable
#define sanity_check  0
#define print_scan_next_array(a,b,c)
#endif


// #define STAT_GC

#ifdef STAT_GC
static dlong addcalled = 0;
static dlong addcalled1 = 0;
static dlong whichcalled = 0;
static dlong whichcalleddirect = 0;

#define whichcalleddirectplusplus whichcalleddirect++
#define whichcalledplusplus whichcalled++
#define addcalledplusplus addcalled++
#define addcalled1plusplus addcalled1++

#else
#define whichcalleddirectplusplus
#define whichcalledplusplus
#define addcalledplusplus 
#define addcalled1plusplus

#endif

extern void free_excess_bigint(dlong, struct machine *);
extern dlong generational_gc;

#define simple_active(i) (i & ~(0x7fffffff))

/*-------------------------------------------------------------------------
The choices_array will contain for each segment:

 the start address
 the number of live cells in it

These two are actually in two separate arrays
choices_array_start
choices_array_live

The array is build up at the beginning of the gc.

We also need a scan and a next pointer for each segment - we'll just
make sure that they are allocated when we do the choices_array.
-------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------*/
static void alloc_choices_array(dlong nr_choices,dlong ***choices_array_startp,
				dlong **choices_array_livep,
				dlong **scan_next_arrayp)
{
  *choices_array_startp = malloc(nr_choices*sizeof(dlong));
  *choices_array_livep = malloc(nr_choices*sizeof(dlong));
  *scan_next_arrayp = malloc(nr_choices*sizeof(dlong*)*2);
  if ((*choices_array_startp) && (*choices_array_livep) && (*scan_next_arrayp))
    return;
  fatal_message("Could not allocate necessary memory during gc");
} /* alloc_choices_array */

/*-------------------------------------------------------------------------*/
static void  free_choices_array(dlong **choices_array_start,
				dlong *choices_array_live,
				dlong *scan_next_array)
{
  free(choices_array_start);
  free(choices_array_live);
  free(scan_next_array);
} /* free_choices_array */

/*-------------------------------------------------------------------------*/
static void build_scan_next_array(dlong *t, dlong choices_array_top,
				  dlong *scan_next_array,
				  dlong *choices_array_live)
{
  dlong i, j;

  i = 0;
  for (j = 0; j <= choices_array_top ; j++)
    {
      scan_next_array[i+1] = scan_next_array[i] = (dlong)t;
      t += choices_array_live[j];
      i += 2;
    }

} /* build_scan_next_array */

/*-------------------------------------------------------------------------*/
static void print_scan_next_array1(dlong choices_array_top, dlong *scan_next_array, char *s)
{
  dlong i = 0;
  dlong j = 0;

  fprintf(stderr,"%s\n",s);
  while (i <= choices_array_top)
    {
      fprintf(stderr,"scan = %ld; next = %ld\n",
             scan_next_array[j], scan_next_array[j+1]);
      i++; j += 2;
    }

} /* print_scan_next_array1 */

/*-------------------------------------------------------------------------*/
static void print_choices_array(dlong choices_array_top, dlong **choices_array_start,
				dlong *choices_array_live)
{
  dlong i = 0;

  fprintf(stderr,"\n");
  while (i <= choices_array_top)
    {
      fprintf(stderr,"%ld cp = %ld; nr = %ld\n",
	     i,(dlong)choices_array_start[i],choices_array_live[i]);
      i++;
   }

} /* print_choices_array */

/*-------------------------------------------------------------------------*/
static void build_choices_array(struct machine *machp, CP_P begin_collected_cps,
				dlong *choices_array_topp,
				dlong ***choices_array_startp,
                                dlong **choices_array_livep,
                                dlong **scan_next_arrayp)
{
  dlong nr_choices = 1;
  CP_P b;
  dlong i;
  dlong **choices_array_start;
  dlong *choices_array_live;


  b = (CP_P)(machp->B);
  while (b != begin_collected_cps)
    {
      nr_choices++;
      b = (CP_P)(b->b);
    }

  nr_choices++; // because we want to add the current H
  alloc_choices_array(nr_choices,choices_array_startp,choices_array_livep,scan_next_arrayp);
  *(choices_array_topp) = i = nr_choices-1;

  choices_array_start = *choices_array_startp;
  choices_array_live = *choices_array_livep;

  choices_array_start[i] = machp->H;
  choices_array_live[i] = 0;
  b = (CP_P)(machp->B);
  while (i-- > 0)
    {
      choices_array_start[i] = b->h;
      choices_array_live[i] = 0;
      b = (CP_P)(b->b);
    }

} /* build_choices_array */

/*-------------------------------------------------------------------------*/
static INLINE dlong which_segment(dlong *p, dlong choices_array_top, dlong **choices_array_start)
{
  dlong **a = choices_array_start;
  dlong len = choices_array_top;
  dlong **mid;

  whichcalleddirectplusplus;

  mid = a + (len >> 1);

  while (1)
    {
      if (*mid <= p)
	{
	  a = mid;
	  len = (len+1)>>1;
	  if (len < 2) return(a-choices_array_start);
	  mid = a + (len >> 1);
	}
      else
	{
          len = len>>1;
          if (len < 2) return(a-choices_array_start);
          mid = a + (len >> 1);
        }
    }
  
} /* which_segment */
 
/*-------------------------------------------------------------------------*/
static INLINE void add2choices_array(dlong *p, dlong c, int *previous_segmentp,
				     dlong choices_array_top,dlong **choices_array_start,
				     dlong *choices_array_live)
{
  dlong i;

  i = *previous_segmentp;
  addcalledplusplus;
  if ((choices_array_start[i] > p) || (p >= choices_array_start[i+1]))
    { *previous_segmentp = i = which_segment(p,choices_array_top,choices_array_start); whichcalledplusplus;}
  choices_array_live[i] += c;
} /* add2choices_array */


/*-------------------------------------------------------------------------*/
static INLINE void add2choices_array1(dlong segment, dlong *choices_array_live)
{
  addcalled1plusplus;
  choices_array_live[segment] += 1;
} /* add2choices_array1 */


/*-------------------------------------------------------------------------*/
static dlong init_simple_active(dlong activeys, dlong *bitmaskp, dlong *bitnrp)
{
  *bitmaskp = activeys & 0x00ffffff;
  *bitnrp = 0;
  return(((activeys >> 24) & 0x7f)*24+1);
} /* init_simple_active */

static dlong next_simple_active(dlong chuncknr, dlong *bitnrp, dlong *bitmaskp)
{
  dlong bitnr = *bitnrp;
  dlong bitmask = *bitmaskp;

  while (bitmask != 0)
    {
      bitnr++;
      if (bitmask & 0x1)
        {
          bitmask = bitmask >> 1;
	  *bitnrp = bitnr;  *bitmaskp = bitmask;
          return(bitnr+chuncknr);
        }
      bitmask = bitmask >> 1;
    }
  return(0);
} /* next_simple_active */

static dlong init_complicated_active(dlong activeys, codep p,
				    dlong **current_activep,
				    dlong *how_many_activep,
				    dlong *bitmaskp,
				    dlong *bitnrp)
{
  dlong *q;

  q = (dlong *)(p+activeys);
  *current_activep = q+1;
  *how_many_activep = *q;
  return(init_simple_active(**current_activep,bitmaskp,bitnrp));
} /* init_complicated_active */

static dlong next_complicated_active(dlong *chuncknrp, dlong *bitnrp, dlong *bitmaskp, dlong *how_many_activep, dlong **current_activep)
{
  dlong i;

  i = next_simple_active(*chuncknrp, bitnrp,bitmaskp);
  if (i) return(i);
  (*how_many_activep)--;
  if (!(*how_many_activep)) return(0);
  ++(*current_activep);
  *chuncknrp = init_simple_active(**current_activep, bitmaskp, bitnrp);
  i = next_simple_active(*chuncknrp, bitnrp,bitmaskp);
  return(i);

} /* next_complicated_active */



#define MARKBIT 0x1
#define BITSBIT 0x2
/* the next one is only used during marking */
#define TRAILEDBIT 0x4
/* the next one is only used during copying in mark&cheney*/
#define SCAN_BITSBIT 0x4


#define get_scanbits(i) (heap_mark_array[i] & SCAN_BITSBIT)
#define set_scanbits(i) heap_mark_array[i] |= SCAN_BITSBIT

#define get_marked(i) (heap_mark_array[i] & MARKBIT)
#define get_isbits(i) (heap_mark_array[i] & BITSBIT)

#define set_trailed(i) heap_mark_array[i] |= TRAILEDBIT
#define get_trailed(i) (heap_mark_array[i] & TRAILEDBIT)
#define clear_trailed(i) heap_mark_array[i] &= ~TRAILEDBIT


#define LS_MARK_BIT           0x01
#define ENV_BIT               0x01

#define ls_marked(p)      (ls_mark_array[startls-p] == LS_MARK_BIT)
#define set_ls_marked(p)   ls_mark_array[startls-p] = LS_MARK_BIT
#define set_env_marked(p)  ls_mark_array[startls-p] = ENV_BIT
#define env_marked(p)      (ls_mark_array[startls-p] == ENV_BIT)
#define clear_ls_mark(p)   ls_mark_array[startls-p] = 0


#define collected_heap_pointer(p) ((begin_collected_heap <= p) && (p <= end_heap))
#define is_in_old_generation(p) ((begin_heap <= p) && (p < begin_collected_heap))

#define set_isbits(i) heap_mark_array[i] |= BITSBIT
#define set_markedbits(i) heap_mark_array[i] |= (MARKBIT | BITSBIT)



/* #define set_marked(i) heap_mark_array[i] |= MARKBIT */

#define set_marked(i) set_marked1(i,heap_mark_array)
static INLINE void set_marked1(dlong i, char *heap_mark_array)
{
  heap_mark_array[i] |= MARKBIT;
} /* set_marked1 */

static void clear_mark_arrays(struct machine *machp)
{
  dlong i;
  char *p;

  i = machp->begin_ls - machp->end_ls;
  p = machp->ls_mark_array;
  while (i--) *p++ = 0;

  i = machp->end_heap - machp->begin_heap;
  p = machp->heap_mark_array;
  while (i--) *p++ = 0;

} /* clear_mark_arrays */


#define clear_marks(i) heap_mark_array[i] &= ~(MARKBIT | BITSBIT)
#define clear_scanbits(i) heap_mark_array[i] &= ~SCAN_BITSBIT


/*
marking starting from a value
returns number of newly marked cells
*/

#define MAXMARKSTACK 1000

#define mark_push(q) { if (stacktop < MAXMARKSTACK) markstack[stacktop++] = q; \
    else count += mark_from_value(q); }
#define mark_pop() { if (stacktop > 0) { p = markstack[--stacktop]; goto tail_recursion; } \
                    else return(count); }

#define mark_from_value(p) \
  mark_from_value1(p,begin_heap,begin_collected_heap,end_heap,heap_mark_array,previous_segmentp,choices_array_top,choices_array_start,choices_array_live)
static dlong mark_from_value1(dlong p, 
			      dlong *begin_heap, dlong *begin_collected_heap, dlong *end_heap, 
			      char *heap_mark_array, int *previous_segmentp,
			      dlong choices_array_top,dlong **choices_array_start, dlong *choices_array_live)
{
  dlong markstack[MAXMARKSTACK];
  dlong count = 0;
  dlong i;
  dlong *q;
  dlong stacktop = 0;

 tail_recursion:

  switch (tag(p))
    {
    case SIMPLE:
      mark_pop();

    case NUMB:
      q = get_number_pointer(p);
      if (! collected_heap_pointer(q))
	mark_pop();
      if (REAL_HEADER == *q)
	{
	  i = q - begin_heap;
	  if (get_marked(i))
	    mark_pop();
	  add2choices_array(q,3,previous_segmentp,choices_array_top,
			    choices_array_start,choices_array_live);
	  set_markedbits(i);
	  set_markedbits(i+1);
	  set_markedbits(i+2);
	  count += 3;
	  mark_pop();
	}
      
      /* better be a BIGINT_HEADER now */
      if (BIGINT_HEADER != *q)
	fatal_message("Expected BIGINT_HEADER - got something else");
      i = q - begin_heap;
      if (get_marked(i+1))
	mark_pop();
      /* do not mark the header */
      set_markedbits(i+1);
      q++; i+= 2;
      {
	dlong siz;
	siz = bigint_size(*q);
	add2choices_array(q,siz+2,previous_segmentp,choices_array_top,
			  choices_array_start,choices_array_live);
	count += siz + 2;
	/* we mark one cell less than the bigint so that forward block can
	   stop when searching backward */
	if (bigint_onheap(*q))
	  {
	    while (--siz)
	      { set_markedbits(i); i++; }
	  }
      }
      mark_pop();

    case LIST:
      q = get_list_pointer(p);
      if (! collected_heap_pointer(q))
	mark_pop();
      i = q - begin_heap;
      if (!get_marked(i))
	{
	  add2choices_array(q,1,previous_segmentp,choices_array_top,
			    choices_array_start,choices_array_live);
	  if (has_atom_tag(*q))
	    {
	      set_markedbits(i);
	      count++;
	    }
	  else
	    {
	      set_marked(i);
	      count++;
	      mark_push(*q);
	    }
	}

      i++; q++;
      if (!get_marked(i))
	{
	  add2choices_array(q,1,previous_segmentp,choices_array_top,
			    choices_array_start,choices_array_live);
	  if (has_atom_tag(*q))
            {
              set_markedbits(i);
	      count++;
	    }
	  else
	    {
	      set_marked(i);
	      count++;
	      p = *q;
	      goto tail_recursion;
	    }
	}
      mark_pop();

    case STRUCT:
      q = get_struct_pointer(p);
      if (! collected_heap_pointer(q))
	mark_pop();
      {
	dlong arity;
	dlong segment;
	i = q - begin_heap;
	if (get_marked(i))
	  mark_pop();
	segment = which_segment(q,choices_array_top,choices_array_start);
	add2choices_array1(segment,choices_array_live);
	set_markedbits(i);
	count++;
	arity = get_funct_arity(*q);
	while (arity--)
	  {
	    i++; q++;
	    if (!get_marked(i))
	      {
		add2choices_array1(segment,choices_array_live);
		count++;
		if (has_atom_tag(*q))
		  set_markedbits(i);
		else
		  {
		    set_marked(i);
		    mark_push(*q);
		  }
	      }
	  }
	mark_pop();
      }

    case ATT:
      q = get_attvar_pointer(p);
      if (! collected_heap_pointer(q))
        mark_pop();
      i = q - begin_heap;
      if (!get_marked(i))
	{
	  add2choices_array(q,1,previous_segmentp,choices_array_top,
			    choices_array_start,choices_array_live);
	  set_marked(i);
	  count++;
	}
      p = (dlong)(q+1);
      goto tail_recursion;

    case STRING:
      q = get_string_pointer(p);
      if (! collected_heap_pointer(q))
        mark_pop();
      i = q - begin_heap;
      if (get_marked(i))
	mark_pop();
      if (!is_string_header(*q))
	fatal_message("Expected STRING header - got something else");
      {
	dlong len;
	len = heapcells_needed_for_string(get_len_from_string_header(*q));
	add2choices_array(q,len+1,previous_segmentp,choices_array_top,
			  choices_array_start,choices_array_live);
	count += len+1;
	i = q - begin_heap;
	while (len--)
	  { set_markedbits(i); i++; }
	set_markedbits(i);
      }
      mark_pop();

    case REF0:
    case REF1:
      q = (dlong *)p;
      if (! collected_heap_pointer(q))
	mark_pop();
      i = q - begin_heap;
      if (get_marked(i))
	mark_pop();
      add2choices_array(q,1,previous_segmentp,choices_array_top,
			choices_array_start,choices_array_live);
      set_marked(i);
      count++;
      q = (dlong *)*q;
      if ((dlong *)p == q)
	{
	  mark_pop();
	}
      if (has_atom_tag(q))  /* not strickly necessary, but often an optimization */
	{
	  set_markedbits(i);
          mark_pop();
	}
      p = (dlong)q;
      goto tail_recursion;
    }

  fatal_message("should not come here during marking");
  return(0);
} /* mark_from_value1 */


#define mark_from_region(b,e) \
  mark_from_region1(b,e,begin_heap,begin_collected_heap,end_heap,heap_mark_array,previous_segmentp,choices_array_top,choices_array_start,choices_array_live)
static dlong mark_from_region1(dlong *b, dlong *e, 
			       dlong *begin_heap, dlong *begin_collected_heap, dlong *end_heap, 
			       char *heap_mark_array, int *previous_segmentp,
			       dlong choices_array_top, dlong **choices_array_start, dlong *choices_array_live)
{
  dlong count = 0;

  while (b <= e)
    count += mark_from_value(*b++);
  return(count);
} /* mark_from_region1 */


#define treat_trail_double_entry(locp,val,tr) \
  treat_trail_double_entry1(locp,val,tr,begin_heap,begin_collected_heap,end_heap,heap_mark_array,bh,early_reset,previous_segmentp,choices_array_top,choices_array_start,choices_array_live)

static INLINE dlong treat_trail_double_entry1(dlong *locp, dlong val, dlong *tr,
					      dlong * begin_heap, dlong *begin_collected_heap, dlong *end_heap,
					      char *heap_mark_array, dlong *bh,
					      dlong early_reset, int *previous_segmentp,
					      dlong choices_array_top,
					      dlong **choices_array_start,dlong *choices_array_live)
{
  if (collected_heap_pointer(locp))
    {
      dlong i = locp-begin_heap;
      clear_trailed(i);
      if (! get_marked(i))
	{
	  if (early_reset)
	    {
	      /* early reset for backtrackable setarg or undef */
	      *locp = val;
	      *tr = *(tr+1) = 0;
	      return(0);
	    }

	  /* mark both the locp and the val */
	  return(mark_from_value(val) + mark_from_value((dlong)locp));
	}

      if (locp >= bh)  /* this one could have been tydied on cut */
	{
	  *tr = *(tr+1) = 0;
	  return(0);
	}
      
      if (locp != (dlong *)val)
	{
	  /* we have to make sure that the val is marked */
	  return(mark_from_value(val));
	}

      return(0);
    }

  if (is_in_old_generation(locp))
    {
      dlong count;
      /* we have to make sure that the val is marked */
      count = mark_from_value(val);
      count += mark_from_value(*locp);
      return(count);
    }

  /* this is a trailed (non-)backtrackable global variable */
  /* its current value has been marked already, but its future (old)
     value might also be reachable */

  return(mark_from_value(val));

  return(0);

} /* treat_trail_double_entry1 */


#define mark_trail_segment(trend,tr,bh) \
  mark_trail_segment1(trend,tr,begin_collected_heap,begin_heap,end_heap,bh,heap_mark_array,early_reset,previous_segmentp,choices_array_top,choices_array_start,choices_array_live)

static INLINE dlong mark_trail_segment1(dlong *trend, dlong *tr, dlong *begin_collected_heap,
					dlong *begin_heap, dlong *end_heap, dlong *bh,
					char *heap_mark_array, dlong early_reset,
					int *previous_segmentp, dlong choices_array_top,
					dlong **choices_array_start, dlong *choices_array_live)
{
  dlong *trtemp = trend;
  dlong *locp;
  dlong val;
  dlong i;
  dlong count = 0;

  trend++;
  while (tr > trend)
    {
      // this part is for detecting multiple trailings within same trail segment
      // only the oldest is relevant
      // should test for is_in_heap rather than collected_heap_pointer
      // but it is an optimization, no necessity
      locp = (dlong *)*(trend);
      if (collected_heap_pointer(locp))
	{
	  i = locp - begin_heap;
	  if (get_trailed(i))
	    *trend = *(trend-1) = 0;
	  else
	    set_trailed(i);
	}
      else if ((begin_heap <= locp) && (locp < begin_collected_heap))
	{
	  // it points into the old generation
	  i = locp - begin_heap;
	  set_trailed(i);
	}
      /* else it is a backtrackable globvar - we don't care about them now */
      trend += 2;
    }

  trend = trtemp;
  while (tr > trend)
    {
      locp = (dlong *)*(--tr);
      if (locp)
	{
	  i = locp - begin_heap;
	  val = *(--tr);
	  count += treat_trail_double_entry(locp,val,tr);
	}
      else
	--tr;
    }
  return(count);
} /* mark_trail_segment1 */

static dlong mark_heap(dlong arity, struct machine *machp, 
		       dlong *begin_collected_heap, CP_P begin_collected_cps,
		       dlong *begin_collected_ls, dlong *begin_collected_tr,
		       dlong early_reset, int *previous_segmentp, dlong choices_array_top,
		       dlong **choices_array_start, dlong *choices_array_live)
{
  dlong count;
  dlong *begin_heap = machp->begin_heap;
  dlong *end_heap = (machp->H)+1;
  char *heap_mark_array = machp->heap_mark_array;
  dlong i;
  dlong activeys;
  CP_P b;
  CP_P prevb;
  CP_P thisb;
  CP_P nextb;
  CP_P remembercp = (CP_P)(begin_collected_cps->b);
  dlong *e, *tr, *copy2;
  codep cp;
  dlong *startls;
  char *ls_mark_array = machp->ls_mark_array;

  startls = machp->begin_ls;

  count = 0;

  /* in case gc is called from ensure_space_for_woken_goals
     we must copy the woken goals themselves */
  {
    dlong i;
    i = get_number_of_woken_goals(machp);
    if (i > 0)
      count += mark_from_region(machp->begin_woken_goals, machp->begin_woken_goals + i-1);
  }


  /* argument registers */
  count += mark_from_region((machp->Areg)+1,(machp->Areg)+arity);

  /* global variables */
  for (i = number_of_symbols(); i--; )
    {
      count += mark_from_value((dlong)(symtab[i].glob_val));
    }

  /* we must mark the heap from the active query and all queries
     that will be executed on backtracking
     we will in the mean time do early reset
              link the choice points backwards,
     clean up the trail and link the choice point back
     this takes care of trail/ls/cp
     if anything else needs marking, do it separately
  */

  prevb = 0;
  b = (CP_P)(machp->B);
  e = machp->E;
  cp = machp->CONTP;
  tr = machp->TR;
  do
    {
      /* first the environments */
      while (e != begin_collected_ls)
        {
          activeys = *(dlong *)(cp-active_yvar_len);
          if (simple_active(activeys))
            {
	      dlong bitmask, chuncknr, bitnr;
              chuncknr = init_simple_active(activeys, &bitmask, &bitnr);
              while ((i = next_simple_active(chuncknr, &bitnr, &bitmask)))
                {
                  if (ls_marked((e+i)))
                    continue;
                  set_ls_marked((e+i));
                  count += mark_from_value(*(e+i));
                }
            }
          else
            {
	      dlong bitmask, chuncknr, bitnr;
	      dlong *current_active;
	      dlong how_many_active;

              chuncknr = init_complicated_active(activeys,cp-active_yvar_len,
				      &current_active, &how_many_active,
				      &bitmask, &bitnr);
              while ((i = next_complicated_active(&chuncknr, &bitnr, &bitmask, 
						  &how_many_active, &current_active)))
                {
                  if (ls_marked((e+i)))
                    continue;
                  set_ls_marked((e+i));
                  count += mark_from_value(*(e+i));
                }
            }
          if (env_marked(e))
            e = begin_collected_ls;
          else
            {
              set_env_marked(e);
              cp = (codep)(*(e+1));
              e = (dlong *)*e;
            }
        }

      /* now the trail from ((CP_P)b)->tr till tr */

      {
        dlong *tmptr;
        tmptr = cleantr(((CP_P)b)->tr);
        count += mark_trail_segment(tmptr,tr,((CP_P)b)->h);
        tr = tmptr;
      }

      /* now the arguments in the choice point
         the H in the choice point doesn't need to be done
      */

      if (b != begin_collected_cps)
	count += mark_from_region((dlong *)(b+1),(b->b)-1);

      /* goto next choice point and turn around the link */
      thisb = b;
      e = b->e;
      cp = b->cp;
      b = (CP_P)(b->b);
      (thisb->b) = (dlong *)prevb;
      prevb = thisb;
    }
  while (b < begin_collected_cps);

  /* still do the last trail segment - needed in case of generational collection
   but otherwise not to do ? still have to find out what happens here :-( */

  count += mark_trail_segment(cleantr(((CP_P)b)->tr),tr,((CP_P)b)->h);

  /* clean up trail and reset the choice point links */

  copy2 = tr = cleantr(((CP_P)b)->tr);
  prevb = begin_collected_cps;

  do
    {
      dlong *tr_until = cleantr(thisb->tr);
      while (tr != tr_until)
	{
	  if (*tr != 0)
	    {
	      *copy2 = *tr;
	      copy2++;
	    }
	  tr++;
	}
      thisb->tr = (dlong *)marktr(copy2);
      nextb = (CP_P)(thisb->b);
      if (thisb != begin_collected_cps) (thisb->b) = (dlong *)prevb;
      else begin_collected_cps->b = (dlong *)remembercp;
      prevb = thisb;
      thisb = nextb;
    }
  while (thisb != 0) ;
  
  /* there is one trail segment more than choice points */
  
  while (tr != machp->TR)
    {
      if (*tr != 0)
        {
          *copy2 = *tr;
          copy2++;
        }
      tr++;
    }

  machp->TR = copy2;

  *tr = 1; /* this value is only used by statistics: some trail
	      entries have been set to 0, so statistics could give
	      the wrong value for the high water mark otherwise
	   */

  return(count);
} /* mark_heap */

#define forward_block(p) \
  forward_block1(p,begin_heap,heap_mark_array,temp_heap,nrblocksp,choices_array_top,choices_array_start,scan_next_array)
static INLINE dlong *forward_block1(dlong *heapp, dlong *begin_heap, 
				    char *heap_mark_array,
				    dlong *temp_heap, dlong *nrblocksp,
				    dlong choices_array_top,
				    dlong **choices_array_start, dlong *scan_next_array)
{
  dlong i = heapp - begin_heap;
  dlong *beginp, *next_heap;
  dlong *return_p;
  dlong *lnext;
  dlong segment;

  if (! get_marked(i)) /* it has been forwarded already */
    return((dlong *)(*heapp));

  /* now find the enclosing marked block */
  /* an enclosing marked block should never contain a bigint */
  /* because in case that bigint is not on the heap, this will cause havoc */

  (*nrblocksp)++;

  beginp = heapp-1;
  while (get_marked(--i))
    beginp--;

  i++; beginp++;
  segment = which_segment(beginp,choices_array_top,choices_array_start);
  next_heap = choices_array_start[segment+1];
  lnext = (dlong *)scan_next_array[2*segment+1];
  while (beginp < heapp)
    {
      *lnext = *beginp;
      *beginp = (dlong)lnext;
      fill_translationtable; 
      if (get_isbits(i))
	set_scanbits(lnext-temp_heap);
      clear_marks(i);
      lnext++;
      beginp++;
      i++;
      while (beginp >= next_heap)
	// while instead of if, because of possible empty segments
	{
	  scan_next_array[2*segment+1] = (dlong)lnext;
	  segment++;
	  lnext = (dlong *)scan_next_array[2*segment+1];
	  next_heap = choices_array_start[segment+1];
	}
    }

  return_p = lnext;

  while (get_marked(i)) // && (*beginp != BIGINT_HEADER))
    {
      *lnext = *beginp;
      *beginp = (dlong)lnext;
      fill_translationtable;
      if (get_isbits(i))
        set_scanbits(lnext-temp_heap);
      clear_marks(i);
      i++;
      beginp++;
      lnext++;
      while ((beginp >= next_heap) && get_marked(i))
	// while instead of if, because of possible empty segments
	{
          scan_next_array[2*segment+1] = (dlong)lnext;
	  segment++;
          lnext = (dlong *)scan_next_array[2*segment+1];
	  next_heap = choices_array_start[segment+1];
	}
    }

  scan_next_array[2*segment+1] = (dlong)lnext;
  return(return_p);
} /* forward_block */

#define forward_block_bigint(p, machp) \
  forward_block_bigint1(p,begin_heap,heap_mark_array,temp_heap,machp,nrblocksp,choices_array_top,choices_array_start,scan_next_array)
static INLINE dlong *forward_block_bigint1(dlong *heapp, dlong *begin_heap, 
					   char *heap_mark_array, dlong *temp_heap,
					   struct machine* machp, dlong *nrblocksp,
					   dlong choices_array_top,
					   dlong **choices_array_start, dlong *scan_next_array)
{
  dlong i = heapp - begin_heap;
  dlong *beginp;
  dlong *return_p;
  dlong *lnext;
  dlong len, segment;

  /* can't use the test (! get_marked(i)) because the header is never
     marked, so that bigints are always copied alone
     but the next cell can be marked !
  */

  if (! get_marked((i+1))) /* it has been forwarded already */
    return((dlong *)(*heapp));

  /* p points to a bigint header - we'll immediately copy the whole
     bigint - taking into account the on_heap - no enclosing thing
     is necessary
  */

  // in principle, we must determine lnext at every step
  // but bigints are in one chunck in one segment
  // if the bigint is not on the heap, it is in the highest segment
  // because of the invariants we try to maintain

  (*nrblocksp)++;

  segment = which_segment(heapp,choices_array_top,choices_array_start);
  lnext = (dlong *)scan_next_array[2*segment+1];


  return_p = lnext;

  *lnext = *heapp; set_scanbits(lnext-temp_heap); clear_marks(i);
  *heapp = (dlong)lnext;
  i++; heapp++; lnext++; clear_marks(i);
  len = bigint_size(*heapp);
  *lnext = bigint_info_onheap(bigint_positive(*heapp),len);
  set_scanbits(lnext-temp_heap);
  lnext++;
  if (bigint_onheap(*heapp))
    {
      while (len--)
	{
	  i++; clear_marks(i);
	  *lnext = *(++heapp);
	  set_scanbits(lnext-temp_heap);
	  lnext++;
	}
      scan_next_array[2*segment+1] = (dlong)lnext;
      return(return_p);
    }


  i = heapp[1];
  heapp = (machp->excess_bigints)[i];

//   memcpy((void *)lnext, (void *)heapp, len * sizeof(dlong));
//   heapp += len;
// 
//   while (len--)
//     { set_scanbits(lnext-temp_heap); lnext++; }
  while (len--)
    { *lnext = *heapp; heapp++; set_scanbits(lnext-temp_heap); lnext++; }
  free_excess_bigint(i, machp);
  scan_next_array[2*segment+1] = (dlong)lnext;
  return(return_p);

} /* forward_block_bigint1 */

#define relocate_heap_pointer(p) p = p + dist

#define forward_from_cell_pointed_to_by(p, machp) \
  forward_from_cell_pointed_to_by1(p,begin_heap,heap_mark_array,dist,temp_heap,machp,&nrblocks,choices_array_top,choices_array_start,scan_next_array,begin_collected_heap)
static INLINE void forward_from_cell_pointed_to_by1(dlong *p, dlong *begin_heap, 
						    char *heap_mark_array, dlong dist,
						    dlong *temp_heap,
						    struct machine* machp,
						    dlong *nrblocksp,dlong choices_array_top,
						    dlong **choices_array_start, dlong *scan_next_array,
						    dlong *begin_collected_heap)
{
  dlong *heapp;
  dlong value = *p;

  switch (tag(value))
    {
    case SIMPLE:
      break;

    case REF0:
    case REF1:
      heapp = (dlong *)value;
      if (heapp < begin_collected_heap) return;
      heapp = forward_block(heapp);
      relocate_heap_pointer(heapp);
      *p = (dlong)heapp;
      break;

    case LIST:
      heapp = get_list_pointer(value);
      if (heapp < begin_collected_heap) return;
      heapp = forward_block(heapp);
      relocate_heap_pointer(heapp);
      *p = make_list(heapp);
      break;

    case STRUCT:
      heapp = get_struct_pointer(value);
      if (heapp < begin_collected_heap) return;
      heapp = forward_block(heapp);
      relocate_heap_pointer(heapp);
      *p = make_struct_p(heapp);
      break;

    case STRING:
      heapp = get_string_pointer(value);
      if (heapp < begin_collected_heap) return;
      heapp = forward_block(heapp);
      relocate_heap_pointer(heapp);
      *p = make_string_pointer(heapp);
      break;

    case ATT: /* here only during (scan != next) loop */
      heapp = get_attvar_pointer(value);
      if (heapp < begin_collected_heap) return;
      heapp = forward_block(heapp);
      relocate_heap_pointer(heapp);
      *p = make_attvar_p(heapp);
      break;

    case NUMB:
      heapp = get_number_pointer(value);
      if (heapp < begin_collected_heap) return;
      if (BIGINT_HEADER == *heapp)
	heapp = forward_block_bigint(heapp, machp);
      else
	heapp = forward_block(heapp);
      relocate_heap_pointer(heapp);
      *p = make_number_pointer(heapp);
      break;
    }

} /* forward_from_cell_pointed_to_by */

static dlong nrofgc = 0;

static dlong copy_heap(dlong *temp_heap,dlong arity,struct machine *machp,
		       dlong *begin_heap, dlong *begin_collected_heap,
		       dlong nr_live_cells, dlong dist, dlong *newh,
		       CP_P begin_collected_cps, dlong choices_array_top,
		       dlong **choices_array_start, dlong *choices_array_live,
		       dlong *scan_next_array)
{
  dlong *scan, *next;
  dlong *end_heap = (machp->H)+1;
  char *heap_mark_array = machp->heap_mark_array;
  dlong i, j, done;
  char *ls_mark_array = machp->ls_mark_array;

  dlong cptop;
  dlong nrblocks = 0;

  cptop = choices_array_top;

  next = temp_heap;

  build_scan_next_array(temp_heap,choices_array_top,scan_next_array, choices_array_live);

  /* backtrackable global variables */
  /* they come first, so as to make the chance bigger that they are
     completely in the old generation */
  {
    dlong i;
    for (i = number_of_symbols(); i--; )
      {
	forward_from_cell_pointed_to_by((dlong *)(&(symtab[i].glob_val)), machp);
      }
  }

  /* woken goals */

  {
    dlong i;
    i = get_number_of_woken_goals(machp);
    while (i)
      {
	i--;
        forward_from_cell_pointed_to_by((machp->begin_woken_goals) + i, machp);
      }
  }


  /* argument registers */
  while (arity)
    { // als arity 3 is derde gc
      forward_from_cell_pointed_to_by((machp->Areg)+arity, machp);
      arity--;
    }

  /* local stack */
  {
    dlong *startls = machp->begin_ls;
    dlong *p = machp->TOS;

    while (p <= startls)
      {
	dlong *q = (dlong *)*p;
	if (ls_marked(p))
	  {
	    if (is_ref(q) && (q <= startls) && (machp->end_ls <= q))
	      ;
	    else
	      forward_from_cell_pointed_to_by(p, machp); 
	    clear_ls_mark(p);
	  }
	else
	  // if not the env or contp then zero it
	  // but pointers with REF* tag don't need that: they can be
	  // recognized later by a range check
	  {
	    if (is_simple(q))
	      ;
	    else if (is_ref(q) && ((q > machp->H) || (q < machp->begin_heap)))
	      ;
	    else if (p < begin_collected_cps->e) *p = make_smallint(666);
	  }
	p++;
      }
	    
  }

  /* choice points */
  {
    CP_P b;
    CP_P e;
    dlong *p;
    dlong *q;

    b = (CP_P)(machp->B);
    e = begin_collected_cps;
    
    while (b != e)
      {
	cptop--;
	newh = newh - choices_array_live[cptop]; // PASOP ????
	b->h = newh;
	q = ((dlong *)b) + CP_FIX_LEN;
	b = (CP_P)(b->b);
	p = (dlong *)b;
	while (q != p)
	  {
	    forward_from_cell_pointed_to_by(q, machp);
	    q++;
	  }
      }
  }

  /* trail */

  {
    dlong *trbegin = cleantr(begin_collected_cps->tr);
    dlong *trend = machp->TR;

    while (trbegin < trend)
      {
	dlong *locp;

	// first the value
	forward_from_cell_pointed_to_by(trbegin, machp);
	trbegin++;
	// now the location
	// the location is in the old gen, the new gen, or the globvars;
	// the latter case needs no treatment here: it is done in the globvars
	// section (hopefully)
	// if in the new gen, just do the "normal" thing, otherwise treat special
	locp = (dlong *)*(trbegin);
	if ((begin_heap <= locp) && (locp < begin_collected_heap))
	  {
	    dlong i = locp - begin_heap;
	    if (get_trailed(i))
	      forward_from_cell_pointed_to_by(locp, machp); 
	    clear_trailed(i);
	  }
	else if ((begin_collected_heap <= locp) && (locp <= end_heap))
	  forward_from_cell_pointed_to_by(trbegin, machp);
	trbegin++;
      }
  }

  print_scan_next_array(choices_array_top, scan_next_array,"before");

  do
    {
      dlong jj = 0;
      done = 1;
      for (j = 0; j < choices_array_top; )
	{
	  scan = (dlong *)scan_next_array[jj];
	  next = (dlong *)scan_next_array[jj+1];
	  if (scan != next)
	    {
	      done = 0;
	      while (scan != next)
		{
		  i = scan-temp_heap;
		  if (!get_scanbits(i))
		    {
		      /* *scan is a pointer of any type:
			 list, a ref, a numb, a struct, attvar or string
			 it always points to the old heap
			 distinction must be made between: does it point to something
			 already forwarded or not ? */
		      forward_from_cell_pointed_to_by(scan, machp);
		      next = (dlong *)scan_next_array[jj+1];
		    }
		  else
		    clear_scanbits(i);
		  scan++;
		}
	      scan_next_array[jj] = (dlong)scan;
	    }
	  j++; jj += 2;
	}
    }
  while (!done);

  print_scan_next_array(choices_array_top, scan_next_array,"after");

  return(nrblocks);

   if (sanity_check)
     if ((next-temp_heap) != nr_live_cells)
       fprintf(stderr,"marked: %ld; collected: %ld\n",
 	      nr_live_cells,(next-temp_heap));

} /* copy_heap */

static dlong decide_on_expansion(dlong nr_live_cells, struct machine *machp,
				 dlong minimal, dlong really_generational,
				 dlong *begin_collected_heap)
{
  dlong totallen, newlen;
  dlong totalneeded, incrneeded;

  newlen = machp->end_heap - begin_collected_heap;
  newlen = (newlen*3)>>2; // 75%
  totallen = machp->end_heap - machp->begin_heap;
  totallen = (totallen*3)>>2;

  totalneeded = (begin_collected_heap - machp->begin_heap) 
                + nr_live_cells + minimal;
  incrneeded = nr_live_cells + minimal;

  if ((totalneeded > totallen) ||
       (really_generational && (incrneeded > newlen)))
    {
      totallen = machp->end_heap - machp->begin_heap;
      totallen = totallen << 1;
      if (minimal >= (totallen - nr_live_cells)) totallen += minimal;
      return(totallen);
    }
  return(0);
} /* decide_on_expansion */

/*-------------------------------------------------------------------------*/
static dlong force_expansion(dlong nr_live_cells,
			    struct machine *machp, dlong minimal)
{
  dlong len;

  len = machp->end_heap - machp->begin_heap;

  len = len << 1;
  if (minimal > (len-nr_live_cells))
    len += minimal;
  return(len);

} /* force_expansion */

/*-------------------------------------------------------------------------*/

static CP_P find_generational_choicepoint(dlong *really_generational, struct machine *machp)
{
  CP_P b;
  
  if (generational_gc && ((machp->nr_minorheapgc) % 100))
    {
      b = (CP_P)(machp->B);
      while ((b < ((CP_P)(machp->begin_cp)-1)) && iscleantr(b->tr))
	b = (CP_P)(b->b);
      if ((b->h - machp->begin_heap)*100 > (machp->H - b->h))
	{ *really_generational = 1; return(b); }
    }

  *really_generational = 0;
  return((CP_P)(machp->begin_cp)-1);
} /* find_generational_choicepoint */

static void write_decent(dlong i, dlong j)
{
  if (i == 0)
    {
      printf("%ld",j);
      return;
    }

  printf("%ld",i);
  if (j <=  9999999) printf("0");
  if (j <=   999999) printf("0");
  if (j <=    99999) printf("0");
  if (j <=     9999) printf("0");
  if (j <=      999) printf("0");
  if (j <=       99) printf("0");
  if (j <=        9) printf("0");
  printf("%ld",j);
} /* write_decent */

extern void representation_sharing(struct machine *, dlong);

static void deal_with_heap_overflow_horse(dlong, struct machine *, dlong);

void deal_with_heap_overflow(dlong arity, struct machine *machp, dlong minimal)
{
  dlong size1, size2;
  deal_with_heap_overflow_horse(arity, machp, minimal);
  size1 = machp->H - machp->begin_heap;
  if (machp->intro_representation_sharing)
    {
      representation_sharing(machp, arity);
      if (machp->intro_representation_sharing > 1)
	{
	  deal_with_heap_overflow_horse(arity, machp, minimal);
	  size2 = machp->H - machp->begin_heap;
	  machp->total_sharedgain1 += (size1-size2)*sizeof(dlong *);
	  while (machp->total_sharedgain1 >= 100000000)
	    {
	      machp->total_sharedgain1 -= 100000000;
	      machp->total_sharedgain2 += 1;
	    }
	  if (verbose_mode) 
	    {
	      dlong i;
	      i = (100.0*(size1-size2))/size1;
	      printf("\nRepresentation Sharing gained %ld Cells (%ld %%) \n",(size1-size2),i);
	    }
	}
    }
} /* deal_with_heap_overflow */


static void deal_with_heap_overflow_horse(dlong arity, struct machine *machp, dlong minimal)
{
  char *mess;
  dlong *newh;
  dlong nr_live_cells;
  dlong t1, t2, t3, dist, expansion_needed;
  dlong *temp_heap;
  dlong early_reset;
  dlong *begin_collected_heap, *begin_collected_tr, *begin_collected_ls;
  CP_P begin_collected_cps;
  dlong heapsize;
  dlong forward_block;
  dlong nrblocks;
  int previous_segment;
  dlong choices_array_top;
  dlong **choices_array_start;
  dlong *choices_array_live;
  dlong *scan_next_array;
  dlong really_generational;

  minimal += machp->total_size_excess_bigints;

  t1 = get_cpu_time();

  begin_collected_cps = find_generational_choicepoint(&really_generational,machp);
  begin_collected_tr = (dlong *)cleantr(begin_collected_cps->tr);
  begin_collected_heap = begin_collected_cps->h;
  begin_collected_ls = machp->begin_ls;
  if (really_generational) early_reset = 0;
  else early_reset = 1;

  heapsize = machp->end_heap-machp->begin_heap;

  build_choices_array(machp,begin_collected_cps,&choices_array_top,
		      &choices_array_start,&choices_array_live,&scan_next_array);

  if (machp->heap_mark_array == 0)
    {
      machp->heap_mark_array = calloc((machp->end_heap - machp->begin_heap + 3),1);
      if (machp->heap_mark_array == 0) 
	fatal_message(mem2low);
      machp->heap_mark_array += 1;
    }

  if (machp->ls_mark_array == 0)
    {
      if (!(machp->ls_mark_array = calloc(machp->begin_ls-machp->end_ls+1,1)))
	fatal_message(mem2low);
    }

  debug_print_heap(arity,machp,0,0);

  previous_segment = 0;
  nr_live_cells = mark_heap(arity,machp,
			    begin_collected_heap,begin_collected_cps,
			    begin_collected_ls,begin_collected_tr,early_reset,
			    &previous_segment,choices_array_top,
			    choices_array_start, choices_array_live);

  t2 = get_cpu_time();

  expansion_needed = decide_on_expansion(nr_live_cells,machp,minimal,
					 really_generational,begin_collected_heap);

  debug_print_heap(arity,machp,0,0);


  if (expansion_needed)
    {
      /* first restart marking of whole heap in case that there was
      generational collection */

      if (begin_collected_cps != ((CP_P)(machp->begin_cp)-1))
	{
	  clear_mark_arrays(machp);
	  begin_collected_cps = (CP_P)(machp->begin_cp)-1;
	  begin_collected_tr = cleantr(begin_collected_cps->tr);
	  begin_collected_heap = begin_collected_cps->h;
	  begin_collected_ls = machp->begin_ls; early_reset = 1;

	  heapsize = machp->end_heap-machp->begin_heap;

	  free_choices_array(choices_array_start,choices_array_live,scan_next_array);
	  build_choices_array(machp,begin_collected_cps,&choices_array_top,
			      &choices_array_start,&choices_array_live,&scan_next_array);
	  
	  previous_segment = 0;
	  nr_live_cells = mark_heap(arity,machp,
				    begin_collected_heap,begin_collected_cps,
				    begin_collected_ls,begin_collected_tr,early_reset,
				    &previous_segment,choices_array_top,
				    choices_array_start, choices_array_live);
	  
	  t2 = get_cpu_time();
	  
	  expansion_needed = force_expansion(nr_live_cells,machp,minimal);
	  really_generational = 0;
	  early_reset = 1;
	}
	  
      temp_heap = (dlong *)calloc(expansion_needed,sizeof(dlong *));
      alloc_translationtable(expansion_needed);
      newh = temp_heap+nr_live_cells;
      /* realloc mark array now: it might need to be bigger than the current
	 heap, because of bigints being sucked in: old code was (in a different place)

	 free((machp->heap_mark_array)-1);
	 machp->heap_mark_array = (char *)calloc(expansion_needed + 2,1)+1;

	 but we need the marking, so we realloc
      */
      machp->heap_mark_array -= 1;
      machp->heap_mark_array = (char *)realloc(machp->heap_mark_array,expansion_needed + 2);
      if (! machp->heap_mark_array)
	fatal_message("failed to allocate temp mark array for gc");
      machp->heap_mark_array[0] = 0;
      machp->heap_mark_array += 1;
      {
	/* the following is crazy: normally, the mark array is calloc-ed
	   the extra part really must be zeroes
	*/
	dlong i;
	for (i = heapsize; i <= expansion_needed; i++)
	  machp->heap_mark_array[i] = 0;
      }
      dist = 0;
    }
  else
    {
      temp_heap = (dlong *)calloc(nr_live_cells,sizeof(dlong *));
      alloc_translationtable(nr_live_cells);
      newh = begin_collected_heap+nr_live_cells;
      dist = begin_collected_heap-temp_heap;
    }

  if (! temp_heap)
    fatal_message("failed to allocate temp heap for gc");

  nrofgc++;
  nrblocks = copy_heap(temp_heap,arity,machp,machp->begin_heap,begin_collected_heap,
		       nr_live_cells,dist,newh,begin_collected_cps,
		       choices_array_top, choices_array_start, choices_array_live, scan_next_array);

//   build_choices_array(machp,begin_collected_cps,&choices_array_top);
//   print_choices_array();


  if (sanity_check)
    {
      char *p = (machp->heap_mark_array)-1;
      dlong i = (machp->end_heap - machp->begin_heap) + 2;
      dlong j = -1;
      while (i)
	{
 	  if (*p)
 	    { fprintf(stderr,"heap_mark_array not empty %ld\n",j); break; }
	  p++; i--; j++;
	}

      p = machp->ls_mark_array;
      i = machp->begin_ls - machp->end_ls;

      j = 0;
      while (i)
	{
	  if (*p)
	    { fprintf(stderr,"ls_mark_array not empty %ld\n",j); break; }
	  p++; i--; j++;
	}
    }

  machp->total_collected1 += ((machp->H - begin_collected_heap) - nr_live_cells)*sizeof(dlong *);
  while (machp->total_collected1 >= 100000000)
    {
      machp->total_collected1 -= 100000000;
      machp->total_collected2 += 1;
    }

  if (expansion_needed)
    {
      /* deallocate old heap and mark arry */
      free(machp->begin_heap);
      /* set machine regs related to heap correct */
      machp->begin_heap = temp_heap;
      machp->end_heap = temp_heap + expansion_needed;
      machp->limit_heap = machp->end_heap-SPARE_HEAP;
      if (machp->shadow_limit_heap)
	machp->shadow_limit_heap = machp->limit_heap;
      begin_collected_cps->h = machp->begin_heap;

      machp->nr_heapexp += 1;
      machp->H = machp->begin_heap+nr_live_cells;
    }
  else
    {
      /* 
	 dlong tmem1, tmem2;
	 tmem1 = get_cpu_time();
	 */
      memcpy(begin_collected_heap,temp_heap,sizeof(dlong *)*nr_live_cells);
      free(temp_heap);
      /* tmem2 = get_cpu_time();
	 printf("copy back and free: %ld\n",tmem2-tmem1);
	 */
      machp->H = begin_collected_heap+nr_live_cells;
    }
  machp->nr_heapgc += 1;
  if (really_generational) machp->nr_minorheapgc += 1;

  if (machp->FH)
    {
      dlong i = 0;
      dlong tot = 0;

      while ((i < choices_array_top) && (choices_array_start[i] < (machp->FH)))
	{ tot += choices_array_live[i]; i++ ;}
      if (i >= choices_array_top)
	machp->FH = machp->H;
      else
	machp->FH = machp->begin_heap + tot;
      //      machp->FH = machp->H; // added because the above might be violating some invariant I don't know about yet
    }

  machp->BH = machp->H - choices_array_live[choices_array_top-1]; // PASOP

  /* a slight hack - why ? */
  //  begin_collected_cps->h = machp->begin_heap;

  debug_print_heap(arity,machp,translationtable,begin_collected_cps->h);

  free_excess_bigints(machp);

  t3 = get_cpu_time();
  machp->total_gc_time += (t3 - t1);
  if (expansion_needed)
    mess = "(expansion)";
  else
    mess = "";

  if (verbose_mode)
    {
      char *gen;
      if (really_generational)
	gen = "minor";
      else
	gen = "major";

      printf("\nHeap %s gc %s %ld - %ld live cells - total freed ",
	     gen,mess,
	     machp->nr_heapgc,
	     nr_live_cells
	     );
      write_decent(machp->total_collected2,machp->total_collected1);
      printf(" bytes\n");
      printf("             mark time %ld msecs - gc time %ld msecs - total time %ld msecs\n",
	     t2-t1,t3-t1,machp->total_gc_time);
#ifdef STAT_GC
      printf("             Nr of blocks                %ld\n",nrblocks);
      printf("             Add called                  %ld\n",addcalled);
      printf("             Add called1                 %ld\n",addcalled1);
      printf("             Which called                %ld\n",whichcalled);
      printf("             Which called direct         %ld\n",whichcalleddirect);
      addcalled = addcalled1 = whichcalled = whichcalleddirect = 0;
#endif
    }

  machp->time_mm += (t3 - t1);
  dealloc_translationtable;
  free_choices_array(choices_array_start,choices_array_live,scan_next_array);
} /* deal_with_heap_overflow_horse */



static void deal_with_tr_overflow_by_expansion(struct machine *machp)
{
  dlong *old_end_tr = machp->end_trail;
  dlong *old_begin_tr = machp->begin_trail;
  dlong tr_size,dist;
  dlong *new_begin_tr;

  tr_size = old_end_tr-old_begin_tr+1;  // why
  new_begin_tr = (dlong *)realloc(old_begin_tr,2*tr_size*sizeof(dlong *));
  if (! new_begin_tr)
    fatal_message("trail expansion failed");

  if (new_begin_tr != old_begin_tr)
    {
      /* the tr pointers in the choice points need to be relocated */
      dlong *b = machp->B;
      dlong **ptr;

      dist = new_begin_tr-old_begin_tr;
      while (b != machp->begin_cp)
	{
	  ptr = &(((CP_P)b)->tr);
	  *ptr = (*ptr) + dist;
	  b = ((CP_P)b)->b;
	}

      machp->begin_trail = new_begin_tr;
      machp->TR = machp->TR + dist;
    }

  machp->end_trail = machp->begin_trail + 2*tr_size - 1; // why
  machp->limit_trail = machp->end_trail - SPARE_TR;
  machp->nr_trexp += 1;


} /* deal_with_tr_overflow_by_expansion */

void deal_with_tr_overflow(struct machine *machp)
{
  dlong *old_end_tr = machp->end_trail;
  dlong *old_begin_tr = machp->begin_trail;
  dlong tr_size,dist;
  dlong *new_begin_tr;
  dlong tbegin, tend;
  float percentage;

  tbegin = get_cpu_time();
  tr_size = old_end_tr-old_begin_tr+1; // why

  tbegin = get_cpu_time();
  deal_with_tr_overflow_by_expansion(machp);
  tend = get_cpu_time();
  machp->time_mm += tend-tbegin;
  if (verbose_mode)
    printf("\nTrail stack expansion - new size = %ld - time %ld msecs\n",2*tr_size,tend-tbegin);


} /* deal_with_tr_overflow */

dlong * deal_with_ls_overflow(struct machine *machp)
{
  dlong *old_end_ls = machp->end_ls;
  dlong *old_begin_ls = machp->begin_ls;
  dlong ls_size,dist;
  dlong *new_end_ls;
  dlong tbegin, tend;


  //  print_ls(machp);
  tbegin = get_cpu_time();
  ls_size = old_begin_ls-old_end_ls+1; // why
  free(machp->ls_mark_array);
  new_end_ls = (dlong *)realloc(old_end_ls,2*ls_size*sizeof(dlong *));
  if (! new_end_ls)
    fatal_message("local stack expansion failed");

  machp->ls_mark_array = calloc(2*ls_size,1);
  if (! machp->ls_mark_array)
    fatal_message("local stack expansion failed");

  memcpy(new_end_ls+ls_size,new_end_ls,ls_size*sizeof(dlong *));

  /* now the local stack is at the right spot, but not relocated yet */

  dist = (new_end_ls+ls_size)-old_end_ls; /* this is the dist for relocating */

  /* we need to relocate the env pointers in the envs and the choice points */

  machp->TOS = machp->TOS + dist;
  machp->E = machp->E + dist;
  machp->end_ls = new_end_ls;
  machp->begin_ls = new_end_ls + 2*ls_size - 1; // why
  machp->limit_ls = machp->end_ls + SPARE_LS;

  {
    /* first the local stack - just scan it */
    dlong *p = machp->TOS;
    dlong i = machp->begin_ls - p;
    while (i--)
      {
	if ((old_end_ls <= (dlong *)(*p)) && ((dlong *)(*p) <= old_begin_ls))
	  *p = (dlong)((dlong *)(*p) + dist);
	p++;
      }
  }

  {
    /* now the choice points a bit more discriminating ... */
    dlong *b = machp->B;
    dlong **pe;

    while (b != machp->begin_cp)
    {
      pe = &(((CP_P)b)->e);
      *pe = (*pe) + dist;
      pe = &(((CP_P)b)->tops);
      *pe = (*pe) + dist;
      b = ((CP_P)b)->b;
    }
  }

  tend = get_cpu_time();
  machp->time_mm += tend-tbegin;
  machp->nr_lsexp += 1;
  if (verbose_mode)
    printf("\nLocal stack expansion - new size = %ld - time %ld msecs\n",2*ls_size,tend-tbegin);

  //  print_ls(machp);
  return(machp->TOS);
} /* deal_with_ls_overflow */



void deal_with_cp_overflow(struct machine *machp)
{
  dlong *old_end_cp = machp->end_cp;
  dlong *old_begin_cp = machp->begin_cp;
  dlong cp_size,dist;
  dlong *new_end_cp;
  dlong *newB;
  dlong *prevb;
  dlong tbegin, tend;

  tbegin = get_cpu_time();
  cp_size = old_begin_cp-old_end_cp+1;  // why
  new_end_cp = (dlong *)realloc(old_end_cp,2*cp_size*sizeof(dlong *));
  if (! new_end_cp)
    fatal_message("choice point expansion failed");

  memcpy(new_end_cp+cp_size,new_end_cp,cp_size*sizeof(dlong *));

  /* now the choicepoint stack is at the right spot, but not relocated yet */

  dist = (new_end_cp+cp_size)-old_end_cp; /* this is the dist for relocating */
  machp->B = newB = machp->B + dist;
  machp->end_cp = new_end_cp;
  machp->begin_cp = new_end_cp + 2*cp_size - 1; // why
  machp->limit_cp = machp->end_cp + SPARE_CP;

  while (newB != machp->begin_cp)
    {
      prevb = (((CP_P)newB)->b);
      prevb += dist;
      ((CP_P)newB)->b = prevb;
      newB = prevb;
    }


  tend = get_cpu_time();
  machp->time_mm += tend-tbegin;
  machp->nr_cpexp += 1;
  if (verbose_mode)
    printf("\nChoice point stack expansion - new size = %ld - time %ld msecs\n",2*cp_size,tend-tbegin);

} /* deal_with_cp_overflow */


dlong * collect_active_yvars(dlong *tmpE, codep tmppc, dlong *tmph)
{
  dlong activeys;
  dlong i;

  activeys = *(dlong *)tmppc;

  if (simple_active(activeys))
    {
      dlong bitmask, chuncknr, bitnr;
      chuncknr = init_simple_active(activeys, &bitmask, &bitnr);
      while ((i = next_simple_active(chuncknr, &bitnr, &bitmask)))
	{
	  *tmph = make_list(tmph+1); tmph++;
	  *tmph = *(tmpE+i); tmph++;
	}
      return(tmph);
    }
    
  {
    dlong bitmask, chuncknr, bitnr;
    dlong *current_active;
    dlong how_many_active;
    
    chuncknr = init_complicated_active(activeys,tmppc,
				       &current_active, &how_many_active,
				       &bitmask, &bitnr);
    while ((i = next_complicated_active(&chuncknr, &bitnr, &bitmask, 
					&how_many_active, &current_active)))
      {
	*tmph = make_list(tmph+1); tmph++;
	*tmph = *(tmpE+i); tmph++;
      }
    return(tmph);
  }
} /* collect_active_yvars */


void install_active_yvars(dlong *tmpE, codep tmppc, dlong *avlp, dlong cutpoint)
{
  dlong activeys;
  dlong i, contents;

  activeys = *(dlong *)tmppc;

  if (simple_active(activeys))
    {
      dlong bitmask, chuncknr, bitnr;
      chuncknr = init_simple_active(activeys, &bitmask, &bitnr);
      while ((i = next_simple_active(chuncknr, &bitnr, &bitmask)))
	{
	  avlp = get_list_pointer(avlp);
	  contents = *avlp;
	  if (is_cut_point(contents))
	    *(tmpE+i) = cutpoint;
	  else
	    *(tmpE+i) = *avlp;
	  avlp++;
	  deref(avlp);
	}
      return;
    }
    
  {
    dlong bitmask, chuncknr, bitnr;
    dlong *current_active;
    dlong how_many_active;
    
    chuncknr = init_complicated_active(activeys,tmppc,
				       &current_active, &how_many_active,
				       &bitmask, &bitnr);
    while ((i = next_complicated_active(&chuncknr, &bitnr, &bitmask, 
					&how_many_active, &current_active)))
      {
	avlp = get_list_pointer(avlp);
	contents = *avlp;
	if (is_cut_point(contents))
	  *(tmpE+i) = cutpoint;
	else
	  *(tmpE+i) = *avlp;
	avlp++;
	deref(avlp);
      }
    return;
  }
} /* install_active_yvars */
