\chapter{Constraint Handling Rules} \label{chr}
%=============================================

\section{Introduction}
%=====================

Constraint Handling Rules (CHR) is a committed-choice bottom-up language
embedded in hProlog. It is designed for writing constraint solvers and is
particularily useful for providing application-specific constraints.
It has been used in many kinds of applications, like scheduling,
model checking, abduction, type checking among many others.

CHR has previously been implemented in other Prolog systems (SICStus,
Eclipse, Yap), Haskell and Java. The hProlog CHR system is based on
the compilation scheme and runtime environment of CHR in SICStus.

In this documentation we restrict ourselves to giving a short overview of CHR
in general and mainly focus on hProlog-specific elements.  For a more thorough
review of CHR we refer the reader to \cite{chr_survey}. More background on
CHR can be found at \cite{chr_site}.

In Section \ref{SyntaxAndSemantics} we present the syntax of CHR in hProlog and
explain informally its operational semantics. Next, Section \ref{practical}
deals with practical issues of writing and compiling hProlog programs containing
CHR. Section \ref{predicates} provides a few useful predicates to inspect
the constraint store and Section \ref{examples} illustrates CHR with two
example programs. Finally, Section \ref{guidelines} concludes with a few
practical guidelines for using CHR.


\section{Syntax and Semantics} \label{SyntaxAndSemantics}
%=============================

\subsection{Syntax}
%-----------------

The syntax of CHR rules in hProlog is the following:

\begin{verbatim}
rules --> rule, rules.
rules --> [].

rule --> name, actual_rule, pragma, [atom('.')].

name --> atom, [atom('@')].
name --> [].

actual_rule --> simplification_rule.
actual_rule --> propagation_rule.
actual_rule --> simpagation_rule.

simplification_rule --> constraints, [atom('<=>')], guard, body.
propagation_rule --> constraints, [atom('==>')], guard, body.
simpagation_rule --> constraints, [atom('\')], constraints, [atom('<=>')], 
                     guard, body.

constraints --> constraint, constraint_id.
constraints --> constraint, [atom(',')], constraints.

constraint --> compound_term.

constraint_id --> [].
constraint_id --> [atom('#')], variable.

guard --> [].
guard --> goal, [atom('|')].

body --> goal.

pragma --> [].
pragma --> [atom('pragma')], actual_pragmas.

actual_pragmas --> actual_pragma.
actual_pragmas --> actual_pragma, [atom(',')], actual_pragmas.

actual_pragma --> [atom('passive(')], variable, [atom(')')].

\end{verbatim}

Additional syntax-related terminology:

\begin{itemize}
\item \textbf{head:} the constraints in an \texttt{actual\_rule} before
                     the arrow (either \texttt{<=>} or \texttt{==>})
\end{itemize}

\subsection{Semantics}
%--------------------

In this subsection the operational semantics of CHR in hProlog are presented
informally. They do not differ essentially from other CHR systems.

When a constraint is called, it is considered an active constraint and
the system will try to apply the rules to it. Rules are tried and executed
sequentially in the order they are written. 

A rule is conceptually tried for an active constraint in the following
way. The active constraint is matched with a constraint in the head of the
rule. If more constraints appear in the head they are looked for among the
suspended constraints, which are called passive constraints in this context. If
the necessary passive constraints can be found and all match with the head of
the rule and the gaurd of the rule succeeds, then the rule is committed and
the body of the rule executed.  If not all the necessary passive constraint
can be found, the matching fails or the guard fails, then the body is not
executed and the process of trying and executing simply continues with 
the following rules. If for
a rule, there are multiple constraints in the head, the active constraint
will try the rule sequentially multiple times, each time trying to match with
another constraint.

This process ends either when the active constraint disappears, i.e. it is
removed by some rule, or after the last rule has been processed. In the latter
case the active constraint becomes suspended.

A suspended constraint is eligible as a passive constraint for an active
constraint. The other way it may interact again with the rules, is when
a variable appearing in the constraint becomes bound to either a nonvariable
or another variable involved in one or more constraints. In that case the
constraint is triggered, i.e. it becomes an active constraint and all the rules
are tried.

\paragraph{Rule Types}
%- - - - - - - - - - 
There are three different kinds of rules, each with their specific semantics:
\begin{itemize}
\item \texttt{simplification:}

The simplification rule removes the constraints in its head and calls its body.

\item \texttt{propagation:}

The propagation rule calls its body exactly once for the constraints in its head.

\item \texttt{simpagation:}

The simpagation rule removes the constraints in its head after the $\backslash$ and then calls its body.
It is an optimization of simplification rules of the form:
\[constraints_1, constraints_2 <=> constraints_1, body \]
Namely, in the simpagation form:
\[ constraints_1 \backslash constraints_2 <=> body \]
The $constraints_1$ constraints are not called in the body.

\end{itemize}

\paragraph{Rule Names}
%- - - - - - - - - - 
Naming a rule is optional and has no semantical meaning. It only functions
as documentation for the programmer.

\paragraph{Pragmas}
%- - - - - - - - -
The semantics of the pragmas are:
\begin{itemize}
\item \textbf{passive/1:} the constraint in the head of a rule with the identifier specified by
                          the \textbf{passive/1} pragma can only act as a passive constraint in that rule.
\end{itemize}
Additional pragmas may be released in the future.

\section{CHR in hProlog Programs} \label{practical}
%===========================

\subsection{Embedding in XSB Programs}

The CHR constraints defined in a particulary \texttt{.chr} file are associated
with the hProlog module. The default module is \texttt{user}. 
One should never load different \texttt{.chr} files with the same CHR module name.

\subsection{Compilation}

A \texttt{.chr} file containing CHR rules should be comiled and loaded like any other \texttt{.pl} file.

\paragraph{Note}

When compiling a \texttt{.chr} file, it is preprocessed and a \texttt{.pl}
file is generated. This \texttt{.pl} file should not be modified. All changes
should be applied to the \texttt{.chr} file.

\section{Useful Predicates} \label{predicates}
%=========================

The \texttt{chr} module contains several useful predicates that allow
inspecting and printing the content of the constraint store.

\begin{description}
\ournewitem{show\_store(+Mod)}{chr}\index{\texttt{show\_store/1}}
  Prints all suspended constraints of module \texttt{Mod} to
  the standard output.
\ournewitem{suspended\_chr\_constraints(+Mod,-List)}{chr}\index{\texttt{suspended\_constraints/2}}
  Returns the list of all suspended CHR constraints of the given module.
\end{description}

\section{Examples} \label{examples}
%================

Here are two example constraint solvers written in CHR.

\begin{itemize}

\item
The program below defines a solver with one constraint, 
\texttt{leq/2}, which is a less-than-or-equal constraint.

\begin{verbatim}
:- module(leq,[cycle/3, leq/2]).

:- constraints leq/2.
reflexivity  @ leq(X,X) <=> true.
antisymmetry @ leq(X,Y), leq(Y,X) <=> X = Y.
idempotence  @ leq(X,Y) \ leq(X,Y) <=> true.
transitivity @ leq(X,Y), leq(Y,Z) ==> leq(X,Z).

cycle(X,Y,Z):-
        leq(X,Y),
        leq(Y,Z),
        leq(Z,X).
\end{verbatim}

\item
The program below implements a simple finite domain
constraint solver.

\begin{verbatim}
:- module(dom,[dom/2]).

:- constraints dom/2. 

dom(X,[]) <=> fail.
dom(X,[Y]) <=> X = Y.
dom(X,L1), dom(X,L2) <=> intersection(L1,L2,L3), dom(X,L3).

intersection([],_,[]).
intersection([H|T],L2,[H|L3]) :-
        member(H,L2), !,
        intersection(T,L2,L3).
intersection([_|T],L2,L3) :-
        intersection(T,L2,L3).
\end{verbatim}
		
\end{itemize}

\section{Guidelines} \label{guidelines}
%==================

In this section we cover several guidelines on how to use CHR to write constraint solvers
and how to do so efficiently.
\begin{itemize}
\item \textbf{Set semantics:}
      The CHR system allows the presence of identical constraints, i.e. multiple constraints
      with the same functor, arity and arguments. For most constraint solvers, this is not
      desirable: it affects efficiency and possibly termination. Hence appropriate simpagation
      rules should be added of the form:
      \[ constraint \backslash constraint <=> true \]
\item \textbf{Multi-headed rules:}
      Multi-headed rules are executed more efficiently when the constraints share one or more variables.
\end{itemize}
