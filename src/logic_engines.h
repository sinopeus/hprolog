/*
  Original written by Timon Van Overveldt Febr-May 2011
  Changes by Bart Demoen
*/
  

#ifdef MULTI_THREADED

// --  Global engine-related macros and variables
#define ENGINE_CANCEL_SIGNAL SIGUSR1
extern dlong MAX_ENGINES;
extern dlong MAX_HUBS;

extern codep engine_toplevel_goalp;

#define DEFAULT_MSG_CHUNK_SIZE 4000

// --  Thread related
#define make_engine_id(id) (id << 1)
#define get_engine_index(id) (id >> 1)
#define is_engine_id(id) (!(id & 1))
#define make_hub_id(id) (id << 1 | 1)
#define get_hub_index(id) (id >> 1)
#define is_hub_id(id) (id & 1)

// --  Global initialization & cleanup
struct engine* init_engines_hubs();
void engines_cleanup();

// -- Hub creation & termination
dlong builtin_new_hub();
int builtin_stop_hub(dlong hub_id, struct machine *locmachp, struct engine* thisengine);

// --  Engine creation & termination
dlong builtin_spawn(dlong* goal, dlong parent_id, short link, struct engine* thisengine);
dlong builtin_engine_start_goal_size(struct engine* thisengine);
int builtin_engine_start_goal(dlong* term, struct engine* thisengine);
int builtin_stop_engine(dlong engine_id, short purge, struct engine* thisengine);

// --  Hub messaging
int builtin_hub_send(dlong to_hub_id, dlong* term, struct engine* thisengine);
struct message* builtin_hub_next_msg(dlong hub_id, dlong from_engine_id, struct engine* thisengine);

// --  Engine messaging
int builtin_engine_send(dlong to_engine_id, dlong* term, struct engine *thisengine);
struct message* builtin_engine_next_msg(dlong from_engine_id, struct engine *thisengine);
int builtin_receive(dlong* term, struct message* msg, struct engine* thisengine);

#else
extern dlong MAX_ENGINES;
extern dlong MAX_HUBS;
#define ENGINE_CANCEL_SIGNAL SIGUSR1
struct engine* init_engines_hubs();


#endif
