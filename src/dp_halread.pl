:- sysmodule(native_hal_read,[]).

%---------------------------------------------------------------------------%
:- export pred alt_hal_read_dcg(hal_read_term,state,state).
:-        mode alt_hal_read_dcg(out,di,uo) is det.
%---------------------------------------------------------------------------%

native_hal_read:alt_hal_read_dcg(Out) -->
	native_hal_read:hal_read_term(Term),
	{ native_hal_read:hal_term_dcg_transfo(Term,Out) }.

%%%%%%%%%%%%%% parts from list.m

%---------------------------------------------------------------------------%
:- pred native_hal_read:my_app(list(T),list(T),list(T)).
:- mode native_hal_read:my_app(in,in,out) is det.
%---------------------------------------------------------------------------%

native_hal_read:my_app([],L,L).
native_hal_read:my_app([X|R],S,[X|T]) :- native_hal_read:my_app(R,S,T).

%---------------------------------------------------------------------------%
:- pred native_hal_read:my_len(list(T)::in,int::out) is det.
%---------------------------------------------------------------------------%

native_hal_read:my_len(L,N) :- native_hal_read:my_len(L,0,N).

%---------------------------------------------------------------------------%
:- pred native_hal_read:my_len(list(T)::in,int::in,int::out) is det.
%---------------------------------------------------------------------------%

native_hal_read:my_len([],I,I).
native_hal_read:my_len([_|R],I,O) :- II is I + 1, native_hal_read:my_len(R,II,O).


%%%%%%%%%%%%%% parts from char.m

%---------------------------------------------------------------------------%
:- pred native_hal_read:is_upper(int::in) is semidet.
%---------------------------------------------------------------------------%
% could be implemented more efficiently

native_hal_read:is_upper(0'A).
native_hal_read:is_upper(0'B).
native_hal_read:is_upper(0'C).
native_hal_read:is_upper(0'D).
native_hal_read:is_upper(0'E).
native_hal_read:is_upper(0'F).
native_hal_read:is_upper(0'G).
native_hal_read:is_upper(0'H).
native_hal_read:is_upper(0'I).
native_hal_read:is_upper(0'J).
native_hal_read:is_upper(0'K).
native_hal_read:is_upper(0'L).
native_hal_read:is_upper(0'M).
native_hal_read:is_upper(0'N).
native_hal_read:is_upper(0'O).
native_hal_read:is_upper(0'P).
native_hal_read:is_upper(0'Q).
native_hal_read:is_upper(0'R).
native_hal_read:is_upper(0'S).
native_hal_read:is_upper(0'T).
native_hal_read:is_upper(0'U).
native_hal_read:is_upper(0'V).
native_hal_read:is_upper(0'W).
native_hal_read:is_upper(0'X).
native_hal_read:is_upper(0'Y).
native_hal_read:is_upper(0'Z).

%---------------------------------------------------------------------------%
:- pred native_hal_read:is_lower(int::in) is semidet.
%---------------------------------------------------------------------------%
% could be implemented more efficiently

native_hal_read:is_lower(0'a).
native_hal_read:is_lower(0'b).
native_hal_read:is_lower(0'c).
native_hal_read:is_lower(0'd).
native_hal_read:is_lower(0'e).
native_hal_read:is_lower(0'f).
native_hal_read:is_lower(0'g).
native_hal_read:is_lower(0'h).
native_hal_read:is_lower(0'i).
native_hal_read:is_lower(0'j).
native_hal_read:is_lower(0'k).
native_hal_read:is_lower(0'l).
native_hal_read:is_lower(0'm).
native_hal_read:is_lower(0'n).
native_hal_read:is_lower(0'o).
native_hal_read:is_lower(0'p).
native_hal_read:is_lower(0'q).
native_hal_read:is_lower(0'r).
native_hal_read:is_lower(0's).
native_hal_read:is_lower(0't).
native_hal_read:is_lower(0'u).
native_hal_read:is_lower(0'v).
native_hal_read:is_lower(0'w).
native_hal_read:is_lower(0'x).
native_hal_read:is_lower(0'y).
native_hal_read:is_lower(0'z).

%---------------------------------------------------------------------------%
:- pred native_hal_read:is_digit(int::in) is semidet.
%---------------------------------------------------------------------------%
% could be implemented more efficiently

native_hal_read:is_digit(0'0).
native_hal_read:is_digit(0'1).
native_hal_read:is_digit(0'2).
native_hal_read:is_digit(0'3).
native_hal_read:is_digit(0'4).
native_hal_read:is_digit(0'5).
native_hal_read:is_digit(0'6).
native_hal_read:is_digit(0'7).
native_hal_read:is_digit(0'8).
native_hal_read:is_digit(0'9).

%---------------------------------------------------------------------------%
:- pred native_hal_read:is_alnum_or_underscore(int::in) is semidet.
%---------------------------------------------------------------------------%

native_hal_read:is_alnum_or_underscore(C) :-
	native_hal_read:is_lower(C) -> true ;
	native_hal_read:is_upper(C) -> true ;
	native_hal_read:is_digit(C) -> true ;
	C == 0'_.

%---------------------------------------------------------------------------%
:- pred native_hal_read:det_int_to_digit(int::in,int::out) is det.
%---------------------------------------------------------------------------%

native_hal_read:det_int_to_digit(X,Y) :-
	(native_hal_read:int_to_digit(X,Y) ->
	    true
	;
	    error("det_int_to_digit - wrong input")
	).

%---------------------------------------------------------------------------%
:- pred native_hal_read:int_to_digit(int,int).
:- mode native_hal_read:int_to_digit(in,out) is semidet.
:- mode native_hal_read:int_to_digit(out,in) is semidet.
%---------------------------------------------------------------------------%
% could be implemented more efficiently
% since hProlog does not have indexing on second arg
% maybe should have a special implementation according to mode ?

native_hal_read:int_to_digit(0, 0'0).
native_hal_read:int_to_digit(1, 0'1).
native_hal_read:int_to_digit(2, 0'2).
native_hal_read:int_to_digit(3, 0'3).
native_hal_read:int_to_digit(4, 0'4).
native_hal_read:int_to_digit(5, 0'5).
native_hal_read:int_to_digit(6, 0'6).
native_hal_read:int_to_digit(7, 0'7).
native_hal_read:int_to_digit(8, 0'8).
native_hal_read:int_to_digit(9, 0'9).
native_hal_read:int_to_digit(10, 0'A).
native_hal_read:int_to_digit(11, 0'B).
native_hal_read:int_to_digit(12, 0'C).
native_hal_read:int_to_digit(13, 0'D).
native_hal_read:int_to_digit(14, 0'E).
native_hal_read:int_to_digit(15, 0'F).
native_hal_read:int_to_digit(16, 0'G).
native_hal_read:int_to_digit(17, 0'H).
native_hal_read:int_to_digit(18, 0'I).
native_hal_read:int_to_digit(19, 0'J).
native_hal_read:int_to_digit(20, 0'K).
native_hal_read:int_to_digit(21, 0'L).
native_hal_read:int_to_digit(22, 0'M).
native_hal_read:int_to_digit(23, 0'N).
native_hal_read:int_to_digit(24, 0'O).
native_hal_read:int_to_digit(25, 0'P).
native_hal_read:int_to_digit(26, 0'Q).
native_hal_read:int_to_digit(27, 0'R).
native_hal_read:int_to_digit(28, 0'S).
native_hal_read:int_to_digit(29, 0'T).
native_hal_read:int_to_digit(30, 0'U).
native_hal_read:int_to_digit(31, 0'V).
native_hal_read:int_to_digit(32, 0'W).
native_hal_read:int_to_digit(33, 0'X).
native_hal_read:int_to_digit(34, 0'Y).
native_hal_read:int_to_digit(35, 0'Z).


%%%%%%%%%%%%%%%% parts from string.m

%---------------------------------------------------------------------------%
:- pred native_hal_read:list_of_strings_to_string(list(string), string).
:- mode native_hal_read:list_of_strings_to_string(in,out) is det.
%---------------------------------------------------------------------------%
% list_of_strings_to_string/2
% 
%---------------------------------------------------------------------------%

native_hal_read:list_of_strings_to_string([],_) :-
	error("unexpected list_of_strings_to_string/2 with empty first arg").
native_hal_read:list_of_strings_to_string([S|ListofStrings],String) :-
	(ListofStrings = [] ->
	    String = S
	;
	    native_hal_read:list_of_strings_to_string(ListofStrings,String1),
	    string:append(S,String1,String)
	).

%---------------------------------------------------------------------------%
:- pred native_hal_read:my_int_to_string(int,string).
:- mode native_hal_read:my_int_to_string(in,out) is det.
%---------------------------------------------------------------------------%
% my_int_to_string/2
% 
%---------------------------------------------------------------------------%

native_hal_read:my_int_to_string(N, Str) :-
	native_hal_read:int_to_base_string(N, 10, Str).


%---------------------------------------------------------------------------%
:- pred native_hal_read:int_to_base_string(int,int,string).
:- mode native_hal_read:int_to_base_string(in,in,out) is det.
%---------------------------------------------------------------------------%
% int_to_base_string/3
% 
%---------------------------------------------------------------------------%

native_hal_read:int_to_base_string(N, Base, Str) :-
	( Base >= 2, Base =< 36	->
	    true
	;
	    error("int_to_base_ invalid base")
	),
	native_hal_read:int_to_base_int_list_1(N, Base, Str).

%---------------------------------------------------------------------------%
:- pred native_hal_read:int_to_base_int_list_1(int, int, string).
:- mode native_hal_read:int_to_base_int_list_1(in,in,out) is det.
%---------------------------------------------------------------------------%
% int_to_base_int_list_1/3
% 
%---------------------------------------------------------------------------%

native_hal_read:int_to_base_int_list_1(N, Base, Str) :-
	% Note that in order to handle MININT correctly,
	% we need to do the conversion of the absolute
	% number into digits using negative numbers
	% (we can't use positive numbers, since -MININT overflows)
	( N < 0	->
	    native_hal_read:int_to_base_int_list_2(N, Base, [], IntList)
	;
	    N1 is 0 - N,
	    native_hal_read:int_to_base_int_list_2(N1, Base, [], IntList)
	),
% 	string:rev_int_list_to_string(IntList,Str).
	ilist_to_string(IntList,Str).

%---------------------------------------------------------------------------%
:- pred native_hal_read:int_to_base_int_list_2(int, int, list(int), list(int)).
:- mode native_hal_read:int_to_base_int_list_2(in,  in,   in,        out) is det.
%---------------------------------------------------------------------------%
% int_to_base_int_list_2/4
%
%---------------------------------------------------------------------------%
native_hal_read:int_to_base_int_list_2(NegN, Base, Acc, Str) :-
	( NegN > -Base ->
	    N is -NegN,
	    native_hal_read:det_int_to_digit(N, DigitChar),
	    Str = [DigitChar|Acc]
	;
	    NegN1 is NegN // Base,
	    N10 is (NegN1 * Base) - NegN,
	    native_hal_read:det_int_to_digit(N10, DigitChar),
	    native_hal_read:int_to_base_int_list_2(NegN1, Base, [DigitChar|Acc], Str)
	).





%%%%%%%%%%%% parts from term.m

%---------------------------------------------------------------------------%
:- pred native_hal_read:context_init(string,int,term_context).
:- mode native_hal_read:context_init(in,in,out) is det.
%---------------------------------------------------------------------------%
% context_init/3
% 
%---------------------------------------------------------------------------%

native_hal_read:context_init(File, LineNumber, context(File, LineNumber)).

%%%%%%%%%%%%%% parts from ops.m

:- pred native_hal_read:op_table(string, category, specifier, int).
:- mode native_hal_read:op_table(in, in, out, out) is semidet.

native_hal_read:op_table(Op,Cat,Spec,Prec) :-
	(ops:dynamic_op_table(Op,Cat,Spec1,Prec1) -> % should be if/3
	    Prec1 >= 0,
	    Spec = Spec1,
	    Prec = Prec1
	;
	    native_hal_read:static_op_table(Op,Cat,Spec,Prec)
	).

:- pred native_hal_read:static_op_table(string, category, specifier, int).
:- mode native_hal_read:static_op_table(in, in, out, out) is semidet.

% original op_table adapted for HAL - still need to be checked - some must be removed

native_hal_read:static_op_table("public",before,fx,1150).
native_hal_read:static_op_table("dynamic",before,fx,1150).
native_hal_read:static_op_table("multifile",before,fx,1150).
native_hal_read:static_op_table("block",before,fx,1150).
native_hal_read:static_op_table("meta_predicate",before,fx,1150).
native_hal_read:static_op_table("initialization",before,fx,1150).
native_hal_read:static_op_table("rem",after,yfx,400).
native_hal_read:static_op_table("pragma",before,fx,1199).
native_hal_read:static_op_table("--->",after,xfy,1179).
native_hal_read:static_op_table("*",after,yfx,400).
native_hal_read:static_op_table("**",after,xfy,200).
native_hal_read:static_op_table("+",after,yfx,500).
native_hal_read:static_op_table("+",before,fx,500).
native_hal_read:static_op_table(",",after,xfy,1000).
native_hal_read:static_op_table("-",after,yfx,500).
native_hal_read:static_op_table("-",before,fx,500).
native_hal_read:static_op_table("->",after,xfy,1050).
native_hal_read:static_op_table("/",after,yfx,400).
native_hal_read:static_op_table("//",after,yfx,400).
native_hal_read:static_op_table("/\\",after,yfx,500).
native_hal_read:static_op_table("xor",after,yfx,400).
native_hal_read:static_op_table(":",after,xfy,550).
native_hal_read:static_op_table(":-",after,xfx,1200).
native_hal_read:static_op_table(":-",before,fx,1200).
native_hal_read:static_op_table(";",after,xfy,1100).
%% native_hal_read:static_op_table("|",before,fx,1190).
native_hal_read:static_op_table("<",after,xfx,700).
native_hal_read:static_op_table("<<",after,yfx,400).
native_hal_read:static_op_table("=",after,xfx,700).
native_hal_read:static_op_table("=..",after,xfx,700).
native_hal_read:static_op_table("=:=",after,xfx,700).
native_hal_read:static_op_table("=<",after,xfx,700).
native_hal_read:static_op_table("==",after,xfx,700).
native_hal_read:static_op_table("=\\=",after,xfx,700).
native_hal_read:static_op_table(">",after,xfx,700).
native_hal_read:static_op_table(">=",after,xfx,700).
native_hal_read:static_op_table(">>",after,yfx,400).
native_hal_read:static_op_table("?-",before,fx,1200).
native_hal_read:static_op_table("@<",after,xfx,700).
native_hal_read:static_op_table("@=<",after,xfx,700).
native_hal_read:static_op_table("@>",after,xfx,700).
native_hal_read:static_op_table("@>=",after,xfx,700).
native_hal_read:static_op_table("\\",before,fy,200).
native_hal_read:static_op_table("\\+",before,fy,900).
native_hal_read:static_op_table("\\/",after,yfx,500).
native_hal_read:static_op_table("\\=",after,xfx,700).
native_hal_read:static_op_table("\\==",after,xfx,700).
native_hal_read:static_op_table("^",after,xfy,200).
native_hal_read:static_op_table("mod",after,xfx,300).
native_hal_read:static_op_table("#",after,yfx,500).
native_hal_read:static_op_table("module",before,fx,1190).
native_hal_read:static_op_table("import",before,fx,1190).
native_hal_read:static_op_table("include",before,fx,1190).
native_hal_read:static_op_table("export",before,fx,1190).
native_hal_read:static_op_table("local",before,fx,1190).
native_hal_read:static_op_table("typedef",before,fx,1180).
native_hal_read:static_op_table("modedef",before,fx,1180).
native_hal_read:static_op_table("instdef",before,fx,1180).
native_hal_read:static_op_table("deriving",after,yfx,1170).
native_hal_read:static_op_table("@",after,xfx,1200).
native_hal_read:static_op_table("pragma",after,xfx,1190).
native_hal_read:static_op_table("<=>",after,xfx,1180).
native_hal_read:static_op_table("\\",after,xfx,1045).
native_hal_read:static_op_table("&",after,xfx,1050).
native_hal_read:static_op_table("func",before,fx,900).
native_hal_read:static_op_table("pred",before,fx,900).
native_hal_read:static_op_table("mode",before,fx,900).
native_hal_read:static_op_table("type",before,fx,900).
native_hal_read:static_op_table("inst",before,fx,900).
native_hal_read:static_op_table("op",before,fx,900).
native_hal_read:static_op_table("class",before,fx,900).
native_hal_read:static_op_table("instance",before,fx,900).
native_hal_read:static_op_table("delay",before,fx,900).
native_hal_read:static_op_table("is",after,yfx,850).
native_hal_read:static_op_table("where",after,yfx,850).
native_hal_read:static_op_table("-->",after,xfx,1150).
native_hal_read:static_op_table("<=",after,yfx,850).
native_hal_read:static_op_table("::",after,xfx,650).
native_hal_read:static_op_table("$",before,fx,300).
native_hal_read:static_op_table("|",after,xfy,1105).  % was 500
native_hal_read:static_op_table("init",after,xfx,310).
native_hal_read:static_op_table("to",after,xfx,310).
native_hal_read:static_op_table("==>",after,xfx,1180).
native_hal_read:static_op_table(":=",after,xfx,700).
native_hal_read:static_op_table("`",before,fx,5).



%---------------------------------------------------------------------------%
:- pred native_hal_read:max_priority(int::out) is det.
%---------------------------------------------------------------------------%
% max_priority/1
% 
%---------------------------------------------------------------------------%

native_hal_read:max_priority(MaxPrior) :- MaxPrior = 1200.

%---------------------------------------------------------------------------%
:- pred native_hal_read:arg_priority(int::out) is det.
%---------------------------------------------------------------------------%
% arg_priority/1
%
%---------------------------------------------------------------------------%

native_hal_read:arg_priority(ArgPrior) :- ArgPrior = 1201.


%---------------------------------------------------------------------------%
:- pred native_hal_read:lookup_binary_prefix_op(string,int,opsassoc, opsassoc).
:- mode native_hal_read:lookup_binary_prefix_op(in,out,out,out) is failure.
%---------------------------------------------------------------------------%
% lookup_binary_prefix_op/4
%
%---------------------------------------------------------------------------%

native_hal_read:lookup_binary_prefix_op(_,_,_,_) :- fail.

%---------------------------------------------------------------------------%
:- pred native_hal_read:lookup_prefix_op(string,int,opsassoc).
:- mode native_hal_read:lookup_prefix_op(in,out,out) is semidet.
%---------------------------------------------------------------------------%
% lookup_prefix_op/3
%
%---------------------------------------------------------------------------%

native_hal_read:lookup_prefix_op(String,Prec,Assoc) :-
	native_hal_read:op_table(String,before,Assoc1,Prec),
	(Assoc1 == fx ->
	    Assoc = x
	;
	    Assoc1 == fy,
	    Assoc = y
	).

%---------------------------------------------------------------------------%
:- pred native_hal_read:lookup_infix_op(string,int,opsassoc,opsassoc).
:- mode native_hal_read:lookup_infix_op(in,out,out,out) is semidet.
%---------------------------------------------------------------------------%
% lookup_infix_op/4
%
%---------------------------------------------------------------------------%

native_hal_read:lookup_infix_op(String,Prec,AssocLeft,AssocRight) :-
        native_hal_read:op_table(String,after,Assoc1,Prec),
	(Assoc1 == xfx ->
	    AssocLeft = x, AssocRight = x
	;
	    (Assoc1 == xfy ->
		AssocLeft = x, AssocRight = y
	    ;
		Assoc1 = yfx,
		AssocLeft = y, AssocRight = x
	    )
	).

%---------------------------------------------------------------------------%
:- pred native_hal_read:lookup_postfix_op(string,int,opsassoc).
:- mode native_hal_read:lookup_postfix_op(in,out,out) is semidet.
%---------------------------------------------------------------------------%
% lookup_postfix_op/3
% 
%---------------------------------------------------------------------------%

native_hal_read:lookup_postfix_op(String,Prec,Assoc) :-
        native_hal_read:op_table(String,after,Assoc1,Prec),
	(Assoc1 == xf ->
	    Assoc = x
	;
	    Assoc1 == yf,
	    Assoc = y
	).


%---------------------------------------------------------------------------%
:- pred native_hal_read:lookup_op(string).
:- mode native_hal_read:lookup_op(in) is semidet.
%---------------------------------------------------------------------------%
% lookup_op/1
% 
%---------------------------------------------------------------------------%

native_hal_read:lookup_op(String) :-
	(native_hal_read:op_table(String, before, _Assoc, _Prec) ->
	    true
	;
	    native_hal_read:op_table(String, after, _Assoc, _Prec)
	).

%%%%%%%%%%%% most of parser.m

%-----------------------------------------------------------------------------%
:- export pred native_hal_read:hal_read_term(hal_read_term, state, state).
:-        mode native_hal_read:hal_read_term(out, di, uo) is det.
%-----------------------------------------------------------------------------%

native_hal_read:hal_read_term(Result) -->
	(io):input_stream_name(FileName),
	native_hal_read:hal_read_term(FileName, Result).

%-----------------------------------------------------------------------------%
:- pred native_hal_read:hal_read_term(string, hal_read_term, state, state).
:- mode native_hal_read:hal_read_term(in, out, di, uo) is det.
%-----------------------------------------------------------------------------%

native_hal_read:hal_read_term(FileName, Result) -->
	native_hal_read:get_token_list(Tokens),
	{ native_hal_read:parse_tokens(FileName, Tokens, Result) }.

%-----------------------------------------------------------------------------%
%
% parse_tokens/{3,4}:
%	Parses a list of tokens.
:- pred native_hal_read:parse_tokens(string, token_list, hal_read_term).
:- mode native_hal_read:parse_tokens(in, in, out) is det.
%-----------------------------------------------------------------------------%

native_hal_read:parse_tokens(FileName, Tokens, Result) :-
	( Tokens = token_nil ->
	    Result = eof
	;
	    native_hal_read:init_state(FileName, Tokens, ParserState0),
	    native_hal_read:parse_whole_term(Term, ParserState0, ParserState),
	    native_hal_read:final_state(ParserState, VarSet, LeftOverTokens),
	    native_hal_read:check_for_errors(Term, VarSet, Tokens, LeftOverTokens, Result)
	).

:- pred native_hal_read:check_for_errors(parse(sterm), dict, token_list, token_list, hal_read_term).
:- mode native_hal_read:check_for_errors(in, in, in, in, out) is det.

native_hal_read:check_for_errors(error(ErrorMessage, ErrorTokens), _VarSet, Tokens,
		_LeftOverTokens, Result) :-
	% check if the error was caused by a bad token
	(
		native_hal_read:check_for_bad_token(Tokens,
			BadTokenMessage, BadTokenLineNum)
	->
		Message = BadTokenMessage,
		LineNum = BadTokenLineNum
	;
		% find the token that caused the error
		(
			ErrorTokens = token_cons(ErrorTok, ErrorTokLineNum, _)
		->
			native_hal_read:token_to_string(ErrorTok, TokString),
			native_hal_read:list_of_strings_to_string( ["Syntax error at ", TokString,
						": ", ErrorMessage], Message),
			LineNum = ErrorTokLineNum
		;
			(
				Tokens = token_cons(_, FirstTokLineNum, _)
			->
				LineNum = FirstTokLineNum
			;
				error("check_for_errors")
			),
			string:append("Syntax error: ", ErrorMessage, Message)
		)
	),
	Result = mercury_error(Message, LineNum).

native_hal_read:check_for_errors(ok(Term), VarSet, Tokens, LeftOverTokens, Result) :-
	(
		native_hal_read:check_for_bad_token(Tokens, Message, LineNum)
	->
		Result = mercury_error(Message, LineNum)
	;
		LeftOverTokens = token_cons(Token, LineNum, _)
	->
		native_hal_read:token_to_string(Token, TokString),
		string:append("Syntax error: unexpected ", TokString, Message),
		Result = mercury_error(Message, LineNum)
	;
		Result = hal_read_term(VarSet, Term)
	).

:- pred native_hal_read:check_for_bad_token(token_list, string, int).
:- mode native_hal_read:check_for_bad_token(in, out, out) is semidet.

native_hal_read:check_for_bad_token(token_cons(Token, LineNum, Tokens),	Message, LineNum) :-
	( Token = io_error(IO_Error) ->
		native_hal_read:error_message(IO_Error, IO_ErrorMessage),
		string:append("I/O error: ", IO_ErrorMessage, Message)
	; Token = junk(Code) ->
		native_hal_read:int_to_base_string(Code, 10, Decimal),
		native_hal_read:int_to_base_string(Code, 16, Hex),
		native_hal_read:list_of_strings_to_string(["Syntax error: Illegal character 0x",
			Hex, " (", Decimal, ") in input"], Message)
	; Token = error(ErrorMessage) ->
		string:append("Syntax error: ", ErrorMessage, Message)
	;
		native_hal_read:check_for_bad_token(Tokens, Message, LineNum)
	).

:- pred native_hal_read:parse_whole_term(parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_whole_term(out, in, out) is det.

native_hal_read:parse_whole_term(Term) -->
	native_hal_read:parse_term(Term0),
	( { Term0 = ok(_) } ->
		( native_hal_read:get_token(end) ->
			{ Term = Term0 }
		;
			native_hal_read:unexpected("operator or `.' expected", Term)
		)
	;
		% propagate error upwards
		{ Term = Term0 }
	).



:- pred native_hal_read:parse_term(parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_term(out, in, out) is det.

native_hal_read:parse_term(Term) -->
	{native_hal_read:max_priority(MaxPrior), % change
	MaxPrior1 is MaxPrior + 1}, 
	native_hal_read:parse_term_2(MaxPrior1, no, Term).

:- pred native_hal_read:parse_arg(parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_arg(out, in, out) is det.

native_hal_read:parse_arg(Term) -->
	{native_hal_read:arg_priority(ArgPrior)}, % change
	native_hal_read:parse_term_2(ArgPrior, yes, Term).

:- pred native_hal_read:parse_term_2(int, bool, parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_term_2(in, in, out, in, out) is det.

native_hal_read:parse_term_2(MaxPriority, IsArg, Term) -->
	native_hal_read:parse_left_term(MaxPriority, IsArg, LeftPriority, LeftTerm0),
	( { LeftTerm0 = ok(LeftTerm) } ->
	    native_hal_read:parse_rest(MaxPriority, IsArg, LeftPriority, LeftTerm, Term)
	;
				% propagate error upwards
	    { Term = LeftTerm0 }
	).

:- pred native_hal_read:parse_left_term(int, bool, int, parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_left_term(in, in, out, out, in, out) is det.

native_hal_read:parse_left_term(MaxPriority, IsArg, OpPriority, Term) -->
	( native_hal_read:get_token(Token, Context) ->
	    (
				% check for unary minus of integer
	      { Token = name("-") },
	      native_hal_read:get_token(integer(X), _IntContext)
	    ->
	      native_hal_read:get_term_context(Context, TermContext),
	      { NegX is 0 - X },
	      { Term = ok(integer(NegX, TermContext)) },
	      { OpPriority = 0 }
	    ;
				% check for unary minus of float
	      { Token = name("-") },
	      native_hal_read:get_token(float(F), _FloatContext)
	    ->
	      native_hal_read:get_term_context(Context, TermContext),
	      { NegF is 0.0 - F },
	      { Term = ok(float(NegF, TermContext)) },
	      { OpPriority = 0 }
	    ;
				% check for binary prefix op
	      { Token = name(Op) },
	      native_hal_read:not_peek_token(open_ct),
	      { native_hal_read:lookup_binary_prefix_op(Op,BinOpPriority, RightAssoc, RightRightAssoc) },
	      { BinOpPriority =< MaxPriority },
	      native_hal_read:peek_token(NextToken),
	      { native_hal_read:could_start_term(NextToken, yes) }
	    ->
	      { native_hal_read:adjust_priority(RightAssoc, BinOpPriority, RightPriority) },
	      { native_hal_read:adjust_priority(RightRightAssoc, BinOpPriority, RightRightPriority) },
	      { OpPriority = BinOpPriority },
	      native_hal_read:parse_term_2(RightPriority, IsArg, RightResult),
	      ( { RightResult = ok(RightTerm) } ->
		  native_hal_read:parse_term_2(RightRightPriority, IsArg,
			       RightRightResult),
		  ( { RightRightResult = ok(RightRightTerm) } ->
		      native_hal_read:get_term_context(Context,
				       TermContext),
		      { Term = ok(functor(2, Op,[RightTerm, RightRightTerm],TermContext)) }
		  ;
				% propagate error upwards
		      { Term = RightRightResult }
		  )
	      ;
				% propagate error upwards
		  { Term = RightResult }
	      )
	    ;
				% check for unary prefix op
	      { Token = name(Op) },
	      native_hal_read:not_peek_token(open_ct),
	      { native_hal_read:lookup_prefix_op(Op, UnOpPriority, RightAssoc) },
	      { UnOpPriority =< MaxPriority },
	      native_hal_read:peek_token(NextToken),
	      { native_hal_read:could_start_term(NextToken, yes) }
	    ->
	      { native_hal_read:adjust_priority(RightAssoc, UnOpPriority,
				RightPriority) },
	      native_hal_read:parse_term_2(RightPriority, IsArg, RightResult),
	      { OpPriority = UnOpPriority },
	      ( { RightResult = ok(RightTerm) } ->
		  native_hal_read:get_term_context(Context, TermContext),
		  { Term = ok(functor(1, Op, [RightTerm], TermContext)) }
	      ;
				% propagate error upwards
		  { Term = RightResult }
	      )
	    ;
	      native_hal_read:parse_simple_term(Token, Context, MaxPriority,Term),
	      { OpPriority = 0 }
	    )
	;
	    native_hal_read:error("unexpected end-of-file at start of sub-term", Term),
	    { OpPriority = 0 }
	).

:- pred native_hal_read:parse_rest(int, bool, int, sterm /* term(T) */, parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_rest(in,  in,   in,  in,      out,          in,         out) is det.

native_hal_read:parse_rest(MaxPriority, IsArg, LeftPriority, LeftTerm, Term) -->
% 	w(t(MaxPriority, IsArg, LeftPriority, LeftTerm)),
	(
				% infix op
	  native_hal_read:get_token(Token, Context),
	  ({ Token = comma, IsArg = no} ->
	      { Op0 = "," }
	  ;
	      {Token = ht_sep, IsArg = no} ->
	      { Op0 = "|" } %% change - not ";" !
	  ;
	      % next two added Fri Jun 25 12:19:21 CEST 2010 - but open to debate :-!
	      ({ Token = open_curly }, native_hal_read:get_token(close_curly,Context)) ->
	      { Op0 = "{}" }
	  ;
	      ({ Token = open_list }, native_hal_read:get_token(close_list,Context)) ->
	      { Op0 = "[]" }
	  ;
	      {Token = name(Op0)}
	  ),
	  (
%% 				% A token surrounded by backquotes is a
%% 				% prefix token being using in an
%% 				% infix manner.
%% 	    { Op0 = "`" },
%% 	    { lookup_operator_term(OpPriority0, LeftAssoc0, RightAssoc0) }
%% 	  ->
%% 	    { OpPriority = OpPriority0 },
%% 	    { LeftAssoc = LeftAssoc0 },
%% 	    { RightAssoc = RightAssoc0 },
%% 	    native_hal_read:get_token(OpToken, _),
%% 	    (
%% 	      { OpToken = name(NameOp) }
%% 	    ->
%% 	      { Op = NameOp },
%% 	      { VariableTerm = [] }
%% 	    ;
%% 	      { OpToken = variable(VariableOp) },
%% 	      { Op = "" },
%% 	      native_hal_read:add_var(VariableOp, Var),
%% 	      native_hal_read:get_term_context(Context, TermContext),
%% 	      { VariableTerm = [var(Var,TermContext)] }
%% 	    ),
%% 	    native_hal_read:get_token(name("`"), _)
%% 	  ;
	    { Op = Op0 },
	    { VariableTerm = [] },
	    { native_hal_read:lookup_infix_op(Op, OpPriority, LeftAssoc, RightAssoc) }
	  ),
	  { OpPriority =< MaxPriority },
	  { native_hal_read:check_priority(LeftAssoc, OpPriority, LeftPriority) }
	->
	  { native_hal_read:adjust_priority(RightAssoc, OpPriority, RightPriority) },
	  native_hal_read:parse_term_2(RightPriority, IsArg, RightTerm0),
	  ( { RightTerm0 = ok(RightTerm) } ->
	      native_hal_read:get_term_context(Context, TermContext),
	      { native_hal_read:my_app(VariableTerm,[LeftTerm, RightTerm],AppRes), %% change
		native_hal_read:my_len(AppRes,Arity),
		OpTerm = functor(Arity, Op, AppRes, TermContext) }, % change
	      %% 				native_hal_read:my_app(VariableTerm,
	      %% 					[LeftTerm, RightTerm]),
	      native_hal_read:parse_rest(MaxPriority, IsArg, OpPriority, OpTerm, Term)
	  ;
				% propagate error upwards
	      { Term = RightTerm0 }
	  )
	;
				% postfix op
	  native_hal_read:get_token(name(Op), Context),
	  { native_hal_read:lookup_postfix_op(Op, OpPriority, LeftAssoc) },
	  { OpPriority =< MaxPriority },
	  { native_hal_read:check_priority(LeftAssoc, OpPriority, LeftPriority) }
	->
	  native_hal_read:get_term_context(Context, TermContext),
	  { OpTerm = functor(1, Op, [LeftTerm], TermContext) },
	  native_hal_read:parse_rest(MaxPriority, IsArg, OpPriority, OpTerm, Term)
	;
	  { Term = ok(LeftTerm) }
	).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:parse_simple_term(token, token_context, int, parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_simple_term(in, in, in, out, in, out) is det.

native_hal_read:parse_simple_term(Token, Context, Priority, Term) -->
    ( native_hal_read:parse_simple_term_2(Token, Context, Priority, Term0) ->
	native_hal_read:check_for_higher_order_term(Term0, Context, Term)
    ;
	native_hal_read:unexpected_tok(Token, Context,
		"unexpected token at start of (sub)term", Term)
    ).

	% term --> integer		% priority 0
	% term --> float		% priority 0
	% term --> name("-") integer	% priority 0
	% term --> name("-") float	% priority 0
	% term --> atom(NonOp)		% priority 0
	% term --> atom(Op)		% priority `max_priority' + 1
	%	atom --> name
	%	atom --> open_list, close_list
	%	atom --> open_curly, close_curly
	% term --> variable		% priority 0
	% term --> atom, open_ct, arg_list, close
	%	arg_list --> arg
	%	arg_list --> arg, comma, arg_list
	% term --> open, term, close
	% term --> open_ct, term, close
	% term --> term, op, term	% with various conditions
	% term --> op, term		% with various conditions
	% term --> term, op		% with various conditions

:- pred native_hal_read:parse_simple_term_2(token, token_context, int, parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_simple_term_2(in, in, in, out, in, out) is semidet.

%% native_hal_read:parse_simple_term_2(ht_sep, Context, Prec, Term) -->
%% 	native_hal_read:parse_simple_term_2(name("|"), Context, Prec, Term).

native_hal_read:parse_simple_term_2(name(Atom), Context, Prec, Term) -->
	native_hal_read:get_term_context(Context, TermContext),
	( native_hal_read:get_token(open_ct) ->
	    native_hal_read:parse_args(Args0),
	    (	{ Args0 = ok(Args) },
		{ native_hal_read:my_len(Args,Arity)} ,
		{Term = ok(functor(Arity, Atom, Args, TermContext)) }
	    ;
				% propagate error upwards
		{ Args0 = error(Message, Tokens) },
		{ Term = error(Message, Tokens) }
	    )
	;
	    { native_hal_read:lookup_op(Atom) ->
	      native_hal_read:max_priority(MaxPrior), % change
	      Prec > MaxPrior
	    ;
	      true
	    },
	    { Term = ok(atom(Atom, TermContext)) }
	).

native_hal_read:parse_simple_term_2(variable(VarName), Context, _, Term) -->
	native_hal_read:add_var(VarName, Var),
	native_hal_read:get_term_context(Context, TermContext),
	{ Term = ok(var(Var,TermContext)) }.

native_hal_read:parse_simple_term_2(integer(Int), Context, _, Term) -->
	native_hal_read:get_term_context(Context, TermContext),
	{ Term = ok(integer(Int, TermContext)) }.

native_hal_read:parse_simple_term_2(float(Float), Context, _, Term) -->
	native_hal_read:get_term_context(Context, TermContext),
	{ Term = ok(float(Float, TermContext)) }.

native_hal_read:parse_simple_term_2(string(String), Context, _, Term) -->
	native_hal_read:get_term_context(Context, TermContext),
	{ Term = ok(string(String, TermContext)) }.

native_hal_read:parse_simple_term_2(char(Int), Context, _, Term) -->
	native_hal_read:get_term_context(Context, TermContext),
	{ Term = ok(char(Int, TermContext)) }.

native_hal_read:parse_simple_term_2(open, _, _, Term) -->
	native_hal_read:parse_term(Term0),
	( { Term0 = ok(_) } ->
		( native_hal_read:get_token(close) ->
			{ Term = Term0 }
		;
			native_hal_read:unexpected("expecting `)' or operator", Term)
		)
	;
		% propagate error upwards
		{ Term = Term0 }
	).

native_hal_read:parse_simple_term_2(open_ct, Context, Prec, Term) -->
	native_hal_read:parse_simple_term_2(open, Context, Prec, Term).

native_hal_read:parse_simple_term_2(open_list, Context, _, Term) -->
	native_hal_read:get_term_context(Context, TermContext),
	( native_hal_read:get_token(close_list) ->
		native_hal_read:parse_special_atom("[]", TermContext, Term)
	;
		native_hal_read:parse_list(Term)
	).

native_hal_read:parse_simple_term_2(open_curly, Context, _, Term) -->
	native_hal_read:get_term_context(Context, TermContext),
	( native_hal_read:get_token(close_curly) ->
	    native_hal_read:parse_special_atom("{}", TermContext, Term)
	;
				% This is a slight departure from ISO Prolog
				% syntax -- instead of parsing "{1,2,3}"
				% as "'{}'(','(1, ','(2, 3)))" we parse
				% it as "'{}'(1,2,3)". This makes the
				% structure of tuple functors the same
				% as other functors.
				% Taken out by Bart Demoen
				% Monash 
				% Tue Nov 29 02:51:28 CET 2005
	    
	    native_hal_read:parse_term(SubTerm0),
	    ( { SubTerm0 = ok(SubTerm) } ->
		( native_hal_read:get_token(close_curly) ->
		    { Term = ok(functor(1,"{}", [SubTerm], TermContext)) }
%% 		{ native_hal_read:conjunction_to_list(SubTerm, ArgTerms) },
%% 		( native_hal_read:get_token(close_curly) ->
%% 		    { native_hal_read:my_len(ArgTerms,Arity) },
%% 		    { Term = ok(functor(Arity,"{}", ArgTerms, TermContext)) }
		;
		    native_hal_read:unexpected("expecting `}' or operator",
			       Term)
		)
	    ;
				% propagate error upwards
		{ Term = SubTerm0 }
	    )
	).

:- pred native_hal_read:conjunction_to_list(term(T), list(term(T))).
:- mode native_hal_read:conjunction_to_list(in, out) is det.

native_hal_read:conjunction_to_list(Term, ArgTerms) :-
	( Term = functor(2, ",", [LeftTerm, RightTerm], _) ->
		native_hal_read:conjunction_to_list(RightTerm, ArgTerms0),
		ArgTerms = [LeftTerm | ArgTerms0]
	;
		ArgTerms = [Term]
	).

:- pred native_hal_read:check_for_higher_order_term(parse(sterm), token_context,
	parse(sterm), pars_state, pars_state).
:- mode native_hal_read:check_for_higher_order_term(in, in, out, in, out) is det.

native_hal_read:check_for_higher_order_term(Term0, Context, Term) -->
	%
	% As an extension to ISO Prolog syntax,
	% we check for the syntax "Term(Args)", and parse it
	% as the term ''(Term, Args).  The aim of this extension
	% is to provide a nicer syntax for higher-order stuff.
	%
	( { Term0 = ok(Term1) }, native_hal_read:get_token(open_ct) ->
	    native_hal_read:get_term_context(Context, TermContext),
	    native_hal_read:parse_args(Args0),
	    (	{ Args0 = ok(Args) } -> %%% used to be  ->
		{ AllArgs = [Term1 | Args] },
		{ native_hal_read:my_len(AllArgs,Arity)} ,
		{ Term2 = ok(functor(Arity,"", AllArgs,TermContext)) },
		native_hal_read:check_for_higher_order_term(Term2, Context, Term)
	    ;
				% propagate error upwards
		{ Args0 = error(Message, Tokens) },
		{ Term = error(Message, Tokens) }
	    )
	;
	    { Term = Term0 }
	).

:- pred native_hal_read:parse_special_atom(string, term_context, parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_special_atom(in, in, out, in, out) is det.

native_hal_read:parse_special_atom(Atom, TermContext, Term) -->
	( native_hal_read:get_token(open_ct) ->
	    native_hal_read:parse_args(Args0),
	    (	{ Args0 = ok(Args) },
		{ native_hal_read:my_len(Args,Arity)},
		{ Term = ok(functor(Arity, Atom, Args, TermContext)) }
	    ;
				% propagate error upwards
		{ Args0 = error(Message, Tokens) },
		{ Term = error(Message, Tokens) }
	    )
	;
	    { Term = ok(atom(Atom, TermContext)) }
	).

:- pred native_hal_read:parse_list(parse(sterm), pars_state, pars_state).
:- mode native_hal_read:parse_list(out, in, out) is det.

native_hal_read:parse_list(List) -->
	{native_hal_read:list_constructor(ListConstructor)},
	native_hal_read:parse_arg(Arg0),
	( { Arg0 = ok(Arg) } ->
	    ( native_hal_read:get_token(Token, Context) ->
		native_hal_read:get_term_context(Context, TermContext),
		( { Token = comma } ->
		    native_hal_read:parse_list(Tail0),
		    ( { Tail0 = ok(Tail) } ->
		        {List = ok(functor(2, ListConstructor, [Arg, Tail], TermContext)) }
		    ;
				% propagate error
			{ List = Tail0 }
		    )
		; { Token = name("|") /* ht_sep */ } ->
		    native_hal_read:parse_arg(Tail0),
		    ( { Tail0 = ok(Tail) } ->
			( native_hal_read:get_token(close_list) ->
		            { List = ok(functor(2, ListConstructor, [Arg, Tail], TermContext)) }
			;
			    native_hal_read:unexpected("expecting ']' or operator",List)
			)
		    ;
				% propagate error
			{ List = Tail0 }
		    )
		;
		    { Arg = functor(2,"|",[Arg1,Arg2],_) } ->
		    { List = ok(functor(2, ListConstructor, [Arg1,Arg2], TermContext)) }
		; { Token = close_list } ->
		    { Tail = atom("[]", TermContext) },
		    { List = ok(functor(2, ListConstructor, [Arg, Tail], TermContext)) }
		;
		    native_hal_read:unexpected_tok(Token, Context,"expected comma, `|', `]', or operator", List)
		)
	    ;
		% XXX error message should state the line that the list started on
		native_hal_read:error("unexpected end-of-file in list", List)
	    )
	;
	    % propagate error
	    { List = Arg0 }
	).

:- pred native_hal_read:parse_args(parse(list(sterm)), pars_state, pars_state).
:- mode native_hal_read:parse_args(out, in, out) is det.

native_hal_read:parse_args(List) -->
	native_hal_read:parse_arg(Arg0),
	(   { Arg0 = ok(Arg) } -> %%% used to be  ->
	    ( native_hal_read:get_token(Token, Context) ->
		( { Token = comma } ->
		    native_hal_read:parse_args(Tail0),
		    ( { Tail0 = ok(Tail) } ->
			{ List = ok([Arg|Tail]) }
		    ;
			% propagate error upwards
		        { List = Tail0 }
		    )
		; { Token = close } ->
		    { List = ok([Arg]) }
		;
		    native_hal_read:unexpected_tok(Token, Context, "expected `,', `)', or operator", List)
		)
	    ;
		native_hal_read:error("unexpected end-of-file in argument list", List)
	    )
	;
	    { Arg0 = error(Message, Tokens) },
	    % propagate error upwards
	    { List = error(Message, Tokens) }
	).

 %-----------------------------------------------------------------------------%
%-----------------------------------------------------------------------------%

	% Routines that manipulate the parser state.


%-----------------------------------------------------------------------------%

	% We encountered an error.  See if the next token
	% was an infix or postfix operator.  If so, it would
	% normally form part of the term, so the error must
	% have been an operator precedence error.  Otherwise,
	% it was some other sort of error, so issue the usual
	% error message.

:- pred native_hal_read:unexpected(string, parse(U),	pars_state, pars_state).
:- mode native_hal_read:unexpected(in, out, in, out) is det.

native_hal_read:unexpected(UsualMessage, Error) -->
	( native_hal_read:get_token(Token, Context) ->
		native_hal_read:unexpected_tok(Token, Context, UsualMessage, Error)
	;
		native_hal_read:error(UsualMessage, Error)
	).

:- pred native_hal_read:unexpected_tok(token, token_context, string, parse(U), pars_state, pars_state).
:- mode native_hal_read:unexpected_tok(in, in, in, out, in, out) is det.

native_hal_read:unexpected_tok(Token, Context, UsualMessage, Error) -->
	% push the token back, so that the error message
	% points at it rather than at the following token
	native_hal_read:unget_token(Token, Context),
	(
	  { Token = name(Op)
	  ; Token = comma, Op = ","
	  },
	  %% 		{ native_hal_read:lookup_infix_op(Op, _, _, _)
	  %% 		; native_hal_read:lookup_postfix_op(Op, _, _)
	  %% 		}
	  { native_hal_read:op_table(Op,after,_,_) }   % this makes it as a whole det I hope
	->
		native_hal_read:error("operator precedence error", Error)
	;
		native_hal_read:error(UsualMessage, Error)
	).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:error(string, parse(U), pars_state, pars_state).
:- mode native_hal_read:error(in, out, in, out) is det.

native_hal_read:error(Message, error(Message, Tokens), ParserState, ParserState) :-
	ParserState = state(_, _, Tokens).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:could_start_term(token, bool).
:- mode native_hal_read:could_start_term(in, out) is det.

native_hal_read:could_start_term(char(_), yes).
native_hal_read:could_start_term(name(_), yes).
native_hal_read:could_start_term(variable(_), yes).
native_hal_read:could_start_term(integer(_), yes).
native_hal_read:could_start_term(float(_), yes).
native_hal_read:could_start_term(string(_), yes).
native_hal_read:could_start_term(open, yes).
native_hal_read:could_start_term(open_ct, yes).
native_hal_read:could_start_term(close, no).
native_hal_read:could_start_term(open_list, yes).
native_hal_read:could_start_term(close_list, no).
native_hal_read:could_start_term(open_curly, yes).
native_hal_read:could_start_term(close_curly, no).
native_hal_read:could_start_term(ht_sep, no).
% native_hal_read:could_start_term(ht_sep, yes).
native_hal_read:could_start_term(comma, no).
native_hal_read:could_start_term(end, no).
native_hal_read:could_start_term(junk(_), no).
native_hal_read:could_start_term(error(_), no).
native_hal_read:could_start_term(io_error(_), no).
native_hal_read:could_start_term(eof, no).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:init_state(string, token_list, pars_state).
:- mode native_hal_read:init_state(in, in, out) is det.

native_hal_read:init_state(FileName, Tokens, ParserState) :-
	compiler_interface:make_empty_dict(VarSet),
	ParserState = state(FileName, VarSet, Tokens).

:- pred native_hal_read:final_state(pars_state, dict, token_list).
:- mode native_hal_read:final_state(in, out, out) is det.

native_hal_read:final_state(state(_FileName, VarSet, TokenList), VarSet, TokenList).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:get_token(token, pars_state, pars_state).
:- mode native_hal_read:get_token(out, in, out) is semidet.

native_hal_read:get_token(Token) -->
	native_hal_read:get_token(Token, _Context).

:- pred native_hal_read:get_token(token, token_context,pars_state, pars_state).
:- mode native_hal_read:get_token(out, out, in, out) is semidet.
:- mode native_hal_read:get_token(in, in, out, in) is det.

native_hal_read:get_token(Token, Context,
		state(FileName, VarSet, Tokens0),
		state(FileName, VarSet, Tokens)) :-
	Tokens0 = token_cons(Token, Context, Tokens).

:- pred native_hal_read:unget_token(token, token_context, pars_state, pars_state).
:- mode native_hal_read:unget_token(in, in, in, out) is det.

native_hal_read:unget_token(Token, Context, ParseState0, ParseState) :-
	native_hal_read:get_token(Token, Context, ParseState, ParseState0).

%---------------------------------------------------------------------------%
:- pred native_hal_read:not_peek_token(token, pars_state, pars_state).
:- mode native_hal_read:not_peek_token(in, in, out) is semidet.
%---------------------------------------------------------------------------%
% not_peek_token/3
%            was needed at some point for circumventing a feature ...
%---------------------------------------------------------------------------%

native_hal_read:not_peek_token(TokenIn,StateIn,StateOut) :-
	native_hal_read:peek_token(TokenOut,StateIn,_),
	(TokenIn = TokenOut ->
	    fail
	;
	    StateOut = StateIn
	).

:- pred native_hal_read:peek_token(token, pars_state, pars_state).
:- mode native_hal_read:peek_token(out, in, out) is semidet.

native_hal_read:peek_token(Token) -->
	native_hal_read:peek_token(Token, _Context).

:- pred native_hal_read:peek_token(token, token_context, pars_state, pars_state).
:- mode native_hal_read:peek_token(out, out, in, out) is semidet.

native_hal_read:peek_token(Token, Context, Pstate, Pstate) :-
	Pstate = state(_, _, Tokens),
	Tokens = token_cons(Token, Context, _).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:add_var(string, var, pars_state, pars_state).
:- mode native_hal_read:add_var(in, out, in, out) is det.

native_hal_read:add_var(VarName, Var, state(FileName, VarSet0, Tokens),	state(FileName, VarSet, Tokens)) :-
	( VarName = "_" ->
	    compiler_interface:add_name(VarName, VarSet0, VarSet, Var)
	;
	    (compiler_interface:get_first_variable_with_name(VarSet0, VarName, V) ->
		Var = V,
		VarSet = VarSet0
	    ;
		compiler_interface:add_name(VarName, VarSet0, VarSet, Var)
	    )
	).

%-----------------------------------------------------------------------------%
%-----------------------------------------------------------------------------%

:- pred native_hal_read:adjust_priority(opsassoc, int, int).
:- mode native_hal_read:adjust_priority(in, in, out) is det.

native_hal_read:adjust_priority(y, Priority, Priority).
native_hal_read:adjust_priority(x, OldPriority, NewPriority) :-
	NewPriority is OldPriority - 1.

:- pred native_hal_read:check_priority(opsassoc, int, int).
:- mode native_hal_read:check_priority(in, in, in) is semidet.

native_hal_read:check_priority(y, MaxPriority, Priority) :-
	Priority =< MaxPriority.
native_hal_read:check_priority(x, MaxPriority, Priority) :-
	Priority < MaxPriority.

:- pred native_hal_read:get_term_context(token_context, term_context, pars_state, pars_state).
:- mode native_hal_read:get_term_context(in, out, in, out) is det.

native_hal_read:get_term_context(TokenContext, TermContext, ParsState, ParsState) :-
	ParsState = state(FileName, _VarSet, _Tokens),
	native_hal_read:context_init(FileName, TokenContext, TermContext).


%%%%%%%%%%%% most of lexer.m

:- pred native_hal_read:get_token_list(token_list, state, state).
:- mode native_hal_read:get_token_list(out, di, uo) is det.
%	Read a list of tokens from the current input stream.
%	Keep reading until we encounter either an `end' token
%	(i.e. a full stop followed by whitespace) or the end-of-file.


:- pred native_hal_read:token_to_string(token, string).
:- mode native_hal_read:token_to_string(in, out) is det.
%	Convert a token to a human-readable string describing the token.

%-----------------------------------------------------------------------------%

%-----------------------------------------------------------------------------%


	% native_hal_read:graphic_token_char(Char): true iff `Char'
	% is "graphic token char" (ISO Prolog 6.4.2).
	% This is exported for use by term_quote_atom.
:- pred native_hal_read:graphic_token_char(int).
:- mode native_hal_read:graphic_token_char(in) is semidet.
%-----------------------------------------------------------------------------%

native_hal_read:token_to_string(char(Int), String) :-
	native_hal_read:my_int_to_string(Int, IntString),
	native_hal_read:list_of_strings_to_string(["token '", IntString, "'"], String).
native_hal_read:token_to_string(name(Name), String) :-
	native_hal_read:list_of_strings_to_string(["token '", Name, "'"], String).
native_hal_read:token_to_string(variable(Var), String) :-
	native_hal_read:list_of_strings_to_string(["variable `", Var, "'"], String).
native_hal_read:token_to_string(integer(Int), String) :-
	native_hal_read:my_int_to_string(Int, IntString),
	native_hal_read:list_of_strings_to_string(["integer `", IntString, "'"], String).
native_hal_read:token_to_string(float(Float), String) :-
	string:float_to_string(Float, FloatString),
	native_hal_read:list_of_strings_to_string(["float `", FloatString, "'"], String).
native_hal_read:token_to_string(string(Token), String) :-
	native_hal_read:list_of_strings_to_string(["string """, Token, """"], String).
native_hal_read:token_to_string(open, "token ` ('").
native_hal_read:token_to_string(open_ct, "token `('").
native_hal_read:token_to_string(close, "token `)'").
native_hal_read:token_to_string(open_list, "token `['").
native_hal_read:token_to_string(close_list, "token `]'").
native_hal_read:token_to_string(open_curly, "token `{'").
native_hal_read:token_to_string(close_curly, "token `}'").
native_hal_read:token_to_string(ht_sep, "token `|'").
native_hal_read:token_to_string(comma, "token `,'").
native_hal_read:token_to_string(end, "token `. '").
native_hal_read:token_to_string(eof, "end-of-file").
native_hal_read:token_to_string(junk(Code), String) :-
	native_hal_read:int_to_base_string(Code, 16, Hex),
	native_hal_read:list_of_strings_to_string(["illegal character <<0x", Hex, ">>"], String).
native_hal_read:token_to_string(io_error(IO_Error), String) :-
	native_hal_read:error_message(IO_Error, IO_ErrorMessage),
	string:append("I/O error: ", IO_ErrorMessage, String).
native_hal_read:token_to_string(error(Message), String) :-
	native_hal_read:list_of_strings_to_string(["illegal token (", Message, ")"], String).

%---------------------------------------------------------------------------%
:- pred native_hal_read:error_message(string,string).
:- mode native_hal_read:error_message(in,out) is det.
%---------------------------------------------------------------------------%
% error_message/2
% 
%---------------------------------------------------------------------------%

native_hal_read:error_message(S,S).


	% We build the tokens up as lists of characters in reverse order.
	% When we get to the end of each token, we call
	% `rev_int_list_to_string/2' to convert that representation
	% into a string.

	% Comments of the form
	%	foo --> bar . baz
	% mean that we are parsing a `foo', and we've already scanned
	% past the `bar', so now we need to match with a `baz'.

native_hal_read:get_token_list(Tokens) -->
	native_hal_read:lex_get_token(Token, Context),
	( { Token = eof } ->
		{ Tokens = token_nil }
	; { Token = end ; Token = error(_) ; Token = io_error(_) } ->
		{ Tokens = token_cons(Token, Context, token_nil) }
	;
		{ Tokens = token_cons(Token, Context, Tokens1) },
		native_hal_read:get_token_list(Tokens1)
	).

%-----------------------------------------------------------------------------%

% some low-level routines

:- pred native_hal_read:get_context(token_context, state, state).
:- mode native_hal_read:get_context(out, di, uo) is det.

native_hal_read:get_context(Context) -->
	(io):get_line_number(Context).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:lex_get_token(token, token_context, state, state).
:- mode native_hal_read:lex_get_token(out, out, di, uo) is det.

native_hal_read:lex_get_token(Token, Context) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		native_hal_read:get_context(Context),
		{ Token = io_error(Error) }
	; { Result = eof },
		native_hal_read:get_context(Context),
		{ Token = eof }
	; { Result = ok(CharCode) },
	    ( { CharCode = 0'  ; CharCode = 0'\t ; CharCode = 0'\n } ->
			native_hal_read:lex_get_token_2(Token, Context)
		; { native_hal_read:is_start_var(CharCode) } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_variable([CharCode], Token)
		; { native_hal_read:is_lower(CharCode) } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_name([CharCode], Token)
		; { CharCode = 0'0 } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_zero(Token)
		; { native_hal_read:is_digit(CharCode) } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_number([CharCode], Token)
		; { native_hal_read:special_token(CharCode, SpecialToken) } ->
		    native_hal_read:get_context(Context),
		    { SpecialToken = open ->
		      Token = open_ct
		    ;
		      Token = SpecialToken
		    }
		; { CharCode = 0'. } ->
		    native_hal_read:get_context(Context),
		    native_hal_read:get_dot(Token)
		; { CharCode = 0'% } ->
		    native_hal_read:skip_to_eol(Token, Context)
		; { CharCode = 0'" ; CharCode = 0'\' } ->
		    native_hal_read:get_context(Context),
		    native_hal_read:get_quoted_name(CharCode, [], Token)
		  ; { CharCode = 0'/ } ->
		    native_hal_read:get_slash(Token, Context)
 		  ; { CharCode = 0'#, fail } ->  % disabled line numbers
		    native_hal_read:get_source_line_number([], Token, Context)
		  ; { CharCode = 0'` } ->
		    native_hal_read:get_context(Context),
		    { Token = name("`") }
		; { native_hal_read:graphic_token_char(CharCode) } ->
		    native_hal_read:get_context(Context),
		    native_hal_read:get_graphic([CharCode], Token)
		  ;
		    native_hal_read:get_context(Context),
		    { Token = junk(CharCode) }
		  )
		).

%---------------------------------------------------------------------------%
:- pred native_hal_read:is_start_var(int).
:- mode native_hal_read:is_start_var(in) is semidet.
%---------------------------------------------------------------------------%
% is_start_var/1
% 
%---------------------------------------------------------------------------%

native_hal_read:is_start_var(CharCode) :-
	(native_hal_read:is_upper(CharCode) ->
	    true
	;
	    CharCode = 0'_
	).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:lex_get_token_2(token, token_context, state, state).
:- mode native_hal_read:lex_get_token_2(out, out, di, uo) is det.

	% This is just like lex_get_token, except that we have already
	% scanned past some whitespace, so '(' gets scanned as `open'
	% rather than `open_ct'.

native_hal_read:lex_get_token_2(Token, Context) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		native_hal_read:get_context(Context),
		{ Token = io_error(Error) }
	; { Result = eof },
		native_hal_read:get_context(Context),
		{ Token = eof }
	; { Result = ok(Char) },
		( { Char = 0'  ; Char = 0'\t ; Char = 0'\n } ->
			native_hal_read:lex_get_token_2(Token, Context)
		; { native_hal_read:is_start_var(Char) } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_variable([Char], Token)
		; { native_hal_read:is_lower(Char) } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_name([Char], Token)
		; { Char = 0'0 } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_zero(Token)
		; { native_hal_read:is_digit(Char) } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_number([Char], Token)
		; { native_hal_read:special_token(Char, SpecialToken) } ->
			native_hal_read:get_context(Context),
			{ Token = SpecialToken }
		; { Char = 0'. } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_dot(Token)
		; { Char = 0'% } ->
			native_hal_read:skip_to_eol(Token, Context)
		; { Char = 0'" ; Char = 0'\' } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_quoted_name(Char, [], Token)
		; { Char = 0'/ } ->
			native_hal_read:get_slash(Token, Context)
		; { Char = 0'#, fail } -> % disabled line numbers
			native_hal_read:get_source_line_number([], Token, Context)
		; { Char = 0'` } ->
			native_hal_read:get_context(Context),
			{ Token = name("`") }
		; { native_hal_read:graphic_token_char(Char) } ->
			native_hal_read:get_context(Context),
			native_hal_read:get_graphic([Char], Token)
		;
			native_hal_read:get_context(Context),
			{ Token = junk(Char) }
		)
	).

%-----------------------------------------------------------------------------%
:- pred native_hal_read:special_token(int, token).
:- mode native_hal_read:special_token(in, out) is semidet.

native_hal_read:special_token(0'(, open).	% May get converted to open_ct
native_hal_read:special_token(0'), close).
native_hal_read:special_token(0'[, open_list).
native_hal_read:special_token(0'], close_list).
native_hal_read:special_token(0'{, open_curly).
native_hal_read:special_token(0'}, close_curly).
native_hal_read:special_token(0'|, name("|")).
%% native_hal_read:special_token(0'|, ht_sep).
native_hal_read:special_token(0',, comma).
native_hal_read:special_token(0';, name(";")).
native_hal_read:special_token(0'!, name("!")).  % change for cut

% native_hal_read:graphic_token_char(0'!). % change for cut
native_hal_read:graphic_token_char(0'#).
native_hal_read:graphic_token_char(0'$).
native_hal_read:graphic_token_char(0'&).
native_hal_read:graphic_token_char(0'*).
native_hal_read:graphic_token_char(0'+).
native_hal_read:graphic_token_char(0'-).
native_hal_read:graphic_token_char(0'.).
native_hal_read:graphic_token_char(0'/).
native_hal_read:graphic_token_char(0':).
native_hal_read:graphic_token_char(0'<).
native_hal_read:graphic_token_char(0'=).
native_hal_read:graphic_token_char(0'>).
native_hal_read:graphic_token_char(0'?).
native_hal_read:graphic_token_char(0'@).
native_hal_read:graphic_token_char(0'^).
native_hal_read:graphic_token_char(0'~).
native_hal_read:graphic_token_char(0'\\).

%-----------------------------------------------------------------------------%

:- pred native_hal_read:get_dot(token, state, state).
:- mode native_hal_read:get_dot(out, di, uo) is det.

native_hal_read:get_dot(Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = end }
	; { Result = ok(Char) },
		( { native_hal_read:whitespace_after_dot(Char) } ->
			 ({ Char == 0'% } ->
			     (io):putback_char(Char)
			 ;
			     { true }
			 ),
			 { Token = end }
		; { native_hal_read:graphic_token_char(Char) } ->
		        {DummyEl = 0'. },
			native_hal_read:get_graphic([Char, DummyEl], Token)
		;
			(io):putback_char(Char),
			{ Token = name(".") }  % do not replace by "[|]"
		)
	).


:- pred native_hal_read:whitespace_after_dot(int).
:- mode native_hal_read:whitespace_after_dot(in) is semidet.

native_hal_read:whitespace_after_dot(0' ).
native_hal_read:whitespace_after_dot(0'\t).
native_hal_read:whitespace_after_dot(0'\n).
native_hal_read:whitespace_after_dot(0'%).

%-----------------------------------------------------------------------------%

	% comments

:- pred native_hal_read:skip_to_eol(token, token_context, state, state).
:- mode native_hal_read:skip_to_eol(out, out, di, uo) is det.

native_hal_read:skip_to_eol(Token, Context) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		native_hal_read:get_context(Context),
		{ Token = io_error(Error) }
	; { Result = eof },
		native_hal_read:get_context(Context),
		{ Token = eof }
	; { Result = ok(Char) },
		( { Char = 0'\n } ->
			native_hal_read:lex_get_token_2(Token, Context)
		;
			native_hal_read:skip_to_eol(Token, Context)
		)
	).

:- pred native_hal_read:get_slash(token, token_context, state, state).
:- mode native_hal_read:get_slash(out, out, di, uo) is det.

native_hal_read:get_slash(Token, Context) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		native_hal_read:get_context(Context),
		{ Token = io_error(Error) }
	; { Result = eof },
		native_hal_read:get_context(Context),
		{ Token = name("/") }
	; { Result = ok(Char) },
		( { Char = 0'* } ->
			native_hal_read:get_comment(Token, Context)
		; { native_hal_read:graphic_token_char(Char) } ->
			native_hal_read:get_context(Context),
		        {DummyEl = 0'/}, 
			native_hal_read:get_graphic([Char, DummyEl], Token)
		;
			(io):putback_char(Char),
			native_hal_read:get_context(Context),
			{ Token = name("/") }
		)
	).


:- pred native_hal_read:get_comment(token, token_context, state, state).
:- mode native_hal_read:get_comment(out, out, di, uo) is det.

native_hal_read:get_comment(Token, Context) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		native_hal_read:get_context(Context),
		{ Token = io_error(Error) }
	; { Result = eof },
		native_hal_read:get_context(Context),
		{ Token = error("unterminated '/*' comment") }
	; { Result = ok(Char) },
		( { Char = 0'* } ->
			native_hal_read:get_comment_2(Token, Context)
		;
			native_hal_read:get_comment(Token, Context)
		)
	).


:- pred native_hal_read:get_comment_2(token, token_context, state, state).
:- mode native_hal_read:get_comment_2(out, out, di, uo) is det.

native_hal_read:get_comment_2(Token, Context) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		native_hal_read:get_context(Context),
		{ Token = io_error(Error) }
	; { Result = eof },
		native_hal_read:get_context(Context),
		{ Token = error("unterminated '/*' comment") }
	; { Result = ok(Char) },
		( { Char = 0'/ } ->
			% end of /* ... */ comment, so get next token
			native_hal_read:lex_get_token_2(Token, Context)
		; { Char = 0'* } ->
			native_hal_read:get_comment_2(Token, Context)
		;
			native_hal_read:get_comment(Token, Context)
		)
	).

%-----------------------------------------------------------------------------%

	% quoted names and quoted strings

:- pred native_hal_read:get_quoted_name(int, list(int), token,	state, state).
:- mode native_hal_read:get_quoted_name(in, in, out, di, uo) is det.

native_hal_read:get_quoted_name(QuoteChar, Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated quote") }
	; { Result = ok(Char) },
		( { Char = QuoteChar } ->
			native_hal_read:get_quoted_name_quote(QuoteChar, Chars, Token)
		; { Char = 0'\\ } ->
			native_hal_read:get_quoted_name_escape(QuoteChar, Chars, Token)
		;
			native_hal_read:get_quoted_name(QuoteChar, [Char | Chars], Token)
		)
	).


:- pred native_hal_read:get_quoted_name_quote(int, list(int), token, state, state).
:- mode native_hal_read:get_quoted_name_quote(in, in, out, di, uo) is det.

native_hal_read:get_quoted_name_quote(QuoteChar, Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ native_hal_read:finish_quoted_name(QuoteChar, Chars, Token) }
	; { Result = ok(Char) },
		( { Char = QuoteChar } ->
			native_hal_read:get_quoted_name(QuoteChar, [Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ native_hal_read:finish_quoted_name(QuoteChar, Chars, Token) }
		)
	).


:- pred native_hal_read:finish_quoted_name(int, list(int), token).
:- mode native_hal_read:finish_quoted_name(in, in, out) is det.

native_hal_read:finish_quoted_name(QuoteChar, Chars, Token) :-
	(Chars = [I], QuoteChar = 0'\' -> %% , fail ->  %% fail prevents char
	    Token = char(I)
	;
	    string:rev_int_list_to_string(Chars, String),
	    ( QuoteChar = 0'" ->
		Token = string(String)
	    ;
		Token = name(String)
	    )
	).

:- pred native_hal_read:get_quoted_name_escape(int, list(int), token, state, state).
:- mode native_hal_read:get_quoted_name_escape(in, in, out, di, uo) is det.

native_hal_read:get_quoted_name_escape(QuoteChar, Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated quoted name") }
	; { Result = ok(Char) },
		( { Char = 0'\n } ->
			native_hal_read:get_quoted_name(QuoteChar, Chars, Token)
		; { native_hal_read:escape_char(Char, EscapedChar) } ->
			{ Chars1 = [EscapedChar | Chars] },
			native_hal_read:get_quoted_name(QuoteChar, Chars1, Token)
		; { Char = 0'x } ->
			native_hal_read:get_hex_escape(QuoteChar, Chars, [], Token)
		; { native_hal_read:is_octal_digit(Char) } ->
			native_hal_read:get_octal_escape(QuoteChar, Chars, [Char],
				Token)
		;
			{ Token = error("invalid escape character") }
		)
	).

:- pred native_hal_read:escape_char(int,int).
:- mode native_hal_read:escape_char(in, out) is semidet.

native_hal_read:escape_char(0'a, 7). % 0'\a).
native_hal_read:escape_char(0'b, 8). % 0'\b).
native_hal_read:escape_char(0'r, 13). % 0'\r).
native_hal_read:escape_char(0'f, 12). % 0'\f).
native_hal_read:escape_char(0't, 9). % 0'\t).
native_hal_read:escape_char(0'n, 10). % 0'\n).
native_hal_read:escape_char(0'v, 11). % 0'\v).
native_hal_read:escape_char(0'\\, 0'\\).
native_hal_read:escape_char(0'\', 0'\').
native_hal_read:escape_char(0'", 0'").
native_hal_read:escape_char(0'`, 0'`).

:- pred native_hal_read:get_hex_escape(int, list(int), list(int), token, state, state).
:- mode native_hal_read:get_hex_escape(in, in, in, out, di, uo) is det.

native_hal_read:get_hex_escape(QuoteChar, Chars, HexChars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated quote") }
	; { Result = ok(Char) },
		( { native_hal_read:is_hex_digit(Char) } ->
		    native_hal_read:get_hex_escape(QuoteChar, Chars, [Char | HexChars], Token)
		; { Char = 0'\\ } ->
		    native_hal_read:finish_hex_escape(QuoteChar, Chars, HexChars, Token)
		;
		    { Token = error("unterminated hex escape") }
		)
	).

:- pred native_hal_read:finish_hex_escape(int, list(int), list(int), token, state, state).
:- mode native_hal_read:finish_hex_escape(in, in, in, out, di, uo) is det.

native_hal_read:finish_hex_escape(QuoteChar, Chars, HexChars, Token) -->
	( { HexChars = [] } ->
	    { Token = error("empty hex escape") }
	;
	    (
	      { native_hal_read:rev_base_int_list_to_int(16, HexChars, IntToken) },
	      { IntToken = integer(Char)}
	    ->
	      native_hal_read:get_quoted_name(QuoteChar, [Char|Chars], Token) 
	    ;
	      { Token = error("invalid hex escape") }
	    )
	).


:- pred native_hal_read:get_octal_escape(int, list(int), list(int), token, state, state).
:- mode native_hal_read:get_octal_escape(in, in, in, out, di, uo) is det.

native_hal_read:get_octal_escape(QuoteChar, Chars, OctalChars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated quote") }
	; { Result = ok(Char) },
		( { native_hal_read:is_octal_digit(Char) } ->
			native_hal_read:get_octal_escape(QuoteChar, Chars,
						[Char | OctalChars], Token)
		; { Char = 0'\\ } ->
			native_hal_read:finish_octal_escape(QuoteChar, Chars, OctalChars,
				Token)
		;
			/****** 
				% We don't report this as an error since
				% we need bug-for-bug compatibility with
				% NU-Prolog
			{ Token = error("unterminated octal escape") }
			******/
			(io):putback_char(Char),
			native_hal_read:finish_octal_escape(QuoteChar, Chars, OctalChars,
				Token)
		)
	).


:- pred native_hal_read:finish_octal_escape(int, list(int), list(int), token, state, state).
:- mode native_hal_read:finish_octal_escape(in, in, in, out, di, uo) is det.

native_hal_read:finish_octal_escape(QuoteChar, Chars, OctalChars, Token) -->
	( { OctalChars = [] } ->
		{ Token = error("empty octal escape") }
	;
		(
			{ native_hal_read:rev_base_int_list_to_int(8, OctalChars, IntToken) },
		        { IntToken = integer(Char) }
		->
			native_hal_read:get_quoted_name(QuoteChar, [Char|Chars], Token) 
		;
			{ Token = error("invalid octal escape") }
		)
	).


%-----------------------------------------------------------------------------%

	% names and variables

:- pred native_hal_read:get_name(list(int), token, state, state).
:- mode native_hal_read:get_name(in, out, di, uo) is det.

native_hal_read:get_name(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ string:rev_int_list_to_string(Chars, Name) },
		{ Token = name(Name) }
	; { Result = ok(Char) },
		( { native_hal_read:is_alnum_or_underscore(Char) } ->
			native_hal_read:get_name([Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ string:rev_int_list_to_string(Chars, Name) },
			{ Token = name(Name) }
		)
	).

	%
	% A line number directive token is `#' followed by an integer
	% (specifying the line number) followed by a newline.
	% Such a token sets the source line number for the next
	% line, but it is otherwise ignored.  This means that line number
	% directives may appear anywhere that a token may appear, including
	% in the middle of terms.
	% (The source file name can be set with a `:- pragma source_file' 
	% declaration.)
	%

:- pred native_hal_read:get_source_line_number(list(int), token, token_context, state, state).
:- mode native_hal_read:get_source_line_number(in, out, out, di, uo) is det.

native_hal_read:get_source_line_number(Chars, Token, Context) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
	    native_hal_read:get_context(Context),
	    { Token = io_error(Error) }
	; { Result = eof },
	    native_hal_read:get_context(Context),
	    { Token = error("unexpected end-of-file in `#' line number directive") }
	; { Result = ok(Char) },
	    ( { native_hal_read:is_digit(Char) } ->
		native_hal_read:get_source_line_number([Char | Chars], Token, Context)
	    ; { Char = 0'\n } ->
		(
		  { native_hal_read:rev_base_int_list_to_int(10, Chars, IntToken) },
		  {IntToken = integer(Int) }, 
		  { Int > 0 }
		->
		  (io):set_line_number(Int),
		  native_hal_read:lex_get_token(Token, Context)
		;
		  native_hal_read:get_context(Context),
		  { string:rev_int_list_to_string(Chars, String) },
		  { native_hal_read:list_of_strings_to_string([
				 "invalid line number `", String,
				 "' in `#' line number directive"],
				Message) },
		  { Token = error(Message) }
		)
	    ;
		native_hal_read:get_context(Context),
		{ string:int_list_to_string([Char], String) },
		{ native_hal_read:list_of_strings_to_string([
			       "invalid character `", String,
			       "' in `#' line number directive"],
			      Message) },
		{ Token = error(Message) }
	    )
	).


:- pred native_hal_read:get_graphic(list(int), token, state, state).
:- mode native_hal_read:get_graphic(in, out, di, uo) is det.

native_hal_read:get_graphic(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ string:rev_int_list_to_string(Chars, Name) },
		{ Token = name(Name) }
	; { Result = ok(Char) },
		( { native_hal_read:graphic_token_char(Char) } ->
			native_hal_read:get_graphic([Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ string:rev_int_list_to_string(Chars, Name) },
			{ Token = name(Name) }
		)
	).


:- pred native_hal_read:get_variable(list(int), token, state, state).
:- mode native_hal_read:get_variable(in, out, di, uo) is det.

native_hal_read:get_variable(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ string:rev_int_list_to_string(Chars, VariableName) },
		{ Token = variable(VariableName) }
	; { Result = ok(Char) },
		( { native_hal_read:is_alnum_or_underscore(Char) } ->
			native_hal_read:get_variable([Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ string:rev_int_list_to_string(Chars, VariableName) },
			{ Token = variable(VariableName) }
		)
	).

%-----------------------------------------------------------------------------%

	% integer and float literals

:- pred native_hal_read:get_zero(token, state, state).
:- mode native_hal_read:get_zero(out, di, uo) is det.

native_hal_read:get_zero(Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = integer(0) }
	; { Result = ok(Char) },
		( { native_hal_read:is_digit(Char) } ->
			native_hal_read:get_number([Char], Token)
		; { Char = 0'\' } ->
			native_hal_read:get_char_code(Token)
		; { Char = 0'b } ->
			native_hal_read:get_binary(Token)
		; { Char = 0'o } ->
			native_hal_read:get_octal(Token)
		; { Char = 0'x } ->
			native_hal_read:get_hex(Token)
		; { Char = 0'. } ->
		    {DummyTail = [] },
			native_hal_read:get_int_dot([0'0|DummyTail], Token)
		; { Char = 0'e ; Char = 0'E } ->
		        {DummyEl = 0'0} ,
			native_hal_read:get_float_exponent([Char, DummyEl], Token)
		;
			(io):putback_char(Char),
			{ Token = integer(0) }
		)
	).


:- pred native_hal_read:get_char_code(token, state, state).
:- mode native_hal_read:get_char_code(out, di, uo) is det.

native_hal_read:get_char_code(Token) -->
 	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated char code constant") }
	; { Result = ok(CharCode) },
	        ({ CharCode = 0'\\ } ->
		    native_hal_read:get_char_code_escape(Token)
		;
		    { Token = integer(CharCode) }
		)
	).

:- pred native_hal_read:get_char_code_escape(token, state, state).
:- mode native_hal_read:get_char_code_escape(out, di, uo) is det.

native_hal_read:get_char_code_escape(Token) -->
 	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated char code constant") }
	; { Result = ok(CharCode1) },
	        ({ native_hal_read:escape_char(CharCode1,CharCode) } ->
		    { Token = integer(CharCode) }
		;
		    { Token = integer(CharCode1) }
		)
	).

:- pred native_hal_read:is_binary_digit(int).
:- mode native_hal_read:is_binary_digit(in) is semidet.
        % True iff the character is a binary digit (0 or 1).
native_hal_read:is_binary_digit(0'0).
native_hal_read:is_binary_digit(0'1).

:- pred native_hal_read:is_octal_digit(int).
:- mode native_hal_read:is_octal_digit(in) is semidet.
native_hal_read:is_octal_digit(0'0).
native_hal_read:is_octal_digit(0'1).
native_hal_read:is_octal_digit(0'2).
native_hal_read:is_octal_digit(0'3).
native_hal_read:is_octal_digit(0'4).
native_hal_read:is_octal_digit(0'5).
native_hal_read:is_octal_digit(0'6).
native_hal_read:is_octal_digit(0'7).

:- pred native_hal_read:is_hex_digit(int).
:- mode native_hal_read:is_hex_digit(in) is semidet.
native_hal_read:is_hex_digit(0'0).
native_hal_read:is_hex_digit(0'1).
native_hal_read:is_hex_digit(0'2).
native_hal_read:is_hex_digit(0'3).
native_hal_read:is_hex_digit(0'4).
native_hal_read:is_hex_digit(0'5).
native_hal_read:is_hex_digit(0'6).
native_hal_read:is_hex_digit(0'7).
native_hal_read:is_hex_digit(0'8).
native_hal_read:is_hex_digit(0'9).
native_hal_read:is_hex_digit(0'a).
native_hal_read:is_hex_digit(0'b).
native_hal_read:is_hex_digit(0'c).
native_hal_read:is_hex_digit(0'd).
native_hal_read:is_hex_digit(0'e).
native_hal_read:is_hex_digit(0'f).
native_hal_read:is_hex_digit(0'A).
native_hal_read:is_hex_digit(0'B).
native_hal_read:is_hex_digit(0'C).
native_hal_read:is_hex_digit(0'D).
native_hal_read:is_hex_digit(0'E).
native_hal_read:is_hex_digit(0'F).

:- pred native_hal_read:get_binary(token, state, state).
:- mode native_hal_read:get_binary(out, di, uo) is det.

native_hal_read:get_binary(Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated binary constant") }
	; { Result = ok(Char) },
		( { native_hal_read:is_binary_digit(Char) } ->
			native_hal_read:get_binary_2([Char], Token)
		;
			(io):putback_char(Char),
			{ Token = error("unterminated binary constant") }
		)
	).

:- pred native_hal_read:get_binary_2(list(int), token, state, state).
:- mode native_hal_read:get_binary_2(in, out, di, uo) is det.

native_hal_read:get_binary_2(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ native_hal_read:rev_base_int_list_to_int(2, Chars, Token) }
	; { Result = ok(Char) },
		( { native_hal_read:is_binary_digit(Char) } ->
			native_hal_read:get_binary_2([Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ native_hal_read:rev_base_int_list_to_int(2, Chars, Token) }
		)
	).

:- pred native_hal_read:get_octal(token, state, state).
:- mode native_hal_read:get_octal(out, di, uo) is det.

native_hal_read:get_octal(Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated octal constant") }
	; { Result = ok(Char) },
		( { native_hal_read:is_octal_digit(Char) } ->
			native_hal_read:get_octal_2([Char], Token)
		;
			(io):putback_char(Char),
			{ Token = error("unterminated octal constant") }
		)
	).

:- pred native_hal_read:get_octal_2(list(int), token, state, state).
:- mode native_hal_read:get_octal_2(in, out, di, uo) is det.

native_hal_read:get_octal_2(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ native_hal_read:rev_base_int_list_to_int(8, Chars, Token) }
	; { Result = ok(Char) },
		( { native_hal_read:is_octal_digit(Char) } ->
			native_hal_read:get_octal_2([Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ native_hal_read:rev_base_int_list_to_int(8, Chars, Token) }
		)
	).

:- pred native_hal_read:get_hex(token, state, state).
:- mode native_hal_read:get_hex(out, di, uo) is det.

native_hal_read:get_hex(Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ Token = error("unterminated hex constant") }
	; { Result = ok(Char) },
		( { native_hal_read:is_hex_digit(Char) } ->
			native_hal_read:get_hex_2([Char], Token)
		;
			(io):putback_char(Char),
			{ Token = error("unterminated hex constant") }
		)
	).

:- pred native_hal_read:get_hex_2(list(int), token, state, state).
:- mode native_hal_read:get_hex_2(in, out, di, uo) is det.

native_hal_read:get_hex_2(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ native_hal_read:rev_base_int_list_to_int(16, Chars, Token) }
	; { Result = ok(Char) },
		( { native_hal_read:is_hex_digit(Char) } ->
			native_hal_read:get_hex_2([Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ native_hal_read:rev_base_int_list_to_int(16, Chars, Token) }
		)
	).

:- pred native_hal_read:get_number(list(int), token, state, state).
:- mode native_hal_read:get_number(in, out, di, uo) is det.

native_hal_read:get_number(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ native_hal_read:rev_base_int_list_to_int(10, Chars, Token) }
	; { Result = ok(Char) },
		( { native_hal_read:is_digit(Char) } ->
			native_hal_read:get_number([Char | Chars], Token)
		; { Char = 0'. } ->
			native_hal_read:get_int_dot(Chars, Token)
		; { Char = 0'e ; Char = 0'E } ->
			native_hal_read:get_float_exponent([Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ native_hal_read:rev_base_int_list_to_int(10, Chars, Token) }
		)
	).

:- pred native_hal_read:get_int_dot(list(int), token, state, state).
:- mode native_hal_read:get_int_dot(in, out, di, uo) is det.

native_hal_read:get_int_dot(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		(io):putback_char(0'.),
		{ native_hal_read:rev_base_int_list_to_int(10, Chars, Token) }
	; { Result = ok(Char) },
		( { native_hal_read:is_digit(Char) } ->
			native_hal_read:get_float_decimals([Char, 0'. | Chars], Token)
		;
			(io):putback_char(Char),
			(io):putback_char(0'.),
			{ native_hal_read:rev_base_int_list_to_int(10, Chars, Token) }
		)
	).

:- pred native_hal_read:get_float_decimals(list(int), token, state, state).
:- mode native_hal_read:get_float_decimals(in, out, di, uo) is det.

	% we've read past the decimal point, so now get the decimals

native_hal_read:get_float_decimals(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
		{ Token = io_error(Error) }
	; { Result = eof },
		{ native_hal_read:rev_int_list_to_float(Chars, Token) }
	; { Result = ok(Char) },
		( { native_hal_read:is_digit(Char) } ->
			native_hal_read:get_float_decimals([Char | Chars], Token)
		; { Char = 0'e ; Char = 0'E } ->
			native_hal_read:get_float_exponent([Char | Chars], Token)
		;
			(io):putback_char(Char),
			{ native_hal_read:rev_int_list_to_float(Chars, Token) }
		)
	).


:- pred native_hal_read:get_float_exponent(list(int), token, state, state).
:- mode native_hal_read:get_float_exponent(in, out, di, uo) is det.

native_hal_read:get_float_exponent(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
	    { Token = io_error(Error) }
	; { Result = eof },
	    { native_hal_read:rev_int_list_to_float(Chars, Token) }
	; { Result = ok(Char) },
	    ( { Char = 0'+ ; Char = 0'- } ->
		native_hal_read:get_float_exponent_2([Char | Chars], Token)
	    ; { native_hal_read:is_digit(Char) } ->
		native_hal_read:get_float_exponent_3([Char | Chars], Token)
	    ;
		(io):putback_char(Char),
		{ Token = error("unterminated exponent in float token") }
	    )
	).


:- pred native_hal_read:get_float_exponent_2(list(int), token, state, state).
:- mode native_hal_read:get_float_exponent_2(in, out, di, uo) is det.

	% we've read past the E signalling the start of the exponent -
	% make sure that there's at least one digit following,
	% and then get the remaining digits

native_hal_read:get_float_exponent_2(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
	    { Token = io_error(Error) }
	; { Result = eof },
	    { Token = error("unterminated exponent in float token") }
	; { Result = ok(Char) },
	    ( { native_hal_read:is_digit(Char) } ->
		native_hal_read:get_float_exponent_3([Char | Chars], Token)
	    ;
		(io):putback_char(Char),
		{ Token = error("unterminated exponent in float token") }
	    )
	).

:- pred native_hal_read:get_float_exponent_3(list(int), token, state, state).
:- mode native_hal_read:get_float_exponent_3(in, out, di, uo) is det.

	% we've read past the first digit of the exponent -
	% now get the remaining digits

native_hal_read:get_float_exponent_3(Chars, Token) -->
	(io):read_char_code(Result),
	( { Result = error(Error) },
	    { Token = io_error(Error) }
	; { Result = eof },
	    { native_hal_read:rev_int_list_to_float(Chars, Token) }
	; { Result = ok(Char) },
	    ( { native_hal_read:is_digit(Char) } ->
		native_hal_read:get_float_exponent_3([Char | Chars], Token)
	    ;
		(io):putback_char(Char),
		{ native_hal_read:rev_int_list_to_float(Chars, Token) }
	    )
	).

%-----------------------------------------------------------------------------%

	% Utility routines

:- pred native_hal_read:rev_base_int_list_to_int(int, list(int), token).
:- mode native_hal_read:rev_base_int_list_to_int(in, in, out) is det.

native_hal_read:rev_base_int_list_to_int(Base, RevChars, Token) :-
	(native_hal_read:rev_base_int_list_to_int(RevChars, Base, 0, Int) ->
	    Token = integer(Int)
	;
	    Token = error("invalid integer token")
	).

%---------------------------------------------------------------------------%
:- pred native_hal_read:rev_base_int_list_to_int(list(int), int, int, int).
:- mode native_hal_read:rev_base_int_list_to_int(in, in, in, out) is semidet.
%---------------------------------------------------------------------------%
% rev_base_int_list_to_int/4
% 
%---------------------------------------------------------------------------%

native_hal_read:rev_base_int_list_to_int([],_,Acc,Acc).
native_hal_read:rev_base_int_list_to_int([C|Cs], Base, Acc, OutInt) :-
	native_hal_read:rev_base_int_list_to_int(Cs, Base, Acc, NewAcc),
	native_hal_read:int_to_digit(I,C),
	OutInt is NewAcc*Base + I.

%---------------------------------------------------------------------------%
:- pred native_hal_read:rev_int_list_to_float(list(int), token).
:- mode native_hal_read:rev_int_list_to_float(in, out) is det.
%---------------------------------------------------------------------------%
% rev_int_list_to_float/2
% provided by lowlevel
%---------------------------------------------------------------------------%

native_hal_read:rev_int_list_to_float(L,FloatToken) :-
	(string:rev_int_list_to_float_lowlevel(L,Float) ->
	    FloatToken = float(Float)
	;
	    FloatToken = error("bad float")
	).

%%%%%%%%%%%% most of prog_io_dcg.m - adapted 

%-----------------------------------------------------------------------------%
:- pred native_hal_read:hal_term_dcg_transfo(hal_read_term::in, hal_read_term::out) is det.
%-----------------------------------------------------------------------------%
% hal_term_dcg_transfo/2
%
%-----------------------------------------------------------------------------%

native_hal_read:hal_term_dcg_transfo(InTerm,OutTerm) :-
	( InTerm = hal_read_term(Dict,Term),
	    Term = functor(2, "-->", [DCG_H, DCG_B], DCG_Context),
	    native_hal_read:head_body_dcg_transfo(Dict, DCG_H, DCG_B, DCG_Context, OutTerm1) ->
	    OutTerm = OutTerm1
	;
	    OutTerm = InTerm
	).

%---------------------------------------------------------------------------%
:- pred native_hal_read:head_body_dcg_transfo(dict, sterm, sterm, term_context,hal_read_term).
:- mode native_hal_read:head_body_dcg_transfo(in,in,in,in,out) is semidet.
%---------------------------------------------------------------------------%
% head_body_dcg_transfo/5
% 
%---------------------------------------------------------------------------%

native_hal_read:head_body_dcg_transfo(VarSet0, DCG_Head, DCG_Body,DCG_Context,OutTerm) :-
	native_hal_read:sym_name_args_module(DCG_Head,HeadSym,Args1,Module),
	native_hal_read:new_dcg_var(VarSet0, 0, VarSet1, N0, DCG_0_Var),
	native_hal_read:parse_dcg_goal(DCG_Body, VarSet1, N0, DCG_0_Var, NewBody, VarSet, _N, DCG_Var),
	native_hal_read:my_app(Args1, [var(DCG_0_Var,DCG_Context),var(DCG_Var,DCG_Context)], Args),
	native_hal_read:my_len(Args,Arity),
	NewHead1 = functor(Arity,HeadSym,Args,DCG_Context),
	(Module == "empty module name :-(" ->
	    NewHead = NewHead1
	;
	    ModuleQual = atom(Module,DCG_Context),
	    NewHead = functor(2, ":",[ModuleQual,NewHead1],DCG_Context)
	),
	OutTerm = hal_read_term(VarSet,functor(2, ":-",[NewHead,NewBody],DCG_Context)).



%-----------------------------------------------------------------------------%

	% Used to allocate fresh variables needed for the DCG expansion.

:- pred native_hal_read:new_dcg_var(dict, int, dict, int, int).
:- mode native_hal_read:new_dcg_var(in, in, out, out, out) is det.

native_hal_read:new_dcg_var(VarSet0, N0, VarSet, N, DCG_0_Var) :-
	native_hal_read:my_int_to_string(N0, StringN),
	string:append("_DCG_", StringN, VarName),
	compiler_interface:add_name(VarName, VarSet0, VarSet, DCG_0_Var), % B? must be improved to check ...
	N is N0 + 1.

%-----------------------------------------------------------------------------%

	% Expand a DCG goal.

%---------------------------------------------------------------------------%
:- pred native_hal_read:get_context(sterm, term_context).
:- mode native_hal_read:get_context(in,out) is det.
%---------------------------------------------------------------------------%
% get_context/2
% 
%---------------------------------------------------------------------------%
native_hal_read:get_context(functor(_, _, _, Context), Context).
native_hal_read:get_context(var( _, Context), Context).
native_hal_read:get_context(integer( _, Context), Context).
native_hal_read:get_context(float( _, Context), Context).
native_hal_read:get_context(string( _, Context), Context).
native_hal_read:get_context(char( _, Context), Context).
native_hal_read:get_context(atom( _, Context), Context).

:- pred native_hal_read:parse_dcg_goal(sterm, dict, int, int, sterm, dict, int, int).
:- mode native_hal_read:parse_dcg_goal(in, in, in, in, out, out, out, out) is det.

native_hal_read:parse_dcg_goal(Term, VarSet0, N0, Var0, TermOut, VarSet, N, Var) :-
				% first, figure out the context for the goal
	native_hal_read:get_context(Term,Context),
				% next, parse it
	% special case for the Prolog cut - should not affect any HAL code
	(Term = atom("!",_) ->
	    % change 30-3-2007
	    TermOut = functor(2, ",",[Term,Unif], Context),
	    Unif = functor(2, "=", [var(Var0,Context), var(Var,Context)],Context),
	    native_hal_read:new_dcg_var(VarSet0, N0, VarSet, N, Var)
	;
	    ( native_hal_read:sym_name_args_module(Term, SymName, Args0,Module) ->
				% First check for the special cases:
		( (Module = "empty module name :-(",
		      native_hal_read:parse_dcg_goal_2(SymName, Args0, Context, VarSet0, N0, Var0, Goal1, VarSet1, N1, Var1)) ->
		    TermOut = Goal1,
		    VarSet = VarSet1,
		    N = N1,
		    Var = Var1
		;
		    native_hal_read:new_dcg_var(VarSet0, N0, VarSet, N, Var),
		    native_hal_read:my_app(Args0,[var(Var0,Context),var(Var,Context)], Args),
		    native_hal_read:my_len(Args,Arity),
		    TermOut1 = functor(Arity, SymName,Args,Context),
		    (Module = "empty module name :-(" ->
			TermOut = TermOut1
		    ;
			TermOut = functor(2, ":", [atom(Module,Context),TermOut1],Context)
		    )
		)
	    ;
				% A call to a free variable, or to a number or string.
				% Just translate it into a call to call/3 - the typechecker
				% will catch calls to numbers and strings.
		native_hal_read:new_dcg_var(VarSet0, N0, VarSet, N, Var),
		TermOut = functor(3, "call", [Term, var(Var0,Context), var(Var,Context)],Context)
	    )
	).

	% native_hal_read:parse_dcg_goal_2(Functor, Args, Context, VarSet0, N0, Var0,
	%			Goal, VarSet, N, Var):
	% VarSet0/VarSet are an accumulator pair which we use to
	% allocate fresh DCG variables; N0 and N are an accumulator pair
	% we use to keep track of the number to give to the next DCG
	% variable (so that we can give it a semi-meaningful name "DCG_<N>"
	% for use in error messages, debugging, etc.).
	% Var0 and Var are an accumulator pair we use to keep track of
	% the current DCG variable.

:- pred native_hal_read:parse_dcg_goal_2(string, list(sterm), term_context, dict,
		int, int, sterm, dict, int, int).
:- mode native_hal_read:parse_dcg_goal_2(in, in, in, in, in, in, out, out, out, out) is semidet.

	% Ordinary goal inside { curly braces }.

%% native_hal_read:parse_dcg_goal_2("{}", [G0 | Gs], Context, VarSet, N, Var, OutTerm, VarSet, N, Var) :-
%% 	% The parser treats '{}/N' terms as tuples (*), so we need
%% 	% to undo the parsing of the argument conjunction here.
%% 	native_hal_read:list_to_conjunction(Gs, Context, G0, OutTerm).
	% this (*) was undone by Bart Demoen Monash Tue Nov 29 02:51:28 CET 2005
	% so now I must undo this too ...
native_hal_read:parse_dcg_goal_2("{}", [OutTerm], _Context, VarSet, N, Var, OutTerm, VarSet, N, Var).


	

% Empty list - just unify the input and output DCG args.
native_hal_read:parse_dcg_goal_2("[]", [], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var) :-
	native_hal_read:new_dcg_var(VarSet0, N0, VarSet, N, Var),
	OutTerm = functor(2, "=", [var(Var0,Context), var(Var,Context)],Context).

	% Call to '='/1 - unify argument with DCG input arg.
native_hal_read:parse_dcg_goal_2("=", [A0], Context, VarSet, N, Var, OutTerm, VarSet, N, Var) :-
	OutTerm = functor(2, "=", [A0, var(Var,Context)],Context).

	% Call to ':='/1 - unify argument with DCG output arg.
native_hal_read:parse_dcg_goal_2(":=", [A0], Context, VarSet0, N0, _Var0, OutTerm, VarSet, N, Var) :-
	native_hal_read:new_dcg_var(VarSet0, N0, VarSet, N, Var),
	OutTerm = functor(2, "=",[A0, var(Var,Context)],Context).

native_hal_read:parse_dcg_goal_2("impure", [G], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var) :-
        native_hal_read:parse_dcg_goal(G, VarSet0, N0, Var0, NewG, VarSet, N, Var),
	OutTerm = functor(1, "impure",[NewG],Context).
	
native_hal_read:parse_dcg_goal_2("semipure", [G], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var) :-
        native_hal_read:parse_dcg_goal(G, VarSet0, N0, Var0, NewG, VarSet, N, Var),
	OutTerm = functor(1, "semipure",[NewG],Context).
	

	% Non-empty list of terminals.  Append the DCG output arg
	% as the new tail of the list, and unify the result with
	% the DCG input arg.
native_hal_read:parse_dcg_goal_2(ListConstructor, [X, Xs], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var) :-
	ListConstructor = ".", native_hal_read:list_constructor(ListConstructor),
	native_hal_read:new_dcg_var(VarSet0, N0, VarSet, N, Var),
	ConsTerm = functor(2, ListConstructor, [X, Xs], Context),
	native_hal_read:term_list_append_term(ConsTerm, var(Var,Context), Term),
	OutTerm = functor(2, "=",[var(Var0,Context), Term],Context).


	% Conjunction.
native_hal_read:parse_dcg_goal_2(",", [A0, B0], Context, VarSet0, N0, Var0,OutTerm, VarSet, N, Var) :-
	native_hal_read:parse_dcg_goal(A0, VarSet0, N0, Var0, A, VarSet1, N1, Var1),
	native_hal_read:parse_dcg_goal(B0, VarSet1, N1, Var1, B, VarSet, N, Var),
	OutTerm = functor(2, ",",[A,B],Context).

native_hal_read:parse_dcg_goal_2("&", [A0, B0], Context, VarSet0, N0, Var0,OutTerm, VarSet, N, Var) :-
	native_hal_read:parse_dcg_goal(A0, VarSet0, N0, Var0, A, VarSet1, N1, Var1),
	native_hal_read:parse_dcg_goal(B0, VarSet1, N1, Var1, B, VarSet, N, Var),
	OutTerm = functor(2, "&",[A,B],Context).


	% Disjunction or if-then-else (Prolog syntax).
native_hal_read:parse_dcg_goal_2(";", [A0, B0], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var) :-
	(
	  A0 = functor(2, "->", [Cond0, Then0], _Context)
	->
	  native_hal_read:parse_dcg_if_then_else(Cond0, Then0, B0, Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var)
	;
	  native_hal_read:parse_dcg_goal(A0, VarSet0, N0, Var0, A1, VarSet1, N1, VarA),
	  native_hal_read:parse_dcg_goal(B0, VarSet1, N1, Var0,	B1, VarSet, N, VarB),
	  ( VarA = Var0, VarB = Var0 ->
	      Var = Var0,
	      OutTerm = functor(2, ";",[A1,B1],Context)
	  ;
	      VarA = Var0 ->
	      Var = VarB,
	      Unify = functor(2, "=",[var(Var,Context),var(VarA,Context)],Context),
	      native_hal_read:append_to_disjunct(A1, Unify, Context, A2),
	      OutTerm = functor(2, ";",[A2,B1],Context)
	  ;
	      VarB = Var0 ->
	      Var = VarA,
	      Unify = functor(2, "=",[var(Var,Context),var(VarB,Context)],Context),
	      native_hal_read:append_to_disjunct(B1, Unify, Context, B2),
	      OutTerm = functor(2, ";",[A1,B2],Context)
	  ;
	      Var = VarB,
 	      native_hal_read:rename_in_term(A1, VarA, VarB, A2),       %     change !!!
%% 	      Unify = functor(2, "=",[var(VarA,Context),var(VarB,Context)],Context),
%% 	      native_hal_read:append_to_disjunct(A1, Unify, Context, A2),
	      OutTerm = functor(2, ";",[A2,B1],Context)
	  )
	).

	% Negation (NU-Prolog syntax).
native_hal_read:parse_dcg_goal_2( "not", [A0], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var ) :-
	native_hal_read:parse_dcg_goal(A0, VarSet0, N0, Var0, A, VarSet, N, _),
	Var = Var0,
	OutTerm = functor(1, "not",[A],Context).

	% Negation (Prolog syntax).
native_hal_read:parse_dcg_goal_2( "\\+", [A0], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var ) :-
	native_hal_read:parse_dcg_goal(A0, VarSet0, N0, Var0, A, VarSet, N, _),
	Var = Var0,
	OutTerm = functor(1, "\\+",[A],Context).

native_hal_read:parse_dcg_goal_2("all", [Vars0, A0], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var) :-
% 	vars(Vars0, Vars),  B?
	Vars = Vars0,
	native_hal_read:parse_dcg_goal(A0, VarSet0, N0, Var0, A, VarSet, N, Var),
	OutTerm = functor(1, "all",[Vars,A],Context).

	% Existential quantification.
native_hal_read:parse_dcg_goal_2("some", [Vars0, A0], Context, VarSet0, N0, Var0, OutTerm, VarSet, N, Var) :-
% 	vars(Vars0, Vars),  B?
	Vars = Vars0,
	native_hal_read:parse_dcg_goal(A0, VarSet0, N0, Var0, A, VarSet, N, Var),
	OutTerm = functor(2, "some",[Vars,A],Context).


:- pred native_hal_read:append_to_disjunct(sterm, sterm, term_context, sterm).
:- mode native_hal_read:append_to_disjunct(in, in, in, out) is det.

native_hal_read:append_to_disjunct(Disjunct0, Goal, Context, Disjunct) :-
	( Disjunct0 = functor(2, ";",[A0,B0],_) ->
		native_hal_read:append_to_disjunct(A0, Goal, Context, A),
		native_hal_read:append_to_disjunct(B0, Goal, Context, B),
	        Disjunct = functor(2, ";",[A,B],Context)
	;
		Disjunct = functor(2, ",",[Disjunct0, Goal],Context)
	).

:- pred native_hal_read:parse_some_vars_dcg_goal(sterm, dict, int, int, sterm, dict, int, int).
:- mode native_hal_read:parse_some_vars_dcg_goal(in, in, in, in, out, out, out, out) is det.
native_hal_read:parse_some_vars_dcg_goal(A0, VarSet0, N0, Var0, A, VarSet, N, Var) :-
	( A0 = functor(2, "some", [_, A1], _Context) ->
	    A2 = A1
	;
	    A2 = A0
	),
	native_hal_read:parse_dcg_goal(A2, VarSet0, N0, Var0, A, VarSet, N, Var).

	% Parse the "if" and the "then" part of an if-then or an
	% if-then-else.
	% If the condition is a DCG goal, but then "then" part
	% is not, then we need to translate
	%	( a -> { b } ; c )
	% as
	%	( a(DCG_1, DCG_2) ->
	%		b,
	%		DCG_3 = DCG_2
	%	;
	%		c(DCG_1, DCG_3)
	%	)
	% rather than
	%	( a(DCG_1, DCG_2) ->
	%		b
	%	;
	%		c(DCG_1, DCG_2)
	%	)
	% so that the implicit quantification of DCG_2 is correct.

:- pred native_hal_read:parse_dcg_if_then(sterm, sterm, term_context, dict, int,
		int, sterm, sterm, dict, int, int).
:- mode native_hal_read:parse_dcg_if_then(in, in, in, in, in, in, out, out, out, out, out) is det.

native_hal_read:parse_dcg_if_then(Cond0, Then0, Context, VarSet0, N0, Var0, Cond, Then, VarSet, N, Var) :-
	native_hal_read:parse_some_vars_dcg_goal(Cond0, VarSet0, N0, Var0, Cond, VarSet1, N1, Var1),
	native_hal_read:parse_dcg_goal(Then0, VarSet1, N1, Var1, Then1, VarSet2, N2, Var2),
	( Var0 \= Var1, Var1 = Var2 -> % a DCG-var in condition, no DCG-var in then
		native_hal_read:new_dcg_var(VarSet2, N2, VarSet, N, Var),
		Unify = functor(2, "=",[var(Var,Context), var(Var2,Context)], Context),
	        Then = functor(2, ",",[Then1, Unify], Context)
	;
		Then = Then1,
		N = N2,
		Var = Var2,
		VarSet = VarSet2
	).

:- pred native_hal_read:parse_dcg_if_then_else(sterm, sterm, sterm, term_context,
		dict, int, int, sterm, dict, int, int).
:- mode native_hal_read:parse_dcg_if_then_else(in, in, in, in, in, in, in,
		out, out, out, out) is det.

native_hal_read:parse_dcg_if_then_else(Cond0, Then0, Else0, Context, VarSet0, N0, Var0,	OutTerm, VarSet, N, Var) :-
	native_hal_read:parse_dcg_if_then(Cond0, Then0, Context, VarSet0, N0, Var0, Cond, Then1, VarSet1, N1, VarThen),
	native_hal_read:parse_dcg_goal(Else0, VarSet1, N1, Var0, Else1, VarSet, N, VarElse),
	( VarThen = Var0, VarElse = Var0 ->  % no DCG in if-then, neither else
		Var = Var0,
		Then = Then1,
		Else = Else1
	; VarThen = Var0 ->  % no DCG in if-then, but DCGs in else
		Var = VarElse,
		Unify = functor(2, "=",[var(Var,Context), var(VarThen,Context)],Context),
		Then = functor(2, ",",[Then1, Unify], Context),
		Else = Else1
	; VarElse = Var0 -> % no DCG in else, but DCGs in if-then
		Var = VarThen,
		Then = Then1,
		Unify = functor(2, "=",[var(Var,Context), var(VarElse,Context)],Context),
	        Else = functor(2, ",",[Else1, Unify], Context)
	;
		% We prefer to substitute the then part since it is likely
		% to be smaller than the else part, since the else part may
		% have a deeply nested chain of if-then-elses.

		% parse_dcg_if_then guarantees that if VarThen \= Var0,
		% then the then part introduces a new DCG variable (i.e.
		% VarThen does not appear in the condition). We therefore
		% don't need to do the substitution in the condition.

		Var = VarElse,
 		native_hal_read:rename_in_term(Then1, VarThen, VarElse, Then), % change
%% 	        Unify = functor(2, "=",[var(VarThen,Context),var(VarElse,Context)],Context),
%%  	        Then = functor(2, ",", [Then1,Unify],Context),
		Else = Else1
	),
	% Goal = if_then_else(SomeVars, Cond, Then, Else) - Context change
	OutIfThen = functor(2, "->",[Cond, Then],Context),
	OutTerm = functor(2, ";",[OutIfThen,Else],Context).

	% native_hal_read:term_list_append_term(ListTerm, Term, Result):
	% 	if ListTerm is a term representing a proper list, 
	%	this predicate will append the term Term
	%	onto the end of the list

:- pred native_hal_read:term_list_append_term(sterm, sterm, sterm).
:- mode native_hal_read:term_list_append_term(in, in, out) is semidet.

native_hal_read:term_list_append_term(List0, Term, List) :-
	( List0 = atom("[]", _Context) ->
		List = Term
	;
	        native_hal_read:list_constructor(ListConstructor),
		List0 = functor(2, ListConstructor, [Head, Tail0], Context2),
		List = functor(2, ListConstructor, [Head, Tail], Context2),
		native_hal_read:term_list_append_term(Tail0, Term, Tail)
	).



%%%%%%%%%%%%%%%%%%%%%%%%
% taken from prog_io.m and possibly others %
%%%%%%%%%%%%%%%%%%%%%%%%

%---------------------------------------------------------------------------%
:- pred native_hal_read:sym_name_args_module(sterm, string, list(sterm), string).
:- mode native_hal_read:sym_name_args_module(in, out, out, out) is semidet.
%---------------------------------------------------------------------------%
% sym_name_args_module/4
% 
%---------------------------------------------------------------------------%

native_hal_read:sym_name_args_module(atom(Functor,_),Functor, [], "empty module name :-(") .
native_hal_read:sym_name_args_module(functor(Arity, Functor,Args,_),Name,Arguments,Module) :-
	(Arity = 2, Functor =  ":", Args = [Arg1,Arg2], Arg1 = atom(Module1,_) ->
	    Module = Module1,
	    (Arg2 = functor(_,Name1,Arguments1,_) ->
		Name = Name1,
		Arguments = Arguments1
	    ;
		Arg2 = atom(Name,_),
		Arguments = []
	    )
	;
	    Name = Functor,
	    Arguments = Args,
	    Module = "empty module name :-("
	).

% native_hal_read:list_to_conjunction(Rest, Context, First, Term).
% convert a list to a "conjunction" (bunch of terms separated by ','s)
% reordered args for indexing

:- pred native_hal_read:list_to_conjunction(list(sterm), term_context, sterm, sterm).
:- mode native_hal_read:list_to_conjunction(in, in, in, out) is det.

native_hal_read:list_to_conjunction([], _, Term, Term).
native_hal_read:list_to_conjunction([Second | Rest], Context, First, Term) :-
	native_hal_read:list_to_conjunction(Rest, Context, Second, Tail),
	Term = functor(2, ",", [First, Tail], Context).


%%%%%%%%%%%%%%%%%%%%%%%%
% adapted from prog_util.m
%%%%%%%%%%%%%%%%%%%%%%%%

%---------------------------------------------------------------------------%
:- pred native_hal_read:rename_in_term(sterm,int,int,sterm).
:- mode native_hal_read:rename_in_term(in,in,in,out) is det.
%---------------------------------------------------------------------------%
% native_hal_read:rename_in_term/4
% 
%---------------------------------------------------------------------------%

native_hal_read:rename_in_term(OldTerm, OldVar, NewVar, NewTerm) :-
	(OldTerm = var(N,Context) ->
	    (N = OldVar ->
		NewTerm = var(NewVar,Context)
	    ;
		NewTerm = OldTerm
	    )
	;
	    (OldTerm = functor(Arity,Name,OldArgs,Context) ->
		native_hal_read:rename_in_listofterms(OldArgs,OldVar, NewVar,NewArgs),
		NewTerm = functor(Arity,Name,NewArgs,Context)
	    ;
		NewTerm = OldTerm
	    )
	).


%---------------------------------------------------------------------------%
:- pred native_hal_read:rename_in_listofterms(list(sterm),int,int,list(sterm)).
:- mode native_hal_read:rename_in_listofterms(in,in,in,out) is det.
%---------------------------------------------------------------------------%
% native_hal_read:rename_in_terms/4
%
%---------------------------------------------------------------------------%

native_hal_read:rename_in_listofterms([],_,_,[]).
native_hal_read:rename_in_listofterms([A|R],OldVar,NewVar,[B|S]) :-
	native_hal_read:rename_in_term(A,OldVar,NewVar,B),
	native_hal_read:rename_in_listofterms(R,OldVar,NewVar,S).


%---------------------------------------------------------------------------%
:- pred native_hal_read:list_constructor(string).
:- mode native_hal_read:list_constructor(out) is det.
%---------------------------------------------------------------------------%
% native_hal_read:list_constructor/1
% 
%---------------------------------------------------------------------------%

native_hal_read:list_constructor("."). % search for "." - it appears more than once
				% but not all occurences may be replaced
				% the ones that should not be replaced, have a comment


w(X,I,I) :- writeln(X).

w(X) :- writeln(X).

w1(X,I,I) :- writeln(X/I).