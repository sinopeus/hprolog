% Mon Jun 23 09:52:11 CEST 2003
%% Bart Demoen K.U.Leuven
%% 
%% This file contains a pretty printer - evolving ...
%% Most recent change: recently.

:- module(pretty_printer,
 	  [
 	   listing/1, pretty_print/1, pretty_print/2
 	  ]).

:- use_module(library(lists)).

% listing/1: currently a bit of a hack :-)



listing(Mod) :-
	Mod:allclauses(Clause),
	pretty_print(Clause),
	fail.
listing(_).

pretty_print(Clause) :-
	pretty_print(Clause,[]).

pretty_print(Clause,Dict) :-
	givenames(Clause,Dict),
	pretty_print1(Clause),
	put(0' ), put(0'.), nl,nl,
	fail.
pretty_print(_,_).

givenames(T,Dict) :-
	allvars(T,[],Vars),
	givenames(Vars,Dict,0).

attr_unify_hook(_,_).

givenames([],_,_).
givenames([V|R],Dict,I) :-
	( spec_lookup(Dict,V,Name) ->
		V = '$VAR'(Name),
		givenames(R,Dict,I)
	; get_attr(V,pretty_printer,N), N == 0 -> 
		V = '$VAR'(-1),
		givenames(R,Dict,I)
	;
		V = '$VAR'(I),
		J is I + 1,
		givenames(R,Dict,J)
	).

spec_lookup([K=V|R],Key,Value) :-
	( Key == K ->
		Value = V
	;
		spec_lookup(R,Key,Value)
	).

allvars(Term, In, Out) :-
        (   var(Term) ->
            addvar(In, Term, Out)
        ;   atomic(Term) ->
            Out=In
        ;   Term=[A|R] ->
            allvars(A, In, In1),
            allvars(R, In1, Out)
        ;   Term=..[_|Args],
            allvars(Args, In, Out)
        ).

addvar(In,Var,Out) :-
	( get_attr(Var,pretty_printer,_) ->
		In = Out,
		put_attr(Var,pretty_printer,1)
	;
		Out = [Var|In],
		put_attr(Var,pretty_printer,0)
	).

pretty_print1(Clause) :-
        (   Clause=(H:-B) ->
            pretty_print2(H, B)
        ;   pretty_print2(Clause, true)
        ).

pretty_print2(H, B) :-
        (   B = true ->
            writeq(H)
        ;   B = ! ->
            writeq(H),
            write(' :- !')
        ;   writeq(H),
            write(' :-'),
            nl,
            pretty_print3(B, 8)
        ).

pretty_print3((A,B),Space) :- !,
	pretty_print3(A,Space),
	put(0',),nl,
	pretty_print3(B,Space).
pretty_print3((A->B;C),Space) :- !,
	tab(Space), put(0'(),
	pretty_print_ite((A->B;C),Space),nl,
	tab(Space), put(0')).
pretty_print3((A->B),Space) :- !,
	tab(Space), put(0'(), nl,
	Space1 is Space+2,
	Space2 is Space+5,
	pretty_print3(A,Space1),
	write(' ->'), nl,
	pretty_print3(B,Space2),nl,
	tab(Space), put(0')).
pretty_print3((A;B),Space) :- !,
	tab(Space), put(0'(), nl,
	pretty_print_disj((A;B),Space),nl,
	tab(Space), put(0')).
pretty_print3(once(G),Space) :- !,
	tab(Space),
	write('once('),
	( simple_goal(G) ->
		pretty_print3(G,1),
		put(0'))
	;
		put(0'(),
		nl,
		Space1 is Space + 4,
		pretty_print3(G,Space1),
		nl,
		tab(Space),
		put(0')),
		put(0'))
	).
	
pretty_print3(A:G,Space) :- !,
 	tab(Space),
	writeq(A), put(0':), writeq(G).
pretty_print3(A,Space) :-
 	tab(Space),
	writeq(A).

pretty_print_ite(G,Space) :-
	Space1 is Space+2,
	Space2 is Space+5,
	( G = (A->B;C) ->
		( simple_goal(A) ->
			pretty_print3(A,2)
		;	
			nl,
			pretty_print3(A,Space1)
		),
		write(' ->'),
		nl,
		pretty_print3(B,Space2),nl,
		tab(Space), put(0';), 
		pretty_print_ite(C,Space)
	; G = (A -> B) ->
		( simple_goal(A) ->
			pretty_print3(A,2)
		;
			nl,
			pretty_print3(A,Space1)
		),
		write(' ->'),
		nl,
		pretty_print3(B,Space2)
	;
		nl,
		pretty_print3(G,Space2)
	).

pretty_print_disj(G,Space) :-
	Space1 is Space+5,
	( G = (A;B), A \= (_ -> _) -> 
		pretty_print_disj(A,Space),nl,
		tab(Space), put(0';), nl,
		pretty_print_disj(B,Space)
	;
		pretty_print3(G,Space1)
	).

simple_goal(G) :-
	G \= (_ , _),
	G \= (_ ; _),
	G \= (_ -> _),
	\+ ( functor(G,once,1), G = once(SG), \+ simple_goal(SG) ).
