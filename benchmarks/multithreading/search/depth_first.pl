depth_search(Space, State, Bound, Solution) :-
  depth(Space, State, Bound, [], Path),
  sysh:reverse(Path, Solution).

depth(Space, State, _, Path, [State| Path]) :-
  goal_state(Space, State).
depth(Space, State, Bound, Path, Solution) :-
  Bound > 0,
  next_state(Space, State, Next, _),
  \+ member_path(Next, [State| Path]),
  Bound2 is Bound - 1,
  depth(Space, Next, Bound2, [State| Path], Solution).

