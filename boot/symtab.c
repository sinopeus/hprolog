
#include "toinclude.h"
#include <stdio.h>
#include <stdlib.h>
#include "mach_macros.h"
#include <string.h>

extern void deal_with_tr_overflow(struct machine *machp);
extern void fatal_message(char *);

static dlong SYMBS=32000;
struct syminfo *symtab = 0;
codep *entries;
char *protection_info;

#define SYMHASHTABLE_LEN 50021
typedef struct hash_entry {dlong index; struct hash_entry *next;} hash_entry;
static struct hash_entry **symhashtable; // should be made expandable

#define SW_LIST      0
#define SW_INT       1
#define SW_REAL      2
#define SW_ATOM      3
#define SW_STRUCT    4
#define SW_CHAR      5
#define SW_STRING    6

#define fake_entry   -1

dlong CODEZONEL=16000000;
byte *codezone;

static dlong MAXPREDSTARTS = 0;
dlong *predstarts;
dlong nrofpredstarts = 0;

static codep *labeltabel = 0;
static dlong MAXLABEL = 0;
static dlong instrs, doing_profiling;

dlong numb_symbs = 0;
static dlong *tempsymbtab; // is allocated/freed on demand/need
codep topofcode;
static dlong hash_table_malloced = 0;

static dlong *active_yvar_zone = 0;
static dlong active_yvar_zone_current = 0;
static dlong active_yvar_zone_limit;
extern dlong compare_version(FILE *);
extern dlong precise_profile_call_site_opt;
extern dlong overwrite_without_asking;
extern dlong compress_table;
extern dlong peep_spec;

#ifdef MULTI_THREADED
static pthread_mutex_t entries_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t predstarts_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t load_compiled_file_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_rwlock_t symtab_rwlock = PTHREAD_RWLOCK_INITIALIZER;

#else

#define pthread_rwlock_rdlock(x)
#define pthread_rwlock_wrlock(x)
#define pthread_rwlock_unlock(x)

#endif

/* I am not happy with these macros, but for now, it will have to do ... */

#define deal_with_loading_select_struct2(dlong0, dlong1, dlong3, topofcode) \
        if (!(0x1 & dlong0)) *(dlong *)(arg_place(locpc,1,select_functor2)) = make_atom(tempsymbtab[dlong1]); \
        if (!(0x2 & dlong0)) *(dlong *)(arg_place(locpc,3,select_functor2)) = make_atom(tempsymbtab[dlong3]);

#define deal_with_loading_select_struct3(dlong0, dlong1, dlong3, dlong5, topofcode) \
        if (!(0x1 & dlong0)) *(dlong *)(arg_place(locpc,1,select_functor3)) = make_atom(tempsymbtab[dlong1]); \
        if (!(0x2 & dlong0)) *(dlong *)(arg_place(locpc,3,select_functor3)) = make_atom(tempsymbtab[dlong3]); \
        if (!(0x4 & dlong0)) *(dlong *)(arg_place(locpc,5,select_functor3)) = make_atom(tempsymbtab[dlong5]);

#define deal_with_loading_select_struct4(dlong0, dlong1, dlong3, dlong5, dlong7, topofcode) \
        if (!(0x1 & dlong0)) *(dlong *)(arg_place(locpc,1,select_functor4)) = make_atom(tempsymbtab[dlong1]); \
        if (!(0x2 & dlong0)) *(dlong *)(arg_place(locpc,3,select_functor4)) = make_atom(tempsymbtab[dlong3]); \
        if (!(0x4 & dlong0)) *(dlong *)(arg_place(locpc,5,select_functor4)) = make_atom(tempsymbtab[dlong5]); \
        if (!(0x8 & dlong0)) *(dlong *)(arg_place(locpc,7,select_functor4)) = make_atom(tempsymbtab[dlong7]);

#define deal_with_loading_select_struct5(dlong0, dlong1, dlong3, dlong5, dlong7, dlong9, topofcode) \
        if (!(0x1 & dlong0)) *(dlong *)(arg_place(locpc,1,select_functor5)) = make_atom(tempsymbtab[dlong1]); \
        if (!(0x2 & dlong0)) *(dlong *)(arg_place(locpc,3,select_functor5)) = make_atom(tempsymbtab[dlong3]); \
        if (!(0x4 & dlong0)) *(dlong *)(arg_place(locpc,5,select_functor5)) = make_atom(tempsymbtab[dlong5]); \
        if (!(0x8 & dlong0)) *(dlong *)(arg_place(locpc,7,select_functor5)) = make_atom(tempsymbtab[dlong7]); \
        if (!(0x10 & dlong0)) *(dlong *)(arg_place(locpc,9,select_functor5)) = make_atom(tempsymbtab[dlong9]);


static char bbb[100];

static char *get_atom_name_carefull(dlong p)
{
  dlong i;
  i = get_atom_index(p);
  pthread_rwlock_rdlock(&symtab_rwlock);
  if ((i < 0) || (i >= numb_symbs))
    {
      pthread_rwlock_rdlock(&symtab_rwlock);
      sprintf(bbb,"%ld (dlong instruct)",i);
      return(bbb);
    }
  char* res =symtab[i].name;
  pthread_rwlock_unlock(&symtab_rwlock);
  return res;
} /* get_atom_name_carefull */

static void store_string(char *p, dlong l, FILE *f)
{
  char a,b;
  dlong ret;
  /* we'll store strings char by char - not pad - comparison will be 
     also char by char for the length
  */

  /* skip a space and a " */
  ret = fscanf(f,"%c%c",&a,&b);
  while (l--)
    {
      ret = fscanf(f,"%c",&a);
      *p++ = a;
    }
  /* skip a space and a " */
  ret = fscanf(f,"%c%c",&a,&b);
} /* store_string */

static void store_bigint(char *q, dlong l, FILE *f)
{
  char a,b;
  dlong *p = (dlong *)q;
  dlong ret;

  /* we'll store bigints a dlong at a time */

  /* skip a space */
  ret = fscanf(f,"%c",&a);
  if (l < 0) l = -l;
  while (l--)
    {
      ret = fscanf(f,"%ld",p);
      p++;
    }
} /* store_bigint */

typedef struct tempindex
{
  union
  {
    dfloat float_value;
    dlong int_value;
    char * string_value;
    char char_value;
  } value;
  dlong strlen;
  dlong hashvalue;
  dlong togoto;
  dlong next_same_hash;
  dlong *to_relocate;
  char type;
  char first;
} tempindex;

dlong number_of_symbols()
{
  pthread_rwlock_rdlock(&symtab_rwlock);
  dlong res = numb_symbs;
  pthread_rwlock_unlock(&symtab_rwlock);
  return res;
} /* number_of_symbols */


static dlong could_be_cyclic(dlong *p)
{
  deref(p);

  return( (tag(p) >= ATT) );

} /* could_be_cyclic */

static dlong hash_value;
static void term_hash_rec(dlong *p, struct machine *machp)
{
  deref2(p,goto is_a_var);

  if (has_atom_tag(p))
    {
      hash_value += ((dlong)p >> 4);
      return;
    }

  if (is_bigint(p))
    {
      /* take only first word */
      p = get_number_pointer(p) + 1;
      hash_value += *p;
      return;
    }

  if (is_real(p))
    {
      /* take only first word */
      p = get_number_pointer(p) + 1;
      hash_value += *p;
      return;
    }

  if (is_string(p))
    {
      p = get_string_pointer(p) + 1;
      hash_value += *p;
      return;
    }

  if (is_struct(p))
    {
      dlong ar;

      p = get_struct_pointer(p);
      hash_value += *p;
      ar = get_funct_arity(*p);
      while (ar--)
	{
	  dlong *q;
	  p++;
	  q = (dlong *)(*p);
	  if (could_be_cyclic(q))
	    {
	      uunconditional_value_trail(p,machp->TR,machp);
	      *p = make_smallint(0);
	    }
	  term_hash_rec(q, machp);
	}
      return;
    }

  if (is_list(p))
    {
      dlong *q;
      p = get_list_pointer(p);
      hash_value += 17;
      q = (dlong *)p[0];
      if (could_be_cyclic(q))
	{
	  uunconditional_value_trail(p,machp->TR,machp);
	  *p = make_smallint(0);
	}
      term_hash_rec(q, machp);
      p++;
      q = (dlong *)p[0];
      if (could_be_cyclic(q))
	{
	  uunconditional_value_trail(p,machp->TR,machp);
	  *p = make_smallint(0);
	}
      term_hash_rec(q, machp);
      return;
    }

  // all vars are now treated the same

 is_a_var:
  hash_value += 17;
  return;
  
} /* term_hash_rec */

static void untrail_till(dlong there, struct machine *machp)
{
  dlong *until = machp->limit_trail + there;
  dlong *tr = machp->TR;

  while (tr > until)
    {
      dlong *q; q = (dlong *)*(--tr);
      if (q) *q = *(--tr);
    }
  machp->TR = tr;
  
} /* untrail_till */

dlong term_hash_builtin(dlong *p, struct machine *machp)
{
  dlong t;
  dlong trailtop;
  hash_value = 0;
  trailtop = machp->TR-machp->limit_trail;
  term_hash_rec(p, machp);
  untrail_till(trailtop, machp);
  t = hash_value % 33554432;
  if (t < 0) t = -t;
  return(t);
} /* term_hash_builtin */


dlong code_size()
{
  return((topofcode-codezone) /* + hash_table_malloced */);
} /* code_size */

void find_pred_index(codep pc, dlong * pindex)
{
  codep sp = codezone;
  codep tmpp;
  dlong i, index;

  index = 0;
  pthread_rwlock_rdlock(&symtab_rwlock);
  for (i = 0; i < numb_symbs; i++)
    {
      tmpp = main_entrypoint(i);
      if (! tmpp) continue;
      if (tmpp > pc) continue;
      if (tmpp >= sp)
	{
	  sp = tmpp;
	  index = i;
	}
    }
  pthread_rwlock_unlock(&symtab_rwlock);

  *pindex = index;
} /* find_pred_index */


void fan(codep pc, struct machine *machp)
{
  dlong index;
  find_pred_index(pc, &index);
  fprintf(stderr,"%s/%ld ",symtab[index].name,symtab[index].arity);
  fprintf(stderr,"e=%ld tos=%ld\n",begin_ls - machp->E,
	  begin_ls - machp->TOS);
  return;
} /* fan */

void find_name_arity(codep pc, dlong * atom, dlong * arity)
{
  dlong index;
  find_pred_index(pc, &index);
  pthread_rwlock_rdlock(&symtab_rwlock);
  *arity = symtab[index].arity;
  pthread_rwlock_unlock(&symtab_rwlock);
  *atom = make_atom(sym_lookup(0,symtab[index].name,USER_MOD));
  return;
} /* find_name_arity */


dlong find_symbol_from_codep(codep pc)
{
  dlong i;

  pthread_rwlock_rdlock(&symtab_rwlock);
  for (i = 0; i < numb_symbs; i++)
  {
    if (pc == main_entrypoint(i))
    {
      pthread_rwlock_unlock(&symtab_rwlock);
      fprintf(stderr,"%s %ld\n",symtab[i].name,symtab[i].arity);
      return(i);
    }
  }
  pthread_rwlock_unlock(&symtab_rwlock);

  return -1;
} /* find_symbol_from_codep */

static void fill_predstarts(codep p)
{
  pthread_mutex_lock(&predstarts_mutex);
  if (nrofpredstarts >= MAXPREDSTARTS)
    {
      if (! MAXPREDSTARTS)
	{
	  /* first time coming here */
	  MAXPREDSTARTS = 5000;
	  predstarts = (dlong *)malloc(MAXPREDSTARTS*sizeof(dlong));
	}
      else
	{
	  MAXPREDSTARTS = MAXPREDSTARTS + MAXPREDSTARTS;
	  predstarts = (dlong *)realloc(predstarts,MAXPREDSTARTS*sizeof(dlong));
	}
      if (! predstarts)
	fatal_message("too many pred entry points");
    }
  predstarts[nrofpredstarts++] = (p-codezone)<<2;
  pthread_mutex_unlock(&predstarts_mutex);
  /* in this way, no relocation necessary and I have 2 bits */
} /*  fill_predstarts */

/*-------------------------------------------------------------------------*/

void init_codezone()
{
  codezone = calloc(CODEZONEL*sizeof(byte),1);
  if (! codezone)
    fatal_message("could not allocate code zone");
  topofcode = codezone;
  fill_predstarts(codezone); /* this takes care of initializing predstarts */

} /* init_codezone */


void make_pred_without_clauses(dlong i)
{
  entries[i] = topofcode;
  fill_opcode(topofcode,call_unkn);
  *(dlong *)(arg_place(topofcode,1,call_unkn)) = i;
  topofcode += call_unkn_len;
  fill_predstarts(topofcode);
} /* make_pred_without_clauses */

dlong not_enough_space_for_cinter()
{
  return(topofcode + call_c_len > (codezone+CODEZONEL));
} /* not_enough_space_for_cinter */

void make_foreign_pred(dlong i, dlong *cfunction)
{
  i = get_funct_symb(i);
  entries[i] = topofcode;
  fill_opcode(topofcode,call_c);
  *(dlong *)(arg_place(topofcode,1,call_c)) = (dlong)(cfunction);
  *(dlong *)(arg_place(topofcode,2,call_c)) = i;
  topofcode += call_c_len;
  fill_predstarts(topofcode);
} /* make_foreign_pred */


void warn_unknown()
{
  dlong i;
  pthread_rwlock_rdlock(&symtab_rwlock);
  for (i = 0; i < numb_symbs; i++)
    {
      codep pc;
      pc = main_entrypoint(i);
      if ((points_to_unknown_call(pc)) && (symtab[i].arity > 0))
	fprintf(stderr,"%s/%ld\n",symtab[i].name,symtab[i].arity);
    }
  pthread_rwlock_unlock(&symtab_rwlock);
} /* warn_unknown */


static dlong switch_hash_size(dlong i)
{
  dlong k = 0;

  while (i)
    {
      i = i >> 1;
      k++;
    }

  if (k > 12) k = 12;
  else if (k < 1) k = 1;

  return((1<<(k+1)));

} /* switch_hash_size */

dlong string_hash(dlong len, char *pc)
{
  dlong res = 0;
  while (len--)
    res = ((res << 1) + *pc++);
  res = (res*7919) & 0x07ffffff;
  return(res%8209);
} /* string_hash */

static dlong deal_with_loading_active_yvar(FILE *f, codep topofcode)
{
  dlong nrslots, slotnr, bitmap, i;
  dlong ret;

  instrs--;

  /* the format in the file f is as follows:
     number of pairs
     pairs of the form: slotnumber 24bits number

     in the code this will become:

     if there is only one pair:
            1S24     S is the slot number 24 = the 24 bits word

     if there is more than one pair:
            0Index   Index is in the table with bitmaps
  */

  ret = fscanf(f," %ld",&nrslots);
  if (nrslots == 1)
    {
      ret = fscanf(f," %ld",&slotnr);
      ret = fscanf(f," %ld",&bitmap);
      i = ((slotnr<<24)+bitmap) | 0x80000000;
      return(i);
    }

  i = active_yvar_zone_current;
  if (!active_yvar_zone)
    {
      /* it means that there is no zone allocated yet */
      active_yvar_zone_limit = 200;
      active_yvar_zone = malloc(active_yvar_zone_limit*sizeof(dlong));
      if (! active_yvar_zone)
	fatal_message("Could not allocate space for temporary active yvars\n");
    }
  if (active_yvar_zone_current+nrslots+2 >= active_yvar_zone_limit)
    {
      active_yvar_zone_limit = active_yvar_zone_limit * 2;
      if (active_yvar_zone_current+nrslots+2 >= active_yvar_zone_limit)
	active_yvar_zone_limit += active_yvar_zone_current+nrslots+2;
      active_yvar_zone = realloc(active_yvar_zone,active_yvar_zone_limit*sizeof(dlong));
      if (! active_yvar_zone)
	fatal_message("Could not allocate space for temporary active yvars\n");
    }

  active_yvar_zone[active_yvar_zone_current++] = (dlong)(topofcode+doing_profiling*(instrs-1)*profile_instr_len);
  active_yvar_zone[active_yvar_zone_current++] = nrslots;

  while (nrslots--)
    {
      ret = fscanf(f," %ld",&slotnr);
      ret = fscanf(f," %ld",&bitmap);
      active_yvar_zone[active_yvar_zone_current++] = (slotnr<<24)+bitmap;
    }
  return(i);

} /* deal_with_loading_active_yvar */

static void fill_ifequal(struct tempindex * pti, codep *toc, dlong argument)
{
  codep pc = *toc;

  if (pti->type == SW_REAL)
    {
      instrs++;
      *(type_opcode *)pc = ifequal_float_goto;
      *(areg_type *)(arg_place(pc,1,ifequal_float_goto)) = (areg_type)argument;
      store_double_arg(arg_place(pc,3,ifequal_float_goto), (pti->value.float_value));
      pti->to_relocate = (dlong *)arg_place(pc,2,ifequal_float_goto);
      *(dlong *)arg_place(pc,2,ifequal_float_goto) = pti->togoto;
      *toc = pc + ifequal_float_goto_len;
    }
  else
    if (pti->type == SW_LIST)
      {
	instrs++;
	*(type_opcode *)pc = ifequal_list_goto;
	*(areg_type *)(arg_place(pc,1,ifequal_list_goto)) = (areg_type)argument;
	pti->to_relocate = (dlong *)arg_place(pc,2,ifequal_list_goto);
	*(dlong *)arg_place(pc,2,ifequal_list_goto) = pti->togoto;
	*toc = pc + ifequal_list_goto_len;
      }
  else
    if (pti->type == SW_INT)
      {
	instrs++;
	*(type_opcode *)pc = ifequal_int_goto;
	*(areg_type *)(arg_place(pc,1,ifequal_int_goto)) = (areg_type)argument;
	*(dlong *)(arg_place(pc,3,ifequal_int_goto)) = (dlong)(pti->value.int_value);
	pti->to_relocate = (dlong *)arg_place(pc,2,ifequal_int_goto);
	*(dlong *)arg_place(pc,2,ifequal_int_goto) = pti->togoto;
	*toc = pc + ifequal_int_goto_len;
      }
  else
    if (pti->type == SW_CHAR)
      {
	instrs++;
	*(type_opcode *)pc = ifequal_char_goto;
	*(areg_type *)(arg_place(pc,1,ifequal_char_goto)) = (areg_type)argument;
	*(char *)(arg_place(pc,2,ifequal_char_goto)) = (pti->value.char_value);
	pti->to_relocate = (dlong *)arg_place(pc,3,ifequal_char_goto);
	*(dlong *)arg_place(pc,3,ifequal_char_goto) = pti->togoto;
	*toc = pc + ifequal_char_goto_len;
      }
  else
    if (pti->type == SW_STRING)
      {
	instrs++;
	*(type_opcode *)pc = ifequal_string_goto;
	*(areg_type *)(arg_place(pc,1,ifequal_string_goto)) = (areg_type)argument;
	*(str_len *)(arg_place(pc,3,ifequal_string_goto)) = (pti->strlen);
	pti->to_relocate = (dlong *)arg_place(pc,2,ifequal_string_goto);
	*(dlong *)arg_place(pc,2,ifequal_string_goto) = pti->togoto;
	move_string(pti->strlen, pti->value.string_value,
		    (char *)arg_place(pc,4,ifequal_string_goto));
	pc += extra_strlen_len_ifequal_string_goto(pc);
	*toc = pc + ifequal_string_goto_len;
      }
  else
    {
      instrs++;
      *(type_opcode *)pc = ifequal_atom_struct_goto;
      *(areg_type *)(arg_place(pc,1,ifequal_atom_struct_goto)) = (areg_type)argument;
      *(dlong *)(arg_place(pc,3,ifequal_atom_struct_goto)) = (dlong)(pti->value.int_value);
      pti->to_relocate = (dlong *)arg_place(pc,2,ifequal_atom_struct_goto);
      *(dlong *)arg_place(pc,2,ifequal_atom_struct_goto) = pti->togoto;
      *toc = pc + ifequal_atom_struct_goto_len;
    }

} /* fill_ifequal */

static void enlarge_labeltabel(dlong k)
{
  if (! labeltabel)
    {
      // first time malloc
      MAXLABEL = 512;
      labeltabel = (codep *)malloc(MAXLABEL*sizeof(codep *));
    }
  else
    {
      // next time -> realloc
      do
	{
	  MAXLABEL = 2*MAXLABEL;
	}
      while (MAXLABEL <= k);
      labeltabel = realloc(labeltabel,MAXLABEL*sizeof(codep *));
    }

  if (! labeltabel)
    fatal_message("too many labels");

} /* enlarge_labeltabel */


static dlong  deal_with_loading_switchonbound(codep *toc, dlong *hashtable,dlong size,
                                            dlong modulo, dlong arity,
                                            dlong index_fail_label,
                                            struct tempindex * tmptable, dlong *nextlabel,
                                            dlong argument)
{
  dlong i,loci,j,focus,locnextlabel,last_same_hash;

  locnextlabel = *nextlabel;

  *(short *)(arg_place((*toc),2,switchonbound)) = (short)modulo;
  *toc += switchonbound_len;
  *toc += modulo * sizeof(dlong);

  j = size; /* Number of cases */
  i = 1;
  while (j)
    {
      while ((i <= size) && (tmptable[i].hashvalue == -1)) i++;
      focus = tmptable[i].hashvalue;
      last_same_hash = hashtable[focus];
      tmptable[i].first = 'o';  /* only */
      if (last_same_hash == i) /* meaning there is no clash */
        {
          j--;
          hashtable[focus] = tmptable[i].togoto;
          tmptable[i].first = 'o';  /* only */
          tmptable[i].hashvalue = -1;
          i++;
          continue;
        }
      hashtable[focus] = locnextlabel;
      if (locnextlabel >= MAXLABEL)
	enlarge_labeltabel(locnextlabel);
      labeltabel[locnextlabel] = *toc + doing_profiling*instrs*profile_instr_len;
      locnextlabel++;
      loci = i++;
      /* generate if_equal instructions */
      while (loci)
        {
          /* first generate an appropriate ifequal*goto instruction */
          j--;
          if (*toc  >= (codezone+CODEZONEL)) return(0);
          fill_ifequal(&tmptable[loci],toc,argument);
          tmptable[loci].hashvalue = -1;
          tmptable[loci].first = 'o';  /* only */
          loci = tmptable[loci].next_same_hash;
        }
      if (index_fail_label == -1)
	{
	  instrs++;
	  *(type_opcode *)(*toc) = fail; *toc += fail_len;
	}
      else
	{
	  instrs++;
	  *(type_opcode *)(*toc) = jump;
	  *(dlong *)(arg_place(*toc,1,jump)) = index_fail_label;
	  *toc += jump_len;
	}
    }
  if (index_fail_label != -1) {
    dlong i;
    for (i = 0; i < modulo; i++) {
      if (*(hashtable + i) == 0 )
	*(hashtable + i) = index_fail_label;
    }
  }
  *nextlabel  = locnextlabel;
  for (i = 1; i <= size; i++)
    if (tmptable[i].strlen)
      free(tmptable[i].value.string_value);
  free(tmptable);
  return(1);

} /* deal_with_loading_switchonbound */


static void deal_with_labels_switchonbound(codep pc)
{
  dlong *hashtable;
  dlong size;
  hashtable = (dlong *)(pc+switchonbound_len);
  size = (dlong)*(short *)(arg_place((pc),2,switchonbound));

  while (size--)
    {
      dlong i;
      i = *hashtable;
      if (i)
        *hashtable = (dlong)labeltabel[i]-doing_profiling*profile_instr_len; // recent change
      else *hashtable = (dlong)labeltabel[0];
      hashtable++;
    }

} /* deal_with_labels_switchonbound */

/* XXX must be revised some day */

static dlong hash(char *name, dlong len, dlong arity, dlong modnr)
{
  dlong i;

  i = 0;

  while (len--)
    {
      i = (i << 1) + (dlong)*name;
      name++;
    }

  i = i*(modnr+1);  /* +1 because modnrs start at 0 */
  i = i & 0x07ffffff;
  i = i + 17*arity;

  return(i);
} /* hash */

void init_symbol_tables()
{
  dlong i;

  symhashtable = (hash_entry **)calloc(SYMHASHTABLE_LEN,sizeof(hash_entry *));
  if (! symhashtable) fatal_message("symbol table initialization failed");

  entries = (codep *)malloc(SYMBS*sizeof(codep));
  if (! entries) fatal_message("symbol table initialization failed");

  protection_info = (char *)malloc(SYMBS*sizeof(char));
  if (! protection_info) fatal_message("symbol table initialization failed");

  symtab = malloc(SYMBS*sizeof(struct syminfo));
  if (! symtab) fatal_message("symbol table initialization failed");

  /* we want user to be the first element in the mod table
     and system the second */

  i = sym_lookup(0,"user",USER_MOD);
  if (i != USER_MOD)
    fatal_message("user module not initted properly");
  i = sym_lookup(0,"sysh",USER_MOD);
  if (i != SYSTEM_MOD)
    fatal_message("hProlog system module not initted properly");

  nil_atom = make_atom(sym_lookup(0,"[]",USER_MOD));
  fill_in_nil_atom_glob_val();

  i = sym_lookup(0,"<unprintable cutpoint>",USER_MOD);

  init_selected_symbols();

} /* init_symbol_tables */

static void relocate_symboltables()
{
  if (verbose_mode)
    printf("\nExpanding symbol table ...             was %ld\n",SYMBS);

  entries =  (codep *)realloc(entries,SYMBS*sizeof(codep)*2);
  if (! entries) fatal_message("symbol table relocation failed");

  protection_info =  (char *)realloc(protection_info,SYMBS*sizeof(char)*2);
  if (! protection_info) fatal_message("symbol table relocation failed");

  symtab =  realloc(symtab,SYMBS*sizeof(struct syminfo)*2);
  if (! symtab) fatal_message("symbol table relocation failed");

  SYMBS = SYMBS*2;

  if (verbose_mode)
    printf("\n                     done:              is %ld\n",SYMBS);

} /* relocate_symboltables */

void free_symboltables()
{
  free(entries);

  {
    dlong i;
    i = numb_symbs;
    while (i--) free(symtab[i].name);
  }
  free(symtab);
  free(labeltabel);

  {
    dlong i;
    struct hash_entry *p, *q;
    for (i = 0; i < SYMHASHTABLE_LEN; i++)
      {
	p = symhashtable[i];
	while (p)
	  {
	    q = p->next; free(p); p = q;
	  }
      }
  }

  free(symhashtable);
  free(active_yvar_zone);
  free(predstarts);

} /* relocate_symboltables */

sympointer sym_lookup(dlong arity, char *name, dlong modnr)
{
  dlong i, len, hashv;
  struct hash_entry *p;

  len = strlen(name);
  hashv = hash(name,len,arity,modnr);
  hashv = hashv % SYMHASHTABLE_LEN;
  p = symhashtable[hashv];

  pthread_rwlock_wrlock(&symtab_rwlock);
  while (p)
    {
      struct syminfo *pp;
      i = p->index; pp = symtab + i;
      if ((arity == pp->arity) &&
          (! strcmp(name,pp->name)) &&
          (pp->module == modnr))
      {
        pthread_rwlock_unlock(&symtab_rwlock);
        return(i);
      }
      p = p->next;
    }

  if (numb_symbs == SYMBS)
    relocate_symboltables();
  i = numb_symbs++;
  p = (struct hash_entry *)malloc(sizeof(hash_entry));
  hash_table_malloced += sizeof(hash_entry);
  p->index = i;
  p->next = symhashtable[hashv];
  symhashtable[hashv] = p;
  symtab[i].arity = arity;
  symtab[i].module = modnr;
  symtab[i].glob_val = (dlong *)(nil_atom);
  symtab[i].name = (char *)malloc(len+1);
  hash_table_malloced += len+1;
  strcpy(symtab[i].name,name);
  entries[i] = 0;
  pthread_rwlock_unlock(&symtab_rwlock);

  return(i);
} /* sym_lookup */

sympointer new_symbol()
{
  dlong i;

  pthread_rwlock_wrlock(&symtab_rwlock);
  if (numb_symbs == SYMBS)
    relocate_symboltables();
  i = numb_symbs++;
  symtab[i].arity = 0;
  symtab[i].module = 0;
  symtab[i].glob_val = (dlong *)(nil_atom);
  symtab[i].name = (char *)malloc(1);
  *(symtab[i].name) = 0;
  entries[i] = 0;
  pthread_rwlock_unlock(&symtab_rwlock);
  return(i);
} /* new_symbol */

void fill_in_nil_atom_glob_val()
{
  pthread_rwlock_wrlock(&symtab_rwlock);
  symtab[0].glob_val = (dlong *)(nil_atom);
  symtab[1].glob_val = (dlong *)(nil_atom);
  symtab[2].glob_val = (dlong *)(nil_atom);
  pthread_rwlock_unlock(&symtab_rwlock);
} /* fill_in_nil_atom_glob_val */

codep sym_codep(dlong arity, char *name, dlong modnr)
{
  return(entries[sym_lookup(arity,name,modnr)]);
} /* sym_codep */

/* printing of the code
 */

type_opcode next_instruction(codep from)
{
type_opcode opc;

#ifdef THREADED
{
    void *adr = *(void **)from;
    dlong j;
    for (j = 0; j < NR_OF_OPCODES; j++)
      if (adr == instr_labels[j])
        {
          opc = j;
          j = NR_OF_OPCODES;
        }
}
#endif
#ifdef SWITCH
      opc = *(type_opcode *)from;
#endif
      return(opc);
} /* next_instruction */


dlong module_from_index(dlong i)
{
  dlong res;
  res = make_atom(symtab[i].module);
  return res;
} /* module_from_index */

static void print_string(dlong l, char *p)
{
  while (l-- > 0)
    printf("%c",*p++);
} /* print_string */

static void print_bigint(dlong l, dlong *p)
{
  if (l < 0)
    {
      l = -l;
      printf("-");
    }
  while (l-- > 0)
    printf("%ld",*p++);
} /* print_string */

static void print_code(codep locpc) 
{
  type_opcode opc;
  dlong len = 20000;

  while (locpc && (len--))
    {
      printf("%6ld ",(dlong)(locpc - codezone));
      printf("%lx ",(unsigned dlong)locpc);
      opc = next_instruction(locpc);
      printf("%s ",instr_name[opc]);
      switch (opc)
	{
#include "print_instrs.h"

	case end_clauses:
	  printf("\n");
	  return;

	default:
	  printf("locpc = %ld - unknown\n",(dlong)(opc));
	  return;
	}
    }
} /* print_code */


void print_code_for_pred(dlong *p)
{
  dlong symbol;
  codep pc;

  deref(p);

  if (is_atom(p))
  {
    symbol = get_atom_index(p);
    pc = main_entrypoint(symbol);
  }
  else
    if (is_struct(p))
    {
      p = get_struct_pointer(p);
      symbol = get_funct_symb(*p);
      pc = main_entrypoint(symbol);
    }
    else
      if (is_smallint(p))
      {
        dlong i;
        i = get_smallint(p);
	if (i < 0)
	  {
	    i = -i;
	    pthread_rwlock_rdlock(&symtab_rwlock);
	    if ((i < 0) ||(i >= numb_symbs))
	      {
		pthread_rwlock_unlock(&symtab_rwlock);
		return;
	      }
	    pthread_rwlock_unlock(&symtab_rwlock);
	    fprintf(stderr,"%s/%ld\n",symtab[i].name,symtab[i].arity);
	    pc = entries[i];
	  }
	else
	  {
	    dlong index;
	    find_pred_index(codezone+i,&index);
	    fprintf(stderr,"%s/%ld\n",symtab[index].name,symtab[index].arity);
	    pc = entries[index];
	  }
      }

  if (pc == 0)  // || points_to_unknown_call(pc))
    return;

  print_code(pc);   

} /* print_code_for_pred */

void set_no_representation_sharing(dlong *p)
{
  dlong symbol;

  deref(p);

  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      symbol = get_funct_symb(*p);
      set_norepshar(symbol);
    }

} /* set_no_representation_sharing */


/* load_one_predicate: reads instructions from a file - for one pred - until
   end_clauses - second argument is first unused label
*/

#define reset_for_instruction_compression opcode = opcodeminus1 = opcodeminus2 = fake_entry

#define update_opcodes(newopc) \
        {*(type_opcode *)locpc = newopc; \
        opcodeminus1 = opcodeminus2; \
        opcodeminus2 = fake_entry; \
        opcode = newopc; \
        locpc += newopc##_##len; \
        instrs--;}

#define update_last_opcode(newopc) \
        {*(type_opcode *)locpc = newopc; \
        opcode = newopc; \
        locpc += newopc##_##len;}

#include "equivalent_opcode.h"

static dlong end_of_chain(dlong lab, dlong profile_extra)
{
  dlong labp;
  codep pc2;
  type_opcode opc2;

  while (1)
    {
      if (lab < 0) return(lab);
      labp =  (dlong)labeltabel[lab] + profile_extra;
      pc2 = (codep)labp;
      opc2 = *(type_opcode *)pc2;
      if (opc2 != jump) return(lab);
      lab = *(dlong *)(arg_place(pc2,1,jump));
    }

} /* end_of_chain */

static dlong load_one_predicate(FILE *f, dlong nrlabs, dlong j, dlong compactcode, dlong setprotected)
{
  char operation[50];
  dlong opcode, opcodeminus1, opcodeminus2; /* last 2 for instruction compression */
  dlong domore = 1;
  codep pc = topofcode;
  dlong *hashtable = 0;
  struct tempindex *tempindextable = 0;
  dlong tempindextop = 0;
  dlong hashsize, pred_arity, size;
  dlong index_fail_label;
  codep startcode = topofcode;
  type_opcode equi_op;
  dlong profile_extra;
  dlong ret;

  doing_profiling = instrs = 0;
  if ((!setprotected) && precise_profile_call_site_opt)
    doing_profiling = 1;

#define locpc topofcode

  instrs++;

  if (!labeltabel)
    enlarge_labeltabel(0);
  labeltabel[0] = 0; /* XXX do this better later */
  opcode = opcodeminus1 = opcodeminus2 = fake_entry;
  while (domore)
    {

      if (locpc > (codezone+CODEZONEL-100-doing_profiling*instrs*profile_instr_len)) /* 100 because I can afford it */
	return(0);


    /* just before reading a new opcode, we want to do instruction
       compression if appropriate
       all compressions are based on at most the last 2 instructions right now
       compression and specialization code is generated in instructions.pl
       probably could have used macros as well ...
    */


      if ((peep_spec & 0x1) == 0) goto no_specialization;

      switch (opcode)
	{

#include "specialize.h"
	} /* end specialization */

    no_specialization:
      if ((peep_spec & 0x2) == 0) goto no_peephole;
      
      switch (equivalent_opcode(opcode)) // moet beter
	{

#include "compress.h"

	} /* end peephole optimisation */

    no_peephole:

    opcodeminus2 = opcodeminus1;
    opcodeminus1 = equivalent_opcode(opcode);

      ret = fscanf(f,"%ld ",&opcode);
      if (! compactcode)
	ret = fscanf(f,"%s ",operation);
      /* fprintf(stdout,"%s %ld\n",operation,opcode); */

      /* fill in the opcode, even for THREADED
	 the label addres is filled in later */
      *(type_opcode *)locpc = equivalent_opcode(opcode);
      instrs++;

      if (opcode == builtin_traverselist_rev_0)
	{
	  // first remove allocate getlist unipvar unitvar
	  instrs = instrs - 4;
	  topofcode = topofcode -
	    allocate_len - getlist_len - unipvar_len - unitvar_len;
	  // now put the instruction
	  *(type_opcode *)locpc = builtin_traverselist_rev_0;
	}
      
      switch (opcode)
	{

	case label:
	  {
	    dlong a1;

	    /* pseudo - put label in tabel */
	    ret = fscanf(f,"%ld",&a1);
	    if (a1 >= MAXLABEL)
	      enlarge_labeltabel(a1);
	    instrs--;
	    labeltabel[a1] = topofcode + doing_profiling*instrs*profile_instr_len;
	    break;
	  }

	case end_clauses :
	  domore = 0;
	  topofcode += end_clauses_len;
	  break;


	case cases:
	  {
            dlong label_u, l_arity, l_size;
            /* pseudo */
	    instrs--;
            ret = fscanf(f,"%ld %ld %ld",&label_u,&l_arity,&l_size);
            index_fail_label = label_u;
            pred_arity = l_arity; size = l_size;
            hashsize = switch_hash_size(size);
            if (topofcode+ hashsize *sizeof(dlong)   > (codezone+CODEZONEL))
	      return(0); // fatal_message("not enough code space");

            hashtable = (dlong *)(topofcode + switchonbound_len);
            memset(hashtable, 0, hashsize*sizeof(dlong));

            tempindextable = (struct tempindex *)malloc((size+1)*sizeof(struct tempindex));
            tempindextop = 1;
            break;
	  }

	case arglabel:
	  /* pseudo */
	  instrs--;
          {
            char c, n[10];
            dlong hashvalue;
            dlong cis, togoto;
	    tempindextable[tempindextop].strlen = 0;
            /* just to skip it */
            ret = fscanf(f,"%s ",n);
            c = n[0];
            if (c == 'l')


              {
                ret = fscanf(f,"%s %ld",n,&togoto);
                hashvalue = HASH_LIST;
                tempindextable[tempindextop].type = SW_LIST;
                tempindextable[tempindextop].value.int_value = 0;
              }
            else if (c == 'f')
	      {
		double fl;
		{
		  char s[100];
		  ret = fscanf(f,"%s ",s);
		  fl = atof(s);
		}
		ret = fscanf(f,"%ld",&togoto);
		hashvalue = (dlong)fl;
		tempindextable[tempindextop].type = SW_REAL;
		tempindextable[tempindextop].value.float_value = fl;
	      }
	    else if (c == 'c')
	      {
		ret = fscanf(f,"%ld %ld",&cis,&togoto);
		tempindextable[tempindextop].type = SW_ATOM;
		hashvalue = tempsymbtab[cis];
		tempindextable[tempindextop].value.int_value = make_atom(tempsymbtab[cis]);
	      }
	    else if (c == 'i')
	      {
		ret = fscanf(f,"%ld %ld",&cis,&togoto);
		tempindextable[tempindextop].type = SW_INT;
		tempindextable[tempindextop].value.int_value = hashvalue = cis;
	      }
	    else if (c == 's')
	      {
		ret = fscanf(f,"%ld %ld",&cis,&togoto);
		tempindextable[tempindextop].type = SW_STRUCT;
		hashvalue = tempsymbtab[cis];
		tempindextable[tempindextop].value.int_value = make_funct(tempsymbtab[cis]);
	      }
	    else if (c == 't') /* string */
	      {
		dlong len;
		char *pc;
		ret = fscanf(f,"%ld \"",&len);
		tempindextable[tempindextop].strlen = len;
		tempindextable[tempindextop].type = SW_STRING;
		if (len)
		  {
		    pc = tempindextable[tempindextop].value.string_value = malloc((len+1)*sizeof(char));
		    if (! pc)
		      fprintf(stderr,"scream string_value\n");
		    while (len--) { ret = fscanf(f,"%c",pc); pc++; }
		  }
		ret = fscanf(f,"\" %ld", &togoto);
		hashvalue = string_hash(tempindextable[tempindextop].strlen,
					 tempindextable[tempindextop].value.string_value);	
	      }
	    else if (c == 'h') /* char */
	      {
		dlong cint;
		ret = fscanf(f,"%ld %ld",&cint,&togoto);
                tempindextable[tempindextop].type = SW_CHAR;
                tempindextable[tempindextop].value.char_value =
                  hashvalue = cint;
	      }
	    else 
	      fatal_message("Bad descriptor in cases");

	    hashvalue = hashvalue & (hashsize-1);

            if (hashvalue < 0)
              hashvalue = - hashvalue;
            tempindextable[tempindextop].hashvalue = hashvalue;
            tempindextable[tempindextop].togoto = togoto;
            tempindextable[tempindextop].next_same_hash = 0;
            tempindextable[tempindextop].to_relocate = 0;
            tempindextable[tempindextop].first = 0;
            if (hashtable[hashvalue])
              {
                dlong i = hashtable[hashvalue];
                tempindextable[i].next_same_hash = tempindextop;
              }
            hashtable[hashvalue] = tempindextop;
            tempindextop++;
            break;
	  }

#include "loading.h"


	default :
	invalid_input:
	  exit_mess("bad opcode while loading %ld\n",(dlong)opcode);
	}
    }

#undef locpc

  /* if we want this piece of code profiled, then this is the time to
     enhance the code with profiling instructions - first block copy
     the code; then copy it back while inserting the profile instruction
  */

  if (doing_profiling)
    {
      /* code is in from startcode to current topofcode
	 we allocate enough space to copy it into temporarily
	 we check whether there will be enough space later on
	 
      */

      codep savezone;
      dlong len = topofcode-startcode;
      savezone = malloc((len+1)*(sizeof(byte)));
      memcpy(savezone,startcode,len);
      pc = savezone;
      topofcode = startcode;
      while (len)
	{
	  /* fill in a profile_instr */
	  *(type_opcode *)topofcode = profile_instr;
	  *(count_type*)(arg_place(topofcode,1,profile_instr)) = 0;
	  *(count_type*)(arg_place(topofcode,2,profile_instr)) = 0;
	  topofcode += profile_instr_len;

	  /* copy one instruction verbatim */
	  /* opcode is still byte code */
	  switch (*(type_opcode *)pc)
	    {
#include "move_instructions_profiling.h"

	    case end_clauses:
	      topofcode -= profile_instr_len;
	      *(type_opcode *)topofcode = end_clauses;
	      topofcode += end_clauses_len;
	      len = 0;
	    }
	}
      free(savezone);
    }
  
  pc = startcode;

  if (doing_profiling)
    profile_extra = -profile_instr_len;
  else profile_extra = 0;

  while (pc != topofcode)
    {
      dlong lab;
      type_opcode opcode;

      opcode = *(type_opcode *)pc;

      // fprintf(stdout,"%s\n",instr_name[opcode]);
      fill_opcode(pc,opcode);
      switch (opcode)
	{
	case end_clauses:
	  return(1);

#define locpc pc
#include "fill_labels.h"
#undef locpc
	default:
	  exit_mess("bad opcode during fill_labels %ld\n",(dlong)*(type_opcode *)pc);
	}
      
    }

  return(0);
} /* load_one_predicate */

typedef dlong *tmp_mod_table_type;

static tmp_mod_table_type load_modules(FILE *f)
{
  dlong i,len,nrofmods,nr,j;
  char namebuf[1024];
  tmp_mod_table_type tmp_mod_table;
  dlong ret;

  ret = fscanf(f,"%ld",&nrofmods);
  tmp_mod_table = malloc(nrofmods*sizeof(dlong));
  if (! tmp_mod_table)
    fatal_message("could not allocate temporary module table");
  i = nrofmods;
  while (i)
    {
      ret = fscanf(f,"%ld %ld",&nr,&len);
      namebuf[0] = fgetc(f); /* skip a space  */
      j = 0; 
      while (j < len) 
  	namebuf[j++] = fgetc(f); 
      namebuf[j] = 0;
      tmp_mod_table[nr] = sym_lookup(0,namebuf,USER_MOD);
      i--;
    }

  return(tmp_mod_table);
} /* load_modules */

boolean load_compiled_file(char *filename, dlong setprotected)
{
  FILE *f;
  dlong i,nrlabs, nrofsymbs, index, arity, nrpreds, len, save_nrpreds, save_nrofpredstarts;
  dlong modnr,versionnumber,compactcode;
  char symbname[1024], operation[100];
  static dlong prev_numb_symbs = 0;
  tmp_mod_table_type tmp_mod_table;
  codep save_topofcode;
  codep *tempentries;
  dlong *tempprednr;
  dlong askagain;
  dlong keepoverwriting;
  dlong ignore_return;

  //  fprintf(stderr, "LOADING %s\n",filename);

  f = fopen(filename,"r"); /* filename must contain a compiled program */

  if (! f) return(FILENOTOPEN);

  /* structure of the file is as follows:
   * 
   *      versionnr
   *      compactcode
   *      nrmods
   *      nr length name --- for each module
   *      nratoms
   *      nr modnr arity length name
   * 
   * 
   *   then follow the compiled predicates
   * 
   *   predicate q 0 0
   *   label pred 1
   *   ...
   *   end_clauses
   * 
   * 
   *   some of this will change - so don't take this too literaly !
   */

  if (!compare_version(f))
    {
      fclose(f);
      return(BAD_VERSION_NUMBER);
    }
  ignore_return = fscanf(f,"%ld",&compactcode);
  /* 0 means full - 1 means compact - anything else is wrong */
  if ((compactcode != 0) && (compactcode != 1))
    {
      fclose(f);
      return(BAD_VERSION_NUMBER);
    }

  tmp_mod_table = load_modules(f);

  ignore_return = fscanf(f,"%ld",&nrofsymbs);

  tempsymbtab = (dlong *)(malloc(nrofsymbs*sizeof(dlong)));
  if (!tempsymbtab)
    fatal_message("malloc failed during consulting");

  index = 0;
  while (nrofsymbs--)
  {
    ignore_return = fscanf(f,"%ld %ld %ld %ld",&index,&modnr,&arity,&len);
    i = 0;
    symbname[0] = fgetc(f); /* skip a space */
    while (len--)
      symbname[i++] = fgetc(f);
    symbname[i] = 0;
    i = tempsymbtab[index] = sym_lookup(arity,symbname,tmp_mod_table[modnr]);
  }

  free(tmp_mod_table);

  ignore_return = fscanf(f,"%ld",&nrpreds);

  tempentries = (codep *)(malloc((nrpreds+1)*sizeof(codep)));
  tempprednr = (dlong *)(malloc((nrpreds+1)*sizeof(dlong)));
  if ((!tempentries) || (!tempprednr))
    fatal_message("malloc failed during consulting");

  save_topofcode = topofcode;
  save_nrpreds = nrpreds;
  save_nrofpredstarts = nrofpredstarts;

  pthread_mutex_lock(&load_compiled_file_mutex);
  while (nrpreds--)
  {
    dlong j;
    codep pc;
    dlong res;

    if (compactcode)
      ignore_return = fscanf(f,"%ld %ld",&i,&nrlabs);
    else
      ignore_return = fscanf(f,"%s %ld %ld",operation,&i,&nrlabs);
    /* operation == "predicate" */
    j = tempsymbtab[i];
    //    fprintf(stderr,"loading %s/%ld\n",symtab[j].name,symtab[j].arity);

    tempentries[nrpreds] = topofcode;
    tempprednr[nrpreds] = j;

    res = load_one_predicate(f,nrlabs,j,compactcode,setprotected);
    if (! res)
      goto notenoughspaceexit;

    j = 0;
    while (j < active_yvar_zone_current)
    {
      codep p;
      dlong i;
      p = (codep)active_yvar_zone[j++];
      i = active_yvar_zone[j++];
      *(dlong *)p = (topofcode-p);
      *(dlong *)topofcode = i;
      topofcode = (codep)(((dlong *)topofcode) + 1);
      while (i--)
      {
        *(dlong *)topofcode = active_yvar_zone[j++];
        topofcode = (codep)(((dlong *)topofcode) + 1);
      }
    }
    active_yvar_zone_current = 0;
    fill_predstarts(topofcode);
  }

  /* the following is a gross overestimate */
  if (not_enough_space_for_n_unknown(numb_symbs-prev_numb_symbs))
    goto notenoughspaceexit;

  askagain = 1;
  keepoverwriting = 0;
  nrpreds = save_nrpreds;
  while (nrpreds--)
  {
    codep pc;
    dlong answer;
    dlong overwriteit = 0;
    dlong givemessage;
    dlong i = tempprednr[nrpreds];
    pc = entries[i];

    if (overwrite_without_asking ||
        (pc == 0) ||
        (points_to_unknown_call(pc)) ||
        (! is_protected(i))) overwriteit = 1;
    else
    {
      if (! askagain) overwriteit = keepoverwriting;
      else
      {
        pthread_rwlock_rdlock(&symtab_rwlock);
        fprintf(stderr,"Protected predicate %s/%ld from module %s not redefined.\n",
            symtab[i].name, symtab[i].arity, symtab[symtab[i].module].name);
        pthread_rwlock_unlock(&symtab_rwlock);
        fprintf(stderr,"           If you want that, use the -O option\n");
        overwriteit = 0;
      }
    }

    if (very_verbose_mode && overwriteit && is_protected(i))
    {
      pthread_rwlock_rdlock(&symtab_rwlock);
      fprintf(stderr,"Overwrite of predicate %s/%ld from module %s\n",
          symtab[i].name,
          symtab[i].arity,
          symtab[symtab[i].module].name);
      pthread_rwlock_unlock(&symtab_rwlock);
    }

    if (setprotected)
      set_protected(i);
    else if (overwriteit)
      set_unprotected(i);
    if (overwriteit)
      entries[i] = tempentries[nrpreds];
  }

  while (prev_numb_symbs != numb_symbs)
  {
    codep pc;
    pc = entries[prev_numb_symbs];
    if (! pc)
      make_pred_without_clauses(prev_numb_symbs);
    prev_numb_symbs++;
  }

  free(tempentries);
  tempprednr[0] = tempprednr[save_nrpreds];
  tempprednr[save_nrpreds] = 0; /* a sentinel */
  free(tempprednr); // until we want to reuse it ...
  free(tempsymbtab);
  pthread_mutex_unlock(&load_compiled_file_mutex);
  fclose(f);
  return(ALLOK);

 notenoughspaceexit:
  fprintf(stderr,"Unexpected notenoughspaceexit\n");
  free(tempentries);
  free(tempprednr);
  free(tempsymbtab);
  topofcode = save_topofcode;
  nrofpredstarts = save_nrofpredstarts;
  active_yvar_zone_current = 0;
  pthread_mutex_unlock(&load_compiled_file_mutex);
  fclose(f);
  return(NOTENOUGHSPACE);

} /* load_compiled_file */


/*************************************************************************/
/*
   Builtins for exception handling using a Prolog-based catch-throw

              $$set_scope_marker/0
              $$unwind_stack/0

   From CW report 98: A 20' implementation of catch and throw

   See comments in dp_builtin: set_scope_marker was removed on Wed Jun  2 14:39:01 CEST 2010

*/

void unwind_stack(struct machine *machp)
{
  /* currently, the code for catch ensures that the cp points to the
     instruction builtin_end_catch_1 */

  register codep cp;
  register dlong *e, *b;
  register void* relevant = catchinstr;

   e = ereg;
   cp = contpreg;

   //   while ( (! (points_to_builtin_end_catch_1(cp))) && (e != begin_ls))
   while ( starcp(cp) != relevant )
     {
       cp = (codep)(*(e+1));
       e = (dlong *)(*e);
     }

   /* now find the corresponding breg */
   b = breg;
   while (((CP_P) b)->e <= e)
     b = ((CP_P) b)->b;
   breg = b;
} /* unwind_stack */



codep unwind_stack_callcc(struct machine *machp)
{
  /* currently, the code for reset ensures that the cp points to the
     instruction builtin_end_catch_1 */

  register codep cp;
  register dlong *e, *b;
  register void* relevant = after_callccinstr;
  //  dlong teller = 0;

   e = ereg;
   cp = contpreg;

   while ( (starcp(cp) != relevant ) && (e != begin_ls))
     {
       //       teller++;
       cp = (codep)(*(e+1));
       e = (dlong *)(*e);
     }

   if (e == begin_ls)
     return(0);

   //   fprintf(stderr,"unwind teller = %ld\n",teller);

   ereg = e;
   return(cp);

} /* unwind_stack_callcc */

dlong *find_intercept(struct machine *machp)
{
  /* currently, the code for reset ensures that the cp points to the
     instruction builtin_after_intercept_0 */

  register codep cp;
  register dlong *e, *b;
  register void* relevant = after_intercept_instr;

   e = ereg;
   cp = contpreg;

   while ( (starcp(cp) != relevant ) && (e != begin_ls))
     {
       cp = (codep)(*(e+1));
       e = (dlong *)(*e);
     }

   if (e == begin_ls)
     return(0);

   return(e);

} /* find_intercept */

