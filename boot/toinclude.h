// #include <valgrind/memcheck.h>
// #include <linux/time.h>
// #include <linux/resource.h>
#include <sys/time.h>
#include <sys/times.h>
#include <sys/resource.h>
#include <unistd.h>
#include <stdio.h>
#ifdef MACBOOKPRO
#include <malloc/malloc.h>
#else
#include <malloc.h>
#endif

#ifdef MULTI_THREADED
#include <pthread.h>
#include <semaphore.h>
#else
#define __thread
#endif

#include "version.h"

#define udlong unsigned dlong
#define udshort unsigned dshort

#ifndef USE_GMP
#define mpz_t dlong *
#endif


#ifdef LINUX

#ifdef HW_P
#define ASM_PC asm ("bx")
#define REGISTER register
#else
#define ASM_PC
#define REGISTER
#endif

#define lochreg hreg
#define loctrreg trreg
#define locbreg breg

#endif

// needed in copyterm.c and logic_engines.c
#define FROM_HEAP 1
#define FROM_OUTSIDE (! FROM_HEAP)


#define safe_wakeup(a,b,c) { wakeup(a,b,c); }

#define USER_MOD 0
#define SYSTEM_MOD 1

#define SPARE_HEAP 2000
#define SPARE_TR 10
#define SPARE_LS 300
#define SPARE_CP 300

#ifdef PLAIN_SWITCH
#define SWITCH
#endif
#ifdef JUMP_TABLE
#define SWITCH
#endif

#ifdef PLAIN_SWITCH
#define INLINE
#else
#define INLINE inline
#endif

#define makeSzero WAMS = 0

#ifdef SWITCH
#define fill_fast(pc,r) *(type_opcode *)pc = r
#endif
#ifdef THREADED
#define fill_fast(pc,r) *(void **)(pc) = instr_labels[r]
#endif

#define SMALLINTRES 0
#define FLOATRES 1
#define BIGINTRES 2


#ifdef BITS32
#include "opcodes_32.h"
#define dshort short
#define dlong long
#define dfloat double
#endif

#ifdef BITS64
#include "opcodes_64.h"
#define dshort int
#define dlong long
#define dfloat long double
#endif


#define current_dbg stdout

typedef unsigned char byte;
typedef byte *codep;
typedef dlong sympointer;
typedef dlong boolean;

struct machine;

#define GLOBAL_CHARBUF_LEN 1024
extern dlong verbose_mode;
extern dlong atom_on, atom_default_prompt;
extern dlong very_verbose_mode;
extern dlong value_prompt;
extern char *instr_name[];
extern byte *codezone;
extern boolean load_compiled_file(char *, dlong);
extern void put_with_escape(FILE *,char);
extern void free_excess_bigints(struct machine *);

extern dlong number_of_symbols();
extern dlong code_size();
extern dlong code_gc(dlong, struct machine*);
extern dlong nil_atom;

extern dlong engine_toplevel_goal_0;

/* the following must be an illegal arity */
#define NOT_PROLOG_CALL -1

#define not_enough_space_for_n_unknown(n) (topofcode + (call_unkn_len*(n)) > (codezone+CODEZONEL))
extern dlong not_enough_space_for_cinter();

#define ALLOK 2
#define BAD_VERSION_NUMBER 3
#define NOTENOUGHSPACE 1
#define FILENOTOPEN 0

#define NOT_SPIED 0
#define SPIED 1


#define REPSHAR 0x8

extern char *protection_info;

#define set_protected(i)  protection_info[i] = (protection_info[i] & 0x1) | 0x2
#define set_unprotected(i)  protection_info[i] = (protection_info[i] & 0x1)
#define is_protected(i)  (protection_info[i] & 0x2)

#define set_norepshar(i) protection_info[i] = protection_info[i] | REPSHAR
#define get_norepshar(i) (protection_info[i] & REPSHAR)


struct syminfo
{ 
  char *name;
  dlong module;
  dlong *glob_val;
  dlong arity;
#ifdef MULTI_THREADED
  pthread_mutex_t flock ;
#endif
#ifdef PROFILING_CALLS
  dlong callcount;
#endif
} syminfo;


#ifdef PROFILING_WAM
#define unif_countplusplus unif_count++
#define rec_unif_countplusplus rec_unif_count++
#define unifintintplusplus unifintint++
#define unifatomatomplusplus unifatomatom++
#define unifdeldelplusplus unifdeldel++
#define unifrealrealplusplus unifrealreal++
#define unifstringstringplusplus unifstringstring++
#define unifvarotherplusplus unifvarother++
#define unifvarvarplusplus unifvarvar++
#define uniflistlistplusplus uniflistlist++
#define unifstructstructplusplus unifstructstruct++
#define unifdelotherplusplus unifdelother++
#endif

#ifdef PROFILING_DEBUG
#define unif_countplusplus
#define rec_unif_countplusplus
#define unifintintplusplus
#define unifatomatomplusplus
#define unifdeldelplusplus
#define unifstringstringplusplus
#define unifrealrealplusplus
#define unifvarotherplusplus
#define unifvarvarplusplus
#define uniflistlistplusplus
#define unifstructstructplusplus
#define unifdelotherplusplus
#endif

#ifdef NO_PROFILING
#define unif_countplusplus
#define rec_unif_countplusplus
#define unifintintplusplus
#define unifatomatomplusplus
#define unifdeldelplusplus
#define unifstringstringplusplus
#define unifrealrealplusplus
#define unifvarotherplusplus
#define unifvarvarplusplus
#define uniflistlistplusplus
#define unifstructstructplusplus
#define unifdelotherplusplus
#endif

#ifdef PROFILING_CALLS
#define unif_countplusplus
#define rec_unif_countplusplus
#define unifintintplusplus
#define unifatomatomplusplus
#define unifdeldelplusplus
#define unifstringstringplusplus
#define unifrealrealplusplus
#define unifvarotherplusplus
#define unifvarvarplusplus
#define uniflistlistplusplus
#define unifstructstructplusplus
#define unifdelotherplusplus
#endif

#define MODULO 0
#define DIVIDE 1

// using a bit in the tr field of a choicepoint
// for indicating it has been collected already
// used in gc.c and in tidy_trail

#define cleantr(t) (dlong *)(((dlong)(t)) & ~0x1)
#define iscleantr(t) (((dlong)(t) & 0x1)==0)
#define marktr(t) (((dlong)(t)) | 0x1)

extern struct syminfo *symtab;

extern codep *entries;

#define main_entrypoint(i)  entries[i]


#define arg_place(where,N,I) (where + I##_arg_##N)

#ifdef ALIGNED_DOUBLE

typedef union
{
  struct two_ints{ dlong i1,i2; } two_ints;
  dfloat f;
} conversion_type;


#define store_double_arg(where, ff)\
{ conversion_type ct;\
	   ct.f = ff; \
           *(dlong *) (where) = ct.two_ints.i1;\
           *(((dlong *) (where)) +1) = ct.two_ints.i2;\
}


#define get_double_arg(ff, where) \
{ conversion_type ct;\
           ct.two_ints.i1 = *(dlong*)(where);\
           ct.two_ints.i2 = *(((dlong*)(where))+1);\
           ff = ct.f; \
}


#else

#define store_double_arg(where,ff)\
         *(dfloat *)(where) = (dfloat) ff
#define get_double_arg(ff, where) ff = *(dfloat *)(where)

#endif

#define fill_try_retry_trust_code(where,what,arity,label,len)  \
          {  *where = what;                                    \
             *(arity_type *)(where + what##_1) = (arity_type)arity; \
             *(label_type *)(where + what##_2) = (label_type)label;   \
             where += len;                                     \
          }

#define HASH_LIST 19

#ifdef THREADED
#define fill_opcode(p,opc) *(void **)p = instr_labels[opc]
#define goto_more_exec goto **(void **)locpc


extern void *instr_labels[];
#define points_to_unknown_call(pc)   (*(void **)pc == instr_labels[call_unkn])
#define points_to_or_retry_trust(pc)   ((*(void **)pc == instr_labels[retrymeorelse]) || (*(void **)pc == instr_labels[trustmeorelsefail]))
#define points_to_builtin_end_catch_1(pc) (*(void **)pc == instr_labels[builtin_end_catch_1])
#define starcp(cp) (*(void **)cp)
#define catchinstr instr_labels[builtin_end_catch_1]
#define after_callccinstr instr_labels[builtin_after_callcc_0]
#define after_intercept_instr instr_labels[builtin_after_intercept_0]

#endif

#define goto_read_loop goto read_loop
#define goto_write_loop goto write_loop


#ifdef SWITCH
#define fill_opcode(p,opc) *(type_opcode *)p = opc
#define points_to_unknown_call(pc)   (*((type_opcode *)(pc)) == call_unkn)
#define points_to_or_retry_trust(pc)   ((*((type_opcode *)(pc)) == retrymeorelse) || (*((type_opcode *)(pc)) == trustmeorelsefail))
#define points_to_builtin_end_catch_1(pc) (*((type_opcode *)(pc)) == builtin_end_catch_1)
#define starcp(cp) (*((type_opcode *)(cp)))
#define catchinstr (void *)builtin_end_catch_1
#define after_callccinstr (void *)builtin_after_callcc_0
#define after_intercept_instr (void *)builtin_after_intercept_0


#ifdef PLAIN_SWITCH
#define goto_more_exec goto more_exec
#endif

#ifdef JUMP_TABLE
#define goto_more_exec goto **(void **)(instr_labels+ *(type_opcode *)locpc)
#endif

//#ifdef JUMP_TABLE
//#define goto_more_exec goto **(void **)(local_instr_labels+ *(type_opcode *)locpc)
//#endif

#endif

#define FILE_READ   0
#define FILE_WRITE  1
#define FILE_APPEND 2
#define FILE_ANY    3
// #define FILE_READ_STRING 4
#define FILE_READ_ATOM 4


struct file_struct
{
  char ungetnr;
  char c1; char c2;  /* we do two ungetchars at most */
  char opentype;     /* READ/WRITE/APPEND */
  FILE *fp;
  dlong f_symbol;
  dlong line_number;
  dlong colnr;
  dlong endoffile;
} file_struct;


#define MAX_SHAR_INIT 500

#define ARGS 255
#define MAX_NR_OPEN_FILES 256
// #define MAX_NR_OPEN_FILES 6

/* the following is needed because attvars use tag-on-data */
#define underef(p) if (is_attributed(p)) p = get_attvar_pointer(p)

#define freeze_heap() if (locmach.FH > ((CP_P)locbreg)->h) ((CP_P)locbreg)->h = locmach.FH
#define adapt_freeze_hreg(newtop) { locmach.BH = locmach.FH = newtop; ((CP_P)locbreg)->h = newtop; }


struct machine_opts
{
  dlong heap_size;
  dlong ls_size;
  dlong cp_size;
  dlong tr_size;
  dlong wokengoals;
  dlong initial_heap_size;
};

typedef struct {
  dlong *   first_chunk ;   /* chunk in which solution list starts the solution list starts at offset +1 */
  dlong *   tail ;          /* tail of solution list - always a free var */
  dlong *   current_chunk ; /* most recent alloc chunk for this solution list */
  dlong *   top_of_chunk ;  /* where next template can be copied to points inside current_chunk */
  dlong *   limit_of_chunk ;  /* points to last cell in chunk that can be used */
  dlong size ;              /* size is the size of the solution list - init = 1
                               when entry not active, size = the next free entry
                               the last free entry, has size = -1, for overflow
                               checking
			    */
  dlong barrier;
} findall_solution_list ;


struct machine
{
  dlong *H;
  dlong *shadow_limit_heap;
  dlong *TR;
  dlong *E;
  dlong *B;
  dlong *BH;
  codep CONTP;
  dlong *TOS;
  dlong *limit_cp;
  dlong *limit_ls;
  dlong *limit_trail;
  dlong excess_woken;
  dlong Areg[ARGS];
  dlong *FH;
  codep P;
  dlong *begin_heap;
  dlong *end_heap;
  dlong *limit_heap;
  dlong *begin_trail;
  dlong *end_trail;
  dlong *begin_ls;
  dlong *end_ls;
  dlong *begin_cp;
  dlong *end_cp;
  dlong *begin_woken_goals;
  dlong nr_woken_goals;
  dlong file_in_nr;
  dlong file_out_nr;
  char *heap_mark_array;
  char *ls_mark_array;
  dlong nr_open_files;
  struct file_struct openfiles[MAX_NR_OPEN_FILES];
  dlong nr_heapgc;
  dlong nr_minorheapgc;
  dlong total_collected1;  /* up to 100.000.000 */
  dlong total_collected2;  /* the rest */
  dlong total_sharedgain1;  /* up to 100.000.000 */
  dlong total_sharedgain2;  /* the rest */
  dlong total_gc_time;
  dlong total_repshar_time;
  dlong nr_heapexp;
  dlong nr_lsexp;
  dlong nr_cpexp;
  dlong nr_trexp;
  dlong time_mm;
  dlong nr_excess_bigints;
  dlong **excess_bigints;
  dlong max_excess_bigints;
  dlong total_size_excess_bigints;
  findall_solution_list *findall_solutions;
  dlong MAX_FINDALLS;
  dlong nextfree;

  dlong **unify_cyclic_stack;
  dlong **unify_cyclic_stack_top;
  dlong **unify_cyclic_stack_limit;
  dlong MAXUNIFYCYCLICSTACK;

  dlong varliststacksize;
  dlong **varliststack;
  dlong varliststacktop;

  dlong intro_representation_sharing;
  dlong *copy_heapbarrier;
  dlong drop_point;
  dlong memo_arity;
  dlong memo_atom;
  dlong memo_func;

  char *int_buffer;
  char *global_charbuf;

  dlong **copy_local_trail;

};

#ifdef MULTI_THREADED
struct message
{
  findall_solution_list* msg;
  dlong from_engine_index;
  struct message* next_msg;
};


struct inbox
{
  struct message* head;
  struct message* tail;
  dlong size;
  pthread_mutex_t mutex;
  sem_t new_push;
  int waiting_push;
  sem_t new_pop;
  int waiting_pop;
};



struct linked_engine
{
  struct engine* engine;
  struct linked_engine* next;
};



struct hub
{
  pthread_mutex_t mutex;

  dlong index;
  int cancelled;
  pthread_rwlock_t cancel_lock;

  // messaging
  struct inbox inbox;

  // linked engines
  struct linked_engine* engines_head;
  dlong engines_count;
  sem_t stopped_engines;
};


struct engine
{
  // Pointer the local copy of the original machine being used by execute_machine.
  struct machine* locmachp;

  pthread_mutex_t mutex;

  // ids & start goal
  dlong index;
  dlong parent_id;
  int cancelled;
  pthread_rwlock_t cancel_lock;
  sem_t* stop_signal;

  // messaging
  struct inbox inbox;

  findall_solution_list* start_goal;

  // linked engines
  struct linked_engine* engines_head;
  dlong engines_count;
  sem_t stopped_engines;
};
#else
struct engine
{
  // The local copy of the original machine being used by execute_machine.
  struct machine* locmachp;

  // ids & start goal
  dlong index;
  dlong parent_id;
  int cancelled;

  // messaging

  findall_solution_list* start_goal;

  // linked engines
  dlong engines_count;
};

#define pthread_mutex_lock(x)
#define pthread_mutex_unlock(x)
#endif

extern void show_info_cp(struct machine *, dlong *);
extern void make_foreign_pred(dlong, dlong *);
extern dlong builtin_2C(char **, struct machine *);
extern codep builtin_metacall(dlong *, struct machine *);
extern dlong compare_terms(dlong *,dlong *, struct machine*);

extern sympointer sym_lookup(dlong, char *, dlong);
extern codep sym_codep(dlong, char *, dlong);
extern void exit_mess(char *, dlong);
extern void fill_in_nil_atom_glob_val();
extern dlong get_cpu_time();

/*
Documentation outdated since 64-bit support

tag = 3 bits
pointers are aligned and have a 0 (on Pentium) as HIB

getting a pointer involves a shift left over 1
making a pointer involves a shift right over 1

except for plain references: they are not shifted
*/

#define REF0        0
#define STRING      1
#define SIMPLE      2
#define NUMB        3

#define REF1        4
#define ATT         5
#define STRUCT      6
#define LIST        7

/*
STRING SIMPLE NUMB are ordered and this is used in is_atomic
see also could_be_cyclic 


SIMPLE means: an atom, a char or a smallint
a SIMPLE tagged word is as follows:

bit4 == 0 -> word indicates an atom or a char
             for atom: indexfield of 21 bits + 7 "arity" bits + 1 ATOM_CHAR bit + SIMPLE (3)
	     for char: 16 bits for char + 5 spare bits + 7 "arity" bits + 1 ATOM_CHAR bit + SIMPLE (3)
bit4 == 1 --> word indicates a smallint of 28 bits - 2s complement

the arity in an atom == 127 (0x7f)
the arity in a char == 126 (0x7e)

a functor cell looks like an atom cell, but its arity is either == 0,
in which case the real arity is to be found from its index (and bigger than 124)
or it is in the arity field

a string header looks like an atom with arity == 125 (0x7d) and the
length of the string can be found in the "index"
*/

#define ATOM_ARITY 0x7f
#define CHAR_ARITY 0x7e
#define STRING_ARITY 0x7d

#define SMALLINT (SIMPLE | 0x8)

#define STRING_HEADER ((STRING_ARITY << 4) | SIMPLE)


/* the next two are the headers of numbers */
/* a cell with HINT or HREAL does not contain anything else */
#define BIGINT_HEADER        0
#define REAL_HEADER          1


#define ARITYTAGMASK     0x7ff

#define is_string_header(t)         (((dlong)t & ARITYTAGMASK) == STRING_HEADER)
#define is_functor_header(t)        ((maxitag(t) == SIMPLE) && (get_funct_arity(t) <= MAX_ARITY))

#define tag(p)                      (((dlong)p) & 0x7)
#define minitag(p)                  (((dlong)p) & 0x3)
#define maxitag(p)                  (((dlong)p) & 0xf)

#define is_simple(p)                (tag(p) == SIMPLE)

#define is_ref(t)                   (minitag(t) == REF0)
#define is_atomic(t)                ((tag(t) >= STRING) && (tag(t) <= NUMB))
#define is_list(t)                  (tag(t) == LIST)
#define is_struct(t)                (tag(t) == STRUCT)
#define is_compound(t)              (tag(t) >= STRUCT)

#define is_bigint(t)                ((tag(t) == NUMB) && (*(get_number_pointer(t)) == BIGINT_HEADER))
#define is_smallint(t)              (maxitag(t) == SMALLINT)
#define is_real(t)                  ((tag(t) == NUMB) && (*(get_number_pointer(t)) == REAL_HEADER))
#define is_atom(t)                  ( ((dlong)t & ARITYTAGMASK) == (SIMPLE | (ATOM_ARITY << 4)) )
#define is_char(t)                  ( ((dlong)t & ARITYTAGMASK) == (SIMPLE | (CHAR_ARITY << 4)) )

#define is_inumber(t)               (is_smallint(t) || is_bigint(t))

#define is_number(t)                (is_smallint(t) || (tag(t) == NUMB))


/* after the header, we have a cell with the length in words and the sign
   the sign is the lower bit: 1 = positive; 0 = negative; zero does not occur
   bit 2 (starting from 1) indicates whether it is out of heap or not
*/

#define ONHEAP 0
#define OUTOFHEAP 2

#define bigint_size(t)              (t >> 2)
#define bigint_positive(t)          ((t) & 0x1)
#define bigint_onheap(t)            (((t) & 0x2) == 0)
#define bigint_info_onheap(pos,len)        ((len << 2) + ONHEAP + (pos))
#define bigint_info_notonheap(pos,len)     ((len << 2) + OUTOFHEAP + (pos))

#define invert_bigint_sign(p,v)			\
  {			      \
  if (bigint_positive(v)) *p = v & (~0x1); \
  else *p = v | 0x1; \
  }

#ifdef USE_GMP
#define build_bigint_from_out_of_range_smallint(where,i,lochreg) \
{ \
*where = make_bigint_pointer(lochreg); \
*lochreg = BIGINT_HEADER; lochreg++; \
if (i < 0) { *lochreg = bigint_info_onheap(0,1); i = -i ; } \
else *lochreg = bigint_info_onheap(1,1); \
lochreg++; \
*lochreg = i; \
lochreg++; \
}
#else
#define build_bigint_from_out_of_range_smallint(where,i,lochreg) {*where = make_smallint(i);}
#endif

/* part that is depending on the 32-64 bits ... */

#ifdef BITS32

#define get_number_pointer(p)       ((dlong *)((unsigned int)(((dlong)p & ~0x7)) >> 1))
#define get_string_pointer(p)       ((dlong *)((unsigned int)(((dlong)p & ~0x7)) >> 1))

#define make_string_pointer(p)      (((dlong)(p) << 1) | STRING)

#define get_struct_pointer(p)       ((dlong *)((unsigned long)(((dlong)p & ~0x7)) >> 1))
#define get_attvar_pointer(p)       ((dlong *)((unsigned long)(((dlong)p & ~0x7)) >> 1))

#define get_list_pointer(p)         ((dlong *)((unsigned long)((~(dlong)p)) >> 1))
#define make_list(x)                (~((dlong)(x) << 1))

#define make_struct_p(p)            (((dlong)(p) << 1) | STRUCT)
#define make_attvar_p(p)            (((dlong)(p) << 1) | ATT)

#define make_number_pointer(p) (((dlong)(p) << 1) | NUMB)

#define MAXSMALLINT 134217727

#define MINSMALLINT -134217727

#define FLOAT_MININT -134217727.0

#define in_smallint_range(i)          ((i <= 134217727) && (i >= MINSMALLINT))
#define unsigned_in_smallint_range(i) (i <= 134217727)

INLINE static dlong add_str_len(dlong i)
{
  /* strings can have length 0 */
  /* the formula is wrong for non-32 bit machines */
  return((((i+3)>>2)<<2));
} /* additional_str_len */

INLINE static dlong add_bigint_len(dlong i)
{
  /* the formula is wrong for non-32 bit machines */
  if (i < 0) i = -i;
  return(i<<2);
} /* additional_bigint_len */

INLINE static dlong heapcells_needed_for_string(dlong i)
{
  /* strings can have length 0 */
  /* the formula is wrong for non-32 bit machines */
  return((((i+3)>>2)));
} /* heapcells_needed_for_string */


#endif // BITS32

#ifdef BITS64
#define get_number_pointer(p)       ((dlong *)((unsigned dlong)(((dlong)p & ~0x7)) ))
#define get_string_pointer(p)       ((dlong *)((unsigned dlong)(((dlong)p & ~0x7)) ))

#define make_string_pointer(p)      (((dlong)(p) ) | STRING)

#define get_struct_pointer(p)       ((dlong *)((unsigned long)(((dlong)p & ~0x7)) ))
#define get_attvar_pointer(p)       ((dlong *)((unsigned long)(((dlong)p & ~0x7)) ))

#define get_list_pointer(p)         ((dlong *)((unsigned long)((~(dlong)p)) ))
#define make_list(x)                (~((dlong)(x) ))

#define make_struct_p(p)            (((dlong)(p) ) | STRUCT)
#define make_attvar_p(p)            (((dlong)(p) ) | ATT)

#define make_number_pointer(p) (((dlong)(p) ) | NUMB)


#define MAXSMALLINT 576460752303423487

#define MINSMALLINT -576460752303423487

#define FLOAT_MININT -576460752303423487.0

#define in_smallint_range(i)          ((i <= MAXSMALLINT) && (i >= MINSMALLINT))
#define unsigned_in_smallint_range(i) (i <= MAXSMALLINT)


INLINE static dlong add_str_len(dlong i)
{
  /* strings can have length 0 */
  /* the formula is wrong for non-64 bit machines */
  return((((i+7)>>3)<<3));
} /* additional_str_len */

INLINE static dlong add_bigint_len(dlong i)
{
  /* the formula is wrong for non-64 bit machines */
  if (i < 0) i = -i;
  return(i<<3);
} /* additional_bigint_len */

INLINE static dlong heapcells_needed_for_string(dlong i)
{
  /* strings can have length 0 */
  /* the formula is wrong for non-64 bit machines */
  return((((i+7)>>3)));
} /* heapcells_needed_for_string */

#endif // BITS64




/* end part depending on 32-64 bits ... */


#define is_attributed(t)            (tag(t) == ATT)

#define is_var(t)                   (is_ref(t) || is_attributed(t))

#define is_string(t)                (tag(t) == STRING)

#define has_atom_tag(t)             (tag(t) == SIMPLE)

#define get_character(p)            (char)((dlong)p >> 16)
#define make_character(c)           ((c << 16) | SIMPLE | (CHAR_ARITY << 4))


#define get_string_val(p)           ((char *)(get_string_pointer(p) + 1))
#define get_len_from_string_header(t) ((dlong)t >> 16)
#define get_string_length(p)        ((*get_string_pointer(p)) >> 16)
#define make_string_header(l)       (l << 16) | STRING_HEADER
#define push_string(l,h,from)       { *h++ = (l << 16) | STRING_HEADER; \
                                      move_string(l,from,(char *)(h)); \
                                      h += heapcells_needed_for_string(l); }
#define build_string_header(l,p,h)  { *p = make_string_pointer(h); \
                                      *h++ = (l << 16) | STRING_HEADER; } 
#define build_string(l,p,h,from)    { build_string_header(l,p,h); \
                                      move_string(l,from,(char *)(h)); \
                                      h += heapcells_needed_for_string(l); }

#define old_get_atom_index(p)           ((dlong)p >> 11)
#define get_atom_name(p)            symtab[get_atom_index(p)].name
#define get_atom_module(p)          symtab[get_atom_index(p)].module
#define make_atom(at)               ((((dlong)at) << 11) | SIMPLE | (ATOM_ARITY << 4))

#define get_atom_index(p)  (((dlong)p >> 11) >= 0) ? ((dlong)p >> 11) : 3

// static INLINE dlong get_atom_index(dlong *p)
// {
//   dlong i = ((dlong)p >> 11);
//   if (i < 0) return(0);
//   return(i);
//   
// } /* get_atom_index */


/* MAX_ARITY must leave space for strings etc */

#define BIG_ARITY
#ifdef BIG_ARITY
#define MAX_ARITY  1000000008
#define MAX_REPRESENTABLE_ARITY 124
#define make_funct(at)              ((symtab[at].arity<=MAX_REPRESENTABLE_ARITY) ? ((((dlong)at) << 11) | (symtab[at].arity << 4) | SIMPLE) : ((((dlong)at) << 11) | SIMPLE))
// #define get_funct_arity(i)          (symtab[((dlong)i) >> 11].arity)
#define get_funct_arity(i)          (((i >> 4) & 0x7f) ? ((i >> 4) & 0x7f) : (symtab[((dlong)i) >> 11].arity))
#else
#define MAX_ARITY 124
#define make_funct(at)              ((((dlong)at) << 11) | (symtab[at].arity << 4) | SIMPLE)
#define get_funct_arity(i)          ((i >> 4) & 0x7f)
#endif


INLINE static dlong equal_strings(dlong l, char *s1, char *s2)
{
  while (l--)
    if (*s1++ != *s2++) return(0);
  return(1);
} /* equal_strings */

INLINE static void move_string(dlong l, char *from, char *to)
{
  while (l--)
    *to++ = *from++;
} /* move_string */


#define get_funct_symb(i)           (i >> 11)
#define get_funct_name(i)           symtab[get_funct_symb(i)].name
#define get_funct_module(i)         symtab[get_funct_symb(i)].module

#define get_smallint(p)             ((dlong)p >> 4)

#define build_smallint(p,i)         *p = make_smallint(i)

#define make_smallint(i)            (((i) << 4) | SMALLINT)

#define SIZE_OF_FLOAT sizeof(dfloat)/sizeof(dlong)

#define SIZE_OF_ATT 9

#define INS1      0
#define EVENT2    1
#define DOM1      2
#define DOM2      3
#define BOUND1    4
#define DOM_ANY1  5
#define DOM_ANY2  6
#define PASATTR   7
#define FDDOMAIN  8

#define   new_attvar(where,i1,e2,d1,d2,b1,da1,da2,pa,fdd) \
   where[INS1] = i1;       \
   where[EVENT2] = e2;     \
   where[DOM1] = d1;       \
   where[DOM2] = d2;       \
   where[BOUND1] = b1;     \
   where[DOM_ANY1] = da1;  \
   where[DOM_ANY2] = da2;  \
   where[PASATTR] = pa;    \
   where[FDDOMAIN] = fdd;


#ifdef ALIGNED_DOUBLE

INLINE dfloat get_aligned_real(dlong *);
#define get_real(p)            get_aligned_real((get_number_pointer(p)+1))

INLINE void push_aligned_real(dlong *, dfloat);
#define push_real(h,f)         *h++ = REAL_HEADER; push_aligned_real(h,f); h += SIZE_OF_FLOAT

#else

#define get_real(p)            *(dfloat *)(get_number_pointer(p) +1)
#define push_real(h,f)         *h++ = REAL_HEADER; *(dfloat *)h = (dfloat)(f); h += SIZE_OF_FLOAT

#endif


#define build_real(p,f,h)      {*p = make_number_pointer(h); push_real(h,f); }

#define make_bigint_pointer(p) make_number_pointer(p)


static INLINE dlong get_number_of_woken_goals(struct machine *machp)
{
  dlong i;
  i = machp->excess_woken;
  return(i>>1);
} /* get_number_of_woken_goals */

static INLINE void set_number_of_woken_goals(dlong i, struct machine *machp)
{
  dlong j;
  j = (machp->excess_woken) & 0x1;
  machp->excess_woken = (i<<1) + j;
} /* set_number_of_woken_goals */



#ifdef USE_GMP
#define multiply_overflow_escape(i,j,res,label) \
{ \
    dfloat f; \
    f = (dfloat)i * j; \
    if ((f > (dfloat)MAXSMALLINT) || (f < FLOAT_MININT)) \
      goto label; \
    res = i*j; \
}
#else
#define multiply_overflow_escape(i,j,res,label) res = i*j
#endif



#define deref3(p0,p,undef)    \
{ dlong *X;                   \
  p0 = 0;                     \
   while (is_ref(p))          \
      { X = (dlong *)*p ;     \
        if (X == p) undef;    \
        p0 = p; p = X;        \
      } }



#define deref2(p,undef)       \
{ register dlong *X;                   \
   while (is_ref(p))          \
      { X = (dlong *)*p ;     \
        if (X == p) undef;    \
        p = X;                \
      } }

#define deref_in(p)       \
{ while (is_ref(p))          \
      { p = (dlong *)*p ;     \
      } }



#define deref(p) deref2(p,break)


/* #define goto_fail_label { goto fail_label; } */
#define goto_fail_label { failure; goto_more_exec; }

#define failure { locpc = (codep)(((struct choicepoint *)locbreg)->failurecont); }



#define noye_trail(loc,bh,trreg,newval) \
if (loc >= bh) \
    *loc = (dlong)newval; \
else \
{ if (!is_ref(*loc) || ((dlong *)(*loc) < bh)) \
    { trail_overflow_check(trreg,trail_limit,macro_deal_with_tr_overflow()) ; \
      *trreg++ = *loc; /* value */ \
      *trreg++ = (dlong)loc; /* location */ \
    } \
  *loc = (dlong)lochreg; \
  *lochreg = (dlong)newval; \
  lochreg++; \
}


/*  #define noye_trail(loc,bh,trreg,newval) \ */
/*  { value_trail(loc,bh,trreg); \ */
/*    *loc = (dlong)newval; }  */

/* the u* versions are used in functions called from machine.h
   these functions need &locmach to be passed in and have corresponding
   formal argument struct machine *machp   */

#define value_trail(p,bh,trreg) \
  trailing_check(p,bh)							\
  { trail_overflow_check(trreg,trail_limit,macro_deal_with_tr_overflow()) ; \
    *trreg++ = *p; /* value */						\
    *trreg++ = (dlong)p; /* location */					\
  }

#define uvalue_trail(p,bh,trreg,mach) \
  trailing_check(p,bh)							\
  { trail_overflow_check(trreg,mach->limit_trail,deal_with_tr_overflow(mach)) ; \
    *trreg++ = *p; /* value */						\
    *trreg++ = (dlong)p; /* location */					\
  }



#define trail(p,bh,trreg) \
  trailing_check(p,bh)							\
  { trail_overflow_check(trreg,trail_limit,macro_deal_with_tr_overflow()) ; \
    *trreg++ = (dlong)p; /* value */					\
    *trreg++ = (dlong)p; /* location */					\
  }

#define utrail(p,bh,trreg,mach) \
  trailing_check(p,bh)							\
  { trail_overflow_check(trreg,mach->limit_trail,deal_with_tr_overflow(mach)) ; \
    *trreg++ = (dlong)p; /* value */					\
    *trreg++ = (dlong)p; /* location */					\
  }


#define unconditional_value_trail(p,trreg) \
  { trail_overflow_check(trreg,trail_limit,macro_deal_with_tr_overflow()) ; \
    *trreg++ = *p; /* value */						\
    *trreg++ = (dlong)p; /* location */					\
  }

#define uunconditional_value_trail(p,trreg,mach)			\
  { trail_overflow_check(trreg,mach->limit_trail,deal_with_tr_overflow(mach)) ; \
    *trreg++ = *p; /* value */						\
    *trreg++ = (dlong)p; /* location */					\
  }


#define untrail(a,bb)		  \
  { register dlong *bloc = bb;	  \
    locmach.excess_woken= 0;			\
    while (a > bloc)				\
      {dlong *q; q = (dlong *)*(--a);		\
	*q = *(--a);				\
      }						\
  }


#define trailing_check(p,bh) if (p < bh)

#define trail_overflow_check(t,l,do) \
         if (t > l) do


struct choicepoint
{
  codep failurecont;
  dlong *h;
  dlong *tr;
  dlong *e;
  dlong *b;
  codep cp;
  dlong *tops;
} choicepoint;

/* convenience */
#define CP_P struct choicepoint *
#define CP_FIX_LEN (sizeof(struct choicepoint)/sizeof(dlong *))


#define recompute_tos(e,b) \
{                                                \
  dlong *le = e; CP_P lb = (CP_P)b;              \
  if (le > (dlong *)(lb->tops))                  \
     tos = (dlong *)(lb->tops);                  \
  else tos = le;                                 \
}


#define reset_regs1(b)                                 \
{                                                            \
  lochreg = b->h;                    \
  ereg = b->e;                    \
  contpreg = (codep)(b->cp);      \
  tos = b->tops;                 \
}

#define reset_regs                                           \
{                                                            \
  CP_P bb; \
  bb = (CP_P)locbreg; \
  reset_regs1(bb); \
}


#define cp_overflow(p) if (p < cp_limit) macro_deal_with_cp_overflow()

#define push_choicepoint(alt,contp,arity)                                         \
{                                                                                 \
  register dlong *p = locbreg;                                                       \
                                                                                  \
  { dlong ar = arity; while (ar) *(--p) = args(ar--); }                           \
  p -= (sizeof(struct choicepoint)/sizeof(dlong *));                              \
  ((CP_P)p)->failurecont = (codep)alt;                            \
  ((CP_P)p)->h = lochreg;                                            \
  ((CP_P)p)->tr = loctrreg;                                          \
  ((CP_P)p)->e = ereg;                                            \
  ((CP_P)p)->b = locbreg;                                            \
  ((CP_P)p)->cp = contp;                                          \
  ((CP_P)p)->tops = tos;                                          \
  locbreg = p;                                                       \
  cp_overflow(p);                                                        \
}

#define push_member_choicepoint(alt,contp,x,r)   \
{                                                                                 \
  register dlong *p = locbreg;                                                       \
                                                                                  \
  *(--p) = (dlong)x; *(--p) = (dlong)r;                            \
  p -= (sizeof(struct choicepoint)/sizeof(dlong *));                              \
  ((CP_P)p)->failurecont = (codep)alt;                            \
  ((CP_P)p)->h = lochreg;                                            \
  ((CP_P)p)->tr = loctrreg;                                          \
  ((CP_P)p)->e = ereg;                                            \
  ((CP_P)p)->b = locbreg;                                            \
  ((CP_P)p)->cp = contp;                                          \
  ((CP_P)p)->tops = tos;                                          \
  locbreg = p;                                                       \
  cp_overflow(p);                                                        \
}


#define push_choicepoint2(alt,contp)                             \
{                                                                \
  register dlong *p = locbreg;                                      \
                                                                 \
  *(--p) = args(2);                                              \
  *(--p) = args(1);                                              \
  p -= (sizeof(struct choicepoint)/sizeof(dlong *));             \
  ((CP_P)p)->failurecont = (codep)alt;                           \
  ((CP_P)p)->h = lochreg;                                           \
  ((CP_P)p)->tr = loctrreg;                                         \
  ((CP_P)p)->e = ereg;                                           \
  ((CP_P)p)->b = locbreg;                                           \
  ((CP_P)p)->cp = contp;                                         \
  ((CP_P)p)->tops = tos;                                         \
  locbreg = p;                                                      \
  cp_overflow(p);                                       \
}

#define push_choicepoint1(alt,contp)                             \
{                                                                \
  register dlong *p = locbreg;                                      \
                                                                 \
  *(--p) = args(1);                                              \
  p -= (sizeof(struct choicepoint)/sizeof(dlong *));             \
  ((CP_P)p)->failurecont = (codep)alt;                           \
  ((CP_P)p)->h = lochreg;                                           \
  ((CP_P)p)->tr = loctrreg;                                         \
  ((CP_P)p)->e = ereg;                                           \
  ((CP_P)p)->b = locbreg;                                           \
  ((CP_P)p)->cp = contp;                                         \
  ((CP_P)p)->tops = tos;                                         \
  locbreg = p;                                                      \
  cp_overflow(p);                                       \
}

#define push_choicepoint3(alt,contp)                             \
{                                                                \
  register dlong *p = locbreg;                                      \
                                                                 \
  *(--p) = args(3);                                              \
  *(--p) = args(2);                                              \
  *(--p) = args(1);                                              \
  p -= (sizeof(struct choicepoint)/sizeof(dlong *));             \
  ((CP_P)p)->failurecont = (codep)alt;                           \
  ((CP_P)p)->h = lochreg;                                           \
  ((CP_P)p)->tr = loctrreg;                                         \
  ((CP_P)p)->e = ereg;                                           \
  ((CP_P)p)->b = locbreg;                                           \
  ((CP_P)p)->cp = contp;                                         \
  ((CP_P)p)->tops = tos;                                         \
  locbreg = p;                                                      \
  cp_overflow(p);                                       \
}

#define push_choicepoint4(alt,contp)                             \
{                                                                \
  register dlong *p = locbreg;                                      \
                                                                 \
  *(--p) = args(4);                                              \
  *(--p) = args(3);                                              \
  *(--p) = args(2);                                              \
  *(--p) = args(1);                                              \
  p -= (sizeof(struct choicepoint)/sizeof(dlong *));             \
  ((CP_P)p)->failurecont = (codep)alt;                           \
  ((CP_P)p)->h = lochreg;                                           \
  ((CP_P)p)->tr = loctrreg;                                         \
  ((CP_P)p)->e = ereg;                                           \
  ((CP_P)p)->b = locbreg;                                           \
  ((CP_P)p)->cp = contp;                                         \
  ((CP_P)p)->tops = tos;                                         \
  locbreg = p;                                                      \
  cp_overflow(p);                                       \
}

#define push_choicepoint5(alt,contp)                             \
{                                                                \
  register dlong *p = locbreg;                                      \
                                                                 \
  *(--p) = args(5);                                              \
  *(--p) = args(4);                                              \
  *(--p) = args(3);                                              \
  *(--p) = args(2);                                              \
  *(--p) = args(1);                                              \
  p -= (sizeof(struct choicepoint)/sizeof(dlong *));             \
  ((CP_P)p)->failurecont = (codep)alt;                           \
  ((CP_P)p)->h = lochreg;                                           \
  ((CP_P)p)->tr = loctrreg;                                         \
  ((CP_P)p)->e = ereg;                                           \
  ((CP_P)p)->b = locbreg;                                           \
  ((CP_P)p)->cp = contp;                                         \
  ((CP_P)p)->tops = tos;                                         \
  locbreg = p;                                                      \
  cp_overflow(p);                                       \
}


#define pop_arguments(arity)                                     \
{                                                                \
  register dlong *p;                                             \
  register dlong *q;                                             \
  p = locbreg + (sizeof(struct choicepoint)/sizeof(dlong *));       \
  q = &(args(1));                                                \
  while (arity--) *q++ = *p++ ;                                  \
}

#define pop_arguments1(arity,b)                            \
{                                                                \
  register dlong *p;                                             \
  register dlong *q;                                             \
  p = (dlong *)(((dlong *)b) + (sizeof(struct choicepoint)/sizeof(dlong *)));  \
  q = &(args(1));                                                \
  while (arity--) *q++ = *p++ ;                                  \
}

#define pop_arguments1bis(b)                                  \
{                                                                \
  register dlong *p;                                             \
  p = (dlong *)(((dlong *)b) + (sizeof(struct choicepoint)/sizeof(dlong *)));  \
  args(1) = *p ;                                                 \
}
#define pop_arguments2(b)                                  \
{                                                                \
  register dlong *p;                                             \
  p = (dlong *)(((dlong *)b) + (sizeof(struct choicepoint)/sizeof(dlong *)));  \
  args(1) = *p ;                                                 \
  args(2) = *(p+1) ;                                             \
}

#define pop_arguments3(b)                                  \
{                                                                \
  register dlong *p;                                             \
  p = (dlong *)(((dlong *)b) + (sizeof(struct choicepoint)/sizeof(dlong *)));  \
  args(1) = *p ;                                                 \
  args(2) = *(p+1) ;                                             \
  args(3) = *(p+2) ;                                             \
}

#define pop_arguments4(b)                                  \
{                                                                \
  register dlong *p;                                             \
  p = (dlong *)(((dlong *)b) + (sizeof(struct choicepoint)/sizeof(dlong *)));  \
  args(1) = *p ;                                                 \
  args(2) = *(p+1) ;                                             \
  args(3) = *(p+2) ;                                             \
  args(4) = *(p+3) ;                                             \
}


#define pop_arguments5(b)                                  \
{                                                                \
  register dlong *p;                                             \
  p = (dlong *)(((dlong *)b) + (sizeof(struct choicepoint)/sizeof(dlong *)));  \
  args(1) = *p ;                                                 \
  args(2) = *(p+1) ;                                             \
  args(3) = *(p+2) ;                                             \
  args(4) = *(p+3) ;                                             \
  args(5) = *(p+4) ;                                             \
}

static INLINE dlong * get_cut_point(dlong *s, dlong i)
{
  dlong *cpt = s + old_get_atom_index(i);
  return(cpt);
} /* make_cut_point */


static INLINE dlong make_cut_point(dlong *p, dlong *q)
{
  dlong cpt = make_atom((q-p));
  return(cpt);
} /* make_cut_point */

static INLINE dlong is_cut_point(dlong i)
{
  if (! is_atom(i)) return(0);
  i = old_get_atom_index(i);
  return(i<0);
} /* is_cut_point */


