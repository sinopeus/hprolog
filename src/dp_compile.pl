/*

Todo:
- include problem ?
- export checken aan import
- imported ops

*/


:- module(compile,[compile/2, compile/3]).

compile(From, To) :-
	compile(From, To, [full,listing]).

compile(From, To, Options) :-
	nb_getval('$compiler_errors',OldCompilerErrors),
	nb_getval('$importedpreds',OldImportedPreds),
	nb_getval('$exportedpreds',OldExportedPreds),
	nb_getval('$fileprefix',OldPrefix),
	
	nb_setval('$compiler_errors',none),
	nb_setval('$importedpreds',[]),
	nb_setval('$exportedpreds',[]),
	fileprefix(From,Prefix),
	nb_setval('$fileprefix',Prefix),

% 	prolog_message(Options, ['Reading ', From, '\n']),
	cputime(T1),
	seeing(Old),
	see(From),
	(readClauses(Program0, MName, Defined0,[],AritFunctions) ->
	    checkexportlist(Defined0),
	    tr_pr_mod(Program0,ProgramTransformed,AritFunctions),
	    once((prolog_flag(optimize,Transform) ; Transform = no)),
	    (Transform == yes, has_a_definition(source_optimize:source_optimize(_,_,_)) ->
		(fake_compileBareProgram(ProgramTransformed, Options, MName,Defined0), fail ; true),
		source_optimize:source_optimize(ProgramTransformed,OptimProgram,MName)
	    ;
		OptimProgram = ProgramTransformed
	    ),
	    ( mymember(listing,Options) ->
%% 		add_listing(OptimProgram,OptimProgram,Program),
%% 		Defined1 = ["allclauses"/1|Defined0]
		add_listing(OptimProgram,OptimProgram,Program1,Defined0,Defined1)
	    ;
		Program1 = OptimProgram,
		Defined1 = Defined0
	    ),
	    % Defined2 = ["reset"/1|Defined1],
	    
	    close(From), see(Old),
	    cputime(T2), T3 is (T2 - T1),
	    prolog_message(Options, ['Reading ', From, ' took ', T3, ' milliseconds\n']),
	    compileBareProgram(Program1, From, To, Options, MName, Defined1),
	    close_output(To),
	    nb_getval('$compiler_errors',Errors),
	    (Errors == none ->
		true
	    ;
		delete_file(To),
		fail
	    )
	;
	    % readClauses currently fails if last clause in file has a syntax error :-(
	    seen, see(Old)
	),
	
	nb_setval('$compiler_errors',OldCompilerErrors),
	nb_setval('$importedpreds',OldImportedPreds),
	nb_setval('$exportedpreds',OldExportedPreds),
	nb_setval('$fileprefix',OldPrefix).


checkexportlist(Defined) :-
	nb_getval('$exportedpreds',Exported),
	(all_functors(Exported) ->
	    if(
	       exported_not_defined(Exported,Defined,Functor),
	       compiler_error([Functor, " exported but not locally defined"],nocontext),
	       true
	      )
	;
	    compiler_error("Export list of module declaration should contain functor descriptions",nocontext)
	).

all_functors([]).
all_functors([Name/Arity|R]) :-
	integer(Arity),
	Arity >= 0,
	string(Name),
	all_functors(R).

exported_not_defined(Exported,Defined,Functor) :-
	mymember(Functor,Exported),
	\+(mymember(Functor,Defined)).


%% add_listing([],Pout,Pout).
%% add_listing([clause(Clause,_)|Rest],Pin,Pout) :-
%% 	copy_term(Clause,NewClause),
%% 	Pout = [clause(functor(1,"allclauses",[NewClause],
%% 			       context(none,-1)),[])|NewPout],
%% 	add_listing(Rest,Pin,NewPout).

add_listing([],Pout,Pout,Defined,Defined).
add_listing([clause(Clause,_)|Rest],Pin,Pout,Din,Dout) :-
	copy_term(Clause,NewClause),
	( NewClause = functor(_,_,[Head,Body],_) ->
	    namearityofhead(Head,Name,Arity),
	    concstr(clause,Name,Arity,ClauseName),
	    Pout = [clause(functor(2,ClauseName,[Head,Body],
				   context(none,-1)),[])|NewPout],
	    (Din = [ClauseName/2|_] ->
		Din1 = Din
	    ;
		Din1 = [ClauseName/2|Din]
	    ),
	    add_listing(Rest,Pin,NewPout,Din1,Dout)
	;
	    % here are the declarations/directives
	    add_listing(Rest,Pin,Pout,Din,Dout)
	).

namearityofhead(atom(X,_),X,0).
namearityofhead(functor(Arity,Name,_,_),Name,Arity).
namearityofhead(list(_,_,_),".",2).

concstr(C,N,A,O) :-
	name(C,CL),
	string_to_ilist(N,NL),
	name(A,AL),
	append(NL,AL,NAL),
	append(CL,NAL,CNAL),
	ilist_to_string(CNAL,O).

readClauses(AllClauses, Name, Defined,PreProc,AFs) :-
	compiler_read(TBs,PreProc),
	TBs = [Term/_ | _],		% first read: no expansion yet
	( is_module_declaration(Term,StrName, Export,ExportedOps) ->
	    nb_setval('$exportedpreds',Export),
	    string_to_atom(StrName,Name),
	    compiler_read(NewTBs,PreProc),
	    readClausesmodl(NewTBs, AllClauses, [], Defined1,[],PreProc,AFs)
	;
	    is_sysmodule_declaration(Term,StrName, Export) ->
	    string_to_atom(StrName,Name),
	    compiler_read(NewTBs,PreProc),
	    readClausesmodl(NewTBs, AllClauses, [(('?-')/1)|Export], Defined1,[],PreProc,AFs)
	;
	    Name = user,
	    readClausesmodl(TBs, AllClauses, [], Defined1,[],PreProc,AFs)
	),
	sort(Defined1,Defined).

pop_continue_or_stop(AllClauses, Export, Defined,ToDoFiles,PreProc,AFs) :-
	(ToDoFiles = [] ->
	    Defined = [],
	    AllClauses = [],
	    AFs = []
	;
	    ToDoFiles = [File|RestToDoFiles],
	    seen,
	    see(File),
	    compiler_read(NewTBs,PreProc),
	    readClausesmodl(NewTBs, AllClauses, Export, Defined, RestToDoFiles,PreProc,AFs)
	).
	
read_and_continue(AllClauses, Export, Defined,ToDoFiles,PreProc,AFs) :-
	compiler_read(NewTBs,PreProc),
	readClausesmodl(NewTBs, AllClauses, Export, Defined, ToDoFiles,PreProc,AFs).
	
readClausesmodl(TBs, AllClauses, Export, Defined,ToDoFiles,PreProc,AFs) :-
	(TBs = [] ->
				% we test here for eof of current in
	    % because current_eof is not in the boot (yet), some acrobatics
	    % doesn't work anymore because has_a_definition is not in boot :-(
% 	    (has_a_definition(current_eof), current_eof ->
	    (callable(current_eof), current_eof ->
		pop_continue_or_stop(AllClauses, Export, Defined,ToDoFiles,PreProc,AFs)
	    ;  
		read_and_continue(AllClauses, Export, Defined,ToDoFiles,PreProc,AFs)
	    )
	;
	    TBs = [Term/Binding|RestTBs],
	    (
	      Term == end_of_file ->
	      pop_continue_or_stop(AllClauses, Export, Defined,ToDoFiles,PreProc,AFs)
	    ;
	      is_declaration(Term,Decl,_) ->
	      treatdecl(Decl,Term,Binding,AllClauses,MoreClauses, ToDoFiles, NewToDoFiles,NewPreProc,AF), % include problem ?
	      (NewPreProc = PreProc ->
		  true
	      ;
		  true
	      ),
	      (var(AF) ->
		  NewAFs = AFs
	      ;
		  AFs = [AF|NewAFs]
	      ),
	      NewDefined = Defined,
	      readClausesmodl(RestTBs, MoreClauses, Export, NewDefined, NewToDoFiles,NewPreProc,NewAFs)
	    ;
	      (getNameArity_fromClause(Term, Name, Arity) ->
		  ( mymember(Name/Arity, Export) ->
		      NewDefined = Defined
		  ;
		      Defined = [Name/Arity|NewDefined] 
		  ),
		  AllClauses = [clause(Term, Binding)|MoreClauses]
	      ;
		  compiler_error("Bad head", Term), nl,
		  MoreClauses = AllClauses,
		  NewDefined = Defined
	      ),
	      NewToDoFiles = ToDoFiles,
	      readClausesmodl(RestTBs, MoreClauses, Export, NewDefined, NewToDoFiles,PreProc,AFs)
	    )
	).

fake_compileBareProgram(Program, Options, MName, Defined) :-
	collectDirectives(Program, Directives, [], OCs),
	replace_spec(OCs,NewOCs),	    
	Pack = no,
	(mymember(debug, Options) -> Debug = yes; Debug = no),
	Info =  info(Pack, MName, Defined, _, Debug),
	abstractify(NewOCs, AbstractClauses, Atoms, Atoms2, Info),
	abstractifyDirectives(Directives, Atoms2, NewDirectives),
	handleAtoms(Atoms, _InfoAtoms),
	groupClauses(AbstractClauses, Preds, _Number, NewDirectives),

	fake_compileProgram(Preds, Options),
	fail.
fake_compileBareProgram(_,_,_,_).



compileBareProgram(Program, From, To, Options, MName, Defined) :-
	prolog_message(Options, ['Compiling ',From, ' ']),
	cputime(T1),
				% collect directives and reverse clauses
	collectDirectives(Program, Directives, [], OCs),
	replace_spec(OCs,NewOCs),	    
	Pack = no,
	(mymember(debug, Options) -> Debug = yes; Debug = no),
	Info =  info(Pack, MName, Defined, _, Debug),
	abstractify(NewOCs, AbstractClauses, Atoms, Atoms2, Info),
	abstractifyDirectives(Directives, Atoms2, NewDirectives),
	handleAtoms(Atoms, InfoAtoms),
	writeInfo(To, InfoAtoms, Options),
	b_setval('%%infoatoms%%',InfoAtoms),
	groupClauses(AbstractClauses, Preds, Number, NewDirectives),
	writePredNr(To, Number, Options),

	compileProgram(Preds, To, Options),
	cputime(T2), T3 is (T2 - T1),
	prolog_message(Options, ['took ', T3, ' milliseconds\n']).


compileProgram([], _, Options) :- end_of_w_file(Options).
compileProgram([Pred|Preds], FileNameOut, Options) :-
	(
	  ( (
	      compilePredicate(Pred, Compiled, Options),
	      peephole(Compiled,Compiled2),
	      writeCode(FileNameOut, Compiled2, Options)
	    ) -> 
	      fail
	  ;
	      arg(1,Pred,Clauses),
	      Clauses = [C1|_],
	      arg(1,C1,Head),
	      compiler_error("Compilation FAILED unexpectedly for predicate", Head), 
	      fail
	  )
	;
	  compileProgram(Preds, FileNameOut, Options)
	).

%% more recent code for massage_reset - no longer used at all
%% massage_reset(
%% 	      [predicate(A,B),allocate(4),getpvar(2,3),getpvar(3,2),
%% 	       call(C,4),active_yvar(1,0,3),
%% 	       put_atom(1,_),put_atom(2,_),
%% 	       inline("after_callcc__M_sysh",0,[]),
%% 	       getpval(3,1),getpval(2,2),dealloc_proceed,end_clauses],
%% 	      Out) :- !,
%% 
%% 	Out = [predicate(A,B),allocate(4),getpvar(2,3),getpvar(3,2),
%% 	       call(C,4),active_yvar(1,0,3),
%% 	       % putatoms removed
%% 	       inline("after_callcc__M_sysh",0,[]),
%% 	       getpval(3,1),getpval(2,2),dealloc_proceed,end_clauses].
%% 
%% 
%% old code for reset/1
%% massage_reset([predicate(A,B),allocate(2),call(C,2),active_yvar(1,0,0),
%% 	       put_atom(2,_),inline("after_callcc__M_sysh",0,[]),
%% 	       get_structure(_,S),uni_void,unitvar(1),deallex(C),end_clauses],
%% 	      Out) :- !,
%% 	
%% 	Out = 
%% 	      [predicate(A,B),allocate(2),call(C,2),active_yvar(1,0,0),
%% 	                     inline("after_callcc__M_sysh",0,[]),
%% 	       get_structure(1,S),uni_void,unitvar(1),deallex(C),end_clauses]
%% 	      .


clauses_p2a([],[]).
clauses_p2a([X|Xs],[Y|Ys]) :-
	prolog2abstract(X,Y),
	clauses_p2a(Xs,Ys).

fake_compileProgram([], Options) :- end_of_w_file(Options).
fake_compileProgram([Pred|Preds], Options) :-
	(
	  ( (
	      compilePredicate(Pred, _Compiled, Options)
	    ) -> 
	      fail
	  ;
	      arg(1,Pred,Clauses),
	      Clauses = [C1|_],
	      arg(1,C1,Head),
	      compiler_error("Fake Compilation FAILED unexpectedly for predicate", Head), 
	      halt
	  )
	;
	  fake_compileProgram(Preds, Options)
	).

abstractify([], [], Atoms, Atoms, _).
abstractify([clause(Term, Bindings)|Clauses],OutClauses, Atoms1, Atoms2, Info) :-
	(abstractifyClause(Term, Head, Body, Atoms1, Atoms3, Info, Id) ->
	    OutClauses = [clause(Head, Body, Bindings, _, Id)|AbstractClauses]
	;
	    compiler_error("Compiler cannot compile clause starting",Term),
	    nl,
	    Atoms2 = Atoms1,
	    OutClauses = AbstractClauses
	    
	),
	abstractify(Clauses, AbstractClauses, Atoms3, Atoms2, Info).

abstractifyClause(Term, NewHead, NewBody, Atoms1, Atoms2, Info, Id) :-
	getHeadBody(Term,Head,Body),  % cannot fail because of previous checks
	getNameArity2(Head,Name,Arity),
	Id = Name/Arity,
	abstractifyHead(Head, NewHead, Atoms1, Atoms3, NewBody1, Acc, Info),
	setarg(4, Info, Cut),
	abstractifyBody(Body, Acc, [], Atoms3, Atoms2, Info),
	(var(Cut) ->
	    NewBody = NewBody1
	;   
	    NewBody = [Cut|NewBody1]
	),
	true.

allTests([]).
allTests([Head|Tail]) :-
	allTestsLit(Head), !,
	allTests(Tail).
allTests([Head]):-
	allTestsLast(Head), ! .

%% allTestsLast(var(_,_,_) = integer(I,_)) :- smallint(I).
%% allTestsLast(integer(I,_) = var(_,_,_)) :- smallint(I).
%% allTestsLast(var(_,_,_) = atom(_,_)).
%% allTestsLast(atom(_,_) = var(_,_,_)).

allTestsLast(=(var(_,_,_),integer(I,_),_)) :- smallint(I).
allTestsLast(=(var(_,_,_),atom(_,_),_)).
allTestsLast(=(integer(I,_),var(_,_,_),_)) :- smallint(I).
allTestsLast(=(atom(_,_),var(_,_,_),_)).

smallint(I) :- \+(bigint(I)).

allTestsLit(test(_,_,_)).
allTestsLit(numericaltest(_,_,_,_)).

abstractifyBody(var(X,C), Body1, Body2, Atoms1, Atoms2, Info) :-
	abstractifyBody(functor(1,"call",[var(X,C)],C), Body1, Body2, Atoms1, Atoms2, Info).
abstractifyBody(integer(I,C), Abstract, Acc, Atoms1, Atoms2, _) :-
	Atoms1 = Atoms2,
	illegal_goal_message("Integer ", I, C),
	Abstract = [syntax_error|Acc].
abstractifyBody(float(I,C), Abstract, Acc, Atoms1, Atoms2, _) :-
	Atoms1 = Atoms2,
	illegal_goal_message("Float ", I, C),
	Abstract = [syntax_error|Acc].
abstractifyBody(char(I,C), Abstract, Acc, Atoms1, Atoms2, _) :-
	Atoms1 = Atoms2,
	illegal_goal_message("Character ", I, C),
	Abstract = [syntax_error|Acc].
abstractifyBody(string(I,C), Abstract, Acc, Atoms1, Atoms2, _) :-
	Atoms1 = Atoms2,
	illegal_goal_message("String ", I, C),
	Abstract = [syntax_error|Acc].
abstractifyBody(atom(StrName,C),Body1, Body2, Atoms1, Atoms2, Info) :-
	abstractifyBodyatom(StrName, Body1, Body2, Atoms1, Atoms2, Info, C).
abstractifyBody(functor(Arity,StrName,Args,C), Body1, Body2, Atoms1, Atoms2, Info) :-
	abstractifyBodyfunctor(StrName,Arity,Args,C,Body1, Body2, Atoms1, Atoms2, Info).
abstractifyBody(list(Head,Tail,C), Body1, Body2, Atoms1, Atoms2, Info) :-
	abstractifyBodyfunctor(".",2,[Head,Tail],C,Body1, Body2, Atoms1, Atoms2, Info).

abstractifyBodyatom("true", Body1, Body2, Atoms1, Atoms2, _Info, _C) :- !,
	Body1 = Body2, Atoms1 = Atoms2.
abstractifyBodyatom("!", Abstract, Acc, Atoms, Atoms, Info, _) :- !,
	arg(4, Info,  '$mark'(var(X, _, _))),
	Abstract = ['_$cutto'(var(X, _, _))|Acc].
%% abstractifyBodyatom("!", Abstract, Acc, Atoms1, Atoms2, Info, _) :- !,
%% 	arg(4, Info,  '$mark'(var(X, _, _))),
%% 	abstractifyBodyatom("dummy_call", Abstract,TempAbstr, Atoms1, Atoms2, Info, context(none,-1)),
%% 	TempAbstr = ['_$cutto'(var(X, _, _))|Acc].
%% abstractifyBodyatom("fail", Abstract, Acc, Atoms1, Atoms1, Info, C) :- !,
%% 	abstractifyBody(functor(2,"=",[integer(0,C),integer(1,C)],C),
%% 			Abstract, Acc, Atoms1, Atoms1, Info).
abstractifyBodyatom("fail", Abstract, Acc, Atoms1, Atoms2, _, _):- !,
	        Abstract = [fail|Acc], Atoms1 = Atoms2.

%% abstractifyBodyatom("error", Body1, Body2, Atoms1, Atoms2, Info, C):- !,
%% 	        Term = functor(2,"error",[atom(File,C),integer(Line,C)],C),
%% 		(C = context(File,Line), string(File), Line > -1 ->
%% 		    true
%% 		;
%% 		    File = "unknown file",
%% 		    Line = 666
%% 		),
%% 		abstractifyBody(Term, Body1, Body2, Atoms1, Atoms2, Info).

% some builtins with arity 0 ...
abstractifyBodyatom(StrName, Abstract, Acc, Atoms1, Atoms2, Info, C) :-
	string_to_atom(StrName, Name), % with other gen of inline_builtin - this disappears
	inline_builtin(Name, 0, _),
	arg(5, Info, no), !,
	Abstract = [inline(StrName, 0, [], C)|Acc],
	Atoms1 = Atoms2.

abstractifyBodyatom(StrName, Abstract, Acc, Atoms1, Atoms2, Info, Context) :-
	Atoms1 = [a(AName, 0, MName, Nr)|Atoms2],
	solveModule(StrName, 0, AName, MName, Info),
	(StrName == "dummy_call" ->
	    Dummy = yes
	;
	    (StrName == "nop_call" ->
		Dummy = nop_call
	    ;
		Dummy = no
	    )
	),
	Abstract = [atomcall(Nr, _, Context,Dummy)|Acc].


abstractifyBodyfunctor(",",2,[Body1, Body2], _, NewBody, Acc, Atoms1, Atoms2, Info) :- !,
	abstractifyBody(Body1, NewBody, Acc1, Atoms1, Atoms3, Info),
	abstractifyBody(Body2, Acc1, Acc, Atoms3, Atoms2, Info).



abstractifyBodyfunctor(";",2,[IfThen, Else], _, Abstract,Acc,Atoms1,Atoms2,Info) :-
	IfThen = functor(2,"->",[If,Then],_),
	!,
	abstractifyBody(If, NewIf, [], Atoms1, Atoms3, Info),
	(allTests(NewIf) ->
	    Abstract = [smallITE(NewIf, NewThen, NewElse, _)|Acc],
	    abstractifyBody(Then, NewThen, [], Atoms3, Atoms4, Info),
	    abstractifyBody(Else, NewElse, [], Atoms4, Atoms2, Info)

	;
	    NoContext = context(none,-1),
	    ExtendedIf = functor(2,",",[If,Cut],NoContext),
	    Cut = functor(1,"_$cutto",[var(NewY,NoContext)],NoContext),
	    abstractifyBody(ExtendedIf, NewExtendedIf, [], Atoms1, Atoms3bis, Info),
	    abstractifyBodyatom("dummy_call", Abstract,Abstr1, Atoms3bis, Atoms4,Info, NoContext),
	    abstractifyBodyfunctor("_$savecp",1,[var(NewY,NoContext)], NoContext,
				   Abstr1,Abstr2,Atoms4,Atoms5,Info),
	    Abstr2 = [ifthenelse(_, NewExtendedIf, NewThen, NewElse, _)|Acc],
	    abstractifyBody(Then, NewThen, [], Atoms5, Atoms6, Info),
	    abstractifyBody(Else, NewElse, [], Atoms6, Atoms2, Info)

	).

abstractifyBodyfunctor(";", 2, Disjs, Context, Abstract, Acc, Atoms1, Atoms2, Info) :- 
        !,
	abstractifyBodyatom("dummy_call", Abstract,TempAbstr, Atoms1, TempAtoms2, Info, Context),
	TempAbstr = [disjs(NewDisjs, _)|Acc],
	abstractifyBodyDisj(functor(2,";",Disjs, Context), NewDisjs, TempAtoms2, Atoms2, Info).

abstractifyBodyfunctor("if",3,[If,Then,Else],_C,Abstract, Acc, Atoms1, Atoms2, Info) :-
	abstractifyBody(If, NewIf, [], Atoms1, Atoms3, Info),
	allTests(NewIf), !,
	Abstract = [smallITE(NewIf, NewThen, NewElse, _)|Acc],
	abstractifyBody(Then, NewThen, [], Atoms3, Atoms4, Info),
	abstractifyBody(Else, NewElse, [], Atoms4, Atoms2, Info).

abstractifyBodyfunctor("if",3,[A,B,C],Context,Abstract, Acc, Atoms1, Atoms2, Info) :- !,
	NewGoals = functor(2,";",[Branch1,Branch2],Context),
	% Branch1 = ('_$savecp'(CP),A, disable_cp(CP), B),
	Branch1 = functor(2,",",[Savecp,R1],Context),
	Savecp = functor(1,"_$savecp",[var(CP,Context)],Context),
	R1 = functor(2,",",[A,R2],Context),
	R2 = functor(2,",",[Disablecp,B],Context),
	Disablecp = functor(1,"disable_cp__M_sysh",[var(CP,Context)],Context),
	Branch2 = C,
	abstractifyBody(NewGoals, Abstract, Acc, Atoms1, Atoms2, Info).

abstractifyBodyfunctor("->",2,[Body1, Body2],Context, ['_$savecp'(var(X, _, Context))|NewBody],
		Acc, Atoms1, Atoms2, Info) :- !,
	abstractifyBody(Body1, NewBody, ['_$cutto'(var(X, _, Context))|Acc1],
			Atoms1, Atoms3, Info),
	abstractifyBody(Body2, Acc1, Acc, Atoms3, Atoms2, Info).

abstractifyBodyfunctor("once",1,[Body], Context, ['_$savecp'(var(X, _, Context))|NewBody],
		Acc, Atoms1, Atoms2, Info) :- !,
	abstractifyBody(Body, NewBody, ['_$cutto'(var(X, _, Context))|Acc],
			Atoms1, Atoms2, Info).

abstractifyBodyfunctor("=", 2, Args, Context, Abstract, Acc, Atoms1, Atoms2, Info) :-
	module_system(bim),
	arg(5, Info, no),!,
	Abstract = [=(NewX,NewY,Context)|Acc1],
	% specialcall
	Args = [X,Y],
	abstractifyBodyInside(X, NewX, Atoms1, Atoms3, Acc1, Acc2, Info),
	abstractifyBodyInside(Y, NewY, Atoms3, Atoms2, Acc2, Acc, Info).


abstractifyBodyfunctor("=", 2, [X, Y], Context, Abstract, Acc, Atoms1, Atoms2, Info) :- 
	arg(5, Info, no),!,
	Abstract = [=(NewX,NewY,Context)|Acc1],
	abstractifyTerm(X, NewX, Atoms1, Atoms3, Acc1, Acc2, 1),
	abstractifyTerm(Y, NewY, Atoms3, Atoms2, Acc2, Acc, 1).

abstractifyBodyfunctor("$initperm", 1, [X], Context, Abstract, Acc, Atoms1, Atoms2, _Info) :- 
	!,
	Abstract = ['$initperm'(NewX,Context)|Acc1],
	abstractifyTerm(X, NewX, Atoms1, Atoms2, Acc1, Acc, 1).

abstractifyBodyfunctor("is",2,[X, Y], _, Abstract, Acc, Atoms1, Atoms2, Info) :- 
	arg(5, Info, no),!,
 	abstractifyTerm(X, NewX, Atoms1, Atoms3, Acc1, Acc, 1),
	abstractifyExpression(Y, NewY, Atoms3, Atoms2),
	splitIsExpression(NewY, NewX, Abstract, Acc1).

abstractifyBodyfunctor("_$cutto",1,[X],_, Abstract, Acc, Atoms1, Atoms2, _) :- !,
	abstractifyTerm(X, NewX, Atoms1, Atoms2, Acc1, Acc, 1),
	Abstract = ['_$cutto'(NewX)|Acc1].

abstractifyBodyfunctor("_$savecp",1,[X],_, Abstract, Acc, Atoms1, Atoms2, _) :- !,
	abstractifyTerm(X, NewX, Atoms1, Atoms2, Acc1, Acc, 1),
	Abstract = ['_$savecp'(NewX)|Acc1].

abstractifyBodyfunctor("\\+",1, [Body], Context, Abstract, Acc, Atoms1, Atoms2, Info) :- !,
	arg(4, Info, OldCut),
	setarg(4, Info, NewCut),
	% NewBody = (Body -> fail; true),
	NewBody = functor(2,";",[Bodyfail,atom("true",Context)],Context),
	Bodyfail = functor(2,"->",[Body,atom("fail",Context)],Context),
	abstractifyBody(NewBody, Abstract1, Acc, Atoms1, Atoms2, Info),
	(var(NewCut) -> Abstract1 = Abstract;
	    NewCut = '$mark'(Var),
	    Abstract = ['_$savecp'(Var)|Abstract1]
	),
	setarg(4, Info, OldCut).

abstractifyBodyfunctor(StrName, 2, [Arg1, Arg2], Context, Abstract, Acc, Atoms1, Atoms2, Info) :-
	isNumericalTest(StrName),
	arg(5, Info, no),
	!,
        Abstract = [numericaltest(StrName, NewArg1, NewArg2,Context)|Acc],
	abstractifyExpression(Arg1, NewArg1, Atoms1, Atoms3),
	abstractifyExpression(Arg2, NewArg2, Atoms3, Atoms2).

abstractifyBodyfunctor(StrName, Arity, Args, Context, Abstract, Acc, Atoms1, Atoms2, Info) :-
	isTest(StrName, Arity),
	arg(5, Info, no), !,
        Acc1 = [test(StrName, NewArgs,Context)|Acc],
	abstractifyArgs(Args, NewArgs, Atoms1, Atoms2, Abstract, Acc1, 1).

abstractifyBodyfunctor(StrName, Arity, Args, Context, Abstract, Acc, Atoms1, Atoms2, Info) :-
	string_to_atom(StrName, Name), % with other gen of inline_builtin - this disappears
	inline_builtin(Name, Arity, _),
	arg(5, Info, no), !,
	Acc1 = [inline(StrName, Arity, NewArgs, Context)|Acc],
	abstractifyArgs(Args, NewArgs, Atoms1, Atoms2, Abstract, Acc1, 1).

abstractifyBodyfunctor(StrName, Arity, Args, Context, Abstract, Acc, Atoms1, Atoms2, Info) :-
	Atoms1 = [a(AName, Arity, MName, Nr)|Atoms3],
	solveModule(StrName, Arity, AName, MName, Info),
	Acc1 = [functorcall(Nr, Arity, NewArgs, _, Context)|Acc],
	(specialcall(StrName, Arity, Which) ->
	    abstractifyArgsSpecial(Args, Which, NewArgs, Atoms3, Atoms2, Abstract, Acc1, Info)
	;
	    abstractifyArgs(Args, NewArgs, Atoms3, Atoms2, Abstract, Acc1, 1)
	).

% choose one of the following
% module_system(bim). % what we want :-)
module_system(ilprolog). % what was the case earlier

% specialcall(_,_,_) :- !, fail.
% specialcall(_, Arity, L) :- module_system(bim), !, mkslist(Arity,L).
specialcall("findall", 3, [n,s,n]).
specialcall("setof",   3, [n,s,n]).
specialcall("bagof",   3, [n,s,n]).

mkslist(N,L) :-
	(N == 0 ->
	    L = []
	;
	    M is N - 1,
	    L = [s|R],
	    mkslist(M,R)
	).

abstractifyBodyInside(Char,Abstract, Atoms1, Atoms2, Acc1, Acc2, _) :-
	Char = char(_,_),
	Abstract = Char,
	Atoms2 = Atoms1,
	Acc2 = Acc1.

abstractifyBodyInside(String,Abstract, Atoms1, Atoms2, Acc1, Acc2, _) :-
	String = string(_,_),
	Abstract = String,
	Atoms2 = Atoms1,
	Acc2 = Acc1.

abstractifyBodyInside(Int,Abstract, Atoms1, Atoms2, Acc1, Acc2, _) :-
	Int = integer(_,_),
	Abstract = Int,
	Atoms2 = Atoms1,
	Acc2 = Acc1.

abstractifyBodyInside(Float,Abstract, Atoms1, Atoms2, Acc1, Acc2, _) :-
	Float = float(_,_),
	Abstract = Float,
	Atoms2 = Atoms1,
	Acc2 = Acc1.

abstractifyBodyInside(var(Body,C), Abstract, Atoms1, Atoms2, Acc1, Acc2, _) :-
	!,
	Atoms2 = Atoms1,
	Acc2 = Acc1,
	Abstract = var(Body,_,C).

abstractifyBodyInside(atom(StrName,C), Abstract, Atoms1, Atoms2, Acc, Acc, Info) :-
	!,
	Atoms1 = [a(AName, 0, MName, Nr)|Atoms2],
	solveModule(StrName, 0, AName, MName, Info),
	Abstract = atom(Nr,C).

abstractifyBodyInside(list(Head,Tail,C), Abstract, Atoms1, Atoms2, Acc1, Acc2, _) :-
	  % this is consult
	!,
	Abstract = list(NewHead, NewTail, C),
	abstractifyTerm(Head, NewHead, Atoms1, Atoms3, Acc1, Acc3, 1),
	abstractifyTerm(Tail, NewTail, Atoms3, Atoms2, Acc3, Acc2, 1).
		     
abstractifyBodyInside(functor(Arity,StrName,Args,Context), Abstract, Atoms1, Atoms2, Acc1, Acc2, Info) :-
	Atoms1 = [a(AName, Arity, MName, Nr)|Atoms3],
	solveModule(StrName, Arity, AName, MName, Info),
	Abstract = functor(Nr, Arity, NewArgs,Context),
	( Arity = 2, againBody(StrName) -> 
	    abstractifyArgsBody(Args, NewArgs, Atoms3, Atoms2, Acc1, Acc2,Info)
	;
	    (specialcall(StrName, Arity, Which) ->
		abstractifyArgsSpecial(Args, Which, NewArgs, Atoms3, Atoms2, Acc1, Acc2, Info)
	    ;
		abstractifyArgs(Args, NewArgs, Atoms3, Atoms2, Acc1, Acc2, 1)
	    )
	).

againBody(",").
againBody(";").
againBody("->").
againBody("^").


% hier is nog iets mis denk ik
solveModule(String, Arity, AName, MName, Info) :-
% 	writeq(solveModule(String, Arity, AName, MName, Info)),nl,
	string_to_atom(String,Name),
	add_module(Name, M, AName),
	( M = user ->
	    arg(3, Info, Defined),
% 	    writeq(Defined),nl,nl,
	    getmodule(String,Arity,Defined,MName,Info)
	;
	    MName = M
	).

getmodule(String,Arity,Defined,MName,Info) :-
	(Arity == 1, String == "?-" ->
	    MName = user
	;
	    ( mymember(String/Arity, Defined) ->
		arg(2, Info, MName)
	    ;
		(
		  nb_getval('$importedpreds',ImportedPreds),
		  in_imported_preds(ImportedPreds,String,Arity,MName) ->
		  true
		;
		  MName = user
		)
	    )
	).

in_imported_preds(Imports,String,Arity,MName) :-
	findall(ModName,compile:in_imported_preds1(Imports,String,Arity,ModName),L),
	(allsamelist(L,MName) -> % L = [MName]  ???
	    true
	;
	    L = [_,_|_],
	    compiler_error(["Predicate ", String/Arity, " is defined in modules ", L,
			    "\n       Please use explicit module qualifications"],nocontext)
	).

allsamelist([A|R],A) :- allsamelist1(R,A).

allsamelist1([],_).
allsamelist1([M|R],M) :- allsamelist1(R,M).

in_imported_preds1([ModName/Listofpreds|R],String,Arity,MName) :-
	(
	  once(mymember(String/Arity,Listofpreds)),
	  MName = ModName
	;
	  in_imported_preds1(R,String,Arity,MName)
	).


abstractifyHead(Head, NewTerm, Atoms1, Atoms2, Acc1, Acc2, Info) :-
	(Head = atom(StrName,Context) ->
	    Acc1 = Acc2,
	    Atoms1 = [a(AName, 0, MName, Nr)|Atoms2],
	    solveModule(StrName, 0, AName, MName, Info),
	    NewTerm = atom(Nr,Context)
	;
	    (Head = functor(Arity,StrName,Args,Context) ->
		Atoms1 = [a(AName, Arity, MName, Nr)|Atoms3],
		solveModule(StrName, Arity, AName, MName, Info),
		NewTerm = functor(Nr, Arity, NewArgs,Context),
		module_system(ModSys),
		(  (ModSys = bim ; (Arity =:= 1, StrName = "?-")) ->
		    abstractifyArgsBody(Args, NewArgs, Atoms3, Atoms2, Acc1, Acc2, Info)
		;
		    abstractifyArgs(Args, NewArgs, Atoms3, Atoms2, Acc1, Acc2, 1)
		)
	    ;
		Head = list(A1,A2,Context),
		abstractifyHead(functor(2,".",[A1,A2],Context),NewTerm, Atoms1, Atoms2, Acc1, Acc2, Info)
	    )
	).


abstractifyTerm(var(Var,Cont), NewTerm, Atoms1, Atoms2, Acc, Acc, _) :- 
	NewTerm = var(Var, _, Cont), Atoms1 = Atoms2.
abstractifyTerm(atom(String,Cont), NewTerm, Atoms1, Atoms2, Acc, Acc, _) :- 
	NewTerm = atom(Nr,Cont),
	string_to_atom(String,Atom),
	add_module(Atom, MName, AName),
	Atoms1 =  [a(AName, 0, MName, Nr)|Atoms2].
abstractifyTerm(integer(Int,Cont), NewTerm, Atoms1, Atoms2, Acc, Acc, _) :-
	NewTerm = integer(Int,Cont),
	Atoms1 = Atoms2.
abstractifyTerm(float(Float,Cont), NewTerm, Atoms1, Atoms2, Acc, Acc, _) :-
	NewTerm = float(Float,Cont),
	Atoms1 = Atoms2.
abstractifyTerm(string(String,Cont), NewTerm, Atoms1, Atoms2, Acc, Acc, _) :-
	NewTerm = string(String,Cont),
	Atoms1 = Atoms2.
abstractifyTerm(char(Char,Cont), NewTerm, Atoms1, Atoms2, Acc, Acc, _) :-
	NewTerm = char(Char,Cont),
	Atoms1 = Atoms2.
abstractifyTerm(list(Head,Tail,Cont), NewTerm, Atoms1, Atoms2, Acc1, Acc2, Count) :-
	(Count > 2048 ->
	    NewTerm = var(Var, _, Cont),
	    Acc1 =  [=(var(Var, _, Cont),NewList,Cont)|Acc3],
	    abstractifyTerm(list(Head,Tail,Cont), NewList, Atoms1, Atoms2,Acc3, Acc2, 1)
	;
	    NewTerm = list(NewHead,NewTail,Cont),
	    Count1 is Count + 1,
	    abstractifyTerm(Head, NewHead, Atoms1, Atoms3, Acc1, Acc3, Count1),
	    abstractifyTerm(Tail, NewTail, Atoms3, Atoms2, Acc3, Acc2, Count1)
	).
abstractifyTerm(functor(Arity,StrName,Args,Cont), NewTerm, Atoms1, Atoms2, Acc1, Acc2, Count) :-
	string_to_atom(StrName,Name),
	add_module(Name, MName, AName),
	Atoms1 = [a(AName, Arity, MName, Nr)|Atoms3],
	NewTerm = functor(Nr, Arity, NewArgs, Cont),
	Count1 is Count + 1,
	abstractifyArgs(Args, NewArgs, Atoms3, Atoms2, Acc1, Acc2, Count1).



abstractifyArgs([], [], Atoms, Atoms, Acc, Acc,  _).
abstractifyArgs([Arg|Args], [NewArg|NewArgs], Atoms1, Atoms2, Acc1, Acc2, Count) :-
	abstractifyTerm(Arg, NewArg, Atoms1, Atoms3, Acc1, Acc3, Count),
	abstractifyArgs(Args, NewArgs, Atoms3, Atoms2, Acc3, Acc2, Count).

abstractifyArgsBody([], [], Atoms, Atoms, Acc, Acc, _).
abstractifyArgsBody([Arg|Args], [NewArg|NewArgs], Atoms1, Atoms2,
		    Acc1, Acc2, Info) :-
	abstractifyBodyInside(Arg, NewArg, Atoms1, Atoms3, Acc1, Acc3, Info),
	abstractifyArgsBody(Args, NewArgs, Atoms3, Atoms2, Acc3, Acc2, Info).

abstractifyArgsSpecial([], _, [], Atoms, Atoms, Acc, Acc, _).
abstractifyArgsSpecial([Arg|Args], [How|Hows], [NewArg|NewArgs],
		       Atoms1, Atoms2, Acc1, Acc2, Info) :-
	( How = n ->
	    abstractifyTerm(Arg, NewArg, Atoms1, Atoms3, Acc1, Acc3, 1)
	;
	    abstractifyBodyInside(Arg, NewArg, Atoms1, Atoms3,
				  Acc1, Acc3, Info)
	),
	abstractifyArgsSpecial(Args, Hows, NewArgs, Atoms3, Atoms2,
			       Acc3, Acc2, Info).

isNumericalTest("<"). 
isNumericalTest(">").
isNumericalTest("=<").
isNumericalTest(">=").
isNumericalTest("=:=").
isNumericalTest("=\\=").

isTest("\\=",2).
isTest("==",2).
isTest("===",2).
isTest("<<<",2).
isTest("\\==",2).
isTest("@<",2).
isTest("@>",2).
isTest("@=<",2).
isTest("@>=",2).
isTest("var",1).
isTest("attvar",1).
isTest("atom",1).
isTest("atomic",1).
isTest("compound",1).
isTest("number",1).
isTest("integer",1).
isTest("nonvar",1).
isTest("callable",1).
isTest("points_to_callcc",1).

emptyGroups(nil).

inGroup(tree(L, Number, Clauses, R), Number1, Clause, NewPreds) :-
	( Number == Number1 ->
	    NewPreds = tree(L, Number, [Clause|Clauses], R)
	;
	    ( Number1 @< Number ->
		inGroup(L, Number1, Clause, NewL),
		NewPreds = tree(NewL, Number, Clauses, R)
	    ;
		inGroup(R, Number1, Clause, NewR),
		NewPreds = tree(L, Number, Clauses, NewR)
	    )
	).

addGroup(Clauses, Number, nil, tree(nil, Number, Clauses, nil)) :- ! .
addGroup(Clauses, Number, tree(L, Number1, Clauses1, R),
	                  tree(NewL, Number1, Clauses1, NewR)) :-
	( Number @< Number1  ->
	    NewR = R,
	    addGroup(Clauses, Number, L, NewL)
	;
	    NewL = L,
	    addGroup(Clauses, Number, R, NewR)
	).

fromGroupsToList(nil, L, L, Number, Number).
fromGroupsToList(tree(L, Number, Clauses, R), List, Acc, Number1, Number2) :-
	fromGroupsToList(R, List1, Acc, Number1, Number3),
	Number4 is Number3 + 1,
	fromGroupsToList(L, List, [pred(Clauses,Number, [])| List1], 
		         Number4, Number2).
fromGroupsToList(tree(L, Number, Clauses, Dirs, R),List,Acc,Number1,Number2) :-
	fromGroupsToList(R, List1, Acc, Number1, Number3),
	Number4 is Number3 + 1,
	fromGroupsToList(L, List, [pred(Clauses,Number, Dirs)| List1], 
		         Number4, Number2).

getNumber(clause(Term, _, _, _, _), Number) :-
	getnumber1(Term,Number).

getnumber1(atom(Number,_),Number).
getnumber1(functor(Number, _, _, _),Number).

groupClauses(Clauses, Groups, Number, NewDirectives) :-
	emptyGroups(Data),
	groupClausesAll(Clauses, Data, Data1),
	addDirectivesToGroups(NewDirectives, Data1, Data2),
	fromGroupsToList(Data2, Groups, [], 0, Number).

inGroupDir(tree(L, Number, Clauses, R), Number1, Dir, NewPreds) :-
	( Number == Number1 ->
	    NewPreds = tree(L, Number, Clauses, [Dir], R)
	;
	    ( Number1 @< Number ->
		inGroupDir(L, Number1, Dir, NewL),
		NewPreds = tree(NewL, Number, Clauses, R)
	    ;
		inGroupDir(R, Number1, Dir, NewR),
		NewPreds = tree(L, Number, Clauses, NewR)
	    )
	).
inGroupDir(tree(L, Number, Clauses, Dirs, R), Number1, Dir, NewPreds) :-
	( Number == Number1 ->
	    NewPreds = tree(L, Number, Clauses, [Dir|Dirs], R)
	;
	    ( Number1 @< Number ->
		inGroupDir(L, Number1, Dir, NewL),
		NewPreds = tree(NewL, Number, Clauses, Dirs, R)
	    ;
		inGroupDir(R, Number1, Dir, NewR),
		NewPreds = tree(L, Number, Clauses, Dirs, NewR)
	    )
	).

addDirectivesToGroups([], Data, Data).
addDirectivesToGroups([Dir|Dirs], Data1, Data2) :-
	arg(2, Dir, Number),
	( inGroupDir(Data1, Number, Dir, Data3) -> true ; true),
	addDirectivesToGroups(Dirs, Data3, Data2).
	

groupClausesAll([], Data, Data).
groupClausesAll([Clause|Clauses], Data, Data1) :-
	getNumber(Clause, Number),
	groupClausesAll2(Clause, Number, Clauses, Data, Data1).

groupClausesAll2(Clause, Number, Clauses, Data, Data2) :-
	(inGroup(Data, Number, Clause, Data1) ->
	  groupClausesAll(Clauses, Data1, Data2)
	;
	  groupClauses1(Clauses, Number, [Clause], Data, Data2)
	).

groupClauses1([], Number, Clauses, Data, Data1) :- !,
	addGroup(Clauses, Number, Data, Data1).
groupClauses1(InClauses, Number, Clauses1, Data, Data1) :-
	InClauses = [Clause|Clauses],
	getNumber(Clause, Number1),
	( Number1 = Number ->
	  groupClauses1(Clauses, Number, [Clause|Clauses1], Data, Data1)
	;
	  addGroup(Clauses1, Number, Data, Data2),
	  groupClausesAll2(Clause, Number1, Clauses, Data2, Data1)
	).


% used to have block as a special case ... now obsolete
abstractifyDirectives(_,[],[]).



me_length([], Len, Len).
me_length([_|L], Len1, Len2) :-
	Len3 is Len1 + 1,
	me_length(L, Len3, Len2).

handleAtoms(Atoms, Info) :-
	me_length(Atoms, 0, Len),
	atomMerge(Len, Atoms, Info).

atomMerge(0, Atoms, Merged) :- !, Atoms =  Merged.
atomMerge(1, Atoms, Merged) :- !, Atoms =  Merged.
atomMerge(Len, Atoms, Merged) :- Len > 0,
	Len1 is Len //2,
	Len2 is Len - Len1,
	splitList(Len1, Atoms, Atoms1, Atoms2),
	atomMerge(Len1, Atoms1, Merged1),
	atomMerge(Len2, Atoms2, Merged2),
	doTheMerge(Merged1, Merged2, Merged).

splitList(0, Atoms1, [], Atoms2) :- !, Atoms1 = Atoms2.
splitList(Len, Atoms1, Atoms2, Atoms3) :- Len >0,
	Atoms1 = [Atom|Atoms4],
	Atoms2 = [Atom|Atoms5],
	Len1 is Len -1,
	splitList(Len1, Atoms4, Atoms5, Atoms3).

doTheMerge([], Merged1, Merged2) :- !, Merged1 = Merged2.
doTheMerge(Merged1, [], Merged2) :- !, Merged1 = Merged2.
doTheMerge(Merged1, Merged2, Merged3) :-
	Merged1 = [Atom1|Merged4],
	Atom1 = a(Name1, Arity1, MName1, Var1),
	Merged2 = [Atom2|Merged5],
	Atom2 = a(Name2, Arity2, MName2, Var2),
	(Name1 == Name2 ->
		(Arity1 ==  Arity2 ->
		        ( MName1 == MName2 ->
			    Var1 = Var2,
			    Merged3 = [Atom1| Merged6],
			    doTheMerge(Merged4, Merged5, Merged6)
			;
			    ( MName1 @< MName2 ->
				Merged3 = [Atom1|Merged6],
				doTheMerge(Merged4, Merged2, Merged6)
			    ;
				Merged3 = [Atom2|Merged6],
				doTheMerge(Merged1, Merged5, Merged6)
			    )
			)
		;
 			( Arity1 < Arity2 ->
				Merged3 = [Atom1|Merged6],
				doTheMerge(Merged4, Merged2, Merged6)
			;
				Merged3 = [Atom2|Merged6],
				doTheMerge(Merged1, Merged5, Merged6)
			)
		)
	;
		(Name1 @< Name2 ->
			Merged3 = [Atom1|Merged6],
			doTheMerge(Merged4, Merged2, Merged6)
		;
			Merged3 = [Atom2|Merged6],
			doTheMerge(Merged1, Merged5, Merged6)
		)
	).
			

compilePredicate(Pred, IsFinal, Options) :-
	startswithvarcutfail(Pred,NewPred,Arg), !,
	compilePredicate1(NewPred, Is1, Is2, Entrys, NewArity, Options),
	compilePredicate2(NewPred, Is2, Entrys, NewArity, Options),
	Is1 = [PI|RestIs],
	predicateInstruction(PI, _, L4),
	newLabelGen(L4),
	IsFinal = [PI,builtin_nonvar_1(Arg)|RestIs].
compilePredicate(Pred, Is1, Options) :-
	compilePredicate1(Pred, Is1, Is2, Entrys, NewArity, Options),
	compilePredicate2(Pred, Is2, Entrys, NewArity, Options),
	Is1 = [PI|RestIs],
	predicateInstruction(PI, _, L4),
	newLabelGen(L4).
	

compilePredicate1(Pred, Is0, Is4, Entrys, NewArity, Options) :-
	predicateInstruction(PI, Number, _),
	Is0 = [PI|Is1],
	Pred = pred(Clauses, Number, Directives),
	(Directives == [] ->
	    true
	;
	    compiler_error("Directives found for predicate",nocontext)
	),
	(Clauses = [clause(functor(_,Arity,[A1|_],_),_,_,_,Id)|_] ->
	    check_redefinition(Id,A1)
	;
	    Arity = 0
	),

	startLabelGen,
	chunkifyClauses(Clauses, Options),
        ( hasCutClauses(Clauses) ->
	    NewArity is Arity + 1,
	    Is1 = [gettbreg(NewArity)|Is3]
	;
	    NewArity = Arity,
	    Is1 = Is3
	),
        ( mymember(noindexing, Options) ->
	    Is3 = Is4
	;   
	    indexing(Clauses, Is3, Is4, Entrys, NewArity)
        ),
	(Clauses = [_] -> true
	;
	 labelEntrys(Clauses, Entrys)
	).

check_redefinition(Name/Arity,Term) :-
	(control_construct(Name,Arity) ->
	    compiler_warning("Control construct redefined",Term)
	;
	    true
	).

control_construct((->),2).
control_construct(Comma,2) :- name(Comma,[0',]).
control_construct((;),2).
control_construct((:-),2).
control_construct((:-),1).


labelEntrys([], []).
labelEntrys([_|Cls], [V|Vs]) :-
        (nonvar(V) ->
	    labelEntry(V);
	    true
	),
	labelEntrys(Cls, Vs).

labelEntry(normal(L)) :-
	newLabelGen(L).
labelEntry(skip_list(_, L1, L2)) :-
	newLabelGen(L1),
	newLabelGen(L2).


hasCutClauses([Cl|_]) :-
	hasCutClause(Cl), ! .
hasCutClauses([_|Cls]) :-
	hasCutClauses(Cls).

hasCutClause(Clause) :-
	arg(2, Clause, Body),
	Body = ['$mark'(_)|_].
	
compilePredicate2(Pred, Is1, Entrys, NewArity, _) :-
	Pred = pred(Clauses, _, _),
	Is2 = [end_clauses],
	generateCodeClauses(Clauses, Entrys, NewArity, Is1, Is2).


generateCodeClauses([Cl|Cls], [Entry|Entrys], Arity, Is1,Is2) :-
	( Cls = [] -> 
	    compileClause(Cl, Is1, Is2, Entry)
	;
	    newLabelGen(L1),
	    (var(Entry) ->
		Is1 = [try_me_else(Arity, L1)| Is3]
	    ;
		entryAddress(Entry, Address),
		Is1 = [try_me_else(Arity, L1), label(Address)| Is3]
	    ),
	    compileClause(Cl, Is3, Is4, Entry),
	    generateCodeClauses2(Cls, Entrys, Arity, L1, Is4, Is2)
	).

generateCodeClauses2([Cl|Cls], [Entry|Entrys], Arity, Label1, Is1, Is2) :-
	(var(Entry) ->
	    Is5 = Is3
	;
	    entryAddress(Entry, Address),
	    Is5 = [label(Address)|Is3]
	),
	( Cls = [] -> 
	    Is1 = [label(Label1), trust_me_else(Arity)|Is5], 
	    compileClause(Cl, Is3, Is2, Entry)
	;   
	    newLabelGen(L3),
	    Is1 = [label(Label1), retry_me_else(Arity, L3)|Is5],
	    compileClause(Cl, Is3, Is4, Entry),
	    generateCodeClauses2(Cls, Entrys, Arity, L3, Is4, Is2)
	).


compileClause(Clause, Is1final, Is2, Entry) :-
	Clause = clause(Head, Body, _, Ref, _),
	collectUnsafe(Ref, Unsafe),
	(Head = atom(_,_) ->
	    Arity = 0
	;
	    Head = functor(_,Arity,_,_)
	),
        newRegisterInfo(Arity, Unsafe, RI1),
	handleCut(Body, NewBody, Arity, RI1),
	heap_usage_head(Head, Arity, HeapUsage0),
	heap_usage_body(Body, HeapUsage0, HeapUsage),
	(HeapUsage > 1500 -> 
	    Is1 = [ check_free_heap_head(Arity, HeapUsage)|Is1b]
	;
	    Is1 = Is1b
	),
	erase(yvar_usage),
	compileHead(Head, Is1a, Is3, RI1, Entry),
	( Ref = [_,_|_] ->
	    check_allocate(proceed(MaxPerm), NewFin, RI1, Is1b, Is1a),
	    compileBody(NewBody, Is4, Is2, RI1, NewFin),
	    getMaxPerm(RI1, MaxPerm),
	    Is3a = Is4,
	    Is3 = Is3a
	;
	    compileBody(NewBody, Is3, Is2, RI1, proceed),
	    ( mybrecorded(yvar_usage, NrRegs) ->
		erase(yvar_usage),
		replace_xvar_yvar(Is1a, Is1Temp),
		AllocateNr is NrRegs - 248,
		Is1b = [allocate(AllocateNr) | Is1Temp]
	    ;
		Is1b = Is1a
	    )
	),
	(mybrecorded(yvar_usage, _) ->
	    compiler_error("Clause needing more than  250 registers could not be compiled",Head),
	    fail
	;
	    true
	),
	fill_size_env(Is1,_),
	Is1 = Is1final.


replace_xvar_yvar(Is1, Is2) :-
	var(Is1), !, Is2 = Is1.
replace_xvar_yvar([I|Is1a], Is2) :- !,
	replace_xvar_yvar_i(I, J),
	Is2 = [J|Is2a],
	replace_xvar_yvar(Is1a, Is2a).
replace_xvar_yvar([], []).

replace_xvar_yvar_i(I, J) :-
	I = unitvar(Reg),!,
	(Reg > 250 ->
	    YVar is Reg - 249,
	    J = unipvar(YVar)
	;
	    J = I
	).
replace_xvar_yvar_i(I, J) :-
	I = unitval(Reg), !,
	(Reg > 250 ->
	    YVar is Reg - 249,
	    J = unipval(YVar)
	;
	    J = I
	).
replace_xvar_yvar_i(I, J) :-
	I =.. [Name|Args],
	replace_xvar_yvar_i(Name, ToChange, NewName),
	replace_xvar_yvar_arg(ToChange, Args, NewArgs),
	J =.. [NewName|NewArgs].
replace_xvar_yvar_i(I, I).

replace_xvar_yvar_arg(1, [Arg|Args], New) :-
	Arg > 250,
	NewArg is Arg - 249,
	New = [NewArg|Args].
replace_xvar_yvar_arg([], New, New).

replace_xvar_yvar_i(unitvar, 1, unipvar).
replace_xvar_yvar_i(unitval, 1, unipval).
replace_xvar_yvar_i(get_structure, 1, getstrv).
replace_xvar_yvar_i(get_structure_untagged, 1, get_structurev_untagged).
replace_xvar_yvar_i(getlist, 1, getlistv).
replace_xvar_yvar_i(get_list_untagged, 1, get_listv_untagged).
replace_xvar_yvar_i(proceed, [], dealloc_proceed).

heap_usage_body([], Usage1, Usage1).
heap_usage_body([BodyEl|Body], Usage1, Usage2) :-
	heap_usage_body_el(BodyEl, Usage1, Usage3),
	( end_of_chunk(BodyEl) ->
	    Usage2 = Usage3
	;
	    heap_usage_body(Body, Usage3, Usage2)
	).

end_of_chunk(atomcall(_, _, _, _)).
end_of_chunk(functorcall(_, _, _, _, _)).
end_of_chunk('$initperm'(_,_)).

heap_usage_body_el(=(X,Y,_), Usage1, Usage2) :- !,
	heap_usage_term(X, Usage1, Usage3),
	heap_usage_term(Y, Usage3, Usage2).
heap_usage_body_el(atomcall(_, _, _, _), Usage1, Usage2) :- !, Usage2 = Usage1.
heap_usage_body_el(functorcall(_, _, Args, _, _), Usage1, Usage2) :- !,
	heap_usage_args(Args, Usage1, Usage2).
heap_usage_body_el('$initperm'(_,_), Usage1, Usage2) :- !, Usage2 = Usage1.

/* here we should add
    disjs/2, smallIE/4, ifthenelse/5.
*/
heap_usage_body_el(inline(Name, Arity, Args,_), Usage1, Usage2) :- !,
	(heap_usage_inline(Name, Arity, Usage1, Usage3) ->
	    heap_usage_args(Args, Usage3, Usage2)
	;
	    heap_usage_args(Args, Usage1, Usage2)
	).

heap_usage_body_el(_, Usage1, Usage2) :-
	/* some builtins consume one heap-cell */
	Usage2 is Usage1 + 1.

heap_usage_inline("functor", 3, Usage1, Usage2) :- Usage2 is Usage1 + 256.
heap_usage_inline("=..", 2, Usage1, Usage2) :- Usage2 is Usage1 + 512.
   /* Do we miss some crucial heap-eager builtins here? */

	
heap_usage_head(Head, Arity, Usage) :-
	( Head = functor(_, _, Args, _) ->
	    Start is -Arity,
	    heap_usage_args(Args, Start,Usage)
	;
	    Usage = 0
	).

heap_usage_term(var(_, _, _), Usage1, Usage2) :- Usage2 is Usage1 + 1.
heap_usage_term(atom(_, _), Usage1, Usage2) :- Usage2 is Usage1 + 1.
heap_usage_term(integer(Int, _), Usage1, Usage2) :-
	(bigint(Int) ->
	    bits_per_word(Bits),
	    (Int > 0 ->
		Usage is (msb(Int)//Bits) + 2
	    ;
		Usage is (msb(-Int)//Bits) + 2
	    ),
	    Usage2 is Usage1 + Usage
	;
	    Usage2 is Usage1 + 1
	).
heap_usage_term(float(_, _), Usage1, Usage2) :- Usage2 is Usage1 + 4.
heap_usage_term(string(S, _), Usage1, Usage2) :-
	string_length(S,L),
	heap_usage_string(L,Q),
	Usage2 is Usage1 + Q.
heap_usage_term(char(_, _), Usage1, Usage2) :- Usage2 is Usage1 + 1.
heap_usage_term(list(T1,T2,_), Usage1, Usage2) :-
	Usage3 is Usage1 + 1,
	heap_usage_term(T1, Usage3, Usage4), 
	heap_usage_term(T2, Usage4, Usage2). 

heap_usage_term(functor(_,_,Args,_), Usage1, Usage2) :-
	Usage3 is Usage1 + 2, 
	heap_usage_args(Args, Usage3, Usage2).


heap_usage_args([], Usage, Usage).
heap_usage_args([Term|Terms], Usage1, Usage2) :-
	heap_usage_term(Term, Usage1, Usage3),
	heap_usage_args(Terms, Usage3, Usage2).



%% % move_down_allocate is disabled - probably permanently
%% 
%% move_down_allocate(Is1, Is2, I) :- Is2 = [I|Is1].

handleCut(Body, NewBody, Arity, RI1) :-
	(Body = ['$mark'(var(_, Var, _))|NewBody] ->
	    NewArity is Arity + 1, 
	    ( Var = perm(_, _, _) ->
		freeInRI(NewArity, RI1),
		newPermVar(Var, [], [NewArity], RI1)
	    ;
		firstUseTemp(Var),
		freeInRI(NewArity, RI1),
		addToRI(NewArity, Var, RI1)
	    )
	;
	    NewBody = Body
	).

compileBody([], Is1, Is2, _, Fin) :-
	(Fin = jump(_) ->
	    Is1 = Is2
	;   
 	    map_final_command(Fin, NewFin),
	    Is1 = [NewFin|Is2]
	).
compileBody([BodyEl|Body], Is1, Is2, RI1, Fin) :-
	compileBodyEl(BodyEl, Is1, Is2, RI1, Fin, Body).


map_final_command(proceed(_), proceed) :- ! .
map_final_command(X, X).

final_command(dealloc_proceed, Nr, [deallex(Nr)|Is2], Is2).
final_command(proceed(_),         Nr, [execute(Nr,Nr)|Is2], Is2).
final_command(proceed,         Nr, [execute(Nr,Nr)|Is2], Is2).

add_allocate(Fin,  Fin, Is1, Is1) :- Fin =jump(_).
add_allocate(Fin,  Fin, Is1, Is1) :- Fin = dealloc_proceed.
add_allocate(Fin,  Fin, Is1, Is1) :- Fin = proceed.
add_allocate(proceed(MaxPerm), dealloc_proceed, Is1, Is2) :-
	Is1 = [allocate(MaxPerm) | Is2].

check_allocate(Fin, NewFin, _, Is1, Is2) :- Fin =jump(_), Is1 = Is2, Fin = NewFin.
check_allocate(Fin, NewFin, _, Is1, Is2) :- Fin = dealloc_proceed, Is1 = Is2, Fin = NewFin.
check_allocate(Fin, NewFin, _, Is1, Is2) :- Fin = proceed, Is1 = Is2, Fin = NewFin.
check_allocate(Fin, NewFin, RI, Is1, Is2) :-
	Fin = proceed(MaxPerm),
	getMaxPerm(RI, N),
	(N > 2  ->
	    Is1 = [allocate(MaxPerm) | Is2],
	    NewFin = dealloc_proceed
        ;
	    Is1 = Is2,
	    NewFin = Fin
	).

compileBodyEl(atomcall(Nr, Ref, _, Dummy), Is1, Is2, RI1, Fin, Body) :-  
	( Body = [], Fin \= jump(_) ->
	    final_command(Fin, Nr, Is1, Is2)
	;   
            add_allocate(Fin, NewFin, Is1, Is1a),
	    collectPermanentAndSaveRI(RI1, Yvars, Is1a, Is3),
	    constructBitmaps(Yvars, Len, BitMaps),
	    I =.. [active_yvar, Len| BitMaps],
	    (Dummy == yes ->
		Is3 = [dummy_call(Nr), I|Is4]
	    ;
		(Dummy == nop_call ->
		    Is3 = [nop_call(Nr), I|Is4]
		;
		    Is3 = [call(Nr,_), I|Is4]
		)
	    ),
	    adaptRI(RI1, Ref),
	    heap_usage_body(Body, 0, HeapUsage),
	    (HeapUsage > 300 -> 
		Is4 = [ check_free_heap_body(HeapUsage)|Is5]
	    ;
		Is4 = Is5
	    ),
	    compileBody(Body, Is5, Is2, RI1, NewFin)
	).

compileBodyEl(functorcall(Nr, _, Args, Ref, _), Is1, Is2, RI1, Fin, Body) :-
	fromArgumentsToHeap(Args, Is1a, Is3, RI1),
	( Body = [], Fin \= jump(_) ->
	     /* allocate might be needed because of approximation of
	        permanent variable liveness : */
	    check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	    final_command(NewFin, Nr, Is3, Is2)
	;   
            add_allocate(Fin, NewFin, Is1, Is1a), 
	    collectPermanentAndSaveRI(RI1, Yvars, Is3, Is4),
	    constructBitmaps(Yvars, Len, BitMaps),
	    I =.. [active_yvar, Len| BitMaps],
	    Is4 = [call(Nr,_), I|Is5],
	    adaptRI(RI1, Ref),
	    heap_usage_body(Body, 0, HeapUsage),
	    (HeapUsage > 300 -> 
		Is5 = [check_free_heap_body(HeapUsage)|Is6]
	    ;
		Is5 = Is6
	    ),
	    compileBody(Body, Is6, Is2, RI1, NewFin)
	).

compileBodyEl('$initperm'(Perms,_Context), Is1, Is2, RI1, Fin, Body) :-
	compile_init_perm(Perms, Is1a, Is3, RI1),
	check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	compileBody(Body, Is3, Is2, RI1, NewFin).

compileBodyEl(=(X,Y,_), Is1, Is2, RI1, Fin, Body) :-
	compileEquality(X, Y, Is1a, Is3, RI1),
	check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	compileBody(Body, Is3, Is2, RI1, NewFin). 
compileBodyEl(X is Y, Is1, Is2, RI1, Fin, Body) :-  
	compileIsExpression(X, Y, Is1a, Is3, RI1),
	check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	compileBody(Body, Is3, Is2, RI1, NewFin).


compileBodyEl(fail, Is1, Is2, RI1, Fin, Body) :-
	Is1 = [fail|Is3],
	compileBody(Body, Is3, Is2, RI1, Fin).

compileBodyEl('_$cutto'(VV), Is1, Is2, RI1, Fin, Body) :-
	VV = var(_, PVar, _),
	(PVar = perm(X,_,Var), nonvar(Var) ->
	    (findRegPermCheck(RI1, PVar, Isin) ->
		( Isin = [R|_] ->
		    Is1a = [puttbreg(R)|Is3]
		;
		    Is1a = [putpbreg(X)|Is3]
		)
	    ;
		 Is1a = [putpbreg(X)|Is3]
	    ),
	    permOccurred(PVar, RI1)
	;   
	    ( PVar = temp(_, _, Var), nonvar(Var) ->
		findReg(RI1, PVar, R),
		Is1a = [puttbreg(R)|Is3],
		tempOccurred(PVar, RI1)
	    ;	
		compiler_error("Clause with cut/1 without mark/1",nocontext),
		fail
	    )
	),
	check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	compileBody(Body, Is3, Is2, RI1, NewFin). 
compileBodyEl('_$savecp'(VV), Is1, Is2, RI1, Fin, Body) :-
	VV = var(_, PVar, _),
	(PVar = perm(X,_,Var) ->
	    ( var(Var) ->
		Is1a = [getpbreg(X)|Is4],
		newPermVar(PVar, [X], [], RI1)
	    ;
		( findRegPermCheck(RI1, PVar, Isin), Isin =  notfound ->
		    Is1a = [getpbreg(X)|Is4],
		    newPermVarDisj(PVar, [X], [], RI1)
		;
		    compiler_error("Clause with mark/1 with used argument",nocontext),
		    fail
		)
	    )
	;   
	    ( PVar = temp(Dest, _, Var), var(Var) ->
		findFreeRegAlways(PVar, Dest, Reg, RI1, Is1a, Is3),
		Is3 = [gettbreg(Reg)|Is4]
	    ;	
		compiler_error("Clause with mark/1 with used argument",nocontext),
		fail
	    )
	),
	check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	compileBody(Body, Is4, Is2, RI1, NewFin). 


compileBodyEl(disjs(Disjs, RefBody), Is1, Is2, RI1, Fin, Body) :-
        add_allocate(Fin, BodyFin, Is1, Is1a), 
	( Body = [], Fin \= jump(_) ->
	    NewFin = BodyFin,
	    Is5 = Is6
	;
	    newLabelGen(L),
	    Is5 = [label(L)|Is6],
	    NewFin= jump(L)
	),
	compileBodyElDisjs(Disjs, Is1a, Is5, _, RI1, 0, NewFin,yes,[],[]),
	adaptRI(RI1, RefBody),
	( Body = [] ->
	    Is6 = Is2
	;
	    compileBody(Body, Is6, Is2, RI1, BodyFin)
	).

compileBodyEl(smallITE(If, Then, Else, Refs), Is1, Is2, RI1, Fin, Body) :-
	Refs =  i(RefA,RefB,RefC),
	newLabelGen(L),
	( If = [Test|Tests] ->
	    compileTest(Test, Is1a, Is3a, RI1, L),
	    copyRI(RI1, RI3),	% do we have to take into account RefA?
	    compileAllTests(Tests, Is3a, Is3, RI3, L)
	;
	    copyRI(RI1, RI3),	% do we have to take into account RefA?
	    Is1a = Is3
	),
	( Body = [], Fin \= jump(_) ->
	    Is1 = Is1a, % dangerous ?
	    compileBody(Then, Is3, Is4, RI3, Fin),
	    Is4 = [label(L)|Is5],
	    copyRIForb(RI1, RefA, RefB, [], RI6), 
	    compileBody(Else, Is5, Is2, RI6, Fin),
	    getMaxPerm(RI3, MPBranch1),
	    getMaxPerm(RI6, MPBranch2),
	    MP is max( MPBranch1, MPBranch2),
	    setMaxPerm(MP, RI1)
	;   
	    add_allocate(Fin, BodyFin, Is1, Is1a), % no idea how to postpone 
	    NewFin= jump(L2),
	    newLabelGen(L2),
	    Is7a = [jump(L2)|Is7b],
	    Is9 = [label(L2)|Is10],
	    compileBody(Then, Is3, Is7, RI3, NewFin),
	    collectPermanentAndSaveRI(RI3, _, Is7, Is7aa),
	    Is7b = [label(L)|Is8],
	    getForbiddenPerm(RI1, RI3, [], Forb),
	    copyRIForb(RI1, RefA, RefB, Forb, RI6), 
	    compileBody(Else, Is8, Is8aa, RI6, NewFin),
	    collectPermanentAndSaveRI(RI6, _, Is8aa, Is8a),
	    getForbiddenPerm(RI1, RI6, Forb, Forb2),
	    getMaxPerm(RI3, MPBranch1),
	    getMaxPerm(RI6, MPBranch2),
	    MP is max( MPBranch1, MPBranch2),
	    setMaxPerm(MP, RI1),
	    checkPermCrea([permCrea(RI3, Is7aa, Is7a),
			   permCrea(RI6, Is8a, Is9)], Forb2, RI1),
	    /* go on with the remaining body */
	    adaptRI(RI1, RefC),
	    (Body = [] -> Is10 = Is2
	    ;
		compileBody(Body, Is10, Is2, RI1, BodyFin)
	    )
	).

compileBodyEl(ifthenelse(Ref, If, Then, Else, RefB), Is1, Is2, RI1,Fin, Body) :-
        /* this is the general scheme */

        add_allocate(Fin, BodyFin, Is1, Is1a),
        /* prepare for starting a disjunction */
	newLabelGen(L),
	collectPermanentAndSaveRI(RI1, Yvars, Is1a, Is3),
	constructBitmaps(Yvars, Len, BitMaps),
	I =.. [active_yvar, Len| BitMaps],
	Is3 = [trymeorelse(L), I|Is4],

	/* compile the IF and THEN */
	copyRI(RI1, RI3),
	compileBody(If, Is4, Is5, RI3, jump(_)),
	Is5 = Is6,
	( Body = [], Fin \= jump(_) ->
	    NewFin = BodyFin, Is7a = Is7b,
	    Is9 = Is10
	;   
	    NewFin= jump(L2),
	    newLabelGen(L2),
	    Is7a = [jump(L2)|Is7b],
	    Is9 = [label(L2)|Is10]
	),
	compileBody(Then, Is6, Is7, RI3, NewFin),
	collectPermanentAndSaveRI(RI3, _, Is7, Is7aa),
	Is7b = [label(L), trustmeorelsefail|Is8],
	getForbiddenPerm(RI1, RI3, [], Forb),

	/* compile the ELSE-part */
	newRIForb(RI1, Ref, Forb, RI6),
	compileBody(Else, Is8, Is8aa, RI6, NewFin),
	collectPermanentAndSaveRI(RI6, _, Is8aa, Is8a),
	
	/* Collect and store info on permanent variables */
	getForbiddenPerm(RI1, RI6, Forb, Forb2),
	getMaxPerm(RI3, MPBranch1),
	getMaxPerm(RI6, MPBranch2),
	MP is max( MPBranch1, MPBranch2),
	setMaxPerm(MP, RI1),
	checkPermCrea([permCrea(RI3, Is7aa, Is7a),
		       permCrea(RI6, Is8a, Is9)], Forb2, RI1),

	/* go on with the remaining body */
	adaptRI(RI1, RefB),
	(Body = [] ->
	    Is10 = Is2
	;
	    compileBody(Body, Is10, Is2, RI1, BodyFin)
	).

compileBodyEl(test(Name, Args,_), Is1, Is2, RI1, Fin, Body) :-
	compileTest(Name, Args, Is1a, Is3, RI1, -1),
	check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	compileBody(Body, Is3, Is2, RI1, NewFin).
compileBodyEl(numericaltest(Name, Arg1, Arg2,_), Is1, Is2, RI1, Fin, Body) :-
	compileNumericalTest(Name, Arg1, Arg2, Is1a, Is3, RI1, -1),
	check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	compileBody(Body, Is3, Is2, RI1, NewFin).

compileBodyEl(inline(Name, Arity, Args,_), Is1, Is2, RI1, Fin, Body) :-
	(specialized_inline_builtin(_, Arity, Name, SpecArgs)
	->
	    fromArgumentsToHeapIS(Args, SpecArgs, Regs, Is1a, Is3, RI1),
	    Is3 = [inline(Name, Arity, Regs)|Is4bis],
	    % something for the permvars only
	    fromArgumentsToHeapIVarsSputperms(Args,Regs,Is4bis,Is4,RI1)
	;
	    (fake_builtin(Name, Arity, Args, Is1, Is4, RI1) ->
		true
	    ;
		fromArgumentsToHeapI(Args, Regs, Is1a, Is3, RI1),
		Is3 = [inline(Name, Arity, Regs)|Is4]
	    )
	),
	check_allocate(Fin, NewFin, RI1, Is1, Is1a),
	compileBody(Body, Is4, Is2, RI1, NewFin).

%% compileBodyEl(inline(Name, Arity, Args,_), Is1, Is2, RI1, Fin, Body) :-
%% 	        (
%% 		  specialized_inline_builtin(_, Arity, Name, SpecArgs),
%% 		  noperms(Args) ->
%% 		  fromArgumentsToHeapIS(Args, SpecArgs, Regs, Is1a, Is3, RI1),
%% 		  Is3 = [inline(Name, Arity, Regs)|Is4]
%% 		;
%% 		% Fri Oct 19 11:17:17 CEST 2007 because permvars can't be argument
%% 		% of a very specialized inlined thing
%% 		  specialized_inline_builtin(OldName,Arity, Name,_) ->
%% 		  fromArgumentsToHeapI(Args, Regs, Is1a, Is3, RI1),
%% 		  Is3 = [inline(OldName, Arity, Regs)|Is4]
%% 		;
%% 		  fake_builtin(Name, Arity, Args, Is1, Is4, RI1) ->
%% 		  true
%% 		;
%% 		  fromArgumentsToHeapI(Args, Regs, Is1a, Is3, RI1),
%% 		  Is3 = [inline(Name, Arity, Regs)|Is4]
%% 		),
%% 		check_allocate(Fin, NewFin, RI1, Is1, Is1a),
%% 		compileBody(Body, Is4, Is2, RI1, NewFin).

noperms([]).
noperms([X|R]) :-
	(X = var(_,perm(_,_,_),_) ->
	    fail
	;
	    noperms(R)
	).


fake_builtin("asm__M_sysh", 1, [Arg], Is1, Is2, RI1) :-
	Arg = functor(Nr,Arity,Args,_),
	Is1 = [Instruction|Is2],
	b_getval('%%infoatoms%%',InfoAtoms),
	member(a(Name,Arity,user,Nr),InfoAtoms),
	transform_instruction_args(Args,NewArgs,RI1),
	Instruction =.. [Name|NewArgs],
	!.

fake_builtin("asm__M_sysh", 1, [Arg], Is1, Is2, RI1) :-
	Arg = atom(Nr,_),
	b_getval('%%infoatoms%%',InfoAtoms),
	member(a(Name,0,user,Nr),InfoAtoms),
	Is1 = [Name|Is2],
	!.

fake_builtin("drop_areg__M_sysh", 2, Args, Is1, Is2, RI1) :-
	Args = [integer(X,_), var(_, TPVar, _)],
	( TPVar = temp(_, _, Occ1), nonvar(Occ1) ->
	    findReg(RI1, TPVar, Reg),
	    (Reg == X -> Is1 = Is2
	    ;
		freeRegisterCall(X, Is1, Is3, RI1),
		Is3 = [movreg(Reg,X)|Is2],
		addToRI(X, TPVar, RI1)
	    ),
	    tempOccurred(TPVar, RI1)
	;
	    (TPVar = perm(NR, _, Occ2), nonvar(Occ2) ->
		( findRegPermCheck(RI1, TPVar, Isin), mymember(X, Isin) ->
		    /* Permanent variabele already has a copy in Reg X */
		    Is1 = Is2
		;
		    freeRegisterCall(X, Is1, Is3, RI1),
		    ( findRegPermCheck(RI1, TPVar, Isin) ->
			( Isin = [R|_] ->
			    /* Moving from another register might be cheaper */
			    Is3 = [movreg(R, X)|Is2]
			;
			    /* could happen in disjunctions: first occurence */
			    write(stderr, "invalid argument for drop_areg, free var\n"), fail
			)
		    ;
			Is3 = [putpval(NR, X)|Is2]
		    ),
		    permOccurred(TPVar, RI1)
		)
	    ;
		write(stderr, "invalid argument for drop_areg\n"), fail
	    )
	).

fake_builtin("assign_areg__M_sysh", 2,Args, Is1, Is1, RI1) :-
	Args = [integer(X,_), var(_, TPVar, _)],
	( TPVar = temp(_, _, Var), var(Var) ->
	    addToRI(X, TPVar, RI1),
	    firstUseTemp(TPVar)
	;
	    (TPVar =  perm(_, _, Var), var(Var) ->
		newPermVar(TPVar, [], [X], RI1)
	    ;
		write(stderr, "invalid argument for assign_areg\n"), fail
	    )
	).

transform_instruction_args([],[],_).
transform_instruction_args([A|As],[NA|NAs],RI1) :-
	(
	  A = integer(AA,_) ->
	  NA = AA
	;
	  A = var(_,perm(Y, _, Var),_) ->
	  nonvar(Var), NA = Y
	;
	  (A = var(_,TVar,_), TVar = temp(_, _, Var)) ->
	  nonvar(Var),
	  findReg(RI1, TVar, X),
	  tempOccurred(TVar, RI1),
	  NA = X
	;
	  A = functor(NR,_,_,_) ->
	  NA = NR
	),
	transform_instruction_args(As,NAs,RI1).



compileBodyElDisjs([], Is1, Is1, _,  RI1, MP, _, _, Forb, PermCrea) :-
	setMaxPerm(MP, RI1),
	checkPermCrea(PermCrea, Forb, RI1).
compileBodyElDisjs([Disj|Disjs], Is1, Is2, L, RI, MPold, Fin,
		   First, Forb, PermCrea) :-
	( First = yes -> /* XXX first disjunct should be treated different,
	                    it could keep on using registers! */
	    collectPermanentAndSaveRI(RI, Yvars, Is1, Is3),
	    constructBitmaps(Yvars, Len, BitMaps),
	    I =.. [active_yvar, Len| BitMaps],
	    newLabelGen(AlterLab),
	    Is3 = [trymeorelse(AlterLab), I|Is4]
	;
	    (Disjs = [] ->
		I = trustmeorelsefail,
		AlterLab = _
	    ;	
		newLabelGen(AlterLab),
		I = retrymeorelse(AlterLab)
	    ),
	    Is1 = [label(L), I|Is4]
	),
	Disj = ['$startDisj'(Ref)|Body],
	newRIForb(RI, Ref, Forb, RICopy),
% 	compileBody(Body, Is4, Is5, RICopy, Fin),
	% prev line vervangen door next 2 op 3-2-2003
	compileBody(Body, Is4, Is5a, RICopy, Fin),
	/* some permanent variables could still be stored in registers */
	collectPermanentAndSaveRI(RICopy, _, Is5a, Is5),
	getMaxPerm(RICopy, MPbranch),
	MPold1 is max(MPold, MPbranch),
	getForbiddenPerm(RI, RICopy, Forb, Forb1),
	(Fin = jump(_), Disjs \= [] -> Is6 = [Fin|Is7]; Is6 = Is7),
	compileBodyElDisjs(Disjs, Is7, Is2, AlterLab, RI, MPold1, Fin,
			   no, Forb1, [permCrea(RICopy, Is5, Is6)|PermCrea]).


compileAllTests([], Is1, Is1, _, _).
compileAllTests([Head|Tail], Is1, Is2, RI1, L) :-
	compileTest(Head, Is1, Is3, RI1, L),
	compileAllTests(Tail, Is3, Is2, RI1, L).


 
constructBitmaps(L,Len,Bitmaps) :-
	sort(L,SL),
	makebitmaps(SL,0,2,25,Bitmaps1, Len1, 0),
	( Bitmaps1 = [] ->
	    Bitmaps = [0,0], Len = 1
	;   
	    Bitmaps = Bitmaps1, Len = Len1
	).


makebitmaps([],_,_,_,[], Len, Len) :- ! .
makebitmaps(L,N,A,B,Bitmaps, Len, AccLen) :-
	L \== [],
	eat(L,A,B,RestL,0,Bitmap),
	M is N + 1,
	AA is A + 24,
	BB is B + 24,
	( Bitmap = 0 ->
	    makebitmaps(RestL,M,AA,BB,Bitmaps, Len, AccLen)
	;   
	    Bitmaps = [N, Bitmap|RestBitmaps],
            AccLen1 is AccLen + 1,
	    makebitmaps(RestL,M,AA,BB,RestBitmaps, Len, AccLen1)
	).

eat([],_,_,[],I,I).
eat([X|RX],A,B,OutL,I,O) :-
	( X > B ->
	    OutL = [X|RX],
	    I = O
	;   
	    XX is X - A,
	    II is I + (1 << XX),
	    eat(RX,A,B,OutL,II,O)
	).



fromArgumentsToHeap(Args, Is1, Is2, RI) :-
	fromArgumentsToHeapCompound(Args, 1, Is1, Is3, RI),
	fromArgumentsToHeapVariable(Args, 1, Is3, Is4, RI),
	fromArgumentsToHeapKnown(Args,    1, Is4, Is2, RI).

fromArgumentsToHeapCompound([], _, Is, Is, _).
fromArgumentsToHeapCompound([Arg|Args], NrArg, Is1, Is2, RI1) :-
	( is_compound(Arg) ->
	    freeRegister(NrArg, Is1, Is3, RI1),
	    putTermInReg(Arg, NrArg, Is3, Is4, RI1)
	;   
	    Is1 = Is4	
	),
	NrArg1 is NrArg + 1,
	fromArgumentsToHeapCompound(Args, NrArg1, Is4, Is2, RI1).

fromArgumentsToHeapVariable([], _, Is, Is, _).
fromArgumentsToHeapVariable([Arg|Args], NrArg, Is1, Is2, RI1) :-
	( Arg = var(_, Info, _) ->
	    fromArgumentToHeapVariable(Info, NrArg, Is1, Is3, RI1)
	;   
	    Is3 = Is1
	),
	NrArg1 is NrArg + 1,
	fromArgumentsToHeapVariable(Args, NrArg1, Is3, Is2, RI1).

fromArgumentsToHeapKnown([], _, Is, Is, _).
fromArgumentsToHeapKnown([Arg|Args], NrArg, Is1, Is2, RI1) :-
	( is_not_compound(Arg) ->  
	    freeRegisterCall(NrArg, Is1, Is3, RI1),
	    putTermInReg(Arg, NrArg, Is3, Is4, RI1)
	;   
	    Is4 = Is1
	),
	NrArg1 is NrArg + 1,
	fromArgumentsToHeapKnown(Args, NrArg1, Is4, Is2, RI1).

is_not_compound(atom(_, _)).
is_not_compound(integer(_, _)).
is_not_compound(float(_, _)).
is_not_compound(string(_, _)).
is_not_compound(char(_, _)).

is_compound(functor(_, _, _, _)).
is_compound(list(_,_,_)).

fromArgumentsToHeapI(Args, Regs, Is1, Is2, RI1) :-
	fromArgumentsToHeapINonVar(Args, Regs, Is1, Is3, RI1, UsedRegs1),
	fromArgumentsToHeapIUnsafe(Args, Regs, Is3, Is4, RI1),
	fromArgumentsToHeapIVars(Args, Regs, Is4, Is2, RI1, UsedRegs2),
	addToFreeInRI(UsedRegs1, RI1),
	addToFreeInRI(UsedRegs2, RI1). 

fromArgumentsToHeapIS(Args, Spec, Regs, Is1, Is2, RI1) :-
	fromArgumentsToHeapINonVarS(Args, Spec, Regs, Is1, Is3, RI1, UsedRegs1),
	fromArgumentsToHeapIUnsafeS(Args, Spec,  Regs, Is3, Is4, RI1),
	fromArgumentsToHeapIVarsS(Args, Spec, Regs, Is4, Is2, RI1, UsedRegs2),
	addToFreeInRI(UsedRegs1, RI1),
	addToFreeInRI(UsedRegs2, RI1).

fromArgumentsToHeapINonVarS([], _, [], Is, Is, _, []).
fromArgumentsToHeapINonVarS([Arg|Args], [Spec|Specs], [Reg|Regs], Is1, Is2, RI1, UR) :-
	( (Arg = var(_, _, _) ; (Spec == smallposint ; Spec == atom)) ->
	    fromArgumentsToHeapINonVarS(Args, Specs, Regs, Is1, Is2, RI1, UR)
	;
	    fromArgumentToHeapINonVar(Arg, Reg, Is1, Is3, RI1, UR, UR1),
	    fromArgumentsToHeapINonVarS(Args, Specs, Regs, Is3, Is2, RI1, UR1)
	).

fromArgumentsToHeapINonVar([], [], Is, Is, _, []).
fromArgumentsToHeapINonVar([Arg|Args], [Reg|Regs], Is1, Is2, RI1, UR) :-
	( Arg = var(_, _, _) ->
	    fromArgumentsToHeapINonVar(Args, Regs, Is1, Is2, RI1, UR)
	;   
	    fromArgumentToHeapINonVar(Arg, Reg, Is1, Is3, RI1, UR, UR1),
	    fromArgumentsToHeapINonVar(Args, Regs, Is3, Is2, RI1, UR1)
	).

fromArgumentsToHeapIUnsafe([], [], Is, Is, _).
fromArgumentsToHeapIUnsafe([Arg|Args], [Reg|Regs], Is1, Is2, RI1) :-
	( Arg = var(_, Type, _), Type = temp(Dest, _, Var),
	    nonvar(Dest), var(Var) ->
	    fromArgumentToHeapIVar(Type, Reg, Is1, Is3, RI1, _, _),
	    fromArgumentsToHeapIUnsafe(Args, Regs, Is3, Is2, RI1)
	;   
	    fromArgumentsToHeapIUnsafe(Args, Regs, Is1, Is2, RI1)
	).

fromArgumentsToHeapIVars([], [], Is, Is, _, []).
fromArgumentsToHeapIVars([Arg|Args], [Reg|Regs], Is1, Is2, RI1, UR) :-
	( Arg = var(_, Type, _), var(Reg) ->
	    fromArgumentToHeapIVar(Type, Reg, Is1, Is3, RI1, UR, UR1),
	    fromArgumentsToHeapIVars(Args, Regs, Is3, Is2, RI1, UR1)
	;   
	    fromArgumentsToHeapIVars(Args, Regs, Is1, Is2, RI1, UR)
	).

fromArgumentsToHeapIUnsafeS([], _, [], Is, Is, _).
fromArgumentsToHeapIUnsafeS([Arg|Args],[Spec|Specs],[Reg|Regs], Is1, Is2, RI1) :-
	( Arg = var(_, Type, _), Type = temp(Dest, _, Var),
	    nonvar(Dest), var(Var) ->
	    (
	      Spec ==  new ->
	      fromArgumentToHeapIVarS(Type, Reg, Is1, Is3, RI1, _, _)
	    ;
	      Spec == ? ->
	      fromArgumentToHeapIVar(Type, Reg, Is1, Is3, RI1, _, _)
	    ;
	      compiler_error(fromArgumentsToHeapIUnsafeS,nocontext)
	    ),
	    fromArgumentsToHeapIUnsafeS(Args, Specs, Regs, Is3, Is2, RI1)
	;
	    fromArgumentsToHeapIUnsafeS(Args, Specs, Regs, Is1, Is2, RI1)
	).

fromArgumentsToHeapIVarsS([], _, [], Is, Is, _, []).
fromArgumentsToHeapIVarsS([Arg|Args], [Spec|Specs], [Reg|Regs], Is1, Is2, RI1, UR) :-
	(
	  Arg = var(_, Type, _), var(Reg) ->
	  (
	    Spec == new, (is_new_var(Type) ; Type == void) -> % is_new_var not needed ?
	    fromArgumentToHeapIVarS(Type, Reg, Is1, Is3, RI1, UR, UR1)
	  ;
	    Spec == ? ->
	    fromArgumentToHeapIVar(Type, Reg, Is1, Is3, RI1, UR, UR1)
	  ),
	  fromArgumentsToHeapIVarsS(Args, Specs, Regs, Is3, Is2, RI1, UR1)
	;
	  Spec == smallposint ->
	  UR = UR1,
	  Is1 = Is3,
	  Arg = integer(I,_),
	  Reg = I,
	  fromArgumentsToHeapIVarsS(Args, Specs, Regs, Is3, Is2, RI1, UR1)
	;
	  Spec == atom ->
	  UR = UR1,
	  Is1 = Is3,
	  Arg = atom(I,_),
	  Reg = I,
	  fromArgumentsToHeapIVarsS(Args, Specs, Regs, Is3, Is2, RI1, UR1)
	;
	  fromArgumentsToHeapIVarsS(Args, Specs, Regs, Is1, Is2, RI1, UR)
	).


fromArgumentToHeapIVar(void, X, Is1, Is2, RI1, UR1, UR2) :- !,
	UR1 = [X|UR2],
	findSaveRegister(X, RI1),
        Is1 = [bldtvar(X)|Is2].
fromArgumentToHeapIVar(TVar, Reg, Is1, Is2, RI1, UR1, UR2) :-
	TVar = temp(Dest, _, Var), !,
	( var(Var) -> 
	    findFreeRegAlways(TVar, Dest, Reg, RI1, Is1, Is3),
	    Is3 = [bldtvar(Reg)| Is2],
	    UR1 = UR2
	;   
	    Is1 = Is2,
	    findReg(RI1, TVar, Reg),
	    ( notLast(TVar) ->
		UR1 = UR2,
		tempOccurred(TVar, RI1)
	    ;	
		UR1 = [Reg|UR2],
		tempOccurred(TVar, RI1),
		freeInRI(Reg, RI1) 
	    )
	).
fromArgumentToHeapIVar(PVar, R, Is1, Is2, RI1, UR1, UR2) :-
	PVar = perm(X, _, Var), 
	( var(Var) -> 
	    findSaveRegister(R, RI1),
	    UR1 = UR2,
	    Is1 = [putpvar(X, R)|Is2],
	    newPermVar(PVar, [X], [R], RI1)
 	;   
	    ( findRegPermCheck(RI1, PVar, Isin) ->
		(   Isin = [R|_] ->
		    Is1 = Is2
		;
		    findSaveRegister(R, RI1),
		    Is1 =[putpvar(X, R)|Is2],
		    newPermVarDisj(PVar, [X], [R], RI1)
		)
	    ;	
		findSaveRegister(R, RI1),
		Is1 = [putpval(X, R)|Is2],
		addToRIPerm(R, PVar, RI1)
	    ),
	    ( notLast(PVar) -> 
		UR1 = UR2, permOccurred(PVar, RI1)
	    ;	
		permOccurred(PVar, RI1),
		freeInRI(R, RI1), UR1 = [R|UR2]
	    )
	).

fromArgumentToHeapIVarS(void, X, Is1, Is2, _, UR1, UR2) :- !,
	UR1 = [X|UR2], X = 0,
        Is1 = Is2.
fromArgumentToHeapIVarS(TVar, Reg, Is1, Is2, RI1, UR1, UR2) :-
	TVar = temp(Dest, _, Var), !,
	var(Var), /* just to be sure */
	findFreeRegAlways(TVar, Dest, Reg, RI1, Is1, Is3),
	Is3 = Is2,
	UR1 = UR2.
fromArgumentToHeapIVarS(_PVar, R, Is1, Is1, RI1, UR1, UR1) :-
	findSaveRegister(R, RI1).
%% fromArgumentToHeapIVarS(PVar, R, Is1, Is2, RI1, UR1, UR2) :-
%% 	PVar = perm(X, _, Var), 
%% 	var(Var), /* just to be sure */
%% 	findSaveRegister(R, RI1),
%% 	UR1 = UR2,
%% 	Is1 = [putpvar(X, R)|Is2],
%% 	newPermVar(PVar, [X], [R], RI1).  % die [] lijkt mis - in [X] veranderd


fromArgumentsToHeapIVarsSputperms([],_,Is,Is,_).
fromArgumentsToHeapIVarsSputperms([Arg|PVars],[Reg|Rs],Is1,Is2,RI1) :-
	( Arg = var(_, PVar, _), PVar = perm(X, _, V), var(V) ->
	    Is1 = [getpvar(X,Reg) | Is3],
	    newPermVar(PVar, [X], [Reg], RI1)
	;
	    Is1 = Is3
	),
	fromArgumentsToHeapIVarsSputperms(PVars,Rs,Is3,Is2,RI1).



fromArgumentToHeapINonVar(Value, Reg, Is1, Is2, RI1, UR1, UR2) :-
	UR1 = [Reg|UR2],
	findSaveRegister(Reg, RI1),
	putTermInReg(Value, Reg, Is1, Is2, RI1).


fromArgumentToHeapVariable(void, NrArg, Is1, Is2, RI1) :- !,
	freeRegisterCall(NrArg, Is1, Is3, RI1),
        Is3 = [bldtvar(NrArg)|Is2].
fromArgumentToHeapVariable(TVar, NrArg, Is1, Is2, RI1) :-
        TVar = temp(_, _, Var), !,
	( var(Var) -> 
	    freeRegister(NrArg, Is1, Is3, RI1),
	    addToRI(NrArg, TVar, RI1),
	    Is3 =[bldtvar(NrArg)|Is2],
	    firstUseTemp(TVar)
        ;   
	    ( findReg(RI1, TVar, NrArg) ->
		Is1 = Is2
	    ;	
		freeRegisterCall(NrArg, Is1, Is3, RI1),
		findReg(RI1, TVar, R),
		addToRI(NrArg, TVar, RI1),
		Is3 = [movreg(R, NrArg)|Is2]
	    ),
	    tempOccurred(TVar, RI1)
	).
fromArgumentToHeapVariable(PVar, NrArg, Is1, Is2, RI1) :-
	PVar = perm(X, _, Var),
        ( var(Var) ->
	    freeRegisterCall(NrArg, Is1, Is3, RI1),
	    Is3 =[putpvar(X, NrArg)|Is2],
	    newPermVar(PVar, [X], [NrArg], RI1)
	;   
	    ( findRegPermCheck(RI1, PVar, Isin), mymember(NrArg, Isin) ->
		Is1 = Is2
	    ;	
		freeRegisterCall(NrArg, Is1, Is3, RI1),
		( findRegPermCheck(RI1, PVar, Isin) ->
		    ( Isin = [R|_] ->
			Is3 = [movreg(R, NrArg)|Is2]
		    ;
			Is3 =[putpvar(X, NrArg)|Is2],
			newPermVarDisj(PVar, [X], [NrArg], RI1)
		    )
		;   
		    Is3 = [putpval(X, NrArg)|Is2]
		)
	    ),
	    permOccurred(PVar, RI1),
	    addUnsafeReg(RI1, NrArg)
	).

putTermInReg(atom(Atom, _), Reg, Is1, Is2, _) :-
	Is1 = [put_atom(Reg, Atom)|Is2].
putTermInReg(integer(Int, _), Reg, Is1, Is2, _) :-
	(bigint(Int) ->
	    Is1 = [put_bigint(Reg, Int)|Is2]
	;
	    Is1 = [put_int(Reg, Int)|Is2]
	).
putTermInReg(float(Float, _), Reg, Is1, Is2, _) :-
	Is1 = [put_float(Reg, Float)|Is2].
putTermInReg(string(S, _), Reg, Is1, Is2, _) :-
	string_length(S,L),
	Is1 = [put_string(Reg, L, S)|Is2].
putTermInReg(char(S, _), Reg, Is1, Is2, _) :-
	Is1 = [put_char(Reg, S)|Is2].
putTermInReg(list(X,Y,_), Reg, Is1, Is2, RI1) :- !,
	Is1 = [putlist(Reg)|Is3],
	putSubTermsOnHeap([X,Y], 2, Is3, Is2, _, RI1).
putTermInReg(functor(Nr, Arity, Args, _), Reg, Is1, Is2, RI1) :-
	Is1 = [put_structure(Reg, Nr)|Is3],
	putSubTermsOnHeap(Args, Arity, Is3, Is2, _, RI1).


putTermInVar(Term, Var, Is1, Is2, RI1) :-
	putTermInReg(Term, Var, Is3, Is2, RI1),
	Is3 = [I|Is4],
	I =.. [Name|Args],
	atom_concat(Name, v, NewName), /* XXX */
	I1 =.. [NewName|Args],
	Is1 = [I1|Is4].

putSubTermsOnHeap(SubTs, Adres, Is1, Is2, L, RI1) :-
	putSubTermsOnHeap1(SubTs, Ns, Is1, Is3, RI1),
	putSubTermsOnHeap2(SubTs, Adres, Ns, Is3, Is2, L, RI1).


putSubTermsOnHeap1([], _, Is, Is, _).
putSubTermsOnHeap1([SubT|SubTs], [N| Ns], Is1, Is2, RI1) :-
	putSubTermOnHeap1(SubT, N, Is1, Is3, RI1),
	putSubTermsOnHeap1(SubTs, Ns, Is3, Is2, RI1).

putSubTermsOnHeap2([], _, _,  Is,  Is, 0, _).
putSubTermsOnHeap2([SubT|SubTs], Adr, [Adr| Ns], Is1, Is2, L, RI1) :-
	putSubTermOnHeap2(SubT, Is1, Is3, SubL, RI1),
	NewAdres is Adr + SubL - 1,
	putSubTermsOnHeap2(SubTs, NewAdres, Ns, Is3, Is2, L2, RI1),
	L is L2 + SubL.


putSubTermOnHeap1Var(void, Is1, Is2, _) :- !,
	Is1 =  [set_void |Is2].
putSubTermOnHeap1Var(TVar, Is1, Is2, RI1) :-
	TVar = temp(Dest, _, Var), !,
	(var(Var) -> 
	    findFreeRegAlways(TVar, Dest, Reg, RI1, Is1, Is3),
	    Is3 =[bldtvar(Reg)| Is2]
	;   
	    findReg(RI1, TVar, X),
	    Is1 = [bldtval(X) |Is2],
	    tempOccurred(TVar, RI1)
	).
putSubTermOnHeap1Var(PVar, [I|Is], Is, RI1) :-
	PVar = perm(X, _, Var), 
	( var(Var) ->
	    I = bldpvar(X),
	    newPermVar(PVar, [X], [], RI1)
	;   
	    ( findRegPermCheck(RI1, PVar, Isin) ->
		( Isin = [R|_] -> 
		    I = bldtval(R)
		;   
		    I = bldpvar(X),
		    newPermVarDisj(PVar, [X], [], RI1)
		)
	    ;	
		I = bldpval(X)
	    ),
	    permOccurred(PVar, RI1)
	).

putSubTermOnHeap1(var(_, Var, _),        _, Is1, Is2, RI1) :-
	putSubTermOnHeap1Var(Var, Is1, Is2, RI1).
putSubTermOnHeap1(atom(Atom, _),      _, [build_atom(Atom)|  Is], Is, _).
putSubTermOnHeap1(integer(Int, C),    _, Is1, Is2, _) :-
	(bigint(Int) ->
	    compiler_error("putSubTermOnHeap1 big int",C)
	;
	    Is1 = [build_int(Int)| Is2]
	).
putSubTermOnHeap1(float(_, _),      Adr, [set_float(Adr)|Is], Is, _).
putSubTermOnHeap1(string(_, _),      Adr, [set_string(Adr)|Is], Is, _).
putSubTermOnHeap1(char(Char, _),      _, [build_char(Char)|Is], Is, _).
putSubTermOnHeap1(list(_,_,_),         Adr, [build_list(Adr)|   Is], Is, _).
putSubTermOnHeap1(functor(_,_,_, _),Adr, [set_struct(Adr)| Is], Is, _).

putSubTermOnHeap2(var(_, _, _),  Is,  Is,  0, _).
putSubTermOnHeap2(atom(_, _),    Is,  Is,  0, _).
putSubTermOnHeap2(char(_, _),    Is,  Is,  0, _).
putSubTermOnHeap2(integer(Int, C), Is1,  Is2,  Size, _) :-
	(bigint(Int) ->
	    Is1 = [bld_int(Int)|Is2],
	    Size = 2,
	    compiler_error("putSubTermOnHeap2 big int",C),
	    fail
	;
	    Is1 = Is2, Size = 0
	).
putSubTermOnHeap2(float(Float, _),Is1,Is2, 3, _) :-  % FLOAT
	Is1 = [bld_float(Float)|Is2].
putSubTermOnHeap2(string(S, _),Is1,Is2, L1, _) :-
	string_length(S,L),
	Is1 = [bld_string(L,S)|Is2],
	string_on_heap(L,L1).
putSubTermOnHeap2(list(X,Y,_),      Is1, Is2, L, RI1) :- 
	putSubTermsOnHeap([X,Y], 2, Is1, Is2, SubL, RI1),
	L is SubL + 2.
putSubTermOnHeap2(functor(Nr, Arity, STs, _), Is1, Is2, L, RI1) :-
	Is1 = [set_functor(Nr)|Is3],
	putSubTermsOnHeap(STs, Arity, Is3, Is2, SubL, RI1),
	L is SubL + Arity + 1.


compileTest(Test, Is1, Is2, RI1, Label) :-
	Test = test(Name, Args, _),
	compileTest(Name, Args, Is1, Is2, RI1, Label).
compileTest(Test, Is1, Is2, RI1, Label) :-
	Test = numericaltest(Name, Arg1, Arg2, _),
	compileNumericalTest(Name, Arg1, Arg2, Is1, Is2, RI1, Label).
compileTest(Test, Is1, Is2, RI1, Label):-
	Test = =(Arg1,Arg2,_),
	compileAssignmentTest(Arg1, Arg2, Is1, Is2, RI1, Label).

compileAssignmentTest(Arg, Val, Is1, Is2, RI1, Label):-
	Val = var(_, _,_), !,
	compileAssignmentTest(Val, Arg, Is1, Is2, RI1, Label).
compileAssignmentTest(Arg, Val, Is1, Is2, RI1, Label):-
	Arg = var(_, Type,_),
	(
	  Type = temp(_, _, Var) ->
	  ( nonvar(Var) ->
	      findReg(RI1, Type, X),
	      tempOccurred(Type, RI1),
	      ( Val = integer(I,_) ->
		  Is1 = [get_int_test(X, I, Label)| Is2]
	      ;
		  Val = atom(A,_),
		  Is1 = [get_atom_test(X, A, Label)| Is2]
	      )
	  ;
	      
	      /* always succeeding test here */
	      compileEquality(Arg, Val,  Is1, Is2, RI1)
	  )
	;
	  Type = perm(X, _, Var),
	  (nonvar(Var) ->
	      (findRegPermCheck(RI1,  Type, [Reg|_]) ->
		  (Val = integer(I,_) ->
		      Is1 = [get_int_test(Reg, I, Label)| Is2]
		  ;
		      Val = atom(A,_),
		      Is1 = [get_atom_test(Reg, A, Label)| Is2]
		  )
	      
	      ;
		  (Val = integer(I,_) ->
		      Is1 = [get_intv_test(X, I, Label)| Is2]
		  ;
		      Val = atom(A,_),
		      Is1 = [get_atomv_test(X, A, Label)| Is2]
		  )
	      ),
	      permOccurred(Type, RI1)
	  ;
	      /* always succeeding test here */
	      compileEquality(Arg, Val,  Is1, Is2, RI1)
	  )
	).


compileTest("==",[A1,atom(Atom,_)],Is1, Is2, RI1, Label) :- !,
	fromArgumentsToHeapI([A1],[R1], Is1, Is3, RI1),
	Is3 = [I|Is2],
	I = test_term_equal_atom(R1, Atom, Label).
compileTest("==",[A1,integer(Int,_)],Is1, Is2, RI1, Label) :- !,
	fromArgumentsToHeapI([A1],[R1], Is1, Is3, RI1),
	Is3 = [I|Is2],
	I = test_term_equal_integer(R1, Int, Label).
compileTest(Name, Args, Is1, Is2, RI1, Label) :-
	fromArgumentsToHeapI(Args, Regs, Is1, Is3, RI1),
	Is3 = [I|Is2],
	makeUpTermComparison(Name, Regs, I, Label).

compileNumericalTest(Name, XIn, YIn, Is1, Is2, RI1, Label):-
	Is4 = [Comparison|Is2],
	(evaluateExpression(XIn, X) -> true; X = XIn),
	(evaluateExpression(YIn, Y) -> true; Y = YIn),
	( makeUpComparisonS1(Name, X, Y, Comparison, Label, CReg2) ->
	    /* argument 1 has been specialised */
	    expressionToRegister(Y, CReg2, Is1, Is4, RI1, []),
	    decideOnRegister(CReg2, RI1, [])
	;
	    ( makeUpComparisonS2(Name, X, Y, Comparison, Label, CReg1) ->
		expressionToRegister(X, CReg1, Is1, Is4, RI1, []),
		decideOnRegister(CReg1, RI1, [])
	    ;
		/* general case*/
		/* hier yvars toelaten in sommige tests ? */
		(
		  make_spec_diffpp(Name,Xnr,Ynr,Label,Is1, Is2),
		  is_pval(XIn, Xnr, RI1), is_pval(YIn, Ynr, RI1) ->
		  true
		;
		  make_spec_difftp(Name,CReg1,Ynr,Label,Is1, Is2),
		  is_tval(XIn,_), is_pval(YIn, Ynr, RI1) ->
		  expressionToRegister(X, CReg1, Is1, Is3, RI1, []),
		  decideOnRegister(CReg1, RI1, []),
		  Used = [CReg1]
		;
		  expressionToRegister(X, CReg1, Is1, Is3, RI1, []),
		  decideOnRegister(CReg1, RI1, []),
		  Used = [CReg1],
		  expressionToRegister(Y, CReg2, Is3, Is4, RI1, Used),
		  decideOnRegister(CReg2, RI1, Used),
		  makeUpComparison(Name, CReg1, CReg2, Comparison, Label)
		)
	    )
	).

is_tval(Var,V) :-
	Var = var(_,temp(_,_,V),_),
	nonvar(V).

is_pval(Var,Nr,RI) :-
	Var = var(_,perm(Nr,_,V),_),
	nonvar(V),
	arg(4,RI,Perms),
	getperminfo(Perms,Nr,Info),
	Info = i(_,Assigned,_),
	Assigned \== [].

getperminfo([A|R],Nr,Info) :-
	(A = i(perm(Nr1,_,_),_,_), Nr == Nr1 ->
	    Info = A
	;
	    getperminfo(R,Nr,Info)
	).

make_spec_diffpp("=\\=",A,B,L,[test_inequalpp(A,B,L)|R],R).
make_spec_diffpp("=<",A,B,L,[test_smaller_or_equalpp(A,B,L)|R],R).

make_spec_difftp("=<",A,B,L,[test_smaller_or_equaltp(A,B,L)|R],R).

expressionToRegister(Term, R, Is1, Is2, RI1, _) :-
	(Term = float(_, _); Term = integer(_, _) ; Term = string(_, _) ; Term = char(_, _)), !,
	putTermInReg(Term, R, Is1, Is2, RI1). % we should avoid this
expressionToRegister(Expression, R, Is1, Is2, RI1, Used) :-
	computeExpression(Expression, R, Is1, Is2, RI1, Used, _).

makeUpTermComparison("@<", [R1, R2], test_term_smaller(R1, R2, L), L).
makeUpTermComparison("@=<", [R1, R2], test_term_smaller_or_equal(R1, R2, L), L).
makeUpTermComparison("@>", [R1, R2], test_term_smaller(R2, R1, L), L).
makeUpTermComparison("@>=", [R1, R2], test_term_smaller_or_equal(R2, R1, L), L).
makeUpTermComparison("\\==", [R1, R2], test_term_not_equal(R1, R2, L), L).
makeUpTermComparison("\\=", [R1, R2], test_term_not_unify(R1, R2, L), L).
makeUpTermComparison("==", [R1, R2], test_term_equal(R1, R2, L), L).
makeUpTermComparison("===", [R1, R2], test_toplevel_equal(R1, R2, L), L).
makeUpTermComparison("<<<", [R1, R2], test_toplevel_smaller(R1, R2, L), L).
makeUpTermComparison("var", [R1], I, L) :-
	(L == -1 -> I = builtin_var_1(R1); I = test_var(R1, L)).
makeUpTermComparison("attvar", [R1], I, L) :-
	(L == -1 -> I = builtin_attvar_1(R1); I = test_attvar(R1, L)).
makeUpTermComparison("atom", [R1], I, L) :-
	(L == -1 -> I = builtin_atom_1(R1); I = test_atom(R1, L)).
makeUpTermComparison("atomic", [R1], I, L) :-
	(L == -1 -> I = builtin_atomic_1(R1); I = test_atomic(R1, L)).
makeUpTermComparison("number", [R1], I, L) :-
	(L == -1 -> I = builtin_number_1(R1); I = test_number(R1, L)).
makeUpTermComparison("integer", [R1], I, L) :-
	(L == -1 -> I = builtin_integer_1(R1); I = test_integer(R1, L)).
makeUpTermComparison("compound", [R1], I, L) :-
	(L == -1 -> I = builtin_compound_1(R1); I = test_compound(R1, L)).
makeUpTermComparison("nonvar", [R1], I, L) :-
	(L == -1 -> I = builtin_nonvar_1(R1); I = test_nonvar(R1, L)).
makeUpTermComparison("callable", [R1], I, L) :-
	(L == -1 -> I = builtin_callable_1(R1); I = test_callable(R1, L)).
makeUpTermComparison("points_to_callcc", [R1], I, L) :-
	(L == -1 -> I = builtin_points_to_callcc_1(R1); I = test_points_to_callcc(R1, L)).

	
makeUpComparison("<", R1, R2, test_smaller(R1, R2, L), L).
makeUpComparison("=<", R1, R2, test_smaller_or_equal(R1, R2, L), L).
makeUpComparison(">", R1, R2, test_smaller(R2, R1, L), L).
makeUpComparison(">=", R1, R2, test_smaller_or_equal(R2, R1, L), L).
makeUpComparison("=\\=", R1, R2, test_inequal(R1, R2, L), L).
makeUpComparison("=:=", R1, R2, test_equal(R1, R2, L), L).

makeUpComparisonS1(">", float(F,_), _, test_smaller_float(R, F, L), L, R) :- ! .
makeUpComparisonS1(">", integer(F,_), _, test_smaller_int(R, F, L), L, R) :- ! .
makeUpComparisonS1(">=",float(F,_),_,test_smaller_or_equal_float(R,F,L), L, R) :- ! .
makeUpComparisonS1(">=",integer(F,_),_,test_smaller_or_equal_int(R,F,L), L, R) :- ! .
makeUpComparisonS1("<", float(F,_), _, test_larger_float(R, F, L), L, R) :- ! .
makeUpComparisonS1("<", integer(F,_), _, test_larger_int(R, F, L), L, R) :- ! .
makeUpComparisonS1("=<",float(F,_),_,test_larger_or_equal_float(R,F,L), L, R) :- ! .
makeUpComparisonS1("=<",integer(F,_),_,test_larger_or_equal_int(R,F,L), L, R) :- ! .
makeUpComparisonS1("=:=",float(F,_),_,test_equal_float(R,F,L), L, R) :- ! .
makeUpComparisonS1("=:=",integer(F,_),_,test_equal_int(R,F,L), L, R) :- ! .
makeUpComparisonS1("=\\=",integer(F,_),_,test_inequal_int(R,F,L), L, R) :- ! .
makeUpComparisonS1("=\\=",float(F,_),_,test_inequal_float(R,F,L), L, R) :- ! .


makeUpComparisonS2("<", _, float(F,_), test_smaller_float(R, F, L), L, R) :- ! .
makeUpComparisonS2("<", _, integer(F,_), test_smaller_int(R, F, L), L, R) :- ! .
makeUpComparisonS2("=<",_,float(F,_),test_smaller_or_equal_float(R,F,L), L, R) :- ! .
makeUpComparisonS2("=<",_,integer(F,_),test_smaller_or_equal_int(R,F,L), L, R) :- ! .
makeUpComparisonS2(">", _, float(F,_), test_larger_float(R, F, L), L, R) :- ! .
makeUpComparisonS2(">", _, integer(F,_), test_larger_int(R, F, L), L, R) :- ! .
makeUpComparisonS2(">=",_,float(F,_),test_larger_or_equal_float(R,F,L), L, R) :- ! .
makeUpComparisonS2(">=",_,integer(F,_),test_larger_or_equal_int(R,F,L), L, R) :- ! .
makeUpComparisonS2("=:=",_,integer(F,_),test_equal_int(R,F,L), L, R) :- ! .
makeUpComparisonS2("=:=",_,float(F,_),test_equal_float(R,F,L), L, R) :- ! .
makeUpComparisonS2("=\\=",_,integer(F,_),test_inequal_int(R,F,L), L, R) :- ! .
makeUpComparisonS2("=\\=",_,float(F,_),test_inequal_float(R,F,L), L, R) :- ! .


compileIsExpression(X, Expression, Is1, Is2, RI1) :-
	(evaluateExpression(Expression, Value) ->
	    compileEquality(X, Value, Is1, Is2, RI1)
	;
	    compileIsExpression1(X, Expression, Is1, Is2, RI1)
	).

compileIsExpression1(integer(I, _), Expression, Is1, Is2, RI1) :-
	Is3 = [get_int(Reg, I)|Is2],
	computeExpressionFirst(Expression, Reg, Is1, Is3, RI1, _),
	stealSaveRegister(Reg, RI1, []).
compileIsExpression1(float(F, _), Expression, Is1, Is2, RI1) :-
	Is3 = [get_float(Reg, F)|Is2],
	computeExpressionFirst(Expression, Reg, Is1, Is3, RI1, _),
	stealSaveRegister(Reg, RI1, []).
compileIsExpression1(var(_, Type, _), Expression, Is1, Is2, RI1) :-
	compileIsExpressionVar(Type, Expression, Is1, Is2, RI1).
compileIsExpression1(atom(_, C), _, _, _, _) :-
	compiler_error("Invalid left argument in arithmetic expression",C), fail.
compileIsExpression1(functor(_, _, _, C), _, _, _, _) :-
	compiler_error("Invalid left argument in arithmetic expression",C), fail.



compileIsExpressionVar(void, Expression, Is1, Is2, RI1) :-
	Is2 = Is1,
	compiler_warning("Computing expression to void variable",Expression),
           /* make sure all registers are reclaimed!: */
        checkDeadTerm(Expression, RI1).

compileIsExpressionVar(TVar, Expression, Is1, Is2, RI1) :-
	TVar = temp(Dest, _, Var),
	computeExpressionFirst(Expression, Reg, Is3, Is4, RI1, Unsafe),
	(var(Var) ->
	    addUnsafeRegs(RI1, Unsafe),
	    findFreeRegAlways(TVar, Dest, Reg, RI1, Is1, Is3),
	    noMoreUnsafeRegs(RI1),
	    Is4 = Is2
	;
	    Is1 = Is3,
	    findReg(RI1, TVar, R),
	    stealSaveRegister(Reg, RI1, []),
	    Is4 = [gettval(R, Reg)|Is2],
	    tempOccurred(TVar, RI1)
	).
compileIsExpressionVar(PVar, Expression, Is1, Is2, RI1) :-
	PVar = perm(X, _, Var),
	computeExpressionFirst(Expression, Reg, Is1, Is3, RI1, _),
	findSaveRegister(Reg, RI1),
	(var(Var) ->
	    newPermVar(PVar, [], [Reg], RI1), Is3 = Is2
%% 	    newPermVar(PVar, [X], [Reg], RI1), % [X] was [] ???
%% 	    Is3 = [getpvar(X, Reg)|Is2], % ??? Is3 = Is2
%% 	    addFreeRI(Reg, RI1) % ???
	;
	    ( findRegPermCheck(RI1, PVar, Isin) ->
		( Isin = [R|_] ->
		    Is3 = [gettval(R, Reg)|Is2],
		    addFreeRI(Reg, RI1)
		;
		    newPermVarDisj(PVar, [], [Reg], RI1), Is3 = Is2
		)
	    ;
		Is3 = [getpval(X, Reg)|Is2],
		addFreeRI(Reg, RI1)
	    ),
	    permOccurred(PVar, RI1)
	).

doComp(A1, A2, R, Op) :-
	( A1 = integer(X, Context) -> true; A1 = float(X, Context)),
	( A2 = integer(Y, _) -> true; A2 = float(Y, _)),
	string_to_atom(Op,AtOp),
	functor(Expression, AtOp, 2),
	arg(1, Expression, X),
	arg(2, Expression, Y),
	metacalled_is(Expression,Temp),  %%% XXX for hProlog
	(integer(Temp) -> R = integer(Temp, Context); R= float(Temp, Context)).

doComp(A1, R, Op) :-
	( A1 = integer(X, Context) -> true; A1 = float(X, Context)),
	string_to_atom(Op,AtOp),
	functor(Expression, AtOp, 1),
	arg(1, Expression, X),
	metacalled_is(Expression,Temp),  %%% XXX for hProlog
	(integer(Temp) -> R = integer(Temp, Context); R= float(Temp, Context)).



evaluateExpression(A,B) :-
	evaluateExpression1(A,B).
	
evaluateExpression1(E, E) :- E = integer(_, _).
evaluateExpression1(E, E) :- E = float(_, _).
% evaluateExpression1(_,_) :- !, fail. was used for avoiding bigints
evaluateExpression1(bin_op(Name, A, B), V) :-
	evaluateExpression1(A, AC),
	evaluateExpression1(B, BC),
	doComp(AC, BC, V, Name).
evaluateExpression1(uni_op(Name, A), V) :-
	evaluateExpression1(A, AC),
	doComp(AC, V, Name).

%% regConstInstruction(_, _, _, integer(I, _), _,_) :-
%% 	bigint(I),
%% 	!,
%% 	fail.
regConstInstruction("+", Reg, R, integer(I, _), Is1, Is2) :- !,
	Is1 = [add_integer(Reg, R, I)|Is2].
regConstInstruction("+", Reg, R, float(F, _), Is1, Is2) :-
	Is1 = [add_float(Reg, R, F)|Is2].
regConstInstruction("-", Reg, R, integer(I, _), Is1, Is2) :- !,
	I1 is 0-I,
	Is1 = [add_integer(Reg, R, I1)|Is2].
regConstInstruction("-", Reg, R, float(F, _), Is1, Is2) :- F1 is 0.0-F,
	Is1 = [add_float(Reg, R, F1)|Is2].
regConstInstruction("//", Reg, R, integer(I, _), Is1, Is2) :- 
	Is1 = [idiv_integer(Reg, R, I)|Is2].
regConstInstruction("mod", Reg, R, integer(I, _), Is1, Is2) :- 
	Is1 = [modulo_integer(Reg, R, I)|Is2].

constRegInstruction("+", Reg, AC, R, Is1, Is2) :-
	regConstInstruction("+", Reg, R, AC, Is1, Is2).
constRegInstruction("-", Reg, integer(I, _), R, Is1, Is2) :-
	Is1 = [integer_sub(Reg, I, R)|Is2].
constRegInstruction("-", Reg, float(F, _), R, Is1, Is2) :-
	Is1 = [float_sub(Reg, F, R)|Is2].
constRegInstruction("*", Reg, AC, R, Is1, Is2) :-
	regConstInstruction("*", Reg, R, AC, Is1, Is2).

expressionCommandRegReg("+",Reg,R1,R2,Is1,Is2) :-  Is1=[add(Reg,R1,R2)      |Is2].
expressionCommandRegReg("-",Reg,R1,R2,Is1,Is2) :-  Is1=[subtr(Reg,R1,R2)    |Is2].
expressionCommandRegReg("*",Reg,R1,R2,Is1,Is2) :-  Is1=[multiply(Reg,R1,R2) |Is2].
expressionCommandRegReg("/",Reg,R1,R2,Is1,Is2) :-  Is1=[division(Reg,R1,R2) |Is2].
expressionCommandRegReg("//",Reg,R1,R2,Is1,Is2) :- Is1=[idivision(Reg,R1,R2)|Is2].
expressionCommandRegReg(">>",Reg,R1,R2,Is1,Is2) :- Is1=[logshiftr(Reg,R1,R2)|Is2].
expressionCommandRegReg("<<",Reg,R1,R2,Is1,Is2) :- Is1=[logshiftl(Reg,R1,R2)|Is2].
expressionCommandRegReg("min",Reg,R1,R2,Is1,Is2) :-Is1=[minimum(Reg,R1,R2)  |Is2].
expressionCommandRegReg("max",Reg,R1,R2,Is1,Is2) :-Is1=[maximum(Reg,R1,R2)  |Is2].
expressionCommandRegReg("mod",Reg,R1,R2,Is1,Is2) :-Is1=[c_modulo(Reg,R1,R2) |Is2].
expressionCommandRegReg("/\\",Reg,R1,R2,Is1,Is2) :-Is1=[bit_and(Reg,R1,R2) |Is2].
expressionCommandRegReg("\\/",Reg,R1,R2,Is1,Is2) :-Is1=[bit_or(Reg,R1,R2) |Is2].
expressionCommandRegReg("xor",Reg,R1,R2,Is1,Is2) :-Is1=[bit_xor(Reg,R1,R2) |Is2].
expressionCommandRegReg("**",Reg,R1,R2,Is1,Is2) :-Is1=[power(Reg,R1,R2) |Is2].
expressionCommandRegReg("^",Reg,R1,R2,Is1,Is2) :-Is1=[power(Reg,R1,R2) |Is2].

expressionCommandReg("round",Reg,R1,Is1,Is2) :-Is1=[round2int(Reg,R1) |Is2].
expressionCommandReg("floor",Reg,R1,Is1,Is2) :-Is1=[float2intfloor(Reg,R1) |Is2].
expressionCommandReg("ceil",Reg,R1,Is1,Is2) :-Is1=[float2intceil(Reg,R1) |Is2].
expressionCommandReg("trunc",Reg,R1,Is1,Is2) :-Is1=[float2inttrunc(Reg,R1) |Is2].
expressionCommandReg("msb",Reg,R1,Is1,Is2) :-Is1=[mostsigbit(Reg,R1) |Is2].
expressionCommandReg("~",Reg,R1,Is1,Is2) :-Is1=[negate(Reg,R1) |Is2].
expressionCommandReg("\\",Reg,R1,Is1,Is2) :-Is1=[negate(Reg,R1) |Is2].
expressionCommandReg("-",Reg,R1,Is1,Is2) :-Is1=[min1(Reg,R1) |Is2].
expressionCommandReg("sqrt",Reg,R1,Is1,Is2) :-Is1=[mysqrt(Reg,R1) |Is2].
expressionCommandReg("atan",Reg,R1,Is1,Is2) :-Is1=[myatan(Reg,R1) |Is2].
expressionCommandReg("sin",Reg,R1,Is1,Is2) :-Is1=[mysin(Reg,R1) |Is2].
expressionCommandReg("cos",Reg,R1,Is1,Is2) :-Is1=[mycos(Reg,R1) |Is2].
expressionCommandReg("abs",Reg,R1,Is1,Is2) :-Is1=[myabs(Reg,R1) |Is2].
expressionCommandReg("log",Reg,R1,Is1,Is2) :-Is1=[mylog(Reg,R1) |Is2].
expressionCommandReg("exp",Reg,R1,Is1,Is2) :-Is1=[myexp(Reg,R1) |Is2].
expressionCommandReg("sign",Reg,R1,Is1,Is2) :-Is1=[mysign(Reg,R1) |Is2].
expressionCommandReg("max",Reg,R1,Is1,Is2) :-Is1=[listmax1(Reg,R1) |Is2].
expressionCommandReg("min",Reg,R1,Is1,Is2) :-Is1=[listmin1(Reg,R1) |Is2].
expressionCommandReg("len",Reg,R1,Is1,Is2) :-Is1=[listlen1(Reg,R1) |Is2].
expressionCommandReg("sum",Reg,R1,Is1,Is2) :-Is1=[listsum1(Reg,R1) |Is2].

expressionCommandRegConstant(Op, Reg, R, BC, Is1, Is2, RI1, Used) :-
	(regConstInstruction(Op, Reg, R, BC, Is1, Is2) ->
	    true
	;
	    stealSaveRegister(R2, RI1, Used),
	    putTermInReg(BC, R2, Is1, Is3, RI1),
	    expressionCommandRegReg(Op, Reg, R, R2, Is3, Is2)
	).

expressionCommandConstantReg(Op, Reg, AC, R, Is1, Is2, RI1, Used) :-
	(constRegInstruction(Op, Reg, AC, R, Is1, Is2) ->
	    true
	;
	    stealSaveRegister(R2, RI1, Used),
	    putTermInReg(AC, R2, Is1, Is3, RI1),
	    expressionCommandRegReg(Op, Reg, R2, R, Is3, Is2)
	).

computeExpressionFirst(Expression, Reg, Is1, Is2, RI1, Unsafe) :-
	computeExpression(Expression, R, Is1, Is3, RI1, [], Unsafe0),
	(var(R) -> Is3 = Is2, Reg = R, Unsafe = Unsafe0
	;
	    Unsafe = [R|Unsafe0],
	    Is3 = [add_integer(Reg, R, 0)|Is2]
	    % Is3 = [movreg(R,Reg)|Is2]
	).


computeExpression(var(_,Var, _), Reg, Is1, Is2, RI1, Used, Unsafe) :- !,
	computeExpressionVar(Var, Reg, Is1, Is2, RI1, Used, Unsafe).


computeExpression(bin_op(Name, A, B), Reg, Is1, Is2, RI1, Used, Unsafe) :- !,
	(evaluateExpression(B, BC) ->
	    Unsafe = [R],
	    computeExpression(A, R, Is1, Is3, RI1, Used, _),
	    decideOnRegister(R, RI1, Used),
	    expressionCommandRegConstant(Name,Reg,R,BC,Is3,Is2,RI1,[R|Used])
	;
	    ( evaluateExpression(A, AC) ->
		Unsafe = [R],
		computeExpression(B, R, Is1, Is3, RI1, Used, _),
		decideOnRegister(R, RI1, Used),
		expressionCommandConstantReg(Name,Reg,AC,R,Is3,Is2,RI1,
					     [R|Used])
	    ;
		Unsafe = [R1,R2],
		computeExpression(B, R2, Is1, Is3, RI1, Used, _),
		decideOnRegister(R2, RI1, Used),
		NewUsed = [R2|Used],
		computeExpression(A, R1, Is3, Is4, RI1, NewUsed, _),
		decideOnRegister(R1, RI1, NewUsed),
		expressionCommandRegReg(Name, Reg, R1, R2, Is4, Is2)
	    )
	).

computeExpression(uni_op(Name, A), Reg, Is1, Is2, RI1, Used, Unsafe) :-
	!,
	(Name == "+" ->
	    computeExpression(A, Reg, Is1, Is2, RI1, Used, Unsafe)
	;
	    Unsafe = [R],
	    computeExpression(A, R, Is1, Is3, RI1, Used, _),
	    decideOnRegister(R, RI1, Used),
	    expressionCommandReg(Name,Reg,R,Is3,Is2)
	).

computeExpression(Other, Reg, Is1, Is2, RI1, Used, []) :-
	Other = functor(_, _, _, _),
	stealSaveRegister(Reg, RI1, Used),
          /* For perfect code we do not have to pass Used.
	     but nasty things could happen if the user
	     has written a free temporary variable in an expression.
             We could check that! XXX */
	putTermInReg(Other, Reg, Is1, Is2, RI1).

computeExpression(Other, Reg, Is1, Is2, RI1, Used, []) :-
	Other = atom(_, _),
	    /* This is probably a bug in the users program,
	       is must generate an exception at runtime */
	stealSaveRegister(Reg, RI1, Used),
	putTermInReg(Other, Reg, Is1, Is2, RI1).
	

decideOnRegister(R2, RI1, Used) :-
	(var(R2) -> stealSaveRegister(R2, RI1, Used); true).

computeExpressionVar(void, 0, Is1, Is2, _, _, []) :-
        Is1 = [error_message(0)|Is2],
	compiler_error("Void variable in expression",nocontext).
computeExpressionVar(TVar, Reg, Is1, Is1, RI1, _, [Reg]) :-
	TVar = temp(_, _, Var),
	(var(Var) ->
	    compiler_error("Free variable in expresssion 1",nocontext),
	    fail  %% ???
	;
	    findReg(RI1, TVar, Reg),
	    tempOccurred(TVar, RI1)
	).
computeExpressionVar(PVar, Reg, Is1, Is2, RI1, Used, [Reg]) :- 
	PVar = perm(X, _, Var),
	(var(Var) ->
	    compiler_error("Free variable in expresssion 2",nocontext),
	    fail%% ???
	;
	    ( findRegPermCheck(RI1, PVar, Isin) ->
		(Isin = [Reg|_] ->
		    Is1 = Is2
		;
		    compiler_error("Free variable in expresssion 3",nocontext),
		    fail%% ???
		)
	    ;
		Is1 = [putpval(X, Reg)| Is2],
		stealSaveRegister(Reg, RI1, Used)
	    ),
	    permOccurred(PVar, RI1)
	).



compileEquality(Term1, Term2, Is1, Is2, RI1) :-
	( identicalterms(Term1,Term2) ->
	    Is1 = Is2,
	    checkDeadTerm(Term1, RI1),
	    checkDeadTerm(Term2, RI1)
	;
	    ( Term1 = var(_, Var, _) ->
		compileEqualityVar(Var, Term1, Term2, Is1, Is2, RI1)
	    ;   
		( Term2 = var(_, Var, _) ->
		    compileEqualityVar(Var, Term2, Term1, Is1, Is2, RI1)
		;	
		    compileEqualityNonVar(Term1, Term2, Is1, Is2, RI1)
		)
	    )
	).


checkDeadTerm(Term, RI) :-  Term = var(_, Type, _),
	checkDeadTermVar(Type, Term, RI).
checkDeadTerm(atom(_, _), _).
checkDeadTerm(float(_, _), _).
checkDeadTerm(string(_, _), _).
checkDeadTerm(char(_, _), _).
checkDeadTerm(integer(_, _), _).
checkDeadTerm(list(X,Y,_), RI) :-
	checkDeadTerm(X, RI),
	checkDeadTerm(Y, RI).
checkDeadTerm(functor(_, _, Args, _), RI) :-  checkDeadTerml(Args, RI).
checkDeadTerm(bin_op(_, Arg1, Arg2), RI) :-
	checkDeadTerm(Arg1, RI),
	checkDeadTerm(Arg2, RI).


checkDeadTerml([],_).
checkDeadTerml([A|As],RI) :- checkDeadTerm(A,RI), checkDeadTerml(As,RI).

checkDeadTermVar(void,_,  _).
checkDeadTermVar(Var, Term, RI) :-  Var = temp(_, _, _),
	checkDeadTermTemp(Var, Term, RI).
checkDeadTermVar(Var, Term, RI) :-  Var = perm(_, _, _),
	checkDeadTermPerm(Var, Term, RI).



compileEqualityVar(void, _, OtherArg, Is1, Is2, RI1) :- !, 
	Is1 = Is2,
        checkDeadTerm(OtherArg, RI1). 

compileEqualityVar(Temp, Term1, Term2, Is1, Is2, RI1) :-
	Temp = temp(_, _, _), !,
	compileEqualityTemp(Term2, Temp, Term1, Is1, Is2, RI1).
compileEqualityVar(Perm, Term1, Term2, Is1, Is2, RI1) :-
	Perm = perm(_, _, _),
	compileEqualityPerm(Term2, Perm, Term1, Is1, Is2, RI1).

compileEqualityTemp(Term2, Temp1, Term1, Is1, Is2, RI1) :- 
        Term2 = var(_, Var, _), !,
	compileEqualityTempVar(Var, Term2, Temp1, Term1, Is1, Is2, RI1).
compileEqualityTemp(Term, Temp, _, Is1, Is2, RI1) :-
	Temp = temp(Dest, _, Var),
	( var(Var) ->
	    findFreeRegAlways(Temp, Dest, Reg, RI1, Is1, Is3),
	    putTermInReg(Term, Reg, Is3, Is2, RI1)
	;   
	    findReg(RI1, Temp, X),
	    tempOccurred(Temp, RI1),
	    unifyTermWithReg(Term, X, Is1, Is2, RI1)
	).

compileEqualityPerm(Term2, Perm, Term1, Is1, Is2, RI1) :- 
	Term2 = var(_, Var, _), !,
	compileEqualityPermVar(Var, Perm, Term1, Is1, Is2, RI1).
compileEqualityPerm(Term, Perm, _, Is1, Is2, RI1) :-
	Perm = perm(X, _, Var),
	( var(Var) ->		
	    newPermVar(Perm, [X], [], RI1),
	    putTermInVar(Term, X, Is1, Is2, RI1)
	;   
	    ( findRegPermCheck(RI1, Perm, Isin) ->
		(Isin = [Reg|_] ->
		    unifyTermWithReg(Term, Reg, Is1, Is2, RI1)
		;
		    newPermVarDisj(Perm, [X], [], RI1),
		    putTermInVar(Term, X, Is1, Is2, RI1)
		)
	    ;
		unifyTermWithVar(Term, X, Is1, Is2, RI1)
	    ),
	    permOccurred(Perm, RI1)
	).


compileEqualityTempVar(void, _, TVar, Term, Is1, Is2, RI) :- !,
	Is1 = Is2,
	checkDeadTermTemp(TVar, Term, RI).
compileEqualityTempVar(TVar1, _, TVar2, _, Is1, Is2, RI1) :-
	TVar1 = temp(_, _, Var1), !,
	TVar2 = temp(_, _, Var2),
	(var(Var1) -> Status1 = new; Status1 = notnew),
	(var(Var2) -> Status2 = new; Status2 = notnew),
        compileEqualityTempVarSS(Status1,Status2,TVar1, TVar2, Is1, Is2, RI1).

compileEqualityTempVar(PVar, Term2, TVar, _, Is1, Is2, RI1) :-
	PVar = perm(_, _, _),
	compileEqualityPermVar(TVar, PVar, Term2, Is1, Is2, RI1). 
			
compileEqualityPermVar(void, PVar, Term1, Is1, Is2, RI1) :- !,
	Is1 = Is2, 
	checkDeadTermPerm(PVar, Term1, RI1).
compileEqualityPermVar(TVar1, PVar2, _, Is1, Is2, RI1) :- 
	TVar1 = temp(Dest1, _, Var1), !,
	PVar2 = perm(X2, _, Var2), 
	( var(Var1) -> 
	    ( var(Var2) ->
		findFreeRegAlways(TVar1, Dest1, Reg, RI1, Is1, Is3),
		newPermVar(PVar2, [X2], [Reg], RI1),
		Is3 = [putpvar(X2, Reg)| Is2]
	    ;	
		( findRegPermCheck(RI1, PVar2, Isin) ->
		    ( Isin = [R2|_] ->
			firstUseTemp(TVar1),
			addToRI(R2, TVar1, RI1), 
			Is1 = Is2
		    ;	
			findFreeRegAlways(TVar1,Dest1,Reg, RI1, Is1, Is3),
			newPermVarDisj(PVar2, [X2], [Reg], RI1),
			Is3 = [putpvar(X2, Reg)| Is2]
		    )
		;   
		    findFreeRegAlways(TVar1, Dest1, Reg, RI1, Is1, Is3),
		    Is3 = [putpval(X2, Reg)| Is2]
		)
	    )
	;   
	    findReg(RI1, TVar1, Reg),
	    ( var(Var2) -> 
		newPermVar(PVar2, [], [Reg], RI1),
		Is1 = Is2
	    ;	
		/* two non_var arguments */
		( findRegPermCheck(RI1, PVar2, Isin2) ->
		    ( Isin2 = [Reg2|_] ->
			Is1 = [gettval(Reg2, Reg)| Is2]
		    ;
			/* turns out to be free anyway */
			newPermVarDisj(PVar2, [], [Reg], RI1),
			Is1 = Is2
		    )
		;   
		    Is1 = [getpval(X2, Reg)| Is2]
		),
		permOccurred(PVar2, RI1)
	    ),
	    tempOccurred(TVar1, RI1)
	). 
compileEqualityPermVar(PVar1, PVar2, _, Is1, Is2, RI1) :-
	PVar1 = perm(_, _, Var1),  
	PVar2 = perm(_, _, Var2),
	( var(Var1) -> How1 = new
	;
	    ( findRegPermCheck(RI1, PVar1, Isin1) ->
		( Isin1 = [R1|_] -> How1 = isin(R1)
		;
		    How1 =  notfound
		)
	    ;
		How1 = found
	    )
	),
	( var(Var2) -> How2 = new
	;
	    ( findRegPermCheck(RI1, PVar2, Isin2) ->
		( Isin2 = [R2|_] -> How2 = isin(R2)
		;
		    How2 =  notfound
		)
	    ;
		How2 = found
	    )
	),
	compileEqualityPermVarSS(How1, How2, PVar1, PVar2, Is1, Is2, RI1),
	( How1 = new -> true
	;
	    permOccurred(PVar1, RI1)
	),
	( How2 = new -> true
	;
	    permOccurred(PVar2, RI1)
	).
	

compileEqualityPermVarSS(new, new, PVar1, PVar2, Is1, Is2, RI1) :-
	arg(1, PVar1, X1),
	arg(1, PVar2, X2),
	Is1 = [bldpvar(X1), move_var(X1, X2)| Is2],
	newPermVar(PVar2, [X2], [], RI1),
	newPermVar(PVar1, [X1], [], RI1). % XXX move_var could be delayed
compileEqualityPermVarSS(new, notfound, PVar1, PVar2, Is1, Is2, RI1) :-
	arg(1, PVar1, X1),
	arg(1, PVar2, X2),
	Is1 = [bldpvar(X2), move_var(X2, X1)|Is2],
	newPermVarDisj(PVar2, [X2], [], RI1),
	newPermVar(PVar1, [X1], [], RI1).
compileEqualityPermVarSS(new, isin(R), PVar1, _, Is1, Is2, RI1) :-
	Is1 = Is2,
	newPermVar(PVar1, [], [R], RI1). % 2 perm vars in one register. Probs?
compileEqualityPermVarSS(new, found, PVar1, PVar2, Is1, Is2, RI1) :-
	arg(1, PVar1, X1),
	arg(1, PVar2, X2),
	newPermVar(PVar1, [X1], [], RI1),
	Is1 = [move_var(X2, X1)| Is2].  % XXX move_var could be delayed
compileEqualityPermVarSS(notfound, new, PVar1, PVar2, Is1, Is2, RI1) :-
	compileEqualityPermVarSS(new, notfound, PVar2, PVar1, Is1, Is2, RI1).
compileEqualityPermVarSS(notfound, notfound, PVar1, PVar2, Is1, Is2, RI1) :-
	arg(1, PVar1, X1),
	arg(1, PVar2, X2),
	Is1 = [bldpvar(X1), move_var(X1, X2)|Is2],
	newPermVarDisj(PVar2, [X2], [], RI1),
	newPermVarDisj(PVar1, [X1], [], RI1).
compileEqualityPermVarSS(notfound, isin(R), PVar1, _, Is1, Is2, RI1) :-
	Is1 = Is2,
	newPermVarDisj(PVar1, [], [R], RI1).
compileEqualityPermVarSS(notfound, found, PVar1, PVar2, Is1, Is2, RI1) :-
	arg(1, PVar1, X1),
	arg(1, PVar2, X2),
	newPermVarDisj(PVar1, [X2], [], RI1),
	Is1 = [move_var(X2,X1)|Is2].
compileEqualityPermVarSS(How1, new, PVar1, PVar2, Is1, Is2, RI1) :-
	How1 = isin(_),
	compileEqualityPermVarSS(new, How1, PVar2, PVar1, Is1, Is2, RI1).
compileEqualityPermVarSS(How1, notfound, PVar1, PVar2, Is1, Is2, RI1) :-
	How1 = isin(_),
	compileEqualityPermVarSS(notfound, How1, PVar2, PVar1, Is1, Is2, RI1).
compileEqualityPermVarSS(isin(R1), isin(R2), _, _, Is1, Is2, _) :-
	Is1 = [gettval(R1, R2)| Is2].
compileEqualityPermVarSS(isin(R1), found, _, PVar2, Is1, Is2, _) :-
	arg(1, PVar2, X2),
	Is1 = [getpval(X2, R1)| Is2].
compileEqualityPermVarSS(found, new, PVar1, PVar2, Is1, Is2, RI1) :-
	compileEqualityPermVarSS(new, found, PVar2, PVar1, Is1, Is2, RI1).
compileEqualityPermVarSS(found, notfound, PVar1, PVar2, Is1, Is2, RI1) :-
	compileEqualityPermVarSS(notfound, found, PVar2, PVar1, Is1, Is2, RI1).
compileEqualityPermVarSS(found, How2, PVar1, PVar2, Is1, Is2, RI1) :-
	How2 = isin(_),
	compileEqualityPermVarSS(How2, found, PVar2, PVar1, Is1, Is2, RI1).
compileEqualityPermVarSS(found, found, PVar1, PVar2, Is1, Is2, _) :-
	arg(1, PVar1, X1),
	arg(1, PVar2, X2),
	Is1 = [getpvalv(X1, X2)| Is2].

compileEqualityTempVarSS(new, new, TVar1, TVar2, Is1, Is2, RI1) :-
	Is3 = [bldtvar(Reg)| Is2],
	firstUseTemp(TVar1),
	arg(1, TVar1, Dest1),
	( findFreeReg(Dest1, Reg, RI1, [], Is1, Is3) ->
	    firstUseTemp(TVar2),
	    addToRI(Reg, TVar1, RI1),
	    addToRI(Reg, TVar2, RI1) /* XXX HELP, double use!! */
	;
	    arg(1, TVar2, Dest2),
	    findFreeRegAlways(TVar2, Dest2, Reg, RI1, Is1, Is3),
	    addToRI(Reg, TVar1, RI1)
	).
compileEqualityTempVarSS(new, notnew, TVar1, TVar2, Is1, Is2, RI1) :-
	Is1 = Is2,
	firstUseTemp(TVar1),
	findReg(RI1, TVar2, Reg),
	addToRI(Reg, TVar1, RI1), /* XXX HELP,double  */
	tempOccurred(TVar2, RI1).
compileEqualityTempVarSS(notnew, new, TVar1, TVar2, Is1, Is2, RI1) :-
	compileEqualityTempVarSS(new, notnew, TVar2, TVar1, Is1, Is2, RI1).

compileEqualityTempVarSS(notnew, notnew, TVar1, TVar2, Is1, Is2, RI1) :-
	findReg(RI1, TVar1, R1),
	findReg(RI1, TVar2, R2),
	(R1 = R2 ->
	    Is1 = Is2
	;
	    Is1 = [gettval(R1, R2)| Is2]
	),
	tempOccurred(TVar1, RI1),
	tempOccurred(TVar2, RI1).


simpleTerm(atom(_, _)). 
simpleTerm(float(_, _)).
simpleTerm(string(_, _)).
simpleTerm(char(_, _)).
simpleTerm(integer(_, _)).

compileEqualityNonVar(Term1, Term2, Is1, Is2, _) :-
	simpleTerm(Term1), !,
	( Term1 = Term2 ->
	    compiler_warning("Always succeeding unification",Term1),
	    Is1 = Is2
	;   
	    compiler_warning("Always failing unification",Term1),
	    Is1 = [fail|Is2]
	).

compileEqualityNonVar(list(X,Y,_), Term2, Is1, Is2, RI1) :-
	( Term2 = list(X2,Y2,_) ->
	    compileEquality(X, X2, Is1, Is3, RI1),
	    compileEquality(Y, Y2, Is3, Is2, RI1)
	;   
	    
            compiler_warning("Always failing unification",Term2),
	    Is1 = [fail|Is2]
	).
compileEqualityNonVar(functor(Nr, Arity, Args,_), Term2, Is1, Is2, RI1) :-
	( Term2 = functor(Nr, Arity, Args1,_) ->
	    compileEqualityNonVarArgs(Args, Args1, Is1, Is2, RI1)
	;   
            compiler_warning("Always failing unification",Term2),
	    Is1 = [fail|Is2]
	).

compileEqualityNonVarArgs([], [], Is, Is, _).
compileEqualityNonVarArgs([Arg1|Args1],[Arg2|Args2], Is1, Is2, RI1) :-
	compileEquality(Arg1, Arg2, Is1, Is3, RI1),
	compileEqualityNonVarArgs(Args1,Args2, Is3, Is2, RI1).
 

indexing(Clauses, Is1, Is2, Entrys, NewArity) :-
	Clauses = [Clause|Clauses1], 
	( Clauses1 = [] ->
	    Is1 = Is2
	;
	    arg(1, Clause, Head),
	    (   Head = functor(_, Arity, _, _),
		collectHeads(Clauses, Heads, Entrys),
		make_to_index(0, Arity, ToIndex),
		(hprolog -> ToIndex2 = [1] ; ToIndex2 = ToIndex),
		indexingArgs(ToIndex2, 3, Heads, NewArity, Is1, Is3, L1),
		Is3 = [label(L1)|Is2] -> true
	    ;  /* no indexing possible */
		Is1 = Is2
	    )
	).

indexingArgs([Index|ToIndex], MaxIndex, Heads, Arity, Is1, Is2, L1) :-
%	write('one try'(Index)), nl,
	indexingArgsTry(Index, Heads, Arity, Is1, Is3, L1, Todo), !,
	MaxIndex1 is MaxIndex -1,
	handleTodo(Todo, ToIndex, MaxIndex1, Arity, Is3, Is2).
indexingArgs([_|ToIndex], MaxIndex, Heads, Arity, Is1, Is2, L1) :-
	indexingArgs(ToIndex, MaxIndex, Heads, Arity, Is1, Is2, L1).

/* indexingArgTry/7 fails in case no indexing can be generated */
indexingArgsTry(Index, Heads, Arity, Is1, Is2, L1, Todo) :-
	findArguments(Index, Heads, Groups, 0, NrVars, 0, Len),
	NrVars < Len, /* Needed, no indexing when all arguments var! */
	( Len > 3 -> NrVars * 2 =< Len; true),  /*  heuristic, may be revise after switchonterm */
        (NrVars = 0 ->
	    VarList = [],
	    groupSortSimple(Groups, Len, NewGroups)
	;
	    groupSort(Groups, Len, NewGroups, VarGroup),
	    VarGroup = group(_, _, VarList, [])
	),
	me_length(NewGroups, 0, Count),
	(Count = 1, NrVars = 0, NewGroups = [G|_], arg(1, G, Type), (Type = l; Type = s) ->
	    /* all clauses have the same argument, being some structure */
	    fail
	;
	    true
	),
	(Count = 1, NrVars >= 1, Groups = [G|_], arg(1, G, var) ->
	    /* a choicepoint will be made anyway, will be most probably an output argument as well */
	    fail
	;
	    true
	),
	(Count < 3, NrVars == 0,
	    check_switchonlist(NewGroups, [c], [l], LAtomic, LCompou, Todo1, TodoTail, Index) ->
	    Is1 = [switchonlist(Index, LAtomic1, LCompou1, LVar)|Is2],
	    inCaseOfFailing(VarList, LAtomic, LAtomic1, TodoTail,T1),
	    inCaseOfFailing(VarList, LCompou, LCompou1, T1, [])
	;
	    (Count < 3, NrVars == 0, 
		check_switchonterm(NewGroups, [c,i,f,t,h], [l,s], LAtomic, LCompou, Todo1, TodoTail) ->
		Is1 = [switchonterm(Index, LAtomic1, LCompou1, LVar)|Is2],
		inCaseOfFailing(VarList, LAtomic, LAtomic1, TodoTail,T1),
		inCaseOfFailing(VarList, LCompou, LCompou1, T1, [])
	    ;
		(Count > 0, Count < 6, fail, % fail for hProlog
		    all_atom_struct(NewGroups, List_A_S, Todo2) ->
		    functor_select_instruction(Count, Index, List_A_S, I,  LVar, LFail),
		    Is1 = [I|Is2],
		    inCaseOfFailing(VarList, -1, LFail, Todo1, Todo2)
		;
		    (Count < 3,  /* this can only be lists, together with vars */
		    check_switchonterm(NewGroups, [c,i,f,t,h], [l,s], LAtomic, LCompou, Todo1, TodoTail) ->
			Is1 = [switchonterm(Index, LAtomic1, LCompou1, LVar)|Is2],
			inCaseOfFailing(VarList, LAtomic, LAtomic1, TodoTail,T1),
			inCaseOfFailing(VarList, LCompou, LCompou1, T1, [])
		    ;
			Is1 = [cases(IndexFailedLabel, Arity, Count)|Is5],
			indexingArgsRec(NewGroups, Is5, Is6, Todo2, Type), % last arg for type
			make_switchonbound(Type),
			Is6 = [switchonbound(Index,Count, 0, LVar) |Is2],
			inCaseOfFailing(VarList, -1, IndexFailedLabel, Todo1, Todo2)
		    )
		)
	    )
	),
	( Count = 1 ->
	    Todo1 = [todo(TodoI, TodoClauses)|Todo18], /* L1 can be reused later on
	                                                  if: (1) no more indexing,
                                                              (2) Var exit of switch
						       */
	    Todo = [todo(TodoI, TodoClauses, L1),todo(LVar, Heads, L1)|Todo18]
	;
	    Todo = [todo(LVar, Heads, L1)|Todo1]
	),
	(var(L1) -> newLabelGen(L1);true).



make_switchonbound(atomliststruct).
make_switchonbound(integer).
make_switchonbound(float).
make_switchonbound(char).
make_switchonbound(string).

hprolog.

all_atom_struct([], [], []).
all_atom_struct([First|Groups], [Info|Infos], Todo) :-
	First = group(Type, Value, Group, []),
	(Type == c -> true; (Type == s->true ; Type = i)),
%	(Type == c -> true; Type == s->true), 
	Info = i(Type, Value, Address),
	(Group = [Cl]->
	    Cl = i(_, _, Entry),
	    entryAddress(Entry, Address),
	    Todo = Todo1
	;
	    Todo =[todo(Address, Group)| Todo1]
	),
	all_atom_struct(Groups, Infos, Todo1).

functor_select_instruction(_, _, _, _, _, _) :- !,
	compiler_error("Error in compiler: functor_select_instruction",no_context), fail.

functor_select_instruction(2, Index, [I1, I2], I, LVar, LFail) :-
	I1 = i(Type1, F1, L1),
	I2 = i(Type2, F2, L2),
	I = select_functor2(Index, What,F1,L1,F2,L2,LVar,LFail),
	make_select_what([Type2, Type1], 0, What).

functor_select_instruction(3, Index, [I1, I2, I3], I, LVar, LFail) :-
	I1 = i(Type1, F1, L1),
	I2 = i(Type2, F2, L2),
	I3 = i(Type3, F3, L3),
	I = select_functor3(Index, What,F1,L1,F2,L2,F3,L3,LVar,LFail),
	make_select_what([Type3, Type2, Type1], 0, What).

functor_select_instruction(4, Index, [I1, I2, I3, I4], I, LVar, LFail) :-
	I1 = i(Type1, F1, L1),
	I2 = i(Type2, F2, L2),
	I3 = i(Type3, F3, L3),
	I4 = i(Type4, F4, L4),
	I = select_functor4(Index, What,F1,L1,F2,L2,F3,L3,F4,L4,LVar,LFail),
	make_select_what([Type4, Type3, Type2, Type1], 0, What).

functor_select_instruction(5, Index, [I1, I2, I3, I4, I5], I, LVar, LFail) :-
	I1 = i(Type1, F1, L1),
	I2 = i(Type2, F2, L2),
	I3 = i(Type3, F3, L3),
	I4 = i(Type4, F4, L4),
	I5 = i(Type5, F5, L5),
	I = select_functor5(Index, What,F1,L1,F2,L2,F3,L3,F4,L4,F5,L5,
			    LVar,LFail),
	make_select_what([Type5, Type4, Type3, Type2, Type1], 0, What).

make_select_what([], What, What).
make_select_what([Type|Types], What1, What2) :-
	( Type = s ->
	    What3 is What1 * 4 + 1
	;
	   (
	     Type = i ->
	     What3 is What1 * 4 + 2
	   ;
	    
	     What3 is What1 * 4
	   )
	),
	make_select_what(Types, What3, What2).

inCaseOfFailing([], Default, Default, Todo, Todo).
inCaseOfFailing([Cl], Default, Label, Todo1, Todo2) :- !, Todo1 = Todo2, 
	(var(Default) ->
	    Label = Default
	;   Cl = i(_, _, Entry),
	    entryAddress(Entry, Label)
	).
inCaseOfFailing(Clauses, Default, Label, Todo1, Todo2) :-
	Clauses = [_|_], 
	(var(Default)  -> Label = Default, Todo1 = Todo2
	;
	    Todo1 = [todo(Label, Clauses)|Todo2]
	).


groupSort(Groups, 1, SGroups, VarGroup) :- !,
	Groups = [Group],
	(arg(1, Group, var) ->
	    VarGroup = Group, SGroups = []
	;
	    SGroups = Groups
	).
groupSort(Groups, Len, SGroups, VarGroup) :-
	Len2 is Len //2,
	Len3 is Len - Len2,
	split(Groups, Len2, Groups1, Groups2),
	groupSort(Groups1, Len2, SGroups1, VarGroup1),
	groupSort(Groups2, Len3, SGroups2, VarGroup2),
	mergeGroups(SGroups1, SGroups2, SGroups, VarGroup1, VarGroup2),
	(var(VarGroup1) -> VarGroup = VarGroup2;
	    (var(VarGroup2) -> VarGroup = VarGroup1;
		VarGroup1 = group(_, _, First, Middle),
		VarGroup2 = group(_, _, Middle, Last),
		VarGroup = group(_, _, First, Last)
	    )
	).


addGroupsFront([],[], _).
addGroupsFront([group(Type, Value, Begin, End)|Groups],
	       [group(Type, Value, NewBegin, End)|NewGroups], VarList) :-
	copyVarList(VarList, NewBegin, Begin),
	addGroupsFront(Groups, NewGroups, VarList).

addGroupsEnd([],[], _).
addGroupsEnd([group(Type, Value, Begin, End)|Groups],
	       [group(Type, Value, Begin, NewEnd)|NewGroups], VarList) :-
	copyVarList(VarList, End, NewEnd),
	addGroupsEnd(Groups, NewGroups, VarList).

copyVarList(VarList, Begin, End) :-
	(var(VarList) ->
	    Begin = End
	;
	    VarList = [El|Tail],
	    Begin = [El|NewBegin1],
	    copyVarList(Tail, NewBegin1, End)
	).


groupSortSimple(Groups, 1, SGroups) :- !, SGroups = Groups. 
groupSortSimple(Groups, Len, SGroups) :-
	Len2 is Len //2,
	Len3 is Len - Len2,
	split(Groups, Len2, Groups1, Groups2),
	groupSortSimple(Groups1, Len2, SGroups1),
	groupSortSimple(Groups2, Len3, SGroups2),
	mergeGroupsSimple(SGroups1, SGroups2, SGroups).

mergeGroups([], SGroups2, SGroups3, VarGroup1, _) :- !,
	(nonvar(VarGroup1) ->
	    arg(3, VarGroup1, VarList1),
	    addGroupsFront(SGroups2, SGroups3,  VarList1)
	;
	    SGroups2 = SGroups3
	).
mergeGroups(SGroups1, [], SGroups3, _, VarGroup2) :- !,
	(nonvar(VarGroup2) ->
	    arg(3, VarGroup2, VarList2),
	    addGroupsEnd(SGroups1, SGroups3, VarList2)
	;
	    SGroups1 = SGroups3
	).
mergeGroups(SGroups1, SGroups2, SGroups, VarGroup1, VarGroup2) :-
	SGroups1 = [Group1| SGroups3],
	Group1 = group(Type1, Value1, Begin1, End1),
	SGroups2 = [Group2| SGroups4],
	Group2 = group(Type2, Value2, Begin2, End2),
	( Type1 == Type2 , Value1 == Value2 ->
	     End1 = Begin2,
	     SGroups = [group(Type1, Value1, Begin1, End2)| SGroups5],
	     mergeGroups(SGroups3, SGroups4, SGroups5, VarGroup1, VarGroup2)
	;
	   ( (Type1 @< Type2; Type1 == Type2, Value1 @< Value2)
	   ->		
	       (nonvar(VarGroup2) ->
		   arg(3, VarGroup2, VarList2),
		   copyVarList(VarList2, End1, NEnd),
		   NGroup1 = group(Type1, Value1, Begin1, NEnd)
	       ;
		   NGroup1 = Group1
	       ),
	       SGroups = [NGroup1|SGroups5],
	       mergeGroups(SGroups3, SGroups2, SGroups5, VarGroup1, VarGroup2)
	   ;
	       (nonvar(VarGroup1) ->
		   arg(3, VarGroup1, VarList1),
		   copyVarList(VarList1, NBegin, Begin2),
		   NGroup2 = group(Type2, Value2, NBegin, End2)
	       ;
		   NGroup2 = Group2
	       ),
	       SGroups = [NGroup2|SGroups5],
	       mergeGroups(SGroups1, SGroups4, SGroups5, VarGroup1, VarGroup2)
	   )
	).
mergeGroupsSimple([], SGroups1, SGroups2) :- !, SGroups1 = SGroups2.
mergeGroupsSimple(SGroups1, [], SGroups2) :- !, SGroups1 = SGroups2.
mergeGroupsSimple(SGroups1, SGroups2, SGroups) :-
	SGroups1 = [Group1| SGroups3],
	Group1 = group(Type1, Value1, Begin1, End1),
	SGroups2 = [Group2| SGroups4],
	Group2 = group(Type2, Value2, Begin2, End2),
	( Type1 == Type2 , Value1 == Value2 ->
	     End1 = Begin2,
	     SGroups = [group(Type1, Value1, Begin1, End2)| SGroups5],
	     mergeGroupsSimple(SGroups3, SGroups4, SGroups5)
	;
	   ( (Type1 @< Type2; Type1 == Type2, Value1 @< Value2)
	   ->		
		SGroups = [Group1|SGroups5],
		mergeGroupsSimple(SGroups3, SGroups2, SGroups5)
	   ;
	        SGroups = [Group2|SGroups5],
		mergeGroupsSimple(SGroups1, SGroups4, SGroups5)
	   )
	).

split(Groups, 0, Groups1, Groups2) :- !, Groups1 = [], Groups = Groups2.
split(Groups, Len, Groups1, Groups2) :- Len > 0,
	Len1 is Len -1,
	Groups = [Group |Groups0],
	Groups1 =  [Group|Groups3],
	split(Groups0, Len1, Groups3, Groups2).

findArguments(_, [], [], NrVars, NrVars, Len, Len).
findArguments(Index, [Head|Heads], [group(Type, Value,[Head|E],E)|Groups], NrVars1, NrVars2, Len1, Len2) :-
	findArgument(Index, Head, Arg),
	typevalue(Arg, Type, Value),	
	Len3 is Len1 + 1,
	( Type = var ->
	    NrVars3 is NrVars1 + 1
	;
	    NrVars3 = NrVars1
	),
	findArguments(Index, Heads, Groups, NrVars3, NrVars2, Len3, Len2).

typevalue(atom(Atom, _),      c, Atom).
typevalue(list(_,_,_),        l, l   ).
typevalue(integer(Int, _C),      Type, Val   ) :-
	(bigint(Int) ->
	    fail
	;
	    Type = i, Val = Int
	).
typevalue(float(F, _),        f, F   ).
typevalue(string(S, _),       t, S   ).
typevalue(char(S, _),         h, S   ).
typevalue(functor(Nr,_,_, _), s, Nr  ).
typevalue(var(_,_, _),        var, _   ).


check_switchonterm([], _, _, -1, -1, Todo, Todo).
check_switchonterm([First|Groups],Atomic,Compound,LAtomic,LCompound,
		                                          Todo, TodoTail) :-
	First = group(Type, _, Group, []),
	(mymember(Type, Atomic) ->
	    LAtomic = Address,
	    check_switchonterm(Groups,[],Compound,_,LCompound,Todo1, TodoTail)
	;
	    mymember(Type, Compound),
	    LCompound = Address,
	    check_switchonterm(Groups,Atomic,[],LAtomic,_,Todo1,TodoTail)
	),
	( Group = [Cl] ->
		Cl = i(_, _, Entry),
	        entryAddress(Entry, Address),
		Todo = Todo1
	;	
		Todo = [todo(Address, Group)|Todo1]
	).

check_switchonlist([], _, _, -1, -1, Todo, Todo, _).
check_switchonlist([First|Groups],Atomic,Compound,LAtomic,LCompound, Todo, TodoTail, Index) :-
	First = group(Type, _, Group, []),
	(mymember(Type, Atomic) ->
	    LAtomic = Address,
	    check_switchonlist(Groups,[],Compound,_,LCompound,Todo1, TodoTail, Index)
	;
	    mymember(Type, Compound),
	    LCompound = Address,
	    check_switchonlist(Groups,Atomic,[],LAtomic,_,Todo1,TodoTail, Index)
	),
	( Group = [Cl] ->
	    Cl = i(_, _, Entry),
	    entryAddress(Entry, Address),
	    Todo = Todo1
	;	
	    Todo = [todo(Address, Group)|Todo1]
	).



entryAddress(normal(Address), Address) :- ! .
entryAddress(skip_list(_, Address, _), Address).


indexingArgsRec([], Is1, Is1, [], _ ).
indexingArgsRec([First|Groups], Is1, Is2, Todo, AllowedType) :-
	First = group(Type, Value, Group, []),
	allowed_type(Type,AllowedType),
	index_command(Type, Value, Address, Instruction),
	Is1 = [Instruction|Is3],
	( Group = [Cl] ->
		Cl = i(_, _, Entry),
	        entryAddress(Entry, Address),!,
		Todo = Todo1
	;	
		Todo = [todo(Address, Group)|Todo1]
	),
	indexingArgsRec(Groups, Is3, Is2, Todo1, AllowedType).

allowed_type(c, atomliststruct).
allowed_type(l, atomliststruct).
allowed_type(s, atomliststruct).
allowed_type(i, integer).
allowed_type(f, float).
allowed_type(h, char).
allowed_type(t, string).

index_command(c, Atom , Address, I) :- I = arglabel(c, Atom , Address).
index_command(l, _    , Address, I) :- I = arglabel(l, l    , Address).
index_command(i, Int  , Address, I) :- I = arglabel(i, Int  , Address).
index_command(f, Float, Address, I) :- I = arglabel(f, Float, Address).
index_command(t, String, Address, I) :-
	string_length(String,Len), I = arglabel(t, Len, String, Address).
index_command(h, Char, Address, I) :- I = arglabel(h, Char, Address).
index_command(s, Nr , Address, I) :- I = arglabel(s, Nr , Address).


collectHeads([], [], []).
collectHeads([clause(functor(_, _, Args, _), Body, _, _, _)|Clauses], 
	     [i(Head, Body, Entry)| Heads], [Entry|Entries]) :-
	Head =.. [p|Args],
	collectHeads(Clauses, Heads, Entries).

make_to_index(Arity, Arity, ToIndex) :- !, ToIndex = [].
make_to_index(I, Arity, [I1|ToIndex]) :- I < Arity,
	I1 is I + 1,
	make_to_index(I1, Arity, ToIndex).

handleTodo([], _, _, _, Is1, Is2) :-  Is1 = Is2.
handleTodo([Todo|Todos], ToIndex, MaxIndex, Arity, Is1, Is2) :-
	( Todo = todo(I, Clauses) ->
	    generalListCode(Clauses, I, ToIndex, MaxIndex, Arity, Is1,Is3)
	;
	    Todo = todo(I, Clauses, VarLabel),
	    generalListCodeSpecial(Clauses, I, ToIndex, MaxIndex, Arity,
				   Is1,Is3, VarLabel)
	),
	handleTodo(Todos, ToIndex, MaxIndex, Arity, Is3, Is2).
	
generalListCodeSpecial(Clauses, I, ToIndex, MaxIndex, Arity,
		       Is1, Is2, VarLabel) :-
	(   MaxIndex > 0,
	    Is1 = [label(I)|Is3],
	    indexingArgs(ToIndex, MaxIndex, Clauses, Arity, Is3, Is2, VarLabel),
	    newLabelGen(I)
	-> true
	;    /* no more indexing possible */
	    I = VarLabel,
	    Is1 = Is2
	).

generalListCode(Clauses, I, ToIndex, MaxIndex, Arity, Is1, Is2) :-
	newLabelGen(I),
	Is1 = [label(I)|Is3],
	generalListCode2(Clauses, Arity, Is4, Is2),
	(   MaxIndex > 0,
	    Is5 = [label(L3)|Is4],
	    indexingArgs(ToIndex, MaxIndex, Clauses, Arity, Is3, Is5, L3) -> true
	;    /* no more indexing possible */
	    Is3 = Is4
	).

findArgument(Index, i(Head, Body, _), Arg) :-
	arg(Index, Head, HeadArg),
	( HeadArg = var(_, _, _), checkInline(Body, HeadArg, Arg) ->
	    true
	;
	    Arg = HeadArg
	).

checkInline([I|Is], Var, Type) :-
	( checkInlineInstr(I, Var, Type) ->
	    true
	;   
	    checkInline(Is, Var, Type)
	).

checkInlineInstr(=(X2, X3,_), X1, Type) :- !,
	( identicalterms(X1,X2) ->
	    checkInlineAssign(X3),
	    Type = X3
	;   
	    identicalterms(X1,X3),
	    checkInlineAssign(X2),
	    Type = X2
	).
checkInlineInstr(test("==",[X2,X3],_), X1, Type) :-
	( identicalterms(X1,X2) ->
	    checkInlineAssign(X3),
	    Type = X3
	;   
	    identicalterms(X1,X3),
	    checkInlineAssign(X2),
	    Type = X2
	).

checkInlineInstr('_$cutto'(_), Var, Var).
checkInlineInstr(atomcall(_, _, _, _), Var, Var).
checkInlineInstr(functorcall(_, _, _, _, _), Var, Var).
checkInlineInstr('$initperm'(_, _), Var, Var).

identicalterms(var(A1,A2,_),var(B1,B2,_)) :- f(A1,A2) == f(B1,B2).
identicalterms(atom(A,_),atom(A,_)).
identicalterms(char(A,_),char(A,_)).
identicalterms(string(A,_),string(A,_)).
identicalterms(integer(A,_),integer(A,_)).
identicalterms(float(A,_),float(A,_)).
identicalterms(list(A1,A2,_),list(B1,B2,_)) :-
	identicalterms(A1,A2),
	identicalterms(B1,B2).
identicalterms(functor(Arity,Name,Args1,_),functor(Arity,Name,Args2,_)) :-
	identicaltermslist(Args1,Args2).

identicaltermslist([],[]).
identicaltermslist([A|As],[B|Bs]) :-
	identicalterms(A,B),
	identicaltermslist(As,Bs).


checkInlineAssign(list(_,_,_)).
checkInlineAssign(integer(_, _)).
checkInlineAssign(float(_, _)).
checkInlineAssign(string(_, _)).
checkInlineAssign(char(_, _)).
checkInlineAssign(atom(_, _)).
checkInlineAssign(functor(_, _, _, _)).

generalListCode2([i(_, _, Entry)| Clauses], Arity, Is1, Is2) :-
	Is1 = [try(Arity, Address)|Is3],
	entryAddress(Entry, Address),
	generalListCode3(Clauses, Arity, Is3, Is2).

generalListCode3([i(_, _, Entry)| Clauses], Arity, Is1, Is2) :-
	entryAddress(Entry, Address),
	(Clauses = [] ->
	    Is1 = [trust(Arity, Address)| Is2]
	;   
	    Is1 =[retry(Arity, Address)| Is3],
	    generalListCode3(Clauses, Arity, Is3, Is2)
	).


chunkifyClauses([], _).
chunkifyClauses([clause(Head, Body, Bindings, Ref, Id)| Clauses], Options) :-
	studyClause(Head, Body, Ref),
 	lookForVoids(Bindings, VoidNames),
% 	VoidNames = [],
	voidWarning(VoidNames, Id),  
	chunkifyClauses(Clauses, Options).

lookForVoids([], []).
lookForVoids([branches(Type) = Name| Bindings], VoidNames) :-
	(
	  Type = void,
	  name(Name, CharList),
	  CharList = [Char|_],
	  Char \= 0'_
	->
	  VoidNames = [Name|VoidNames1]
        ;
	  VoidNames1 = VoidNames
	),
	lookForVoids(Bindings, VoidNames1).

voidWarning([], _).
voidWarning(Vars, Id) :-
	Vars = [_|L],
	compiler_out("Singleton variable"),
	(L = [] ->
	    true
	;
	    compiler_out("s")
	),
        compiler_out(" in clause for defining predicate "),
	compiler_out(Id),
	compiler_out(" "),
	compiler_out(Vars),
	compiler_out('\n').

classifyPackVars([]).
classifyPackVars([Var|Vars]) :-
	Var = branches(Branches),
	classifyPackVarsBranches(Branches, WasVoid),
	setarg(1, Var, WasVoid),
	classifyPackVars(Vars).

classifyPackVarsBranches([], WasVoid) :-
	( WasVoid = [] -> true ; true).
classifyPackVarsBranches([branch(Chunks, _)|Branches], WasVoid) :-
	classifyPackVarsBranch(Chunks, WasVoid),
	classifyPackVarsBranches(Branches, WasVoid).

classifyPackVarsBranch(Branch, WasVoid) :-
	Branch = [chunk(_, Occ)|OtherChunk],
	( OtherChunk = [] ->
		Occ = [occ(Info, _)|OtherOcc],
	        ( OtherOcc = [] ->
			Info = void, WasVoid = void
		;
		  	countOccPack(OtherOcc, 1, Len),
			notifyPack(Occ, temp(_, Len, _))
		)
	;
		countPermPack(Branch, 0, Lenn),
		notifyRecPack(Branch, perm(_, Lenn, _))
	).

notifyPack([], _).
notifyPack([occ(Val, _)|Occs], Val) :-
	notifyPack(Occs, Val).

notifyRecPack([], _).
notifyRecPack([chunk(_, Occ)|Vars], Val) :-
	notifyPack(Occ, Val),
	notifyRecPack(Vars, Val).

countOccPack([], Nr, Nr).
countOccPack([occ(_, _)|Occs], Nr, Nr1) :-
	Nr2 is Nr + 1,
	countOccPack(Occs, Nr2, Nr1).

countPermPack([], Nr, Nr).
countPermPack([chunk(_, Occ)|OVars], Nr, Nr1) :-
	countOccPack(Occ, Nr, Nr2),
	countPermPack(OVars, Nr2, Nr1).

addOccurencePackBranch(Branch, ChunkNr, Info, Where) :-
	arg(1, Branch, Chunks),
	Chunks = [Chunk|_],
	Chunk = chunk(ChunkNr1, Occs),
	( ChunkNr = ChunkNr1 ->
	    setarg(2, Chunk, [occ(Info, Where)|Occs])
	;   
	    setarg(1, Branch, [chunk(ChunkNr, [occ(Info, Where)])|Chunks])
	).


studyClause(Head, Body, Ref):-
	searchVariablesTerm(Head, Vars, VarsAcc, 1, head, [0]),
	initNI(NI),
	searchVariablesBody(Body, VarsAcc, [], Ref, NI, _, DropAreg, []),
	classifyPackVars(Vars),
	assignDropAreg(DropAreg).

assignDropAreg([]).
assignDropAreg([ [integer(N,_), var(_,Var, _)] | Other ]):-
	        (Var = temp([N], _, _) -> true; true),
		assignDropAreg(Other).

searchVariablesTerm(atom(_,_), Vars, Vars, _, _, _).
searchVariablesTerm(integer(_,_), Vars, Vars, _, _, _).
searchVariablesTerm(float(_,_), Vars, Vars, _, _, _).
searchVariablesTerm(string(_,_), Vars, Vars, _, _, _).
searchVariablesTerm(char(_,_), Vars, Vars, _, _, _).
searchVariablesTerm(functor(_, _, Args,_), Vars, VarsAcc, ChunkNr, _, Root) :-
	searchVariablesArgs(Args, Vars, VarsAcc, ChunkNr, arg, Root).
searchVariablesTerm(bin_op(_, A1,A2), Vars, VarsAcc, ChunkNr, _, Root) :-
	searchVariablesArgs([A1,A2], Vars, VarsAcc, ChunkNr, arg,Root).
searchVariablesTerm(list(A1,A2,_), Vars, VarsAcc, ChunkNr, _, Root) :-
	searchVariablesArgs([A1,A2], Vars, VarsAcc, ChunkNr, arg,Root).
searchVariablesTerm(var(Var, Info,_), Vars, VarsEnd,ChunkNr,Where,Root) :-
	( var(Var) -> Vars = [Var|VarsEnd]; Vars = VarsEnd),
	addoccurence(Var, ChunkNr, Info, Where, Root).
searchVariablesTerm(uni_op(_, A1), Vars, VarsAcc, ChunkNr, _, Root) :-
	searchVariablesArgs([A1], Vars, VarsAcc, ChunkNr, arg,Root).

addoccurence(Var, ChunkNr, Info, Where, Root) :-
	(var(Var) ->
	        Chunks = [chunk(ChunkNr, [occ(Info, Where)])],
		Var = branches([branch(Chunks, Root)])
	;
	    Var = branches(Branches),
	    Branches = [Branch|_],
	    Branch = branch(_, BranchRoots),
	    BranchRoots = [BranchRoot|_],
	    (mymember(BranchRoot, Root) ->
		addOccurencePackBranch(Branch, ChunkNr, Info, Where)
	    ;
		Root = [NewBranchNr|_],
		( NewBranchNr > BranchRoot ->
		    NewChunks = [chunk(ChunkNr, [occ(Info, Where)])],
		    NewBranch = branch(NewChunks, Root),
		    setarg(1, Var , [NewBranch|Branches])
		;
		    /* We have to change the root and branches
		       of this variable! */
		    addOccurencePackBranch(Branch, ChunkNr, Info, Where),
		    fixBranches(Var, NewBranchNr)
		)
	    )
	).
	    
fixBranches(Var, CurrentRoot) :-
	Var = branches([Branch|Branches]),
	Branch = branch(_, BranchRoots),
	decideOnNewRoot(BranchRoots, CurrentRoot, NewRoots),
	setarg(2, Branch, NewRoots),
        appendOtherBranches(Branches, Branch, CurrentRoot, Remainder),
	setarg(1, Var, [Branch|Remainder]).


appendOtherBranches([], _, _, []).
appendOtherBranches(OrigBranches, Branch, CurrentRoot, Remainder) :-
	OrigBranches = [Branch2|Branches2],
	Branch2 = branch(Chunks2, BranchRoots2),
	BranchRoots2 = [BranchRoot2|_],
	( CurrentRoot < BranchRoot2 ->
	    Branch = branch(Chunks, _),
	    conc(Chunks, Chunks2, Chunks3),
	    setarg(1, Branch, Chunks3),
	    appendOtherBranches(Branches2, Branch, CurrentRoot, Remainder)
	;
	    Remainder = OrigBranches
	).

decideOnNewRoot(OldRoots, CurrentRoot, NewRoot) :-
	OldRoots = [N|BranchRoots],
	( N > CurrentRoot ->
	    decideOnNewRoot(BranchRoots, CurrentRoot, NewRoot)
	;
	    NewRoot = OldRoots
	).

searchVariablesBody([], Vars, Vars, nocall, NI, NI, DA, DA):- ! .
searchVariablesBody(B, Vars, VarsAcc, Ref, NI1, NI2, DA, DA1):-
	B = [Body|Bodys],
	searchVariablesBodyEl(Body, B, Vars,Vars1,Ref,NRef, NI1, NI3, DA, DA2),
	searchVariablesBody(Bodys, Vars1, VarsAcc, NRef, NI3, NI2, DA2, DA1).

searchVariablesBodyRef([], Vars, Vars, Ref, NI, NI, Ref, DA,DA):- ! .
searchVariablesBodyRef(B, Vars, VarsAcc, Ref, NI1, NI2, FinalRef, DA,DA1):-
	B = [Body|Bodys],
	searchVariablesBodyEl(Body, B, Vars,Vars1,Ref,NRef, NI1, NI3, DA, DA2),
	searchVariablesBodyRef(Bodys,Vars1,VarsAcc,NRef,NI3,NI2,FinalRef,DA2,DA1).


simpleBodyEl('$initperm'(X, _), [X]) :- write(1).
simpleBodyEl(=(X, Y,_), [X, Y]).
simpleBodyEl(X is Y, [X, Y]).
simpleBodyEl('$mark'(X), [X]).
simpleBodyEl('_$savecp'(X), [X]).
simpleBodyEl('_$cutto'(X), [X]).
simpleBodyEl(fail, []).
simpleBodyEl(test(_, Args, _), Args).
simpleBodyEl(numericaltest(_, Arg1, Arg2, _), [Arg1,Arg2]).
simpleBodyEl(inline(_, _, Args, _), Args).

checkDropAreg(BodyEl, DA, DA1):-
	( BodyEl = inline("drop_areg__M_sysh", 2, Args, _) ->
	    DA = [Args|DA1]
	;
	    DA = DA1
	).

searchVariablesBodyEl(BodyEl, _, Vars, VarsEnd, Ref1,Ref2, NI1, NI2, DA, DA1):-
	simpleBodyEl(BodyEl, Args), !, NI1 = NI2,
	checkDropAreg(BodyEl, DA, DA1),
	Ref1 = Ref2,
	arg(1, NI1, ChunkNr),
	arg(4, NI1, Root),
	searchVariablesArgs(Args, Vars, VarsEnd, ChunkNr, arg, Root).
searchVariablesBodyEl(Call, B, Vars, VarsEnd, Ref1, Ref2, NI1, NI2, DA, DA):-
	Call = atomcall(_, Ref2, _, _), !,
	Ref1 = B,
	Vars = VarsEnd,
	newChunkNr(NI1, NI2).
searchVariablesBodyEl(Call, B, Vars, VarsEnd, Ref1, Ref2, NI1, NI2, DA, DA):-
	Call = functorcall(_, _, Args, Ref2, _), !,
	Ref1 = B,
	newChunkNr(NI1, NI2),
	arg(1, NI1, ChunkNr),
	arg(4, NI1, Root),
	searchVariablesArgs(Args, Vars, VarsEnd, ChunkNr, call, Root).
searchVariablesBodyEl(Disj, _, Vars, VarsEnd, Ref1, Ref2, NI1, NI2, DA, DA1):-
	Disj = disjs(List, Ref2), !,
	Ref1 = [atomcall(_, _, _, _), dummy|_], % make sure allocate is added.
	prepareDisj(NI1, NI3),
	searchVariablesBodyDisj(List, Vars, VarsEnd, NI3, NI4, DA, DA1),
	postDisj(NI1, NI3, NI4, NI2).

searchVariablesBodyEl(IfThenElse, B, Vars, VarsEnd, Ref1, Ref2, NI1, NI2,DA,DA1):-
	IfThenElse = smallITE(If, Then, Else, i(RefA, RefB, Ref2)), !,
	prepareDisj(NI1, NI3),
	newBranchNrFirst(NI3, NI4),
	searchVariablesBody(If, Vars, Vars2, _, NI4, NI5, DA, DA2),
	searchVariablesBody(Then, Vars2, Vars3, RefA, NI5, NI6, DA2, DA3),
	newBranchNrSmallIf(NI4, NI6, NI8),
	searchVariablesBody(Else, Vars3, VarsEnd, RefB, NI8, NI9, DA3, DA1),
	mergeRefs(RefA, RefB, B, Ref1),
	postDisj(NI1, NI3, NI9, NI2).
searchVariablesBodyEl(IfThenElse, _, Vars, VarsEnd, Ref1,Ref2,NI1,NI2,DA,DA1):-
	IfThenElse = ifthenelse(_, _, _, _, Ref2), !,
	prepareDisj(NI1, NI3),
	searchVariablesBodyITE(IfThenElse, Vars, VarsEnd, Ref1,NI3,NI4,DA,DA1),
	postDisj(NI1, NI3, NI4, NI2).

searchVariablesBodyITE(IfThenElse, Vars, VarsEnd, RefA, NI1, NI2, DA, DA1):-
	IfThenElse = ifthenelse(Ref3, If, Then, Else, _),
	newBranchNrFirst(NI1, NI3),
	searchVariablesBodyRef(If, Vars, Vars2, Ref1, NI3, NI4, Ref2, DA, DA2),
	searchVariablesBody(Then, Vars2, Vars3, Ref2, NI4, NI6, DA2, DA3),
	firstRef(Ref1, Ref2, RefA),
	afterBranch(NI3, NI6, NI7),
	newBranchNr(NI7, NI8),
	searchVariablesBody(Else, Vars3, VarsEnd, Ref3, NI8, NI9, DA3, DA1),
	afterBranch(NI8, NI9, NI2).


mergeRefs(nocall, RefB, [_], Ref1) :- !, Ref1 = RefB.
mergeRefs(RefA, nocall, [_], Ref1) :- !, Ref1 = RefA.
mergeRefs([C|Cs1], [_|Cs2], [_], Ref1) :- !,
	( Cs1 == [], Cs2 == [] ->
	    Ref1 = [C]
	;
	    Ref1 = [C, dummy|_]
	).
mergeRefs(RefA, RefB , _, Ref1) :- 
	firstRef(RefA, RefB, Ref1).


firstRef(Ref1, Ref2, RefA) :-
	(Ref1 == nocall -> RefB = Ref2; RefB = Ref1),
	( RefB = [C|_] ->
	    RefA = [C, dummy|_]
	;
	    RefA = [atomcall(_, _, _, _), dummy|_] % a safe approximation
	).


searchVariablesBodyDisj([], Vars, Vars, NI1, NI1, DA, DA).
searchVariablesBodyDisj([First|Other], Vars, VarsEnd, NI1, NI2, DA, DA1):-
	newBranchNr(NI1, NI3),
	First = ['$startDisj'(Ref)|Body],
	searchVariablesBody(Body, Vars, Vars2, Ref, NI3, NI4,DA, DA2),
	afterBranch(NI3, NI4, NI5),
	searchVariablesBodyDisj(Other, Vars2, VarsEnd, NI5, NI2, DA2, DA1).

searchVariablesArgs([], Vars, Vars, _, _, _).
searchVariablesArgs([Term|Args], Vars1, Vars2, ChunkNr, Where, Root) :-
	searchVariablesTerm(Term, Vars1, Vars3, ChunkNr, Where, Root),
	searchVariablesArgs(Args, Vars3, Vars2, ChunkNr, Where, Root).


delta_div(Delta1,Delta2) :-
	(integer(Delta1) ->
	    D is Delta1 // 2,
	    (D > 0 ->
		Delta2 = D
	    ;
		Delta2 is Delta1 / 2
	    )
	;
	    Delta2 is Delta1 / 2
	).



initNI(numberInfo(1, 2, 65536, [0])).

newChunkNr(NI1, NI2) :-
	NI1  = numberInfo(_, ChunkNr1, Delta, Root),
	ChunkNr2 is ChunkNr1 + 1,
	NI2  = numberInfo(ChunkNr1, ChunkNr2, Delta, Root).

prepareDisj(NI1, NI2) :-
	NI1  = numberInfo(ChunkNr1, ChunkNr2, Delta1, Root),
	Root = [BranchNr|_],
	delta_div(Delta1, Delta2),
	NI2  = numberInfo(ChunkNr1, ChunkNr2, Delta2, [BranchNr|Root]).

postDisj(NI1, NI3, NI4, NI2) :-
	arg(2, NI4, ChunkNr1),
	arg(4, NI1, Root),
	arg(3, NI3, Delta),
	ChunkNr2 is ChunkNr1 + 1,
	NI2 = numberInfo(ChunkNr1, ChunkNr2, Delta, Root).

newBranchNr(NI1, NI2) :-
	NI1  = numberInfo(_, ChunkNr1, Delta1, [BranchNr1|Root]),
	delta_div(Delta1, Delta2),
	BranchNr2 is BranchNr1 + Delta1,
	ChunkNr2 is ChunkNr1 + 1,
	NI2  = numberInfo(ChunkNr1, ChunkNr2, Delta2, [BranchNr2|Root]).

newBranchNrFirst(NI1, NI2) :-
	NI1  = numberInfo(ChunkNr1, ChunkNr2, Delta1, [BranchNr1|Root]),
	delta_div(Delta1, Delta2),
	BranchNr2 is BranchNr1 + Delta1,
	NI2  = numberInfo(ChunkNr1, ChunkNr2, Delta2, [BranchNr2|Root]).

afterBranch(NI3, NI4, NI5) :-
	arg(1, NI4, ChunkNr1),
	arg(2, NI4, ChunkNr2),
	arg(3, NI3, Delta),
	arg(4, NI3, Root),
	NI5 = numberInfo(ChunkNr1, ChunkNr2, Delta, Root).

newBranchNrSmallIf(NI5, NI6, NI8) :-
	arg(1, NI5, ChunkNr1),
	arg(2, NI6, ChunkNr2),
	arg(3, NI5, Delta1),
	arg(4, NI5, Root),
	Root = [BranchNr1|Root1],
	BranchNr2 is BranchNr1 + Delta1,
	NI8 = numberInfo(ChunkNr1, ChunkNr2, Delta2, [BranchNr2|Root1]),
	delta_div(Delta1, Delta2).

writeInfo(FileName, Info, Options) :-
	telling(Old),
	tell(FileName),
	write_version,
	decide_how(Options, How),
	write_type(How),
	collectFrom(Info, 0, Len, Assoc, 0, LenMod),
	write_ato(How, LenMod),
	write_ato(How, '\n'),
	writeMod(Assoc, How),
	write_ato(How, Len),
	write_ato(How, '\n'),
	declareFunctors(Info, Assoc, How),
	tell(Old).

decide_how(Options, How) :-
	mymember(compress, Options), !, How = compressed.
decide_how(Options, How) :-
	mymember(compact, Options), !, How = compact.
decide_how(_, full).

write_type(compressed) :- write(2), start_compression.
write_type(compact) :-    write(1), nl.
write_type(full) :-       write(0),nl.

writeMod([], _) :- ! .
writeMod([i(MName, Nr)|Other], How) :-
	write_ato(How, Nr), write_ato(How, ' '),
        atom_codes(MName, Chars),
	me_length(Chars, 0, Len),
	write_ato(How, Len), write_ato(How, ' '),
	write_ato(How, MName), write_ato(How, '\n'),
	writeMod(Other, How).

collectFrom([], Len, Len, _, LenMod, LenMod).
collectFrom([A|L], Len1, Len2, Assoc, LenMod1, LenMod2) :-
	arg(3, A, MName),
	mymember(i(MName, Nr), Assoc),
	(var(Nr) ->
	    Nr = LenMod1,
	    LenMod3 is LenMod1 + 1
	;
	    LenMod3 = LenMod1
	),
	arg(4, A, Len1),
	Len3 is Len1 + 1,
	collectFrom(L, Len3, Len2, Assoc, LenMod3, LenMod2).


declareFunctors([], _, _).
declareFunctors([a(Name, Arity, MName, IName)|List], Assoc, How) :-
%  	(How = full -> write(IName), write(' '); true),
 	write(IName), write(' '),
	mymember(i(MName, ModNr), Assoc),
	write_ato(How, ModNr), write_ato(How, ' '),
	write_ato(How, Arity), write_ato(How, ' '),
	atom_codes(Name, Chars),
	me_length(Chars, 0, Len),
	write_ato(How, Len), write_ato(How, ' '),
	write_ato(How, Name), write_ato(How, '\n'),
	declareFunctors(List, Assoc, How).

writePredNr(FileName, Number, Options) :-
	telling(Old),
	tell(FileName),
	(mymember(compress, Options) ->
	    write_atomic_compressed(Number),
	    write_atomic_compressed('\n')
	;
	    write(Number), nl
	),
	tell(Old).

writeCode(FileName, Compiled, Options) :-
	nb_getval('$compiler_errors',Errors),
	(Errors == none ->
	    telling(Old),
	    tell(FileName),
	    decide_how(Options, How),
	    Compiled = [predicate(Nr, NrLabels)| Compiled1],
	    (How = full -> write('predicate ');true),
	    write_ato(How, Nr), write_ato(How, ' '),
	    write_ato(How, NrLabels),
	    (How = full -> nl; write_ato(How, ' ')),
	    writeCCode(Compiled1, How),
	    (How = full -> true; write_ato(How, '\n')),
	    tell(Old)
	;
	    true
	).


write_ato(full, X) :- (string(X) -> wrs(X) ; sysh:write_atomic(X)).
write_ato(compact, X) :- (string(X) -> wrs(X) ; sysh:write_atomic(X)).

wrs(S) :- put(0'"), string_to_ilist(S,Il), wrs1(Il), put(0'"), fail.
wrs(_).
wrs1([]).
wrs1([X|R]) :- put(X), wrs1(R).


writeCCode([end_clauses|_], How) :- !,
	(writeInstruction(end_clauses, How) ; true).
writeCCode([Instruction |_], How) :-
	writeInstruction(Instruction, How).
writeCCode([_|Other], How) :-
	writeCCode(Other, How).

writeInstruction(inline(StrName, Arity, Args), How) :- !,
	string_to_atom(StrName,Name),
	inline_builtin_naming(Name, NName, Arity),
	atom_concat('builtin_', NName, P1),
	name(Underscore,[0'_]),
	atom_concat(P1,Underscore, P2),
	atom_concat(P2, Arity, OpcodeName),
	opcode(OpcodeName, Opcode),
	write_ato(How, Opcode),
	write_ato(How, ' '),
	(How = full ->  write(OpcodeName), write(' ');true),
	write_args_instruction(Args, How),
	(How = full -> nl;true),
	fail.

writeInstruction(Instruction, How) :-
	functor(Instruction, Name, _),
	( opcode(Name, Opcode) ->
	    write_ato(How, Opcode)
	;
	    write(How, unknown(Name)),nl,
	    (throw('unknown opcode');true) % in case no throw defined
	),
	write_ato(How, ' '),
	(How = full ->  write(Name), write(' ');true),
	Instruction =.. [_|Args],
	(Instruction = put_bigint(A,Int) ->
	    write_args_instruction([A], How),
	    bigint_print(Int)
	;
	    Instruction = put_bigintv(A,Int) ->
	    write_args_instruction([A], How),
	    bigint_print(Int)
	;
	    write_args_instruction(Args, How)
	),
	(How = full -> nl;true),
	fail.


write_args_instruction([], _).
write_args_instruction([Arg|Args], How) :-
	write_ato(How, Arg), write_ato(How, ' '),
	write_args_instruction(Args, How).

end_of_w_file(Options) :-
	( mymember(compress, Options) ->
	    stop_compression
	;
	    true
	).


close_output(File) :-
	telling(Now),
	tell(File),
	told,
	tell(Now).


predicateInstruction(predicate(Number, Labels), Number, Labels).

prolog_message_list([]).
prolog_message_list([Head|Tail]) :-
	compiler_out(Head),
	prolog_message_list(Tail).

prolog_message(Options, List) :-
	((mymember(silent, Options); prolog_flag(verbose,X), X = no)  -> true
	;
	    prolog_message_list(List)
	).


treatdecl(list(_,_,_), Term,Bindings,AllClauses,MoreClauses, ToDoFiles, ToDoFiles,_PreProc,_AF) :-
	!,
	AllClauses = [clause(Term, Bindings)|MoreClauses].
treatdecl(atom(StrName,_),Term,Bindings,AllClauses,MoreClauses, ToDoFiles, ToDoFiles,_PreProc,_AF) :-
	!,
	(ignore_decl(StrName,0,Term) ->
	    MoreClauses = AllClauses
	;
	    AllClauses = [clause(Term, Bindings)|MoreClauses]
	).
treatdecl(functor(Arity,StrName,Args,C), Term, Bindings,AllClauses,MoreClauses, ToDoFiles, NewToDoFiles,PreProc,AF) :-
	(
	  ignore_decl(StrName,Arity,Term) ->
	  MoreClauses = AllClauses,
	  NewToDoFiles = ToDoFiles
	;
	  (Arity == 3, StrName == "op", compiler_op(Args)) ->
	  MoreClauses = AllClauses,
	  NewToDoFiles = ToDoFiles
	;
	  (Arity == 1, StrName == "arithmetic_function") ->
	  Args = [Arg],
	  Arg = functor(2,"/",[atom(Name,_),integer(Ar,_)],_),
	  AF = Name/Ar,
	  MoreClauses = AllClauses,
	  NewToDoFiles = ToDoFiles
	;
	  (Arity == 1, StrName == "include", Args = [atom(StrFile,_)]) ->
	  string_to_atom(StrFile,NewFile),
	  seeing(OldFile),
	  see(NewFile), % XXX context enzo gebruiken
	  NewToDoFiles = [OldFile|ToDoFiles],
	  MoreClauses = AllClauses
	;
	  (Arity == 2, StrName == "use_module") ->
	  add2imports2(Args,FileName,PreProc),
	  atom_to_string(FileName,Str),
	  is_declaration(NewTerm,functor(1,"use_module__M_sysh",[atom(Str,C)],C),C),
	  AllClauses = [clause(NewTerm, Bindings)|MoreClauses],
	  NewToDoFiles = ToDoFiles
	;
	  (Arity == 1, StrName == "use_module") ->
	  add2imports1(Args,FileName,PreProc),
	  atom_to_string(FileName,Str),
	  is_declaration(NewTerm,functor(1,"use_module__M_sysh",[atom(Str,C)],C),C),
	  AllClauses = [clause(NewTerm, Bindings)|MoreClauses],
	  NewToDoFiles = ToDoFiles
	;
	  AllClauses = [clause(Term, Bindings)|MoreClauses],
	  NewToDoFiles = ToDoFiles
	),
	! .
treatdecl(Term, _, _, Clauses, Clauses, ToDoFiles, ToDoFiles,_PreProc,_AF) :-
	compiler_warning("Something wrong with declaration ",Term).

add2imports1([ModuleDescription],FileOut,PreProc) :-
	add2imports2([ModuleDescription,all],FileOut,PreProc).

add2imports2([ModuleAbstract,Listofpreds],FileName,PreProc) :-
	abstract2prolog(ModuleAbstract,Module),
	nb_getval('$fileprefix',PrefixContext),
	seeing(OldFile),
	getfilename(Module,PrefixContext,FileName1),
	(decideonsuffix(FileName1,FileName) ->
	    true
	;
	    compiler_error(bad_usemodule(getfilename(Module,PrefixContext,FileName1)),none),
	    fail	
	),
	(FileName == OldFile ->
	    compiler_error(["Module uses itself through use_module."],nocontext)
	;
	    (
	      see(FileName),
	      read(ModuleDeclaration),
	      (
		(get_exportedpreds_exportedops_preprocessor(ModuleDeclaration,
							    ModName,
							    ExportedOps,
							    ExportedPreds,
							    PreProc),
		    nb_getval('$importedpreds',Old),
		    (Listofpreds == all ->
			transformexportlist1(ExportedPreds,Exports1),
			nb_setval('$importedpreds',[ModName/Exports1|Old])
		    ;
			transformexportlist2(Listofpreds,Exports1), % check ?
			nb_setval('$importedpreds',[ModName/Exports1|Old])
		    )) ->
		    close(FileName)
		;
		    close(FileName), see(OldFile),
		    fail
		),
	      (PreProc == [] ->
		  true
	      ;
		  (
		    sysh:consult_once(FileName),
		    fail
		  ;
		    true
		  )
	      ),
	      (check_ops1(ExportedOps) -> see(OldFile) ; see(OldFile), fail)
	    )
	).

get_exportedpreds_exportedops_preprocessor((:- module(ModName,ExportedPreds,
						      ExportedOps,PreProc)),
					   ModName,
					   ExportedOps,
					   ExportedPreds,
					   PreProc) :- ! .
get_exportedpreds_exportedops_preprocessor((:- module(ModName,ExportedPreds)),
					   ModName,
					   [],
					   ExportedPreds,
					   []).
get_exportedpreds_exportedops_preprocessor((:- module(ModName)),
					   ModName,
					   [],
					   [],
					   []).

transformexportlist1([],[]).
transformexportlist1([Atom/Arity|R],[String/Arity|S]) :-
	name(Atom,L), ilist_to_string(L,String),
	transformexportlist1(R,S).

transformexportlist2(atom(_,_),[]).
transformexportlist2(list(El,R,_),[String/Arity|S]) :-
	El = functor(2,"/",[atom(String,_),integer(Arity,_)],_),
	transformexportlist2(R,S).

compiler_op([integer(Prec,_),atom(StrFixity,_),atom(StrName,_)]) :-
	string_to_atom(StrFixity,Fixity),
	string_to_atom(StrName,Name),
	op(Prec,Fixity,Name).	

ignore_decl("module",2,Term) :- compiler_error("Module declaration not first in file",Term).
ignore_decl("sysmodule",2,Term) :- compiler_error("Sysmodule declaration not first in file",Term).
ignore_decl("module",1,Term) :- compiler_error("Module declaration not first in file",Term).
ignore_decl("sysmodule",1,Term) :- compiler_error("Sysmodule declaration not first in file",Term).
ignore_decl("pred",1,_).
ignore_decl("interface",0,_).
ignore_decl("implementation",0,_).
ignore_decl("import_module",1,_).
ignore_decl("import",1,_).
ignore_decl("type",1,_).
ignore_decl("typedef",1,_).
ignore_decl("pragma",1,_).
ignore_decl("--->",2,_).
ignore_decl("export_abstract",1,_).
ignore_decl("export",1,_).
ignore_decl("io",1,_).
ignore_decl("instance",1,_).
ignore_decl("inst",1,_).


collectDirectives([], [], Clauses, Clauses).
collectDirectives([Clause| Other], Directives, NormalClauses, Acc) :-
	 Clause = clause(Directive, Bindings),
	 ( is_declaration(Directive, DeDi,Context) ->
	     ( real_directive(DeDi) ->
		 Directives = [DeDi|OtherDirectives],
		 collectDirectives(Other, OtherDirectives, NormalClauses, Acc)
	     ;
		 collectDirectives(Other, Directives,
				   [clause(functor(1,"?-",[DeDi],Context), Bindings)|
				    NormalClauses], Acc)
	     )
	 ;
	     collectDirectives(Other, Directives, [Clause|NormalClauses], Acc)
	).

real_directive(functor(1,Name,_,_)) :-
	real_directive1(Name).

real_directive1("index").
real_directive1("meta_predicate").
real_directive1("mode").
real_directive1("block").

	

abstractifyBodyDisj(Term, Abstract, Atoms1, Atoms2, Info) :-
	(Term = var(_,C) ->
	    abstractifyBodyDisj(functor(1,"call",[Term],C), Abstract, Atoms1, Atoms2, Info)
	;
	    (Term = functor(2,";",[Disj1,Disj2],_) ->
		(Disj1 = functor(2,"->",_,_) ->
		    Abstract = [['$startDisj'(_)|NewDisj]],
		    abstractifyBody(Term, NewDisj, [], Atoms1, Atoms2, Info)
		;
		    Abstract = [['$startDisj'(_)|OtherDisj]|Disjs],
		    abstractifyBody(Disj1, OtherDisj, [], Atoms1, Atoms3, Info),
		    abstractifyBodyDisj(Disj2, Disjs, Atoms3, Atoms2, Info)
		)
	    ;
		Abstract = [['$startDisj'(_)|NewDisj]],
		abstractifyBody(Term, NewDisj, [], Atoms1, Atoms2, Info)
	    )
	).

fill_size_env(V,_) :- var(V), !.
fill_size_env([],_) :- !.
fill_size_env([I|Is],E) :-
	(
	  I = allocate(E) -> fill_size_env(Is,E)
	;
	  I = call(_,E) -> fill_size_env(Is,E)
	;
	  fill_size_env(Is,E)
	).


has_vars(var(_, _, _)).
has_vars(bin_op(_, X, Y)) :- ( has_vars(X), !; has_vars(Y) ) .
has_vars(functor(_, _, _, _)). % being lazy

splitIsExpressionArg(Op, NewOp, Abstract, Acc) :-
	Op = bin_op(_, _, _), has_vars(Op), !,
	NewOp = var(VV, _, _),
	splitIsExpression(Op, var(VV, _, _), Abstract, Acc).
splitIsExpressionArg(Op, NewOp, Abstract, Acc) :-
	Op = functor(_, _, _, _), !,
	NewOp = var(VV, _, _),
	Abstract = [var(VV, _, _) = Op|Acc].
splitIsExpressionArg(Op, Op, Abstract, Abstract).

splitIsExpression(NewY, NewX, Abstract, Acc) :-
	NewY = bin_op(Name, Op1, Op2), !,
        splitIsExpressionArg(Op1, NewOp1, Abstract, Acc1),
        splitIsExpressionArg(Op2, NewOp2, Acc1, Acc2),
 	Acc2 = [NewX is bin_op(Name, NewOp1, NewOp2)|Acc].
splitIsExpression(NewY, NewX, Abstract, Acc) :-
	NewY = uni_op(Name, Op1), !,
        splitIsExpressionArg(Op1, NewOp1, Abstract, Acc1),
 	Acc1 = [NewX is uni_op(Name, NewOp1)|Acc].
splitIsExpression(NewY, NewX, Abstract, Acc) :-
	NewY = functor(_, _, _, _), !,
		% Avoid moves of registers before the term is created:
 	Abstract = [var(VV, _, _) = NewY, NewX is var(VV, _, _)|Acc].
splitIsExpression(NewY, NewX, Abstract, Acc) :-
 	Abstract = [NewX is NewY|Acc].


abstractifyExpression(var(Var,C), NewTerm, Atoms1, Atoms2) :- !, 
	NewTerm = var(Var, _, C), Atoms1 = Atoms2.
abstractifyExpression(integer(Int,C), NewTerm, Atoms1, Atoms2) :- !,
	NewTerm = integer(Int, C),
	Atoms1 = Atoms2.
abstractifyExpression(float(Float,C), NewTerm, Atoms1, Atoms2) :- !,
	NewTerm = float(Float, C),
	Atoms1 = Atoms2.

abstractifyExpression(functor(2,StrName,[A,B],_), NewTerm, Atoms1, Atoms2) :-
	compiledExpression(StrName), !,
	NewTerm = bin_op(StrName, NA, NB),
	abstractifyExpression(A, NA, Atoms1, Atoms3),
	abstractifyExpression(B, NB, Atoms3, Atoms2).
abstractifyExpression(functor(1,StrName,[A],_), NewTerm, Atoms1, Atoms2) :-
	compiledExpression1(StrName), !,
	NewTerm = uni_op(StrName, NA),
	abstractifyExpression(A, NA, Atoms1, Atoms2).

% some errors
abstractifyExpression(Term,_,_,_) :-
	Term = string(_,_), !,
	compiler_error("String in arithmetic expression",Term),
	fail.
abstractifyExpression(Term,_,_,_) :-
	Term = char(_,_), !,
	compiler_error("Character in arithmetic expression",Term),
	fail.
abstractifyExpression(Term,_,_,_) :- 
	Term = list(_,_,_), !,
	compiler_error("List in arithmetic expression",Term),
	fail.
abstractifyExpression(Term,_,_,_) :-
	Term = atom(_,_), !,
	compiler_error("Atom in arithmetic expression",Term),
	fail.
abstractifyExpression(Term, _,_,_) :-
	compiler_error("Non-supported functor in arithmetic expression",Term),
	fail.

compiledExpression("+").
compiledExpression("-").
compiledExpression("*").
compiledExpression("/").
compiledExpression("//").
compiledExpression("<<").
compiledExpression(">>").
compiledExpression("max").
compiledExpression("min").
compiledExpression("mod").
compiledExpression("\\/").
compiledExpression("/\\").
compiledExpression("xor").
compiledExpression("**").
compiledExpression("^").

compiledExpression1("+").
compiledExpression1("-").
compiledExpression1("round").
compiledExpression1("floor").
compiledExpression1("ceil").
compiledExpression1("trunc").
compiledExpression1("msb").
compiledExpression1("~").
compiledExpression1("\\").
compiledExpression1("sqrt").
compiledExpression1("atan").
compiledExpression1("cos").
compiledExpression1("abs").
compiledExpression1("sin").
compiledExpression1("log").
compiledExpression1("exp").
compiledExpression1("sign").
compiledExpression1("max").
compiledExpression1("len").
compiledExpression1("min").
compiledExpression1("sum").


undoUnsafe(nocall).
undoUnsafe([Ref|_]) :-
	Ref = atomcall(_, _, _, _), ! .
undoUnsafe([Ref|_]) :-
	Ref = functorcall(_, _, Args, _, _),
	undoUnsafeArgs(Args).

undoUnsafeArgs([]).
undoUnsafeArgs([Arg|Args]) :-
	(Arg = var(_, Temp, _), Temp = temp(Dest, _, _), nonvar(Dest) ->
	    setarg(1, Temp, _)
	;
	    true
	),
	undoUnsafeArgs(Args).



collectUnsafe(nocall, []).
collectUnsafe([Ref|_], Unsafe) :-
	Ref = atomcall(_, _, _, _), !, Unsafe = [].
collectUnsafe([Ref|_], Unsafe) :-
	Ref = functorcall(_, _, Args, _, _),
	unsafeArgs(Args, 1, Unsafe).

unsafeArgs([], _, []).
unsafeArgs([Arg|Args], Nr, Unsafe) :-
	unsafeArg(Arg, Nr, Unsafe, Unsafe1),
	Nr1 is Nr + 1,
	unsafeArgs(Args, Nr1, Unsafe1).

unsafeArg(atom(_, _), _, Unsafe, Unsafe).
unsafeArg(functor(_, _, _, _), Nr, [Nr|Unsafe], Unsafe).
unsafeArg(float(_, _), _, Unsafe, Unsafe).
unsafeArg(string(_, _), _, Unsafe, Unsafe).
unsafeArg(char(_, _), _, Unsafe, Unsafe).
unsafeArg(integer(_, _), _, Unsafe, Unsafe).
unsafeArg(list(_,_,_), Nr, [Nr|Unsafe], Unsafe).
unsafeArg(var(_, Info, _), Nr, Unsafe1, Unsafe2) :-
	unsafeArgVar(Info, Nr, Unsafe1, Unsafe2).

unsafeArgVar(void, _, Unsafe, Unsafe).
unsafeArgVar(perm(_, _, _), _, Unsafe, Unsafe).
unsafeArgVar(temp(Dest,_, _), Nr, [Nr|Unsafe], Unsafe) :-
	mymember(Nr, Dest).



peephole(In,Out) :-
	In = C0,
	(specialize -> special_switch(C0,C1) ; C0 = C1),
	(merge -> univoids(C1,C2) ; C1 = C2), % needed here
	(specialize -> spec_select_functor(C2,C6) ; C2 = C6),
	remove_nop_call(C6,C7),
	C7 = C8,
	redundant(C8,C9),
	C9 = Out.

specialize :- nb_getval('%%specialize',X), X == yes.
merge :- nb_getval('%%merge',X), X == yes.

% remove calls to susp_dummy which where only there to force
% saving things to the environment
% do not forget the active_yvars behind it

remove_nop_call([],[]).
remove_nop_call([nop_call(_),_|R],Out) :-
	!,
	remove_nop_call(R,Out).
remove_nop_call([I|R],[I|S]) :-
	remove_nop_call(R,S).


% redundant instructions are removed - for now only
%              get_atom(A,S), put_atom(A,S)

redundant([],[]).
redundant([get_atom(ArgNr,Atom),put_atom(ArgNr,Atom)|R],[get_atom(ArgNr,Atom)|S]) :-
	!,
	redundant(R,S).
redundant([put_atom(ArgNr,Atom),get_atom(ArgNr,Atom)|R],[put_atom(ArgNr,Atom)|S]) :-
	!,
	redundant(R,S).
redundant([I|R],[I|S]) :-
	redundant(R,S).



% N uni_void instructions after each other are compressed to uni_void_n(N); N > 1
% this can't be done later (e.g. in symtab.c) because otherwise getlist_unifies
% and collapse_unifies eat the univoids

univoids([],[]).
univoids([uni_void,X|Rest],Out) :- !,
	(X == uni_void ->
	    univoids([uni_void_2|Rest],Out)
	;
	    Out = [uni_void|S],
	    univoids([X|Rest],S)
	).
univoids([uni_void2,X|Rest],Out) :- !,
	(X == uni_void ->
	    univoids([uni_void_3|Rest],Out)
	;
	    Out = [uni_void_2|S],
	    univoids([X|Rest],S)
	).
univoids([uni_void_2,X|Rest],Out) :- !,
	(X == uni_void ->
	    univoids([uni_void_3|Rest],Out)
	;
	    Out = [uni_void_2|S],
	    univoids([X|Rest],S)
	).
univoids([uni_void_3,X|Rest],Out) :- !,
	(X == uni_void ->
	    univoids([uni_void_n(4)|Rest],Out)
	;
	    Out = [uni_void_3|S],
	    univoids([X|Rest],S)
	).
univoids([uni_void_n(N),X|Rest],Out) :- !,
	(X == uni_void ->
	    M is N + 1,
	    univoids([uni_void_n(M)|Rest],Out)
	;
	    Out = [uni_void_n(N)|S],
	    univoids([X|Rest],S)
	).
univoids([X|R],[X|S]) :-
	univoids(R,S).

% special_switch

% special_switch(I,I) :- ! .

% special_switch is meant to recognize the situation
% switchonlist listlab ... listlab: getlist unifies
%
% it is transformed to
%
% switchonlist_skip(_alloc) newlistlab ... getlist newlistlab unifies
%

special_switch(In,Out) :-
	special_switch3(In,Out1,NewLabel), !,
	deal_with_new_label(Out1,NewLabel,Out).
special_switch(In,In).

/* only for hprolog: */
deal_with_new_label([predicate(A,N)|R],N,[predicate(A,M)|R]) :-
	M is N + 1.


special_switch3([I|R],[NewI|S],NewLabel) :-
	(I = switchonlist(1,L1,L2,L3), special_switch(R,S,L1,L2,L3,NewI,NewLabel) ->
%% 	(I = switchonlist(1,L1,L2,L3), fail, special_switch(R,S,L1,L2,L3,NewI,NewLabel) ->
	    true
	;
	    NewI = I,
	    special_switch3(R,S,NewLabel)
	).

special_switch([Label, allocate(Alloc), getlist(1)|R],Out,ConstLabel,ListLabel,VarLabel,NewSwitch,NewListLabel) :-
	!,
	Label = label(Label1),
	Label1 == ListLabel,
	NewSwitch = switchonlist_skip_alloc(1,Alloc,ConstLabel,NewListLabel,VarLabel),
	Out = [Label,allocate(Alloc), getlist(1),label(NewListLabel)|R].
special_switch([Label,
		getlist(1),unitvar(Arg4),unitvar(1),
		getlist(Arg3), unitval(Arg4), unitvar(Arg3)|R],
	       Out,ConstLabel,ListLabel,VarLabel,NewSwitch,NewListLabel) :-
	!,
	Label = label(Label1),
	Label1 == ListLabel,
	NewSwitch = switchonlist_append(1,Arg3,Arg4,ConstLabel,NewListLabel,VarLabel),
	Out = [Label,getlist(1), unitvar(Arg4), unitvar(1),
	       getlist_tval_tvar(Arg3,Arg4,Arg3),
	       % was getlist(Arg3), unitval(Arg4), unitvar(Arg3),
	       label(NewListLabel)|R].
special_switch([Label,getlist(1)|R],Out,ConstLabel,ListLabel,VarLabel,NewSwitch,NewListLabel) :-
	!,
	Label = label(Label1),
	Label1 == ListLabel,
	NewSwitch = switchonlist_skip(1,ConstLabel,NewListLabel,VarLabel),
	Out = [Label,getlist(1),label(NewListLabel)|R].
special_switch([I|R],[I|S],L1,L2,L3,NewSwitch,NewLabel) :-
	special_switch(R,S,L1,L2,L3,NewSwitch,NewLabel).


% getlist_unifies - does now also getstr + unifies

getlist_unifies([],[]).
getlist_unifies([Inst|Rest], Pil) :- getlist_unifies1(Inst, Rest, Pil).

getlist_unifies1(get_structure(R0,Sym), [I1,I2|Rest],
	      [getstr_tvar_tvar(R0,R1,R2,Sym)|OptRest]) :-
	uni_tvar_comb(I1,I2,R1,R2),
	!,
	getlist_unifies(Rest, OptRest).
getlist_unifies1(Inst, Rest, [Inst|OptRest]) :- 
	getlist_unifies(Rest, OptRest).


spec_select_functor([],[]).
spec_select_functor([cases(Fail,_,5),
		     arglabel(SC1,F1,L1),
		     arglabel(SC2,F2,L2),
		     arglabel(SC3,F3,L3),
		     arglabel(SC4,F4,L4),
		     arglabel(SC5,F5,L5),
		     switchonbound(1,_,_,Varl)|Rest],
		    [select_functor5(What,F1,L1,F2,L2,F3,L3,F4,L4,F5,L5,Varl,Fail)|Rest]) :-
        make_select_functor_what([SC5,SC4,SC3,SC2,SC1],0,What),
        ! .
spec_select_functor([cases(Fail,_,4),
		     arglabel(SC1,F1,L1),
		     arglabel(SC2,F2,L2),
		     arglabel(SC3,F3,L3),
		     arglabel(SC4,F4,L4),
		     switchonbound(1,_,_,Varl)|Rest],
		    [select_functor4(What,F1,L1,F2,L2,F3,L3,F4,L4,Varl,Fail)|Rest]) :-
        make_select_functor_what([SC4,SC3,SC2,SC1],0,What),
        ! .
spec_select_functor([cases(Fail,_,3),
		     arglabel(SC1,F1,L1),
		     arglabel(SC2,F2,L2),
		     arglabel(SC3,F3,L3),
		     switchonbound(1,_,_,Varl)|Rest],
		    [select_functor3(What,F1,L1,F2,L2,F3,L3,Varl,Fail)|Rest]) :-
        make_select_functor3_what(SC1,SC2,SC3,What),
        ! .
spec_select_functor([cases(Fail,_,2),
		     arglabel(SC1,F1,L1),
		     arglabel(SC2,F2,L2),
		     switchonbound(1,_,_,Varl)|Rest],
		    [select_functor2(What,F1,L1,F2,L2,Varl,Fail)|Rest]) :-
        make_select_functor2_what(SC1,SC2,What),
        ! .
spec_select_functor([X|R],[X|S]) :-
        spec_select_functor(R,S).

make_select_functor_what([],I,I).
make_select_functor_what([W|R],I,O) :-
	s_c(W,V),
	II is 2*I + V,
	make_select_functor_what(R,II,O).


make_select_functor3_what(A,B,C,What) :-
        s_c(A,AA),
        s_c(B,BB),
        s_c(C,CC),
        What is 4*CC + 2*BB + AA.

make_select_functor2_what(A,B,What) :-
        s_c(A,AA),
        s_c(B,BB),
        What is 2*BB + AA.

s_c(s,1).
s_c(c,0).


/* register.pro : handles information about register allocation
                  and permanent variables as well

  This structure has the format
            registerInfo(TempAlloc, Free, Unsafe, PermAlloc)

	parameters:
           TempAlloc:  a list with each element information on 
                       one temporary variable:
		i(TVar, Assigned, Isin)
                        TVar: the temporary variable itself.
                        Assigned: the list of registers the temporary    
                                  variables is in, this registers are
                                  contain this temporay variable in next call
                        Isin: the list of variables the temporary variable is
                              known to be in, and not member of Assigned
            Free: The registers not used at the moment
            Unsafe: The argumentpositions in the next call containing
                    temporary variables, these registers must be avoided
                    when a arbitrary register is needed
            PermAlloc: All permanent cuurently alive. A list with for
                       each such permanent variable:  
                 i(PVar, Assigned, Isin)
                      PVar: The permanent variable itself.
                      Assigned: If empty list, the permanent variable has 
                                not been stored, Isin cannot be empty, 
                                otherwise a list with one element, the
                                number of the permanent variable.
                      Isin: A list of register, of whichthe permanent variable
                            is known to be in.
            PermNr: The first number to be used for a permanent variable
*/


is_new_var(temp(_, _, V)) :- var(V).
is_new_var(perm(_, _, V)) :- var(V).


% Make a new register-information data-structure
newRegisterInfo(Arity, Unsafe,
		registerInfo([],['..'(Arity1)], Unsafe, [],2, [], [])) :-
	Arity1 is Arity + 1.

addUnsafeReg(RI1, R) :-
	arg(7, RI1, RegUnsafe),
	setarg(7, RI1, [R|RegUnsafe]).

addUnsafeRegs(_, []).
addUnsafeRegs(RI1, [R|L]) :-
	addUnsafeReg(RI1, R),
	addUnsafeRegs(RI1, L).

noMoreUnsafeRegs(RI1) :-
	setarg(7, RI1, []).

copyRI(registerInfo(RegAlloc, Free, Unsafe, PermAlloc,PermCount,
		    PermUnsafe, RegUnsafe),
       registerInfo(RegAlloc, Free, Unsafe, PermAlloc,PermCount,
		    PermUnsafe, RegUnsafe)).
       

adaptRI(RI1, Ref) :-
	setarg(1, RI1, []),
	setarg(2, RI1, ['..'(1)]), 
	arg(4, RI1, PermAlloc),
	setarg(4, RI1, PermAlloc1),
	setarg(7, RI1, []),
	removeIsinFromPermAlloc(PermAlloc, PermAlloc1),
	setarg(3, RI1, Unsafe),
	collectUnsafe(Ref, Unsafe).

conc([], X, X).
conc([X|Y], Z, [X|W]) :-
	conc(Y, Z, W).

newRIForb(RI1, Ref, Forb, RI2) :-
	arg(4, RI1, PermAlloc),
	arg(5, RI1, PermNr),
	arg(6, RI1, FPNrs),
	removeIsinFromPermAlloc(PermAlloc, PermAlloc1),
	collectUnsafe(Ref, Unsafe),
	conc(FPNrs, Forb, FPNrs1),
	removeSmaller(FPNrs1, PermNr, FPNrs2),
	RI2 = registerInfo([],['..'(1)], Unsafe, PermAlloc1, PermNr,FPNrs2,[]).

removeSmaller([], _, []).
removeSmaller(OrigList, Nr, NewList):-
	OrigList = [Head|Tail],
	( Head < Nr ->
	    removeSmaller(Tail, Nr, NewList)
	;
	    NewList = OrigList
	).


copyRIForb(RI1, RefA, Ref, Forb, RI2) :-
	RI1= registerInfo(RegAlloc0, Free0, _, PermAlloc0,PermNr, FPNrs, _),
	undoUnsafe(RefA),
	collectUnsafe(Ref, Unsafe),
	conc(FPNrs, Forb, FPNrs1),
	pruneRegAlloc(RegAlloc0, RegAlloc1, Free0, Free1),
	resetPermAlloc(PermAlloc0, PermAlloc1, Forb),
	RI2 = registerInfo(RegAlloc1,Free1,Unsafe,PermAlloc1,PermNr,FPNrs1,[]).

resetPermAlloc([], [], _).
resetPermAlloc([Info0|PermAlloc0], [Info1|PermAlloc1], Forb) :-
	Info0 = i(PVar, Assigned, _),
	(Assigned = [] ->
	    PVar = perm(Nr, _, _),
	    (mymember(Nr, Forb) ->
		Info1 = Info0
	    ;
		Info1 = Info0,
		setarg(1, PVar, _)
	    )
	;
	    Info1 = Info0
	),
	resetPermAlloc(PermAlloc0, PermAlloc1, Forb).



pruneRegAlloc([], [], Free, Free).
pruneRegAlloc([Reggie|Reggies], Regs, Free0, Free1) :-
	Reggie = i(TVar, Isin),
	TVar = temp(_, Len, Var),
	(Len == Var ->
	    moveSetToFree(Isin, Free0, Free2),
	    Regs = Other
	;
	    Free2 = Free0,
	    Regs = [Reggie|Other]
	),
	pruneRegAlloc(Reggies, Other, Free2, Free1).


removeIsinFromPermAlloc([], []).
removeIsinFromPermAlloc([i(PVar, Assign, _)|PermAlloc], 
			[i(PVar, Assign1, [])|PermAlloc1]) :-
	(Assign = [] -> Assign1 = [N], arg(1, PVar, N); Assign1 = Assign),
	removeIsinFromPermAlloc(PermAlloc, PermAlloc1).


checkDeadTermTemp(TVar, Term, RI1) :-
	TVar = temp(_, Len, Var),
	(var(Var) ->
	    Len1 is Len - 1,
	    ( Len1 > 1 ->
	       setarg(2, TVar, Len1)
	    ;
	        setarg(2, Term, void)
	    )
	;
	    tempOccurred(TVar, RI1)
	).
	    
% Change information after another occurrence of a temporary variable
tempOccurred(TVar, RI1) :-
	TVar = temp(_, Len, Var),
	New is Var + 1,
	setarg(3, TVar, New),
	(New < Len ->
	    true
	;
	    moveIsinToFree(TVar, RI1)
	).
notLast(TVar) :-
	arg(2, TVar, Len),
	arg(3, TVar, Var),
	Len - 1 > Var.


/* the permanent variable his stil alive: there is at least one occurence in
the code to compile
*/

alivePVar(PVar):-
	arg(2, PVar, Len),
	arg(3, PVar, Count),
	Count < Len .


checkDeadTermPerm(PVar, Term, RI1) :-
	PVar = perm(_, Len, Var),
	(var(Var) ->
	    Len1 is Len - 1,
	    ( Len1 > 1 ->
		setarg(2, PVar, Len1)
	    ;
               setarg(2, Term, void)
	    )
		
	;
	    permOccurred(PVar, RI1)
	).
	 

% Change information after annother occurrence of a permanent variable
permOccurred(PVar, RI1) :-
	PVar = perm(_, Len, Var),
	New is Var + 1,
	setarg(3, PVar, New),
	(New < Len ->
	    true
	;
	    moveIsinToFreePerm(PVar, RI1)
	).

setMaxPerm(MaxPerm, RI) :- 
	setarg(5, RI, MaxPerm),
	arg(6, RI, FPNrs),
	setarg(6, RI, NewFPNrs),
	pruneFPNrs(FPNrs, MaxPerm, NewFPNrs).

pruneFPNrs([], _, []).
pruneFPNrs(Forbs, Max, New) :-
	Forbs = [X|Xs],
	(X < Max ->
	    pruneFPNrs(Xs, Max, New)
	;
	    New = Forbs
	).


getMaxPerm(RI, MaxPerm) :-
	arg(5, RI, MaxPerm).


getForbiddenPerm(RIOld, RINew, OldForb, NewForb) :-
	arg(4, RIOld, PermAllocOld),
	arg(4, RINew, PermAllocNew),
	checkAlloc(PermAllocNew, PermAllocOld , OldForb, NewForb).

checkAlloc([], _, Forb, Forb).
checkAlloc([Perm|Perms], OldAlloc, Forbs1, Forbs2) :-
	arg(1, Perm, PVar),
	( mymember(i(PVar, Assign, _), OldAlloc), Assign = [_|_] ->
	    checkAlloc(Perms, OldAlloc, Forbs1, Forbs2)
	;
	    arg(1, PVar, Nr),
	    insertSorted(Forbs1, Nr, Forbs3),
	    checkAlloc(Perms, OldAlloc, Forbs3, Forbs2)
	).

checkPermCrea(PermCrea, Forb, RI):-
	/* first collect those newly created permanent variables in
	the RI of the branches */
	findExtraPermVarsForRI(PermCrea, Forb, ToAdd),
	/* prune dead permanent variables */
	pruneDeadVariables(Forb, ToAdd,NewForb, NewToAdd),
	/* add extra instructions where needed */
	checkPermCrea1(PermCrea, NewForb),
	/* add the permanentr variables created during disjunction to
	the RI
	*/
	arg(4, RI, PermAlloc),
	setarg(4, RI, PermAllocNew),
	conc(NewToAdd, PermAlloc, PermAllocNew).

pruneDeadVariables([], [], [], []).
pruneDeadVariables([Nr|Forb], [Add|Adds], NewForb, NewToAdd):-
	arg(1, Add, Perm),
	(alivePVar(Perm) ->
	    NewForb = [Nr|Forb1],
	    NewToAdd = [Add|Adds1],
	    pruneDeadVariables(Forb, Adds, Forb1, Adds1)
	;
	    pruneDeadVariables(Forb, Adds, NewForb, NewToAdd)
	).

findExtraPermVarsForRI([], _, _).
findExtraPermVarsForRI([permCrea(RI, _, _)|PermCrea], Forb, ToAdd):-
	arg(4, RI, PermAlloc),
	findExtraPermVarsForRI1(Forb, ToAdd, PermAlloc),
	findExtraPermVarsForRI(PermCrea, Forb, ToAdd).


findExtraPermVarsForRI1([], [], _).
findExtraPermVarsForRI1([Nr|Forb], [Add|ToAdd], PermAlloc):-
	(var(Add), creaMember(Nr, PermAlloc, Perm) ->
	    Add = i(Perm, [Nr], [])
	;
	    true
	),
	findExtraPermVarsForRI1(Forb, ToAdd, PermAlloc).


checkPermCrea1([], _).
checkPermCrea1([permCrea(RI, Is1, Is2)|PermCrea], Forb) :-
	arg(4, RI, PermAlloc),
        checkPermCreaForb(Forb, PermAlloc, Is1, Is2),
	checkPermCrea1(PermCrea, Forb).

checkPermCreaForb([], _, Is, Is).
checkPermCreaForb([Nr|Forb],  PermAlloc, Is1, Is2):-
	( creaMember(Nr, PermAlloc, _) ->
	    Is1 = Is3
	;
	    Is1 = [bldpvar(Nr)|Is3]
	),
	checkPermCreaForb(Forb, PermAlloc, Is3, Is2).


creaMember(Nr, [PP|PermAlloc], Perm) :-
	arg(1, PP, PVar),
	arg(1, PVar, Nr1),
	(Nr == Nr1 ->
	    Perm = PVar
	;
	    creaMember(Nr, PermAlloc, Perm)
	).

insertSorted([], Nr, [Nr]).
insertSorted(Old, Nr, New) :-
	Old = [Nr1|Nrs],
	(Nr1 < Nr ->
	    New = [Nr1|New1],
	    insertSorted(Nrs, Nr, New1)
	;
	    ( Nr = Nr1 -> New = Old;
		New = [Nr|Old]
	    )
	).

% Add information on a permanent variable, occuring for the first time
newPermVar(PVar, Assigned, Isin, RI1) :-
        (Assigned = [] ->
	    true /* The permanent variable is currently
	            stored in a register */
	;
	    getPermVarNr(RI1, PermNr)
	),
	arg(4, RI1, RegAlloc),
	arg(3, PVar, 1), 
	arg(1, PVar, PermNr),
	RegAlloc1 = [i(PVar, Assigned, Isin)|RegAlloc],
	setarg(4, RI1, RegAlloc1).
	
newPermVarDisj(PVar, Assigned, Isin, RI1) :-
	arg(4, RI1, RegAlloc),
	RegAlloc1 = [i(PVar, Assigned, Isin)|RegAlloc],
	setarg(4, RI1, RegAlloc1).

getPermVarNr(RI1, PermNr) :-
	arg(5, RI1, PermNr1),
	arg(6, RI1, FPNrs),
	PermNr2 is PermNr1 + 1,
	setarg(5, RI1, PermNr2),
	( FPNrs = [PermNr1|FPNrs1] ->
	    setarg(6, RI1, FPNrs1),
	    getPermVarNr(RI1, PermNr)
	;
	    PermNr = PermNr1
	).
	

addToIsin(Nr, Isin, Isin1) :-
	 addToSet(Nr, Isin, Isin1).
	 
addToFreeInRI([], _) :- ! .
%% addToFreeInRI(Regs, RI1) :-
%% 	arg(2, RI1, Free),
%% 	setarg(2, RI1, Free1),
%% 	addListToFree(Regs, Free, Free1).
%% 26-12-2003
addToFreeInRI(Regs, RI1):-
	/* first remove from Regs, all regs that are still in use */
	arg(1, RI1, Allocated),
	arg(4, RI1, PermAlloc),
	removeRegsUsedInPermAlloc(Regs, PermAlloc, Regs0),
	removeRegsUsedInTempAlloc(Regs0, Allocated, Regs1),
	
	/* add them to the Free-list */
	arg(2, RI1, Free),
	setarg(2, RI1, Free1),
	addListToFree(Regs1, Free, Free1).

addListToFree([], Free, Free).
addListToFree([Reg|Regs], Free, Free1) :-
	addToFree(Reg, Free, Free2),
	addListToFree(Regs, Free2, Free1).

addToFree(Nr, RegFree, RegFree1) :-
	addToSet(Nr, RegFree, RegFree1).
	 

addToSet(Nr, [], [Nr]).
addToSet(Nr, ['..'(X)], NewSet) :- !,
	(Nr is X - 1 -> NewSet = ['..'(Nr)];
	 NewSet = [Nr, '..'(X)]
	).

addToSet(Nr, [X|Y], NewSet) :-
	(Nr > X -> addToSet(Nr, Y, NewSet1), addToSet(X, NewSet1, NewSet);
	 NewSet = [Nr, X|Y]
	).

% Temporary variable Var is contained in register I now.
addToRI(I, Var, RI1) :-
	arg(1, RI1, Allocated),
	setarg(1, RI1, Allocated1),
	addToVarReg(I, Var, Allocated, Allocated1).

% Permanent variable Var is contained in register (or environment position) 
% I  now.
addToRIPerm(I, Var, RI1) :-
	arg(4, RI1, PermAlloc),
	setarg(4, RI1, PermAlloc1),
	addToVarRegPerm(I, Var, PermAlloc, PermAlloc1).

addToVarReg(I, Var, [], RI) :- !, RI = [i(Var, [I])]. 
addToVarReg(I, Var, [First|Other], NewList) :-
        First = i(Var1, Isin),
	( Var == Var1 ->
	    addToIsin(I, Isin, Isin1),
	    NewList = [i(Var1, Isin1)|Other]
	;   
	    NewList = [First| NewOther],
	    addToVarReg(I, Var, Other, NewOther)
	).

addToVarRegPerm(I, Var, [], RI) :- !, RI = [i(Var, [], [I])]. 
addToVarRegPerm(I, Var, [First|Other], NewList) :-
        First = i(Var1, Assigned, Isin),
	( Var == Var1 ->
	    addToIsin(I, Isin, Isin1),
	    NewList = [i(Var1, Assigned, Isin1)|Other]
	;   
	    NewList = [First| NewOther],
	    addToVarRegPerm(I, Var, Other, NewOther)
	).

/* findReg/3 find a register which was allocated to the temporary variable
*/
findReg(RI, Var, Reg) :-
	arg(1, RI, Allocated),
	findReg2(Allocated, Var, Reg).

findReg2([i(Var1, Isin)|Other], Var, Reg) :-
	(Var == Var1 ->
           memberIsin(Reg, Isin)
        ;
	   findReg2(Other, Var, Reg)
	).

findRegPerm2([], _, notfound).
findRegPerm2([i(Var1, _, Isin)|Other], Var, Reg) :-
	(Var == Var1 ->
	    Isin = [_|_],
	    Reg = Isin
        ;
	    findRegPerm2(Other, Var, Reg)
	).

findRegPermCheck(RI, Var, Reg) :-
	arg(4, RI, Allocated),
	findRegPerm2(Allocated, Var, Reg).


replaceIsin(_, _, [], []).
replaceIsin(Reg1, Reg2, [F1|Allocated1], [F2|Allocated2]):-
	(F1 = i(Var, [Reg1]) ->
	    F2 = i(Var, [Reg2]),
	    replaceIsin(Reg1, Reg2, Allocated1,Allocated2)
	;
	    F1 = F2,
	    replaceIsin(Reg1, Reg2, Allocated1, Allocated2)
	).

removeIsin2(Var, [First| Allocated], Allocated1, Isin1) :-
	First = i(Var1, Isin),
	(Var == Var1 ->
            Allocated1 = Allocated,
	    Isin1 = Isin
        ;
	    Allocated1 = [First|Allocated3], 
            removeIsin2(Var, Allocated, Allocated3, Isin1)
	).
removeIsinPerm2(Var, [First| Allocated], Allocated1, Isin1) :-
	First = i(Var1, _, Isin),
	(Var == Var1 ->
            Allocated1 = Allocated,
	    Isin1 = Isin
        ;
	    Allocated1 = [First|Allocated3], 
            removeIsinPerm2(Var, Allocated, Allocated3, Isin1)
	).

moveIsinToFree(Var, RI) :-
	arg(1, RI, Allocated),
	removeIsin2(Var, Allocated, Allocated1, Isin),
	setarg(1, RI, Allocated1),
	arg(4, RI, PermAlloc),
	removeRegsUsedInPermAlloc(Isin, PermAlloc, NewIsin0),
	removeRegsUsedInTempAlloc(NewIsin0, Allocated1, NewIsin),
	(
	  NewIsin == [] -> true
	;
	  arg(2, RI,Free),
	  setarg(2, RI, Free1),
	  moveSetToFree(NewIsin, Free, Free1)
	).

removeRegsUsedInPermAlloc([], _, New) :- !, New = [].
removeRegsUsedInPermAlloc(Isin, [], New) :- !, New = Isin.
removeRegsUsedInPermAlloc(Isin, [i(_, _, ToRemove)| Other], New) :-
	set_difference_unsorted(Isin, ToRemove, Isin1),
	removeRegsUsedInPermAlloc(Isin1, Other, New).

removeRegsUsedInTempAlloc([], _, New) :- !, New = [].
removeRegsUsedInTempAlloc(Isin, [], New) :- !, New = Isin.
removeRegsUsedInTempAlloc(Isin, [i(_,ToRemove)| Other], New) :-
	set_difference_unsorted(Isin, ToRemove, Isin1),
	removeRegsUsedInTempAlloc(Isin1, Other, New).
	
set_difference_unsorted(Set, [], Result) :- !, Result = Set.
set_difference_unsorted(Set, [Head|Tail], Result) :-
	(set_member_unsorted(Head, Set, Set1) ->
	    Set2 = Set1
	;
	    Set2 = Set
	),
	set_difference_unsorted(Set2, Tail, Result).

set_member_unsorted(Head, [El|Set], New) :-
	(Head == El ->
	    New =  Set
	;
	    New = [El|New1],
	    set_member_unsorted(Head, Set, New1)
	).


moveIsinToFreePerm(Var, RI1):-
	arg(4, RI1, PermAlloc),
	setarg(4, RI1, PermAlloc1),
	removeIsinPerm2(Var, PermAlloc, PermAlloc1, Isin),
	arg(1, RI1, Allocated),
	removeRegsUsedInPermAlloc(Isin, PermAlloc1, NewIsin0),
	removeRegsUsedInTempAlloc(NewIsin0, Allocated, NewIsin),
	(
	  NewIsin == [] -> true
	;
	  
	  arg(2, RI1, Free),
	  setarg(2, RI1, Free1),
	  moveSetToFree(NewIsin, Free, Free1)
	).

moveSetToFree([], Free, Free).
moveSetToFree([I|Y], Free, Free1) :-
	addToFree(I, Free, Free2),
	moveSetToFree(Y, Free2, Free1).


addFreeRI(I, RI) :-
	arg(2, RI, Free),
	setarg(2, RI, Free1),
	addToFree(I, Free, Free1).


firstUseTemp(TVar) :-
	arg(3, TVar, 1). /* setarg */


findFreeRegAlways(TVar, Dest, Reg, RI1, Is1, Is3) :-
	arg(3, TVar, 1), /* setarg */
	( findFreeReg(Dest, Reg, RI1,  [], Is1, Is3) ->
	    addToRI(Reg, TVar, RI1)
	;   
	    Is1 = Is3,
	    findSaveRegister(Reg, RI1),
	    addToRI(Reg, TVar, RI1)
	).

findFreeReg(Dest, Reg, RI1, _, Is1, Is2) :-
	arg(2, RI1, Free), 
	normalmember(Reg, Dest),
	freemember(Reg, Free, Free1), !,
	Is1 = Is2,
	setarg(2, RI1, Free1).

findFreeReg(Dest, Reg, RI1, Prev, Is1, Is2) :-
	arg(1, RI1, Allocated),
	normalmember(Reg, Dest),
	notmember(Reg, Prev),
	findFreeReg2(Allocated, A, A, Reg, RI1, Prev, Is1, Is2).


findFreeReg2([], [], A2, Reg, RI1, _, Is1, Is2) :- 
	arg(4, RI1, PermAlloc),
	findFreeReg3(PermAlloc, A, Reg, RI1, Is1, Is2),
	setarg(4, RI1, A),
	setarg(1, RI1, A2).
findFreeReg2([First|Allocated], A1, A2, Reg, RI1, Prev, Is1, Is2) :-
	First = i(Var, Isin),
	memberIsin(Reg, Isin, Isin1), !,
	A1 = [i(Var, Isin1)|Allocated],
	Var = temp(Dest, _, _),
	( Isin1 == [] ->
	    (regUsedElse(Allocated, Reg) ->
		/* compiler_warning(Problem in findFreeReg2 - register to be moved used twice,nocontext), */
		fail
	    ;
		true
	    ),
	    normalmember(Reg2, Dest),
	    arg(2, RI1, Free),
	    arg(7, RI1, RegUnsafe),
	    notmember(Reg2, RegUnsafe),
	    (freemember(Reg2, Free, Free1) ->
		setarg(1, RI1, A2),
		setarg(2, RI1, Free1),
		Is1 = [movreg(Reg, Reg2)|Is2]
	    ;
		notmember(Reg2, [Reg| Prev]),
		findFreeReg2(A2, NA, NA, Reg2, RI1, [Reg| Prev], Is1, 
			                        [movreg(Reg, Reg2)| Is2])
	    ),
	    replaceRegInIsin(Reg, Reg2, RI1),
	    addToRI(Reg2, Var, RI1)
	;   
	    Is1 = Is2,
	    setarg(1, RI1, A2)
        ).

findFreeReg2([F|Allocated], [F|A1], A2, Reg, RI1, Prev, Is1, Is2) :-
	findFreeReg2(Allocated, A1, A2, Reg, RI1, Prev, Is1, Is2).

findFreeReg3([First|Allocated], A1, Reg, RI, Is1, Is2) :-
	First = i(Var, Assigned, Isin),
	memberIsin(Reg, Isin, Isin1), !,
	A1 = [i(Var, Assigned1, Isin1)|Allocated],
	( Assigned == [] ->
		Var = perm(Nr, _, _),
	        (var(Nr) -> getPermVarNr(RI, Nr); true),
		Assigned1 = [Nr],
		Is1 = [getpvar(Nr, Reg)|Is2]
	;
		Assigned1 = Assigned,
		Is1 = Is2
	).
findFreeReg3([F|Allocated], [F|A1], Reg, RI, Is1, Is2) :-
	findFreeReg3(Allocated, A1, Reg, RI, Is1, Is2).


regUsedElse([First|_], Reg) :-
	First = i(_, Isin),
	memberIsin(Reg, Isin, _), ! .
regUsedElse([_|Allocated], Reg) :-
	regUsedElse(Allocated, Reg).

findFreeRegCall2([], [], A2, Reg, RI1, _, Is1, Is2) :- 
	arg(4, RI1, PermAlloc),
	findFreeReg3(PermAlloc, A, Reg, RI1, Is1, Is2),
	setarg(4, RI1, A),
	setarg(1, RI1, A2).
findFreeRegCall2([First|Allocated], A1, A2, Reg, RI1, Prev, Is1, Is2) :-
	First = i(Var, Isin),
	memberIsin(Reg, Isin, Isin1), !, /* we found our register */
	A1 = [i(Var, Isin1)|Allocated],
	Var = temp(Dest, _, _),
	( Isin1 == [] ->
	    (regUsedElse(Allocated, Reg) ->
		compiler_warning("Problem in findFreeRegCall2 - register to be moved used twice",nocontext),
		fail
	    ;
		true
	    ),
	    (var(Dest) ->
		/* since we are handling a call, and all functors
		   and list have been handled. And we do not need
	           the register in an argument (Dest is a variable).
	           The reason we get here is because the variable
	           occurs in another branch of a disjunction.
	           Thus we can just take the register */
		Is1 = Is2,
		setarg(1, RI1, A2)
	    ;
		normalmember(Reg2, Dest),
		arg(2, RI1, Free),
		(freemember(Reg2, Free, Free1) ->
		    setarg(1, RI1, A2),
		    setarg(2, RI1, Free1),
		    Is1 = [movreg(Reg, Reg2)| Is2]
		;
		    notmember(Reg2, [Reg| Prev]),
		    findFreeRegCall2(A2, NA, NA, Reg2, RI1, [Reg| Prev], Is1, 
				     [movreg(Reg, Reg2)| Is2])
		),
		replaceRegInIsin(Reg, Reg2, RI1),
		addToRI(Reg2, Var, RI1)
	    )
	;
	    /* we still have another register to count on! */
	    Is1 = Is2,
	    setarg(1, RI1, A2)
        ).

findFreeRegCall2([F|Allocated], [F|A1], A2, Reg, RI1, Prev, Is1, Is2) :-
	findFreeRegCall2(Allocated, A1, A2, Reg, RI1, Prev, Is1, Is2).


replaceRegInIsin(Reg1, Reg2, RI) :-
	arg(1, RI, TempAlloc),
	replaceInIsinTemp(Reg1, Reg2, TempAlloc, NewTempAlloc),
	setarg(1, RI, NewTempAlloc),
	arg(4, RI, PermAlloc),
	replaceInIsinPerm(Reg1, Reg2, PermAlloc, NewPermAlloc),
	setarg(4, RI, NewPermAlloc).


replaceInIsinTemp(_, _, [], []).
replaceInIsinTemp(Reg1, Reg2, [A|Alloc], [A1|Alloc1]) :-
	A = i(Var, Isin),
	(memberReplace(Reg1, Reg2, Isin, Isin1) ->
	    A1 = i(Var, Isin1)
	;
	    A1 = A
	),
	replaceInIsinTemp(Reg1, Reg2, Alloc, Alloc1).
	
replaceInIsinPerm(_, _, [], []).
replaceInIsinPerm(Reg1, Reg2, [A|Alloc], [A1|Alloc1]) :-
	A = i(Var, P, Isin),
	(memberReplace(Reg1, Reg2, Isin, Isin1) ->
	    A1 = i(Var, P, Isin1)
	;
	    A1 = A
	),
	replaceInIsinPerm(Reg1, Reg2, Alloc, Alloc1).


findSaveRegister(Reg, RI1) :-
	arg(2, RI1, Free),
	arg(3, RI1, Unsafe),
	arg(7, RI1, RegUnsafe),
	normalsetmember(Reg, Free, Free1),
	notmember(Reg, Unsafe),
	notmember(Reg, RegUnsafe), !, 
	setarg(2, RI1, Free1).


stealSaveRegister(Reg, RI, Used) :-
	arg(2, RI, Free),
	arg(3, RI, Unsafe),
	normalsetmember(Reg, Free),
	notmember(Reg, Unsafe),
	notmember(Reg, Used), ! .
	

findSaveRegister(Reg, Free, Unsafe, RegUnsafe, Free1) :-
	normalsetmember(Reg, Free, Free1),
	notmember(Reg, Unsafe),  
	notmember(Reg, RegUnsafe), ! . 

freeRegister(Reg, Is1, Is2, RI1) :-
	arg(2, RI1, Free),
        (freemember(Reg, Free, Free1) ->
	    setarg(2, RI1, Free1),
	    Is1 = Is2
	;
	    arg(1, RI1, Allocated),
	    (findFreeReg2(Allocated, A, A, Reg, RI1, [], Is1, Is2) -> true
	    ;
		arg(3, RI1, Unsafe),
		arg(7, RI1, RegUnsafe),
		findSaveRegister(Reg2, Free, Unsafe, RegUnsafe, Free1),
		Is1 = [movreg(Reg, Reg2)|Is2],
		replaceIsin(Reg, Reg2, Allocated, Allocated1),
	        setarg(1, RI1, Allocated1),
		setarg(2, RI1, Free1)
	    ) 
	).

/* freeRegisterCall will free a register, with the
    assumption that, if a register is occupied by a
    temporary variable that does not occur as an argument
    in the call, we can steal the register: could happen
    in disjunctions */
freeRegisterCall(Reg, Is1, Is2, RI1) :-
	arg(2, RI1, Free),
        (freemember(Reg, Free, Free1) ->
	    setarg(2, RI1, Free1),
	    Is1 = Is2
	;
	    arg(1, RI1, Allocated),
	    (findFreeRegCall2(Allocated, A, A, Reg, RI1, [], Is1, Is2) -> true
	    ;
		arg(3, RI1, Unsafe),
		arg(7, RI1, RegUnsafe),
		findSaveRegister(Reg2, Free, Unsafe, RegUnsafe, Free1),
		Is1 = [movreg(Reg, Reg2)|Is2],
		replaceIsin(Reg, Reg2, Allocated, Allocated1),
	        setarg(1, RI1, Allocated1),
		setarg(2, RI1, Free1)
	    ) 
	).          


%% freeInRI(I, RI) :-
%% 	arg(2, RI, Free),
%% 	freemember(I, Free, Free1), !,
%% 	setarg(2, RI, Free1).
%% 26-12-2003

freeInRI(I, RI):-
	arg(2, RI, Free),
	(freemember(I, Free, Free1) ->
	    setarg(2, RI, Free1)
	;
	    true
	).


freemember(I, Global, Global1) :-
	setmember(I, Global, Global1).


setmember(I, ['..'(I)], ['..'(J)]) :- !, J is I + 1.
setmember(I, ['..'(J)], [J|Other]) :- !, 
	I > J,
	K is J + 1,
	setmember(I, ['..'(K)], Other).
setmember(I, [I|Other], Other) :- ! .
setmember(I, [J|Other], [J| Other1]) :-
	setmember(I, Other, Other1).


normalsetmember(I, ['..'(I)], ['..'(J)]) :-
	J is I + 1,
	( I > 250 -> notify_yvar_usage(I); true).
normalsetmember(I, ['..'(J)], [J|Other]) :- !,
	K is J + 1,
	normalsetmember(I, ['..'(K)], Other).
normalsetmember(I, [I|Other], Other).
normalsetmember(I, [J|Other], [J| Other1]) :-
	normalsetmember(I, Other, Other1).


normalsetmember(I, ['..'(I)]) :-
	( I > 250 -> notify_yvar_usage(I); true).
normalsetmember(I, ['..'(J)]) :- !, 
	K is J + 1,
	normalsetmember(I, ['..'(K)]).
normalsetmember(I, [I|_]).
normalsetmember(I, [_|Other]) :-
	normalsetmember(I, Other).

notify_yvar_usage(I) :-
	mybrecorded(yvar_usage, J), !,
	(I > J ->
	    mybrerecord(yvar_usage, I)
	;
	    true
	).
notify_yvar_usage(I) :-
	brerecord(yvar_usage, I).

	    

memberIsin(X, Reg) :-
	 mymember(X, Reg).
memberIsin(X, Reg, Reg1) :-
	 mymember(X, Reg, Reg1).

mymember(X, [Y|W]) :-
	(X = Y ->
	    true
	;
	    mymember(X, W)
	).

mymember(X, [X|Y], Y) :- ! .
mymember(X, [W|Y], [W|Z]) :-
	mymember(X, Y, Z).

normalmember(_, []) :- !, fail.
normalmember(X, [X|_]).
normalmember(X, [_|Y]) :-
	normalmember(X, Y).

notmember(_, []).
notmember(X, [X|_]) :- !, fail.
notmember(X, [_|Y]) :- notmember(X, Y).


collectPermanentAndSaveRI(RI, Yvars, Is1, Is2) :-
	arg(4, RI, PermAlloc),
	setarg(4, RI, PermAlloc2),
	collectPermanentAndSaveRI2(PermAlloc, PermAlloc2, Yvars, Is1, Is2, RI).

collectPermanentAndSaveRI2([], [], [], Is1, Is1, _).
collectPermanentAndSaveRI2([Info|PermAlloc], PermAlloc0,
			 Yvars0, Is1, Is2, RI) :-
        Info = i(PVar, Assigned, Isin),
	PVar = perm(Yvar, Len, Count),
	(Len == Count -> /* The permanent variable is dead */
	    Yvars0 = Yvars,
	    Is1 = Is3,
	    PermAlloc0 = PermAlloc2
	;
	    Yvars0 = [Yvar|Yvars],
	    PermAlloc0 = [Info2|PermAlloc2], 
	    ( Assigned = [] ->
		(var(Yvar) -> getPermVarNr(RI, Yvar); true),
		Isin = [Reg|_],
		Is1 = [getpvar(Yvar, Reg)|Is3],
		Info2 = i(PVar, [Yvar], Isin)
	    ;
		Is1 = Is3, Info2 = Info
	    )
	),	
	collectPermanentAndSaveRI2(PermAlloc, PermAlloc2, Yvars, Is3, Is2, RI).


memberReplace(El, R, [El|T], [R|T]) :- ! .
memberReplace(El, R, [N|T], [N|T1]) :-
	memberReplace(El, R, T, T1).

startLabelGen:-
	brerecord('$label', 1).

newLabelGen(L) :-
	brecorded('$label', L),
	L2 is L + 1,
	brerecord('$label', L2).

compileHead(Head, Is1, Is2, RI1, Entry) :-
	( Head =  functor(_, _, Args, _) ->
	    (nonvar(Entry), Entry = skip_list(Index, _, L2) ->
		nth(Index, Args, Arg),
		addFreeRI(Index, RI1),
		compileHeadArguments1OccVars(Args, 1, Done, Is1, Is1b, RI1),
		compileHeadArgumentCompound_skip(Arg, Index, Is1b, Is1a, RI1, L2),
		insertOrdered(Index, Done, Done0)
	    ;
		Done0 = [],
		Is1 = Is1a
	    ),
	    compileHeadArgumentsKnown(   Args, 1, Done0, Done1,Is1a, Is3, RI1),
	    compileHeadArgumentsVars(    Args, 1, Done1, Done2,Is3,  Is4, RI1),
	    compileHeadArgumentsCompound(Args, 1, Done2, _    ,Is4,  Is2, RI1)
	;
	    Is1 = Is2
	).

insertOrdered(I, [], [I]).
insertOrdered(I, L0,  L) :-
	L0 = [J|Js],
	(I < J ->
	    L = [I|L0]
	;
	    L = [J|L1],
	    insertOrdered(I, Js, L1)
	).

nth(1, [Head|_], Head).
nth(N, [_|Tail], Term) :-
	N > 1,
	N1 is N - 1, 
	nth(N1, Tail, Term).

compileHeadArgumentsKnown([], _, Done, Done, Is, Is, _).
compileHeadArgumentsKnown([Arg|Args], Nr, Done1, Done2, Is1, Is2, RI1) :-
	( Done1 = [Nr|Done1a] ->
	    Done2 = [Nr|Done2a],
	    Is3 = Is1
	;
	    Done1a = Done1,
	    ( is_not_compound(Arg) ->
	        addFreeRI(Nr, RI1),
		Done2 = [Nr|Done2a],
		compileHeadArgumentKnown(Arg, Nr, Is1, Is3, RI1)
	    ;
		Is3 = Is1,
		Done2a = Done2
	    )
	),
	Nr1 is Nr + 1,
	compileHeadArgumentsKnown(Args, Nr1, Done1a, Done2a, Is3, Is2, RI1).
compileHeadArgumentsVars([], _, Done, Done, Is, Is, _).
compileHeadArgumentsVars([Arg|Args], Nr, Done1, Done2, Is1, Is2, RI1) :-
	( Done1 = [Nr|Done1a] ->
	    Done2 = [Nr|Done2a],
	    Is3 = Is1
	;
	    Done1a = Done1,
	    ( Arg = var(_, Info, _) ->  
		Done2 = [Nr|Done2a],
		compileHeadArgumentVar(Info, Nr, Is1, Is3, RI1)
	    ;
		Is3 = Is1,
		Done2a = Done2
	    )
	),
	Nr1 is Nr + 1,
	compileHeadArgumentsVars(Args, Nr1, Done1a, Done2a, Is3, Is2, RI1).

compileHeadArguments1OccVars([], _, [], Is, Is, _).
compileHeadArguments1OccVars([Arg|Args], Nr, Done2, Is1, Is2, RI1) :-
	( Arg = var(_, Info, _),
	    compileHeadArgument1OccVar(Info, Nr, Is1, Is3, RI1) ->
	    Done2 = [Nr|Done2a]
	;
	    Is3 = Is1,
	    Done2a = Done2
	),
	Nr1 is Nr + 1,
	compileHeadArguments1OccVars(Args, Nr1, Done2a, Is3, Is2, RI1).

compileHeadArgumentsCompound([], _, Done, Done, Is, Is, _).
compileHeadArgumentsCompound([Arg|Args], Nr, Done1, Done2, Is1, Is2, RI1) :-
	( Done1 = [Nr|Done1a] ->
	    Done2 = [Nr|Done2a],
	    Is3 = Is1
	;
	    Done1a = Done1,
	    ( is_compound(Arg) ->
	        addFreeRI(Nr, RI1),
		Done2 = [Nr|Done2a],
		compileHeadArgumentCompound(Arg, Nr, Is1, Is3, RI1)
	    ;
		Is3 = Is1,
		Done2a = Done2
	    )
	),
	Nr1 is Nr + 1,
	compileHeadArgumentsCompound(Args, Nr1, Done1a, Done2a, Is3, Is2, RI1).
	
unifyTermWithReg(Term, Reg, Is1, Is2, RI1) :-
	( is_not_compound(Term) ->
	    compileHeadArgumentKnown(Term, Reg, Is1, Is2, RI1)
	;
	    compileHeadArgumentCompound(Term, Reg, Is1, Is2, RI1)
	).


unifyTermWithVar(atom(Atom, _), X, Is1, Is2, _) :-
	Is1 = [get_atomv(X, Atom)|Is2].
unifyTermWithVar(integer(Int, _), X, Is1, Is2, _) :-
	Is1 = [get_intv(X, Int)|Is2].
unifyTermWithVar(float(Float, _), X, Is1, Is2, _) :-
	Is1 = [get_floatv(X, Float)|Is2].
unifyTermWithVar(string(String, _), X, Is1, Is2, _) :-
	string_length(String,Len),
	Is1 = [get_stringv(X, Len, String)|Is2].
unifyTermWithVar(char(Char, _), X, Is1, Is2, _) :-
	Is1 = [get_charv(X, Char)|Is2].
unifyTermWithVar(List, X, Is1, Is2, RI1) :- 
        List = list(A1, A2, _), 
	Args = [A1, A2],
	( has_untagged(Args, 0, HowMany) ->
	    Is1 = [get_listv_untagged(X, HowMany)| Is4]
	;   
	    Is1 = [getlistv(X)| Is4]
	),
	compileHeadSubTerms(Args, 0, Is4, Is2, RI1).
unifyTermWithVar(functor(Name, _, Args, _), X, Is1, Is2, RI1) :-
	( has_untagged(Args, 0, HowMany) ->
	    Is1 = [get_structurev_untagged(X, Name, HowMany)|Is4]
	;   
	    Is1 = [getstrv(X, Name)|Is4]
	),
	compileHeadSubTerms(Args, 1, Is4, Is2, RI1).


compileHeadArgumentVar(void, ArgNr, Is1, Is1, RI1) :- 
	addFreeRI(ArgNr, RI1).
compileHeadArgumentVar(TVar, ArgNr, Is1, Is2, RI1) :-
	TVar =  temp(_, _, Var), !,
	(var(Var) -> 
	    addToRI(ArgNr, TVar, RI1),
	    firstUseTemp(TVar), Is1 = Is2
	;
	    findReg(RI1, TVar, X),
	    Is1 = [gettval(X, ArgNr) | Is2],
	    addToRI(ArgNr, TVar, RI1),
	    tempOccurred(TVar, RI1)
	).
compileHeadArgumentVar(PVar, ArgNr, Is1, Is2, RI1) :- 
	PVar = perm(X, _, Var), !,
        ( var(Var) ->
	    newPermVar(PVar, [], [ArgNr], RI1),
	    Is1 = Is2
	;   
	    ( findRegPermCheck(RI1, PVar, Isin),
		Isin = [Reg|_] ->
		Is1 = [gettval(Reg, ArgNr)| Is2] 
	    ;	
		Is1 = [getpval(X, ArgNr)| Is2]
	    ),
	    addToRIPerm(ArgNr, PVar, RI1),
	    permOccurred(PVar, RI1)
	).

compileHeadArgument1OccVar(void, ArgNr, Is1, Is1, RI1) :- 
	addFreeRI(ArgNr, RI1).
compileHeadArgument1OccVar(TVar, ArgNr, Is1, Is2, RI1) :-
	TVar =  temp(_, _, Var), !,
	var(Var),
	addToRI(ArgNr, TVar, RI1),
	firstUseTemp(TVar),
	Is1 = Is2.
compileHeadArgument1OccVar(PVar, ArgNr, Is1, Is2, RI1) :- 
	PVar = perm(_, _, Var), 
        var(Var),
	newPermVar(PVar, [], [ArgNr], RI1),
	Is1 = Is2.

compileHeadArgumentKnown(atom(Atom, _), ArgNr, Is1, Is2, _) :-
	Is1 = [get_atom(ArgNr, Atom)|Is2].
compileHeadArgumentKnown(integer(Int, _), ArgNr, Is1, Is2, _) :-
	Is1 = [get_int(ArgNr, Int)|Is2].
compileHeadArgumentKnown(float(Float, _), ArgNr, Is1, Is2, _) :-
	Is1 = [get_float(ArgNr, Float)|Is2].
compileHeadArgumentKnown(string(String, _), ArgNr, Is1, Is2, _) :-
	string_length(String,Len),
	Is1 = [get_string(ArgNr, Len, String)|Is2].
compileHeadArgumentKnown(char(Char, _), ArgNr, Is1, Is2, _) :-
	Is1 = [get_char(ArgNr, Char)|Is2].

compileHeadArgumentCompound_skip(list(A,B,_), ArgNr, Is1, Is2, RI1, L) :- 
	Args = [A, B],
	( has_untagged(Args, 0, HowMany) ->
		Is1 = [get_list_untagged(ArgNr, HowMany), label(L)| Is4]
	;
		Is1 = [getlist(ArgNr), label(L)| Is4]
	),
	compileHeadSubTerms(Args, 0, Is4, Is2, RI1).

compileHeadArgumentCompound(list(A,B,_), ArgNr, Is1, Is2, RI1) :- 
	Args = [A, B],
	( has_untagged(Args, 0, HowMany) ->
		Is1 = [get_list_untagged(ArgNr, HowMany)| Is4]
	;
		Is1 = [getlist(ArgNr)| Is4]
	),
	compileHeadSubTerms(Args, 0, Is4, Is2, RI1).
compileHeadArgumentCompound(functor(Nr, _, Args,_), ArgNr, Is1, Is2, RI1) :-
	( has_untagged(Args, 0, HowMany) ->
	   Is1 = [get_structure_untagged(ArgNr, Nr, HowMany)|Is4]
	;
	   Is1 = [get_structure(ArgNr, Nr)|Is4]
	),
	compileHeadSubTerms(Args, 1, Is4, Is2, RI1).

has_untagged([], Acc, Res) :- Acc >0, Res = Acc.
has_untagged([Arg|Args], Acc, Res) :-
	( untagged(Arg, HowMany) ->
	   Acc1 is Acc + HowMany
	;
	   Acc1 = Acc
	), 
	has_untagged(Args, Acc1, Res).

untagged(float(_, _), 3).  % FLOAT zoek O1 is O + 3
untagged(integer(Int,C), S) :-
	(bigint(Int) ->
	    S = 2,
	    compiler_error("untagged big int",C),
	    fail
	;
	    fail
	).
untagged(string(S, _), L1) :-
	string_length(S,L),
	string_on_heap(L,L1).

compileHeadSubTerms([], _, Is, Is, _) :- ! .
compileHeadSubTerms([Arg|Args], UntagOffset, Is1, Is2, RI1) :-
        (is_compound(Arg) ->
		compileHeadSubTermCompound(Arg, Args, UntagOffset, 
							Is1, Is2, RI1)
	;
 		compileHeadSubTerm3(Arg, UntagOffset,Extra,Is1, Is3, RI1),
		NewOffset is UntagOffset + Extra,
		compileHeadSubTerms(Args, NewOffset, Is3, Is2, RI1)
	).
	
compileHeadSubTerm3(integer(Int, C), O, 3, Is1, Is2, _) :-
	(bigint(Int) -> compiler_error("compileHeadSubTerm3 bigint",C) ; true), fail,
	(Int =< 134217727, Int >= -134217727 ->
	    fail
	;
	    compiler_error("compileHeadSubTerm3: int out of range",C),
	    fail
	),
	O1 is O + 2,
	(O1 > 65000 ->
	    fatal_message("Too large untagged offset",O1)
	;
	    true
	),
	Is1 = [unify_int_untagged(Int, O1) | Is2]. 
compileHeadSubTerm3(float(Float, _), O, 4, Is1, Is2, _) :-   % FLOAT
	O1 is O + 3,
	(O1 > 65000 ->
	    fatal_message("Too large untagged offset",O1)
	;
	    true
	),
	Is1 = [unify_float_untagged(Float, O1) | Is2]. 
compileHeadSubTerm3(string(String, _), O, L2, Is1, Is2, _) :-
	string_length(String,L),
	string_on_heap(L,L1), 
	L2 is L1 + 1,
	O1 is O + L1,
	(O1 > 65000 ->
	    fatal_message("Too large untagged offset",O1)
	;
	    true
	),
	Is1 = [unify_string_untagged(O1, L,String) | Is2]. 
compileHeadSubTerm3(integer(Int, _), _, 1, [unify_int(Int) | Is2], Is2, _).
compileHeadSubTerm3(atom(Atom, _), _, 1, [unify_atom(Atom) | Is2], Is2, _).
compileHeadSubTerm3(char(Atom, _), _, 1, [unify_char(Atom) | Is2], Is2, _).
compileHeadSubTerm3(var(_, Info, _), _, 1, Is1, Is2, RI1) :- 
	compileHeadSubTermVar(Info, Is1, Is2, RI1).

compileHeadSubTermCompound(List, Args, UntagOffset, Is1, Is2, RI1) :-
	List = list(A,B, _), !,
	(Args == [] -> 
		( has_untagged([A,B], 0, HowMany) ->
			Is1 = [unify_list_untagged(HowMany)| Is3]
		;
			Is1 = [unify_list| Is3]
		),
		compileHeadSubTerms([A,B], 0, Is3, Is2, RI1)
	;
		findSaveRegister(SafeReg, RI1),
		Is1 = [unitvar(SafeReg)|Is3],
		U1 is UntagOffset + 1,
		compileHeadSubTerms(Args, U1, Is3, Is4, RI1),
	        addFreeRI(SafeReg, RI1),
		compileHeadArgumentCompound(List, SafeReg, Is4, Is2, RI1)
	).
compileHeadSubTermCompound(Functor, Args, UntagOffset, Is1, Is2, RI1) :-
	Functor = functor(Nr,_,FArgs, _),
	(Args == [] -> 
		( has_untagged(FArgs, 0, HowMany) ->
			Is1 = [unify_structure_untagged(Nr, HowMany)|Is3]
		;
			Is1 = [unify_structure(Nr)|Is3]
		),			
		compileHeadSubTerms(FArgs, 1, Is3, Is2, RI1)
	;
		findSaveRegister(SafeReg, RI1),
		Is1 = [unitvar(SafeReg)|Is3],
		U1 is UntagOffset + 1,
		compileHeadSubTerms(Args, U1, Is3, Is4, RI1),
	        addFreeRI(SafeReg, RI1),
		compileHeadArgumentCompound(Functor,SafeReg, Is4,Is2,RI1)
	).


compileHeadSubTermVar(void, Is1, Is2, _) :-
	Is1 = [uni_void|Is2].
compileHeadSubTermVar(TVar, Is1, Is2, RI1) :- 
	TVar = temp(Dest, _, Var), !,
	(var(Var) ->
	   findFreeRegAlways(TVar, Dest, Reg, RI1, Is1, Is3),
           Is3 = [unitvar(Reg)| Is2]
         ;
	   findReg(RI1, TVar, X),
	   Is1 = [unitval(X) | Is2],
	   tempOccurred(TVar, RI1)
	).
compileHeadSubTermVar(PVar, Is1, Is2, RI1) :- 
	PVar = perm(X, _, Var),
	( var(Var) -> 
	    Is1 = [unipvar(X)| Is2],
	    newPermVar(PVar, [X], [], RI1)
	;   
	    ( findRegPermCheck(RI1, PVar, Isin) ->
		( Isin = [R|_] ->
		    Is1 = [unitval(R) | Is2],
		    permOccurred(PVar, RI1)
		;
		    newPermVarDisj(PVar, [X], [], RI1),
		    Is1 = [unipvar(X) | Is2]
		)
	    ;	
		Is1 = [unipval(X) | Is2],
		permOccurred(PVar, RI1)
	    )
	).
write_version:- version_number(X), write(X), nl.

erase(yvar_usage) :- brerecord(yvar_usage,-1).
mybrecorded(yvar_usage, J) :- brecorded(yvar_usage,X), X > -1, J = X.
mybrerecord(yvar_usage, J) :- brerecord(yvar_usage, J).
atom_codes(X,Y) :- atom_chars(X,Y).
brecorded(Atom,N) :- nb_getval(Atom,NN), NN = N.
brerecord(Atom,N) :- nb_setval(Atom,N).


%%%%%%%%%%%%%%%%%%%%%%%

compiler_read(TVs,PreProc) :-
	repeat,
	comp_read_var_dcg(T,V),
	!,
	(PreProc == [] ->
	    TVs = [T/V]
	;
	    (T == end_of_file ->
		call(PreProc,end_of_file,T1s),
		addnil(T1s,TVs)
	    ;
		(
		  abstract2prolog(T,T1),
		  call(PreProc,T1,T1s),
		  addnil(T1s,TVs)
		->
		  true
		;
		  TVs = [T/V]
		)
	    )
	).

addnil([],[]).
addnil([Ta|Tas],[T/[]|Ts]) :-
	(Ta == end_of_file ->
	Ta = T
	;
	prolog2abstract(Ta,T)
	),
	addnil(Tas,Ts).

comp_read_var_dcg(Term,Vars) :-
	native_hal_read:alt_hal_read_dcg(T,_,_),
	(
	  T == eof ->
	  Term = end_of_file,
	  Vars = []
	;
	  T = mercury_error(String,Int) ->
	  write(String), write(' at line '), write(Int),
	  seeing(File,_),
	  write(' of file '),
	  write(File),
	  nl,
	  nb_setval('$compiler_errors',syntax),
	  fail
	;
	  T = hal_read_term(Dict,Sterm),
	  Dict = dict(_,Varsl),
	  comp_mk_varlist(Varsl,Vars,VarsDict),
	  comp_mk_term(Sterm,Term,VarsDict)
	).

comp_mk_varlist([],[],[]).
comp_mk_varlist([SName - VarNr|R],Out1,Out2) :-
	(SName == "_" ->
	    Out2 = [VarNr = _Var|SS],
	    comp_mk_varlist(R,Out1,SS)
	;
	    string_to_ilist(SName,IName),
	    atomic2list(Name,IName,0),
	    Out1 = [Var = Name|RR],
	    Out2 = [VarNr = Var|SS],
	    comp_mk_varlist(R,RR,SS)
	).

comp_mk_term(Term, NewTerm, VarsDict) :-
	(Term = var(N,C) ->
	    findvar(N,VarsDict,V),
	    NewTerm = var(V,C)
	;
	    (Term = functor(Arity,StrName,Args,C) ->
		(Arity =:= 2, (StrName == "." ; StrName = "[|]") ->
 		    Args = [A1,A2],
 		    comp_mk_term(A1,NewA1,VarsDict),
 		    comp_mk_term(A2,NewA2,VarsDict),
		    NewTerm = list(NewA1,NewA2,C)
		;
		    comp_mk_termlist(Args,NewArgs,VarsDict),
		    NewTerm = functor(Arity,StrName,NewArgs,C)
		)
	    ;
		NewTerm = Term
	    )
	).

comp_mk_termlist([],[],_).
comp_mk_termlist([A|RA],[B|RB],Vars) :-
	comp_mk_term(A,B,Vars),
	comp_mk_termlist(RA,RB,Vars).

findvar(N,[Vn = V|R],Out) :-
	(N == Vn ->
	    Out = V
	;
	    findvar(N,R,Out)
	).


is_declaration(functor(1,":-",[Decl],Context),Decl,Context).

is_sysmodule_declaration(Term,ModName,ExportList) :-
	Term = functor(1,":-",[ModuleDecl],_),
	is_sysmodule_declaration1(ModuleDecl,ModName,ExportList).

is_sysmodule_declaration1(ModuleDecl,ModName,ExportList) :-
	% :- module(ModName, ExportList)
	(ModuleDecl = functor(2,"sysmodule",[atom(ModName,_),ExportModuleDecl],_) ->
	    check_export_term(ExportModuleDecl, ExportList)
	;
	    % :- module(ModName), ExportList = []
	    ModuleDecl = functor(1,"sysmodule",[atom(ModName,_)],_),
	    ExportList = []
	).

is_module_declaration(Term,ModName,ExportList,ExportedOps) :-
	Term = functor(1,":-",[ModuleDecl],_),
	is_module_declaration1(ModuleDecl,ModName,ExportList, ExportedOps).

is_module_declaration1(functor(Arity,"module",Args,_),ModName,ExportList, ExportedOps) :-
	Args = [atom(ModName,_)|RestModDeclaration],
	(
	  Arity == 1 ->
	  ExportList = [],
	  ExportedOps = []
	;
	  Arity == 2 ->
	  RestModDeclaration = [ExportModuleDecl],
	  check_export_term(ExportModuleDecl, ExportList),
	  ExportedOps = []
	;
	  Arity == 4,
	  RestModDeclaration = [ExportModuleDecl, ExportedOpsAbstract|_],
	  check_export_term(ExportModuleDecl, ExportList),
	  check_ops(ExportedOpsAbstract, ExportedOps)
	).

check_ops(atom("[]",_),[]).
check_ops(list(functor(3,"op",Args,_),R,_),[Args|RO]) :-
	compiler_op(Args),
	check_ops(R,RO).

check_ops1([]).
check_ops1([op(Prec,Fixity,Name)|R]) :-
	op(Prec,Fixity,Name),
	check_ops1(R).

check_export_term(atom("[]",_),[]).
check_export_term(list(Pred,Preds,_),[PredAtomName/PredArity|ExportList]) :-
	(Pred = functor(2,"/",[atom(PredStringName,_),integer(PredArity,_)],_) ->
	    % string_to_atom(PredStringName,PredAtomName)
	    PredStringName = PredAtomName
	;
	    (Pred = functor(2,"/",[char(0',,_),integer(PredArity,_)],_) ->
		% name(PredAtomName,[0',])
		PredAtomName = ","
	    ;
		compiler_error("Error in module declaration",Pred),
		fail
	    )
	),
	check_export_term(Preds,ExportList).


% until we implement it in machine.h ...
string_to_atom(PredStringName,PredAtomName) :-
	string_to_ilist(PredStringName,Il),
	(string_to_ilist("__M_",M),
	    conc(Pre,Last,Il),
	    conc(M,Mod,Last) ->
	    atomic2list(NnoMod,Pre,2),
	    atomic2list(ModAt,Mod,2),
	    add_module(PredAtomName,ModAt,NnoMod)
	;
	    atomic2list(PredAtomName,Il,2)
	).

atom_to_string(Atom,String) :-
	name(Atom,Ilist),
	ilist_to_string(Ilist,String).

getNameArity_fromClause(Clause,Name,Arity) :-
	(Clause = functor(2,":-",[Head|_],_) ->
	    getNameArity_fromHead(Head,Name,Arity)
	;
	    getNameArity_fromHead(Clause,Name,Arity)
	).

getNameArity_fromHead(Head,Name,Arity) :-
	(Head = functor(2,":",[Mod,Head1],_), Mod = atom(_,_) ->
	    getNameArity(Head1,Name,Arity)
	;
	    getNameArity(Head,Name,Arity)
	).

getNameArity(Head,Name,Arity) :-
	(Head = atom(Name,_) ->
	    Arity = 0
	;
	    (Head = functor(Arity,Name,_,_) ->
		true
	    ;
		Head = list(_,_,_),
		Name = ".",
		Arity = 2
	    )
	).

getNameArity2(Head,Name,Arity) :-
	(Head = atom(StringName,_) ->
	    string_to_atom(StringName,Name),
	    Arity = 0
	;
	    (Head = functor(Arity,StringName,_,_) ->
		string_to_atom(StringName,Name)
	    ;
		Head = list(_,_,_),
		string_to_atom(".",Name),
		Arity = 2
	    )
	).

compiler_error(String,Term) :-
	nb_setval('$compiler_errors',compiler),
	write(2,"Error: "),
	wrl(String),
	(get_context(Term,File,Line) ->
	    write(2," at line "),
	    write(2,Line),
	    write(2," of file "),
	    write(2,File)
	;
	    true
	),
	nl(2).

wrl(X) :- atomic(X), !, (X == [] -> true ; write(2,X)).
wrl([X|R]) :- write(2,X), wrl(R).

compiler_warning(String,Term) :-
	write(2,"Warning: "),
	write(2,String),
	(get_context(Term,File,Line) ->
	    write(2," at line "),
	    write(2,Line),
	    write(2," of file "),
	    write(2,File)
	;
	    true
	),
	nl(2).

compiler_out(String) :-
	write(2,String).

fatal_message(A,B) :-
	write(2,"Fatal compiler error: "),
	write(2,A),
	write(2,B),
	halt(7).


get_context(nocontext,_,_) :- !, fail.
get_context(context(File,Line),File,Line). 
get_context(atom(_,C),File,Line) :- get_context(C, File, Line).
get_context(var(_,C),File,Line) :- get_context(C, File, Line).
get_context(var(_,_,C),File,Line) :- get_context(C, File, Line).
get_context(integer(_,C),File,Line) :- get_context(C, File, Line).
get_context(float(_,C),File,Line) :- get_context(C, File, Line).
get_context(string(_,C),File,Line) :- get_context(C, File, Line).
get_context(char(_,C),File,Line) :- get_context(C, File, Line).
get_context(functor(_,_,_,C),File,Line) :- get_context(C, File, Line).
get_context(list(_,_,C),File,Line) :- get_context(C, File, Line).
get_context(bin_op(_,T,_), File, Line) :- get_context(T, File, Line).
get_context(uni_op(_,T), File, Line) :- get_context(T, File, Line).


getHeadBody(Clause,Head,Body) :-
	(Clause = functor(2,":-",[Head,Body],_) ->
	    true
	;
	    Head = Clause, Body = atom("true",context(none,0))
	).

illegal_goal_message(Type, Value, Context) :-
	Context = context(File,Line),
	write(Type), write(Value), write(" at line "), write(Line),
	write(" in file "), write(File), write(" cannot appear as a goal\n").

%%%%%%%%%%%

tr_pr_mod(I,O,AFs) :- tr_module_pp(I,O,AFs).

% for transforming asd:qwe into qwe__M_asd

tr_module_pp([],[], _).
tr_module_pp([Cl |Rest], Out, AFs) :-
	Cl = clause(Clause, V),
	tr_module_clause(Clause,NewClause,AFs),
	Out = [clause(NewClause,V)|RestOut],
	tr_module_pp(Rest,RestOut, AFs).

tr_module_clause(Clin,Clout,AFs) :-
	tr_bigint(Clin,Clin1),
	tr_module_term(Clin1,Clout1),
	tr_arit_clause(Clout1,Clout,AFs).

tr_module_term(Tin,Tout) :- Tin = var(_,_), Tin = Tout.
tr_module_term(Tin,Tout) :- Tin = integer(_,_), Tin = Tout.
tr_module_term(Tin,Tout) :- Tin = float(_,_), Tin = Tout.
tr_module_term(Tin,Tout) :- Tin = string(_,_), Tin = Tout.
tr_module_term(Tin,Tout) :- Tin = char(_,_), Tin = Tout.
tr_module_term(Tin,Tout) :- Tin = atom(_,_), Tin = Tout.
tr_module_term(list(A,B,C),list(NA,NB,C)) :-                 % can this occur ?
	tr_module_term(A,NA),
	tr_module_term(B,NB).
tr_module_term(Term,NewTerm) :-
	Term = functor(Arity,StrName,Args,Context),
	(Arity =:= 2, StrName = ":", added_module(Args,Context,NewTerm) ->
	    true
	;
	    tr_module_list_terms(Args,NewArgs),
	    NewTerm = functor(Arity,StrName,NewArgs,Context)
	).



tr_arit_clause(functor(2,":-",[Head,Body],Context),
	       functor(2,":-",[Head,NewBody],Context), AFs) :-
%         AFs \== [],
	tr_arit_body(Body,NewBody,Done,AFs),
	nonvar(Done), ! .
tr_arit_clause(T,T,_).


arith("is").
arith("<").
arith("=<").
arith(">").
arith(">=").
arith("=:=").
arith("=\\=").

tr_arit_body(Body,NewBody,Done,AFs) :-
	(Body = functor(2,Control,[G1,G2],C), control(Control) ->
	    NewBody = functor(2,Control,[NG1,NG2],C),
	    tr_arit_body(G1,NG1,Done,AFs),
	    tr_arit_body(G2,NG2,Done,AFs)
	;
	    (Body = functor(2,Arith,Args,C), arith(Arith) ->
		tr_arit(Arith,Args,NewArgs,NewGoals,TailGoals,Done,AFs),
		(var(NewGoals) ->
		    NewBody = Body
		;
		    NewBody = NewGoals,
		    NewArgs = [NA1,NA2],
		    (NA1 = var(_,_), NA2 = var(_,_), Arith = "is" ->
			TailGoals = functor(2,"=",NewArgs,C)
		    ;
			TailGoals = functor(2,Arith,NewArgs,C)
		    )
		)
	    ;
		NewBody = Body
	    )
	).

control(",").
control(";").
control("->").


tr_arit(StrName,[Left,Right],[NewLeft,NewRigth],Goals,TailGoals,Done,AFs) :-
	(StrName == "is" ->
	    NewLeft = Left,
	    Goals = TGs
	;
	    tr_a(Left,NewLeft,Goals,TGs,Done,AFs)
	),
	tr_a(Right,NewRigth,TGs,TailGoals,Done,AFs).

tr_a(Tin,Tout,Ig,Og,_,_) :- Tin = var(_,_), Tin = Tout, Ig = Og.
tr_a(Tin,Tout,Ig,Og,_,_) :- Tin = integer(_,_), Tin = Tout, Ig = Og.
tr_a(Tin,Tout,Ig,Og,_,_) :- Tin = float(_,_), Tin = Tout, Ig = Og.
tr_a(Tin,Tout,Ig,Og,_,_) :- Tin = string(_,_), Tin = Tout, Ig = Og.
tr_a(Tin,Tout,Ig,Og,_,_) :- Tin = char(_,_), Tin = Tout, Ig = Og.
tr_a(Tin,Tout,Ig,Og,Done,AFs) :-
	Tin = atom(Name,Context),
	(trans_arit_atom(Name,NewGoal,Var,Context,Done,AFs) ->
	    Tout = Var,
	    Ig = functor(2,",",[NewGoal,Og],Context)
	;
	    Tin = Tout, Ig = Og
	).
tr_a(Tin,Tout,Ig,Og,Done,AFs) :-
	Tin = functor(Arity,Name,Args,Context),
	tr_a_l(Args,NewArgs,Ig,NewIg,Done,AFs),
	(trans_arit_functor(Name,Arity,NewArgs,NewGoal,Var,Context,Done,AFs) ->
	    Tout = Var,
	    NewIg = functor(2,",",[NewGoal,Og],Context)
	;
	    Var = var(_,Context),
	    Tout = Var,
	    NewGoal = functor(2,"is",
			      [Var,functor(Arity,Name,NewArgs,Context)],Context),
	    NewIg = functor(2,",",[NewGoal,Og],Context)
	).

tr_a_l([],[],Ig,Og,_,_) :- Ig = Og.
tr_a_l([A|As],[NewA|NewAs],Ig,Og,Done,AFs) :-
	tr_a(A,NewA,Ig,Ig1,Done,AFs),
	tr_a_l(As,NewAs,Ig1,Og,Done,AFs).

trans_arit_atom(Name,functor(1,Name,[var(V,C)],C),var(V,C),C,Done,AFs) :-
	mymember(Name/0,AFs),
	!,
	Done = done.

trans_arit_functor(Name,Arity,Args,functor(NewArity,Name,NewArgs,C),var(V,C),C,Done,AFs) :-
	mymember(Name/Arity,AFs),
	!,
	Done = done,
	NewArity is Arity + 1,
	conc(Args,[var(V,C)],NewArgs).

trans_arit_functor(Name,Arity,Args,functor(NewArity,NewName,NewArgs,C),var(V,C),C,Done,AFs) :-
	Arity = 1,
	builtin_arith_functions(Name,NewName),
	!,
	Done = done,
	NewArity = 2,
	Args = [A],
	NewArgs = [A,var(V,C)].

builtin_arith_functions(_,_) :- fail.
%% builtin_arith_functions("len","length").
%% builtin_arith_functions("max","max__M_sysh").
%% builtin_arith_functions("min","min__M_sysh").
%% builtin_arith_functions("sum","sum__M_sysh").



added_module([atom(Mod,_),Term],Context,NewTerm) :-
	(Term = atom(StrName,_) ->
	    NewTerm = atom(NewStrName,Context)
	;
	    Term = functor(Arity,StrName,Args,_),
	    NewTerm = functor(Arity,NewStrName,NewArgs,Context),
	    tr_module_list_terms(Args,NewArgs)
	),
	string_append(StrName,"__M_",StrNameM),
	string_append(StrNameM,Mod,NewStrName).


tr_module_list_terms([],[]).
tr_module_list_terms([A|As],[B|Bs]) :-
	tr_module_term(A,B),
	tr_module_list_terms(As,Bs).

string_append(S1,S2,S3) :-
	string_to_ilist(S1,L1),
	string_to_ilist(S2,L2),
	conc(L1,L2,L3),
	ilist_to_string(L3,S3).

%%%%%%%%%%%

compile_init_perm(APerms, Is1, Is2, RI1) :-
	collect_perms(APerms,Perms), !,
	collect_yvarnumbers(Perms,Numbers,RI1),
	sort(Numbers,Nrs),
	gencodeinnitpermvars(Nrs,Is1,Is2).
compile_init_perm(APerms, _,_,_) :-
	write(compile_init_perm(APerms)-failed),nl,
	fail.

collect_yvarnumbers([],[],_).
collect_yvarnumbers([Perm|R],[Nr|Nrs],RI1) :-
	Perm = perm(X, _, _),
	newPermVar(Perm, [X], [], RI1),
	X = Nr,
	collect_yvarnumbers(R,Nrs,RI1).

gencodeinnitpermvars([],Is1,Is2) :- !, Is2 = Is1.
gencodeinnitpermvars([N],Is1,Is2) :- !, Is1 = [initperm(N)|Is2].
gencodeinnitpermvars([N1,N2],Is1,Is2) :- !, N is (N1 << 8) + N2, Is1 = [initperm(N)|Is2].
gencodeinnitpermvars([N1,N2,N3],Is1,Is2) :- !, N is (N1 << 16) + (N2 << 8) + N3, Is1 = [initperm(N)|Is2].
gencodeinnitpermvars([N1,N2,N3,N4|R],Is1,Is2) :- !, N is (N1 << 24) + (N2 << 16) + (N3 << 8) + N4, Is1 = [initperm(N)|Is3], gencodeinnitpermvars(R,Is3,Is2).
	    

collect_perms(atom(_,_),[]).
collect_perms(list(var(_,P,_),R,_),Out) :-
	(P = perm(_,_,Var), var(Var) ->
	    Out = [P|S],
	    collect_perms(R,S)
	;
	    collect_perms(R,Out)
	).




%%%%%%%%%%%

% 32-64 bit per word dependencies

string_on_heap(Len,StringUsage) :-
	heap_usage_string(Len,S),
	StringUsage is S - 1. % not counting the pointer with a tag

heap_usage_string(Len,StringUsage) :-
	bits_per_word(Bits),
	(Bits =:= 32 ->
	    StringUsage is ((Len + 3)>>2) + 2
	;
	    (Bits =:= 64 ->
		StringUsage is ((Len + 7)>>3) + 2
	    ;
		myerror
	    )
	).

%%%%%%%%%%


%% Dealing with bigints - not optimal, but it will do fine :-)

% tr_bigint(X,X) :- ! . % for the time being
tr_bigint(X,Y) :- tr_bigint1(X,Y).

tr_bigint1(functor(2,":-",[H,B],Context),NewClause) :-
        !,
	NewClause = functor(2,":-",[NewH,NewB],Context),
	trgoal_bigint(H,NewH,HGoals),
	trbody_bigint(B,NewB1),
	(var(HGoals) ->
	    NewB = NewB1
	;
	    NewB = functor(2,",",[HGoals,NewB1],Context)
	).
tr_bigint1(functor(1,":-",[B],Context),NewClause) :-
        !,
	NewClause = functor(1,":-",[NewB],Context),
	trbody_bigint(B,NewB).
tr_bigint1(H,NewClause) :-
	NewClause = functor(2,":-",[NewH,NewB],nocontext), % context could be better
	trgoal_bigint(H,NewH,HGoals),
	(var(HGoals) ->
	    NewB = atom("true",nocontext)
	;
	    NewB = HGoals
	).

trbody_bigint(functor(2,Name,Args,Context),NewBody) :-
	(Name == "," ; Name == ";" ; Name == "->"), !,
	Args = [A1,A2],
	trbody_bigint(A1,NA1),
	trbody_bigint(A2,NA2),
	NewBody = functor(2,Name,[NA1,NA2],Context).
trbody_bigint(G,NewBody) :-
	trgoal_bigint(G,NewG,PutBigInts),
	combinegoals(PutBigInts,NewG,NewBody).


trgoal_bigint(GoalIn, GoalOut, _Rest) :- GoalIn = var(_,_), GoalOut = GoalIn.
trgoal_bigint(GoalIn, GoalOut, _Rest) :- GoalIn = atom(_,_), GoalOut = GoalIn.
trgoal_bigint(GoalIn, GoalOut, _Rest) :- GoalIn = float(_,_), GoalOut = GoalIn.
trgoal_bigint(GoalIn, GoalOut, _Rest) :- GoalIn = string(_,_), GoalOut = GoalIn.
trgoal_bigint(GoalIn, GoalOut, _Rest) :- GoalIn = char(_,_), GoalOut = GoalIn.
trgoal_bigint(GoalIn, GoalOut, Rest) :- GoalIn = integer(Int,C),
	(bigint(Int) ->
	    GoalOut = var(_X,C),
	    Rest = functor(2,"putbigint__M_sysh",[GoalOut,GoalIn],C)
	;
	    GoalOut = GoalIn
	).
trgoal_bigint(GoalIn, GoalOut, Rest) :-
	GoalIn = list(A,B,C),
	trgoal_bigint(A,NA,P1),
	trgoal_bigint(B,NB,P2),
	combinegoals(P1,P2,Rest),
	GoalOut = list(NA,NB,C).
trgoal_bigint(functor(Arity,Name,Args,Context), GoalOut, PutBigInts) :-
	tr_bigintargs(Args,NewArgs,_EmptyPuts,PutBigInts),
	GoalOut = functor(Arity,Name,NewArgs,Context).

tr_bigintargs([],[],P,P).
tr_bigintargs([A|R],[NA|NR],InPs,OutPs) :-
	trgoal_bigint(A,NA,P),
	combinegoals(InPs,P,NewInPs),
	tr_bigintargs(R,NR,NewInPs,OutPs).

combinegoals(A,B,Goals) :-
	(var(A) ->
	    (var(B) ->
		true
	    ;
		Goals = B
	    )
	;
	    (var(B) ->
		Goals = A
	    ;
		Goals = functor(2,",",[A,B],nocontext)
	    )
	).

%%%%%%%%

% the following are part of a redesign of use_module
% done while in Frankfurt airport  9 november 2004
%               Elwood with a jetlag
%               Apollo Bay

fileprefix(File,Prefix) :-
	name(File,L1),
	conc(L2,[0'/|L3],L1),
	\+(conc(_,[0'/|_], L3)),
	!,
	name(Prefix,L2).
fileprefix(_,'').

% next clause deals with library case
getfilename(library(Atom),_Context,AbsFile) :-
	!,
	atom(Atom),
	sysh:getenv(0,InstallDir),
	name(Atom,La),
	name(InstallDir,Li),
	conc(Li,[0'l, 0'i, 0'b, 0'/ | La],FullL),
	normalise(FullL,NewFullL),
	name(AbsFile,NewFullL).
	
% next clause deals with ~/ ./ / and the rest
getfilename(Atom,Context,AbsFile) :-
	atom(Atom),
	name(Atom,L1),
	L1 = [F | L2],
	(F == 0'/ ->
	    !,
	    FullL = L1
	;
	    (
	      F == 0'~,
	      L2 = [ 0'/ | _],
	      !,
	      sysh:getenv('HOME',Prefix),
	      name(Prefix,L3),
	      conc(L3,L2,FullL)
	    ;
	      (F == 0'. , L2 = [ 0'/ | _] ->
		  L4 = L2
	      ;
		  L4 = [0'/ |L1]
	      ),
	      (atom(Context) ->
		  Prefix = Context
	      ;
		  sysh:getenv('PWD',Prefix)
	      ),
	      name(Prefix,L3),
	      conc(L3,L4,FullL) 
	    )
	),
	normalise(FullL,NewFullL),
	name(AbsFile,NewFullL).



normalise(In,Out) :-
	conc(L1,L2,In),
	conc(L3,L4,L2),
	(
	  conc([0'/ | Up],[0'/, 0'. , 0'.],L3),
	  \+(conc(_,[0'/|_],Up)),
	  L5 = L4
	;
	  L3 = [0'/, 0'/],
	  L5 = [0'/|L4]
	),
	!,
	conc(L1,L5,NewIn),
	normalise(NewIn,Out).
normalise(In,In).


decideonsuffix(In,Out) :-
	normalmember(Suffix,['.pl', '']),
	strip_suffix(In,Suffix,Out),
	file_exists(Out),
	! .

strip_suffix(File,Suffix,WithSuffix) :-
	strip_extension(File,Suffix,NoSuffix),
	give_extension(NoSuffix,Suffix,WithSuffix).

strip_extension(File,Extension,Stripped) :-
	atomic2list(File,FileList,0),
	atomic2list(Extension,ExtList,0),
	( conc(StrippedList,ExtList,FileList) ->
	    atomic2list(Stripped,StrippedList,2)
	;
	    Stripped = File
	).

give_extension(File,Extension,ExtFile) :-
	atomic2list(File,FileList,0),
	atomic2list(Extension,ExtensionList,0),
	conc(FileList,ExtensionList,ExtFileList),
	atomic2list(ExtFile,ExtFileList,2).

%%%%%%%%%%%
% replacing some calls to builtins (functor/3) with a specialized variant
% this is sometimes not possible later
%%%%%%%%%%%

replace_spec([],[]).
replace_spec([Clause|Rest],[NewClause|NewRest]) :-
	Clause = clause(Term, Bindings),
	(Term = functor(2,":-",Args,Context),
	    Args = [Head,Body],
	    term_variables(Head,HVars),
	    replace_spec_term(Body,NewBody,HVars,Done),
	    nonvar(Done) ->
	    NewTerm = functor(2,":-",[Head,NewBody],Context),
	    NewClause = clause(NewTerm,Bindings)
	;
	    NewClause = Clause
	),
	replace_spec(Rest,NewRest).

replace_spec_term(functor(2,",",[G1,G2],Cont),Out,Vars,Done) :- !,
	Out = functor(2,",",[NG1,NG2],Cont),
	replace_spec_term(G1,NG1,Vars,Done),
	term_variables((G1+Vars),NewVars),
	replace_spec_term(G2,NG2,NewVars,Done).
replace_spec_term(functor(2,";",[G1,G2],Cont),Out,Vars,Done) :- !,
	Out = functor(2,";",[NG1,NG2],Cont),
	replace_spec_term(G1,NG1,Vars,Done),
	term_variables((G1+Vars),NewVars),
	replace_spec_term(G2,NG2,NewVars,Done).
replace_spec_term(functor(2,"->",[G1,G2],Cont),Out,Vars,Done) :- !,
	Out = functor(2,"->",[NG1,NG2],Cont),
	replace_spec_term(G1,NG1,Vars,Done),
	term_variables((G1+Vars),NewVars),
	replace_spec_term(G2,NG2,NewVars,Done).
replace_spec_term(Compound,NewCompound,Vars,Done) :-
	Compound = functor(Arity,Name,Args,Cont),
	specialized_inline_builtin(Name,Arity,NewName,Specs),
	complies2spec(Args,Specs,Vars), !,
	Done = done,
	NewCompound = functor(Arity,NewName,Args,Cont).
replace_spec_term(G,G,_,_).

complies2spec([],[],_).
complies2spec([Arg|Args],[Spec|Specs],Vars) :-
	check_spec(Spec,Arg,Vars,NewVars),
	complies2spec(Args,Specs,NewVars).

check_spec(?,_,Vars,Vars).
check_spec(new,var(X,_),Vars,NewVars) :-
	notvarisin(Vars,X),
	NewVars = [X|Vars].
check_spec(smallposint,integer(I,_),Vars,Vars) :-
	-1000000 < I,
	I < 1000000.
check_spec(atom,atom(_,_),Vars,Vars).


notvarisin([],_).
notvarisin([Y|R],X) :-
	X \== Y,
	notvarisin(R,X).


%%%%%%%%%%%
%% long overdue, and sadly needed ...
%%%%%%%%%%%

abstract2prolog(var(X,_),X).
abstract2prolog(atom(S,_),A) :- string_to_atom(S,A).
abstract2prolog(integer(I,_),I).
abstract2prolog(float(F,_),F).
abstract2prolog(char(C,_),Char) :- int_to_char(C,Char).
abstract2prolog(string(S,_),S).
abstract2prolog(list(Head,Tail,_),[HP|TP]) :-
	abstract2prolog(Head,HP), abstract2prolog(Tail,TP).
abstract2prolog(functor(_,StrName,Args,_),Term) :-
	abstract2prologl(Args,NewArgs),
	string_to_atom(StrName,Atom),
	Term =.. [Atom|NewArgs].

abstract2prologl([],[]).
abstract2prologl([A|R],[B|S]) :-
	abstract2prolog(A,B),
	abstract2prologl(R,S).

%%%%%%%%%%%

prolog2abstract(Var,Out) :- var(Var), !, Out = var(Var,nocontext).
prolog2abstract(Atom,Out) :- atom(Atom), !,
	atom_to_string_with_module(Atom,String), Out = atom(String,nocontext).
prolog2abstract(I,O) :- integer(I), !, O = integer(I,nocontext).
prolog2abstract(F,O) :- float(F), !, O = float(F,nocontext).
prolog2abstract(C,O) :- char(C), !, char_to_int(C,CI), O = char(CI,nocontext).
prolog2abstract(S,O) :- string(S), !, O = string(S,nocontext).
prolog2abstract([H|T],O) :- !,
	prolog2abstract(H,HO),
	prolog2abstract(T,TO),
	O = list(HO,TO,nocontext).
prolog2abstract(Term,O) :-
	Term =.. [Name|Args],
	functor(Term,_,Arity),
	atom_to_string_with_module(Name,SName),
	p2al(Args,NewArgs),
	O = functor(Arity,SName,NewArgs,nocontext).

p2al([],[]).
p2al([A|R],[A1|R1]) :- prolog2abstract(A,A1), p2al(R,R1).

atom_to_string_with_module(Atom,String) :-
	add_module(Atom,M,A),
	name(A,AL),
	(M == user ->
	    ilist_to_string(AL,String)
	;
	    name(M,ML),
	    conc(AL,[0'_,0'_,0'M,0'_|ML],All),
	    ilist_to_string(All,String)
	).


%%%%%%%%%%%

startswithvarcutfail(Pred,NewPred,Arg) :-
	Pred = pred(Clauses,X,Y),
	Clauses = [FirstCl|RestCls],
	FirstCl = clause(Head,Body,_,_,_),
	Body = ['$mark'(var(CH,_,_)),
		test("var",[var(Var,_,_)],_),
		'_$cutto'(var(CH,_,_)),fail],
	Head = functor(_,_,Vars,_),
	alldiffvars(Vars,VarsList),
	varisin(Var,1,VarsList,Arg),
	!,
	NewPred = pred(RestCls,X,Y).

alldiffvars(Vars,VarsList) :-
	collectvars(Vars,VarsList),
	\+(\+(nonesame(VarsList,1))).
nonesame([],_).
nonesame([V|R],V) :- VV is V + 1, nonesame(R,VV).

collectvars([],[]).
collectvars([var(A,_,_)|R],[A|S]) :- collectvars(R,S).

varisin(V,I,[W|R],J) :-
	(V == W ->
	    J = I
	;
	    II is I + 1,
	    varisin(V,II,R,J)
	).


%%%%%%%%%%%

% todo or done
% string_to_atom in machine.h

w(X) :- writeq(X), nl.

