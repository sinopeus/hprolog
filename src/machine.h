#include "sat/instructions.h"

hcase(getpvarputpvar)
getpvarputpvar_label:
profile(getpvarputpvar);
{
  dlong yvar1;
  dlong areg2;

  yvar1 = *(yvar_type *)(arg_place(locpc,2,getpvarputpvar));
  areg2 = *(areg_type *)(arg_place(locpc,1,getpvarputpvar));
  ereg[yvar1] = args(areg2);

  yvar1 = *(yvar_type *)(arg_place(locpc,4,getpvarputpvar));
  areg2 = *(areg_type *)(arg_place(locpc,3,getpvarputpvar));
  *lochreg = ereg[yvar1] = args(areg2) = (dlong)lochreg;

  locpc += getpvarputpvar_len;
  lochreg++;

  goto_more_exec;
}


hcase(getpvar)
getpvar_label:
profile(getpvar);
{
  dlong yvar1;
  dlong areg2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,getpvar));
  areg2 = *(areg_type *)(arg_place(locpc,2,getpvar));

  locpc += getpvar_len;
  ereg[yvar1] = args(areg2);

  goto_more_exec;
}

hcase(getpval)
getpval_label:
profile(getpval);
{
  dlong yvar1;
  dlong areg2;
  dlong *p1, *p2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,getpval));
  p1 = (dlong *)ereg[yvar1];
  deref(p1);

  areg2 = *(areg_type *)(arg_place(locpc,2,getpval));
  p2 = (dlong *)args(areg2);
  deref(p2);

  locpc += getpval_len;

  if (is_ref(p2) && (! is_ref(p1)))
    {
      underef(p1);
      trail(p2,bh,loctrreg);
      *p2 = (dlong)p1;
    }
  else
    if (is_ref(p1) && (! is_ref(p2)))
      {
	underef(p2);
	trail(p1,bh,loctrreg);
	*p1 = (dlong)p2;
      }
    else
      {
	if (!unify_terms(p1,p2,&locmach))
	  goto_fail_label;
      }

  goto_more_exec;
}

hcase(putpval)
putpval_label:
profile(putpval);
{
  dlong yvar1;
  dlong areg2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,putpval));
  areg2 = *(areg_type *)(arg_place(locpc,2,putpval));

  locpc += putpval_len;
  args(areg2) = ereg[yvar1];

  goto_more_exec;
}

hcase(putpvar)
putpvar_label:
profile(putpvar);
{
  dlong yvar1;
  dlong areg2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,putpvar));
  areg2 = *(areg_type *)(arg_place(locpc,2,putpvar));

  *lochreg = ereg[yvar1] = args(areg2) = (dlong)lochreg;

  locpc += putpvar_len;
  lochreg++;

  goto_more_exec;
}


hcase(putpvarvar)
putpvarvar_label:
profile(putpvarvar);
{
  dlong areg1;
  dlong yvar1;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,putpvarvar));
  yvar1 = *(yvar_type*)(arg_place(locpc,3,putpvarvar));
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvarvar));
  yvar1 = *(yvar_type*)(arg_place(locpc,4,putpvarvar));
  locpc += putpvarvar_len;
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  goto_more_exec;
}

hcase(putpvarval)
putpvarval_label:
profile(putpvarval);
{
  dlong areg1;
  dlong yvar1;
  dlong *p;

  areg1 = *(areg_type*)(arg_place(locpc,1,putpvarval));
  yvar1 = *(yvar_type*)(arg_place(locpc,3,putpvarval));
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvarval));
  yvar1 = *(yvar_type*)(arg_place(locpc,4,putpvarval));
  locpc += putpvarval_len;

  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  goto_more_exec;
}

hcase(putpvalval)
putpvalval_label:
profile(putpvalval);
{
  dlong areg1, yvar1;
  dlong *p;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,putpvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,3,putpvalval));
  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,4,putpvalval));
  locpc += putpvalval_len;
  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  goto_more_exec;
}

hcase(putpvarvarvar)
putpvarvarvar_label:
profile(putpvarvarvar);
{
  dlong areg1;
  dlong yvar1;

  areg1 = *(areg_type*)(arg_place(locpc,1,putpvarvarvar));
  yvar1 = *(yvar_type*)(arg_place(locpc,4,putpvarvarvar));
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvarvarvar));
  yvar1 = *(yvar_type*)(arg_place(locpc,5,putpvarvarvar));
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  areg1 = *(areg_type*)(arg_place(locpc,3,putpvarvarvar));
  yvar1 = *(yvar_type*)(arg_place(locpc,6,putpvarvarvar));

  locpc += putpvarvarvar_len;

  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  goto_more_exec;
}

hcase(putpvarvarval)
putpvarvarval_label:
profile(putpvarvarval);
{
  dlong areg1;
  dlong yvar1;
  dlong *p;

  areg1 = *(areg_type*)(arg_place(locpc,1,putpvarvarval));
  yvar1 = *(yvar_type*)(arg_place(locpc,4,putpvarvarval));
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvarvarval));
  yvar1 = *(yvar_type*)(arg_place(locpc,5,putpvarvarval));
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  areg1 = *(areg_type*)(arg_place(locpc,3,putpvarvarval));
  yvar1 = *(yvar_type*)(arg_place(locpc,6,putpvarvarval));

  locpc += putpvarvarval_len;

  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  goto_more_exec;
}

hcase(putpvarvalval)
putpvarvalval_label:
profile(putpvarvalval);
{
  dlong areg1;
  dlong yvar1;
  dlong *p;

  areg1 = *(areg_type*)(arg_place(locpc,1,putpvarvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,4,putpvarvalval));
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvarvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,5,putpvarvalval));
  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  areg1 = *(areg_type*)(arg_place(locpc,3,putpvarvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,6,putpvarvalval));

  locpc += putpvarvalval_len;

  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  goto_more_exec;
}

hcase(putpvalvalval)
putpvalvalval_label:
profile(putpvalvalval);
{
  dlong areg1;
  dlong yvar1;
  dlong *p;

  areg1 = *(areg_type*)(arg_place(locpc,1,putpvalvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,4,putpvalvalval));
  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvalvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,5,putpvalvalval));
  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  areg1 = *(areg_type*)(arg_place(locpc,3,putpvalvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,6,putpvalvalval));

  locpc += putpvalvalval_len;

  p = (dlong *)ereg[yvar1];
  //      deref(p); underef(p);
  args(areg1) = (dlong)p;

  goto_more_exec;
}


hcase(putpvalvalvalval)
     putpvalvalvalval_label:
profile(putpvalvalvalval);
{
  dlong areg1;
  dlong yvar1;
  dlong *p;

  areg1 = *(areg_type*)(arg_place(locpc,1,putpvalvalvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,5,putpvalvalvalval));
  p = (dlong *)ereg[yvar1];
  args(areg1) = (dlong)p;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvalvalvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,6,putpvalvalvalval));
  p = (dlong *)ereg[yvar1];
  args(areg1) = (dlong)p;

  areg1 = *(areg_type*)(arg_place(locpc,3,putpvalvalvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,7,putpvalvalvalval));
  p = (dlong *)ereg[yvar1];
  args(areg1) = (dlong)p;

  areg1 = *(areg_type*)(arg_place(locpc,4,putpvalvalvalval));
  yvar1 = *(yvar_type*)(arg_place(locpc,8,putpvalvalvalval));

  locpc += putpvalvalvalval_len;

  p = (dlong *)ereg[yvar1];
  args(areg1) = (dlong)p;

  goto_more_exec;
}


hcase(put4pval)
     put4pval_label:
profile(put4pval);
{
  dlong yvar1;
  dlong *p;

  yvar1 = *(yvar_type*)(arg_place(locpc,1,put4pval));
  p = (dlong *)ereg[yvar1];
  args(1) = (dlong)p;

  yvar1 = *(yvar_type*)(arg_place(locpc,2,put4pval));
  p = (dlong *)ereg[yvar1];
  args(2) = (dlong)p;

  yvar1 = *(yvar_type*)(arg_place(locpc,3,put4pval));
  p = (dlong *)ereg[yvar1];
  args(3) = (dlong)p;

  yvar1 = *(yvar_type*)(arg_place(locpc,4,put4pval));

  locpc += put4pval_len;

  p = (dlong *)ereg[yvar1];
  args(4) = (dlong)p;

  goto_more_exec;
}

hcase(getpvar2)
getpvar2_label:
profile(getpvar2);
{
  dlong areg1, yvar1;

  areg1 = *(areg_type *)(arg_place(locpc,1,getpvar2));
  yvar1 = *(yvar_type *)(arg_place(locpc,3,getpvar2));
  ereg[yvar1] = args(areg1);
  areg1 = *(areg_type *)(arg_place(locpc,2,getpvar2));
  yvar1 = *(yvar_type *)(arg_place(locpc,4,getpvar2));
  locpc += getpvar2_len;
  ereg[yvar1] = args(areg1);
  goto_more_exec;
}

hcase(getpvar3)
getpvar3_label:
profile(getpvar3);
{
  dlong areg1, yvar1;

  areg1 = *(areg_type *)(arg_place(locpc,1,getpvar3));
  yvar1 = *(yvar_type *)(arg_place(locpc,4,getpvar3));
  ereg[yvar1] = args(areg1);
  areg1 = *(areg_type *)(arg_place(locpc,2,getpvar3));
  yvar1 = *(yvar_type *)(arg_place(locpc,5,getpvar3));
  ereg[yvar1] = args(areg1);
  areg1 = *(areg_type *)(arg_place(locpc,3,getpvar3));
  yvar1 = *(yvar_type *)(arg_place(locpc,6,getpvar3));
  locpc += getpvar3_len;
  ereg[yvar1] = args(areg1);
  goto_more_exec;
}

hcase(getpvar4)
getpvar4_label:
profile(getpvar4);
{
  dlong areg1, yvar1;

  areg1 = *(areg_type *)(arg_place(locpc,1,getpvar4));
  yvar1 = *(yvar_type *)(arg_place(locpc,5,getpvar4));
  ereg[yvar1] = args(areg1);
  areg1 = *(areg_type *)(arg_place(locpc,2,getpvar4));
  yvar1 = *(yvar_type *)(arg_place(locpc,6,getpvar4));
  ereg[yvar1] = args(areg1);
  areg1 = *(areg_type *)(arg_place(locpc,3,getpvar4));
  yvar1 = *(yvar_type *)(arg_place(locpc,7,getpvar4));
  ereg[yvar1] = args(areg1);
  areg1 = *(areg_type *)(arg_place(locpc,4,getpvar4));
  yvar1 = *(yvar_type *)(arg_place(locpc,8,getpvar4));
  locpc += getpvar4_len;
  ereg[yvar1] = args(areg1);
  goto_more_exec;
}

hcase(gettval)
gettval_label:
profile(gettval);
{
  register dlong *p1;
  register dlong *p2;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,gettval)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,gettval)));
  locpc += gettval_len;

  deref2(p1,goto p1var);
  deref2(p2,goto p1nonvarp2var);
  /* both nonvar */
  if (unify_terms(p1,p2,&locmach)) goto_more_exec;
  goto_fail_label;

 p1var:
  deref2(p2,goto p1varp2var);

  /* p1varp2nonvar */
  underef(p2);
  trail(p1,bh,loctrreg);
  *p1 = (dlong)p2;
  goto_more_exec;

 p1nonvarp2var:
  underef(p1);
  trail(p2,bh,loctrreg);
  *p2 = (dlong)p1;
  goto_more_exec;

 p1varp2var:
  if (p1 == p2) goto_more_exec;         
  if (p2 > p1)                
    {                         
      trail(p2,bh,loctrreg);  
      *p2 = (dlong)p1;        
      goto_more_exec;             
    }                         
  trail(p1,bh,loctrreg);          
  *p1 = (dlong)p2;                
  goto_more_exec;                     
}

hcase(gettval_proceed)
gettval_proceed_label:
profile(gettval_proceed);
{
  register dlong *p1;
  register dlong *p2;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,gettval_proceed)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,gettval_proceed)));
  locpc = contpreg;

  deref2(p1,goto gpp1var);
  deref2(p2,goto gpp1nonvarp2var);
  /* both nonvar */
  if (unify_terms(p1,p2,&locmach)) goto_more_exec;
  goto_fail_label;

 gpp1var:
  deref2(p2,goto gpp1varp2var);

  /* p1varp2nonvar */
  underef(p2);
  trail(p1,bh,loctrreg);
  *p1 = (dlong)p2;
  goto_more_exec;

 gpp1nonvarp2var:
  underef(p1);
  trail(p2,bh,loctrreg);
  *p2 = (dlong)p1;
  goto_more_exec;

 gpp1varp2var:
  if (p1 == p2) goto_more_exec;         
  if (p2 > p1)                
    {                         
      trail(p2,bh,loctrreg);  
      *p2 = (dlong)p1;        
      goto_more_exec;             
    }                         
  trail(p1,bh,loctrreg);          
  *p1 = (dlong)p2;                
  goto_more_exec;                     
}

hcase(movreg)
movreg_label:
profile(movreg);
{
  dlong areg1;
  dlong areg2;

  areg1 = *(areg_type *)(arg_place(locpc,1,movreg));
  areg2 = *(areg_type *)(arg_place(locpc,2,movreg));

  locpc += movreg_len;
  args(areg2) = args(areg1);
  goto_more_exec;
}

hcase(movreg2)
movreg2_label:
profile(movreg2);
{
  dlong areg1;
  dlong areg2;

  areg1 = *(areg_type *)(arg_place(locpc,1,movreg2));
  areg2 = *(areg_type *)(arg_place(locpc,2,movreg2));
  args(areg2) = args(areg1);
  areg1 = *(areg_type *)(arg_place(locpc,3,movreg2));
  areg2 = *(areg_type *)(arg_place(locpc,4,movreg2));

  locpc += movreg2_len;
  args(areg2) = args(areg1);
  goto_more_exec;
}

hcase(add)
add_label:
profile_builtin(add);
{
  dlong areg1, res;
  dlong *p2, *p3;

  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,add)));
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,add)));
  areg1 = *(areg_type *)(arg_place(locpc,1,add));
  locpc += add_len;

  deref(p3); deref(p2);

  if (is_smallint(p2) && is_smallint(p3))
    {
      res = get_smallint(p3) + get_smallint(p2);
      if (in_smallint_range(res))
        {build_smallint(&(args(areg1)),res); goto_more_exec;}
      build_bigint_from_out_of_range_smallint(&(args(areg1)),res,lochreg);
      goto_more_exec;
    }

  if (is_real(p2) && is_real(p3))
    {
      dfloat f2, f3;
      f2 = get_real(p2);
      f3 = get_real(p3);

      build_real(&(args(areg1)),f2+f3,lochreg);
      goto_more_exec;
    }

  if (is_real(p2) || is_real(p3))
    {
      dfloat f2, f3;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_add;
      if (!get_float_from_number(p3,&f3,&locmach)) goto arith_error_add;

      build_real(&(args(areg1)),f2+f3,lochreg);
      goto_more_exec;
    }

  res = addsub_two_inumbers(p2,p3,1,&locmach);
  if (! res) goto arith_error_add;
  args(areg1) = res;
  goto_more_exec;

}

hcase(power)
power_label:
profile_builtin(power);
{
  dlong areg;
  dfloat f1, f2;
  dlong *p2, *p3;

  areg = *(areg_type *)(arg_place(locpc,2,power));
  p2 = (dlong *)args(areg);
  areg = *(areg_type *)(arg_place(locpc,3,power));
  p3 = (dlong *)args(areg);
  areg = *(areg_type *)(arg_place(locpc,1,power));
  locpc += power_len;

  deref(p3);
  deref(p2);

  if (! get_float_from_number(p2,&f1,&locmach)) goto arith_error_power;
  if (! get_float_from_number(p3,&f2,&locmach)) goto arith_error_power;

  f1 = pow(f1,f2);

  build_real(&(args(areg)),f1,lochreg);
  goto_more_exec;
}


hcase(subtr)
subtr_label:
profile_builtin(subtr);
{
  dlong areg1, res;
  dlong *p2, *p3;

  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,subtr)));
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,subtr)));
  areg1 = *(areg_type *)(arg_place(locpc,1,subtr));
  locpc += subtr_len;

  deref(p3); deref(p2);

  if (is_smallint(p2) && is_smallint(p3))
    {
      res = get_smallint(p2) - get_smallint(p3);
      if (in_smallint_range(res))
        build_smallint(&(args(areg1)),res);
      else
        build_bigint_from_out_of_range_smallint(&(args(areg1)),res,lochreg);
      goto_more_exec;
    }

  if (is_real(p2) && is_real(p3))
    {
      dfloat f2, f3;
      f2 = get_real(p2);
      f3 = get_real(p3);

      build_real(&(args(areg1)),f2-f3,lochreg);
      goto_more_exec;
    }

  if (is_real(p2) || is_real(p3))
    {
      dfloat f2, f3;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_subtr;
      if (!get_float_from_number(p3,&f3,&locmach)) goto arith_error_subtr;

      build_real(&(args(areg1)),f2-f3,lochreg);
      goto_more_exec;
    }

  res = addsub_two_inumbers(p2,p3,0,&locmach);
  if (! res) goto arith_error_subtr;
  args(areg1) = res;
  goto_more_exec;
}


hcase(bit_and)
bit_and_label:
profile_builtin(bit_and);
{
  dlong areg;
  dlong *p1,*p2;
  dlong i2,i3;

  areg = *(areg_type *)(arg_place(locpc,2,bit_and));
  p1 = (dlong *)args(areg);
  deref(p1);

  areg = *(areg_type *)(arg_place(locpc,3,bit_and));
  p2 = (dlong *)args(areg);
  deref(p2);

  areg = *(areg_type *)(arg_place(locpc,1,bit_and));

  locpc += bit_and_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      i2 = get_smallint(p1);
      i3 = get_smallint(p2);
      build_smallint(&(args(areg)),i2&i3);
      goto_more_exec;
    }

  if ((! is_inumber(p1)) || (! is_inumber(p2)))
    goto arith_error_bit_and;

  {
    dlong res = and_two_inumbers(p1,p2,&locmach);
    if (! res) goto arith_error_bit_and;
    args(areg) = res;
  }
  goto_more_exec;
}


hcase(bit_or)
bit_or_label:
profile_builtin(bit_or);
{
  dlong areg;
  dlong *p1,*p2;
  dlong i2,i3;

  areg = *(areg_type *)(arg_place(locpc,2,bit_or));
  p1 = (dlong *)args(areg);
  deref(p1);

  areg = *(areg_type *)(arg_place(locpc,3,bit_or));
  p2 = (dlong *)args(areg);
  deref(p2);

  areg = *(areg_type *)(arg_place(locpc,1,bit_or));

  locpc += bit_or_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      i2 = get_smallint(p1);
      i3 = get_smallint(p2);
      build_smallint(&(args(areg)),i2|i3);
      goto_more_exec;
    }

  if ((! is_inumber(p1)) || (! is_inumber(p2)))
    goto arith_error_bit_or;

  {
    dlong res = or_two_inumbers(p1,p2,&locmach);
    if (! res) goto arith_error_bit_or;
    args(areg) = res;
  }
  goto_more_exec;
}


hcase(bit_xor)
bit_xor_label:
profile_builtin(bit_xor);
{
  dlong areg;
  dlong *p1,*p2;
  dlong i2,i3;

  areg = *(areg_type *)(arg_place(locpc,2,bit_xor));
  p1 = (dlong *)args(areg);
  deref(p1);

  areg = *(areg_type *)(arg_place(locpc,3,bit_xor));
  p2 = (dlong *)args(areg);
  deref(p2);

  areg = *(areg_type *)(arg_place(locpc,1,bit_xor));

  locpc += bit_xor_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      i2 = get_smallint(p1);
      i3 = get_smallint(p2);
      build_smallint(&(args(areg)),i2^i3);
      goto_more_exec;
    }

  if ((! is_inumber(p1)) || (! is_inumber(p2)))
    goto arith_error_bit_xor;

  {
    dlong res = xor_two_inumbers(p1,p2,&locmach);
    if (! res) goto arith_error_bit_xor;
    args(areg) = res;
  }
  goto_more_exec;
}

hcase(multiply)
multiply_label:
profile_builtin(multiply);
{
  dlong areg1, res;
  dlong *p2, *p3;

  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,multiply)));
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,multiply)));
  areg1 = *(areg_type *)(arg_place(locpc,1,multiply));
  locpc += multiply_len;

  deref(p3); deref(p2);

  if (is_smallint(p2) && is_smallint(p3))
    {
      dlong i,j;
      i = get_smallint(p3); j = get_smallint(p2);
      multiply_overflow_escape(i,j,res,mult2ints);
      build_smallint(&(args(areg1)),res);
      goto_more_exec;
    }

  if (is_real(p2) && is_real(p3))
    {
      dfloat f2, f3;
      f2 = get_real(p2);
      f3 = get_real(p3);

      build_real(&(args(areg1)),f2*f3,lochreg);
      goto_more_exec;
    }

  if (is_real(p2) || is_real(p3))
    {
      dfloat f2, f3;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_multiply;
      if (!get_float_from_number(p3,&f3,&locmach)) goto arith_error_multiply;

      build_real(&(args(areg1)),f2*f3,lochreg);
      goto_more_exec;
    }

 mult2ints:
  res = multiply_two_inumbers(p2,p3,&locmach);
  if (! res) goto arith_error_multiply;
  args(areg1) = res;
  goto_more_exec;
}

hcase(division)
division_label:
profile_builtin(division);
{
  dlong areg1, areg2, areg3;
  dlong *p2, *p3;
  dfloat f2, f3;

  areg2 = *(areg_type *)(arg_place(locpc,2,division));
  p2 = (dlong *)args(areg2);
  areg3 = *(areg_type *)(arg_place(locpc,3,division));
  p3 = (dlong *)args(areg3);
  areg1 = *(areg_type *)(arg_place(locpc,1,division));
  locpc += division_len;

  deref(p3);
  deref(p2);

  if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_division;
  if (!get_float_from_number(p3,&f3,&locmach)) goto arith_error_division;
  
  build_real(&(args(areg1)),f2/f3,lochreg);
  goto_more_exec;
}

hcase(idivision)
idivision_label:
profile_builtin(idivision);
{
  dlong areg;
  dlong i2,i3;
  dlong *p1, *p2;

  areg = *(areg_type *)(arg_place(locpc,2,idivision));
  p1 = (dlong *)args(areg);
  deref(p1);

  areg = *(areg_type *)(arg_place(locpc,3,idivision));
  p2 = (dlong *)args(areg);
  deref(p2);

  areg = *(areg_type *)(arg_place(locpc,1,idivision));
  locpc += idivision_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      i2 = get_smallint(p1);
      i3 = get_smallint(p2);
      if (! i3) goto arith_error_idivision;
      build_smallint(&(args(areg)),(i2/i3));
      goto_more_exec;
    }

  {
    dlong res;
    res = bigint_divide(p1,p2,DIVIDE,&locmach);
    if (! res) goto arith_error_idivision;
    args(areg) = res;
  }
  goto_more_exec;
}

hcase(idiv_integer)
idiv_integer_label:
profile_builtin(idiv_integer);
{
  dlong areg;
  dlong i2,i3;
  dlong *p;

  areg = *(areg_type *)(arg_place(locpc,2,idiv_integer));
  p = (dlong *)args(areg);
  deref(p);

  i3 = *(dlong *)(arg_place(locpc,3,idiv_integer));
  areg = *(areg_type *)(arg_place(locpc,1,idiv_integer));
  locpc += idiv_integer_len;
  if (! i3) goto arith_error_idiv_integer;
  if (is_smallint(p))
    {
      i2 = get_smallint(p);
      build_smallint(&(args(areg)),(i2/i3));
      goto_more_exec;
    }

  if (! is_bigint(p)) goto arith_error_idiv_integer;

  {
    dlong res = bigint_divide(p,(dlong *)make_smallint(i3),DIVIDE,&locmach);
    if (!res) goto arith_error_idiv_integer;
    args(areg) = res;
  }
  goto_more_exec;
}

hcase(logshiftl)
logshiftl_label:
profile_builtin(logshiftl);
{
  dlong areg, res;
  dlong i2,i3;
  dlong *p1,*p2;

  areg = *(areg_type *)(arg_place(locpc,2,logshiftl));
  p1 = (dlong *)args(areg);

  areg = *(areg_type *)(arg_place(locpc,3,logshiftl));
  p2 = (dlong *)args(areg);

  areg = *(areg_type *)(arg_place(locpc,1,logshiftl));
  locpc += logshiftl_len;

  res = shiftleft_two_inumbers(p1,p2,&locmach);
  if (! res)
    goto arith_error_logshiftl;

  args(areg) = res;
  goto_more_exec;
}

hcase(logshiftr)
logshiftr_label:
profile_builtin(logshiftr);
{
  dlong areg, res;
  dlong i2,i3;
  dlong *p1,*p2;

  areg = *(areg_type *)(arg_place(locpc,2,logshiftr));
  p1 = (dlong *)args(areg);

  areg = *(areg_type *)(arg_place(locpc,3,logshiftr));
  p2 = (dlong *)args(areg);

  areg = *(areg_type *)(arg_place(locpc,1,logshiftr));
  locpc += logshiftr_len;

  res = shiftright_two_inumbers(p1,p2,&locmach);
  if (! res)
    goto arith_error_logshiftr;

  args(areg) = res;
  goto_more_exec;
}

hcase(maximum)
maximum_label:
profile_builtin(maximum);
{
  dlong areg1, areg2, areg3;
  dlong *p2, *p3;

  areg2 = *(areg_type *)(arg_place(locpc,2,maximum));
  p2 = (dlong *)args(areg2);
  areg3 = *(areg_type *)(arg_place(locpc,3,maximum));
  p3 = (dlong *)args(areg3);
  areg1 = *(areg_type *)(arg_place(locpc,1,maximum));
  locpc += maximum_len;

  deref(p3);
  deref(p2);

  if (is_smallint(p2) && is_smallint(p3))
    {
      dlong i2;
      dlong i3;
      i2 = get_smallint(p2);
      i3 = get_smallint(p3);
      if (i2 > i3)
	args(areg1) = (dlong)p2;
      else
	args(areg1) = (dlong)p3;
      goto_more_exec;
    }

  if (is_real(p2) && is_real(p3))
    {
      dfloat f2, f3;
      f2 = get_real(p2);
      f3 = get_real(p3);
      if (f2 > f3)
	args(areg1) = (dlong)p2;
      else
	args(areg1) = (dlong)p3;
      goto_more_exec;
    }

  if (is_real(p2) || is_real(p3))
    {
      dfloat f2, f3;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_maximum;
      if (!get_float_from_number(p3,&f3,&locmach)) goto arith_error_maximum;
      if (f2 > f3)
	args(areg1) = (dlong)p2;
      else
	args(areg1) = (dlong)p3;
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p2,p3,&locmach, &er);
    
    if (er) goto arith_error_maximum;
    
    if (res > 0)
      args(areg1) = (dlong)p2;
    else
      args(areg1) = (dlong)p3;
  }
  goto_more_exec;
}

hcase(minimum)
minimum_label:
profile_builtin(minimum);
{
  dlong areg1, areg2, areg3;
  dlong *p2, *p3;

  areg2 = *(areg_type *)(arg_place(locpc,2,minimum));
  p2 = (dlong *)args(areg2);
  areg3 = *(areg_type *)(arg_place(locpc,3,minimum));
  p3 = (dlong *)args(areg3);
  areg1 = *(areg_type *)(arg_place(locpc,1,minimum));
  locpc += minimum_len;

  deref(p3);
  deref(p2);

  if (is_smallint(p2) && is_smallint(p3))
    {
      dlong i2;
      dlong i3;
      i2 = get_smallint(p2);
      i3 = get_smallint(p3);
      if (i2 < i3)
	args(areg1) = (dlong)p2;
      else
	args(areg1) = (dlong)p3;
      goto_more_exec;
    }

  if (is_real(p2) && is_real(p3))
    {
      dfloat f2, f3;
      f2 = get_real(p2);
      f3 = get_real(p3);
      if (f2 < f3)
	args(areg1) = (dlong)p2;
      else
	args(areg1) = (dlong)p3;
      goto_more_exec;
    }

  if (is_real(p2) || is_real(p3))
    {
      dfloat f2, f3;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_minimum;
      if (!get_float_from_number(p3,&f3,&locmach)) goto arith_error_minimum;
      if (f2 < f3)
	args(areg1) = (dlong)p2;
      else
	args(areg1) = (dlong)p3;
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p2,p3,&locmach, &er);
    
    if (er) goto arith_error_minimum;
    
    if (res < 0)
      args(areg1) = (dlong)p2;
    else
      args(areg1) = (dlong)p3;
  }
  goto_more_exec;
}

hcase(c_modulo)
c_modulo_label:
profile_builtin(c_modulo);
{
  dlong areg,i2,i3;
  dlong *p1,*p2;

  areg = *(areg_type *)(arg_place(locpc,2,c_modulo));
  p1 = (dlong *)args(areg);
  deref(p1);

  areg = *(areg_type *)(arg_place(locpc,3,c_modulo));
  p2 = (dlong *)args(areg);
  deref(p2);

  areg = *(areg_type *)(arg_place(locpc,1,c_modulo));
  locpc += c_modulo_len;

  if (is_smallint(p1) && is_smallint(p2))
     {
       i2 = get_smallint(p1);
       i3 = get_smallint(p2);
       if (! i3) goto arith_error_c_modulo;
       build_smallint(&(args(areg)),i2%i3);
       goto_more_exec;
     }

  if ((! is_inumber(p1)) || (! is_inumber(p2))) goto arith_error_c_modulo;
  if (is_smallint(p2))
    {
      i3 = get_smallint(p2);
      if (! i3) goto arith_error_c_modulo;
      i2 = bigint_divide(p1,p2,MODULO,&locmach);
      args(areg) = i2;
      goto_more_exec;
    }

  if (is_smallint(p1))
    {
      args(areg) = make_smallint(0);
      goto_more_exec;
    }

  i2 = bigint_divide(p1,p2,MODULO,&locmach);
  if (! i2) goto arith_error_c_modulo;
  args(areg) = i2;
  goto_more_exec;
}

hcase(modulo_integer)
modulo_integer_label:
profile_builtin(modulo_integer);
{
  dlong areg, i2,i3;
  dlong *p1;

  areg = *(areg_type *)(arg_place(locpc,2,modulo_integer));
  p1 = (dlong *)args(areg);
  deref(p1);

  i3 = *(dlong *)(arg_place(locpc,3,modulo_integer));

  areg = *(areg_type *)(arg_place(locpc,1,modulo_integer));

  locpc += modulo_integer_len;
  if (! i3) goto arith_error_modulo_integer;

  if (is_smallint(p1))
    {
      i2 = get_smallint(p1);
      build_smallint(&(args(areg)),i2%i3);
      goto_more_exec;
    }

  else if (! is_bigint(p1)) goto arith_error_modulo_integer;

  i2 = bigint_divide(p1,(dlong *)make_smallint(i3),MODULO,&locmach);
  args(areg) = i2;
  goto_more_exec;
}

hcase(float2intfloor)
float2intfloor_label:
profile(float2intfloor);
{
  dlong areg2, i;
  dlong *p1;
  dfloat f1;

  p1 = (dlong *)args(*(areg_type*)(arg_place(locpc,2,float2intfloor)));
  areg2 = *(areg_type*)(arg_place(locpc,1,float2intfloor));
  locpc += float2intfloor_len;
  deref(p1);
  if (is_inumber(p1)) { args(areg2) = (dlong)p1; goto_more_exec; }

  if (! is_real(p1)) goto arith_error_float2intfloor;

  f1 = floor(get_real(p1));
  if (in_smallint_range(f1))
    {
      i = f1;
      build_smallint(&(args(areg2)),i);
      goto_more_exec;
    }
  args(areg2) = float_to_bigint(get_real(p1),&locmach);
  goto_more_exec;
}

hcase(float2intceil)
float2intceil_label:
profile(float2intceil);
{
  dlong areg2, i;
  dlong *p1;
  dfloat f1;

  p1 = (dlong *)args(*(areg_type*)(arg_place(locpc,2,float2intceil)));
  areg2 = *(areg_type*)(arg_place(locpc,1,float2intceil));
  locpc += float2intceil_len;
  deref(p1);
  if (is_inumber(p1)) { args(areg2) = (dlong)p1; goto_more_exec; }

  if (! is_real(p1)) goto arith_error_float2intceil;

  f1 = ceil(get_real(p1));
  if (in_smallint_range(f1))
    {
      i = f1;
      build_smallint(&(args(areg2)),i);
      goto_more_exec;
    }
  args(areg2) = float_to_bigint(get_real(p1),&locmach);
  goto_more_exec;
}

hcase(float2inttrunc)
float2inttrunc_label:
profile(float2inttrunc);
{
  dlong areg2, i;
  dlong *p1;
  dfloat f1;

  p1 = (dlong *)args(*(areg_type*)(arg_place(locpc,2,float2inttrunc)));
  areg2 = *(areg_type*)(arg_place(locpc,1,float2inttrunc));
  locpc += float2inttrunc_len;
  deref(p1);
  if (is_inumber(p1)) { args(areg2) = (dlong)p1; goto_more_exec; }

  if (! is_real(p1)) goto arith_error_float2inttrunc;

  f1 = trunc(get_real(p1));
  if (in_smallint_range(f1))
    {
      i = f1;
      build_smallint(&(args(areg2)),i);
      goto_more_exec;
    }
  args(areg2) = float_to_bigint(get_real(p1),&locmach);
  goto_more_exec;
}

hcase(round2int)
round2int_label:
profile(round2int);
{
  dlong areg2, i;
  dlong *p1;
  dfloat f1;

  p1 = (dlong *)args(*(areg_type*)(arg_place(locpc,2,round2int)));
  areg2 = *(areg_type*)(arg_place(locpc,1,round2int));
  locpc += round2int_len;
  deref(p1);

  if (is_inumber(p1)) { args(areg2) = (dlong)p1; goto_more_exec; }

  if (! is_real(p1)) goto arith_error_round2int;

  f1 = rint(get_real(p1));
  if (in_smallint_range(f1))
    {
      i = f1;
      build_smallint(&(args(areg2)),i);
      goto_more_exec;
    }
  args(areg2) = float_to_bigint(get_real(p1),&locmach);
  goto_more_exec;
}

hcase(mostsigbit)
mostsigbit_label:
profile_builtin(mostsigbit);
{
  dlong areg1,j;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,2,mostsigbit));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,mostsigbit));

  locpc += mostsigbit_len;

  j = bigint_low_msb(p, &locmach);
  if (j < 0) goto arith_error_mostsigbit;
  build_smallint(&(args(areg1)),j);
  goto_more_exec;
}

hcase(negate)
negate_label:
profile_builtin(negate);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,2,negate));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,negate));

  locpc += negate_len;

  if (is_smallint(p))
    {
      dlong i = get_smallint(p);
      i = ~i;
      if (in_smallint_range(i))
	build_smallint(&(args(areg1)),i);
      else
	build_bigint_from_out_of_range_smallint(&(args(areg1)),i,lochreg);
      goto_more_exec;
    }

  if (!is_bigint(p))
    goto arith_error_negate;

  {
    dlong res = addsub_two_inumbers(p,(dlong *)make_smallint(1),1,&locmach);

    if (is_smallint(res))
      build_smallint(&(args(areg1)),-(get_smallint(res)));
    else
      {
	p = (dlong *)res;
	p = get_number_pointer(p);
	p++;
	// invert sign
	invert_bigint_sign(p,*p);
	args(areg1) = res;
      }
  goto_more_exec;
  }
}

hcase(min1)
min1_label:
profile_builtin(min1);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,2,min1));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,min1));

  locpc += min1_len;

  if (is_smallint(p))
    build_smallint(&(args(areg1)),-(get_smallint(p)));
  else if (is_real(p))
    { build_real(&(args(areg1)),-(get_real(p)),lochreg); }
  else if (is_bigint(p))
    args(areg1) = bigint_low_abs(p,&locmach,0);
  else goto arith_error_min1;
  goto_more_exec;
}


hcase(mysqrt)
mysqrt_label:
profile_builtin(mysqrt);
{
  dlong areg1;
  dlong *p;
  dfloat f1;

  areg1 = *(areg_type *)(arg_place(locpc,2,mysqrt));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,mysqrt));

  locpc += mysqrt_len;

  if (is_smallint(p))
    f1 = get_smallint(p);
  else
    if (is_real(p))
      f1 = get_real(p);
    else
      if (is_bigint(p))
	{
	  if (!get_float_from_number(p,&f1,&locmach))
	    goto arith_error_mysqrt;
	}
      else
	goto arith_error_mysqrt;

  f1 = sqrt(f1);

  build_real(&(args(areg1)),f1,lochreg);
  goto_more_exec;
}

hcase(myatan)
myatan_label:
profile_builtin(myatan);
{
  dlong areg1;
  dlong *p;
  dfloat f1;

  areg1 = *(areg_type *)(arg_place(locpc,2,myatan));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,myatan));

  locpc += myatan_len;

  if (is_smallint(p))
    f1 = get_smallint(p);
  else
    if (is_real(p))
      f1 = get_real(p);
    else
      if (is_bigint(p))
	{
	  if (!get_float_from_number(p,&f1,&locmach))
	    goto arith_error_myatan;
	}
      else
	goto arith_error_myatan;

  f1 = atan(f1);

  build_real(&(args(areg1)),f1,lochreg);
  goto_more_exec;
}

hcase(mysin)
mysin_label:
profile_builtin(mysin);
{
  dlong areg1;
  dlong *p;
  dfloat f1;

  areg1 = *(areg_type *)(arg_place(locpc,2,mysin));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,mysin));

  locpc += mysin_len;

  if (is_smallint(p))
    f1 = get_smallint(p);
  else
    if (is_real(p))
      f1 = get_real(p);
    else
      if (is_bigint(p))
	{
	  if (!get_float_from_number(p,&f1,&locmach))
	    goto arith_error_mysin;
	}
      else
	goto arith_error_mysin;

  f1 = sin(f1);

  build_real(&(args(areg1)),f1,lochreg);
  goto_more_exec;
}

hcase(mycos)
mycos_label:
profile_builtin(mycos);
{
  dlong areg1;
  dlong *p;
  dfloat f1;

  areg1 = *(areg_type *)(arg_place(locpc,2,mycos));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,mycos));

  locpc += mycos_len;

  if (is_smallint(p))
    f1 = get_smallint(p);
  else
    if (is_real(p))
      f1 = get_real(p);
    else
      if (is_bigint(p))
	{
	  if (!get_float_from_number(p,&f1,&locmach))
	    goto arith_error_mycos;
	}
      else
	goto arith_error_mycos;
  
  f1 = cos(f1);

  build_real(&(args(areg1)),f1,lochreg);
  goto_more_exec;
}

hcase(mylog)
mylog_label:
profile_builtin(mylog);
{
  dlong areg1;
  dlong *p;
  dfloat f1;

  areg1 = *(areg_type *)(arg_place(locpc,2,mylog));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,mylog));

  locpc += mylog_len;

  if (is_smallint(p))
    f1 = get_smallint(p);
  else
    if (is_real(p))
      f1 = get_real(p);
    else
      if (is_bigint(p))
	{
	  if (!get_float_from_number(p,&f1,&locmach))
	    goto arith_error_mylog;
	}
      else
	goto arith_error_mylog;
  
  f1 = log(f1);

  build_real(&(args(areg1)),f1,lochreg);
  goto_more_exec;
}

hcase(myexp)
myexp_label:
profile_builtin(myexp);
{
  dlong areg1;
  dlong *p;
  dfloat f1;

  areg1 = *(areg_type *)(arg_place(locpc,2,myexp));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,myexp));

  locpc += myexp_len;

  if (is_smallint(p))
    f1 = get_smallint(p);
  else
    if (is_real(p))
      f1 = get_real(p);
    else
      if (is_bigint(p))
	{
	  if (!get_float_from_number(p,&f1,&locmach))
	    goto arith_error_myexp;
	}
      else
	goto arith_error_myexp;
  
  f1 = exp(f1);

  build_real(&(args(areg1)),f1,lochreg);
  goto_more_exec;
}

hcase(myabs)
myabs_label:
profile_builtin(myabs);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,2,myabs));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,myabs));

  locpc += myabs_len;

  if (is_smallint(p))
    {
      dlong i = get_smallint(p);
      if (i < 0)
	{
	  i = -i;
	  args(areg1) = make_smallint(i);
	}
      else
	args(areg1) = (dlong)p;
      goto_more_exec;
    }

  if (is_real(p))
    {
      dfloat f1;
      f1 = get_real(p);
      if (f1 < 0)
	{
	  f1 = -f1;
	  build_real(&(args(areg1)),f1,lochreg);
	}
      else
	args(areg1) = (dlong)p;
      goto_more_exec;
    }      

  if (is_bigint(p))
    {
      args(areg1) = bigint_low_abs(p,&locmach,1);
      goto_more_exec;
    }

  goto arith_error_myabs;
}

hcase(mysign)
mysign_label:
profile_builtin(mysign);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,2,mysign));
  p = (dlong *)args(areg1);
  deref(p);

  areg1 = *(areg_type *)(arg_place(locpc,1,mysign));

  locpc += mysign_len;

  if (is_smallint(p))
    {
      dlong i = get_smallint(p);
      if (i < 0) args(areg1) = make_smallint(-1);
      else if (i > 0) args(areg1) = make_smallint(1);
      else args(areg1) = make_smallint(0);
      goto_more_exec;
    }

  if (is_real(p))
    {
      dfloat f1;
      f1 = get_real(p);
      if (f1 < 0) args(areg1) = make_smallint(-1);
      else if (f1 > 0) args(areg1) = make_smallint(1);
      else args(areg1) = make_smallint(0);
      goto_more_exec;
    }      

  if (is_bigint(p))
    {
      p = get_number_pointer(p)+1;
      if (bigint_positive(*p)) args(areg1) = make_smallint(1);
      else args(areg1) = make_smallint(-1);
      goto_more_exec;
    }

  goto arith_error_mysign;
}

hcase(listmax1)
listmax1_label:
profile(listmax1);
{
  dlong areg1;
  dlong areg2;
  dlong *locmax, *p, *el;
  dlong mustbeint;

  areg1 = *(areg_type*)(arg_place(locpc,1,listmax1));
  areg2 = *(areg_type*)(arg_place(locpc,2,listmax1));

  locpc += listmax1_len;

  p = (dlong *)args(areg1);
  deref(p);

  if (is_list(p))
    {
      p = get_list_pointer(p);
      locmax = (dlong *)p[0];
      deref(locmax);
      if (! is_number(locmax)) goto_fail_label;
      if (is_real(locmax)) mustbeint = 0;
      else mustbeint = 1;
      p++; deref(p)
    }
  else goto_fail_label;

  while (is_list(p))
    {
      p = get_list_pointer(p);
      el = (dlong *)p[0];
      deref(el); if (! is_number(el)) goto_fail_label;
      if (mustbeint)
	{
	  if (is_real(el)) goto_fail_label;
	  if (is_smallint(el) && is_smallint(locmax))
	    {
	      if (get_smallint(el) > get_smallint(locmax))
		locmax = el;
	    }
	  else
	    {
	      dlong res, er;
	      res = compare_two_inumbers(el,locmax,&locmach,&er);
	      if (er) goto_fail_label;
	      if (res > 0)
		locmax = el;
	    }
	}
      else
	{
	  dfloat f1, f2;
	  if (!is_real(el)) goto_fail_label;
	  if (!get_float_from_number(el,&f1,&locmach)) goto_fail_label;
	  if (!get_float_from_number(locmax,&f2,&locmach)) goto_fail_label;
	  if (f1 > f2) locmax = el;
	}
      p = (dlong *)p[1];
      deref(p);      
    }

  if ((dlong)p != nil_atom) goto_fail_label;

  args(areg2) = (dlong)locmax;
  goto_more_exec;
}

hcase(listmin1)
listmin1_label:
profile(listmin1);
{
  dlong areg1;
  dlong areg2;
  dlong *locmin, *p, *el;
  dlong mustbeint;

  areg1 = *(areg_type*)(arg_place(locpc,1,listmin1));
  areg2 = *(areg_type*)(arg_place(locpc,2,listmin1));

  locpc += listmin1_len;

  p = (dlong *)args(areg1);
  deref(p);

  if (is_list(p))
    {
      p = get_list_pointer(p);
      locmin = (dlong *)p[0];
      deref(locmin);
      if (! is_number(locmin)) goto_fail_label;
      if (is_real(locmin)) mustbeint = 0;
      else mustbeint = 1;
      p++; deref(p)
    }
  else goto_fail_label;

  while (is_list(p))
    {
      p = get_list_pointer(p);
      el = (dlong *)p[0];
      deref(el); if (! is_number(el)) goto_fail_label;
      if (mustbeint)
	{
	  if (is_real(el)) goto_fail_label;
	  if (is_smallint(el) && is_smallint(locmin))
	    {
	      if (get_smallint(el) < get_smallint(locmin))
		locmin = el;
	    }
	  else
	    {
	      dlong res, er;
	      res = compare_two_inumbers(el,locmin,&locmach,&er);
	      if (er) goto_fail_label;
	      if (res < 0)
		locmin = el;
	    }
	}
      else
	{
	  dfloat f1, f2;
	  if (!is_real(el)) goto_fail_label;
	  if (!get_float_from_number(el,&f1,&locmach)) goto_fail_label;
	  if (!get_float_from_number(locmin,&f2,&locmach)) goto_fail_label;
	  if (f1 < f2) locmin = el;
	}
      p = (dlong *)p[1];
      deref(p);      
    }

  if ((dlong)p != nil_atom) goto_fail_label;

  args(areg2) = (dlong)locmin;
  goto_more_exec;
}

hcase(listlen1)
listlen1_label:
profile(listlen1);
{
  dlong areg1, i;
  dlong areg2;
  dlong *p;

  areg1 = *(areg_type*)(arg_place(locpc,1,listlen1));
  areg2 = *(areg_type*)(arg_place(locpc,2,listlen1));

  locpc += listlen1_len;

  p = (dlong *)args(areg2);
  deref(p);

  i = 0;
  while (is_list(p))
    {
      i++;
      p = get_list_pointer(p);
      p++; deref(p);
    }

  if ((dlong)p != nil_atom) goto_fail_label;

  build_smallint(&(args(areg1)),i);

  goto_more_exec;
}

hcase(listsum1)
listsum1_label:
profile(listsum1);
{
  dlong areg1;
  dlong areg2;
  mpz_t intsum;
  dlong *p, *el;
  dlong mustbeint;
  dfloat floatsum;

  areg1 = *(areg_type*)(arg_place(locpc,1,listsum1));
  areg2 = *(areg_type*)(arg_place(locpc,2,listsum1));

  locpc += listsum1_len;

  p = (dlong *)args(areg2);
  deref(p);

  if ((dlong)p == nil_atom)
    {
      args(areg1) = make_smallint(0);
      goto_more_exec;
    }

  if (! is_list(p)) goto_fail_label;

  p = get_list_pointer(p);
  p = (dlong *)p[0];
  deref(p);
  if (! is_number(p)) goto_fail_label;
  if (is_real(p)) { mustbeint = 0; floatsum = 0.0; }
  else { mustbeint = 1; init_intsum(&intsum); }

  p = (dlong *)args(areg2);
  deref(p);

  if (mustbeint)
    {
      while (is_list(p))
	{
	  p = get_list_pointer(p);
	  el = (dlong *)p[0];
	  deref(el);
	  if (! is_inumber(el)) goto_fail_label;
	  // add_mpz_hint(intsum, el, &locmach);
	  add_ints(&intsum, el, &locmach);
	  p = (dlong *)p[1];
	  deref(p);
	}
      args(areg1) = bigint_result(intsum, &locmach);
      if ((dlong)p != nil_atom) goto_fail_label;
      goto_more_exec;
    }

  while (is_list(p))
    {
      dfloat f1;
      p = get_list_pointer(p);
      el = (dlong *)p[0];
      deref(el);
      if (!is_real(el)) goto_fail_label;
      if (!get_float_from_number(el,&f1,&locmach)) goto_fail_label;
      floatsum += f1;
      p = (dlong *)p[1];
      deref(p);      
    }
  if ((dlong)p != nil_atom) goto_fail_label;

  build_real(&(args(areg1)),floatsum,lochreg);
  goto_more_exec;
}

hcase(put_int)
put_int_label:
profile(put_int);
{
  dlong areg1;
  dlong int2;

  areg1 = *(areg_type *)(arg_place(locpc,1,put_int));
  int2 = *(dlong *)(arg_place(locpc,2,put_int));
  locpc += put_int_len;
  args(areg1) = int2;
  goto_more_exec;
}


hcase(put_int2)
put_int2_label:
profile(put_int2);
{
  dlong areg1;
  dlong int2;

  areg1 = *(areg_type *)(arg_place(locpc,1,put_int2));
  int2 = *(dlong *)(arg_place(locpc,3,put_int2));
  args(areg1) = int2;

  areg1 = *(areg_type *)(arg_place(locpc,2,put_int2));
  int2 = *(dlong *)(arg_place(locpc,4,put_int2));
  locpc += put_int2_len;
  args(areg1) = int2;

  goto_more_exec;
}


hcase(put_int3)
put_int3_label:
profile(put_int3);
{
  dlong areg1;
  dlong int2;

  areg1 = *(areg_type *)(arg_place(locpc,1,put_int3));
  int2 = *(dlong *)(arg_place(locpc,4,put_int3));
  args(areg1) = int2;

  areg1 = *(areg_type *)(arg_place(locpc,2,put_int3));
  int2 = *(dlong *)(arg_place(locpc,5,put_int3));
  args(areg1) = int2;

  areg1 = *(areg_type *)(arg_place(locpc,3,put_int3));
  int2 = *(dlong *)(arg_place(locpc,6,put_int3));
  locpc += put_int3_len;
  args(areg1) = int2;

  goto_more_exec;
}

hcase(put_int4)
put_int4_label:
profile(put_int4);
{
  dlong areg1;
  dlong int2;

  areg1 = *(areg_type *)(arg_place(locpc,1,put_int4));
  int2 = *(dlong *)(arg_place(locpc,5,put_int4));
  args(areg1) = int2;

  areg1 = *(areg_type *)(arg_place(locpc,2,put_int4));
  int2 = *(dlong *)(arg_place(locpc,6,put_int4));
  args(areg1) = int2;

  areg1 = *(areg_type *)(arg_place(locpc,3,put_int4));
  int2 = *(dlong *)(arg_place(locpc,7,put_int4));
  args(areg1) = int2;

  areg1 = *(areg_type *)(arg_place(locpc,4,put_int4));
  int2 = *(dlong *)(arg_place(locpc,8,put_int4));
  locpc += put_int4_len;
  args(areg1) = int2;

  goto_more_exec;
}


hcase(put_atom)
put_atom_label:
profile(put_atom);
{
  dlong areg1;
  dlong atom2;

  areg1 = *(areg_type *)(arg_place(locpc,1,put_atom));
  atom2 = *(dlong *)(arg_place(locpc,2,put_atom));
  locpc += put_atom_len;

  args(areg1) = atom2;
  goto_more_exec;
}


hcase(getstrv)
getstrv_label:
profile(getstrv);
{
  dlong yvar1;
  dlong struct2;
  dlong *p;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,getstrv));
  struct2 = *(dlong *)(arg_place(locpc,2,getstrv));

  p = (dlong *)ereg[yvar1];
  locpc += getstrv_len;
  deref2(p,goto get_structv_is_undef);
  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      if (*p != struct2)
	goto_fail_label
	  else
	    WAMS = p+1;
      goto_more_exec;
    }

  if (! is_attributed(p))
    goto_fail_label;

  safe_wakeup(p,(dlong *)(make_struct_p(lochreg)),&locmach);
  goto rest_of_get_strv_write;

 get_structv_is_undef:
  trail(p,bh,loctrreg);
  *p = make_struct_p(lochreg);
 rest_of_get_strv_write:
  makeSzero;
  *lochreg = struct2;
  lochreg++;
  goto_more_exec;
}

hcase(set_string)
set_string_label:
profile(set_string);
{
  dlong offset;
  offset = *(dlong *)(arg_place(locpc,1,set_string));
  locpc += set_string_len;
  *lochreg = make_string_pointer((lochreg+offset));
  lochreg++;
  goto_more_exec;
}

hcase(get_structure)
get_structure_label:
profile(get_structure);
{
  dlong areg1;
  dlong struct2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,get_structure));
  struct2 = *(dlong *)(arg_place(locpc,2,get_structure));

  p = (dlong *)args(areg1);
  locpc += get_structure_len;
  deref2(p,goto get_structure_is_undef);
  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      if (*p != struct2)
	goto_fail_label
	  else
	    WAMS = p+1;
      goto_more_exec;
    }

  if (! is_attributed(p))
    goto_fail_label;

  safe_wakeup(p,(dlong *)(make_struct_p(lochreg)),&locmach);
  goto rest_of_get_struc_write;

 get_structure_is_undef:
  trail(p,bh,loctrreg);
  *p = make_struct_p(lochreg);
 rest_of_get_struc_write:
  makeSzero;
  *lochreg = struct2;
  lochreg++;
  goto_more_exec;
}

hcase(put_structure)
put_structure_label:
profile(put_structure);
{
  dlong areg1;
  dlong struct2;

  areg1 = *(areg_type *)(arg_place(locpc,1,put_structure));
  struct2 = *(dlong *)(arg_place(locpc,2,put_structure));
  locpc += put_structure_len;

  args(areg1) = make_struct_p(lochreg);
  *lochreg = struct2;
  lochreg++;
  goto_more_exec;
}

hcase(getstr_tvar)
getstr_tvar_label:
profile(getstr_tvar);
{
  register dlong *p;
  register dlong areg;

  areg = *(areg_type *)(arg_place(locpc,1,getstr_tvar));
  p = (dlong *)args(areg);
  deref2(p,goto getstr_tvar_is_undef);
  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      WAMS = p+2;
      if (*p != *(dlong *)(arg_place(locpc,3,getstr_tvar)))
	goto_fail_label;
      areg = *(areg_type *)(arg_place(locpc,2,getstr_tvar));
      locpc += getstr_tvar_len;
      args(areg) = *(p+1);
      goto_more_exec;
    }
  if (! is_attributed(p))
    goto_fail_label;

  safe_wakeup(p,(dlong *)(make_struct_p(lochreg)),&locmach);
  goto rest_of_getstr_tvar_write;

 getstr_tvar_is_undef:
  trail(p,bh,loctrreg);
  *p = make_struct_p(lochreg);
 rest_of_getstr_tvar_write:
  makeSzero;
  p = lochreg;
  lochreg = p+2;
  *p = *(dlong *)(arg_place(locpc,3,getstr_tvar));
  areg = *(areg_type *)(arg_place(locpc,2,getstr_tvar));
  args(areg) = (dlong)(++p);
  locpc += getstr_tvar_len;
  *p = (dlong)p;
  goto_more_exec;
}

hcase(getstr_tvar_tvar)
getstr_tvar_tvar_label:
profile(getstr_tvar_tvar);
{
  register dlong *p;
  register dlong areg;

  areg = *(areg_type *)(arg_place(locpc,1,getstr_tvar_tvar));
  p = (dlong *)args(areg);
  deref2(p,goto getstr_tvar_tvar_is_undef);
  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      WAMS = p+3;
      if (*p != *(dlong *)(arg_place(locpc,4,getstr_tvar_tvar)))
	goto_fail_label;
      areg = *(areg_type *)(arg_place(locpc,2,getstr_tvar_tvar));
      args(areg) = *(p+1);
      areg = *(areg_type *)(arg_place(locpc,3,getstr_tvar_tvar));
      locpc += getstr_tvar_tvar_len;
      args(areg) = *(p+2);
      goto_more_exec;
    }
  if (! is_attributed(p))
    goto_fail_label;

  safe_wakeup(p,(dlong *)(make_struct_p(lochreg)),&locmach);
  goto rest_of_getstr_tvar_tvar_write;

 getstr_tvar_tvar_is_undef:
  trail(p,bh,loctrreg);
  *p = make_struct_p(lochreg);
 rest_of_getstr_tvar_tvar_write:
  makeSzero;
  p = lochreg;
  lochreg = p+3;
  *p = *(dlong *)(arg_place(locpc,4,getstr_tvar_tvar));
  areg = *(areg_type *)(arg_place(locpc,2,getstr_tvar_tvar));
  args(areg) = (dlong)(++p);
  *p = (dlong)p;
  areg = *(areg_type *)(arg_place(locpc,3,getstr_tvar_tvar));
  args(areg) = (dlong)(++p);
  *p = (dlong)p;
  locpc += getstr_tvar_tvar_len;
  goto_more_exec;
}

hcase(put_structurev)
put_structurev_label:
profile(put_structurev);
{
  dlong yvar1;
  dlong struct2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,put_structurev));
  struct2 = *(dlong *)(arg_place(locpc,2,put_structurev));
  locpc += put_structurev_len;

  ereg[yvar1] = make_struct_p(lochreg);
  *lochreg = struct2;
  lochreg++;
  goto_more_exec;
}

hcase(unipvar)
unipvar_label:
profile(unipvar);
{
  if (WAMS)
    {
      dlong yvar1;

      yvar1 = *(yvar_type *)(arg_place(locpc,1,unipvar));
      locpc += unipvar_len;

      ereg[yvar1] = *WAMS;
      WAMS++;
      goto_more_exec;
    }
  else
    {
      dlong yvar1;

      yvar1 = *(yvar_type *)(arg_place(locpc,1,unipvar));
      locpc += unipvar_len;

      *lochreg = ereg[yvar1] = (dlong)lochreg;
      lochreg++;
      goto_more_exec;
    }
}

hcase(unipvarpvar)
unipvarpvar_label:
profile(unipvarpvar);
{
  if (WAMS)
    {
      dlong yvar1;

      yvar1 = *(yvar_type *)(arg_place(locpc,1,unipvarpvar));
      ereg[yvar1] = *WAMS;
      WAMS++;
      yvar1 = *(yvar_type *)(arg_place(locpc,2,unipvarpvar));
      locpc += unipvarpvar_len;
      ereg[yvar1] = *WAMS;
      WAMS++;
      goto_more_exec;
    }
  else
    {
      dlong yvar1;

      yvar1 = *(yvar_type *)(arg_place(locpc,1,unipvarpvar));
      *lochreg = ereg[yvar1] = (dlong)lochreg;
      lochreg++;

      yvar1 = *(yvar_type *)(arg_place(locpc,2,unipvarpvar));
      locpc += unipvarpvar_len;
      *lochreg = ereg[yvar1] = (dlong)lochreg;
      lochreg++;

      goto_more_exec;
    }
}

hcase(unipval)
unipval_label:
profile(unipval);
{
  if (WAMS)
    {
      dlong yvar1;
      dlong *p;

      yvar1 = *(yvar_type *)(arg_place(locpc,1,unipval));
      locpc += unipval_len;

      p = (dlong *)ereg[yvar1];
      if (! unify_terms(WAMS,p,&locmach))
	goto_fail_label;
      WAMS++;
      goto_more_exec;
    }
  else
    {
      dlong yvar1;
      dlong *p;

      yvar1 = *(yvar_type *)(arg_place(locpc,1,unipval));
      locpc += unipval_len;

      p = (dlong *)ereg[yvar1];
      deref(p); underef(p);
      *lochreg = (dlong)p;
      lochreg++;
      goto_more_exec;
    }
}

hcase(bldpvar)
bldpvar_label:
profile(bldpvar);
{
  dlong yvar1;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,bldpvar));
  locpc += bldpvar_len;

  *lochreg = ereg[yvar1] = (dlong)lochreg;
  lochreg++;
  goto_more_exec;
}

hcase(bldpval)
bldpval_label:
profile(bldpval);
{
  dlong yvar1;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,bldpval));
  locpc += bldpval_len;

  *lochreg = ereg[yvar1];
//   {
//     dlong *p = ereg[yvar1];
//     deref(p);
//     *lochreg = p;
//   }
  lochreg++;
  goto_more_exec;
}

hcase(bldpval2)
bldpval2_label:
profile(bldpval2);
{
  dlong yvar1;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,bldpval2));
  *lochreg = ereg[yvar1];
  lochreg++;
  yvar1 = *(yvar_type *)(arg_place(locpc,2,bldpval2));
  locpc += bldpval2_len;

  *lochreg = ereg[yvar1];
  lochreg++;
  goto_more_exec;
}

hcase(getpbreg)
getpbreg_label:
profile(getpbreg);
{
  dlong yvar1;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,getpbreg));
  locpc += getpbreg_len;

  ereg[yvar1] = make_cut_point(locmach.begin_cp,locbreg);
  goto_more_exec;
}

hcase(putpbreg)
putpbreg_label:
profile(putpbreg);
{
  dlong yvar1;
  dlong *p, *q;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,putpbreg));
  locpc += putpbreg_len;
  q = (dlong *)ereg[yvar1];
  deref(q);

  p = get_cut_point(locmach.begin_cp, (dlong)q);
  if (locbreg == p) goto_more_exec;

  loctrreg = tidy_trail(p,locbreg,loctrreg,lochreg);
  locbreg = p;
  freeze_heap();

  bh = ((CP_P)locbreg)->h;
  /* recompute TOS */
  recompute_tos(ereg,locbreg);
  goto_more_exec;
}

hcase(allocate)
allocate_label:
profile(allocate);
{
  dlong yvar1;
  dlong *loctos;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,allocate));
  locpc += allocate_len;

  loctos = tos;
  tos = loctos = loctos - yvar1;
  *loctos = (dlong)ereg;
  ereg = loctos;
  if (loctos < ls_limit)
    loctos = deal_with_ls_overflow(&locmach);
  *(loctos+1) = (dlong)contpreg;
  
  goto_more_exec;
}

hcase(allocategetpvar)
allocategetpvar_label:
profile(allocategetpvar);
{
  dlong yvar1;
  dlong areg2;
  dlong *loctos;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,allocategetpvar));

  loctos = tos;
  tos = loctos = loctos - yvar1;
  *loctos = (dlong)ereg;
  ereg = loctos;
  if (loctos < ls_limit)
    loctos = deal_with_ls_overflow(&locmach);
  *(loctos+1) = (dlong)contpreg;

  yvar1 = *(yvar_type *)(arg_place(locpc,2,allocategetpvar));
  areg2 = *(areg_type *)(arg_place(locpc,3,allocategetpvar));

  locpc += allocategetpvar_len;
  ereg[yvar1] = args(areg2);

  goto_more_exec;
}

hcase(unitvar)
unitvar_label:
profile(unitvar);
{
  if (WAMS)
    {
      dlong areg1;

      areg1 = *(areg_type *)(arg_place(locpc,1,unitvar));
      locpc += unitvar_len;

      args(areg1) = *WAMS;
      WAMS++;
      goto_more_exec;
    }
  else
    {
      dlong areg1;

      areg1 = *(areg_type *)(arg_place(locpc,1,unitvar));
      locpc += unitvar_len;

      *lochreg = args(areg1) = (dlong)lochreg;
      lochreg++;
      goto_more_exec;
    }
}

hcase(unitval)
unitval_label:
profile(unitval);
{
  if (WAMS)
    {
      dlong areg1;
      dlong *p1, *p2;

      areg1 = *(areg_type *)(arg_place(locpc,1,unitval));

      p1 = (dlong *)args(areg1); deref(p1);
      locpc += unitval_len;
      p2 = WAMS++; deref(p2);

      pre_unify_terms(p1,p2,continue_unitval);

    continue_unitval:
      goto_more_exec;
    }

  {
    dlong areg1;
    dlong *p;

    areg1 = *(areg_type *)(arg_place(locpc,1,unitval));
	    
    p = (dlong *)args(areg1);
    locpc += unitval_len;

    deref(p); underef(p);
    *lochreg = (dlong)p;
    lochreg++;
    goto_more_exec;
  }
}

hcase(unify_list)
unify_list_label:
profile(unify_list);
{
  if (!WAMS)
    {
      locpc += unify_list_len;
      *lochreg = make_list(lochreg+1);
      lochreg++;
      goto_more_exec;
    }
  else
    {
      dlong *p;
      locpc += unify_list_len;
      p = WAMS;
      deref2(p,goto unify_list_is_undef);
      if (is_list(p))
	{
	  WAMS = get_list_pointer(p);
	  goto_more_exec ;
	}
      if (! is_attributed(p))
	goto_fail_label;

      safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
      makeSzero;
      goto_more_exec;

    unify_list_is_undef:
      makeSzero;
      *p = make_list(lochreg);
      trail(p,bh,loctrreg);
      goto_more_exec;
    }
}


hcase(unify_list_tvar)
unify_list_tvar_label:
profile(unify_list_tvar);
{
  if (!WAMS)
    {
      dlong areg1;
      areg1 = *(areg_type *)(arg_place(locpc,1,unify_list_tvar));
      locpc += unify_list_tvar_len;
      *lochreg = make_list(lochreg+1);
      lochreg++;
      *lochreg = args(areg1) = (dlong)lochreg;
      lochreg++;
      goto_more_exec;
    }
  else
    {
      dlong *p;
      dlong areg1;
      areg1 = *(areg_type *)(arg_place(locpc,1,unify_list_tvar));
      locpc += unify_list_tvar_len;
      p = WAMS;
      deref2(p,goto unify_list_tvar_is_undef);
      if (is_list(p))
	{
	  WAMS = get_list_pointer(p);
	  args(areg1) = *WAMS;
	  WAMS++;
	  goto_more_exec ;
	}
      if (! is_attributed(p))
	goto_fail_label;

      safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
      makeSzero;
      *lochreg = args(areg1) = (dlong)lochreg;
      lochreg++;
      goto_more_exec;

    unify_list_tvar_is_undef:
      makeSzero;
      *p = make_list(lochreg);
      *lochreg = args(areg1) = (dlong)lochreg;
      lochreg++;
      trail(p,bh,loctrreg);
      goto_more_exec;
    }
}


hcase(putlist)
putlist_label:
profile(putlist);
{
  dlong areg1;

  areg1 = *(areg_type *)(arg_place(locpc,1,putlist));
  locpc += putlist_len;

  args(areg1) = make_list(lochreg);
  goto_more_exec;
}

hcase(bldtvar)
bldtvar_label:
profile(bldtvar);
{
  dlong areg1;

  areg1 = *(areg_type *)(arg_place(locpc,1,bldtvar));
  locpc += bldtvar_len;

  *lochreg = args(areg1) = (dlong)lochreg;
  lochreg++;
  goto_more_exec;
}

hcase(swap)
swap_label:
profile(swap);
{
  dlong areg1;
  dlong areg2;
  dlong p;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,swap));
  areg2 = *(areg_type*)(arg_place(locpc,2,swap));
  locpc += swap_len;
  p = args(areg1);
  args(areg1) = args(areg2);
  args(areg2) = p;
  
  goto_more_exec;
}

hcase(bldtvar2)
bldtvar2_label:
profile(bldtvar2);
{
  dlong areg1;
  dlong *heaptop = lochreg;

  lochreg = heaptop+2;

  areg1 = *(areg_type *)(arg_place(locpc,1,bldtval2));
  *heaptop = args(areg1) = (dlong)heaptop;

  areg1 = *(areg_type *)(arg_place(locpc,2,bldtval2));
  locpc += bldtval2_len;
  heaptop[1] = args(areg1) = (dlong)(heaptop+1);

  goto_more_exec;
}

hcase(bldtval)
bldtval_label:
profile(bldtval);
{
  dlong areg1;

  areg1 = *(areg_type *)(arg_place(locpc,1,bldtval));
  locpc += bldtval_len;

  *lochreg = args(areg1);
  lochreg++;
  goto_more_exec;
}

hcase(bldtval2)
bldtval2_label:
profile(bldtval2);
{
  dlong areg1;
  dlong *heaptop = lochreg;

  lochreg = heaptop+2;

  areg1 = *(areg_type *)(arg_place(locpc,1,bldtval2));
  *heaptop = args(areg1);

  areg1 = *(areg_type *)(arg_place(locpc,2,bldtval2));
  locpc += bldtval2_len;
  heaptop[1] = args(areg1);

  goto_more_exec;
}

hcase(bldtval3)
bldtval3_label:
profile(bldtval3);
{
  dlong areg1;
  dlong *heaptop = lochreg;

  lochreg = heaptop+3;

  areg1 = *(areg_type *)(arg_place(locpc,1,bldtval3));
  *heaptop = args(areg1);

  areg1 = *(areg_type *)(arg_place(locpc,2,bldtval3));
  heaptop[1] = args(areg1);

  areg1 = *(areg_type *)(arg_place(locpc,3,bldtval3));
  locpc += bldtval3_len;
  heaptop[2] = args(areg1);

  goto_more_exec;
}

hcase(gettbreg)
gettbreg_label:
profile(gettbreg);
{
  dlong areg1;

  areg1 = *(areg_type *)(arg_place(locpc,1,gettbreg));
  locpc += gettbreg_len;

  args(areg1) = make_cut_point(locmach.begin_cp,locbreg);
  goto_more_exec;
}

hcase(puttbreg)
puttbreg_label:
profile(puttbreg);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,puttbreg));
  locpc += puttbreg_len;
  p = (dlong *)args(areg1);
  deref(p);

  p = get_cut_point(locmach.begin_cp, (dlong)p);
  if (locbreg == p) goto_more_exec;

  loctrreg = tidy_trail(p,locbreg,loctrreg,lochreg);
  locbreg = p;
  freeze_heap();

  bh = ((CP_P)locbreg)->h;
  /* recompute TOS */
  recompute_tos(ereg,locbreg);
  goto_more_exec;
}


hcase(builtin_cut1fail_0)
builtin_cut1fail_0_label:
profile(builtin_cut1fail_0);
{
  locbreg = ((CP_P)locbreg)->b;
  goto_fail_label;
}


hcase(uni_tvar_tval)
uni_tvar_tval_label:
profile(uni_tvar_tval);
{
  if (WAMS)
    {
      dlong areg, *p1, *p2;

      areg = *(areg_type *)(arg_place(locpc,1,uni_tvar_tval));
      args(areg) = *WAMS++;

      areg = *(areg_type *)(arg_place(locpc,2,uni_tvar_tval));
      locpc += uni_tvar_tval_len;
      p1 = (dlong *)args(areg); deref(p1);
      p2 = (dlong *)*WAMS++; deref(p2);
      pre_unify_terms(p1,p2,uni_tvar_tval_ok);
      goto_fail_label;
    uni_tvar_tval_ok:
      goto_more_exec;
    }

  {
    dlong areg;
    dlong *p;

    areg = *(areg_type *)(arg_place(locpc,1,uni_tvar_tval));
    *lochreg = args(areg) = (dlong)lochreg;
    lochreg++;

    areg = *(areg_type *)(arg_place(locpc,2,uni_tvar_tval));
    p = (dlong *)args(areg);
    locpc += uni_tvar_tval_len;
    deref(p); underef(p);
    *lochreg = (dlong)p;
    lochreg++;

    goto_more_exec;
  }
}

hcase(uni_tval_tval)
uni_tval_tval_label:
profile(uni_tval_tval);
{
  if (WAMS)
    {
      dlong areg;
      dlong *p1, *p2;

      areg = *(areg_type *)(arg_place(locpc,1,uni_tval_tval));
      p1 = (dlong *)args(areg); deref(p1);
      p2 = WAMS++; deref(p2);

      pre_unify_terms(p1,p2,uni_tval_tval_second_arg);

    uni_tval_tval_second_arg:

      areg = *(areg_type *)(arg_place(locpc,2,uni_tval_tval));
      p1 = (dlong *)args(areg); deref(p1);
      p2 = WAMS++; deref(p2);

      pre_unify_terms(p1,p2,uni_tval_tval_finish);

    uni_tval_tval_finish:

      locpc += uni_tval_tval_len;
      goto_more_exec;
    }

  {
    dlong areg;
    dlong *p;

    areg = *(areg_type *)(arg_place(locpc,1,uni_tval_tval));
    p = (dlong *)args(areg);
    deref(p); underef(p);
    *lochreg = (dlong)p;
    lochreg++;

    areg = *(areg_type *)(arg_place(locpc,2,uni_tval_tval));
    locpc += uni_tval_tval_len;
    p = (dlong *)args(areg);
    deref(p); underef(p);
    *lochreg = (dlong)p;
    lochreg++;

    goto_more_exec;
  }
}

hcase(call_c)
call_c_label:
profile(call_c);
{
  dlong ignore_result, index, arity;
  dlong (*c_function)();
  c_function = (dlong(*)())(*(dlong **)(arg_place(locpc,1,call_c)));
  index = *(dlong *)(arg_place(locpc,2,call_c));
  arity = symtab[index].arity;
  ignore_result = call_c_function(arity,c_function,&(locmach.Areg[0]));
  locpc = contpreg;
  goto_more_exec;
}

hcase(putpvarcall)
putpvarcall_label:
profile(putpvarcall);
{
  dlong yvar1;
  dlong areg2;
  dlong symbol3;
  
  inferencesplusplus;

  yvar1 = *(yvar_type*)(arg_place(locpc,1,putpvarcall));
  areg2 = *(areg_type*)(arg_place(locpc,2,putpvarcall));
  *lochreg = ereg[yvar1] = args(areg2) = (dlong)lochreg;
  lochreg++;

  symbol3 = *(dlong *)(arg_place(locpc,3,putpvarcall));
  
  contpreg = (codep)(locpc + putpvarcall_len + active_yvar_len);
  debug_info_call(symbol3,"putpvarcall");

  locpc = main_entrypoint(symbol3);

  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_putpvarcall;

  goto_more_exec;
}

hcase(getpvarcall)
getpvarcall_label:
profile(getpvarcall);
{
  dlong yvar1;
  dlong areg2;
  dlong symbol3;
  
  inferencesplusplus;

  yvar1 = *(yvar_type*)(arg_place(locpc,1,getpvarcall));
  areg2 = *(areg_type*)(arg_place(locpc,2,getpvarcall));
  ereg[yvar1] = args(areg2);

  symbol3 = *(symbol_type*)(arg_place(locpc,3,getpvarcall));
  
  contpreg = (codep)(locpc + getpvarcall_len + active_yvar_len);
  debug_info_call(symbol3,"getpvarcall");

  locpc = main_entrypoint(symbol3);

  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_putpvarcall;

  goto_more_exec;
}

hcase(putpvarvalvalcall)
putpvarvalvalcall_label:
profile(putpvarvalvalcall);
{
  dlong yvar1;
  dlong areg1;
  dlong *p;
  dlong symbol3;
  
  inferencesplusplus;

  areg1 = *(areg_type*)(arg_place(locpc,1,putpvarvalvalcall));
  yvar1 = *(yvar_type*)(arg_place(locpc,4,putpvarvalvalcall));
  *lochreg = ereg[yvar1] = args(areg1) = (dlong)lochreg;
  lochreg++;

  areg1 = *(areg_type*)(arg_place(locpc,2,putpvarvalvalcall));
  yvar1 = *(yvar_type*)(arg_place(locpc,5,putpvarvalvalcall));
  p = (dlong *)ereg[yvar1];
  args(areg1) = (dlong)p;

  areg1 = *(areg_type*)(arg_place(locpc,3,putpvarvalvalcall));
  yvar1 = *(yvar_type*)(arg_place(locpc,6,putpvarvalvalcall));

  p = (dlong *)ereg[yvar1];
  args(areg1) = (dlong)p;

  symbol3 = *(dlong *)(arg_place(locpc,7,putpvarvalvalcall));
  
  contpreg = (codep)(locpc + putpvarvalvalcall_len + active_yvar_len);
  debug_info_call(symbol3,"putpvarvalvalcall");

  locpc = main_entrypoint(symbol3);

  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_putpvarvalvalcall;

  goto_more_exec;
}

hcase(call)
call_label:
profile(call);
{
  dlong symbol1;

  inferencesplusplus;

  contpreg = (codep)(locpc + call_len + active_yvar_len);
  symbol1 = *(dlong *)(arg_place(locpc,1,call));
  debug_info_call(symbol1,"call");

  locpc = main_entrypoint(symbol1);

  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_call;

  goto_more_exec;
}

hcase(dummy_call)
dummy_call_label:
profile(dummy_call);
{
  contpreg = locpc = (codep)(locpc + dummy_call_len + active_yvar_len);
  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_dummy_call;
  goto_more_exec;
}

hcase(call_unkn)
call_unkn_label:
profile(call_unkn);
{
  dlong symbol, arity, i;

  inferencesplusplus;

  symbol = *(dlong *)(arg_place(locpc,1,call_unkn));
  if (! symbol)
    {
      fprintf(stderr,"Calling unknown functor that isn't a predicate - sorry, no more info\n");
      goto_fail_label;
    }

  arity = symtab[symbol].arity;
  if (arity == 0)
    args(1) = make_atom(symbol);
  else
    {
      dlong *h = lochreg;
      *h = make_funct(symbol);
      h++;
      i = 1;
      while (arity--)
	*h++ = args(i++);
      args(1) = make_struct_p(lochreg);
      lochreg = h;
    }

  locpc = main_entrypoint(symbol_error_unknown_pred_1);

  goto_more_exec;
}


hcase(deallex)
deallex_label:
profile(deallex);
{
  dlong symbol1;
	    
  inferencesplusplus;

  contpreg = (codep)(*(ereg+1));
  {                                                   
    dlong *le;
    CP_P lb;

    le = ereg = (dlong *)(*ereg);
    lb = (CP_P)locbreg;        

    if (le > (dlong *)(lb->tops))                     
      tos = (dlong *)(lb->tops);              
    else tos = le;                             
  }

  symbol1 = *(dlong *)(arg_place(locpc,1,deallex));
  debug_info_call(symbol1,"deallex");

  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_deallex;
  locpc = main_entrypoint(symbol1);
	
  goto_more_exec;
}

hcase(profile_instr)
profile_instr_label:
profile(profile_instr);
{
  dlong low, high;

  low = *(count_type*)(arg_place(locpc,1,profile_instr)) + 1;
  if (low >= 1000000000)
    {
      *(count_type*)(arg_place(locpc,1,profile_instr)) = low - 1000000000;
      *(count_type*)(arg_place(locpc,2,profile_instr)) += 1;
    }
  else
    *(count_type*)(arg_place(locpc,1,profile_instr)) = low;
  
  locpc += profile_instr_len;
  goto_more_exec;
}



hcase(switchonlist)
switchonlist_label:
profile(switchonlist);
{
  register dlong *p;

  p = (dlong *)args(1);

  if (is_list(p))
    {
    islist_switchonlist:
      locpc = ((codep)(*(dlong *)(arg_place(locpc,3,switchonlist))));
      goto_more_exec;
    }

  if (is_atom(p))
    {
    isatom_switchonlist:
      locpc = ((codep)(*(dlong *)(arg_place(locpc,2,switchonlist))));
      goto_more_exec;
    }

  deref2(p,goto isvar_switchonlist);

  if (is_list(p))
    {
      goto islist_switchonlist;
    }

  if (has_atom_tag(p))
    {
      goto isatom_switchonlist;
    }

  if (! is_attributed(p)) goto_fail_label;

 isvar_switchonlist:
  locpc = ((codep)(*(dlong *)(arg_place(locpc,4,switchonlist))));
  goto_more_exec;
}

hcase(execute)
execute_label:
profile(execute);
{
  dlong symbol1;

  inferencesplusplus;

  symbol1 = *(dlong *)(arg_place(locpc,2,execute)); // 1 and 2 are the same but ...
  debug_info_call(symbol1,"execute");

  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_execute;

  // the order below is crucial when in multi-threading mode
  // like below, no locking is needed - commented out for now
  *(dlong *)(arg_place(locpc,1,execute)) = (dlong)(main_entrypoint(symbol1));
  fill_opcode(locpc,fast_execute);

  locpc = main_entrypoint(symbol1);

  goto_more_exec;
}

hcase(fast_execute)
fast_execute_label:
profile(fast_execute);
{
  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_fast_execute;
  locpc = (codep)(*(dlong *)(arg_place(locpc,1,fast_execute)));

  goto_more_exec;

}

hcase(switchonlist_skip_alloc)
     switchonlist_skip_alloc_label:
profile(switchonlist_skip_alloc);
{
  register dlong *p;

  p = (dlong *)args(1);
  // p = (dlong *)args((*(dlong *)(arg_place(locpc,1,switchonlist_skip_alloc))));
  //  deref2(p,goto isvar_switchonlist_skip_alloc);

  if (is_list(p))
    {
    islist_switchonlist_skip_alloc:
      WAMS = get_list_pointer(p);
      /* allocate */
      {
        dlong yvar1;
        dlong *loctos;

        yvar1 = *(yvar_type *)(arg_place(locpc,2,switchonlist_skip_alloc));
	loctos = tos;
	tos = loctos = loctos - yvar1;
	*loctos = (dlong)ereg;
	ereg = loctos;
	if (loctos < ls_limit)
	  loctos = deal_with_ls_overflow(&locmach);
	*(loctos+1) = (dlong)contpreg;
      }
      locpc = ((codep)(*(dlong *)(arg_place(locpc,4,switchonlist_skip_alloc))));
      goto_more_exec;
    }

  if (has_atom_tag(p))
    {
    isatom_switchonlist_skip_alloc:
      locpc = ((codep)(*(dlong *)(arg_place(locpc,3,switchonlist_skip_alloc))));
      goto_more_exec;
    }

  deref2(p,goto isvar_switchonlist_skip_alloc);

  if (is_list(p))
    {
      goto islist_switchonlist_skip_alloc;
    }

  if (has_atom_tag(p))
    {
      goto isatom_switchonlist_skip_alloc;
    }

  if (! is_attributed(p)) goto_fail_label;

 isvar_switchonlist_skip_alloc:
  locpc = ((codep)(*(dlong *)(arg_place(locpc,5,switchonlist_skip_alloc))));
  goto_more_exec;
}

hcase(switchonlist_skip)
switchonlist_skip_label:
profile(switchonlist_skip);
{
  register dlong *p;

  p = (dlong *)args(1);
  // p = (dlong *)args((*(dlong *)(arg_place(locpc,1,switchonlist_skip))));
  //  deref2(p,goto isvar_switchonlist_skip);

  if (is_list(p))
    {
    islist_switchonlist_skip:
      locpc = ((codep)(*(dlong *)(arg_place(locpc,3,switchonlist_skip))));
      WAMS = get_list_pointer(p);
      goto_more_exec;
    }

  if (has_atom_tag(p))
    {
    isatom_switchonlist_skip:
      locpc = ((codep)(*(dlong *)(arg_place(locpc,2,switchonlist_skip))));
      goto_more_exec;
    }

  deref2(p,goto isvar_switchonlist_skip);

  if (is_list(p))
    {
      goto islist_switchonlist_skip;
    }

  if (has_atom_tag(p))
    {
      goto isatom_switchonlist_skip;
    }

  if (! is_attributed(p)) goto_fail_label;

 isvar_switchonlist_skip:
  locpc = ((codep)(*(dlong *)(arg_place(locpc,4,switchonlist_skip))));
  goto_more_exec;
}

hcase(switchonlist_append)
switchonlist_append_label:
profile(switchonlist_append);
{
  register dlong *p, *q;

  p = (dlong *)args(1);

  deref2(p,goto isvar_switchonlist_append);

  if (is_list(p))
    {
      dlong areg3, areg4;
    islist_switchonlist_append:
      areg3 = *(areg_type *)(arg_place(locpc,2,switchonlist_append));
      q = (dlong *)args(areg3);
      deref2(q,goto arg3_append_ref);

      p = get_list_pointer(p);
      lochreg[0] = p[0];
      args(1) = p[1];
      areg4 = *(areg_type *)(arg_place(locpc,3,switchonlist_append));
      locpc = ((codep)(*(dlong *)(arg_place(locpc,5,switchonlist_append))));
      locpc -= getlist_tval_tvar_len;
      args(areg4) = lochreg[0]; // p[0];
      lochreg++;
      goto_more_exec;


    arg3_append_ref:
      {
	register dlong *h;

	trail(q,bh,loctrreg);
	p = get_list_pointer(p);
	areg4 = *(areg_type *)(arg_place(locpc,3,switchonlist_append));
	h = lochreg;
	args(areg4) = h[0] = p[0];
	args(1) = p[1];
	*q = make_list(h);
	locpc = ((codep)(*(dlong *)(arg_place(locpc,5,switchonlist_append))));
	h++;
	args(areg3) = h[0] = (dlong)(h);
	lochreg = ++h;
	goto_more_exec;
      }
    }

  if (has_atom_tag(p))
    {
      locpc = ((codep)(*(dlong *)(arg_place(locpc,4,switchonlist_append))));
      goto_more_exec;
    }

  if (! is_attributed(p)) goto_fail_label;

 isvar_switchonlist_append:
  locpc = ((codep)(*(dlong *)(arg_place(locpc,6,switchonlist_append))));
  goto_more_exec;
}



hcase(uni_tvar_tvar)
uni_tvar_tvar_label:
profile(uni_tvar_tvar);
{
  if (WAMS)
    {
      register dlong areg1;
      register dlong areg2;

      areg1 = *(areg_type *)(arg_place(locpc,1,uni_tvar_tvar));
      areg2 = *(areg_type *)(arg_place(locpc,2,uni_tvar_tvar));
      locpc += uni_tvar_tvar_len;
      args(areg1) = WAMS[0];
      args(areg2) = WAMS[1];
      WAMS += 2;
      goto_more_exec;
    }

  {
    dlong areg;

    areg = *(areg_type *)(arg_place(locpc,1,uni_tvar_tvar));
    *lochreg = args(areg) = (dlong)lochreg;
    lochreg++;
    areg = *(areg_type *)(arg_place(locpc,2,uni_tvar_tvar));
    locpc += uni_tvar_tvar_len;
    *lochreg = args(areg) = (dlong)lochreg;
    lochreg++;

    goto_more_exec;
  }
}

hcase(getlist_tval_tvar)
getlist_tval_tvar_label:
profile(getlist_tval_tvar);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,getlist_tval_tvar)));

  deref2(p,goto is_a_var_getlist_tval_tvar);

  if (is_list(p))
    {
      dlong *p1, *p2;
      dlong areg;
      p = get_list_pointer(p); p2 = (dlong *)*p; deref(p2);
      areg = *(areg_type *)(arg_place(locpc,2,getlist_tval_tvar));
      p1 = (dlong *)args(areg); deref(p1);

      pre_unify_terms(p2,p1,getlist_tval_tvar_read);
    getlist_tval_tvar_read:
      areg = *(areg_type *)(arg_place(locpc,3,getlist_tval_tvar));
      args(areg) = *(p+1);
      locpc += getlist_tval_tvar_len;
      goto_more_exec;
    }

  if (is_ref(p))
    {
    is_a_var_getlist_tval_tvar:
      trail(p,bh,loctrreg);
      *p = make_list(lochreg);
      {
	register dlong areg1;
	register dlong areg2;

	areg1 = *(areg_type *)(arg_place(locpc,2,getlist_tval_tvar));
	areg2 = *(areg_type *)(arg_place(locpc,3,getlist_tval_tvar));
	locpc += getlist_tval_tvar_len;
	lochreg[0] = args(areg1);
	lochreg++;
	lochreg[0] = args(areg2) = (dlong)(lochreg);
	lochreg++;
	goto_more_exec;
      }
    }

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
      {
	register dlong areg1;
	register dlong areg2;

	areg1 = *(areg_type *)(arg_place(locpc,2,getlist_tval_tvar));
	areg2 = *(areg_type *)(arg_place(locpc,3,getlist_tval_tvar));
	locpc += getlist_tval_tvar_len;
	lochreg[0] = args(areg1);
	lochreg++;
	lochreg[0] = args(areg2) = (dlong)(lochreg);
	lochreg++;
	goto_more_exec;
      }
    }

  goto_fail_label;
}


hcase(getlist_tval_tvar_same)
getlist_tval_tvar_same_label:
profile(getlist_tval_tvar_same);
{
  dlong *p;
  register dlong areg2;

  areg2 = *(areg_type *)(arg_place(locpc,1,getlist_tval_tvar_same));

  p = (dlong *)args(areg2);

  deref2(p,goto is_a_var_getlist_tval_tvar_same);

  if (is_list(p))
    {
      dlong *p1, *p2;
      dlong areg;
      p = get_list_pointer(p); p2 = (dlong *)*p; deref(p2);
      areg = *(areg_type *)(arg_place(locpc,2,getlist_tval_tvar_same));
      p1 = (dlong *)args(areg); deref(p1);

      pre_unify_terms(p2,p1,getlist_tval_tvar_same_read);
    getlist_tval_tvar_same_read:
      areg = *(areg_type *)(arg_place(locpc,1,getlist_tval_tvar_same));
      args(areg) = *(p+1);
      locpc += getlist_tval_tvar_same_len;
      goto_more_exec;
    }

  if (is_ref(p))
    {
    is_a_var_getlist_tval_tvar_same:
      trail(p,bh,loctrreg);
      *p = make_list(lochreg);
      {
	register dlong areg1;

	areg1 = *(areg_type *)(arg_place(locpc,2,getlist_tval_tvar_same));
	locpc += getlist_tval_tvar_same_len;
	lochreg[0] = args(areg1);
	lochreg++;
	lochreg[0] = args(areg2) = (dlong)(lochreg);
	lochreg++;
	goto_more_exec;
      }
    }

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
      {
	register dlong areg1;
	register dlong areg2;

	areg1 = *(areg_type *)(arg_place(locpc,2,getlist_tval_tvar_same));
	areg2 = *(areg_type *)(arg_place(locpc,1,getlist_tval_tvar_same));
	locpc += getlist_tval_tvar_same_len;
	lochreg[0] = args(areg1);
	lochreg++;
	lochreg[0] = args(areg2) = (dlong)(lochreg);
	lochreg++;
	goto_more_exec;
      }
    }

  goto_fail_label;
}


hcase(getlist)
getlist_label:
profile(getlist);
{
  dlong areg1;
  dlong *p, *q;

  areg1 = *(areg_type *)(arg_place(locpc,1,getlist));
  p = (dlong *)args(areg1);
  locpc += getlist_len;

 still_not_sure:
  if (!is_ref(p)) goto maybe_list;
  q = (dlong *)*p;
  if (p != q) { p = q; goto still_not_sure; }

  *p = make_list(lochreg);
  trail(p,bh,loctrreg);
  makeSzero;
  goto_more_exec;

 maybe_list:
  if (is_list(p))
    {
    is_a_list_getlist:
      WAMS = get_list_pointer(p);
      goto_more_exec ;
    }

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
      makeSzero;
      goto_more_exec;
    }

  goto_fail_label;

}

hcase(uni_tval_tvar)
uni_tval_tvar_label:
profile(uni_tval_tvar);
{
  if (WAMS)
    {
      dlong areg, *p1, *p2;
      
      areg = *(areg_type *)(arg_place(locpc,1,uni_tval_tvar));
      p1 = (dlong *)args(areg); deref(p1);
      p2 = (dlong *)*WAMS++; deref(p2);
      pre_unify_terms(p1,p2,uni_tval_tvar_ok);
      goto_fail_label;
    uni_tval_tvar_ok:
      areg = *(areg_type *)(arg_place(locpc,2,uni_tval_tvar));
      locpc += uni_tval_tvar_len;
      args(areg) = *WAMS++;

      goto_more_exec;
    }

  {
    register dlong areg;
    register dlong *loch;

    loch = lochreg;
    areg = *(areg_type *)(arg_place(locpc,1,uni_tval_tvar));
    *loch = (dlong)args(areg);
    loch++;

    areg = *(areg_type *)(arg_place(locpc,2,uni_tval_tvar));
    *loch = args(areg) = (dlong)loch;
    locpc += uni_tval_tvar_len;
    lochreg = loch+1;

    goto_more_exec;
  }
}

hcase(getlist_tvar_tvar)
getlist_tvar_tvar_label:
profile(getlist_tvar_tvar);
{
  dlong areg;
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,getlist_tvar_tvar)));

  deref2(p,goto is_a_var_getlist_tvar_tvar);

  if (is_list(p))
    {
      p = get_list_pointer(p);
      areg = *(areg_type *)(arg_place(locpc,2,getlist_tvar_tvar));
      args(areg) = *p;
      areg = *(areg_type *)(arg_place(locpc,3,getlist_tvar_tvar));
      locpc += getlist_tvar_tvar_len;
      args(areg) = *(p+1);
      goto_more_exec;
    }

  if (is_ref(p))
    {
    is_a_var_getlist_tvar_tvar:
      trail(p,bh,loctrreg);
      *p = make_list(lochreg);
    rest_of_construction_getlist_tvar_tvar:
      areg = *(areg_type *)(arg_place(locpc,2,getlist_tvar_tvar));
      *lochreg = args(areg) = (dlong)lochreg;
      lochreg++;
      areg = *(areg_type *)(arg_place(locpc,3,getlist_tvar_tvar));
      *lochreg = args(areg) = (dlong)lochreg;
      locpc += getlist_tvar_tvar_len;
      lochreg++;
      goto_more_exec;
    }

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
      goto rest_of_construction_getlist_tvar_tvar;
    }

  goto_fail_label
    }

// hcase(getlist_tvar_tvar_same)
// getlist_tvar_tvar_same_label:
// profile(getlist_tvar_tvar_same);
// {
//   dlong areg;
//   dlong *p;
// 
//   areg = *(areg_type *)(arg_place(locpc,1,getlist_tvar_tvar_same));
//   p = (dlong *)args(areg);
// 
//   deref2(p,goto is_a_var_getlist_tvar_tvar_same);
// 
//   if (is_list(p))
//     {
//       p = get_list_pointer(p);
//       args(areg) = *(p+1);
//       areg = *(areg_type *)(arg_place(locpc,2,getlist_tvar_tvar_same));
//       locpc += getlist_tvar_tvar_same_len;
//       args(areg) = *p;
//       goto_more_exec;
//     }
// 
//   if (is_ref(p))
//     {
//     is_a_var_getlist_tvar_tvar_same:
//       trail(p,bh,loctrreg);
//       *p = make_list(lochreg);
//     rest_of_construction_getlist_tvar_tvar_same:
//       areg = *(areg_type *)(arg_place(locpc,2,getlist_tvar_tvar_same));
//       *lochreg = args(areg) = (dlong)lochreg;
//       lochreg++;
//       areg = *(areg_type *)(arg_place(locpc,1,getlist_tvar_tvar_same));
//       *lochreg = args(areg) = (dlong)lochreg;
//       locpc += getlist_tvar_tvar_same_len;
//       lochreg++;
//       goto_more_exec;
//     }
// 
//   if (is_attributed(p))
//     {
//       safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
//       goto rest_of_construction_getlist_tvar_tvar_same;
//     }
// 
//   goto_fail_label
//     }

hcase(getlist_tvar)
getlist_tvar_label:
profile(getlist_tvar);
{
  dlong areg;
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,getlist_tvar)));

  deref2(p,goto is_a_var_getlist_tvar);

  if (is_list(p))
    {
      p = get_list_pointer(p);
      areg = *(areg_type *)(arg_place(locpc,2,getlist_tvar));
      args(areg) = *p;
      locpc += getlist_tvar_len;
      WAMS = p+1;
      goto_more_exec;
    }

  if (is_ref(p))
    {
    is_a_var_getlist_tvar:
      trail(p,bh,loctrreg);
      *p = make_list(lochreg);
    rest_of_construction_getlist_tvar:
      makeSzero;
      areg = *(areg_type *)(arg_place(locpc,2,getlist_tvar));
      *lochreg = args(areg) = (dlong)lochreg;
      locpc += getlist_tvar_len;
      lochreg++;
      goto_more_exec;
    }

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
      goto rest_of_construction_getlist_tvar;
    }

  goto_fail_label
    }

hcase(getlist_tval)
getlist_tval_label:
profile(getlist_tval);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,getlist_tval)));

  deref2(p,goto is_a_var_getlist_tval);

  if (is_list(p))
    {
      dlong *p1, *p2;
      dlong areg;
      p = get_list_pointer(p); p2 = (dlong *)*p; deref(p2);
      areg = *(areg_type *)(arg_place(locpc,2,getlist_tval));
      p1 = (dlong *)args(areg); deref(p1);

      pre_unify_terms(p2,p1,getlist_tval_read);
    getlist_tval_read:
      locpc += getlist_tval_len;
      WAMS = (p+1);
      goto_more_exec;
    }

  if (is_ref(p))
    {
    is_a_var_getlist_tval:
      trail(p,bh,loctrreg);
      *p = make_list(lochreg);
      {
	register dlong areg1;
	register dlong areg2;

	areg1 = *(areg_type *)(arg_place(locpc,2,getlist_tval));
	locpc += getlist_tval_len;
	lochreg[0] = args(areg1);
	lochreg++;
	makeSzero;
	goto_more_exec;
      }
    }

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
      {
	register dlong areg1;
	register dlong areg2;

	areg1 = *(areg_type *)(arg_place(locpc,2,getlist_tval));
	locpc += getlist_tval_len;
	lochreg[0] = args(areg1);
	lochreg++;
	makeSzero;
	goto_more_exec;
      }
    }

  goto_fail_label;
}
hcase(get_float)
get_float_label:
profile(get_float);
{
  dlong areg1;
  dfloat f2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,get_float));
  get_double_arg(f2,(arg_place(locpc,2,get_float)));

  locpc += get_float_len;
  p = (dlong *)args(areg1);
  deref(p);
  if (is_ref(p))
    {
      trail(p,bh,loctrreg);
      build_real(p,f2,lochreg);
      goto_more_exec;
    }

  if (is_real(p) && (get_real(p) == f2))
    goto_more_exec;

  if (! is_attributed(p))
    goto_fail_label;

	
  safe_wakeup(p,(dlong *)make_number_pointer(lochreg),&locmach);
  push_real(lochreg,f2);
  goto_more_exec;
}

hcase(put_float)
put_float_label:
profile(put_float);
{
  dlong areg1;
  dfloat f2;

  areg1 = *(areg_type *)(arg_place(locpc,1,put_float));
  get_double_arg(f2,(arg_place(locpc,2,put_float)));
  locpc += put_float_len;

  build_real(&(args(areg1)),f2,lochreg);
  goto_more_exec;
}


hcase(put_floatv)
put_floatv_label:
profile(put_floatv);
{
  dlong yvar1;
  dfloat f2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,put_floatv));
  get_double_arg(f2,(arg_place(locpc,2,put_floatv)));
  locpc += put_floatv_len;

  build_real(&(ereg[yvar1]),f2,lochreg);
  goto_more_exec;
}

hcase(try)
try_label:
profile(try);
{
  dlong arity1;

  arity1 = *(arity_type*)(arg_place(locpc,1,try));

  push_choicepoint((locpc+try_len),contpreg,arity1);
  locpc = ((codep)(*(dlong *)(arg_place(locpc,2,try))));
  bh = lochreg;

  goto_more_exec;
}

hcase(try2)
try2_label:
profile(try2);
{
  push_choicepoint2((locpc+try2_len),contpreg);
  locpc = ((codep)(*(dlong *)(arg_place(locpc,1,try2))));
  bh = lochreg;

  goto_more_exec;
}

hcase(retry)
retry_label:
profile(retry);
{
  dlong arity1;
  CP_P bb;

  arity1 = *(arity_type*)(arg_place(locpc,1,retry));

  bb = (CP_P)locbreg;
  untrail(loctrreg,(bb->tr));
  reset_regs1(bb);
  bb->failurecont = (codep)(locpc+retry_len);
  locpc = ((codep)(*(dlong *)(arg_place(locpc,2,retry))));

  pop_arguments1(arity1,bb);

  bh = bb->h;

  goto_more_exec;
}

hcase(retry3)
retry3_label:
profile(retry3);
{
  CP_P bb;

  bb = (CP_P)locbreg;
  untrail(loctrreg,(bb->tr));
  reset_regs1(bb);
  bb->failurecont = (codep)(locpc+retry3_len);
  locpc = ((codep)(*(dlong *)(arg_place(locpc,1,retry3))));

  pop_arguments3(bb);

  bh = bb->h;

  goto_more_exec;
}

hcase(trust)
trust_label:
profile(trust);
{
  dlong arity1;
  CP_P bb;

  arity1 = *(arity_type*)(arg_place(locpc,1,trust));
  locpc = ((codep)(*(dlong *)(arg_place(locpc,2,trust))));

  bb = (CP_P)locbreg;
  untrail(loctrreg,(bb->tr));
  reset_regs1(bb);

  pop_arguments1(arity1,bb);

  locbreg =  bb->b;
  freeze_heap();
  bh = ((CP_P)locbreg)->h;

  goto_more_exec;
}

hcase(trust2)
trust2_label:
profile(trust2);
{
  CP_P bb;

  locpc = ((codep)(*(dlong *)(arg_place(locpc,1,trust2))));

  bb = (CP_P)locbreg;
  untrail(loctrreg,(bb->tr));
  reset_regs1(bb);

  pop_arguments2(bb);

  locbreg =  bb->b;
  freeze_heap();
  bh = ((CP_P)locbreg)->h;

  goto_more_exec;
}

hcase(switchonterm)
switchonterm_label:
profile(switchonterm);
{

  /* dlong areg1; */
  dlong *p;

  /* areg1 = *(areg_type *)(arg_place(locpc,1,switchonterm)); */

  p = (dlong *)args(1);
  deref2(p,goto isvar_switchonterm);

  if (is_compound(p))
    locpc = (codep)(*(dlong *)(arg_place(locpc,3,switchonterm)));
  else
    if (is_atomic(p))
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,switchonterm)));
    else
      {
      isvar_switchonterm:
	locpc = (codep)(*(dlong *)(arg_place(locpc,4,switchonterm)));
      }
  if (! locpc) goto_fail_label; /* is this possible ? */
  goto_more_exec;
}

hcase(switchonbound)
switchonbound_label:
profile(switchonbound);
{
  /* dlong areg1; */
  dlong *p;
  dlong hashvalue = 0;
  dlong *hashtable;

	/* areg1 = *(areg_type *)(arg_place(locpc,1,switchonbound)); */

  p = (dlong *)args(1);
  deref2(p,goto switchonbound_var);
  bridge = p;
  if (is_struct(p))
    hashvalue = get_funct_symb(*(get_struct_pointer(p)));
  else if (is_char(p))
    hashvalue = get_character(p);
  else if (is_atom(p))
    hashvalue = get_atom_index(p);
  else if (is_smallint(p))
    hashvalue = get_smallint(p);
  else if (is_real(p))
    {
      double fl = get_real(p);
      hashvalue = fl;
    }
  else if (is_var(p))
    {
    switchonbound_var:
      locpc = (codep)(*(dlong *)(arg_place(locpc,4,switchonbound)));
      goto_more_exec;
    }
  else if (is_list(p))
    hashvalue = HASH_LIST;
  else if (is_string(p)) /* is_string */
    hashvalue = string_hash(get_string_length(p),get_string_val(p));
  else
    if (is_bigint(p))
      {
	// act as var
	locpc = (codep)(*(dlong *)(arg_place(locpc,4,switchonbound)));
	goto_more_exec;
      }

	
  hashvalue = hashvalue & ((*(short*)(arg_place(locpc,2,switchonbound)))-1);

  hashtable = (dlong *) ( locpc + switchonbound_len);

  locpc = (codep)(hashtable[hashvalue]);
  if (! locpc) goto_fail_label;
  goto_more_exec;
}

hcase(select_functor2)
select_functor2_label:
profile(select_functor2);
{
  dlong label2;
  dlong struct1, incomingstruct;
  dlong *p;

  p = (dlong *)args(1);
  deref2(p,goto select_functor2_var);
  if (is_attributed(p)) goto select_functor2_var;
  if (is_struct(p))
    incomingstruct = *get_struct_pointer(p);
  else
    incomingstruct = (dlong)p;

  struct1 = *(dlong *)(arg_place(locpc,1,select_functor2));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,select_functor2)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,3,select_functor2));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,4,select_functor2)));
      goto_more_exec;
    }
  locpc = (codep)(*(dlong *)(arg_place(locpc,6,select_functor2)));
  if (locpc) goto_more_exec;
  goto_fail_label;

 select_functor2_var:
  locpc = (codep)(*(dlong *)(arg_place(locpc,5,select_functor2)));
  goto_more_exec;
}


hcase(select_functor3)
select_functor3_label:
profile(select_functor3);
{
  dlong label2;
  dlong struct1, incomingstruct;
  dlong *p;

  p = (dlong *)args(1);
  deref2(p,goto select_functor3_var);
  if (is_attributed(p)) goto select_functor3_var;
  if (is_struct(p))
    incomingstruct = *get_struct_pointer(p);
  else
    incomingstruct = (dlong)p;

  struct1 = *(dlong *)(arg_place(locpc,1,select_functor3));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,select_functor3)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,3,select_functor3));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,4,select_functor3)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,5,select_functor3));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,6,select_functor3)));
      goto_more_exec;
    }
  locpc = (codep)(*(dlong *)(arg_place(locpc,8,select_functor3)));
  if (locpc) goto_more_exec;
  goto_fail_label;

 select_functor3_var:
  locpc = (codep)(*(dlong *)(arg_place(locpc,7,select_functor3)));
  goto_more_exec;
}


hcase(trymeorelse)
trymeorelse_label:
profile(trymeorelse);
{
  dlong label1;
  
  label1 = (dlong)((codep)*(dlong *)(arg_place(locpc,1,trymeorelse)));
  
  locpc += trymeorelse_len + active_yvar_len;
  push_choicepoint(label1,locpc,0);
  bh = lochreg;
  
  goto_more_exec;
}

hcase(getpbreg_trymeorelse)
getpbreg_trymeorelse_label:
profile(getpbreg_trymeorelse);
{
  dlong label1;
  dlong yvar1;
  
  yvar1 = *(yvar_type *)(arg_place(locpc,1,getpbreg_trymeorelse));
  ereg[yvar1] = make_cut_point(locmach.begin_cp,locbreg);
  label1 = (dlong)((codep)*(dlong *)(arg_place(locpc,2,getpbreg_trymeorelse)));
  
  locpc += getpbreg_trymeorelse_len + active_yvar_len;
  push_choicepoint(label1,locpc,0);
  bh = lochreg;
  
  goto_more_exec;
}

hcase(retrymeorelse)
retrymeorelse_label:
profile(retrymeorelse);
{
  dlong label1;

  label1 = (dlong)((codep)*(dlong *)(arg_place(locpc,1,retrymeorelse)));

  untrail(loctrreg,((CP_P)locbreg)->tr);
  reset_regs;
  bh = lochreg;
  locpc += retrymeorelse_len;

  ((CP_P)locbreg)->failurecont = (codep)label1;

  goto_more_exec;
}

hcase(jump)
jump_label:
profile(jump);
{
  locpc = ((codep)(*(dlong *)(arg_place(locpc,1,jump))));

  goto_more_exec;
}

hcase(choicepoint_reuse)
choicepoint_reuse_label:
profile(choicepoint_reuse);
{

  dlong areg1;
  dlong label2;
  dlong *p = locbreg;

  areg1 = *(areg_type*)(arg_place(locpc,1,choicepoint_reuse));
  label2 = *(label_type*)(arg_place(locpc,2,choicepoint_reuse));
  locpc = ((codep)*(label_type*)(arg_place(locpc,3,choicepoint_reuse)));
  {
    ((CP_P)p)->failurecont = (codep)label2;
    p += (CP_FIX_LEN - 1) + areg1;
    while (areg1) *p-- = args(areg1--);
  }
  goto_more_exec;
}

hcase(fail)
fail_label:
profile(fail);
{
  failure;
  goto_more_exec;
}


hcase(dealloc_proceed)
dealloc_proceed_label:
profile(dealloc_proceed);
{
  locpc = (codep)(*(ereg+1));
  ereg = (dlong *)(*ereg);
  /* assign to contpreg done at heap overflow */
  recompute_tos(ereg,locbreg);
  check_cancellation();

  if (lochreg > heap_limit)
    goto deal_with_heap_overflow_dealloc_proceed;

  goto_more_exec;
}

hcase(proceed)
proceed_label:
profile(proceed);
{
  locpc = contpreg;
  goto_more_exec;
}

hcase(builtin_halt_1)
builtin_halt_1_label:
profile(builtin_halt_1);
{
  dlong *p1;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_halt_1)));
  deref(p1);
  if (! is_smallint(p1))
    goto_fail_label;
  return_code = get_smallint(p1);
  goto end_session;
}

hcase(builtin_waar_0)
builtin_waar_0_label:
profile(builtin_waar_0);
{
  dlong *p1;
  dlong i;

  p1 = (dlong *)args(10);
  deref(p1);
  i = get_smallint(p1);

  fprintf(stderr,"%ld  ",i);
  fan(locpc,&locmach);
  locpc += builtin_waar_0_len;
  goto_more_exec;
}

hcase(trustmeorelsefail)
trustmeorelsefail_label:
profile(trustmeorelsefail);
{
  locpc += trustmeorelsefail_len;
  untrail(loctrreg,((CP_P)locbreg)->tr);
  reset_regs;

  locbreg =  ((CP_P)locbreg)->b;
  freeze_heap();
  bh = ((CP_P)locbreg)->h;

  goto_more_exec;
}

hcase(get_int)
get_int_label:
profile(get_int);
{
  dlong areg1;
  dlong int2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,get_int));
  p = (dlong *)args(areg1);
  deref2(p,goto get_int_is_var);
  int2 = *(dlong *)(arg_place(locpc,2,get_int));
  locpc += get_int_len;
  if (int2 == (dlong)p)
    goto_more_exec;

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)int2,&locmach);
      goto_more_exec;
    }

  goto_fail_label;

 get_int_is_var:
  *p = *(dlong *)(arg_place(locpc,2,get_int));
  locpc += get_int_len;
  trail(p,bh,loctrreg);
  goto_more_exec;
}

hcase(get_int2)
get_int2_label:
profile(get_int2);
{
  dlong a1;
  dlong i1;
  dlong *p;

  a1 = *(areg_type *)(arg_place(locpc,1,get_int2));
  p = (dlong *)args(a1);
  deref2(p,goto get_int2_is_var1);
  i1 = *(dlong *)(arg_place(locpc,3,get_int2));

  if (i1 == (dlong)p)
    goto rest_get_int2;

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)i1,&locmach);
      goto rest_get_int2;
    }

  goto_fail_label;

 get_int2_is_var1:
  *p = *(dlong *)(arg_place(locpc,3,get_int2));
  trail(p,bh,loctrreg);

 rest_get_int2:
  a1 = *(areg_type *)(arg_place(locpc,2,get_int2));
  p = (dlong *)args(a1);
  deref2(p,goto get_int2_is_var2);
  i1 = *(dlong *)(arg_place(locpc,4,get_int2));
  locpc += get_int2_len;

  if (i1 == (dlong)p)
    goto_more_exec;

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)i1,&locmach);
      goto_more_exec;
    }

  goto_fail_label;

 get_int2_is_var2:
  *p = *(dlong *)(arg_place(locpc,4,get_int2));
  locpc += get_int2_len;
  trail(p,bh,loctrreg);
  goto_more_exec;
}

hcase(get_int3)
get_int3_label:
profile(get_int3);
{
  dlong areg1;
  dlong i1;
  register dlong *p;

  i1 = *(dlong *)(arg_place(locpc,4,get_int3));
  areg1 = *(areg_type *)(arg_place(locpc,1,get_int3));
  p = (dlong *)args(areg1);
  deref2(p,goto get_int3_is_var1);
  if (i1 == (dlong)p)
    goto arg2_get_int3;

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)i1,&locmach);
      goto arg2_get_int3;
    }

  goto_fail_label;

 get_int3_is_var1:
  *p = i1;
  trail(p,bh,loctrreg);


 arg2_get_int3:

  i1 = *(dlong *)(arg_place(locpc,5,get_int3));
  areg1 = *(areg_type *)(arg_place(locpc,2,get_int3));
  p = (dlong *)args(areg1);
  deref2(p,goto get_int3_is_var2);
  if (i1 == (dlong)p)
    goto arg3_get_int3;

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)i1,&locmach);
      goto arg3_get_int3;
    }

  goto_fail_label;

 get_int3_is_var2:
  *p = i1;
  trail(p,bh,loctrreg);

 arg3_get_int3:

  i1 = *(dlong *)(arg_place(locpc,6,get_int3));
  areg1 = *(areg_type *)(arg_place(locpc,3,get_int3));
  p = (dlong *)args(areg1);
  locpc += get_int3_len;
  deref2(p,goto get_int3_is_var3);
  if (i1 == (dlong)p)
    goto_more_exec;

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)i1,&locmach);
      goto_more_exec;
    }

  goto_fail_label;

 get_int3_is_var3:
  *p = i1;
  trail(p,bh,loctrreg);
  goto_more_exec;
}

hcase(get_int_proceed)
get_int_proceed_label:
profile(get_int_proceed);
{
  dlong areg1;
  dlong int2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,get_int_proceed));
  p = (dlong *)args(areg1);
  deref2(p,goto get_int_proceed_is_var);
  int2 = *(dlong *)(arg_place(locpc,2,get_int_proceed));
  locpc = contpreg;
  if (int2 == (dlong)p)
    goto_more_exec;

  if (is_attributed(p))
    {
      safe_wakeup(p,(dlong *)int2,&locmach);
      goto_more_exec;
    }

  goto_fail_label;

 get_int_proceed_is_var:
  *p = *(dlong *)(arg_place(locpc,2,get_int_proceed));
  locpc = contpreg;
  trail(p,bh,loctrreg);
  goto_more_exec;
}

hcase(add_integer_p)
add_integer_p_label:
profile_builtin(add_integer_p);
{
  dlong areg1, areghelp;
  dlong yvar2;
  dlong *p2;
  dlong i;

  areg1 = *(areg_type *)(arg_place(locpc,1,add_integer_p));
  yvar2 = *(yvar_type *)(arg_place(locpc,2,add_integer_p));
  p2 = (dlong *)ereg[yvar2];
  areghelp = *(areg_type *)(arg_place(locpc,3,add_integer_p));
  args(areghelp) = (dlong)p2;
  deref(p2);
  i = *(dlong *)(arg_place(locpc,4,add_integer_p));
  locpc += add_integer_p_len;

  if (is_smallint(p2))
    {
      dlong res;
      res = i + get_smallint(p2);
      if (in_smallint_range(res))
        build_smallint(&(args(areg1)),res);
      else
	build_bigint_from_out_of_range_smallint(&(args(areg1)),res,lochreg);
      goto_more_exec;
    }

  if (is_real(p2))
    {
      build_real(&(args(areg1)),i+get_real(p2),lochreg);
      goto_more_exec;
    }

  if (! is_bigint(p2))
    goto arith_error_add_integer_p;


  {
    dlong res = addsub_two_inumbers(p2,(dlong *)make_smallint(i),1,&locmach);
    if (! res) goto arith_error_add_integer_p;
    args(areg1) = res;
  }
  goto_more_exec;
}

hcase(add_integer)
add_integer_label:
profile_builtin(add_integer);
{
  dlong areg1;
  dlong areg2;
  dlong *p2;
  dlong i;

  areg1 = *(areg_type *)(arg_place(locpc,1,add_integer));
  areg2 = *(areg_type *)(arg_place(locpc,2,add_integer));
  p2 = (dlong *)args(areg2);
  deref(p2);
  i = *(dlong *)(arg_place(locpc,3,add_integer));
  locpc += add_integer_len;

  if (is_smallint(p2))
    {
      dlong res;
      res = i + get_smallint(p2);
      if (in_smallint_range(res))
        build_smallint(&(args(areg1)),res);
      else
	build_bigint_from_out_of_range_smallint(&(args(areg1)),res,lochreg);
      goto_more_exec;
    }

  if (is_real(p2))
    {
      build_real(&(args(areg1)),i+get_real(p2),lochreg);
      goto_more_exec;
    }

  if (! is_bigint(p2))
    goto arith_error_add_integer;


  {
    dlong res = addsub_two_inumbers(p2,(dlong *)make_smallint(i),1,&locmach);
    if (! res) goto arith_error_add_integer;
    args(areg1) = res;
  }
  goto_more_exec;
}

hcase(add1)
add1_label:
profile_builtin(add1);
{
  dlong areg1;
  dlong *p1;
  dlong i;

  areg1 = *(areg_type *)(arg_place(locpc,1,add1));
  p1 = (dlong *)args(areg1);
  deref(p1);
  locpc += add1_len;

  if (is_smallint(p1))
    {
      dlong res;
      res = 1 + get_smallint(p1);
      if (in_smallint_range(res))
        build_smallint(&(args(areg1)),res);
      else
	build_bigint_from_out_of_range_smallint(&(args(areg1)),res,lochreg);
      goto_more_exec;
    }

  if (is_real(p1))
    {
      build_real(&(args(areg1)),1+get_real(p1),lochreg);
      goto_more_exec;
    }

  if (! is_bigint(p1))
    goto arith_error_add1;


  {
    dlong res = addsub_two_inumbers(p1,(dlong *)make_smallint(1),1,&locmach);
    if (! res) goto arith_error_add1;
    args(areg1) = res;
  }
  goto_more_exec;
}

hcase(sub1)
sub1_label:
profile_builtin(sub1);
{
  dlong areg1;
  dlong *p1;
  dlong i;

  areg1 = *(areg_type *)(arg_place(locpc,1,sub1));
  p1 = (dlong *)args(areg1);
  deref(p1);
  locpc += sub1_len;

  if (is_smallint(p1))
    {
      dlong res;
      res = get_smallint(p1) - 1;
      if (in_smallint_range(res))
        build_smallint(&(args(areg1)),res);
      else
	build_bigint_from_out_of_range_smallint(&(args(areg1)),res,lochreg);
      goto_more_exec;
    }

  if (is_real(p1))
    {
      build_real(&(args(areg1)),get_real(p1)-1,lochreg);
      goto_more_exec;
    }

  if (! is_bigint(p1))
    goto arith_error_sub1;


  {
    dlong res = addsub_two_inumbers(p1,(dlong *)make_smallint(1),0,&locmach);
    if (! res) goto arith_error_add_integer;
    args(areg1) = res;
  }
  goto_more_exec;
}

hcase(integer_sub)
integer_sub_label:
profile_builtin(integer_sub);
{
  dlong areg1;
  dlong areg2;
  dlong *p2;
  dlong i;

  areg1 = *(areg_type *)(arg_place(locpc,1,integer_sub));
  areg2 = *(areg_type *)(arg_place(locpc,2,integer_sub));
  p2 = (dlong *)args(areg2);
  deref(p2);
  i = *(dlong *)(arg_place(locpc,3,integer_sub));
  locpc += integer_sub_len;

  if (is_smallint(p2))
    {
      dlong res;
      res = i - get_smallint(p2);
      if (in_smallint_range(res))
        build_smallint(&(args(areg1)),res);
      else
        build_bigint_from_out_of_range_smallint(&(args(areg1)),res,lochreg);
      goto_more_exec;
    }

  if (is_real(p2))
    {
      build_real(&(args(areg1)),i-get_real(p2),lochreg);
      goto_more_exec;
    }

  
  if (! is_bigint(p2))
    goto arith_error_integer_sub;

  {
    dlong res = addsub_two_inumbers((dlong *)make_smallint(i),p2,0,&locmach);
    if (! res) goto arith_error_integer_sub;
    args(areg1) = res;
  }
  goto_more_exec;
}

hcase(add_float)
add_float_label:
profile_builtin(add_float);
{
  dlong areg1;
  dlong areg2;
  dlong *p2;
  dfloat f1;

  areg1 = *(areg_type *)(arg_place(locpc,1,add_float));
  areg2 = *(areg_type *)(arg_place(locpc,2,add_float));
  p2 = (dlong *)args(areg2);
  deref(p2);
  get_double_arg(f1,(arg_place(locpc,3,add_float)));
  locpc += add_float_len;

  if (is_smallint(p2))
    build_real(&(args(areg1)),f1+get_smallint(p2),lochreg)
      else
	if (is_real(p2))
	  build_real(&(args(areg1)),f1+get_real(p2),lochreg)
	    else goto arith_error_add_float;

  goto_more_exec;
}

hcase(float_sub)
float_sub_label:
profile_builtin(float_sub);
{
  dlong areg1;
  dlong areg2;
  dlong *p2;
  dfloat f1;

  areg1 = *(areg_type *)(arg_place(locpc,1,float_sub));
  areg2 = *(areg_type *)(arg_place(locpc,2,float_sub));
  p2 = (dlong *)args(areg2);
  deref(p2);
  get_double_arg(f1,(arg_place(locpc,3,float_sub)));
  locpc += float_sub_len;

  if (is_smallint(p2))
    build_real(&(args(areg1)),f1-get_smallint(p2),lochreg)
      else
	if (is_real(p2))
	  build_real(&(args(areg1)),f1-get_real(p2),lochreg)
	    else goto arith_error_float_sub;

  goto_more_exec;
}


hcase(builtin_traverselist_rev_0)
builtin_traverselist_rev_0_label:
profile(builtin_traverselist_rev_0);
{
  /*
        allocate Y3
        getlist A1
        unipvar Y2
        unitvar A1
        builtin_traverselist_rev_0
        call a/1 (user)
  */

  dlong *p1;

 more_traverse_list_rev:
  p1 = (dlong *)args(1); deref(p1);
  if (! is_list(p1))
    {
      locpc = contpreg;
      goto_more_exec;
    }

  // getlist A1
  p1 = get_list_pointer(p1);
  // unitvar A1
  args(1) = p1[1];
  // allocate 3
  {
    dlong *loctos;
    
    loctos = tos;
    tos = loctos = loctos - 3;
    *loctos = (dlong)ereg;
    ereg = loctos;
    if (loctos < ls_limit)
      loctos = deal_with_ls_overflow(&locmach);
    *(loctos+1) = (dlong)contpreg;
  }

  // unipvar Y2
  ereg[2] = (dlong)p1;

  contpreg = (codep)(locpc + builtin_traverselist_rev_0_len + 
		     call_len + active_yvar_len);

  goto more_traverse_list_rev;
}


hcase(builtin_traverselist_rev1_0)
builtin_traverselist_rev1_0_label:
profile(builtin_traverselist_rev1_0);
{
  /*
       allocate 4
       getlist 1
       unipvar 2
       unitvar 1
       builtin_traverselist_rev1_0
       getpvar 3 2
       call a/1 (user)
  */

  dlong *p1;

 more_traverse_list_rev1:
  p1 = (dlong *)args(1); deref(p1);
  if (! is_list(p1))
    {
      locpc = contpreg;
      goto_more_exec;
    }

  // getlist A1
  p1 = get_list_pointer(p1);
  // unitvar A1
  args(1) = p1[1];
  // allocate 4
  {
    dlong *loctos;
    
    loctos = tos;
    tos = loctos = loctos - 4;
    *loctos = (dlong)ereg;
    ereg = loctos;
    if (loctos < ls_limit)
      loctos = deal_with_ls_overflow(&locmach);
    *(loctos+1) = (dlong)contpreg;
  }

  // unipvar Y2
  ereg[2] = (dlong)p1;

  // getpvar 3 2
  ereg[3] = args(2);

  contpreg = (codep)(locpc + builtin_traverselist_rev1_0_len + 
		     call_len + active_yvar_len);

  goto more_traverse_list_rev1;
}


hcase(builtin_load_file_2)
builtin_load_file_2_label:
profile_builtin(builtin_load_file_2);
{
  dlong *p1,*p2, areg, res, dones;
  char *fn;

  areg = *(areg_type *)(arg_place(locpc,1,builtin_load_file_2));
  p1 = (dlong *)args(areg);
  deref(p1);
  areg = *(areg_type *)(arg_place(locpc,2,builtin_load_file_2));
  p2 = (dlong *)args(areg);

  locpc += builtin_load_file_2_len;

  if (! is_atom(p1))
    goto error_load_file_2;
  deref(p2);
  fn = get_atom_name(p1);
  if ((dlong)p2 != nil_atom)
    {
      /* extend with the installation path */
      strcpy(fbuffer,install_dir);
      strcat(fbuffer,fn);
      fn = fbuffer;
      areg = 1;
    }
  else
    areg = 0;
  dones = 0;
 do_once_more:
  locmach.P = locpc;
  locmach.CONTP = contpreg;
  res = load_compiled_file(fn,areg);
  locpc = locmach.P;
  contpreg = locmach.CONTP;

  if (res == ALLOK)
    goto_more_exec;

  if (BAD_VERSION_NUMBER == res)
    fatal_message("unexpectedly loading file with bad version number ... giving up");
  if (FILENOTOPEN == res)
    goto error_load_file_2;

  /* res == NOTENOUGHSPACE */
  locmach.P = locpc; locmach.CONTP = contpreg;
  res = code_gc(dones, &locmach); /* if argument > 0, expand, else collect */
  locpc = locmach.P; contpreg = locmach.CONTP;
  if ((! res) && (dones > 0))
    goto_fail_label;
  dones +=1;
  goto do_once_more;
}

hcase(builtin_file_time_stamp_3)
builtin_file_time_stamp_3_label:
profile_builtin(builtin_file_time_stamp_3);
{
  dlong *p1,*p2, *p3;
  dlong i1, i3;
  FILE *F;
  struct stat stat_buff1;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_file_time_stamp_3)));
  deref(p1);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_file_time_stamp_3)));
  deref(p2);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_file_time_stamp_3)));
  deref(p3);
  if ((! is_atom(p1)) || (! is_ref(p2)) || (! is_smallint(p3)))
    goto_fail_label;
  i3 = get_smallint(p3);
  locpc += builtin_file_time_stamp_3_len;

  stat_buff1.st_mtime = 0;
  i1 = stat(get_atom_name(p1),&stat_buff1);
  i1 = stat_buff1.st_mtime;
  if (i1 && i3)
    {
      /* the file exists and it is a .w file */
      F = fopen(get_atom_name(p1),"r");
      if (F == 0)
	i1 = 0;
      else
	{
	  i3 = compare_version(F);
	  fclose(F);
	  if (i3 == 0)
	    i1 = 0;
	}
    }
  trail(p2,bh,loctrreg);
  if (in_smallint_range(i1))
    build_smallint(p2,i1);
  else
    build_bigint_from_out_of_range_smallint(p2,i1,lochreg);
  goto_more_exec;
}

hcase(builtin_needs_recompile_2)
builtin_needs_recompile_2_label:
profile_builtin(builtin_needs_recompile_2);
{
  dlong *p1,*p2;
  dlong i1,ignore_result;
  FILE *F;
  struct stat stat_buff1, stat_buff2;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_needs_recompile_2)));
  deref(p1);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_needs_recompile_2)));
  deref(p2);
  if ((! is_atom(p1)) || (! is_atom(p2)))
    goto_fail_label;
  ignore_result = stat(get_atom_name(p1),&stat_buff1);
  ignore_result = stat(get_atom_name(p2),&stat_buff2);
  locpc += builtin_needs_recompile_2_len;

  /* both files must exist ! */

  if (stat_buff1.st_mtime > stat_buff2.st_mtime)
    goto_more_exec;
  F = fopen(get_atom_name(p2),"r");
  if (F == 0)
    goto_more_exec;
  i1 = compare_version(F);
  fclose(F);
  if (i1 == 0)
    goto_more_exec;
  goto_fail_label;
}

hcase(builtin_file_exists_1)
builtin_file_exists_1_label:
profile_builtin(builtin_file_exists_1);
{
  dlong *p1;
  dlong i1;
  struct stat stat_buff1, stat_buff2;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_needs_recompile_2)));
  deref(p1);
  if (! is_atom(p1))
    goto_fail_label;
  i1 = stat(get_atom_name(p1),&stat_buff1);
  locpc += builtin_file_exists_1_len;

  if (! i1)
    goto_more_exec;
  goto_fail_label;
}


hcase(builtin_var_1)
builtin_var_1_label:
profile_builtin(builtin_var_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_var_1)));
  locpc += builtin_var_1_len;
  deref(p);
  if (is_ref(p) || is_attributed(p))
    goto_more_exec;
  goto_fail_label
    }

hcase(builtin_nonvar_1)
builtin_nonvar_1_label:
profile_builtin(builtin_nonvar_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nonvar_1)));
  locpc += builtin_nonvar_1_len;
  deref(p);
  if (is_ref(p) || is_attributed(p))
    goto_fail_label
      goto_more_exec;
}

hcase(builtin_atom_1)
builtin_atom_1_label:
profile_builtin(builtin_atom_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_atom_1)));
  locpc += builtin_atom_1_len;
  deref(p);
  if (! is_atom(p))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_integer_1)
builtin_integer_1_label:
profile_builtin(builtin_integer_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_integer_1)));
  locpc += builtin_integer_1_len;
  deref(p);
  if (is_smallint(p)) goto_more_exec;
  if (is_bigint(p)) goto_more_exec;
  goto_fail_label;
}

hcase(builtin_bigint_1)
builtin_bigint_1_label:
profile_builtin(builtin_bigint_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_bigint_1)));
  locpc += builtin_bigint_1_len;
  deref(p);
  if (! is_bigint(p))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_bigint_print_1)
builtin_bigint_print_1_label:
profile(builtin_bigint_print_1);
{
  dlong areg1;
  dlong *p;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_bigint_print_1));
  p = (dlong *)args(areg1); deref(p);
  if (! is_bigint(p))
    goto_fail_label;
  bigint_print(current_out,p, &locmach);
  locpc += builtin_bigint_print_1_len;
  goto_more_exec;
}

hcase(builtin_float_1)
builtin_float_1_label:
profile_builtin(builtin_float_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_float_1)));
  locpc += builtin_float_1_len;
  deref(p);
  if (! is_real(p))
    goto_fail_label
      goto_more_exec;
}

hcase(builtin_nonnumber_1)
builtin_nonnumber_1_label:
profile_builtin(builtin_nonnumber_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nonnumber_1)));
  locpc += builtin_nonnumber_1_len;
  deref(p);
  if (is_number(p))
    goto_fail_label
      goto_more_exec;
}

hcase(builtin_number_1)
builtin_number_1_label:
profile_builtin(builtin_number_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_number_1)));
  locpc += builtin_number_1_len;
  deref(p);
  if (! is_number(p))
    goto_fail_label
      goto_more_exec;
}

hcase(builtin_atomic_1)
builtin_atomic_1_label:
profile_builtin(builtin_atomic_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_atomic_1)));
  locpc += builtin_atomic_1_len;
  deref(p);
  if (! is_atomic(p))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_compound_1)
builtin_compound_1_label:
profile_builtin(builtin_compound_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_compound_1)));
  locpc += builtin_compound_1_len;
  deref(p);
  if (! is_compound(p))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_has_a_definition_1)
builtin_has_a_definition_1_label:
profile_builtin(builtin_has_a_definition_1);
{
  dlong *p;
  dlong symbol;
  codep pc;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_has_a_definition_1)));
  locpc += builtin_has_a_definition_1_len;
  deref(p);
  if (is_list(p)) goto_more_exec; /* because [file] */
  if (is_atom(p))
    symbol = get_atom_index(p);
  else
    if (is_struct(p))
      {
	p = get_struct_pointer(p);
	symbol = get_funct_symb(*p);
	if (symbol == colon_2)
	  {
	    dlong *p1, *p2;
	    p1 = p+1; deref(p1);
	    p2 = p+2; deref(p2);
	    if (! is_atom(p1)) goto error_has_a_definition_1;
	    if (is_atom(p2))
	      symbol = sym_lookup(0,
				  symtab[get_atom_index(p2)].name,
				  get_atom_index(p1));
	    else
	      if (is_struct(p2))
		{
		  dlong arity, module;
		  char *name;
		  p2 = get_struct_pointer(p2);
		  symbol = get_funct_symb(*p2);
		  arity = symtab[symbol].arity;
		  name = symtab[symbol].name;
		  module = get_atom_index(p1);
		  symbol = sym_lookup(arity,name,module);
		}
	      else goto_fail_label
	  }
      }
    else
      goto_fail_label

  pc = main_entrypoint(symbol);

  if ((pc == 0) || (points_to_unknown_call(pc)))
    goto_fail_label

      goto_more_exec;
}

hcase(builtin_callable_1)
builtin_callable_1_label:
profile_builtin(builtin_callable_1);
{
  dlong *p;
  dlong symbol;
  codep pc;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_callable_1)));
  locpc += builtin_callable_1_len;
  deref(p);
  if (is_list(p)) goto_more_exec;
  else if (is_atom(p)) goto_more_exec;
  else if (is_struct(p)) goto_more_exec;
  else goto_fail_label;

}

hcase(test_callable)
test_callable_label:
profile_builtin(test_callable);
{
  dlong *p;
  dlong symbol;
  codep pc;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,test_callable)));
  locpc += test_callable_len;
  deref(p);
  if (is_struct(p)) goto_more_exec;
  else if (is_atom(p)) goto_more_exec;
  else if (is_list(p)) goto_more_exec;
    else
    {
      locpc -= test_callable_len;
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_callable)));
      goto_more_exec;
    }

}

hcase(builtin_pred_erase_1)
builtin_pred_erase_1_label:
profile_builtin(builtin_pred_erase_1);
{
  dlong *p;
  dlong symbol;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_pred_erase_1)));
  locpc += builtin_pred_erase_1_len;
  deref(p);
  if (is_atom(p))
    symbol = get_atom_index(*p);
  else
    if (is_struct(p))
      {
	p = get_struct_pointer(p);
	symbol = get_funct_symb(*p);
      }
    else
      goto error_pred_erase_1;
  if (entries[symbol])
    make_pred_without_clauses(symbol);

  goto_more_exec;
}

hcase(builtin_low_is_list_1)
builtin_low_is_list_1_label:
profile_builtin(builtin_low_is_list_1);
{
  dlong *p;
  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_low_is_list_1)));
  locpc += builtin_low_is_list_1_len;
  deref(p);
  if (! is_list(p))
    goto_fail_label
      goto_more_exec;
}

hcase(builtin_functor_3)
builtin_functor_3_label:
profile_builtin(builtin_functor_3);
{
  dlong *p1;
  dlong *p2;
  dlong *p3;
  dlong arity;
  
 redo_functor_3:

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_functor_3)));
//   locpc += builtin_functor_3_len;
//   locpc = contpreg;
  deref2(p1, goto functor3_ref1);

  goto functor3_nonref1;

 functor3_ref1:

  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_functor_3)));
  deref(p3);
  if (is_smallint(p3))
    arity = get_smallint(p3);
  else goto error_functor_3;

  if ((lochreg+arity+10) > heap_limit)
    {
      macro_deal_with_heap_overflow(3,arity+10);
      goto redo_functor_3;
    }
  
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_functor_3)));
  deref(p2);
  if (is_atom(p2))
    {
      if (arity == 0)
	{
	  locpc = contpreg;
	  if (is_ref(p1))
	    {
	      trail(p1,bh,loctrreg);
	      *p1 = (dlong)p2;
	    }
	  else
	    { safe_wakeup(p1,p2,&locmach); }
	  goto_more_exec;
	}

      {
	dlong *h = lochreg;

	if ((arity == locmach.memo_arity) && ((dlong)p2 == locmach.memo_atom))
	  {
	    *h = locmach.memo_func;
	    if (is_ref(p1))
	      {
		trail(p1,bh,loctrreg);
		*p1 = make_struct_p(h);
	      }
	    else
	      { safe_wakeup(p1,(dlong *)(make_struct_p(h)),&locmach); }
	    locpc = contpreg;
	    h++;
	    lochreg = h + arity;
	    while (arity--)
	      {
		*h = (dlong)h;
		h++;
	      }
	    goto_more_exec;
	  }
	else
	  {
	    char *name;
	    dlong index,mod;
	    if ((arity > MAX_ARITY) || (arity < 0))
	      { locpc += builtin_functor_3_len; goto error_functor_3;}
	    locpc = contpreg;
	    name = get_atom_name(p2);
	    if (((dlong)p2 == atom_dot_0) && (arity == 2))
	      {
		if (is_ref(p1))
		  {
		    trail(p1,bh,loctrreg);
		    *p1 = make_list(h);
		    h--;
		  }
		else
		  { 
		    safe_wakeup(p1,(dlong *)(make_list(h)),&locmach);
		    h--;
		  }
	      }
	    else
	      {
		mod = get_atom_module(p2);
		if (is_ref(p1))
		  {
		    trail(p1,bh,loctrreg);
		    *p1 = make_struct_p(h);
		  }
		else
		  { safe_wakeup(p1,(dlong *)(make_struct_p(h)),&locmach); }
		index = sym_lookup(arity,name,mod);
		*h = make_funct(index);
	      }
	  }
	h++;
	lochreg = h + arity;
	while (arity--)
	  {
	    *h = (dlong)h;
	    h++;
	  }
	goto_more_exec;
      }
    }
  
  if (! is_atomic(p2)) goto error_functor_3;
  if (arity != 0) goto error_functor_3;

  if (is_ref(p1))
    {
      trail(p1,bh,loctrreg);
      *p1 = (dlong)p2;
    }
  else
    { safe_wakeup(p1,p2,&locmach); }
  
  goto_more_exec;
  
 functor3_nonref1:

  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_functor_3)));
  deref(p2);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_functor_3)));
  deref(p3);
  locpc += builtin_functor_3_len;
  if (is_struct(p1))
    {
      dlong *q;
      q = get_struct_pointer(p1);
      locmach.memo_func = *q;
      locmach.memo_arity = 
	arity = get_funct_arity(*q);
      p1 = (dlong *)make_atom(sym_lookup(0,get_funct_name(*q),get_funct_module(*q)));
      locmach.memo_atom = (dlong)p1;
      goto label1_functor_3;
    }
  
  if (is_atomic(p1))
    {
      arity = 0;
    label1_functor_3:
      switch (tag(p2))
	{
	case REF0:
	case REF1:
	  trail(p2,bh,loctrreg);
	  *p2 = (dlong)p1;
	  break;
	case ATT:
	  safe_wakeup(p2,p1,&locmach);
	  break;
	case SIMPLE:
	  if (p1 != p2) goto_fail_label;
	  break;
	case NUMB:
	  if (is_smallint(p2))
	    { if (! is_smallint(p1) || (get_smallint(p1) != get_smallint(p2))) goto_fail_label;}
	  else
	    if (is_real(p2))
	      { if (! is_real(p1) || (get_real(p1) != get_real(p2))) goto_fail_label; }
	  else
	    /* must be a bigint now */
	    {
	      dlong res, er;
	      res = compare_two_inumbers(p1,p2,&locmach,&er);
	      if (er) goto_fail_label;
	      if (res) goto_fail_label;
	    }
	  break;
	case STRING:
	  if (! is_string(p1)) goto_fail_label;
	  if (get_string_length(p1) != get_string_length(p2)) goto_fail_label;
	  if (! equal_strings(get_string_length(p1),get_string_val(p1),get_string_val(p2))) goto_fail_label;
	  break;
	default:
	  goto_fail_label;
	}

      if (is_ref(p3))
	{
	  trail(p3,bh,loctrreg);
	  build_smallint(p3,arity);
	}
      else
	if (is_attributed(p3))
	  { safe_wakeup(p3,(dlong *)(make_smallint(arity)),&locmach); }
	else
	  if (is_smallint(p3))
	    {
	      if (get_smallint(p3) != arity)
		goto_fail_label;
	    }
	  else goto error_functor_3;
      goto_more_exec;
    }
  
  if (is_list(p1))
    {
      arity = 2;
      p1 = (dlong *)atom_dot_0;
      goto label1_functor_3;
    }

  /* p1 can only be a ref or a attvar */

}

hcase(builtin_functor_spec_3)
builtin_functor_spec_3_label:
profile_builtin(builtin_functor_spec_3);
{
  dlong *p1;
  dlong areg2, areg3;
  dlong arity;

  areg2 = *(areg_type *)(arg_place(locpc,2,builtin_functor_spec_3));
  areg3 = *(areg_type *)(arg_place(locpc,3,builtin_functor_spec_3));
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_functor_spec_3))); deref(p1);
  
  locpc += builtin_functor_spec_3_len;
  if (is_struct(p1))
    {
      dlong q;
      q = *(get_struct_pointer(p1));
      locmach.memo_func = q;
      p1 = (dlong *)make_atom(sym_lookup(0,get_funct_name(q),get_funct_module(q)));
      locmach.memo_atom = (dlong)p1;
      locmach.memo_arity = 
	arity = get_funct_arity(q);
      args(areg2) = (dlong)p1;
      args(areg3) = make_smallint(arity);
      goto_more_exec;
    }

  if (is_atomic(p1))
    {
      arity = 0;
      goto label1_functor_spec_3;
    }

  if (is_list(p1))
    {
      arity = 2;
      p1 = (dlong *)atom_dot_0;
      goto label1_functor_spec_3;
    }

  goto error_functor_spec_3;
  goto_fail_label;

 label1_functor_spec_3:
  args(areg2) = (dlong)p1;
  args(areg3) = make_smallint(arity);
  goto_more_exec;
}

hcase(builtin_mgu_variables_3)
builtin_mgu_variables_3_label:
profile_builtin(builtin_mgu_variables_3);
{
  dlong *p1;
  dlong *p2;
  dlong *p3;
  dlong arity;
  dlong *saveH;
  
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_mgu_variables_3))); deref(p1);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_mgu_variables_3))); deref(p2);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_mgu_variables_3))); deref(p3);
  
  locpc += builtin_mgu_variables_3_len;
//   if (! is_ref(p3))
//     goto error_mgu_variables_3;
//   trail(p3,bh,loctrreg);

  if (p1 == p2)
    {
      *p3 = nil_atom;
      goto_more_exec;
    }

  {
    /* should not count on a choicepoint being pushed */
    dlong saveTR = loctrreg - locmach.limit_trail; /* trail expansion can happen
						      during unify */
    dlong *oldtr;
    dlong *oldBH;

    oldBH = bh; bh = hreg;


    if (! unify_terms(p1,p2,&locmach))
      {
        bh = oldBH;
        goto_fail_label;
      }
    bh = oldBH;

    /* make this heap safe at some point */
    /* be careful with how heapgc affects the trail ! */

    oldtr = locmach.limit_trail + saveTR;
    saveH = hreg;
    while (loctrreg > oldtr)
      {
	dlong *l, *s, *v;
	/* not all trailings where non-destructive: attvars ! */
	s = l = (dlong *)*(--loctrreg);
        v = (dlong *)*(--loctrreg);
	*hreg = (dlong)s;
	deref(l);
	hreg++;
	*hreg = (dlong)(make_list(hreg+1));
	if (is_var(l))
	  {
	    hreg++;
	    *hreg = (dlong)l;
	    hreg++;
	    *hreg = (dlong)(make_list(hreg+1));
	  }
	hreg++;
	*s = (dlong)v;	  
      }
  }

  if (hreg == saveH)
    *p3 = nil_atom;
  else
    {
      *p3 = make_list(saveH);
      *(hreg-1) = nil_atom;
    }

  set_number_of_woken_goals(0,&locmach);
  goto_more_exec;
}

hcase(builtin_mgu_3)
builtin_mgu_3_label:
profile_builtin(builtin_mgu_3);
{
  dlong *p1;
  dlong *p2;
  dlong *p3;
  dlong arity;
  dlong *saveH;
  
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_mgu_3))); deref(p1);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_mgu_3))); deref(p2);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_mgu_3))); deref(p3);
  
  locpc += builtin_mgu_3_len;
  if (! is_ref(p3))
    goto error_mgu_3;
  trail(p3,bh,loctrreg);

  if (p1 == p2)
    {
      *p3 = nil_atom;
      goto_more_exec;
    }

  {
    /* should not count on a choicepoint being pushed */
    dlong saveTR = loctrreg - locmach.limit_trail; /* trail expansion can happen
						      during unify */
    dlong *oldtr;
    dlong *oldBH;

    oldBH = bh; bh = hreg;


    if (! unify_terms(p1,p2,&locmach))
      {
	bh = oldBH;
	goto_fail_label;
      }
    bh = oldBH;

    /* make this heap safe at some point */
    /* be careful with how heapgc affects the trail ! */

    oldtr = locmach.limit_trail + saveTR;
    saveH = hreg;
    while (loctrreg > oldtr)
      {
	dlong *s, *v;
	dlong *hh;
	/* not all trailings where non-destructive: attvars ! */
	s = (dlong *)*(--loctrreg);
	v = (dlong *)*(--loctrreg);
	hh = hreg+2;
	*hreg = make_struct_p(hh);
	hreg++;
	hh[0] = make_funct(symbol_equal_2);
	hh[1] = (dlong)s;
	hh[2] = *s;
	hh += 3;
	*hreg = (dlong)(make_list(hh));
	hreg = hh;
	*s = (dlong)v;	  
      }
  }

  if (hreg == saveH)
    *p3 = nil_atom;
  else
    {
      *p3 = make_list(saveH);
      *(hreg-4) = nil_atom;
    }

  set_number_of_woken_goals(0,&locmach);
  goto_more_exec;
}

hcase(builtin_nocheck_arg_spec_3)
builtin_nocheck_arg_spec_3_label:
profile(builtin_nocheck_arg_spec_3);
{ /* differs from builtin_nocheck_arg_3 in 3th arg which is new */
  dlong *p1;
  dlong *p2;
  dlong areg3;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nocheck_arg_spec_3))); deref(p1);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_nocheck_arg_spec_3))); deref(p2);
  p2 = get_struct_pointer(p2) + get_smallint(p1);
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_nocheck_arg_spec_3));

  locpc += builtin_nocheck_arg_spec_3_len;

  deref(p2); underef(p2);
  args(areg3) = (dlong)p2;
  goto_more_exec;
}

hcase(builtin_nocheck_arg_int_3)
builtin_nocheck_arg_int_3_label:
profile(builtin_nocheck_arg_int_3);
{ /* differs from builtin_nocheck_arg_3 in first arg is manifest*/

  dlong *p2, *p3;

  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_nocheck_arg_int_3))); deref(p2);
  p2 = get_struct_pointer(p2);
  p2 += *(areg_type *)(arg_place(locpc,1,builtin_nocheck_arg_int_3));
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_nocheck_arg_int_3))); deref(p3);

  locpc += builtin_nocheck_arg_int_3_len;

  trail(p3,bh,loctrreg);
  deref(p2); underef(p2);
  *p3 = (dlong)p2;
  goto_more_exec;
}

hcase(builtin_nocheck_arg_int_spec_3)
builtin_nocheck_arg_int_spec_3_label:
profile(builtin_nocheck_arg_int_spec_3);
{ /* differs from builtin_nocheck_arg_3 in 3th arg which is new and first arg is manifest*/

  dlong *p2;
  dlong areg3;

  areg3 = *(areg_type *)(arg_place(locpc,3,builtin_nocheck_arg_int_spec_3));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_nocheck_arg_int_spec_3))); deref(p2);
  p2 = get_struct_pointer(p2) + *(areg_type *)(arg_place(locpc,1,builtin_nocheck_arg_int_spec_3));;

  locpc += builtin_nocheck_arg_int_spec_3_len;

  deref(p2); underef(p2);
  args(areg3) = (dlong)p2;
  goto_more_exec;
}

hcase(builtin_nocheck_arg_3)
builtin_nocheck_arg_3_label:
profile_builtin(builtin_nocheck_arg_3);
{ // differs from arg in that no checking is done:
  // arg1 = int in correct range
  // arg2 = structure (not even a list)
  // arg3 = a free variable and not an attvar

  dlong *p1;
  dlong *p2;
  dlong *p3;

  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_nocheck_arg_3))); deref(p3);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_nocheck_arg_3))); deref(p2);
  p2 = get_struct_pointer(p2);
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nocheck_arg_3))); deref(p1);
  p2 += get_smallint(p1);

  locpc += builtin_nocheck_arg_3_len;

  trail(p3,bh,loctrreg);
  deref(p2); underef(p2);
  *p3 = (dlong)p2;
  goto_more_exec;
}

hcase(builtin_arg_spec_3)
builtin_arg_spec_3_label:
profile(builtin_arg_spec_3);
{ /* differs from builtin_arg_3 in 3th arg which is new */
  dlong *p1;
  dlong *p2;
  dlong index;

  dlong areg3;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_arg_spec_3))); deref(p1);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_arg_spec_3))); deref(p2);
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_arg_spec_3));

  locpc += builtin_arg_spec_3_len;

  if (! is_smallint(p1)) goto error_arg_spec_3;
  index = get_smallint(p1);

  if (is_struct(p2))
    {
      p2 = get_struct_pointer(p2);
      if ((index < 1) || (index > get_funct_arity(*p2))) goto_fail_label;
    }
  else
    if (is_list(p2))
      {
        p2 = get_list_pointer(p2);
        if ((index != 1) && (index != 2)) goto_fail_label;
        index--;
      }
    else goto_fail_label;

  p2 += index;

  deref(p2); underef(p2);
  args(areg3) = (dlong)p2;
  goto_more_exec;
}

hcase(builtin_arg_int_3)
builtin_arg_int_3_label:
profile(builtin_arg_int_3);
{ /* differs from builtin_arg_3 in first arg is manifest*/

  dlong *p2, *p3;
  dlong index;

  index = *(areg_type *)(arg_place(locpc,1,builtin_arg_int_3));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_arg_int_3))); deref(p2);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_arg_int_3))); deref(p3);

  locpc += builtin_arg_int_3_len;

  if (is_struct(p2))
    {
      p2 = get_struct_pointer(p2);
      if ((index < 1) || (index > get_funct_arity(*p2))) goto_fail_label
                                                           p2 += index;
      if (is_ref(p3))
        {
          trail(p3,bh,loctrreg);
          deref(p2); underef(p2);
          *p3 = (dlong)p2;
          goto_more_exec;
        }
      if (unify_terms(p2,p3,&locmach))
        goto_more_exec;
    }
  else
    if (is_list(p2))
      {
        p2 = get_list_pointer(p2);
        if ((index == 1) || (index == 2))
          {
            if (unify_terms(p2+index-1,p3,&locmach))
              goto_more_exec;
          }
      }

  goto_fail_label;
}

hcase(builtin_arg_int_spec_3)
builtin_arg_int_spec_3_label:
profile(builtin_arg_int_spec_3);
{ /* differs from builtin_arg_3 in 3th arg which is new and first arg is manifest*/

  dlong *p2;
  dlong index;
  dlong areg3;

  index = *(areg_type *)(arg_place(locpc,1,builtin_arg_int_spec_3));
  areg3 = *(areg_type *)(arg_place(locpc,3,builtin_arg_int_spec_3));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_arg_int_spec_3))); deref(p2);

  locpc += builtin_arg_int_spec_3_len;

  if (is_struct(p2))
    {
      p2 = get_struct_pointer(p2);
      if ((index < 1) || (index > get_funct_arity(*p2))) goto_fail_label;
    }
  else
    if (is_list(p2))
      {
        p2 = get_list_pointer(p2);
        if ((index != 1) && (index != 2)) goto_fail_label;
        index--;
      }
    else goto_fail_label;

  p2 += index;

  deref(p2); underef(p2);
  args(areg3) = (dlong)p2;
  goto_more_exec;
}

hcase(builtin_arg_3)
builtin_arg_3_label:
profile_builtin(builtin_arg_3);
{
  dlong *p1;
  dlong *p2;
  dlong *p3;
  dlong index;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_arg_3))); deref(p1);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_arg_3))); deref(p3);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_arg_3))); deref(p2);

  locpc += builtin_arg_3_len;

  if (! is_smallint(p1)) goto error_arg_3;
  index = get_smallint(p1);

  if (is_struct(p2))
    {
      p2 = get_struct_pointer(p2);
      if ((index < 1) || (index > get_funct_arity(*p2))) goto_fail_label
							   p2 += index;
      if (is_ref(p3))
	{
	  trail(p3,bh,loctrreg);
	  deref(p2); underef(p2);
	  *p3 = (dlong)p2;
	  goto_more_exec;
	}
      if (unify_terms(p2,p3,&locmach))
	goto_more_exec;
    }
  else
    if (is_list(p2))
      {
	p2 = get_list_pointer(p2);
	if ((index == 1) || (index == 2))
	  {
	    if (unify_terms(p2+index-1,p3,&locmach))
	      goto_more_exec;
	  }
      }

  goto_fail_label;
}


hcase(builtin_setarg_3)
builtin_setarg_3_label:
profile_builtin(builtin_setarg_3);
{
  dlong *p1;
  dlong *p2;
  dlong *p3;
  dlong index;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_setarg_3))); deref(p1);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_setarg_3))); deref(p3);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_setarg_3))); deref(p2);
  underef(p3);

  locpc += builtin_setarg_3_len;

  if (! is_smallint(p1)) goto error_setarg_3;
  index = get_smallint(p1);

  if (is_struct(p2))
    {
      p2 = get_struct_pointer(p2);
      if ((index < 1) || (index > get_funct_arity(*p2))) goto_fail_label
							   p2 += index;
      // trail(p2,bh,loctrreg); *p2 = (dlong)p3;
      // value_trail(p2,bh,loctrreg); *p2 = (dlong)p3;
      noye_trail(p2,bh,loctrreg,p3);
      goto_more_exec;
    }
  else
    if (is_list(p2))
      {
	p2 = get_list_pointer(p2);
	if ((index != 1) && (index != 2))
	  goto error_setarg_3;
	p2 += (index-1);
	// trail(p2,bh,loctrreg); *p2 = (dlong)p3;
	// value_trail(p2,bh,loctrreg); *p2 = (dlong)p3;
	noye_trail(p2,bh,loctrreg,p3);
	goto_more_exec;
      }

  goto_fail_label
    }

hcase(builtin_nb_setarg_3)
builtin_nb_setarg_3_label:
profile_builtin(builtin_nb_setarg_3);
{
  dlong *p1;
  dlong *p2;
  dlong *p3;
  dlong index;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nb_setarg_3))); deref(p1);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_nb_setarg_3))); deref(p3);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_nb_setarg_3))); deref(p2);
  underef(p3);

  locpc += builtin_nb_setarg_3_len;

  if (! is_smallint(p1)) goto error_nb_setarg_3;
  index = get_smallint(p1);

  if (is_struct(p2))
    {
      p2 = get_struct_pointer(p2);
      if ((index < 1) || (index > get_funct_arity(*p2))) goto_fail_label;
      p2 += index;
    }
  else
    if (is_list(p2))
      {
	p2 = get_list_pointer(p2);
	if ((index != 1) && (index != 2)) goto_fail_label;
	p2 += (index-1);
      }
  else
    goto_fail_label;

  *p2 = (dlong)p3;
  /* freeze heap as with nb_setval */
  if (!has_atom_tag(p3)) /* smallint or char or atom */
    /* this one could become
       newtop = max(segment p2; segment p3 points to);
       adapt_freeze_hreg(newtop); --> tighter setting of FH
    */
    {
      /* if p3 crosses H boundary forwards, then trail; --> for gengc */
      /* the following is an approximation */
      value_trail(p2,bh,trreg);
      adapt_freeze_hreg(lochreg);
    }
  goto_more_exec;
}

hcase(builtin_univ_2)
builtin_univ_2_label:
profile_builtin(builtin_univ_2);
{
  dlong *p1,*p2;

 redo_univ_2:
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_univ_2)));
  deref(p2);
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_univ_2)));
  deref(p1);
  locpc += builtin_univ_2_len;

  if (is_compound(p1))
    {
      dlong startl;
      dlong sym, arity;

      if (is_list(p1))
	{
	  p1 = get_list_pointer(p1);
	  sym = symbol_dot_2;
	  arity = 2;
	}
      else
	{
	  p1 = get_struct_pointer(p1);
	  sym = get_funct_symb(*p1);
	  arity = get_funct_arity(*p1);
	  p1++;
	}

      if ((lochreg+(2*arity)+10) > heap_limit)
	{
	  locpc -= builtin_univ_2_len;
	  macro_deal_with_heap_overflow(2,(2*arity)+10);
	  goto redo_univ_2;
	  }

      sym = sym_lookup(0,symtab[sym].name,symtab[sym].module);
      startl = make_list(lochreg);
      *lochreg++ = make_atom(sym);
      *lochreg = make_list(lochreg+1);
      lochreg++;
      while (arity--)
	{
	  *lochreg = *p1;
	  lochreg++;
	  *lochreg = make_list(lochreg+1);
	  lochreg++;
	  p1++;
	}
      *(lochreg-1) = nil_atom;
      if (! unify_terms((dlong *)startl,p2,&locmach))
	goto_fail_label;
      goto_more_exec;
    }

  if (is_var(p1))
    {
      /* p2 should be a list starting with an atom */
      /* or a single element list with a number    */
      dlong len;
      dlong *wheretoputit, *r, *psave;

      psave = p2;

      len = 0;
      while (is_list(p2) && (len <= MAX_ARITY))
        {
          p2 = get_list_pointer(p2);
          len++;
          p2++;
          deref(p2);
        }

      if ((lochreg+len+10) > heap_limit)
        {
          locpc -= builtin_univ_2_len;
          macro_deal_with_heap_overflow(2,len+10);
          goto redo_univ_2;
	  }

      p2 = psave;

      if (is_ref(p1))
	trail(p1,bh,loctrreg);
      len = 0;
      wheretoputit = lochreg;
      while (is_list(p2) && (len <= MAX_ARITY))
	{
	  r = p2 = get_list_pointer(p2);
	  deref(r); underef(r);
	  *wheretoputit = (dlong)r;
	  len++;
	  wheretoputit++;
	  p2++;
	  deref(p2);
	}
	    
      if ((len == 0) || (! is_atomic(*lochreg)) || ((dlong)p2 != nil_atom))
	goto error_univ_2;

      if (len == 1)
	{
	  if (is_ref(p1))
	    *p1 = *lochreg;
	  else
	    { safe_wakeup(p1,(dlong *)(*lochreg),&locmach); }
	  goto_more_exec;
	}
	    
      if (! is_atom(*lochreg))
	goto error_univ_2;
	    
      len--; /* is the arity of the structure */
      if ((len == 2) && (!strcmp(get_atom_name(*lochreg),".")))
	{
	  if (is_ref(p1))
	    {
	      *p1 = make_list(lochreg+1) ;
	      lochreg += len+1;
	    }
	  else
	    {
	      lochreg += len+1;
	      safe_wakeup(p1,(dlong *)(make_list(lochreg-len)),&locmach);
	    }
	}
      else
	{
	  *lochreg = make_funct(sym_lookup(len,get_atom_name(*lochreg), get_atom_module(*lochreg)));
	  if (is_ref(p1))
	    {
	      *p1 = make_struct_p(lochreg);
	      lochreg += len+1;
	    }
	  else
	    {
	      lochreg += len+1;
	      safe_wakeup(p1,(dlong *)(make_struct_p(lochreg-len-1)),&locmach);
	    }
	}
      goto_more_exec;
    }

  if ( ! is_atomic(p1))
    goto error_univ_2;

  /* p1 is atomic */
  /* p2 must unify with a list with one element: the atomic */

  *lochreg = (dlong)p1;
  lochreg++;
  *lochreg = nil_atom;
  lochreg++;
  if (! unify_terms(p2,(dlong *)make_list(lochreg-2),&locmach))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_classify_goal_2)
builtin_classify_goal_2_label:
profile(builtin_classify_goal_2);
{
  dlong areg1;
  dlong areg2;
  dlong soort;
  dlong *p1, *p2;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_classify_goal_2));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_classify_goal_2));
  p2 = (dlong *)args(areg2); deref(p2);

  /*
    0 = non-callable
    1 = user-defined callable (or built-in)
    2 = !
    
   */

  switch (tag(p1))
    {
    case REF0:
    case REF1:
    case ATT: soort = 0; break;

    case NUMB:
    case STRING: soort = 0; break;

    case LIST: soort = 1; break;

    case SIMPLE:
      if (is_atom(p1))
	{
	  if ((dlong)p1 == atom_cut) soort = 2;
	  else soort = 1;
	}
      else soort = 0;
      break;

    case STRUCT:
      {
        dlong i;
        char *pc;
        p1 = get_struct_pointer(p1);
        i = get_funct_arity(*p1);
        if (i == 2)
          {
            /* , ; -> */
            if (*p1 == funct_comma_2) soort = 3;
	    else if (*p1 == funct_semicolon_2) soort = 4;
	    else if (*p1 == funct_arrow_2) soort = 5;
	    else soort = 1;
          }
	else
        if (i == 3)
          {
            if (*p1 == funct_if_3) soort = 6;
	    else soort = 1;
          }
	else
	  soort = 1;
      }
    }

  *p2 = make_smallint(soort);
  locpc += builtin_classify_goal_2_len;
  goto_more_exec;
}

hcase(builtin_getenv_2)
builtin_getenv_2_label:
profile_builtin(builtin_getenv_2);
{
  dlong areg1, areg2;
  dlong *p1, *p2;
  char *pc;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_getenv_2));
  p1 = (dlong *)(args(areg1));
  deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_getenv_2));
  p2 = (dlong *)(args(areg2));
  deref(p2);

  if (! is_ref(p2)) goto_fail_label;

  /* we allow ourselves a small twist here: if arg1 is an int,
     we'll return the installation directory */

  if (is_inumber(p1))
    pc = install_dir;
  else
    {
      if (! is_atom(p1)) goto_fail_label;
      pc = getenv(get_atom_name(p1));
      if (! pc) goto_fail_label;
    }

  trail(p2,bh,loctrreg);
  *p2 = make_atom(sym_lookup(0,pc,USER_MOD));

  locpc += builtin_getenv_2_len;
  goto_more_exec;
}


hcase(builtin_write_var_1)
builtin_write_var_1_label:
profile_builtin(builtin_write_var_1);
{
  dlong *p;
  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_write_var_1)));
  locpc += builtin_write_var_1_len;
  deref(p);
  if (! low_write_var1(p,&locmach))
    goto error_write_var_1;
  goto_more_exec;
}

hcase(builtin_write_var_2)
builtin_write_var_2_label:
profile_builtin(builtin_write_var_2);
{
  dlong *p1, *p2;
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_write_var_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_write_var_2)));
  locpc += builtin_write_var_2_len;
  deref(p1); deref(p2);
  if (! low_write_var2(p1, p2, &locmach))
    goto error_write_var_2;
  goto_more_exec;
}

hcase(builtin_write_atomic_3)
builtin_write_atomic_3_label:
profile_builtin(builtin_write_atomic_3);
{
  dlong *p1, *p2, *p3;
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_write_atomic_3)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_write_atomic_3)));
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_write_atomic_3)));
  locpc += builtin_write_atomic_3_len;
  deref(p1); deref(p2); deref(p3);
  low_write_atomic3(p1,p2,&locmach,(dlong)p3);
  goto_more_exec;
}

hcase(builtin_write_atomic_4)
builtin_write_atomic_4_label:
profile_builtin(builtin_write_atomic_4);
{
  dlong *p1, *p2, *p3, *p4;
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_write_atomic_4)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_write_atomic_4)));
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_write_atomic_4)));
  p4 = (dlong *)args(*(areg_type *)(arg_place(locpc,4,builtin_write_atomic_4)));
  locpc += builtin_write_atomic_4_len;
  deref(p1); deref(p2); deref(p3); deref(p4);
  if (! low_write_atomic4(p1,p2,p3,&locmach,(dlong)p4))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_put_1)
builtin_put_1_label:
profile_builtin(builtin_put_1);
{
  dlong i;
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_put_1)));
  locpc += builtin_put_1_len;
  deref(p);
  if (! is_smallint(p))
    goto error_put_1;

  i = get_smallint(p);
  fputc(i,current_out);
  goto_more_exec;
}

hcase(builtin_low_put_2)
builtin_low_put_2_label:
profile_builtin(builtin_low_put_2);
{
  dlong *p1, *p2;
  dlong i;
  FILE *f;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_low_put_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_low_put_2)));
  locpc += builtin_low_put_2_len;
  if (low_put2(p1,p2,&locmach))
    goto_more_exec;
  goto error_low_put_2;
}

hcase(builtin_cputime_1)
builtin_cputime_1_label:
profile_builtin(builtin_cputime_1);
{
  dlong tim;
  dlong *p;

  tim = get_cpu_time();

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_cputime_1)));
  locpc += builtin_cputime_1_len;
  deref(p);
  if (! is_ref(p))
    goto error_cputime_1;
  else
    {
      trail(p,bh,loctrreg);
      build_smallint(p,tim);
    }

  goto_more_exec;
}

hcase(builtin_unixtime_1)
builtin_unixtime_1_label:
profile_builtin(builtin_unixtime_1);
{
  dlong tim;
  dlong *p;

  tim = get_unix_time();

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_unixtime_1)));
  locpc += builtin_unixtime_1_len;
  deref(p);
  if (! is_ref(p))
    goto error_unixtime_1;
  else
    {
      trail(p,bh,loctrreg);
      build_smallint(p,tim);
    }

  goto_more_exec;
}

hcase(builtin_random_1)
builtin_random_1_label:
profile_builtin(builtin_random_1);
{
  dfloat r;
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_random_1)));
  locpc += builtin_random_1_len;
  deref(p);
  if (is_ref(p))
    {
      r = drand48();
      trail(p,bh,loctrreg);
      build_real(p, r, lochreg);
      goto_more_exec;
    }

  if (is_real(p))
    {
      int i = get_real(p);
      srand48(i);
      goto_more_exec;
    }
  goto error_random_1;
}


hcase(builtin_call_1)
builtin_call_1_label:
profile_builtin(builtin_call_1);
{
  dlong areg1;
  dlong *p;
  codep savep;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_call_1));
  p = (dlong *)args(areg1);
  deref(p);
  if ((savep = builtin_metacall(p,&locmach)))
    {
      locpc = savep;
      goto_more_exec;
    }
  locpc += builtin_call_1_len; /* for errormess */
  underef(p); args(1) = (dlong)p;
  goto error_call_1;
}

hcase(builtin_module_call_2)
builtin_module_call_2_label:
profile_builtin(builtin_module_call_2);
{
  dlong *p1, *p2;
  dlong symbol2, mod1;

  inferencesplusplus;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_module_call_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_module_call_2)));
  locpc += builtin_module_call_2_len; /* for errormess */
  deref(p1);
  if (! is_atom(p1)) goto error_module_call_2;
  mod1 = get_atom_index(p1);
//   if (symtab[mod1].module != USER_MOD) goto error_module_call_2;
  deref(p2);
  if (is_struct(p2))
    {
      dlong arity;
  
      p2 = get_struct_pointer(p2);
      {
	codep savep = locpc;
	symbol2 = get_funct_symb(*p2);
	arity = get_funct_arity(*p2);
	if (arity > ARGS)
	  goto error_module_call_2;
	symbol2 = sym_lookup(arity,symtab[symbol2].name,mod1);
	locpc = main_entrypoint(symbol2);
	if (! locpc)
	  {
	    locpc = savep;
	    goto error_module_call_2;
	  }
      }
      p2 += arity;
      while (arity)
	{
	  args(arity) = *p2;
	  p2--;
	  arity--;
	}
      goto_more_exec;
    }

  if (is_atom(p2))
    {
      codep savep = locpc;
      symbol2 = get_atom_index(p2);
      symbol2 = sym_lookup(0,symtab[symbol2].name,mod1);
      locpc = main_entrypoint(symbol2);
      if (! locpc)
	{
	  locpc = savep;
	  goto error_module_call_2;
	}
      goto_more_exec;
    }

  goto error_module_call_2;
}

hcase(builtin_atomic2list_3)
builtin_atomic2list_3_label:
profile_builtin(builtin_atomic2list_3);
{
  dlong *p1,*p2,*p3;
  char *pc;
  dlong what;
  char chararray[3];

 redo_atomic2list:
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_atomic2list_3)));
  deref(p1);
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_atomic2list_3)));
  deref(p2);
  p3 = (dlong *)args(*(areg_type *)(arg_place(locpc,3,builtin_atomic2list_3)));
  deref(p3);
  what = get_smallint(p3);
  locpc += builtin_atomic2list_3_len;
	
  if (is_var(p1))
    {
      dlong len,locali;

      len = listorstring_to_Cstring(p2,locmach.global_charbuf,GLOBAL_CHARBUF_LEN,&locali,what&0x1);
      if (len < 0) goto error_atomic2list_3;

      if ((locali == MAKE_ATOM) || (what&0x2))
	locali = make_atom(sym_lookup(0,locmach.global_charbuf,USER_MOD));
      else
	if (locali == MAKE_INTEGER)
	  locali = make_smallint(atoi(locmach.global_charbuf));  // could be bigint ?
	else
	  {
	    locali = make_number_pointer(lochreg);
	    push_real(lochreg,(atof(locmach.global_charbuf)));
	  }
      if (is_ref(p1))
	{
	  trail(p1,bh,loctrreg);
	  *p1 = locali;
	}
      else
	{ safe_wakeup(p1,(dlong *)locali,&locmach); }

      goto_more_exec;
    }

  if (is_smallint(p1))
    {
      dlong i;
      
      pc = locmach.int_buffer;
      i = get_smallint(p1);
      i = sprintf(pc,"%ld",i);
      locmach.int_buffer[i] = 0;
      pc = locmach.int_buffer;
      goto string_to_list_label;
    }

  if (is_real(p1))
    {
      dfloat f1;
      dlong i;
      pc = fbuffer;
      f1 = get_real(p1);
#ifdef BITS32
      i = sprintf(pc,"%e",f1);
#else
      i = sprintf(pc,"%Le",f1);
#endif
      fbuffer[i] = 0;
      pc = fbuffer;
      goto string_to_list_label;
    }

  if ((!is_atom(p1)) && (!is_char(p1)))
    goto error_atomic2list_3;

    {
      dlong *beginp, *p, len;
      
      if (is_atom(p1))
	{
	  pc = get_atom_name(p1);
	  len = strlen(pc);
	  if (2*len + lochreg > heap_limit)
	    {
	      locpc -= builtin_atomic2list_3_len;
	      macro_deal_with_heap_overflow(3,2*len);
	      goto redo_atomic2list;
	    }
	}
      else
	// if (is_char(p1))
	{
	  pc = chararray;
	  pc[0] = get_character(p1);
	  pc[1] = 0;
	  len = 1;
	}

  string_to_list_label:
    if (*pc == 0)
      {
	if (is_ref(p2))
	  {
	    trail(p2,bh,loctrreg);
	    *p2 = (dlong)nil_atom;
	  }
	else
	  if (is_attributed(p2))
	    { safe_wakeup(p2,(dlong *)nil_atom,&locmach); }
	  else
	    {
	      if (nil_atom != (dlong)p2)
		goto_fail_label;
	    }
      }
    else
      {
	beginp = p = lochreg;
	while (*pc)
	  {
	    build_smallint(p,(*pc));
	    *(p+1) = make_list(p+2);
	    p += 2;
	    pc++;
	  }
	lochreg = p;
	*(p-1) = nil_atom;
	if (is_ref(p2))
	  {
	    trail(p2,bh,loctrreg);
	    *p2 = make_list(beginp);
	  }
	else
	  {
	    dlong d;
	    d = make_list(beginp);
	    if (! unify_terms(p2,(dlong *)d,&locmach))
	      goto_fail_label;
	  }
      }
  }
	
  goto_more_exec;
}

hcase(builtin_stream_property_4)
builtin_stream_property_4_label:
profile(builtin_stream_property_4);
{
//   dlong areg1;
//   dlong areg2;
//   dlong areg3;
//   dlong areg4;
// 
//   areg1 = *(areg_type*)(arg_place(locpc,1,builtin_stream_property_4));
//   areg2 = *(areg_type*)(arg_place(locpc,2,builtin_stream_property_4));
//   areg3 = *(areg_type*)(arg_place(locpc,3,builtin_stream_property_4));
//   areg4 = *(areg_type*)(arg_place(locpc,4,builtin_stream_property_4));

  fprintf(stderr,"builtin_stream_property_4 not yet implemented\n"); return(0);
  locpc += builtin_stream_property_4_len;
  goto_more_exec;
}

hcase(builtin_open_stream_3)
builtin_open_stream_3_label:
profile(builtin_open_stream_3);
{
  dlong *p1, *p2, *p3;
  dlong i;

  p1 = (dlong *)(args(*(areg_type*)(arg_place(locpc,1,builtin_open_stream_3))));
  p2 = (dlong *)(args(*(areg_type*)(arg_place(locpc,2,builtin_open_stream_3))));
  p3 = (dlong *)(args(*(areg_type*)(arg_place(locpc,3,builtin_open_stream_3))));
  locpc += builtin_open_stream_3_len;

  i = low_open_stream_3(p1,p2,&locmach);
  if (i < 0) goto_fail_label;

  deref(p3);
  trail(p3,bh,loctrreg);
  build_smallint(p3,i);
  goto_more_exec;
}

hcase(builtin_low_get_open_stream_3)
builtin_low_get_open_stream_3_label:
profile(builtin_low_get_open_stream_3);
{
  dlong areg, i;
  dlong *p1,*p2,*p3;
  
  areg = *(areg_type*)(arg_place(locpc,1,builtin_low_get_open_stream_3));
  p1 = (dlong *)args(areg);
  areg = *(areg_type*)(arg_place(locpc,2,builtin_low_get_open_stream_3));
  p2 = (dlong *)args(areg); deref(p2);
  areg = *(areg_type*)(arg_place(locpc,3,builtin_low_get_open_stream_3));
  p3 = (dlong *)args(areg); deref(p3);
  
  locpc += builtin_low_get_open_stream_3_len;
  if ((i = low_get_open_stream3(p1,p2,p3,&locmach)) == -1)
    goto_fail_label;
  trail(p2,bh,loctrreg);
  trail(p3,bh,loctrreg);
  build_smallint(p2,i);
  build_smallint(p3,i); /* seems like redundant, but streams are perhaps
			   not integers :-) */
  goto_more_exec;
}

hcase(builtin_low_close_stream_1)
builtin_low_close_stream_1_label:
profile(builtin_low_close_stream_1);
{
  dlong *p1;

  p1 = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_low_close_stream_1)));
  locpc += builtin_low_close_stream_1_len;
  if (! low_close_stream1(p1,&locmach))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_delete_file_1)
builtin_delete_file_1_label:
profile(builtin_delete_file_1);
{
  dlong i, *p;

  p = (dlong *)(args(*(areg_type*)(arg_place(locpc,1,builtin_delete_file_1))));
  deref(p);
  locpc += builtin_delete_file_1_len;
  if (! is_atom(p))
    goto error_delete_file_1;
  i = get_atom_index(p);
  if (!remove(symtab[i].name))
    goto_more_exec;
  goto error_delete_file_1;
}

hcase(builtin_get0_1)
builtin_get0_1_label:
profile_builtin(builtin_get0_1);
{
  dlong *p, i;
  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_get0_1)));
  locpc += builtin_get0_1_len;
  deref(p);
  i = low_get0_1(current_in_nr, &locmach);

  if (is_ref(p))
    {
      trail(p,bh,loctrreg);
      build_smallint(p,i);
      goto_more_exec;
    }

  if (! is_attributed(p))
    goto error_get0_1;

  safe_wakeup(p,(dlong *)(make_smallint(i)),&locmach);

  goto_more_exec;
}

hcase(builtin_current_eof_0)
builtin_current_eof_0_label:
profile(builtin_current_eof_0);
{
  if (! low_current_eof0(current_in_nr, &locmach))
    goto_fail_label;
  locpc += builtin_current_eof_0_len;
  goto_more_exec;
}

hcase(builtin_get0_stream_2)
builtin_get0_stream_2_label:
profile_builtin(builtin_get0_stream_2);
{
  dlong *p1, *p2, i;
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_get0_stream_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_get0_stream_2)));
  deref(p1);
  locpc += builtin_get0_stream_2_len;
  deref(p2);
  i = low_get0_stream_2(p1, &locmach);

  if (i == -2)
    goto error_get0_stream_2;

  if (is_ref(p2))
    {
      trail(p2,bh,loctrreg);
      build_smallint(p2,i);
      goto_more_exec;
    }

  if (! is_attributed(p2))
    goto error_get0_stream_2;

  safe_wakeup(p2,(dlong *)(make_smallint(i)),&locmach);

  goto_more_exec;
}

hcase(builtin_see_1)
builtin_see_1_label:
profile_builtin(builtin_see_1);
{
  dlong *p;

  /* see/1 can succeed or err */
  /* err when argument has bad type or cannot be seen */

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_see_1)));
  locpc += builtin_see_1_len;
  if (low_see_1(p,&locmach))
    goto_more_exec;
  goto error_see_1;
}

hcase(builtin_get_line_number_1)
builtin_get_line_number_1_label:
profile_builtin(builtin_get_line_number_1);
{
  dlong areg1;
  dlong *p;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_get_line_number_1));
  p = (dlong *)(args(areg1));
  deref(p);
  locpc += builtin_get_line_number_1_len;
  if (! is_ref(p))
    goto error_get_line_number_1;
  trail(p,bh,loctrreg);
  build_smallint(p,locmach.openfiles[current_in_nr].line_number);
  goto_more_exec;
}

hcase(builtin_ungetichar_1)
builtin_ungetichar_1_label:
profile(builtin_ungetichar_1);
{
  dlong *p;
  dlong i;

  p =(dlong *)(args( *(areg_type*)(arg_place(locpc,1,builtin_ungetichar_1))));
  deref(p);
  if (! is_smallint(p)) goto_more_exec;
  i = get_smallint(p);
  locpc += builtin_ungetichar_1_len;

  if (locmach.openfiles[current_in_nr].ungetnr == 0)
    {
      locmach.openfiles[current_in_nr].ungetnr = 1;
      locmach.openfiles[current_in_nr].c1 = i;
    }
  else
  if (locmach.openfiles[current_in_nr].ungetnr == 1)
    {
      locmach.openfiles[current_in_nr].ungetnr = 2;
      locmach.openfiles[current_in_nr].c2 = locmach.openfiles[current_in_nr].c1;
      locmach.openfiles[current_in_nr].c1 = i;
    }
  goto_more_exec;
}

hcase(builtin_seen_0)
builtin_seen_0_label:
profile_builtin(builtin_seen_0);
{
  locpc += builtin_seen_0_len;
  low_seen_0(&locmach);
  goto_more_exec;
}

hcase(builtin_seeing_2)
builtin_seeing_2_label:
profile_builtin(builtin_seeing_2);
{
  dlong symbol, i;
  dlong *p1, *p2;
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_seeing_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_seeing_2)));
  locpc += builtin_seeing_2_len;
  deref2(p1, goto arg1_is_ref_seeing_2);
  goto error_seeing_2;
 arg1_is_ref_seeing_2:
  deref2(p2, goto arg2_is_ref_seeing_2);
  goto error_seeing_2;
 arg2_is_ref_seeing_2:
  i = current_in_nr;
  symbol = make_atom(locmach.openfiles[i].f_symbol);
  trail(p1,bh,loctrreg);
  *p1 = symbol;
  trail(p2,bh,loctrreg);
  build_smallint(p2,i);
  goto_more_exec;
}

hcase(builtin_tell_1)
builtin_tell_1_label:
profile_builtin(builtin_tell_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_tell_1)));
  locpc += builtin_tell_1_len;
  if (low_tell_1(p,&locmach, "w"))
    goto_more_exec;
  goto error_tell_1;
}

hcase(builtin_append_1)
builtin_append_1_label:
profile_builtin(builtin_append_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_append_1)));
  locpc += builtin_append_1_len;
  if (low_tell_1(p,&locmach, "a"))
    goto_more_exec;
  goto error_append_1;
}

hcase(builtin_telling_2)
builtin_telling_2_label:
profile_builtin(builtin_telling_2);
{
  dlong symbol, i;
  dlong *p1, *p2;
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_telling_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_telling_2)));
  locpc += builtin_telling_2_len;
  deref2(p1, goto arg1_is_ref_telling_2);
  goto error_telling_2;
 arg1_is_ref_telling_2:
  deref2(p2, goto arg2_is_ref_telling_2);
  goto error_telling_2;
 arg2_is_ref_telling_2:
  i = current_out_nr;
  symbol = make_atom(locmach.openfiles[i].f_symbol);
  trail(p1,bh,loctrreg);
  *p1 = symbol;
  trail(p2,bh,loctrreg);
  build_smallint(p2,i);
  goto_more_exec;
}

hcase(builtin_told_0)
builtin_told_0_label:
profile_builtin(builtin_told_0);
{
  locpc += builtin_told_0_len;
  low_told_0(&locmach);
  goto_more_exec;
}

hcase(builtin_close_1)
builtin_close_1_label:
profile_builtin(builtin_close_1);
{
  dlong *p;
  dlong symbol, i;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_close_1)));
  locpc += builtin_close_1_len;
  if (low_close1(p,&locmach))
    goto_more_exec;
  goto error_close_1;
}

hcase(builtin_htoggles_2)
builtin_htoggles_2_label:
profile_builtin(builtin_htoggles_2);
{
  dlong areg1, areg2, res;
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_htoggles_2));
  areg2 = *(areg_type *)(arg_place(locpc,2,builtin_htoggles_2));
	
  res = htoggles_2_builtin(areg1,areg2,&locmach);
  if (res) 
    {
      locpc += builtin_htoggles_2_len;
      goto_more_exec;
    }
  goto_fail_label;
}

hcase(builtin_internalstat_5)
builtin_internalstat_5_label:
profile_builtin(builtin_internalstat_5);
{
  /* moved to copyterm.c - not the best place, but better than here */

  if (! builtin_internalstat(*(areg_type *)(arg_place(locpc,1,builtin_internalstat_5)),
			     *(areg_type *)(arg_place(locpc,2,builtin_internalstat_5)),
			     *(areg_type *)(arg_place(locpc,3,builtin_internalstat_5)),
			     *(areg_type *)(arg_place(locpc,4,builtin_internalstat_5)),
			     *(areg_type *)(arg_place(locpc,5,builtin_internalstat_5)),
			     &locmach))
    goto_fail_label;
  locpc += builtin_internalstat_5_len;
  goto_more_exec;
}

hcase(builtin_copy_term_2)
builtin_copy_term_2_label:
profile_builtin(builtin_copy_term_2);
{
  dlong *t, *p2;

 redo_copy_term:
  t = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_copy_term_2)));
  deref(t);
  if (!is_atomic(t))
    {
      t = builtin_copy_term(t,&locmach);
      if (! t)
	{
	  dlong spare_heap;

	  spare_heap = (locmach.end_heap - lochreg) << 1; /* twice the current free space */
	  macro_deal_with_heap_overflow(2,spare_heap);
	  goto redo_copy_term;
	}
    }
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_copy_term_2)));
  locpc += builtin_copy_term_2_len;
  deref2(p2,goto copy_term_2_ref);

  if (!unify_terms(t,p2,&locmach))
    goto_fail_label;
  goto_more_exec;

 copy_term_2_ref:
  trail(p2,bh,loctrreg);
  *p2 = (dlong)t;
  goto_more_exec;
}

hcase(builtin_copy_term_nat_2)
builtin_copy_term_nat_2_label:
profile_builtin(builtin_copy_term_nat_2);
{
  dlong *t, *p2;

 redo_copy_term_nat:
  t = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_copy_term_nat_2)));
  deref(t);
  if (!is_atomic(t))
    {
      t = builtin_copy_term_nat(t,&locmach);
      if (! t)
	{
	  dlong spare_heap;

	  spare_heap = (locmach.end_heap - lochreg) << 1; /* twice the current free space */
	  macro_deal_with_heap_overflow(2,spare_heap);
	  goto redo_copy_term_nat;
	}
    }
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_copy_term_nat_2)));
  locpc += builtin_copy_term_nat_2_len;
  deref2(p2,goto copy_term_nat_2_ref);

  if (!unify_terms(t,p2,&locmach))
    goto_fail_label;
  goto_more_exec;

 copy_term_nat_2_ref:
  trail(p2,bh,loctrreg);
  *p2 = (dlong)t;
  goto_more_exec;
}

hcase(builtin_back_trace_0)
builtin_back_trace_0_label:
profile(builtin_back_trace_0);
{
  back_trace(&locmach);
  locpc += builtin_back_trace_0_len;
  goto_more_exec;
}


hcase(builtin_ancestors_1)
builtin_ancestors_1_label:
profile(builtin_ancestors_1);
{
  dlong *p1;
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_ancestors_1)));
  deref2(p1, goto is_var_builtin_ancestors_1);
  goto_fail_label;

 is_var_builtin_ancestors_1:
  trail(p1,bh,loctrreg);
  *p1 = ancestors(&locmach);
  locpc += builtin_ancestors_1_len;
  goto_more_exec;
}


hcase(builtin_b_getval_2)
builtin_b_getval_2_label:
profile_builtin(builtin_b_getval_2);
{
  dlong *p1, *p2;
  dlong i;
  /* code is identical to nb_getval */

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_b_getval_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_b_getval_2)));
  locpc += builtin_b_getval_2_len;
  deref(p1);
  if (! is_atom(p1)) goto_fail_label;
  i = get_atom_index(p1);

  deref2(p2,goto b_getval_2_ref);

  if (! is_attributed(p2))
    {
      p1 = (dlong *)(symtab[i].glob_val);
      deref(p1); underef(p1);
      if (!unify_terms(p1,p2,&locmach))
	goto_fail_label;
      goto_more_exec;
    }

  safe_wakeup(p2,(dlong *)(symtab[i].glob_val),&locmach);
  goto_more_exec;

 b_getval_2_ref:
  trail(p2,bh,loctrreg);
  p1 = (dlong *)(symtab[i].glob_val);
  deref(p1); underef(p1);
  *p2 = (dlong)p1;
  goto_more_exec;
}

hcase(builtin_b_getval_spec_2)
builtin_b_getval_spec_2_label:
hcase(builtin_nb_getval_spec_2)
builtin_nb_getval_spec_2_label:
profile_builtin(builtin_b_getval_spec_2);
{
  dlong *p1;
  dlong areg2;
  dlong i;

  /* code is identical to nb_getval_spec */

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_b_getval_spec_2)));
  areg2 = *(areg_type *)(arg_place(locpc,2,builtin_b_getval_spec_2));
  locpc += builtin_b_getval_spec_2_len;
  deref(p1);
  if (! is_atom(p1)) goto_fail_label;
  i = get_atom_index(p1);

 b_getval_spec_2_ref:
  p1 = (dlong *)(symtab[i].glob_val);
  deref(p1); underef(p1);
  args(areg2) = (dlong)p1;
  goto_more_exec;
}

hcase(builtin_b_setval_2)
builtin_b_setval_2_label:
profile_builtin(builtin_b_setval_2);
{
  dlong *p1, *p2;
  dlong i;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_b_setval_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_b_setval_2)));
  locpc += builtin_b_setval_2_len;
  deref(p1);
  deref(p2); underef(p2);
  if (! is_atom(p1)) goto_fail_label;
  i = get_atom_index(p1);
  p1 = (dlong *)(&(symtab[i].glob_val));
  if (is_atom(*p1))
    {
      /* it is the first time that the variable is assigned a value to */
      /* we will put a reference to the new value which will be on the heap */
      /* this is the only place where value trailing of the thing is ok */
      /* but it is unconditional ... */
      unconditional_value_trail(p1,loctrreg);
      *p1 = (dlong)lochreg;
      *lochreg = (dlong)p2;
      lochreg++;
      goto_more_exec;
    }

  if (! is_ref(p1))
    fprintf(stderr,"unexpected non ref in b_setval - please inform Bart Demoen\n");

  p1 = (dlong *)(*p1);
  noye_trail(p1,bh,loctrreg,p2);
  goto_more_exec;
}


hcase(builtin_nb_getval_2)
builtin_nb_getval_2_label:
profile_builtin(builtin_nb_getval_2);
{
  dlong *p1, *p2;
  dlong i;
  /* code is identical to b_getval */

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nb_getval_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_nb_getval_2)));
  locpc += builtin_nb_getval_2_len;
  deref(p1);
  if (! is_atom(p1)) goto_fail_label;
  i = get_atom_index(p1);

  deref2(p2,goto nb_getval_2_ref);

  if (! is_attributed(p2))
    {
      p1 = (dlong *)(symtab[i].glob_val);
      deref(p1); underef(p1);
      if (!unify_terms(p1,p2,&locmach))
	goto_fail_label;
      goto_more_exec;
    }

  safe_wakeup(p2,(dlong *)(symtab[i].glob_val),&locmach);
  goto_more_exec;

 nb_getval_2_ref:
  trail(p2,bh,loctrreg);
  p1 = symtab[i].glob_val;
  deref(p1); underef(p1);
  *p2 = (dlong)(p1);
  goto_more_exec;
}

hcase(builtin_nb_setval_2)
builtin_nb_setval_2_label:
profile_builtin(builtin_nb_setval_2);
{
  dlong *p1, *p2;
  dlong i;

  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nb_setval_2)));
  p2 = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_nb_setval_2)));
  locpc += builtin_nb_setval_2_len;
  deref(p1);
  deref(p2);
  if (! is_atom(p1)) goto_fail_label;
  i = get_atom_index(p1);
  p1 = (dlong *)(&(symtab[i].glob_val));
  if (has_atom_tag(p2)) /* smallint or char or atom */
    *p1 = (dlong)(p2);
  else
    {
      underef(p2);
      *p1 = (dlong)lochreg;
      *lochreg = (dlong)p2;
      lochreg++;
      adapt_freeze_hreg(lochreg);
    }
  goto_more_exec;
}

hcase(builtin_exclude_from_domain_2)
builtin_exclude_from_domain_2_label:
profile(builtin_exclude_from_domain_2);
{
  dlong *p1, *p2, *f, *l, *newl;

  locpc += builtin_exclude_from_domain_2_len;
  p2 = (dlong *)args(2); deref(p2);
  p1 = (dlong *)args(3); deref(p1);

 exclude_loop:

  if (! is_attributed(p1))
    {
      if (p1 == p2) goto_fail_label;
      goto do_one_more;
    }

  f = get_attvar_pointer(p1);
  f++; deref(f);
  f = l = get_struct_pointer(f) + 2;
  deref(l);

  newl = delfromdomain(l,p2);

  if (0 != newl)
    {
      if (is_smallint(newl))
	{
	  // arg 1 and 2 are in the correct place allready
	  // we add arg 3 so that we can execute exclude_list_trigger/3
	  // arg 3 is the woken goal

	  locpc = main_entrypoint(exclude_list_trigger_3);
	  p1 = get_attvar_pointer(p1);
	  value_trail(p1,bh,trreg);
	  *p1 = (dlong)newl;
	  p1++;
	  deref(p1);
	  p1 = get_struct_pointer(p1) + 1;
	  args(3) = *p1;
	  goto_more_exec;

	}
      if ((dlong)newl == nil_atom) {goto_fail_label}
      noye_trail(f,bh,loctrreg,newl);
    }

 do_one_more:
  {
    dlong *p3;
    p3 = (dlong *)args(1); deref(p3);
    if (! is_list(p3)) goto_more_exec;
    p3 = get_list_pointer(p3);
    p1 = (dlong *)(p3[0]); deref(p1);
    args(1) = p3[1];
    goto exclude_loop;
  }
}




hcase(builtin_add_module_3)  /* fatal - does not support attributed vars */
builtin_add_module_3_label:
profile_builtin(builtin_add_module_3);
{
  dlong areg1, areg2, areg3;
  dlong *p1, *p2, *p3, *q;
  dlong symbol1, symbol2, symbol3;

  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_add_module_3));
  areg2 = *(areg_type *)(arg_place(locpc,2,builtin_add_module_3));
  areg3 = *(areg_type *)(arg_place(locpc,3,builtin_add_module_3));

  p1 = (dlong *)args(areg1); deref(p1);
  p2 = (dlong *)args(areg2); deref(p2);
  p3 = (dlong *)args(areg3); deref(p3);
	      
  locpc += builtin_add_module_3_len;

  if (is_ref(p1))
  {
    if (! is_atom(p2))
      goto error_add_module_3;

    symbol2 = get_atom_index(p2);

    if (symtab[symbol2].module != USER_MOD) goto_fail_label;
    if (is_atom(p3))
      {
	symbol3 = get_atom_index(p3);
	if (symtab[symbol3].module != USER_MOD) goto_fail_label;
	trail(p1,bh,loctrreg);
	*p1 = make_atom(sym_lookup(0,symtab[symbol3].name,symbol2));
	goto_more_exec;
      }

    if (! is_struct(p3)) goto error_add_module_3;

    {
      dlong arity;
      p3 = get_struct_pointer(p3);
      symbol3 = get_funct_symb(*p3);
      arity = get_funct_arity(*p3);
      if (symtab[symbol3].module != USER_MOD) goto_fail_label;
      trail(p1,bh,loctrreg);
      *p1 = make_struct_p(lochreg);
      *lochreg = make_funct(sym_lookup(arity,symtab[symbol3].name,symbol2));  
      lochreg++;
      while (arity--)
	{
	  p3++;
	  *lochreg = *p3;
	  lochreg++;
	}
      goto_more_exec;
    }
  }

  if (is_atom(p1))
  {
    symbol1 = get_atom_index(p1);
    q = (dlong *)module_from_index(symbol1);
    if (is_ref(p2))
      {
	trail(p2,bh,loctrreg);
	*p2 = (dlong)q;
      }
    else if (p2 != q) goto_fail_label;
    q = (dlong *)make_atom(sym_lookup(0,symtab[symbol1].name,USER_MOD));
    if (is_ref(p3))
      {
	trail(p3,bh,loctrreg);
	*p3 = (dlong)q;
      }
    else
      if (p3 != q) goto_fail_label;
    goto_more_exec;
  }

  if (is_struct(p1))
    {
      dlong arity;
      p1 = get_struct_pointer(p1);
      symbol1 = get_funct_symb(*p1);
      q = (dlong *)module_from_index(symbol1);
      if (is_ref(p2))
	{
	  trail(p2,bh,loctrreg);
	  *p2 = (dlong)q;
	}
      else if (p2 != q) goto_fail_label;
      arity = get_funct_arity(*p1);
      if (is_ref(p3))
	{
	  trail(p3,bh,loctrreg);
	  *p3 = make_struct_p(lochreg);
	  *lochreg = make_funct(sym_lookup(arity,symtab[symbol1].name,USER_MOD));  
	  lochreg++;
	  while (arity--)
	    {
	      p1++;
	      *lochreg = *p1;
	      lochreg++;
	    }
	}
      else
	{
	  if (! is_struct(p3)) goto error_add_module_3;
	  p3 = get_struct_pointer(p3);
	  if (*p3 != make_funct(sym_lookup(arity,symtab[symbol1].name,USER_MOD)))
	    goto_fail_label;
	  while (arity--)
	    {
	      p1++;p3++;
	      if (!unify_terms(p1, p3, &locmach))
		goto error_add_module_3;
	    }
	}
      goto_more_exec;
    }
  goto error_add_module_3;
}

hcase(builtin_ground_1)
builtin_ground_1_label:
profile_builtin(builtin_ground_1);
{
  dlong areg1;
  dlong *p1;
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_ground_1));
  locpc += builtin_ground_1_len;
  p1 = (dlong *)args(areg1);
  deref(p1);
  if (is_atomic(p1)) goto_more_exec;
  if (is_ground(p1, &locmach)) goto_more_exec;
  goto_fail_label;
}

hcase(ifequal_int_goto)
ifequal_int_goto_label:
profile_builtin(ifequal_int_goto);
{
  dlong areg1;
  dlong *p;
  dlong dlong3;

  /*  areg1 = *(areg_type *)(arg_place(locpc,1,ifequal_int_goto));
  p = (dlong *)args(areg1);
  while (is_ref(p))
  p = (dlong *)*p; */ p = bridge;
  dlong3 = *(dlong *)(arg_place(locpc,3,ifequal_int_goto));
  if (is_smallint(p) && (dlong3 == get_smallint(p)))
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,ifequal_int_goto)));
      if (! locpc) goto_fail_label;
      goto_more_exec;
    }

  locpc += ifequal_int_goto_len;
  goto_more_exec;
}

hcase(ifequal_char_goto)
ifequal_char_goto_label:
profile_builtin(ifequal_char_goto);
{
  dlong areg1;
  dlong *p;
  char dchar2;

  /*  areg1 = *(areg_type *)(arg_place(locpc,1,ifequal_char_goto));
  p = (dlong *)args(areg1);
  while (is_ref(p))
  p = (dlong *)*p; */ p = bridge;
  dchar2 = *(char *)(arg_place(locpc,2,ifequal_char_goto));
  if (is_char(p) && (dchar2 == get_character(p)))
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,3,ifequal_char_goto)));
      if (! locpc) goto_fail_label;
      goto_more_exec;
    }

  locpc += ifequal_char_goto_len;
  goto_more_exec;
}

hcase(ifequal_string_goto)
ifequal_string_goto_label:
profile_builtin(ifequal_string_goto);
{
  dlong areg1;
  dlong *p;
  dlong plen, strlen3;
  char *dstring4;

  /*  areg1 = *(areg_type *)(arg_place(locpc,1,ifequal_string_goto));
  p = (dlong *)args(areg1);
  while (is_ref(p))
  p = (dlong *)*p; */ p = bridge;
  strlen3 = *(str_len *)(arg_place(locpc,3,ifequal_string_goto));
  dstring4 = (char *)(arg_place(locpc,4,ifequal_string_goto));
  if (is_string(p) && (get_string_length(p) == strlen3) &&
      equal_strings(strlen3, dstring4, get_string_val(p)))
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,ifequal_string_goto)));
      if (! locpc) goto_fail_label;
      goto_more_exec;
    }

  locpc += extra_strlen_len_ifequal_string_goto( locpc);
  locpc += ifequal_string_goto_len;
  goto_more_exec;
}

hcase(ifequal_atom_struct_goto)
ifequal_atom_struct_goto_label:
profile_builtin(ifequal_atom_struct_goto);
{
  dlong areg1;
  dlong *p;
  dlong dlong3;

  /*  areg1 = *(areg_type *)(arg_place(locpc,1,ifequal_atom_struct_goto));
  p = (dlong *)args(areg1);
  while (is_ref(p))
  p = (dlong *)*p; */ p = bridge;
  dlong3 = *(dlong *)(arg_place(locpc,3,ifequal_atom_struct_goto));
  if ((is_atom(p) && (dlong3 == (dlong)p))
      || (is_struct(p) && (dlong3 == *(get_struct_pointer(p)))))
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,ifequal_atom_struct_goto)));
      if (! locpc) goto_fail_label;
      goto_more_exec;
    }

  locpc += ifequal_atom_struct_goto_len;
  goto_more_exec;
}

hcase(ifequal_float_goto)
ifequal_float_goto_label:
profile_builtin(ifequal_float_goto);
{
  dlong areg1;
  dlong *p;
  dfloat f3;

  /*  areg1 = *(areg_type *)(arg_place(locpc,1,ifequal_float_goto));
  p = (dlong *)args(areg1);
  while (is_ref(p))
  p = (dlong *)*p; */ p = bridge;
  get_double_arg(f3, arg_place(locpc,3,ifequal_float_goto));
  if (is_real(p) && (f3 == get_real(p)))
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,ifequal_float_goto)));
      if (! locpc) goto_fail_label;
      goto_more_exec;
    }

  locpc += ifequal_float_goto_len;
  goto_more_exec;
}

hcase(ifequal_list_goto)
ifequal_list_goto_label:
profile_builtin(ifequal_list_goto);
{
  dlong areg1;
  dlong *p;
  
  /*   areg1 = *(areg_type *)(arg_place(locpc,1,ifequal_list_goto));
  p = (dlong *)args(areg1);
  while (is_ref(p))
  p = (dlong *)*p; */ p = bridge;
  if (is_list(p))
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,ifequal_list_goto)));
      if (! locpc) goto_fail_label;
      goto_more_exec;
    }

  locpc += ifequal_list_goto_len;
  goto_more_exec;
}

hcase(bld_float)
bld_float_label:
profile(bld_float);
{
  dfloat f1;

  get_double_arg(f1, arg_place(locpc,1,bld_float));
  locpc += bld_float_len;
  push_real(lochreg,f1);
  goto_more_exec;
}


hcase(build_int)
build_int_label:
profile(build_int);
{
  dlong dlong1;

  dlong1 = *(dlong *)(arg_place(locpc,1,build_int));
  locpc += build_int_len;
  *lochreg = dlong1;
  lochreg++;
  goto_more_exec;
}

hcase(test_var)
test_var_label:
profile_builtin(test_var);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_var));
  p = (dlong *)args(areg1);
  deref(p);

  if (is_ref(p) || is_attributed(p))
    locpc += test_var_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_var)));
  goto_more_exec;
}

hcase(test_var_p)
test_var_p_label:
profile_builtin(test_var_p);
{
  dlong areg1, yvar2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_var));

  yvar2 = *(yvar_type *)(arg_place(locpc,2,test_var_p));
  p = (dlong *)ereg[yvar2];
  args(areg1) = (dlong)p;
  deref(p);

  if (is_ref(p) || is_attributed(p))
    locpc += test_var_p_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,3,test_var_p)));
  goto_more_exec;
}

hcase(get_int_test)
hcase(get_atom_test)
get_int_test_label:
get_atom_test_label:

profile_builtin(get_int_test);
{
  dlong areg1, int_at;
  dlong *p1;

  int_at = *(dlong *)(arg_place(locpc,2,get_int_test));
  areg1 = *(areg_type *)(arg_place(locpc,1,get_int_test));
  p1 = (dlong *)args(areg1);
  deref2(p1, goto case_ref_get_int_test);

  if ((dlong)p1 == int_at)
    {
      locpc += get_int_test_len;
      goto_more_exec;
    }

  if (! is_attributed(p1))
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,3,get_int_test)));
      goto_more_exec;
    }

  safe_wakeup(p1,(dlong *)int_at,&locmach);
  locpc += get_int_test_len;
  goto_more_exec;

 case_ref_get_int_test:
  locpc += get_int_test_len;
  trail(p1,bh,loctrreg);
  *p1 = int_at;
  goto_more_exec;

}

hcase(get_intv_test)
hcase(get_atomv_test)
get_intv_test_label:
get_atomv_test_label:

profile_builtin(get_intv_test);
{
  dlong yvar1, int_at;
  dlong *p1;

  int_at = *(dlong *)(arg_place(locpc,2,get_intv_test));

  yvar1 = *(yvar_type*)(arg_place(locpc,1,get_intv_test));
  p1 = (dlong *)ereg[yvar1];
  deref2(p1, goto case_ref_get_intv_test);

  if ((dlong)p1 == int_at)
    {
      locpc += get_intv_test_len;
      goto_more_exec;
    }

  if (! is_attributed(p1))
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,3,get_intv_test)));
      goto_more_exec;
    }

  safe_wakeup(p1,(dlong *)int_at,&locmach);
  locpc += get_intv_test_len;
      printf("attr\n");
  goto_more_exec;

 case_ref_get_intv_test:
  locpc += get_intv_test_len;
  trail(p1,bh,loctrreg);
  *p1 = int_at;
  goto_more_exec;

}

hcase(test_attvar)
test_attvar_label:
profile_builtin(test_attvar);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_attvar));
  p = (dlong *)args(areg1);
  deref(p);

  if (is_attributed(p))
    locpc += test_attvar_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_attvar)));
  goto_more_exec;
}

hcase(builtin_cyclic_list_1)
builtin_cyclic_list_1_label:
profile_builtin(builtin_cyclic_list_1);
{
  dlong areg1;
  dlong *p;
  dlong max;

  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_cyclic_list_1));
  locpc += builtin_cyclic_list_1_len;
  p = (dlong *)args(areg1);
  deref(p);

  max = locmach.H - locmach.begin_heap;
  while (max--)
    {
      if (! is_list(p)) goto_fail_label;
      p = get_list_pointer(p);
      p++;
      deref(p);
    }

  goto_more_exec;
}

hcase(test_atom)
test_atom_label:
profile_builtin(test_atom);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_atom));
  p = (dlong *)args(areg1);
  deref(p);

  if (is_atom(p))
    locpc += test_atom_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_atom)));
  goto_more_exec;
}

hcase(test_atomic)
test_atomic_label:
profile_builtin(test_atomic);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_atomic));
  p = (dlong *)args(areg1);
  deref(p);

  if (is_atomic(p))
    locpc += test_atomic_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_atomic)));
  goto_more_exec;
}

hcase(test_number)
test_number_label:
profile_builtin(test_number);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_number));
  p = (dlong *)args(areg1);
  deref(p);

  if (is_number(p))
    locpc += test_number_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_number)));
  goto_more_exec;
}

hcase(test_integer)
test_integer_label:
profile_builtin(test_integer);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_integer));
  p = (dlong *)args(areg1);
  deref(p);

  if (is_smallint(p) || is_bigint(p))
    locpc += test_integer_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_integer)));
  goto_more_exec;
}

hcase(test_integer_p)
test_integer_p_label:
profile_builtin(test_integer_p);
{
  dlong areg1, yvar2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_integer));

  yvar2 = *(yvar_type *)(arg_place(locpc,2,test_integer_p));
  p = (dlong *)ereg[yvar2];
  args(areg1) = (dlong)p;
  deref(p);

  if (is_smallint(p) || is_bigint(p))
    locpc += test_integer_p_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,3,test_integer_p)));
  goto_more_exec;
}

hcase(test_compound)
test_compound_label:
profile_builtin(test_compound);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_compound));
  p = (dlong *)args(areg1);
  deref(p);

  if (is_compound(p))
    locpc += test_compound_len;
  else
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_compound)));
  goto_more_exec;
}

hcase(test_nonvar)
test_nonvar_label:
profile_builtin(test_nonvar);
{
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_nonvar));
  p = (dlong *)args(areg1);
  deref(p);

  if (is_ref(p) || is_attributed(p))
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_nonvar)));
  else
    locpc += test_nonvar_len;
  goto_more_exec;
}

hcase(test_nonvar_p)
test_nonvar_p_label:
profile_builtin(test_nonvar_p);
{
  dlong areg1, yvar2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_nonvar));

  yvar2 = *(yvar_type *)(arg_place(locpc,2,test_nonvar_p));
  p = (dlong *)ereg[yvar2];
  args(areg1) = (dlong)p;
  deref(p);

  if (is_ref(p) || is_attributed(p))
    locpc = (codep)(*(dlong *)(arg_place(locpc,3,test_nonvar_p)));
  else
    locpc += test_nonvar_p_len;
  goto_more_exec;
}

hcase(test_term_not_unify)
test_term_not_unify_label:
profile_builtin(test_term_not_unify);
{
  dlong areg1;
  dlong areg2;
  dlong result;
  dlong *backup_BH;  /* realloc cp cannot occur ? */
  dlong backup_trail;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_term_not_unify));
  areg2 = *(areg_type *)(arg_place(locpc,2,test_term_not_unify));
	
  backup_BH = bh;
  bh = lochreg;
  backup_trail = loctrreg - locmach.begin_trail;
  result = unify_terms((dlong *)args(areg1),(dlong *)args(areg2),&locmach);
  bh = backup_BH;
  { 
    dlong *totr = locmach.begin_trail + backup_trail;
    untrail(loctrreg, totr);
  }
  if (result) 
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,3,test_term_not_unify)));
      if (! locpc) goto_fail_label;
    }
  else
    locpc += test_term_not_unify_len;
  goto_more_exec;
}

hcase(builtin_term_hash_2)
builtin_term_hash_2_label:
profile_builtin(builtin_term_hash_2);
{
  dlong *p1;
  dlong *p2;
  dlong h;
  
  p1 = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_term_hash_2)));
  p2 = (dlong *)args(*(areg_type*)(arg_place(locpc,2,builtin_term_hash_2)));
  deref(p2);
  if (! is_ref(p2)) goto_fail_label;

  locpc += builtin_term_hash_2_len;

  h = term_hash_builtin(p1,&locmach);

  trail(p2,bh,loctrreg);
  *p2 = make_smallint(h);
  goto_more_exec;
}


hcase(builtin_compare_3)
builtin_compare_3_label:
profile_builtin(builtin_compare_3);
{
  dlong areg1,areg2,areg3,result;
  dlong *p;

  areg2 = *(areg_type *)(arg_place(locpc,2,builtin_compare_3));
  areg3 = *(areg_type *)(arg_place(locpc,3,builtin_compare_3));
  result = compare_terms((dlong *)args(areg2),(dlong *)args(areg3), &locmach);
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_compare_3));
  locpc += builtin_compare_3_len;
  if (result < 0)
    result = atom_at_smaller;
  else
    if (result > 0)
      result = atom_at_bigger;
  else
    result = atom_at_equal;

  p = (dlong *)args(areg1);
  deref(p);
  if (is_ref(p))
    {
      trail(p,bh,loctrreg);
      *p = result;
      goto_more_exec;
    }

  if (!unify_terms(p,(dlong *)result,&locmach))
    goto_fail_label;

  goto_more_exec;
}

hcase(builtin_compare_spec_3)
builtin_compare_spec_3_label:
profile_builtin(builtin_compare_spec_3);
{
  dlong areg1,areg2,areg3,result;

  areg2 = *(areg_type *)(arg_place(locpc,2,builtin_compare_spec_3));
  areg3 = *(areg_type *)(arg_place(locpc,3,builtin_compare_spec_3));
  result = compare_terms((dlong *)args(areg2),(dlong *)args(areg3), &locmach);
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_compare_spec_3));
  locpc += builtin_compare_spec_3_len;
  if (result < 0)
    result = atom_at_smaller;
  else
    if (result > 0)
      result = atom_at_bigger;
  else
    result = atom_at_equal;

  args(areg1) = result;
  goto_more_exec;
}


hcase(test_term_equal)
test_term_equal_label:
profile_builtin(test_term_equal);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong result;
  dlong *p, *q;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_term_equal));
  areg2 = *(areg_type *)(arg_place(locpc,2,test_term_equal));
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_term_equal)));
  locpc += test_term_equal_len;

  p = (dlong *)args(areg1); q = (dlong *)args(areg2);

  deref(p); deref(q);
  if (is_simple(p) || is_simple(q))
    result = (p != q);
  else
    result = compare_terms(p,q, &locmach);
  if (result == 0) goto_more_exec;
  locpc = label3;
  if (! locpc) goto_fail_label;
  goto_more_exec;
}

hcase(test_term_equal_integer)
test_term_equal_integer_label:
hcase(test_term_equal_atom)
test_term_equal_atom_label:
profile_builtin(test_term_equal_atom);
{
  dlong areg1;
  dlong atom2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_term_equal_atom));
  p = (dlong *)args(areg1);
  atom2 = *(dlong *)(arg_place(locpc,2,test_term_equal_atom));
  deref(p);
  if ((dlong)p != atom2) 
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,3,test_term_equal_atom)));
      if (locpc) goto_more_exec;
      goto_fail_label;
    }
  locpc += test_term_equal_atom_len;
  goto_more_exec;
}

hcase(test_term_not_equal)
test_term_not_equal_label:
profile_builtin(test_term_not_equal);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong result;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_term_not_equal));
  areg2 = *(areg_type *)(arg_place(locpc,2,test_term_not_equal));
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_term_not_equal)));
  locpc += test_term_not_equal_len;

  result = compare_terms((dlong *)args(areg1),(dlong *)args(areg2), &locmach);
  if (result != 0) goto_more_exec;
  locpc = label3;
  if (! locpc) goto_fail_label;
  goto_more_exec;
}

hcase(test_term_smaller_or_equal)
test_term_smaller_or_equal_label:
profile_builtin(test_term_smaller_or_equal);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong result;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_term_smaller_or_equal));
  areg2 = *(areg_type *)(arg_place(locpc,2,test_term_smaller_or_equal));
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_term_smaller_or_equal)));
  locpc += test_term_smaller_or_equal_len;

  result = compare_terms((dlong *)args(areg1),(dlong *)args(areg2), &locmach);
  if (result <= 0) goto_more_exec;
  locpc = label3;
  if (! locpc) goto_fail_label;
  goto_more_exec;
}

hcase(test_term_smaller)
test_term_smaller_label:
profile_builtin(test_term_smaller);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong result;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_term_smaller));
  areg2 = *(areg_type *)(arg_place(locpc,2,test_term_smaller));
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_term_smaller)));
  locpc += test_term_smaller_len;

  result = compare_terms((dlong *)args(areg1),(dlong *)args(areg2), &locmach);
  if (result < 0) goto_more_exec;
  locpc = label3;
  if (! locpc) goto_fail_label;
  goto_more_exec;
}

hcase(test_equal)
test_equal_label:
profile_builtin(test_equal);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong *p1, *p2;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_equal));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type *)(arg_place(locpc,2,test_equal));
  p2 = (dlong *)args(areg2); deref(p2);
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_equal)));
  locpc += test_equal_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      if (get_smallint(p1) != get_smallint(p2))
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  if (is_real(p1) || is_real(p2))
    {
      dfloat f1, f2;
      if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_equal;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_test_equal;
      if (f1 != f2)
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p1,p2,&locmach,&er);

    if (er) goto arith_error_test_equal;

    if (res != 0)
      { locpc = label3; if (!locpc) goto_fail_label;}
  }
  goto_more_exec;
}

hcase(test_toplevel_equal)
test_toplevel_equal_label:
profile(test_toplevel_equal);
{ dlong areg1;
  dlong areg2;
  codep label3;
  dlong *p1,*p2;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,test_toplevel_equal));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,test_toplevel_equal));
  p2 = (dlong *)args(areg2); deref(p2);
  if (p1 == p2)
    {
      locpc += test_toplevel_equal_len;
        goto_more_exec;
    }

  label3 = (codep)(*(label_type*)(arg_place(locpc,3,test_toplevel_equal)));
  locpc = label3; if (!locpc) goto_fail_label;
  goto_more_exec;  
}

hcase(test_toplevel_smaller)
test_toplevel_smaller_label:
profile(test_toplevel_smaller);
{ dlong areg1;
  dlong areg2;
  codep label3;
  dlong *p1,*p2;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,test_toplevel_smaller));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,test_toplevel_smaller));
  p2 = (dlong *)args(areg2); deref(p2);
  if (p1 < p2)
    {
      locpc += test_toplevel_smaller_len;
        goto_more_exec;
    }

  label3 = (codep)(*(label_type*)(arg_place(locpc,3,test_toplevel_smaller)));
  locpc = label3; if (!locpc) goto_fail_label;
  goto_more_exec;  
}

hcase(test_inequal)
test_inequal_label:
profile_builtin(test_inequal);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong *p1, *p2;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_inequal));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type *)(arg_place(locpc,2,test_inequal));
  p2 = (dlong *)args(areg2); deref(p2);
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_inequal)));
  locpc += test_inequal_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      if (get_smallint(p1) == get_smallint(p2))
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  if (is_real(p1) || is_real(p2))
    {
      dfloat f1, f2;
      if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_inequal;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_test_inequal;
      if (f1 == f2)
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p1,p2,&locmach,&er);

    if (er) goto arith_error_test_inequal;

    if (res == 0)
      { locpc = label3; if (!locpc) goto_fail_label;}
  }
  goto_more_exec;
}

hcase(test_inequalpp)
test_inequalpp_label:
profile_builtin(test_inequalpp);
{
  dlong yvar1;
  dlong yvar2;
  codep label3;
  dlong *p1, *p2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,test_inequalpp));
  p1 = (dlong *)ereg[yvar1]; deref(p1);
  yvar2 = *(yvar_type *)(arg_place(locpc,2,test_inequalpp));
  p2 = (dlong *)ereg[yvar2]; deref(p2);
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_inequalpp)));
  locpc += test_inequalpp_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      if (get_smallint(p1) == get_smallint(p2))
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  if (is_real(p1) || is_real(p2))
    {
      dfloat f1, f2;
      if (!get_float_from_number(p1,&f1,&locmach)) goto_fail_label; // goto arith_error_test_inequalpp;
      if (!get_float_from_number(p2,&f2,&locmach)) goto_fail_label; // goto arith_error_test_inequalpp;
      if (f1 == f2)
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p1,p2,&locmach,&er);

    if (er) goto_fail_label; // goto arith_error_test_inequalpp;

    if (res == 0)
      { locpc = label3; if (!locpc) goto_fail_label;}
  }
  goto_more_exec;
}

hcase(test_smaller_or_equalpp)
test_smaller_or_equalpp_label:
profile_builtin(test_smaller_or_equalpp);
{
  dlong yvar1;
  dlong yvar2;
  codep label3;
  dlong *p1, *p2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,test_smaller_or_equalpp));
  p1 = (dlong *)ereg[yvar1]; deref(p1);
  yvar2 = *(yvar_type *)(arg_place(locpc,2,test_smaller_or_equalpp));
  p2 = (dlong *)ereg[yvar2]; deref(p2);
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_smaller_or_equalpp)));
  locpc += test_smaller_or_equalpp_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      if (get_smallint(p1) > get_smallint(p2))
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  if (is_real(p1) || is_real(p2))
    {
      dfloat f1, f2;
      if (!get_float_from_number(p1,&f1,&locmach)) goto_fail_label;
      if (!get_float_from_number(p2,&f2,&locmach)) goto_fail_label;
      if (f1 > f2)
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p1,p2,&locmach,&er);

    if (er) goto_fail_label; // goto arith_error_test_smaller_or_equalpp;

    if (res > 0)
      { locpc = label3; if (!locpc) goto_fail_label;}
  }
  goto_more_exec;
}

hcase(test_smaller_or_equaltp)
test_smaller_or_equaltp_label:
profile_builtin(test_smaller_or_equaltp);
{
  dlong areg1;
  dlong yvar2;
  codep label3;
  dlong *p1, *p2;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_or_equaltp));
  p1 = (dlong *)args(areg1); deref(p1);
  yvar2 = *(yvar_type *)(arg_place(locpc,2,test_smaller_or_equaltp));
  p2 = (dlong *)ereg[yvar2]; deref(p2);
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_smaller_or_equaltp)));
  locpc += test_smaller_or_equaltp_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      if (get_smallint(p1) > get_smallint(p2))
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  if (is_real(p1) || is_real(p2))
    {
      dfloat f1, f2;
      if (!get_float_from_number(p1,&f1,&locmach)) goto_fail_label;
      if (!get_float_from_number(p2,&f2,&locmach)) goto_fail_label;
      if (f1 > f2)
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p1,p2,&locmach,&er);

    if (er) goto_fail_label; // goto arith_error_test_smaller_or_equaltp;

    if (res > 0)
      { locpc = label3; if (!locpc) goto_fail_label;}
  }
  goto_more_exec;
}

hcase(test_smaller_or_equal)
test_smaller_or_equal_label:
profile_builtin(test_smaller_or_equal);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong *p1, *p2;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_or_equal));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type *)(arg_place(locpc,2,test_smaller_or_equal));
  p2 = (dlong *)args(areg2); deref(p2);
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_smaller_or_equal)));
  locpc += test_smaller_or_equal_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      if (get_smallint(p1) > get_smallint(p2))
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  if (is_real(p1) || is_real(p2))
    {
      dfloat f1, f2;
      if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_smaller_or_equal;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_test_smaller_or_equal;
      if (f1 > f2)
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p1,p2,&locmach,&er);

    if (er) goto arith_error_test_smaller_or_equal;

    if (res > 0)
      { locpc = label3; if (!locpc) goto_fail_label;}
  }
  goto_more_exec;
}

hcase(test_smaller_or_equal_spec)
test_smaller_or_equal_spec_label:
profile_builtin(test_smaller_or_equal_spec);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong *p1, *p2;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_or_equal_spec));
  p1 = (dlong *)args(areg1); deref_in(p1);
  areg2 = *(areg_type *)(arg_place(locpc,2,test_smaller_or_equal_spec));
  p2 = (dlong *)args(areg2); deref_in(p2);
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_smaller_or_equal_spec)));
  locpc += test_smaller_or_equal_spec_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      if (get_smallint(p1) > get_smallint(p2))
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p1,p2,&locmach,&er);

    if (res > 0)
      { locpc = label3; if (!locpc) goto_fail_label;}
  }
  goto_more_exec;
}

hcase(test_smaller)
test_smaller_label:
profile_builtin(test_smaller);
{
  dlong areg1;
  dlong areg2;
  codep label3;
  dlong *p1, *p2;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type *)(arg_place(locpc,2,test_smaller));
  p2 = (dlong *)args(areg2); deref(p2);
  label3 = (codep)(*(dlong *)(arg_place(locpc,3,test_smaller)));
  locpc += test_smaller_len;

  if (is_smallint(p1) && is_smallint(p2))
    {
      if (get_smallint(p1) >= get_smallint(p2))
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  if (is_real(p1) || is_real(p2))
    {
      dfloat f1, f2;
      if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_smaller;
      if (!get_float_from_number(p2,&f2,&locmach)) goto arith_error_test_smaller;
      if (f1 >= f2)
	{ locpc = label3; if (!locpc) goto_fail_label;}
      goto_more_exec;
    }

  {
    dlong res, er;
    res = compare_two_inumbers(p1,p2,&locmach,&er);

    if (er) goto arith_error_test_smaller;

    if (res >= 0)
      { locpc = label3; if (!locpc) goto_fail_label;}
  }
  goto_more_exec;
}

hcase(trust_me_else)
trust_me_else_label:
profile(trust_me_else);
{
  dlong arity1;

  arity1 = *(arity_type *)(arg_place(locpc,1,trust_me_else));
  locpc += trust_me_else_len;

  untrail(loctrreg,((CP_P) locbreg)->tr);
  reset_regs;

  pop_arguments(arity1);
  
  locbreg =  ((CP_P) locbreg)->b;
  freeze_heap();
  bh = ((CP_P) locbreg)->h;
  
  goto_more_exec;
}

hcase(trustmeelse5)
trustmeelse5_label:
profile(trustmeelse5);
{
  CP_P bb;
  locpc += trustmeelse5_len;

  bb = (CP_P)locbreg;
  untrail(loctrreg,((CP_P) locbreg)->tr);
  reset_regs;

  pop_arguments5(bb);
  
  locbreg =  ((CP_P) locbreg)->b;
  freeze_heap();
  bh = ((CP_P) locbreg)->h;

  goto_more_exec;
}

hcase(trustmeelse4)
trustmeelse4_label:
profile(trustmeelse4);
{
  CP_P b;
  locpc += trustmeelse4_len;

  b = (CP_P)locbreg;
  untrail(loctrreg,((CP_P) locbreg)->tr);
  reset_regs;

  pop_arguments4(b);
  
  locbreg =  ((CP_P) locbreg)->b;
  freeze_heap();
  bh = ((CP_P) locbreg)->h;

  goto_more_exec;
}

hcase(trustmeelse3)
trustmeelse3_label:
profile(trustmeelse3);
{
  CP_P b;
  locpc += trustmeelse3_len;

  b = (CP_P)locbreg;
  untrail(loctrreg,((CP_P) locbreg)->tr);
  reset_regs;

  pop_arguments3(b);
  
  locbreg =  ((CP_P) locbreg)->b;
  freeze_heap();
  bh = ((CP_P) locbreg)->h;

  goto_more_exec;
}

hcase(trustmeelse2)
trustmeelse2_label:
profile(trustmeelse2);
{
  CP_P b;
  locpc += trustmeelse2_len;

  b = (CP_P)locbreg;
  untrail(loctrreg,((CP_P) locbreg)->tr);
  reset_regs;

  pop_arguments2(b);
  
  locbreg =  ((CP_P) locbreg)->b;
  freeze_heap();
  bh = ((CP_P) locbreg)->h;

  goto_more_exec;
}

hcase(trustmeelse1)
trustmeelse1_label:
profile(trustmeelse1);
{
  CP_P b;
  locpc += trustmeelse1_len;

  b = (CP_P)locbreg;
  untrail(loctrreg,((CP_P) locbreg)->tr);
  reset_regs;

  pop_arguments1bis(b);
  
  locbreg =  ((CP_P) locbreg)->b;
  freeze_heap();
  bh = ((CP_P) locbreg)->h;

  goto_more_exec;
}

hcase(retry_me_else)
retry_me_else_label:
profile(retry_me_else);
{
  dlong arity1;
  dlong label2;
  arity1 = *(arity_type *)(arg_place(locpc,1,retry_me_else));
  label2 = (*(dlong *)(arg_place(locpc,2,retry_me_else)));

  untrail(loctrreg,((CP_P) locbreg)->tr);
  reset_regs;
  ((CP_P) locbreg)->failurecont = (codep) label2;
  locpc += retry_me_else_len;

  pop_arguments(arity1);

  bh = ((CP_P) locbreg)->h;

  goto_more_exec;
}

hcase(retrymeelse5)
retrymeelse5_label:
profile(retrymeelse5);
{
  dlong label2;
  CP_P b;

  b = (CP_P)locbreg;
  label2 = (*(dlong *)(arg_place(locpc,1,retrymeelse5)));

  untrail(loctrreg,((CP_P) b)->tr);
  reset_regs;
  ((CP_P) locbreg)->failurecont = (codep) label2;
  locpc += retrymeelse5_len;

  pop_arguments5(b);

  bh = ((CP_P) b)->h;

  goto_more_exec;
}

hcase(retrymeelse4)
retrymeelse4_label:
profile(retrymeelse4);
{
  dlong label2;
  CP_P b;

  b = (CP_P)locbreg;
  label2 = (*(dlong *)(arg_place(locpc,1,retrymeelse4)));

  untrail(loctrreg,((CP_P) b)->tr);
  reset_regs;
  ((CP_P) locbreg)->failurecont = (codep) label2;
  locpc += retrymeelse4_len;

  pop_arguments4(b);

  bh = ((CP_P) b)->h;

  goto_more_exec;
}

hcase(retrymeelse3)
retrymeelse3_label:
profile(retrymeelse3);
{
  dlong label2;
  CP_P b;

  b = (CP_P)locbreg;
  label2 = (*(dlong *)(arg_place(locpc,1,retrymeelse3)));

  untrail(loctrreg,((CP_P) b)->tr);
  reset_regs;
  ((CP_P) locbreg)->failurecont = (codep) label2;
  locpc += retrymeelse3_len;

  pop_arguments3(b);

  bh = ((CP_P) b)->h;

  goto_more_exec;
}

hcase(gettbreg_try_me_else5)
gettbreg_try_me_else5_label:
profile(gettbreg_try_me_else5);
args(5) = make_cut_point(locmach.begin_cp,locbreg);
hcase(trymeelse5)
trymeelse5_label:
profile(trymeelse5);
{
  dlong label2;

  label2 = *(dlong *)(arg_place(locpc,1,trymeelse5));
  push_choicepoint5(label2,contpreg);
  locpc += trymeelse5_len;
  bh = lochreg;
  goto_more_exec;
}

hcase(gettbreg_try_me_else4)
gettbreg_try_me_else4_label:
profile(gettbreg_try_me_else4);
   args(4) = make_cut_point(locmach.begin_cp,locbreg);
hcase(trymeelse4)
trymeelse4_label:
profile(trymeelse4);
{
  dlong label2;

  label2 = *(dlong *)(arg_place(locpc,1,trymeelse4));
  push_choicepoint4(label2,contpreg);
  locpc += trymeelse4_len;
  bh = lochreg;
  goto_more_exec;
}

hcase(gettbreg_try_me_else3)
gettbreg_try_me_else3_label:
profile(gettbreg_try_me_else3);
   args(3) = make_cut_point(locmach.begin_cp,locbreg);
hcase(trymeelse3)
trymeelse3_label:
profile(trymeelse3);
{
  dlong label2;

  label2 = *(dlong *)(arg_place(locpc,1,trymeelse3));
  push_choicepoint3(label2,contpreg);
  locpc += trymeelse3_len;
  bh = lochreg;
  goto_more_exec;
}

hcase(retrymeelse2)
retrymeelse2_label:
profile(retrymeelse2);
{
  dlong label2;
  CP_P b;

  b = (CP_P)locbreg;
  label2 = (*(dlong *)(arg_place(locpc,1,retrymeelse2)));

  untrail(loctrreg,((CP_P) b)->tr);
  reset_regs;
  ((CP_P) locbreg)->failurecont = (codep) label2;
  locpc += retrymeelse2_len;

  pop_arguments2(b);

  bh = ((CP_P) b)->h;

  goto_more_exec;
}

hcase(retrymeelse1)
retrymeelse1_label:
profile(retrymeelse1);
{
  dlong label1;
  CP_P b;

  b = (CP_P)locbreg;
  label1 = (*(dlong *)(arg_place(locpc,1,retrymeelse1)));

  untrail(loctrreg,((CP_P) b)->tr);
  reset_regs;
  ((CP_P) locbreg)->failurecont = (codep) label1;
  locpc += retrymeelse1_len;

  pop_arguments1bis(b);

  bh = ((CP_P) b)->h;

  goto_more_exec;
}

hcase(gettbreg_try_me_else2)
gettbreg_try_me_else2_label:
profile(gettbreg_try_me_else2);
   args(2) = make_cut_point(locmach.begin_cp,locbreg);
hcase(trymeelse2)
trymeelse2_label:
profile(trymeelse2);
{
  dlong label2;

  label2 = *(dlong *)(arg_place(locpc,1,trymeelse2));
  push_choicepoint2(label2,contpreg);
  locpc += trymeelse2_len;
  bh = lochreg;
  goto_more_exec;
}

hcase(gettbreg_try_me_else1)
gettbreg_try_me_else1_label:
profile(gettbreg_try_me_else1);
   args(1) = make_cut_point(locmach.begin_cp,locbreg);
hcase(trymeelse1)
trymeelse1_label:
profile(trymeelse1);
{
  dlong label1;

  label1 = *(dlong *)(arg_place(locpc,1,trymeelse1));
  push_choicepoint1(label1,contpreg);
  locpc += trymeelse1_len;
  bh = lochreg;
  goto_more_exec;
}

hcase(gettbreg_try_me_else)
gettbreg_try_me_else_label:
profile(gettbreg_try_me_else);
{
  dlong label2;
  dlong arity1;
  arity1 = *(arity_type *)(arg_place(locpc,1,try_me_else));
  args(arity1) = make_cut_point(locmach.begin_cp,locbreg);
hcase(try_me_else)
try_me_else_label:
  arity1 = *(arity_type *)(arg_place(locpc,1,try_me_else));
  label2 = *(dlong *)(arg_place(locpc,2,try_me_else));
  push_choicepoint(label2,contpreg,arity1);
  locpc += try_me_else_len;
  bh = lochreg;
  goto_more_exec;
}

hcase(put_intv)
put_intv_label:
profile(put_intv);
{
  dlong yvar1;
  dlong int2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,put_intv));
  int2 = *(dlong *)(arg_place(locpc,2,put_intv));
  locpc += put_intv_len;

  ereg[yvar1] = int2;
  goto_more_exec;
}

hcase(get_intv)
get_intv_label:
profile(get_intv);
{
  dlong yvar1;
  dlong int2;
  dlong *p;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,get_intv));
  int2 = *(dlong *)(arg_place(locpc,2,get_intv));

  locpc += get_intv_len;
  p = (dlong *)ereg[yvar1];
  deref2(p,goto get_intv_ref);

  if ((dlong)p == int2)
    goto_more_exec;

  if (!is_attributed(p))
    goto_fail_label;

  {
    safe_wakeup(p,(dlong *)int2,&locmach);
    goto_more_exec;
  }

 get_intv_ref:
  trail(p,bh,loctrreg);
  *p = int2;
  goto_more_exec;
}

hcase(get_floatv)
get_floatv_label:
profile(get_floatv);
{
  dlong yvar1;
  dfloat f2;
  dlong *p,q;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,get_floatv));
  get_double_arg(f2, arg_place(locpc,2,get_floatv));

  locpc += get_floatv_len;
  p = (dlong *)ereg[yvar1];
  deref2(p,goto get_floatv_ref);

  if (is_real(p) && (get_real(p) == f2))
    goto_more_exec;

  if (! is_attributed(p))
    goto_fail_label;

  build_real(&q,f2,lochreg);
  safe_wakeup(p,(dlong *)q,&locmach);
  goto_more_exec;

 get_floatv_ref:
  trail(p,bh,loctrreg);
  build_real(p,f2,lochreg);
  goto_more_exec;
}

hcase(build_list)
build_list_label:
profile(build_list);
{
  dlong distance;

  distance = *(dlong *)(arg_place(locpc,1,build_list));
  locpc += build_list_len;
  *lochreg = make_list(lochreg+distance);
  lochreg++;
  goto_more_exec;
}

hcase(unify_float_untagged)
unify_float_untagged_label:
profile(unify_float_untagged);
{
  dfloat f1;

  get_double_arg(f1, arg_place(locpc,1,unify_float_untagged));
  if (WAMS)
    {
      dlong *p,q;
      locpc += unify_float_untagged_len;
	  
      p = WAMS++;
      deref2(p,goto unify_float_untagged_ref);
      if (is_real(p) && (get_real(p) == f1))
	goto_more_exec;

      if (! is_attributed(p))
	 goto_fail_label;

      build_real(&q,f1,lochreg);
      safe_wakeup(p,(dlong *)q,&locmach);
      goto_more_exec;

    unify_float_untagged_ref:
      trail(p,bh,loctrreg);
      build_real(p, f1, lochreg);
      goto_more_exec;
    }
  else
    {
      dlong offset;
      offset = *(dlong *)(arg_place(locpc,2,unify_float_untagged));
      locpc += unify_float_untagged_len;
      *lochreg = make_number_pointer((lochreg-offset));
      *(lochreg-offset) = REAL_HEADER;
      offset--;
      store_double_arg((lochreg-offset),f1);
      lochreg++;
    }
  goto_more_exec;
}


hcase(unify_int)
unify_int_label:
profile(unify_int);
{
  dlong int1;

  int1 = *(dlong *)(arg_place(locpc,1,unify_int));
  if (WAMS)
    {
      dlong *p;
      locpc += unify_int_len;
	  
      p = WAMS++;
      deref2(p,goto unify_int_ref);
      if ((dlong)p == int1)
	goto_more_exec;

      if (! is_attributed(p))
	 goto_fail_label;

      safe_wakeup(p,(dlong *)int1,&locmach);
      goto_more_exec;

    unify_int_ref:
      trail(p,bh,loctrreg);
      *p = int1;
      goto_more_exec;
    }
  else
    {
      locpc += unify_int_len;
      *lochreg = int1;
      lochreg++;
    }
  goto_more_exec;  
}

hcase(unify_int2)
unify_int2_label:
profile(unify_int2);
{

  if (WAMS)
    {
      dlong int1;
      dlong *p;

      int1 = *(dlong *)arg_place(locpc,1,unify_int2);
      p = WAMS++;
      deref2(p,goto unify_int2_ref1);
      if ((dlong)p == int1)
	goto next_int;
	  
      if (! is_attributed(p))
	goto_fail_label;
	  
      safe_wakeup(p,(dlong *)int1,&locmach);
      goto next_int;;
	  
    unify_int2_ref1:
      trail(p,bh,loctrreg);
      *p = int1;

    next_int:

      int1 = *(dlong *)arg_place(locpc,2,unify_int2);
      locpc += unify_int2_len;

      p = WAMS++;
      deref2(p,goto unify_int2_ref2);
      if ((dlong)p == int1)
	goto_more_exec;
	  
      if (! is_attributed(p))
	goto_fail_label;
	  
      safe_wakeup(p,(dlong *)int1,&locmach);
      goto_more_exec;
	  
    unify_int2_ref2:
      trail(p,bh,loctrreg);
      *p = int1;

      goto_more_exec;
    }
  else
    {
      dlong int1;
      int1 = *(dlong *)(arg_place(locpc,1,unify_int2));
      *lochreg = int1;
      lochreg++;
      int1 = *(dlong *)(arg_place(locpc,2,unify_int2));
      *lochreg = int1;
      lochreg++;
      locpc += unify_int2_len;
    }
  goto_more_exec;  
}

hcase(get_structure_untagged)
get_structure_untagged_label:
profile(get_structure_untagged);
{
  dlong areg1;
  dlong struct2;
  dlong offset3;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,get_structure_untagged));
  struct2 = *(dlong *)(arg_place(locpc,2,get_structure_untagged));
  offset3 = *(dlong *)(arg_place(locpc,3,get_structure_untagged));
  locpc += get_structure_untagged_len;
  p = (dlong *)args(areg1);
  deref2(p, goto get_structure_untagged_ref);
  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      if (*p != struct2)
	{ goto_fail_label; }
      else
	WAMS = p+1;
      goto_more_exec;
    }

  if (! is_attributed(p))
    goto_fail_label;

  WAMS = 0;
  safe_wakeup(p,(dlong *)make_struct_p(lochreg+offset3),&locmach);
  lochreg += offset3;
  *lochreg = struct2;
  lochreg++;
  goto_more_exec;

 get_structure_untagged_ref:
  WAMS = 0;
  trail(p,bh,loctrreg);
  *p = make_struct_p(lochreg+offset3);
  lochreg += offset3;
  *lochreg = struct2;
  lochreg++;
  goto_more_exec;
}

hcase(get_list_untagged)
get_list_untagged_label:
profile(get_list_untagged);
{
  dlong areg1;
  dlong offset2;
  dlong *p;

  areg1 = *(areg_type *)(arg_place(locpc,1,get_list_untagged));
  offset2 = *(dlong *)(arg_place(locpc,2,get_list_untagged));
  locpc += get_list_untagged_len;
  p = (dlong *)args(areg1);
  deref2(p,goto get_list_untagged_ref);

  if (is_list(p))
    {
      WAMS = get_list_pointer(p);
      goto_more_exec ;
    }

  if (! is_attributed(p))
    goto_fail_label;

  safe_wakeup(p,(dlong *)make_list( lochreg+offset2),&locmach);
  WAMS = 0;
  lochreg += offset2;
  goto_more_exec;

 get_list_untagged_ref:
  WAMS = 0;
  trail(p,bh,loctrreg);
  *p = make_list( lochreg+offset2);
  lochreg += offset2;
  goto_more_exec;
}

hcase(get_listv_untagged)
get_listv_untagged_label:
profile(get_listv_untagged);
{
  dlong yvar1;
  dlong offset2;
  dlong *p;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,get_listv_untagged));
  offset2 = *(dlong *)(arg_place(locpc,2,get_listv_untagged));
  locpc += get_listv_untagged_len;
  p = (dlong *)ereg[yvar1];
  deref2(p,goto get_listv_untagged_ref);

  if (is_list(p))
    {
      WAMS = get_list_pointer(p);
      goto_more_exec ;
    }

  if (! is_attributed(p))
    goto_fail_label;

  safe_wakeup(p,(dlong *)make_list( lochreg+offset2),&locmach);
  WAMS = 0;
  lochreg += offset2;
  goto_more_exec;

 get_listv_untagged_ref:
  WAMS = 0;
  trail(p,bh,loctrreg);
  *p = make_list( lochreg+offset2);
  lochreg += offset2;
  goto_more_exec;
}

hcase(get_structurev_untagged)
get_structurev_untagged_label:
profile(get_structurev_untagged);
{
  dlong yvar1;
  dlong struct2;
  dlong offset3;
  dlong *p;
  
  yvar1 = *(yvar_type*)(arg_place(locpc,1,get_structurev_untagged));
  struct2 = *(dlong *)(arg_place(locpc,2,get_structurev_untagged));
  offset3 = *(dlong *)(arg_place(locpc,3,get_structurev_untagged));
  locpc += get_structurev_untagged_len;

  p = (dlong *)ereg[yvar1];
  deref2(p, goto get_structurev_untagged_ref);
  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      if (*p != struct2)
	{ goto_fail_label; }
      else
	WAMS = p+1;
      goto_more_exec;
    }

  if (! is_attributed(p))
    goto_fail_label;

  WAMS = 0;
  safe_wakeup(p,(dlong *)make_struct_p(lochreg+offset3),&locmach);
  lochreg += offset3;
  *lochreg = struct2;
  lochreg++;
  goto_more_exec;

 get_structurev_untagged_ref:
  WAMS = 0;
  trail(p,bh,loctrreg);
  *p = make_struct_p(lochreg+offset3);
  lochreg += offset3;
  *lochreg = struct2;
  lochreg++;
  goto_more_exec;
}

hcase(unify_list_untagged)
unify_list_untagged_label:
profile(unify_list_untagged);
{
  if (WAMS)
    {
      dlong *p;
      locpc += unify_list_untagged_len;
      p = WAMS;
      deref2(p,goto unify_list_untagged_ref);
      if (is_list(p))
	{
	  WAMS = get_list_pointer(p);
	  goto_more_exec;
	}

      if (!is_attributed(p))
	goto_fail_label;

      WAMS = 0;
      safe_wakeup(p,(dlong *)make_list( lochreg),&locmach);
      goto_more_exec;

    unify_list_untagged_ref:
	  WAMS = 0;
	  *p = make_list( lochreg);
	  trail(p,bh,loctrreg);
	  goto_more_exec;
    }

    {
      dlong spare = *(dlong *)(arg_place(locpc,1,unify_list_untagged));
      locpc += unify_list_untagged_len;
      *lochreg = make_list(lochreg+1+spare);
      lochreg += spare+1;
      goto_more_exec;
    }
}

hcase(set_void)
set_void_label:
profile(set_void);
{
  locpc += set_void_len;

  *lochreg = (dlong)lochreg;
  lochreg++;
  goto_more_exec;
}

hcase(set_void2)
set_void2_label:
profile(set_void2);
{
  register dlong *h = lochreg;
  locpc += set_void2_len;
  h[0] = (dlong)h;
  h[1] = (dlong)(h+1);
  lochreg = h + 2;
  goto_more_exec;
}

hcase(set_void3)
set_void3_label:
profile(set_void3);
{
  register dlong *h = lochreg;
  locpc += set_void3_len;
  h[0] = (dlong)h;
  h[1] = (dlong)(h+1);
  h[2] = (dlong)(h+2);
  lochreg = h + 3;
  goto_more_exec;
}

hcase(set_void4)
set_void4_label:
profile(set_void4);
{
  register dlong *h = lochreg;
  locpc += set_void4_len;
  h[0] = (dlong)h;
  h[1] = (dlong)(h+1);
  h[2] = (dlong)(h+2);
  h[3] = (dlong)(h+3);
  lochreg = h + 4;
  goto_more_exec;
}

hcase(uni_void)
uni_void_label:
profile(uni_void);
{
  locpc += uni_void_len;
  if (WAMS)
    {
      WAMS++;
      goto_more_exec;
    }
  *lochreg = (dlong)lochreg;
  lochreg++;
  goto_more_exec;
}

hcase(uni_void_2)
uni_void_2_label:
profile(uni_void_2);
{
  locpc += uni_void_2_len;
  if (WAMS)
    {
      WAMS += 2;
      goto_more_exec;
    }
  {
    dlong *h = lochreg;
    *h = (dlong)h; h++;
    *h = (dlong)h; h++;
    lochreg = h;
  }
  goto_more_exec;
}

hcase(uni_void_3)
uni_void_3_label:
profile(uni_void_3);
{
  locpc += uni_void_3_len;
  if (WAMS)
    {
      WAMS += 3;
      goto_more_exec;
    }
  {
    dlong *h = lochreg;
    *h = (dlong)h; h++;
    *h = (dlong)h; h++;
    *h = (dlong)h; h++;
    lochreg = h;
  }
  goto_more_exec;
}

hcase(uni_void_n)
uni_void_n_label:
profile(uni_void_n);
{
  dlong n;

  n = *(dlong *)(arg_place(locpc,1,uni_void_n));
  locpc += uni_void_n_len;
  if (WAMS)
    {
      WAMS += n;
      goto_more_exec;
    }
  {
    dlong *h = lochreg;
    while (n--)
      {
	*h = (dlong)h;
	h++;
      }
    lochreg = h;
  }
  goto_more_exec;
}

hcase(putlistv)
putlistv_label:
profile(putlistv);
{
  dlong yvar1;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,putlistv));
  locpc += putlistv_len;

  ereg[yvar1] = make_list(lochreg);
  goto_more_exec;
}

hcase(getlistv)
getlistv_label:
profile(getlistv);
{
  dlong yvar1;
  dlong *p;

  yvar1 = *(yvar_type*)(arg_place(locpc,1,getlistv));

  p = (dlong *)ereg[yvar1];
  locpc += getlistv_len;

  deref2(p,goto getlistv_ref);

  if (is_list(p))
    {
      WAMS = get_list_pointer(p);
      goto_more_exec ;
    }

  if (! is_attributed(p))
    goto_fail_label;

  WAMS = 0;
  safe_wakeup(p,(dlong *)make_list(lochreg),&locmach);
  goto_more_exec;

 getlistv_ref:
  WAMS = 0;
  *p = make_list(lochreg);
  trail(p,bh,loctrreg);
  goto_more_exec;
}

hcase(set_float)
set_float_label:
profile(set_float);
{
  dlong offset;
  offset = *(dlong *)(arg_place(locpc, 1, set_float));
  locpc += set_float_len;
  *lochreg = make_number_pointer((lochreg+offset));
  lochreg++;
  goto_more_exec;
}

hcase(set_struct)
set_struct_label:
profile(set_struct);
{
  dlong offset;

  offset = *(dlong *)(arg_place(locpc, 1, set_struct));
  locpc += set_struct_len;
  *lochreg = make_struct_p(lochreg+offset);
  lochreg++;
  goto_more_exec;
}

hcase(unify_structure)
unify_structure_label:
profile(unify_structure);
{
  dlong struct1;
      
  struct1 = *(dlong *)(arg_place(locpc,1,unify_structure));
  locpc += unify_structure_len;
  if (WAMS)
    {
      dlong *p;
      p = (dlong *)*WAMS;
      deref2(p,goto unify_structure_ref);

      if (is_struct(p))
	{
	  p = get_struct_pointer(p);
	  if (*p != struct1)
	    { goto_fail_label; }
	  else
	    WAMS = p+1;
	  goto_more_exec;
	}

      if (! is_attributed(p))
	goto_fail_label;

      WAMS = 0;
      safe_wakeup(p,(dlong *)make_struct_p(lochreg),&locmach);
      *lochreg = struct1;
      lochreg++;
      goto_more_exec;

    unify_structure_ref:
      WAMS = 0;
      *p = make_struct_p(lochreg);
      *lochreg = struct1;
      lochreg++;
      trail(p,bh,loctrreg);
      goto_more_exec;
    }

  *lochreg = make_struct_p(lochreg+1);
  lochreg++;
  *lochreg = struct1;
  lochreg++;
  goto_more_exec;
}

hcase(set_functor)
set_functor_label:
profile(set_functor);
{
  dlong struct1;
  struct1 = *(dlong *)(arg_place(locpc,1,set_functor));
  locpc += set_functor_len;
  *lochreg  = struct1;       
  lochreg++;
  goto_more_exec;
}

hcase(move_var)
move_var_label:
profile(move_var);
{
  dlong yvar1, yvar2;
  yvar1 = *(yvar_type *)(arg_place(locpc,1,move_var));
  yvar2 = *(yvar_type *)(arg_place(locpc,2,move_var));
  locpc += move_var_len;
  ereg[yvar2] = ereg[yvar1];
  goto_more_exec;
}

hcase(move_var2)
move_var2_label:
profile(move_var2);
{
  dlong yvar1, yvar2;
  yvar1 = *(yvar_type *)(arg_place(locpc,1,move_var2));
  yvar2 = *(yvar_type *)(arg_place(locpc,2,move_var2));
  ereg[yvar2] = ereg[yvar1];

  yvar1 = *(yvar_type *)(arg_place(locpc,3,move_var2));
  yvar2 = *(yvar_type *)(arg_place(locpc,4,move_var2));
  locpc += move_var2_len;
  ereg[yvar2] = ereg[yvar1];

  goto_more_exec;
}

hcase(move_var3)
move_var3_label:
profile(move_var3);
{
  dlong yvar1, yvar2;
  yvar1 = *(yvar_type *)(arg_place(locpc,1,move_var3));
  yvar2 = *(yvar_type *)(arg_place(locpc,2,move_var3));
  ereg[yvar2] = ereg[yvar1];

  yvar1 = *(yvar_type *)(arg_place(locpc,3,move_var3));
  yvar2 = *(yvar_type *)(arg_place(locpc,4,move_var3));
  ereg[yvar2] = ereg[yvar1];

  yvar1 = *(yvar_type *)(arg_place(locpc,5,move_var3));
  yvar2 = *(yvar_type *)(arg_place(locpc,6,move_var3));
  locpc += move_var3_len;
  ereg[yvar2] = ereg[yvar1];

  goto_more_exec;
}

hcase(getpvalv)
getpvalv_label:
profile(getpvalv);
{
  dlong yvar1;
  dlong yvar2;
  dlong *p1, *p2;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,getpvalv));
  p1 = (dlong *)ereg[yvar1];
  deref(p1);

  yvar2 = *(yvar_type*)(arg_place(locpc,2,getpvalv));
  p2 = (dlong *)ereg[yvar2];
  deref(p2);

  locpc += getpvalv_len;

  pre_unify_terms(p1,p2,finish_getpvalv);

 finish_getpvalv:
  goto_more_exec;

}

hcase(builtin_unwind_stack_callcc_0)
builtin_unwind_stack_callcc_0_label:
profile(builtin_unwind_stack_callcc_0);
{
  codep tmppc;

  tmppc = unwind_stack_callcc(&locmach);
  if (! tmppc)
    {
      locpc += builtin_unwind_stack_callcc_0_len;
      goto error_unwind_stack_callcc_0;
    }


  // we skip over the builtin_after_callcc_0 instruction
  locpc = tmppc + builtin_after_callcc_0_len;

  recompute_tos(ereg,locbreg);
  goto_more_exec;
}

hcase(builtin_after_callcc_0)
builtin_after_callcc_0_label:
profile(builtin_after_callcc_0);
{
  // now this instruction is responsible for handling the case that we
  // got here without a continue/1
  // three things are safe: (1) put something safe in Areg1 (2) fail (3) error
  //  goto_fail_label;

  args(1) = args(2) = make_smallint(0);

  locpc += builtin_after_callcc_0_len;
  goto_more_exec;
}

hcase(builtin_after_intercept_0)
builtin_after_intercept_0_label:
profile(builtin_after_intercept_0);
{
  locpc += builtin_after_intercept_0_len;
  goto_more_exec;
}

hcase(builtin_find_intercept_2)
builtin_find_intercept_2_label:
profile(builtin_find_intercept_2);
{
  dlong areg1, areg2;
  dlong *p1, *p2;
  dlong *tempe;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_find_intercept_2));
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_find_intercept_2));
  p1 = (dlong *)args(areg1); deref(p1);
  p2 = (dlong *)args(areg2); deref(p2);

  locpc += builtin_find_intercept_2_len;

  tempe = find_intercept(&locmach);

  if (! tempe) goto_more_exec;

  *p1 = tempe[3];
  *p2 = tempe[2];

  goto_more_exec;
}

hcase(builtin_nextEP_3)
builtin_nextEP_3_label:
profile(builtin_nextEP_3);
{
  dlong areg1, areg2, areg3;
  dlong *p1, *p2, *p3;
  dlong i1, i2, i3;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_nextEP_3));
  p1 = (dlong *)args(areg1); deref(p1);
  if (is_atom(p1))
    i1 = ereg - locmach.begin_ls;
  else
    i1 = get_smallint(p1); // slightly unsafe :-)

  {
    dlong *tmpe = locmach.begin_ls + i1;
    codep tmppc;
    
    tmppc = (codep)tmpe[1];
    tmpe = (dlong *)tmpe[0];
    
    i2 = tmpe - locmach.begin_ls;
    i3 = tmppc - codezone;
  }
  
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_nextEP_3));
  p2 = (dlong *)args(areg2); deref(p2);
  *p2 = make_smallint(i2);

  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_nextEP_3));
  p3 = (dlong *)args(areg3); deref(p3);
  *p3 = make_smallint(i3);

  locpc += builtin_nextEP_3_len;
  goto_more_exec;
}

hcase(builtin_get_one_tailbody_3)
builtin_get_one_tailbody_3_label:
profile(builtin_get_one_tailbody_3);
{
  dlong areg1, areg2, areg3, i;
  dlong *p1, *p2, *p3, *tmpE;
  codep tmppc;
  register dlong *localh;

  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_get_one_tailbody_3));
  p2 = (dlong *)args(areg2); deref(p2);
  i = get_smallint(p2);

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_get_one_tailbody_3));
  p1 = (dlong *)args(areg1); deref(p1);
  tmpE = locmach.begin_ls + get_smallint(p1);

  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_get_one_tailbody_3));
  p3 = (dlong *)args(areg3); deref(p3);

  localh = lochreg;
  *p3 = make_struct_p(localh);
  localh[0] = funct_continuation_2;
  localh[1] = make_smallint(i); // codepointer: points behind active_yvar

  localh +=2;
  tmppc = codezone + i - active_yvar_len;
  localh = collect_active_yvars(tmpE,tmppc,localh);
  localh[0] = nil_atom;
  lochreg = localh+1;

  locpc += builtin_get_one_tailbody_3_len;
  goto_more_exec;
}

hcase(test_points_to_callcc)
test_points_to_callcc_label:
profile(test_points_to_callcc);
{
  dlong areg1, i;
  dlong *p1;
  codep cp;
  dlong label2;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,test_points_to_callcc));
  p1 = (dlong *)args(areg1); deref(p1);

  i = get_smallint(p1);

  cp = codezone + i;

  if (starcp(cp) != after_callccinstr)
    locpc = (codep)(*(dlong *)(arg_place(locpc,2,test_points_to_callcc)));
  else
    locpc += test_points_to_callcc_len;

  goto_more_exec;
}

hcase(builtin_points_to_callcc_1)
builtin_points_to_callcc_1_label:
profile(builtin_points_to_callcc_1);
{
  dlong areg1, i;
  dlong *p1;
  codep cp;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_points_to_callcc_1));
  p1 = (dlong *)args(areg1); deref(p1);

  i = get_smallint(p1);

  cp = codezone + i;

  if (starcp(cp) != after_callccinstr)
    goto_fail_label;
  
  locpc += builtin_points_to_callcc_1_len;
  goto_more_exec;
}

hcase(builtin_new_call_continuation_0)
builtin_new_call_continuation_0_label:
profile(builtin_new_call_continuation_0);
{

  // '$cont$'(_E,_P,_LastE) :- sysh:new_call_continuation.

  // put contpreg in LastE[1]
  // install E and P

  dlong *laste;
  dlong i;
  i = get_smallint(args(3));
  laste = locmach.begin_ls + i;
  laste[0] = (dlong)ereg;
  laste[1] = (dlong)contpreg;

  i = get_smallint(args(1));
  ereg = locmach.begin_ls + i;

  i = get_smallint(args(2));
  locpc = codezone + i;

  goto_more_exec;
}

hcase(builtin_wln_1)
builtin_wln_1_label:
profile(builtin_wln_1);
{
  dlong areg1, i;
  dlong *p;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_wln_1));
  p = (dlong *)args(areg1);
  deref(p);
  i = get_atom_index(p);

  fprintf(stderr,"%s\n",symtab[i].name);

  locpc += builtin_wln_1_len;
  goto_more_exec;
}

hcase(builtin_current_reg_2)
builtin_current_reg_2_label:
profile(builtin_current_reg_2);
{
  dlong areg1, areg2, i;
  dlong *p;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_current_reg_2));
  p = (dlong *)args(areg1); deref(p);
  i = get_smallint(p);

  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_current_reg_2));
  p = (dlong *)args(areg2); deref(p);

  if (i == 1) // E
    i = ereg - locmach.begin_ls; 
  else
    i = contpreg - codezone;

  *p = make_smallint(i);
  locpc += builtin_current_reg_2_len;
  goto_more_exec;
}

hcase(builtin_setEinB_0)
builtin_setEinB_0_label:
profile(builtin_setEinB_0);
{
  CP_P lb = (CP_P)locbreg;
  dlong *max;

  if (ereg < tos)
    max = ereg;
  else max = tos;

  lb->tops = max;

  locpc += builtin_setEinB_0_len;
  goto_more_exec;
}


hcase(builtin_end_new_shift_3)
builtin_end_new_shift_3_label:
profile(builtin_end_new_shift_3);
{
  dlong areg3, i;
  dlong *p;

  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_end_new_shift_3));
  p = (dlong *)args(areg3); deref(p);
  i = get_smallint(p);

  p = locmach.begin_ls + i;

  locpc = (codep)p[1];
  locpc += builtin_after_callcc_0_len;
  ereg = (dlong *)p[0];

  goto_more_exec;
}

hcase(builtin_call_tailbody_1)
builtin_call_tailbody_1_label:
profile(builtin_call_tailbody_1);
{
  dlong areg1;
  dlong *p1, *p2, *yvarlistp, *p;
  codep tmppc;
  dlong size, i;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_call_tailbody_1));
  p1 = (dlong *)args(areg1); deref(p1);

  // assuming all is correct ...

  p1 = get_struct_pointer(p1);
  p2 = (dlong *)p1[1]; deref(p2); i = get_smallint(p2); i -= active_yvar_len;
  tmppc = codezone + i;
  p = (dlong *)tmppc;
  size = p[-1];

  yvarlistp = (dlong *)p1[2];
  deref(yvarlistp);
  
  // make an environment (as allocate with size)
  {
    dlong *loctos;

    loctos = tos;
    tos = loctos = loctos - size;
    *loctos = (dlong)ereg;
    ereg = loctos;
    if (loctos < ls_limit)
      loctos = deal_with_ls_overflow(&locmach);
    *(loctos+1) = (dlong)contpreg;
  }

  // fill the slots appropriately - cut slots treated specially
  {
    dlong cutpoint = make_cut_point(locmach.begin_cp,locbreg);
    install_active_yvars(ereg,tmppc,yvarlistp,cutpoint);
  }

  locpc = tmppc + active_yvar_len;
  goto_more_exec;
}

hcase(builtin_construct_delcont_1)
builtin_construct_delcont_1_label:
profile(builtin_construct_delcont_1);
{

  // first make sure there is enough space on the heap make a gross
  // overestimate, because making a precise estimate costs too much

  dlong size;
  dlong *localh;
  void* relevant = after_callccinstr;
  dlong *tmpe;
  codep tmppc;
  dlong *p;


  size = locmach.begin_ls - ereg;
  size = 5+2*size;

  if ((lochreg + size) > heap_limit)
    macro_deal_with_heap_overflow(1,size);

  localh = lochreg;

  args(2) = args(1); // the Term
  args(1) = make_struct_p(localh);
  *localh = funct_call_continuation_1;
  localh++;
  p = localh++;

  tmpe = ereg;
  tmppc = contpreg;

  while ((starcp(tmppc) != relevant ) && (tmpe != locmach.begin_ls))
    {
      dlong i;
      *p = make_list(localh);
      p = localh+1;
      localh[0] = make_struct_p(localh+2);
      localh += 2;
      localh[0] = funct_continuation_2;
      i = tmppc - codezone;
      localh[1] = make_smallint(i);
      localh += 2;
      tmppc -= active_yvar_len;
      //      { dlong *dummy = collect_active_yvars(tmpe,tmppc,localh); }
      localh = collect_active_yvars(tmpe,tmppc,localh);
      localh[0] = nil_atom;
      localh++;

      tmppc = (codep)(tmpe[1]);
      tmpe = (dlong *)(tmpe[0]);      
    }
  
  *p = nil_atom;

  lochreg = localh;

  locpc = tmppc + builtin_after_callcc_0_len;
  ereg = tmpe;

  goto_more_exec;
}


hcase(builtin_reset_cont_0)
builtin_reset_cont_0_label:
profile(builtin_reset_cont_0);
{
  // '$newcont$'(_E,_P,_LastE) :- sysh:reset_cont, fail.
  // reset in LastE the E to E and the CP to P
  // fail

  dlong *laste, *p, *e;
  codep tmppc;
  dlong i;

  p = (dlong *)args(2); deref(p);
  i = get_smallint(p);
  tmppc = codezone + i;

  e = (dlong *)args(1); deref(e);
  i = get_smallint(e);
  e = locmach.begin_ls + i;

  laste = (dlong *)args(3); deref(laste);
  i = get_smallint(laste);
  laste = locmach.begin_ls + i;

  laste[0] = (dlong)e;
  laste[1] = (dlong)tmppc;  

  goto_fail_label;
}

hcase(builtin_asm_1)
builtin_asm_1_label:
profile(builtin_asm_1);
{
  fprintf(stderr,"builtin_asm_1 never implemented\n"); return;
  locpc += builtin_asm_1_len;
  goto_more_exec;
}


hcase(builtin_unwind_stack_0)
builtin_unwind_stack_0_label:
profile_builtin(builtin_unwind_stack_0);
{
  unwind_stack(&locmach);
  freeze_heap();
  bh = ((CP_P) locbreg)->h;  /* not sure about this one ! */
  failure;
  // now locpc points to a retrymeelse3 label
  // we want to set locpc to that label
  locpc = (codep)((*(dlong *)(arg_place(locpc,1,retrymeelse3))));
  goto_more_exec;
}


hcase(builtin_end_catch_1)
builtin_end_catch_1_label:
profile(builtin_end_catch_1);
{

  dlong areg1;
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_end_catch_1));
  if (ereg[areg1] == make_cut_point(locmach.begin_cp,locbreg))
    {
      // we want to clean up the choicepoint by catch
      // and fail
      locbreg = ((CP_P)locbreg)->b;
      freeze_heap();
      bh = ((CP_P)locbreg)->h;
    }

  // otherwise we just want to perform a dealloc_proceed
  // so we better go there :-)

  goto dealloc_proceed_label;
}

hcase(builtin_attvar_1)
builtin_attvar_1_label:
profile_builtin(builtin_attvar_1);
{
  
  dlong areg1;
  dlong *p;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_attvar_1));
  p = (dlong *)args(areg1);
  deref(p);
  locpc += builtin_attvar_1_len;
  if (is_attributed(p))
    goto_more_exec;
  goto_fail_label;
}

hcase(builtin_make_new_attvar_1)
builtin_make_new_attvar_1_label:
profile_builtin(builtin_make_new_attvar_1);
{
  
  dlong areg1;
  dlong *p;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_make_new_attvar_1));
  p = (dlong *)args(areg1);
  deref(p);
  if (! is_ref(p))
    goto_fail_label;
  trail(p,bh,loctrreg);
  *p = (dlong)lochreg;
  *lochreg = (dlong)make_attvar_p(lochreg);
  lochreg++;
  *lochreg = nil_atom;
  lochreg++;
  locpc += builtin_make_new_attvar_1_len;
  goto_more_exec;
}

hcase(builtin_put_attr_lowlevel_init_2)
builtin_put_attr_lowlevel_init_2_label:
profile_builtin(builtin_put_attr_lowlevel_init_2);
{
  
  dlong areg1;
  dlong areg2;
  dlong *p1, *p2;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_put_attr_lowlevel_init_2));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_put_attr_lowlevel_init_2));
  p2 = (dlong *)args(areg2); deref(p2); underef(p2);

  locpc += builtin_put_attr_lowlevel_init_2_len;

  if (is_ref(p1))
    {
      dlong *h = lochreg;

      lochreg = h + 4;
      trail(p1,bh,loctrreg);
      *p1 = (dlong)h;
      h[0] = (dlong)make_attvar_p(h);
      h[1] = make_list(h+2);
      h[2] = (dlong)p2;
      h[3] = nil_atom;
      goto_more_exec;
    }
  else if (! is_attributed(p1))
    goto_fail_label;

  {
    dlong *goal;
    dlong *h = lochreg;

    lochreg = h + 2;

    p1 = get_attvar_pointer(p1);
    p1++; goal = p1; deref(goal); underef(goal);
    h[0] = (dlong)p2;
    h[1] = (dlong)goal;
    p2 = (dlong *)make_list(h);
    noye_trail(p1,bh,loctrreg,p2);
    goto_more_exec;
  }
}

hcase(builtin_put_attr_lowlevel_2)
builtin_put_attr_lowlevel_2_label:
profile_builtin(builtin_put_attr_lowlevel_2);
{
  
  dlong areg1;
  dlong areg2;
  dlong *p1, *p2;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_put_attr_lowlevel_2));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_put_attr_lowlevel_2));
  p2 = (dlong *)args(areg2); deref(p2);

  if (! is_attributed(p1))
    goto_fail_label;
  p1 = get_attvar_pointer(p1);
  p1++;
  underef(p2);
  noye_trail(p1,bh,loctrreg,p2);
  locpc += builtin_put_attr_lowlevel_2_len;
  goto_more_exec;
}


hcase(builtin_varfreeze_2)
builtin_varfreeze_2_label:
profile_builtin(builtin_varfreeze_2);
{

  dlong areg1;
  dlong areg2;
  dlong *p1, *p2;

  /* first argument is var - maybe an attributed var with or without freeze */

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_varfreeze_2));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_varfreeze_2));
  p2 = (dlong *)args(areg2); deref(p2); underef(p2);

  locpc += builtin_varfreeze_2_len;

  if (is_ref(p1))
    {
      dlong i;
      trail(p1,bh,loctrreg);
      *p1 = (dlong)lochreg;
      *lochreg = (dlong)make_attvar_p(lochreg);
      lochreg++;
      *lochreg = (dlong)p2;
      lochreg++;
      goto_more_exec;
    }

  /* p1 is an attributed var but we succeed if it isn't */

  if (! is_attributed(p1)) goto_more_exec;

  p1 = get_attvar_pointer(p1);
  p1++;

  {
    dlong *s, *g;
    s = (dlong *)make_struct_p(lochreg);
    g = (dlong *)*p1; deref(g); underef(g);
    *lochreg = make_funct(sysh_conjunction_2);
    lochreg++;
    *lochreg = (dlong)g;
    lochreg++;
    *lochreg = (dlong)p2;
    lochreg++;
    noye_trail(p1,bh,loctrreg,s);
    goto_more_exec;
  }
}



hcase(builtin_put_attr_lowlevel_3)
builtin_put_attr_lowlevel_3_label:
profile_builtin(builtin_put_attr_lowlevel_3);
{
  
  dlong areg1;
  dlong areg2;
  dlong areg3;
  dlong *p1, *p2, *p3, *q, *q1;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_put_attr_lowlevel_3));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_put_attr_lowlevel_3));
  p2 = (dlong *)args(areg2); deref(p2);
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_put_attr_lowlevel_3));
  p3 = (dlong *)args(areg3); deref(p3); underef(p3);

  if (! is_atom(p2))
    goto_fail_label;

  locpc += builtin_put_attr_lowlevel_3_len;

  if (is_ref(p1))
    {
      trail(p1,bh,loctrreg);
      *p1 = (dlong)lochreg;
      *lochreg = (dlong)make_attvar_p(lochreg); lochreg++;
      *lochreg = make_list(lochreg+1); lochreg++;
      *lochreg = (dlong)p2; lochreg++;
      *lochreg = make_list(lochreg+1); lochreg++;
      *lochreg = (dlong)p3;  lochreg++;
      *lochreg = nil_atom;   lochreg++;
      goto_more_exec;
    }

  if (! is_attributed(p1))
    goto_fail_label;

  p1 = get_attvar_pointer(p1);
  p1++;

  q = p1; deref(q);
  while (is_list(q))
    {
      q = get_list_pointer(q);
      q1 = q; deref(q1);
      if (q1 == p2)
	{
	  /* we found the attribute
	     overwrite next element of the list */
	  q++; deref(q); q = get_list_pointer(q);
	  noye_trail(q,bh,loctrreg,p3);
	  goto_more_exec;
	}
      q++; deref(q); q = get_list_pointer(q); q++; deref(q);
    }

  /* extend attribute list with this new attribute */
  q = p1; deref(q);
  q1 = (dlong *)(make_list(lochreg));
  *lochreg = (dlong)p2;  lochreg++;
  *lochreg = make_list(lochreg+1);  lochreg++;
  *lochreg = (dlong)p3;  lochreg++;
  *lochreg = (dlong)q;  lochreg++;
  noye_trail(p1,bh,loctrreg,q1);
  goto_more_exec;
}

hcase(builtin_get_attr_lowlevel_2)
builtin_get_attr_lowlevel_2_label:
profile_builtin(builtin_get_attr_lowlevel_2);
{
  
  dlong areg1;
  dlong areg2;
  dlong *p1, *p2;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_get_attr_lowlevel_2));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_get_attr_lowlevel_2));
  p2 = (dlong *)args(areg2); deref(p2);

  if (! is_attributed(p1))
    goto_fail_label;
  if (! is_ref(p2))
    goto_fail_label;
  p1 = get_attvar_pointer(p1);
  p1++; deref(p1); underef(p1);

  trail(p2,bh,loctrreg);
  *p2 = (dlong)p1;
  
  locpc += builtin_get_attr_lowlevel_2_len;
  goto_more_exec;
}

hcase(builtin_get_attr_lowlevel_3)
builtin_get_attr_lowlevel_3_label:
profile_builtin(builtin_get_attr_lowlevel_3);
{
  
  dlong areg1;
  dlong areg2;
  dlong areg3;
  dlong *p1, *p2, *p3, *q, *q1;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_get_attr_lowlevel_3));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_get_attr_lowlevel_3));
  p2 = (dlong *)args(areg2); deref(p2);
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_get_attr_lowlevel_3));
  p3 = (dlong *)args(areg3); deref(p3);

  if ((! is_attributed(p1)) || (! is_atom(p2) || (!is_ref(p3))))
    goto_fail_label;

  locpc += builtin_get_attr_lowlevel_3_len;

  p1 = get_attvar_pointer(p1);
  p1++;

  q = p1; deref(q);
  while (is_list(q))
    {
      q = get_list_pointer(q);
      q1 = q; deref(q1);
      if (q1 == p2)
	{
	  /* we found the attribute */
	  q++; deref(q); q = get_list_pointer(q); deref(q); underef(q);
	  trail(p3,bh,loctrreg);
	  *p3 = (dlong)q;
	  goto_more_exec;
	}
      q++; deref(q); q = get_list_pointer(q); q++; deref(q);
    }

  goto_fail_label;
}

hcase(builtin_bind_attvar_2)
builtin_bind_attvar_2_label:
profile_builtin(builtin_bind_attvar_2);
{
  dlong areg1, areg2;
  dlong *p1, *p2;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_bind_attvar_2));
  p1 = (dlong *)args(areg1); deref(p1);

  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_bind_attvar_2));
  p2 = (dlong *)args(areg2); deref(p2);

  if (! is_attributed(p1))
    {
      if (! unify_terms(p1,p2,&locmach))
        goto_fail_label;
    }
  else
    {
      p1 = get_attvar_pointer(p1);
      value_trail(p1,bh,trreg);
      underef(p2);
      *p1 = (dlong)p2;
    }

  locpc += builtin_bind_attvar_2_len;
  goto_more_exec;
}

hcase(builtin_term_variables_3)
builtin_term_variables_3_label:
profile_builtin(builtin_term_variables_3);
{
  dlong areg1, areg2, areg3, len, spare_heap;
  dlong *p1, *p2, *p3;
  
 redo_after_gc:

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_term_variables_3));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_term_variables_3));
  p2 = (dlong *)args(areg2); deref(p2);
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_term_variables_3));
  p3 = (dlong *)args(areg3); deref(p3);

  len = varlist_init(p1, &locmach);
  spare_heap = 2*len+1;
  if (lochreg+spare_heap > heap_limit)
    {
      macro_deal_with_heap_overflow(3,2*len+1);
      goto redo_after_gc;
    }
  varlist_final(lochreg,p3, &locmach);
  p1 = lochreg;
  lochreg += spare_heap;
  locpc += builtin_term_variables_3_len;
  if (is_ref(p2))
    {
      trail(p2,bh,loctrreg);
      *p2 = (dlong)p1;
      goto_more_exec;
    }

  if (unify_terms(p2,p1,&locmach))
    goto_more_exec;
  goto_fail_label;
}

hcase(builtin_all_term_variables_3)
builtin_all_term_variables_3_label:
profile_builtin(builtin_all_term_variables_3);
{
  dlong areg1, areg2, areg3, len, spare_heap;
  dlong *p1, *p2, *p3;
  
 redo_after_gc_all:

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_all_term_variables_3));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_all_term_variables_3));
  p2 = (dlong *)args(areg2); deref(p2);
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_all_term_variables_3));
  p3 = (dlong *)args(areg3); deref(p3);

  len = all_varlist_init(p1, &locmach);
  spare_heap = 2*len+1;
  if (lochreg+spare_heap > heap_limit)
    {
      macro_deal_with_heap_overflow(3,2*len+1);
      goto redo_after_gc_all;
    }
  all_varlist_final(lochreg,p3, &locmach);
  p1 = lochreg;
  lochreg += spare_heap;
  locpc += builtin_all_term_variables_3_len;
  if (is_ref(p2))
    {
      trail(p2,bh,loctrreg);
      *p2 = (dlong)p1;
      goto_more_exec;
    }

  if (unify_terms(p2,p1,&locmach))
    goto_more_exec;
  goto_fail_label;
}


hcase(builtin_set_copy_heap_barrier_1)
builtin_set_copy_heap_barrier_1_label:
profile(builtin_set_copy_heap_barrier_1);
{

  dlong areg1;
  dlong *p1;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_set_copy_heap_barrier_1));
  p1 = (dlong *)args(areg1); deref(p1);
  locpc += builtin_set_copy_heap_barrier_1_len;
  areg1 = get_smallint(p1);
  if (areg1)
    locmach.copy_heapbarrier = areg1 + locmach.begin_heap;
  else
    locmach.copy_heapbarrier = 0;

  goto_more_exec;
}

hcase(builtin_current_heap_1)
builtin_current_heap_1_label:
profile(builtin_current_heap_1);
{

  dlong areg1;
  dlong *p1;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_current_heap_1));
  p1 = (dlong *)args(areg1); deref(p1);
  locpc += builtin_current_heap_1_len;
  *p1 = make_smallint((lochreg - locmach.begin_heap));
  
  goto_more_exec;
}

hcase(builtin_findall_get_solutions_3)
builtin_findall_get_solutions_3_label:
profile_builtin(builtin_findall_get_solutions_3);
{
  dlong a1, a2, a3;
  dlong *p;


  a3 = *(areg_type *)(arg_place(locpc,3,builtin_findall_get_solutions_3));
  p = (dlong *)args(a3); deref(p);
  a3 = get_smallint(p);
  a1 = get_findall_size(a3, &locmach);

  if (lochreg + a1 > heap_limit)
    macro_deal_with_heap_overflow(3,a1);
  
  a1 = *(areg_type *)(arg_place(locpc,1,builtin_findall_get_solutions_3));
  a2 = *(areg_type *)(arg_place(locpc,2,builtin_findall_get_solutions_3));
  locpc += builtin_findall_get_solutions_3_len;
  
  if (findall_get_solutions(a1,a2,a3,&locmach,1))
    goto_more_exec;
  goto_fail_label;
}

hcase(builtin_findall_add_2)
builtin_findall_add_2_label:
profile_builtin(builtin_findall_add_2);
{
  dlong a1, a2;

  a1 = *(areg_type *)(arg_place(locpc,1,builtin_findall_add_2));
  a2 = *(areg_type *)(arg_place(locpc,2,builtin_findall_add_2));

  locpc += builtin_findall_add_2_len;

  if (findall_add(a1,a2,&locmach))
    //    goto_more_exec;
  goto_fail_label;
}

hcase(builtin_findall_init_2)
builtin_findall_init_2_label:
profile_builtin(builtin_findall_init_2);
{
  dlong i1, i2, *p;
  dlong q;

  i2 = *(areg_type*)(arg_place(locpc,2,builtin_findall_init_2));
  p = (dlong *)args(i2);
  deref(p); /* trust it is called with integer arg */
  i2 = get_smallint(p);

  i1 = *(areg_type*)(arg_place(locpc,1,builtin_findall_init_2));
  p = (dlong *)args(i1);
  deref(p); /* trust it is called with free variable */

  locpc += builtin_findall_init_2_len;

  if (i2) q = make_cut_point(locmach.begin_cp,locbreg); else q = 0;

  i1 = findall_init_c(q,&locmach);
  trail(p,bh,loctrreg);
  build_smallint(p,i1);
  goto_more_exec;
}

hcase(builtin_internal_prolog_flag_3)
builtin_internal_prolog_flag_3_label:
profile_builtin(builtin_internal_prolog_flag_3);
{
  dlong areg1, areg2, areg3;
  dlong *p1,*p2,*p3;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_internal_prolog_flag_3));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_internal_prolog_flag_3));
  p2 = (dlong *)args(areg2); deref(p2);
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_internal_prolog_flag_3));
  p3 = (dlong *)args(areg3); deref(p3);
  if (is_ref(p2))
    trail(p2,bh,loctrreg);
  if ((p2 != p3) && (is_ref(p3)))
    trail(p3,bh,loctrreg);
  
  if (! Cprolog_flag(p1,p2,p3,&locmach))
    goto_fail_label;
  locpc += builtin_internal_prolog_flag_3_len;
  goto_more_exec;
}


hcase(builtin_ilist_to_string_2)
builtin_ilist_to_string_2_label:
profile(builtin_ilist_to_string_2);
{
  dlong *pilist, *pstring;
  dlong len;
  
 tryagain_ilist2string:
  pilist = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_ilist_to_string_2)));
  pstring = (dlong *)args(*(areg_type*)(arg_place(locpc,2,builtin_ilist_to_string_2)));
  deref(pilist); deref(pstring);
  if (! is_ref(pstring)) goto_fail_label;
  len = get_list_length(pilist,lochreg-locmach.begin_heap);
  if (len < 0) goto_fail_label;
  if ((lochreg + len) > heap_limit)  /* actually, (len+7)>>2 is enough */
    {
      macro_deal_with_heap_overflow(2,len);
      goto tryagain_ilist2string;
    }
  locpc += builtin_ilist_to_string_2_len;
  trail(pstring,bh,loctrreg);
  build_string_header(len,pstring,lochreg);
  {
    char *pc = (char *)lochreg;
    dlong *p;
    dlong i = sizeof(dlong);
    while (len)
      { 
	pilist = p = get_list_pointer(pilist);
	deref(p);
	*pc++ = get_smallint(p);
	pilist++; deref(pilist);
	i--;
	if (i == 0)
	  {
	    lochreg++;
	    i = sizeof(dlong);
	  }
	len--;
      }
    if (i < (dlong)sizeof(dlong))
      {
	while (i--) *pc++ = 0;
	lochreg++;
      }
  }
  goto_more_exec;
}

hcase(builtin_new_symbol_1)
builtin_new_symbol_1_label:
profile(builtin_new_symbol_1);
{
  dlong areg1;
  dlong *p1;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_new_symbol_1));
  p1 = (dlong *)args(areg1);
  deref(p1);

  if (! is_ref(p1)) goto_fail_label;
  locpc += builtin_new_symbol_1_len;

  trail(p1,bh,loctrreg);
  *p1 = make_atom(new_symbol());
  goto_more_exec;
}

hcase(builtin_string_to_ilist_2)
builtin_string_to_ilist_2_label:
profile(builtin_string_to_ilist_2);
{
  dlong *pilist, *pstring;
  dlong len;
  char *pc;

 tryagain_string2ilist:
  pstring = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_string_to_ilist_2)));
  pilist = (dlong *)args(*(areg_type*)(arg_place(locpc,2,builtin_string_to_ilist_2)));
  deref(pstring); deref(pilist);
  if (! is_ref(pilist)) goto_fail_label;
  if (! is_string(pstring)) goto_fail_label;
  len = get_string_length(pstring);
  if ((lochreg + 2*len) > heap_limit)
    {
      macro_deal_with_heap_overflow(2,2*len);
      goto tryagain_string2ilist;
    }
  locpc += builtin_string_to_ilist_2_len;
  pc = get_string_val(pstring);
  trail(pilist,bh,loctrreg);
  if (len == 0)
    { *pilist = nil_atom; goto_more_exec;}
  
  *pilist = make_list(lochreg);
  while (len--)
    {
      dlong *q;
      q = lochreg + 2;
      build_smallint(lochreg,*pc);
      lochreg++;
      *lochreg = make_list(q);
      lochreg = q;
      pc++;
    }
  *(lochreg-1) = nil_atom;
  goto_more_exec;
}

hcase(builtin_string_1)
builtin_string_1_label:
profile(builtin_string_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_string_1)));
  locpc += builtin_string_1_len;
  deref(p);
  if (! is_string(p))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_string_length_2)
builtin_string_length_2_label:
profile(builtin_string_length_2);
{
  dlong *pint, *pstring;
  dlong len;

  pstring = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_string_length_2)));
  pint = (dlong *)args(*(areg_type*)(arg_place(locpc,2,builtin_string_length_2)));
  locpc += builtin_string_length_2_len;
  deref(pstring); deref(pint);
  if (!is_string(pstring) || !is_ref(pint))
    goto error_string_length_2;
  len = get_string_length(pstring);
  trail(pint,bh,loctrreg);
  build_smallint(pint,len);
  goto_more_exec;
}

hcase(bld_string)
bld_string_label:
profile(bld_string);
{
  dlong str_len1;
  char *dstring2;

  str_len1 = *(str_len*)(arg_place(locpc,1,bld_string));
  dstring2 = (char *)(arg_place(locpc,2,bld_string));

  push_string(str_len1, lochreg, dstring2);

  locpc += add_str_len(str_len1);
  locpc += bld_string_len;
  goto_more_exec;
}

hcase(get_string)
get_string_label:
profile(get_string);
{
  dlong *p1;
  dlong str_len2;
  char *dstring3;

  p1 = (dlong *)(args(*(areg_type*)(arg_place(locpc,1,get_string))));
  str_len2 = *(str_len*)(arg_place(locpc,2,get_string));
  dstring3 = (char *)(arg_place(locpc,3,get_string));
  locpc += add_str_len(str_len2);
  locpc += get_string_len;
  deref2(p1,goto get_string_isvar);
  if (is_string(p1))
    {
      dlong len1 = get_string_length(p1);
      char *s1 = get_string_val(p1);
      if (len1 != str_len2) goto_fail_label;
      if (! equal_strings(len1,s1,dstring3)) goto_fail_label;
      goto_more_exec;
    }

  if (! is_attributed(p1))
    goto_fail_label;

  {
    dlong q;
    build_string(str_len2, &q, lochreg, dstring3);
    safe_wakeup(p1,(dlong *)q,&locmach);
    goto_more_exec;
  }
  

 get_string_isvar:
  trail(p1,bh,loctrreg);
  build_string(str_len2, p1, lochreg, dstring3);  
  goto_more_exec;
}

hcase(put_string)
put_string_label:
profile(put_string);
{
  dlong areg1;
  dlong str_len2;
  char *dstring3;

  areg1 = *(areg_type*)(arg_place(locpc,1,put_string));
  str_len2 = *(str_len*)(arg_place(locpc,2,put_string));
  dstring3 = (char *)(arg_place(locpc,3,put_string));

  build_string(str_len2, &(args(areg1)), lochreg, dstring3);
  locpc += add_str_len(str_len2);
  locpc += put_string_len;
  goto_more_exec;
}

hcase(get_stringv)
get_stringv_label:
profile(get_stringv);
{
  dlong *p1;
  dlong str_len2;
  char *dstring3;

  p1 = (dlong *)(ereg[*(yvar_type*)(arg_place(locpc,1,get_stringv))]);
  str_len2 = *(str_len*)(arg_place(locpc,2,get_stringv));
  dstring3 = (char *)(arg_place(locpc,3,get_stringv));
  locpc += add_str_len(str_len2);
  locpc += get_stringv_len;
  deref2(p1,goto get_stringv_isvar);
  if (is_string(p1))
    {
      dlong len1 = get_string_length(p1);
      char *s1 = get_string_val(p1);
      if (len1 != str_len2) goto_fail_label;
      if (! equal_strings(len1,s1,dstring3)) goto_fail_label;
      goto_more_exec;
    }

  if (! is_attributed(p1))
    goto_fail_label;

  {
    dlong q;
    build_string(str_len2, &q, lochreg, dstring3);
    safe_wakeup(p1,(dlong *)q,&locmach);
    goto_more_exec;
  }
  

 get_stringv_isvar:
  trail(p1,bh,loctrreg);
  build_string(str_len2, p1, lochreg, dstring3);  
  goto_more_exec;
}

hcase(put_stringv)
put_stringv_label:
profile(put_stringv);
{
  dlong yvar1;
  dlong str_len2;
  char *dstring3;

  yvar1 = *(yvar_type*)(arg_place(locpc,1,put_stringv));
  str_len2 = *(str_len*)(arg_place(locpc,2,put_stringv));
  dstring3 = (char *)(arg_place(locpc,3,put_stringv));

  build_string(str_len2, &(ereg[yvar1]), lochreg, dstring3);
  locpc += add_str_len(str_len2);
  locpc += put_stringv_len;
  goto_more_exec;
}

hcase(unify_string_untagged)
unify_string_untagged_label:
profile(unify_string_untagged);
{
  dlong offset;
  dlong str_len2;
  char *dstring3;

  offset = *(dlong *)(arg_place(locpc,1,unify_string_untagged));
  str_len2 = *(str_len*)(arg_place(locpc,2,unify_string_untagged));
  dstring3 = (char *)(arg_place(locpc,3,unify_string_untagged));
  locpc += add_str_len(str_len2);
  locpc += unify_string_untagged_len;

  if (WAMS)
    {
      dlong *p,q;
      p = WAMS++;
      deref2(p,goto unify_string_untagged_ref);
      if (is_string(p))
	{
	  dlong len = get_string_length(p);
	  char *s = get_string_val(p);
	  if (len != str_len2) goto_fail_label;
	  if (! equal_strings(len,s,dstring3)) goto_fail_label;
	  goto_more_exec;
	}

      if (! is_attributed(p))
	goto_fail_label;

      build_string(str_len2, &q, lochreg, dstring3);
      safe_wakeup(p,(dlong *)q,&locmach);
      goto_more_exec;

    unify_string_untagged_ref:
      trail(p,bh,loctrreg);
      build_string(str_len2, p, lochreg, dstring3);  
      goto_more_exec;
    }

  *lochreg = make_string_pointer((lochreg-offset));
  *(lochreg-offset) = make_string_header(str_len2);
  move_string(str_len2, dstring3, (char *)(lochreg-offset+1));
  lochreg++;
  goto_more_exec;
}

hcase(put_char)
put_char_label:
profile(put_char);
{
  dlong areg1;
  dlong dchar2;

  areg1 = *(areg_type*)(arg_place(locpc,1,put_char));
  dchar2 = *(dchar*)(arg_place(locpc,2,put_char));
  locpc += put_char_len;
  args(areg1) = dchar2;
  goto_more_exec;
}

hcase(put_bigint)
put_bigint_label:
profile(put_bigint);
{
  dlong areg1;
  dlong bigintlen;
  dlong *p, *p3;

  areg1 = *(areg_type*)(arg_place(locpc,1,put_bigint));
  bigintlen = *(dlong *)(arg_place(locpc,2,put_bigint));
  p3 = (dlong *)(arg_place(locpc,3,put_bigint));
  
  p = lochreg;
  args(areg1) = make_bigint_pointer(p);
  *p++ = BIGINT_HEADER;
  if (bigintlen < 0)
    {
      bigintlen = -bigintlen;
      *p = bigint_info_onheap(0,bigintlen);
    }
  else
    *p = bigint_info_onheap(1,bigintlen);
  p++;
  while (bigintlen--)
    *p++ = *p3++;
  lochreg = p;

  locpc = (codep)p3;
  goto_more_exec;
}

hcase(put_bigintv)
put_bigintv_label:
profile(put_bigintv);
{
  dlong yvar1;
  dlong bigintlen;
  dlong *p, *p3;

  yvar1 = *(yvar_type *)(arg_place(locpc,1,put_bigintv));
  bigintlen = *(dlong *)(arg_place(locpc,2,put_bigintv));
  p3 = (dlong *)(arg_place(locpc,3,put_bigintv));
  
  p = lochreg;
  ereg[yvar1] = make_bigint_pointer(p);
  *p++ = BIGINT_HEADER;
  if (bigintlen < 0)
    {
      bigintlen = -bigintlen;
      *p = bigint_info_onheap(0,bigintlen);
    }
  else
    *p = bigint_info_onheap(1,bigintlen);
  p++;
  while (bigintlen--)
    *p++ = *p3++;
  lochreg = p;

  locpc = (codep)p3;
  goto_more_exec;
}

hcase(builtin_char_to_int_2)
builtin_char_to_int_2_label:
profile(builtin_char_to_int_2);
{
  dlong *pint, *pchar;
  dlong i;

  pchar = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_char_to_int_2)));
  pint = (dlong *)args(*(areg_type*)(arg_place(locpc,2,builtin_char_to_int_2)));
  deref(pchar) ; deref(pint);
  locpc += builtin_char_to_int_2_len;
  if (! is_ref(pint) || ! is_char(pchar))
    goto_fail_label;
  i = get_character(pchar);
  trail(pint,bh,loctrreg);
  build_smallint(pint,i);
  goto_more_exec;
}

hcase(builtin_int_to_char_2)
builtin_int_to_char_2_label:
profile(builtin_int_to_char_2);
{
  dlong *pint, *pchar;
  dlong i;
  
  pint = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_int_to_char_2)));
  pchar = (dlong *)args(*(areg_type*)(arg_place(locpc,2,builtin_int_to_char_2)));
  deref(pchar) ; deref(pint);
  locpc += builtin_int_to_char_2_len;
  if (! is_ref(pchar) || ! is_smallint(pint))
    goto_fail_label;
  i = get_smallint(pint);
  trail(pchar,bh,loctrreg);
  *pchar = make_character(i);
  goto_more_exec;
}

hcase(builtin_char_1)
builtin_char_1_label:
profile(builtin_char_1);
{
  dlong *p;

  p = (dlong *)args(*(areg_type*)(arg_place(locpc,1,builtin_char_1)));
  locpc += builtin_char_1_len;
  deref(p);
  if (! is_char(p))
    goto_fail_label;
  goto_more_exec;
}

hcase(builtin_atom_index_2)
builtin_atom_index_2_label:
profile(builtin_atom_index_2);
{
  dlong areg1, areg2;
  dlong *p1, *p2;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_atom_index_2));
  p1 = (dlong *)args(areg1); deref(p1);
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_atom_index_2));
  p2 = (dlong *)args(areg2); deref(p2);

  locpc += builtin_atom_index_2_len;
  if (is_atom(p1) && is_ref(p2))
    {
      trail(p2,bh,loctrreg);
      *p2 = make_smallint(get_atom_index(p1));
      goto_more_exec;
    }
  goto error_atom_index_2;
}

hcase(builtin_findall_cleanup_1)
builtin_findall_cleanup_1_label:
profile_builtin(builtin_findall_cleanup_1);
{
  dlong i, *p;

  i = *(areg_type*)(arg_place(locpc,1,builtin_findall_cleanup_1));
  p = (dlong *)args(i);
  deref(p);
  i = get_smallint(p); /* trust it is called with an integer */
  locpc += builtin_findall_cleanup_1_len;
  findall_free(i, NULL, &locmach);
  goto_more_exec;
}

hcase(builtin_print_heap_0)
builtin_print_heap_0_label:
profile(builtin_print_heap_0);
{
  print_heap(0, &locmach);
  locpc += builtin_print_heap_0_len;
  goto_more_exec;
}

hcase(builtin_call_10)
builtin_call_10_label:
profile_builtin(builtin_call_10);
{
  glob_arity = 9;
  goto deal_with_extendible_call;
}

hcase(builtin_call_9)
builtin_call_9_label:
profile_builtin(builtin_call_9);
{
  glob_arity = 8;
  goto deal_with_extendible_call;
}

hcase(builtin_call_8)
builtin_call_8_label:
profile_builtin(builtin_call_8);
{
  glob_arity = 7;
  goto deal_with_extendible_call;
}

hcase(builtin_call_7)
builtin_call_7_label:
profile_builtin(builtin_call_7);
{
  glob_arity = 6;
  goto deal_with_extendible_call;
}

hcase(builtin_call_6)
builtin_call_6_label:
profile_builtin(builtin_call_6);
{
  glob_arity = 5;
  goto deal_with_extendible_call;
}

hcase(builtin_call_5)
builtin_call_5_label:
profile_builtin(builtin_call_5);
{
  glob_arity = 4;
  goto deal_with_extendible_call;
}

profile_builtin(builtin_call_4);
builtin_call_4_label:
profile_builtin(builtin_call_4);
{
  glob_arity = 3;
  goto deal_with_extendible_call;
}

hcase(builtin_call_3)
builtin_call_3_label:
profile_builtin(builtin_call_3);
{
  glob_arity = 2;
  goto deal_with_extendible_call;
}

hcase(builtin_mapcall_3)
builtin_mapcall_3_label:
profile_builtin(builtin_mapcall_3);
{
  dlong *p1,*p2,*p3;
  dlong arity;
  codep newlocpc;

  p1 = (dlong *)args(1); deref(p1);
  if (!is_struct(p1)) goto_fail_label;
  p1 = get_struct_pointer(p1);
  arity = get_funct_arity(*p1);
  if (arity < 2) goto_fail_label;
  newlocpc = main_entrypoint(get_funct_symb(*p1));
  if (! newlocpc) goto_fail_label;
  locpc = newlocpc;
  { dlong *q = locmach.Areg;

    p3 = (dlong *)args(3);
    p2 = (dlong *)args(2);
    q[arity] = (dlong)p3;
    q[arity-1] = (dlong)p2;
    arity -= 2;
    while (arity)
      {
	q[arity] = p1[arity];
	arity-- ;
      }
  }
  goto_more_exec;
}

hcase(builtin_call_2)
builtin_call_2_label:
profile_builtin(builtin_call_2);
{
  glob_arity = 1;
  goto deal_with_extendible_call;
}

hcase(builtin_disable_cp_1)
builtin_disable_cp_1_label:
profile_builtin(builtin_disable_cp_1);
{
  dlong areg;
  dlong *p, *old_cp;

  areg = *(areg_type *)(arg_place(locpc,1,builtin_disable_cp_1));
  p = (dlong *)args(areg);
  deref(p);
  old_cp = p = get_cut_point(locmach.begin_cp, (dlong)p);
  locpc += builtin_disable_cp_1_len;
  if (old_cp == locbreg)
    {
      /* we cut away one choicepoint */
      locbreg = ((CP_P)locbreg)->b;
      freeze_heap();
      bh = ((CP_P)locbreg)->h;
    }
  else
    ((struct choicepoint *)old_cp)->failurecont = main_entrypoint(symbol_cutfail_0)
      + try_me_else_len + proceed_len;
goto_more_exec;
}

hcase(builtin_smash_cp_3)
builtin_smash_cp_3_label:
profile_builtin(builtin_smash_cp_3);
{
  dlong areg,i;
  dlong *p1, *p2, *p3;

  areg = *(areg_type *)(arg_place(locpc,1,builtin_smash_cp_3));
  p1 = (dlong *)args(areg);
  deref(p1);
  areg = *(areg_type *)(arg_place(locpc,2,builtin_smash_cp_3));
  p2 = (dlong *)args(areg);
  deref(p2);
  i = get_smallint(p2);
  p1 = get_cut_point(locmach.begin_cp, (dlong)p1);
  areg = *(areg_type *)(arg_place(locpc,3,builtin_smash_cp_3));
  p3 = (dlong *)args(areg);
  deref(p3);
  locpc += builtin_smash_cp_3_len;
  p1 += (sizeof(struct choicepoint)/sizeof(dlong *)) + i - 1;
  *p1 = (dlong)p3;
  goto_more_exec;
}

hcase(builtin_timeout_1)
builtin_timeout_1_label:
profile_builtin(builtin_timeout_1);
{
  dlong areg, *p, i;

  areg = *(areg_type *)(arg_place(locpc,1,builtin_timeout_1));
  p = (dlong *)args(areg);
  deref(p);
  i = get_smallint(p);
  locpc += builtin_timeout_1_len;
  i = (dlong)alarm((unsigned int)i);
  goto_more_exec;
}

hcase(builtin_internal_sort_2)
builtin_internal_sort_2_label:
profile_builtin(builtin_internal_sort_2);
{
  dlong *p, *q;
  dlong l;

  /* should always be called with free second arg and so that it does
     not need trailing */

  locpc += builtin_internal_sort_2_len;

  p = (dlong *)args(1);
  deref(p);
  l = 0;
  while (is_list(p))
    {
      l++;
      p = get_list_pointer(p) + 1;
      deref(p);
    }
  if (nil_atom != (dlong)p)
    goto_fail_label;
  if (l < 2)
    {
      q = (dlong *)args(2);
      deref(q);
      *q = args(1);
      goto_more_exec;
    }

  if ((lochreg + 2*l) > heap_limit)
    macro_deal_with_heap_overflow(2,2*l);

  p = lochreg;
  l = do_sort((dlong *)args(1),l,lochreg, &locmach);
  lochreg += 2*l;
  q = (dlong *)args(2);
  deref(q);
  *q = make_list(p);
  goto_more_exec;  
}

hcase(builtin_internal_key_sort_2)
builtin_internal_key_sort_2_label:
profile_builtin(builtin_internal_key_sort_2);
{
  dlong *p, *q;
  dlong l;

  /* should always be called with free second arg and so that it does
     not need trailing */

  locpc += builtin_internal_key_sort_2_len;

  p = (dlong *)args(1);
  deref(p);
  l = 0;
  while (is_list(p))
    {
      l++;
      p = get_list_pointer(p) + 1;
      deref(p);
    }
  if (nil_atom != (dlong)p)
    goto_fail_label;
  if (l < 2)
    {
      q = (dlong *)args(2);
      deref(q);
      *q = args(1);
      goto_more_exec;
    }

  if ((lochreg + 2*l) > heap_limit)
    macro_deal_with_heap_overflow(2,2*l);

  p = lochreg;
  l = do_key_sort((dlong *)args(1),l,lochreg, &locmach);
  lochreg += 2*l;
  q = (dlong *)args(2);
  deref(q);
  *q = make_list(p);
  goto_more_exec;  
}

hcase(builtin_internal_numbervars_2)
builtin_internal_numbervars_2_label:
profile_builtin(builtin_internal_numbervars_2);
{
  dlong *p, *q;
  static dlong saveTR;

  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_internal_numbervars_2)));
  q = (dlong *)args(*(areg_type *)(arg_place(locpc,2,builtin_internal_numbervars_2)));
  locpc += builtin_internal_numbervars_2_len;
  deref(p); deref(q);
  if (get_smallint(q) == 0)
    {
      dlong i = numb_symbs;
      /* instant the vars in p */
      saveTR = loctrreg - locmach.limit_trail;
      while (is_list(p))
	{
	  q = get_list_pointer(p);
	  p = q+1;
	  deref(q);
	  if (is_ref(q))
	    {
	      unconditional_value_trail(q,loctrreg);
	      *q = make_atom(i);
	      i++;
	    }
	  else
	    goto_fail_label;
	  deref(p);
	}
      goto_more_exec;
    }

  /* uninstant the vars */
  p = locmach.limit_trail + saveTR;
  untrail(loctrreg,p);
  goto_more_exec;  
}

hcase(builtin_garbage_collect_0)
builtin_garbage_collect_0_label:
profile_builtin(builtin_garbage_collect_0);
{
  macro_deal_with_heap_overflow(0,SPARE_HEAP);
  locpc += builtin_garbage_collect_0_len;
  goto_more_exec;
}

hcase(builtin_cinter_4)
builtin_cinter_4_label:
profile_builtin(builtin_cinter_4);
{
  dlong res;
  char *mes;

  locpc += builtin_cinter_4_len;
  if (not_enough_space_for_cinter())
    {
      locmach.P = locpc; locmach.CONTP = contpreg;
      res = code_gc(0, &locmach);
      res = 1;
      if (not_enough_space_for_cinter())
	res = code_gc(1, &locmach); /* expand */
      locpc = locmach.P; contpreg = locmach.CONTP;
      if (! res)
	goto_fail_label;
    }
  res = builtin_2C(&mes, &locmach);
  goto_more_exec;
}


hcase(builtin_argv_2)
builtin_argv_2_label:
profile_builtin(builtin_argv_2);
{
  dlong areg1, areg2, i;
  dlong *p1, *p2;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_argv_2));
  p1 = (dlong *)args(areg1); deref(p1);

  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_argv_2));
  p2 = (dlong *)args(areg2); deref(p2);

  locpc += builtin_argv_2_len;

  if ((!is_smallint(p1)) || (! is_ref(p2)))
    goto error_argv_2;

  i = get_smallint(p1);
  if ((i < 0) || (i + glob_min_argc > glob_max_argc))
    goto_fail_label;

  trail(p2,bh,loctrreg);
  *p2 = make_atom(sym_lookup(0,glob_argv[i + glob_min_argc],USER_MOD));

  goto_more_exec;
}

hcase(builtin_codegc_0)
builtin_codegc_0_label:
profile_builtin(builtin_codegc_0);
{
  dlong ignore_result;

  locmach.P = locpc; locmach.CONTP = contpreg;
  ignore_result = code_gc(0, &locmach); /* ignore return value */
  locpc = locmach.P; contpreg = locmach.CONTP;
  locpc += builtin_codegc_0_len;
  goto_more_exec;
}

hcase(builtin_print_code_1)
builtin_print_code_1_label:
profile_builtin(builtin_print_code_1);
{
  dlong areg1;
  areg1 = *(areg_type *)(arg_place(locpc,1,builtin_print_code_1));
  locpc += builtin_print_code_1_len;
  print_code_for_pred((dlong *)args(areg1));
  goto_more_exec;
}

hcase(builtin_count_call_2)
builtin_count_call_2_label:
profile_builtin(builtin_count_call_2);
{
  dlong areg, index;
  dlong *p1, *p2;
  areg = *(areg_type *)(arg_place(locpc,1,builtin_count_call_2));
  p1 = (dlong *)args(areg); deref(p1);
  areg = *(areg_type *)(arg_place(locpc,2,builtin_count_call_2));
  p2 = (dlong *)args(areg); deref(p2);
  locpc += builtin_count_call_2_len;
  if (! is_ref(p2))
    goto_fail_label;
  if (is_atom(p1))
    index = get_atom_index(p1);
  else
    if (is_struct(p1))
      index = get_funct_symb(*get_struct_pointer(p1));
  else
    goto_fail_label;
#ifdef PROFILING_CALLS
  index = symtab[index].callcount;
#else
  index = 0;
#endif
  trail(p2,bh,loctrreg);
  build_smallint(p2,index);
  goto_more_exec;
}

hcase(nop)
nop_label:
profile(nop);
{
  locpc += nop_len;
  goto_more_exec;
}

hcase(builtin_assign_areg_2)
hcase(builtin_drop_areg_2)
hcase(active_yvar)
builtin_assign_areg_2_label:
builtin_drop_areg_2_label:
active_yvar_label:
{
  fatal_message("active_yvar/assign_areg/drop_areg is impossible");
}



hcase(check_free_heap_head)
check_free_heap_head_label:
profile(check_free_heap_head);
{
  dlong size = *(dlong *) arg_place(locpc,2,check_free_heap_head);
  locpc += check_free_heap_head_len;

  check_cancellation();

  if (lochreg + size> heap_limit)
    goto deal_with_heap_overflow_check_head;
  goto_more_exec;
}

hcase(check_free_heap_body)
check_free_heap_body_label:
profile(check_free_heap_body);
{
  dlong size = *(dlong *) arg_place(locpc,1,check_free_heap_body);
  locpc += check_free_heap_body_len;

  check_cancellation();

  if (lochreg + size> heap_limit)
    goto deal_with_heap_overflow_check_body;
  goto_more_exec;
}

hcase(initperm)
initperm_label:
profile(initperm);
{
  dlong d, yvar;
  
  d = *(dlong *)(arg_place(locpc,1,initperm));
  locpc += initperm_len;

  yvar = d & 0xff; d = d >> 8;
  if (yvar) { *lochreg = ereg[yvar] = (dlong)lochreg; lochreg++; }
  
  yvar = d & 0xff; d = d >> 8;
  if (yvar) { *lochreg = ereg[yvar] = (dlong)lochreg; lochreg++; }
  
  yvar = d & 0xff; d = d >> 8;
  if (yvar) { *lochreg = ereg[yvar] = (dlong)lochreg; lochreg++; }
  
  yvar = d & 0xff;
  if (yvar) { *lochreg = ereg[yvar] = (dlong)lochreg; lochreg++; }
  
  goto_more_exec;
}


hcase(select_functor4)
select_functor4_label:
profile(select_functor4);
{
  dlong label2;
  dlong struct1, incomingstruct;
  dlong *p;

  p = (dlong *)args(1);
  deref2(p,goto select_functor4_var);
  if (is_attributed(p)) goto select_functor4_var;
  if (is_struct(p))
    incomingstruct = *get_struct_pointer(p);
  else
    incomingstruct = (dlong)p;

  struct1 = *(dlong *)(arg_place(locpc,1,select_functor4));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,select_functor4)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,3,select_functor4));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,4,select_functor4)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,5,select_functor4));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,6,select_functor4)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,7,select_functor4));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,8,select_functor4)));
      goto_more_exec;
    }
  locpc = (codep)(*(dlong *)(arg_place(locpc,10,select_functor4)));
  if (locpc) goto_more_exec;
  goto_fail_label;

 select_functor4_var:
  locpc = (codep)(*(dlong *)(arg_place(locpc,9,select_functor4)));
  goto_more_exec;
}

hcase(select_functor5)
select_functor5_label:
profile(select_functor5);
{
  dlong label2;
  dlong struct1, incomingstruct;
  dlong *p;

  p = (dlong *)args(1);
  deref2(p,goto select_functor5_var);
  if (is_attributed(p)) goto select_functor5_var;
  if (is_struct(p))
    incomingstruct = *get_struct_pointer(p);
  else
    incomingstruct = (dlong)p;

  struct1 = *(dlong *)(arg_place(locpc,1,select_functor5));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,2,select_functor5)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,3,select_functor5));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,4,select_functor5)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,5,select_functor5));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,6,select_functor5)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,7,select_functor5));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,8,select_functor5)));
      goto_more_exec;
    }
  struct1 = *(dlong *)(arg_place(locpc,9,select_functor5));
  if (incomingstruct == struct1)
    {
      locpc = (codep)(*(dlong *)(arg_place(locpc,10,select_functor5)));
      goto_more_exec;
    }
  locpc = (codep)(*(dlong *)(arg_place(locpc,12,select_functor5)));
  if (locpc) goto_more_exec;
  goto_fail_label;

 select_functor5_var:
  locpc = (codep)(*(dlong *)(arg_place(locpc,11,select_functor5)));
  goto_more_exec;
}

hcase(test_equal_float)
test_equal_float_label:
profile_builtin(test_equal_float);
{
  dlong areg1;
  dfloat f1, f2;
  codep label3;
  dlong *p1, *p2;
  
  areg1 = *(areg_type *)(arg_place(locpc,1,test_equal_float));
  get_double_arg(f2, arg_place(locpc,2,test_equal_float));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_equal_float)));
  locpc += test_equal_float_len;
  
  p1 = (dlong *)args(areg1); deref(p1);
  
  if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_equal_float;
  if (f1 == f2)
    goto_more_exec;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_inequal_float)
test_inequal_float_label:
profile_builtin(test_inequal_float);
{
  dlong areg1;
  dfloat f1, f2;
  codep label3;
  dlong *p1, *p2;
  
  areg1 = *(areg_type *)(arg_place(locpc,1,test_inequal_float));
  get_double_arg(f2, arg_place(locpc,2,test_inequal_float));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_inequal_float)));
  locpc += test_inequal_float_len;
  
  p1 = (dlong *)args(areg1); deref(p1);
  
  if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_inequal_float;
  if (f1 != f2)
    goto_more_exec;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_larger_float)
test_larger_float_label:
profile_builtin(test_larger_float);
{
  dlong areg1;
  dfloat f1, f2;
  codep label3;
  dlong *p1, *p2;
  
  areg1 = *(areg_type *)(arg_place(locpc,1,test_larger_float));
  get_double_arg(f2, arg_place(locpc,2,test_larger_float));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_larger_float)));
  locpc += test_larger_float_len;
  
  p1 = (dlong *)args(areg1); deref(p1);
  
  if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_larger_float;
  if (f1 > f2)
    goto_more_exec;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_larger_or_equal_float)
test_larger_or_equal_float_label:
profile_builtin(test_larger_or_equal_float);
{
  dlong areg1;
  dfloat f1, f2;
  codep label3;
  dlong *p1, *p2;
  
  areg1 = *(areg_type *)(arg_place(locpc,1,test_larger_or_equal_float));
  get_double_arg(f2, arg_place(locpc,2,test_larger_or_equal_float));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_larger_or_equal_float)));
  locpc += test_larger_or_equal_float_len;
  
  p1 = (dlong *)args(areg1); deref(p1);
  
  if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_larger_or_equal_float;
  if (f1 >= f2)
    goto_more_exec;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_smaller_float)
test_smaller_float_label:
profile_builtin(test_smaller_float);
{
  dlong areg1;
  dfloat f1, f2;
  codep label3;
  dlong *p1, *p2;
  
  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_float));
  get_double_arg(f2, arg_place(locpc,2,test_smaller_float));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_smaller_float)));
  locpc += test_smaller_float_len;
  
  p1 = (dlong *)args(areg1); deref(p1);
  
  if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_smaller_float;
  if (f1 < f2)
    goto_more_exec;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_smaller_or_equal_float)
test_smaller_or_equal_float_label:
profile_builtin(test_smaller_or_equal_float);
{
  dlong areg1;
  dfloat f1, f2;
  codep label3;
  dlong *p1;
  
  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_or_equal_float));
  get_double_arg(f2, arg_place(locpc,2,test_smaller_or_equal_float));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_smaller_or_equal_float)));
  locpc += test_smaller_or_equal_float_len;
  
  p1 = (dlong *)args(areg1); deref(p1);
  
  if (!get_float_from_number(p1,&f1,&locmach)) goto arith_error_test_smaller_or_equal_float;
  if (f1 <= f2)
    goto_more_exec;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_equal_int)
test_equal_int_label:
profile_builtin(test_equal_int);
{
  dlong areg1;
  dlong int2;
  codep label3;
  dlong *p1;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_equal_int));
  int2 = *(dlong *)(arg_place(locpc,2,test_equal_int));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_equal_int)));
  locpc += test_equal_int_len;

  p1 = (dlong *)args(areg1); deref(p1);

  if (is_smallint(p1))
    {
      if (get_smallint(p1) == int2) goto_more_exec;
    } 
  else 
    if (is_real(p1))
      {
	if (get_real(p1) == int2) goto_more_exec;
      }
    else 
      if (! is_bigint(p1))
	goto error_test_equal_int;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_inequal_int)
test_inequal_int_label:
profile_builtin(test_inequal_int);
{
  dlong areg1;
  dlong int2;
  codep label3;
  dlong *p1;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_inequal_int));
  int2 = *(dlong *)(arg_place(locpc,2,test_inequal_int));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_inequal_int)));
  locpc += test_inequal_int_len;

  p1 = (dlong *)args(areg1); deref(p1);

  if (is_smallint(p1))
    {
      if (get_smallint(p1) != int2) goto_more_exec;
    } 
  else if (is_real(p1))
    {
      if (get_real(p1) != int2) goto_more_exec;
    }
  else if (is_bigint(p1))
    {
      goto_more_exec;
    }
  else goto error_test_inequal_int;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_larger_int)
test_larger_int_label:
profile_builtin(test_larger_int);
{
  dlong areg1;
  dlong int2;
  codep label3;
  dlong *p1;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_larger_int));
  int2 = *(dlong *)(arg_place(locpc,2,test_larger_int));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_larger_int)));
  locpc += test_larger_int_len;

  p1 = (dlong *)args(areg1); deref(p1);

  if (is_smallint(p1))
    {
      if (get_smallint(p1) > int2) goto_more_exec;
    } 
  else if (is_real(p1))
    {
      if (get_real(p1) > int2) goto_more_exec;
    }
  else if (is_bigint(p1))
    {
      dlong s1;
      p1 = get_number_pointer(p1)+1;
      s1 = bigint_positive(*p1);
      if (s1) goto_more_exec;
    }
  else goto error_test_larger_int;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_larger_or_equal_int)
test_larger_or_equal_int_label:
profile_builtin(test_larger_or_equal_int);
{
  dlong areg1;
  dlong int2;
  codep label3;
  dlong *p1;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_larger_or_equal_int));
  int2 = *(dlong *)(arg_place(locpc,2,test_larger_or_equal_int));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_larger_or_equal_int)));
  locpc += test_larger_or_equal_int_len;

  p1 = (dlong *)args(areg1); deref(p1);

  if (is_smallint(p1))
    {
      if (get_smallint(p1) >= int2) goto_more_exec;
    } 
  else if (is_real(p1))
    {
      if (get_real(p1) >= int2) goto_more_exec;
    }
  else if (is_bigint(p1))
    {
      dlong s1;
      p1 = get_number_pointer(p1)+1;
      s1 = bigint_positive(*p1);
      if (s1) goto_more_exec;
    }
  else goto error_test_larger_or_equal_int;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_smaller_int)
test_smaller_int_label:
profile_builtin(test_smaller_int);
{
  dlong areg1;
  dlong int2;
  codep label3;
  dlong *p1;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_int));
  int2 = *(dlong *)(arg_place(locpc,2,test_smaller_int));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_smaller_int)));
  locpc += test_smaller_int_len;

  p1 = (dlong *)args(areg1); deref(p1);

  if (is_smallint(p1))
    {
      if (get_smallint(p1) < int2) goto_more_exec;
    } 
  else if (is_real(p1))
    {
      if (get_real(p1) < int2) goto_more_exec;
    }
  else if (is_bigint(p1))
    {
      dlong s1;
      p1 = get_number_pointer(p1)+1;
      s1 = bigint_positive(*p1);
      if (!s1) goto_more_exec;
    }
  else goto error_test_smaller_int;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(test_smaller_or_equal_int)
test_smaller_or_equal_int_label:
profile_builtin(test_smaller_or_equal_int);
{
  dlong areg1;
  dlong int2;
  codep label3;
  dlong *p1;

  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_or_equal_int));
  int2 = *(dlong *)(arg_place(locpc,2,test_smaller_or_equal_int));
  label3 = (codep)(*(label_type *)(arg_place(locpc,3,test_smaller_or_equal_int)));
  locpc += test_smaller_or_equal_int_len;

  p1 = (dlong *)args(areg1); deref(p1);

  if (is_smallint(p1))
    {
      if (get_smallint(p1) <= int2) goto_more_exec;
    } 
  else if (is_real(p1))
    {
      if (get_real(p1) <= int2) goto_more_exec;
    }
  else if (is_bigint(p1))
    {
      dlong s1;
      p1 = get_number_pointer(p1)+1;
      s1 = bigint_positive(*p1);
      if (!s1) goto_more_exec;
    }
  else goto error_test_smaller_or_equal_int;
  locpc = label3;
  if (locpc) goto_more_exec;
  goto_fail_label;
}

hcase(unify_structure_untagged)
unify_structure_untagged_label:
profile(unify_structure_untagged);
{
  dlong struct1, spare;

  struct1 = *(dlong *)(arg_place(locpc,1,unify_structure_untagged));
  spare =*(dlong *)(arg_place(locpc,2,unify_structure_untagged));
  locpc += unify_structure_untagged_len;

  if (WAMS)
    {
      dlong *p;
      p = WAMS;
      deref2(p,goto unify_structure_untagged_isvar);

      if (is_struct(p))
	{
	  p = get_struct_pointer(p);
          if (*p != struct1)
            { goto_fail_label; }
          else
            WAMS = p+1;
          goto_more_exec;
	}

      if (! is_attributed(p))
        goto_fail_label;

      WAMS = 0;
      lochreg += spare;
      safe_wakeup(p,(dlong *)make_struct_p(lochreg),&locmach);
      *lochreg = struct1;
      lochreg++;
      goto_more_exec;

    unify_structure_untagged_isvar:
      WAMS = 0;
      lochreg += spare;
      *p = make_struct_p(lochreg);
      *lochreg = struct1;
      lochreg++;
      trail(p,bh,loctrreg);
      goto_more_exec;
    }

  *lochreg = make_struct_p(lochreg+1+spare);
  lochreg+= spare+1;
  *lochreg = struct1;
  lochreg++;
  goto_more_exec;
}

hcase(append_entry)
append_entry_label:
profile(append_entry);
{
  /* sysh:append(in,?,out) */
  /* with transfer to regular append when something is fishy */

  register dlong *p1, *p3;

  p1 = (dlong *)args(1);
  p3 = (dlong *)args(3);
  deref2(p3,goto append_entry_is_ref);
  goto regular_append;

 append_entry_is_ref:

  deref2(p1,goto regular_append);

  if (is_list(p1))
    {
      trail(p3,bh,loctrreg);
      *p3 = make_list(lochreg);
      p3 = lochreg + 1;
      p1 = get_list_pointer(p1);
      p3[-1] = (dlong)p1;
      p1 = (dlong *)p1[1];
    }
  else
    if ((dlong)p1 == nil_atom)
      {
	trail(p3,bh,loctrreg);
	locpc = contpreg;
	*p3 = args(2);
	goto_more_exec;
      }
    else
      goto pre_regular_append;


  for (;;)
    {
      deref2(p1,goto pre_regular_append);
      if (is_list(p1))
	{
 	  p1 = get_list_pointer(p1);
	  *p3 = make_list((p3+1));
 	  p3 += 2;
 	  p3[-1] = (dlong)p1;
 	  p1 = (dlong *)p1[1];

 	  if (p3 > heap_limit)
 	    goto pre_regular_append;
	}
      else
	if ((dlong)p1 == nil_atom)
	  {
	    locpc = contpreg;
	    *p3 = args(2);
	    lochreg = p3+1;
	    goto_more_exec;
	  }
	else
	  goto pre_regular_append;
    }

 pre_regular_append:
  underef(p1); args(1) = (dlong)p1;
  args(3) = *p3 = (dlong)p3;
  lochreg = p3+1;
 regular_append:
  locpc += append_entry_len;
  goto_more_exec;

}

hcase(reverse_entry)
reverse_entry_label:
profile(reverse_entry);
{
  /* sysh:reverse(in,free) */
  /* with transfer to regular reverse when something is fishy */

  register dlong *p1, *p2, *heapcache, *limitcache, accu;

  accu = nil_atom;
  heapcache = lochreg;
  limitcache = heap_limit;

  p1 = (dlong *)args(1); deref(p1);
  p2 = (dlong *)args(2); deref(p2);
  if (!is_ref(p2)) goto fishy_reverse;

  while (is_list(p1))
    {
      p1 = get_list_pointer(p1);
      heapcache[0] = *p1;
      p1++;
      heapcache[1] = accu;
      accu = make_list(heapcache);
      heapcache += 2;
      deref(p1);
      if (heapcache > limitcache)
	goto fishy_reverse;
    }

  if ((dlong)p1 == nil_atom)
    {
      lochreg = heapcache;
      locpc = contpreg;
      *p2 = accu;
      goto_more_exec;
    }

 fishy_reverse:
  lochreg = heapcache;
  underef(p1); underef(p2);
  args(1) = (dlong)p1;
  args(2) = accu;
  args(3) = (dlong)p2;
  locpc += reverse_entry_len;
  goto_more_exec;  
}

hcase(last_entry)
last_entry_label:
profile(last_entry);
{
  /* sysh:last(in,out) */
  /* with transfer to regular last when something is fishy */

  dlong *p1, *p2, *pcache;

  p1 = (dlong *)args(1);
  deref(p1);
  p2 = (dlong *)args(2);
  deref2(p2,goto last_entry_is_ref);
  goto regular_last;

 last_entry_is_ref:

  while (is_list(p1))
    {
      pcache = get_list_pointer(p1);
      p1 = pcache+1;
      deref(p1);
      if ((dlong)p1 == nil_atom)
	{
	  locpc = contpreg;
	  trail(p2,bh,loctrreg);
	  *p2 = *pcache;
	  goto_more_exec;
	}
    }

 pre_regular_last:
  underef(p1); args(1) = (dlong)p1;
 regular_last:
  locpc += last_entry_len;
  goto_more_exec;

}

hcase(length_entry)
length_entry_label:
profile(length_entry);
{
  /* sysh:length(in,out) */
  /* with transfer to regular length when something is fishy */

  register dlong *p1;
  dlong *p2;
  register dlong i = 0;
  dlong max;

  p1 = (dlong *)args(1);
  deref(p1);
  p2 = (dlong *)args(2);
  deref2(p2,goto length_entry_is_ref);
  locpc += length_entry_len;
  goto_more_exec;

 length_entry_is_ref:

  max = locmach.H - locmach.begin_heap;
  while (is_list(p1))
    {
    p1_is_list:
      i++;
      if (i > max)
	{
	  fprintf(stderr,"list length of cyclic list impossible\n");
	  goto_fail_label;
	}
      p1 = get_list_pointer(p1) + 1;
      p1 = (dlong *)(*p1);
      if (is_list(p1)) goto p1_is_list;
      deref(p1);
    }

  if ((dlong)p1 == nil_atom)
    {
      locpc = contpreg;
      trail(p2,bh,loctrreg);
      *p2 = make_smallint(i);
      goto_more_exec;
    }

  locpc += length_entry_len;
  goto_more_exec;
}

hcase(member_entry)
member_entry_label:
profile(member_entry);
{
  /* interrupt dealing ok because entered with a call */
  /* and because member itself does not generate garbage */
  /* but what about the continuations in the choicepoint ? */

  /*
    Areg[1] = X
    Areg[2] = [A|R] or fail
    create choice point with saved values X and R
                        and alternative = member_retry
    if (unify(X,A)) proceed
    else fail
  */
  dlong *l, *a, *r, *x;
  l = (dlong *)args(2);
  deref(l);
  if (! is_list(l))
    {
      locpc += member_entry_len + member_retry_len;
      goto_more_exec;
    }
  l = get_list_pointer(l);
  a = (dlong *)(*l); r = (dlong *)(*(l+1));
  deref(a); deref(r);
  x = (dlong *)args(1); deref(x);
  if ((dlong)r == nil_atom)
    {
      if (unify_terms(x,a,&locmach))
	{
	  locpc = contpreg;
	  goto_more_exec;
	}
      goto_fail_label;
    }
  locpc += member_entry_len;
  push_member_choicepoint(locpc,contpreg,x,r);
  bh = lochreg;
  if (unify_terms(x,a,&locmach))
    {
      locpc = contpreg;
      goto_more_exec;
    }
  goto_fail_label;
}

hcase(member_retry)
member_retry_label:
profile(member_retry);
{
  /*
    untrail;
    resetregs
    pick up x and r from choicepoint
    while (islist(r))
    {
      r = [a|newr];
      if (!unify(a,x)) {untrail; r = newr; continue;}
      move newr to choicepoint;
      proceed;
    }
    cut; fail;
   */

  CP_P bb;
  dlong *p, *x, *r, *a;
  bb = (CP_P)locbreg;
  untrail(loctrreg,(bb->tr));
  reset_regs1(bb);
  bh = bb->h;
  p = (dlong *)(((dlong *)bb) + (sizeof(struct choicepoint)/sizeof(dlong *)));
  r = (dlong *)(*p);
  x = (dlong *)(*(p+1));
  deref(r);
  while (is_list(r))
    {
      r = get_list_pointer(r);
      a = (dlong *)(*r); r = (dlong *)(*(r+1));
      deref(a);
      if (! unify_terms(a,x,&locmach))
	{
	  untrail(loctrreg,(bb->tr));
	  deref(r);
	}
      else
	{
	  deref(r);
	  //	  if (is_list(r))
	    *p = (dlong)r;
// 	  else
// 	    {
// 	      locbreg = ((CP_P)locbreg)->b;
// 	      freeze_heap();
// 	    }
	  locpc = contpreg;
	  goto_more_exec;
	}
    }
  locbreg = ((CP_P)locbreg)->b;
  freeze_heap();
  args(1) = (dlong)x;
  args(2) = (dlong)r;
  locpc += member_retry_len;
  goto_more_exec;
}

hcase(builtin_pickup_1)
builtin_pickup_1_label:
profile(builtin_pickup_1);
{
  fprintf(stderr,"Bad use of pickup/1\n");
  goto_fail_label;
}

hcase(builtin_pickup_spec_1)
builtin_pickup_spec_1_label:
profile(builtin_pickup_spec_1);
{
  dlong areg1;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_pickup_spec_1));
  args(areg1) = locmach.drop_point;
  locpc += builtin_pickup_spec_1_len;
  goto_more_exec;
}

hcase(builtin_drop_1)
builtin_drop_1_label:
profile(builtin_drop_1);
{
  dlong areg1;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_drop_1));
  locmach.drop_point = args(areg1);
  locpc += builtin_drop_1_len;
  goto_more_exec;
}

hcase(build_list_int)
build_list_int_label:
profile(build_list_int);
{
  dlong distance;
  dlong dlong2;

  distance = *(dlong *)(arg_place(locpc,1,build_list_int));
  *lochreg = make_list(lochreg+distance);
  lochreg++;

  dlong2 = *(dlong *)(arg_place(locpc,2,build_list_int));
  locpc += build_list_int_len;
  *lochreg = dlong2;
  lochreg++;

  goto_more_exec;
}


hcase(builtin_add_attr_4)
builtin_add_attr_4_label:
profile_builtin(builtin_add_attr_4);
{

  dlong areg1;
  dlong *p1, *p2, *p3, *p4;

  /* first argument is attvar - with or without the attribute module */
  /* add_init_attr(X,mod,G,foo(_,_)) */
  /* if X has mod, replace OldAtt by foo(OldAtt,G) - else give it G */

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_add_attr_4));
  p1 = (dlong *)args(areg1); deref(p1);
  areg1 = *(areg_type*)(arg_place(locpc,2,builtin_add_attr_4));
  p2 = (dlong *)args(areg1); deref(p2);
  areg1 = *(areg_type*)(arg_place(locpc,3,builtin_add_attr_4));
  p3 = (dlong *)args(areg1); deref(p3); underef(p3);
  areg1 = *(areg_type*)(arg_place(locpc,4,builtin_add_attr_4));
  p4 = (dlong *)args(areg1); deref(p4);
  
  if (! is_atom(p2))
    goto_fail_label;

  locpc += builtin_add_attr_4_len;

  /* p1 is an attributed var */

  p1 = get_attvar_pointer(p1);
  p1++;

  {
    dlong *q, *old, *q1;
    q = p1; deref(q);
    while (is_list(q))
      {
	q = get_list_pointer(q);
	q1 = q; deref(q1);
	if (q1 == p2)
	  {
	    /* we found the attribute 
	       overwrite next element of the list */
	    q++; deref(q); old = q = get_list_pointer(q);
	    deref(old); underef(old);
	    noye_trail(q,bh,loctrreg,p4);
	    p4 = get_struct_pointer(p4);
	    p4[1] = (dlong)old;
	    p4[2] = (dlong)p3;
	    goto_more_exec;
	  }
	q++; deref(q); q = get_list_pointer(q); q++; deref(q);
      }

    /* extend attribute list with this new attribute */
    q = p1; deref(q);
    q1 = (dlong *)(make_list(lochreg));
    *lochreg = (dlong)p2;  lochreg++;
    *lochreg = make_list(lochreg+1);  lochreg++;
    *lochreg = (dlong)p3;  lochreg++;
    *lochreg = (dlong)q;  lochreg++;
    noye_trail(p1,bh,loctrreg,q1);
    goto_more_exec;
  }
}

hcase(builtin_info_cp_1)
builtin_info_cp_1_label:
profile(builtin_info_cp_1);
{
  dlong *p;
  p = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_info_cp_1)));
  deref(p);
  show_info_cp(&locmach, get_cut_point(locmach.begin_cp, (dlong)p));
  locpc += builtin_info_cp_1_len;
  goto_more_exec;
}

hcase(builtin_notbadmetagoal_2)
builtin_notbadmetagoal_2_label:
profile(builtin_notbadmetagoal_2);
{
  dlong areg1;
  dlong areg2;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_notbadmetagoal_2));
  if (badmetagoal((dlong *)args(areg1))) goto_fail_label;
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_notbadmetagoal_2));
  locpc += builtin_notbadmetagoal_2_len;
  if (badmetagoal((dlong *)args(areg2))) goto_fail_label;

  goto_more_exec;
}

hcase(builtin_term_type_2)
builtin_term_type_2_label:
profile(builtin_term_type_2);
{
  dlong areg1, areg2;
  dlong res;
  dlong *term, *itstype;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_term_type_2));
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_term_type_2));

  term = (dlong *)args(areg1); deref(term);
  itstype = (dlong *)args(areg2); deref(itstype);

  switch (tag(term))
    {
    case REF0:
    case REF1:
      res = 0; break;

    case STRING:
      res = 1; break;

    case SIMPLE:
      if (is_atom(term)) res = 2;
      else if (is_char(term)) res = 3;
      else res = 4;
      break;

    case NUMB:
      if (is_real(term)) res = 5;
      else res = 6;
      break;

    case ATT:
      res = 7; break;

    case STRUCT:
      res = 8; break;

    case LIST:
      res = 9; break;
    }

  *itstype = make_smallint(res);
  locpc += builtin_term_type_2_len;
  goto_more_exec;
}


hcase(builtin_shell_2)
builtin_shell_2_label:
profile(builtin_shell_2);
{
  dlong areg1, areg2;
  dlong result, index;
  dlong *pat, *pres;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_shell_2));
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_shell_2));
  locpc += builtin_shell_2_len;

  pat = (dlong *)args(areg1); deref(pat);
  if (! is_atom(pat))
    goto error_shell_2;
  pres = (dlong *)args(areg2); deref(pres);
  if (! is_ref(pres))
    goto error_shell_2;

  index = get_atom_index(pat);
  result = system(symtab[index].name);

  trail(pres,bh,loctrreg);
  *pres = make_smallint(result);  
  goto_more_exec;
}


hcase(end_clauses)
end_clauses_label:
{
  fatal_message("end_clauses is impossible");
}

hcase(label)
label_label:
{
  fatal_message("label is impossible");
}

hcase(cases)
cases_label:
{
  fatal_message("cases is impossible");
}

hcase(arglabel)
arglabel_label:
{
  fatal_message("arglabel is impossible");
}

hcase(get_char)
get_char_label:
profile(get_char);
{
  fatal_message("instruction get_char is gone !\n");
}

hcase(unify_char)
unify_char_label:
profile(unify_char);
{
  fatal_message("instruction unify_char is gone !\n");
}

hcase(unify_atom)
unify_atom_label:
profile(unify_atom);
{
  fatal_message("instruction unify_atom is gone !\n");
}

hcase(build_char)
build_char_label:
profile(build_char);
{
  fatal_message("instruction build_char is gone !\n");
}

hcase(build_atom)
build_atom_label:
profile(build_atom);
{
  fatal_message("instruction build_atom is gone !\n");
}

hcase(get_atom)
get_atom_label:
profile(get_atom);
{
  fatal_message("instruction get_atom is gone !\n");
}

hcase(put_atomv)
put_atomv_label:
profile(put_atomv);
{
  fatal_message("obsolete put_atomv\n");
}

hcase(put_charv)
put_charv_label:
profile(put_charv);
{
  fatal_message("obsolete put_charv\n");
}

hcase(get_atomv)
get_atomv_label:
profile(get_atomv);
{
  fatal_message("obsolete get_atomv\n");
}

hcase(get_charv)
get_charv_label:
profile(get_charv);
{
  fatal_message("obsolete get_charv\n");
}

hcase(error_message)
error_message_label:
profile(error_message);
{
  dlong i;

  i = *(dlong *)(arg_place(locpc,1,error_message));
  build_smallint(&(args(1)),i);
  find_name_arity(locpc,&(args(2)),&i);
  build_smallint(&(args(3)),i);
  locpc = main_entrypoint(symbol_error_message_3);
  goto_more_exec;
}


deal_with_heap_overflow_check_head:
{
  dlong arity, size, symbol;

  locpc -= check_free_heap_head_len;
  arity = *(areg_type *) arg_place(locpc,1,check_free_heap_head);
  size = *(dlong *) arg_place(locpc,2,check_free_heap_head);
  if (heap_limit == 0)
    {
      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
        goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol_true_0,&locmach);
      goto_more_exec;
    }

  locmach.shadow_limit_heap = locmach.limit_heap;
  macro_deal_with_heap_overflow(arity,size);
  locpc += check_free_heap_head_len;
  goto_more_exec;
}

deal_with_heap_overflow_check_body:
{
  dlong arity, size, symbol;

  locpc -= check_free_heap_body_len;
  arity = 0;
  contpreg = locpc; /* check_free_heap_body should always follow
		       a call with an active yvar after it */
  size = *(dlong *) arg_place(locpc,1,check_free_heap_body);
  if (heap_limit == 0)
    {
      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
        goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol_true_0,&locmach);
      goto_more_exec;
    }
  
  locmach.shadow_limit_heap = locmach.limit_heap;
  macro_deal_with_heap_overflow(0, size);
  locpc += check_free_heap_body_len;
  goto_more_exec;
}

deal_with_heap_overflow_execute:
deal_with_heap_overflow_fast_execute:
{
  dlong symbol, arity;

  symbol = *(dlong *)(arg_place(locpc,2,execute)); // and fast_execute ??
  arity = symtab[symbol].arity;
  locpc = main_entrypoint(symbol);

  if (heap_limit == 0)
    {
      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
	goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol,&locmach);
      goto_more_exec;
    }

  locmach.shadow_limit_heap = locmach.limit_heap;
  macro_deal_with_heap_overflow(arity,SPARE_HEAP);
  goto_more_exec;
}

deal_with_heap_overflow_deallex:
{
  dlong symbol, arity;

  symbol = *(dlong *)(arg_place(locpc,1,deallex));
  arity = symtab[symbol].arity;
  locpc = main_entrypoint(symbol);

  if (heap_limit == 0)
    {
      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
	goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol,&locmach);
      goto_more_exec;
    }

  locmach.shadow_limit_heap = locmach.limit_heap;
  macro_deal_with_heap_overflow(arity,SPARE_HEAP);
  goto_more_exec;
}

deal_with_heap_overflow_dealloc_proceed:
{
  dlong symbol, arity;

  arity = 0;
  contpreg = locpc;
  if (heap_limit == 0)
    {
      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
        goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol_true_0,&locmach);
      goto_more_exec;
    }

  locmach.shadow_limit_heap = locmach.limit_heap;
  macro_deal_with_heap_overflow(0,SPARE_HEAP);
  goto_more_exec;
}

deal_with_heap_overflow_dummy_call:
{
  dlong symbol, arity;

  if (heap_limit == 0)
    {
      locpc = contpreg - call_len - active_yvar_len;
      symbol = *(dlong *)(arg_place(locpc,1,call));
      arity = symtab[symbol].arity;

      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
        goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol,&locmach);
      goto_more_exec;
    }

  locmach.shadow_limit_heap = locmach.limit_heap;
  symbol = *(dlong *)(arg_place((contpreg - dummy_call_len - active_yvar_len),1,call));
  arity = symtab[symbol].arity;
  macro_deal_with_heap_overflow(arity,SPARE_HEAP);
  goto_more_exec;
}

deal_with_heap_overflow_call:
{
  dlong symbol, arity;

  if (heap_limit == 0)
    {
      locpc = contpreg - call_len - active_yvar_len;
      symbol = *(dlong *)(arg_place(locpc,1,call));
      arity = symtab[symbol].arity;

      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
        goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol,&locmach);
      goto_more_exec;
    }

  locmach.shadow_limit_heap = locmach.limit_heap;
  symbol = *(dlong *)(arg_place((contpreg - call_len - active_yvar_len),1,call));
  arity = symtab[symbol].arity;
  macro_deal_with_heap_overflow(arity,SPARE_HEAP);
  goto_more_exec;
}

deal_with_heap_overflow_putpvarcall:
{
  dlong symbol, arity;

  if (heap_limit == 0)
    {
      locpc = contpreg - putpvarcall_len - active_yvar_len;
      symbol = *(dlong *)(arg_place(locpc,3,putpvarcall));
      arity = symtab[symbol].arity;

      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
        goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol,&locmach);
      goto_more_exec;
    }

  locmach.shadow_limit_heap = locmach.limit_heap;
  symbol = *(dlong *)(arg_place((contpreg - putpvarcall_len - active_yvar_len),3,putpvarcall));
  arity = symtab[symbol].arity;
  macro_deal_with_heap_overflow(arity,SPARE_HEAP);
  goto_more_exec;
}

deal_with_heap_overflow_putpvarvalvalcall:
{
  dlong symbol, arity;

  if (heap_limit == 0)
    {
      locpc = contpreg - putpvarvalvalcall_len - active_yvar_len;
      symbol = *(dlong *)(arg_place(locpc,7,putpvarvalvalcall));
      arity = symtab[symbol].arity;

      locmach.shadow_limit_heap = locmach.limit_heap;
      if ((!interrupt_flag) && (!(get_number_of_woken_goals(&locmach))))
        goto_more_exec;
      ensure_space_for_woken_goals(locmach);
      locpc = treat_asynchrous_call(symbol,&locmach);
      goto_more_exec;
    }

  locmach.shadow_limit_heap = locmach.limit_heap;
  symbol = *(dlong *)(arg_place((contpreg - putpvarvalvalcall_len - active_yvar_len),7,putpvarvalvalcall));
  arity = symtab[symbol].arity;
  macro_deal_with_heap_overflow(arity,SPARE_HEAP);
  goto_more_exec;
}

deal_with_extendible_call:
{
  dlong i = glob_arity;
  dlong arity;
  dlong *p;
  codep newlocpc;

  /* find arity of argument1 first */
  p = (dlong *)args(1); deref(p);
  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      arity = get_funct_arity(*p);
      if ((arity+i) >= ARGS)
	goto_fail_label;
      newlocpc = main_entrypoint(sym_lookup(arity+i,get_funct_name(*p),get_funct_module(*p)));
    }
  else
    if (is_atom(p))
      {
	arity = 0;
	newlocpc = main_entrypoint(sym_lookup(i,get_atom_name(p),get_atom_module(p)));
      }
  else
    if (is_list(p))
      {
	arity = 2;
	newlocpc = main_entrypoint(sym_lookup(2+i,".",USER_MOD));
	p = get_list_pointer(p) - 1;
      }			
  else
    goto call_n_error;

  if (! newlocpc) 
    goto call_n_error;
  locpc = newlocpc;

  /* now the arguments - start with putting the given args right */
  /* there are 2 special cases */
  if (arity == 1)
    args(1) = *(p+1);
  else
    if (arity == 0)
      {
	dlong j;
	/* got to move i arguments one place to the left */
	for (j = 1; i--; j++)
	  args(j) = args(j+1);
      }
  else
    {
      dlong j,k;
      k = i+1;
      for (j = arity + i; i--; j--)
	args(j) = args(k--);
      for (j = 1; arity--; j++)
	args(j) = *(++p);
    }

  goto_more_exec;

 call_n_error:
  if (glob_arity == 1) { locpc += builtin_call_2_len; goto error_call_2; }
  if (glob_arity == 2) { locpc += builtin_call_3_len; goto error_call_3; }
  if (glob_arity == 3) { locpc += builtin_call_4_len; goto error_call_4; }
  if (glob_arity == 4) { locpc += builtin_call_5_len; goto error_call_5; }
  if (glob_arity == 5) { locpc += builtin_call_6_len; goto error_call_6; }
  if (glob_arity == 6) { locpc += builtin_call_7_len; goto error_call_7; }
  if (glob_arity == 7) { locpc += builtin_call_8_len; goto error_call_8; }
  if (glob_arity == 8) { locpc += builtin_call_9_len; goto error_call_9; }
  if (glob_arity == 9) { locpc += builtin_call_10_len; goto error_call_10; }
  fprintf(stderr,"impossible call inside call/n - message to be updated\n");
  goto_fail_label;
}

hcase(builtin_norepshar_1)
builtin_norepshar_1_label:
profile(builtin_norepshar_1);
{
  dlong areg1;
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_norepshar_1));
  locpc += builtin_norepshar_1_len;
  set_no_representation_sharing((dlong *)args(areg1));
  goto_more_exec;
}


#include "builtin_errors.c"

error_builtin:
{
  dlong i = glob_arity;
  dlong j;

  build_smallint(&(args(i+1)),glob_nrbuiltin);
  find_name_arity(locpc,&(args(i+2)),&j);
  build_smallint(&(args(i+3)),j);
  locpc = main_entrypoint(sym_lookup(i+3,"error_builtin",SYSTEM_MOD));
  goto_more_exec;
}

arith_error_arith5:
{
  dlong i;

  args(3) = make_atom(sym_lookup(0,arithsymb,USER_MOD));
  find_name_arity(locpc,&(args(4)),&i);
  build_smallint(&(args(5)),i);
  locpc = main_entrypoint(symbol_error_builtin_arith5);
  goto_more_exec;
}

arith_error_arith5bis:
{
  dlong i, i1, i2;

  locpc -= add_len;
  i = *(areg_type *)(arg_place(locpc,2,add)); i1 = args(i);
  i = *(areg_type *)(arg_place(locpc,3,add)); i2 = args(i);
  args(1) = i1;
  args(2) = i2;
  args(3) = make_atom(sym_lookup(0,arithsymb,USER_MOD));
  find_name_arity(locpc,&(args(4)),&i);
  build_smallint(&(args(5)),i);
  locpc = main_entrypoint(symbol_error_builtin_arith5);
  goto_more_exec;
}

arith_error_test_smaller:
{
  arithsymb = "<";
  goto arith_error_compare;
}

arith_error_test_smaller_or_equal:
{
  arithsymb = "=<";
  goto arith_error_compare;
}

arith_error_test_inequal:
{
  arithsymb = "=\\=";
  goto arith_error_compare;
}

arith_error_test_equal:
{
  arithsymb = "=:=";
  goto arith_error_compare;
}

arith_error_compare:
{
  dlong areg1, areg2, i;

  locpc -= test_equal_len;
  areg1 = *(areg_type *)(arg_place(locpc,1,test_equal)); /* generic test_equal */
  areg1 = args(areg1);
  areg2 = *(areg_type *)(arg_place(locpc,2,test_equal)); /* generic test_equal */
  areg2 = args(areg2);
  args(1) = areg1;
  args(2) = areg2;
  args(3) = make_atom(sym_lookup(0,arithsymb,USER_MOD));
  find_name_arity(locpc,&(args(4)),&i);
  build_smallint(&(args(5)),i);
  locpc = main_entrypoint(symbol_error_builtin_arith5);
  goto_more_exec;
}

arith_error_float_sub:
{
  dlong areg2;
  dfloat f1;

  locpc -= float_sub_len;
  areg2 = *(areg_type *)(arg_place(locpc,2,float_sub));
  get_double_arg(f1,(arg_place(locpc,3,float_sub)));
  args(2) = args(areg2);
  build_real(&args(1),f1,lochreg);
  arithsymb = "-";
  goto arith_error_arith5;
}

arith_error_add_float:
{
  dlong areg2;
  dfloat f1;

  locpc -= add_float_len;
  areg2 = *(areg_type *)(arg_place(locpc,2,add_float));
  get_double_arg(f1,(arg_place(locpc,3,add_float)));
  args(1) = args(areg2);
  build_real(&args(2),f1,lochreg);
  arithsymb = "+";
  goto arith_error_arith5;
}

arith_error_integer_sub:
{
  dlong areg2;
  dlong i;

  locpc -= integer_sub_len;
  areg2 = *(areg_type *)(arg_place(locpc,2,integer_sub));
  i = *(dlong *)(arg_place(locpc,3,integer_sub));
  args(2) = args(areg2);
  args(1) = make_smallint(i);
  arithsymb = "-";
  goto arith_error_arith5;
}

arith_error_idiv_integer:
{
  dlong areg2;
  dlong i;

  locpc -= idiv_integer_len;
  areg2 = *(areg_type *)(arg_place(locpc,2,idiv_integer));
  i = *(dlong *)(arg_place(locpc,3,idiv_integer));
  args(1) = args(areg2);
  args(2) = make_smallint(i);
  arithsymb = "//";
  goto arith_error_arith5;
}

arith_error_modulo_integer:
{
  dlong areg2;
  dlong i;

  locpc -= modulo_integer_len;
  areg2 = *(areg_type *)(arg_place(locpc,2,modulo_integer));
  i = *(dlong *)(arg_place(locpc,3,modulo_integer));
  args(1) = args(areg2);
  args(2) = make_smallint(i);
  arithsymb = "mod";
  goto arith_error_arith5;
}

arith_error_add_integer_p:
{
  dlong yvar2;
  dlong i;

  locpc -= add_integer_p_len;
  yvar2 = *(areg_type *)(arg_place(locpc,2,add_integer_p));
  i = *(dlong *)(arg_place(locpc,4,add_integer_p));
  args(1) = ereg[yvar2];
  args(2) = make_smallint(i);
  arithsymb = "+";
  goto arith_error_arith5;
}

arith_error_add_integer:
{
  dlong areg2;
  dlong i;

  locpc -= add_integer_len;
  areg2 = *(areg_type *)(arg_place(locpc,2,add_integer));
  i = *(dlong *)(arg_place(locpc,3,add_integer));
  args(1) = args(areg2);
  args(2) = make_smallint(i);
  arithsymb = "+";
  goto arith_error_arith5;
}

arith_error_add1:
{
  dlong areg1;
  dlong i;

  locpc -= add1_len;
  areg1 = *(areg_type *)(arg_place(locpc,1,add1));
  args(1) = args(areg1);
  args(2) = make_smallint(1);
  arithsymb = "+";
  goto arith_error_arith5;
}

arith_error_sub1:
{
  dlong areg1;
  dlong i;

  locpc -= sub1_len;
  areg1 = *(areg_type *)(arg_place(locpc,1,sub1));
  args(1) = args(areg1);
  args(2) = make_smallint(1);
  arithsymb = "-";
  goto arith_error_arith5;
}

generic_arith_error_test_float:
{
  dlong areg1;
  dfloat f2;

  locpc -= test_smaller_or_equal_float_len;
  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_or_equal_float));
  get_double_arg(f2, arg_place(locpc,2,test_smaller_or_equal_float));
  args(1) = args(areg1);
  args(2) = make_number_pointer(lochreg);
  push_real(lochreg,f2);
  goto arith_error_arith5;
}

arith_error_test_smaller_or_equal_float:
arithsymb = "=<";
goto generic_arith_error_test_float;

arith_error_test_smaller_float:
arithsymb = "<";
goto generic_arith_error_test_float;

arith_error_test_larger_or_equal_float:
arithsymb = ">=";
goto generic_arith_error_test_float;

arith_error_test_larger_float:
arithsymb = ">";
goto generic_arith_error_test_float;

arith_error_test_inequal_float:
arithsymb = "=\\=";
goto generic_arith_error_test_float;

arith_error_test_equal_float:
arithsymb = "=:=";
goto generic_arith_error_test_float;


generic_arith_error_test_int:
{
  dlong areg1;
  dlong int2;
  locpc -= test_smaller_int_len;
  areg1 = *(areg_type *)(arg_place(locpc,1,test_smaller_int));
  int2 = *(dlong *)(arg_place(locpc,2,test_smaller_int));
  args(1) = args(areg1);
  args(2) = make_smallint(int2);
  goto arith_error_arith5;
}

error_test_smaller_or_equal_int:
arithsymb = "=<";
goto generic_arith_error_test_int;

error_test_smaller_int:
arithsymb = "<";
goto generic_arith_error_test_int;

error_test_larger_or_equal_int:
arithsymb = ">=";
goto generic_arith_error_test_int;

error_test_larger_int:
arithsymb = ">";
goto generic_arith_error_test_int;

error_test_inequal_int:
arithsymb = "=\\=";
goto generic_arith_error_test_int;

error_test_equal_int:
arithsymb = "=:=";
goto generic_arith_error_test_int;


arith_error_arith4:
{
  dlong areg2;
  dlong i;

  locpc -= min1_len;
  areg2 = *(areg_type *)(arg_place(locpc,2,min1)); /* generic min1 */
  areg2 = args(areg2);
  args(1) = areg2;
  args(2) = make_atom(sym_lookup(0,arithsymb,0));
  find_name_arity(locpc,&(args(3)), &i);
  build_smallint(&(args(4)),i);
  locpc = main_entrypoint(sym_lookup(4,"error_builtin_arith4",SYSTEM_MOD));
  goto_more_exec;
}


arith_error_mysqrt:
arithsymb = "sqrt";
goto arith_error_arith4;


arith_error_myabs:
arithsymb = "abs";
goto arith_error_arith4;

arith_error_mysign:
arithsymb = "sign";
goto arith_error_arith4;

arith_error_mylog:
arithsymb = "log";
goto arith_error_arith4;

arith_error_myexp:
arithsymb = "exp";
goto arith_error_arith4;

arith_error_mycos:
arithsymb = "cos";
goto arith_error_arith4;

arith_error_mysin:
arithsymb = "sin";
goto arith_error_arith4;

arith_error_myatan:
arithsymb = "atan";
goto arith_error_arith4;

arith_error_min1:
arithsymb = "-";
goto arith_error_arith4;

arith_error_negate:
arithsymb = "~";
goto arith_error_arith4;

arith_error_mostsigbit:
arithsymb = "msb";
goto arith_error_arith4;

arith_error_round2int:
arithsymb = "round";
goto arith_error_arith4;

arith_error_float2intceil:
arithsymb = "ceil";
goto arith_error_arith4;

arith_error_float2inttrunc:
arithsymb = "trunc";
goto arith_error_arith4;

arith_error_float2intfloor:
arithsymb = "floor";
goto arith_error_arith4;

arith_error_c_modulo:
arithsymb = "mod";
goto arith_error_arith5bis;

arith_error_minimum:
arithsymb = "min";
goto arith_error_arith5bis;

arith_error_maximum:
arithsymb = "max";
goto arith_error_arith5bis;

arith_error_logshiftr:
arithsymb = ">>";
goto arith_error_arith5bis;

arith_error_logshiftl:
arithsymb = "<<";
goto arith_error_arith5bis;

arith_error_idivision:
arithsymb = "//";
goto arith_error_arith5bis;

arith_error_division:
arithsymb = "/";
goto arith_error_arith5bis;

arith_error_multiply:
arithsymb = "*";
goto arith_error_arith5bis;

arith_error_bit_or:
arithsymb = "\\/";
goto arith_error_arith5bis;

arith_error_bit_xor:
arithsymb = "xor";
goto arith_error_arith5bis;

arith_error_bit_and:
arithsymb = "/\\";
goto arith_error_arith5bis;

arith_error_subtr:
arithsymb = "-";
goto arith_error_arith5bis;

arith_error_power:
arithsymb = "**";
goto arith_error_arith5bis;

arith_error_add:
arithsymb = "+";
goto arith_error_arith5bis;

hcase(builtin_sleep_1)
builtin_sleep_1_label:
profile(builtin_sleep_1);
{
  dlong areg1;
  dlong *p1;
  dlong i; dfloat f;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_sleep_1));
  p1 = (dlong *)args(areg1); deref(p1);
  locpc += builtin_sleep_1_len;

  if (is_smallint(p1))
    {
      i = get_smallint(p1);
      f = (dfloat) i;
    }
  else if (is_real(p1))
    {
      f = get_real(p1);
      i = (dlong) f;
    }
  else goto error_sleep_1;

  struct timespec secs = {
    i, (dlong) ((f - ((dfloat) i))*10e8)
  };

  struct timespec remaining;
  dlong result;
  do
    {
      result = nanosleep(&secs, &remaining);
      secs = remaining;
    }
  while (result);

  goto_more_exec;
}

hcase(builtin_flush_output_1)
builtin_flush_output_1_label:
profile(builtin_flush_output_1);
{
  dlong areg1;
  dlong *p1;
  dlong i;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_flush_output_1));
  locpc += builtin_flush_output_1_len;
  p1 = (dlong *) args(areg1); deref(p1);
  if (is_smallint(p1))
    i = get_smallint(p1);
  else if (is_atom(p1))
    {
      dlong symbol = get_atom_index(p1);
      i = file2stream(symbol,FILE_WRITE,&locmach);
    }
  else
    goto error_flush_output_1;
  
  if ((i < 0) || (i >= locmach.nr_open_files))
    goto error_flush_output_1;

  fflush(locmach.openfiles[i].fp);

  goto_more_exec;
}

#ifdef MULTI_THREADED
/* - Logic engines: creation & ids - */
// Get the ID of the current engine
hcase(builtin_pid_1)
builtin_pid_1_label:
profile(builtin_pid_1);
{
  dlong areg1;
  dlong *p1;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_pid_1));
  locpc += builtin_pid_1_len;
  p1 = (dlong *)args(areg1); deref(p1);

  if (!is_ref(p1))
    goto error_pid_1;

  trail(p1,bh,loctrreg);
  build_smallint(p1,make_engine_id(thisengine->index));

  goto_more_exec;
}

// Create a new messaging hub
hcase(builtin_hub_1)
builtin_hub_1_label:
profile(builtin_hub_1);
{
  dlong areg1;
  dlong *p1;
  dlong hub_id;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_hub_1));
  locpc += builtin_hub_1_len;

  // Engine id
  p1 = (dlong *)args(areg1); deref(p1);
  if (!is_ref(p1))
    goto error_hub_1;

  hub_id = builtin_new_hub();
  // The hub couldn't be created
  if (hub_id < 0)
    goto error_hub_1;

  trail(p1,bh,loctrreg);
  build_smallint(p1, make_hub_id(hub_id));

  goto_more_exec;
}

// Run an engine with given goal - return its ID
hcase(builtin_spawn_3)
builtin_spawn_3_label:
profile(builtin_spawn_3);
{
  dlong areg1, areg2, areg3;
  dlong *p1, *p2, *p3;
  dlong engine_index;
  dlong parent_id;
  short link = 0;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_spawn_3)); // parent to link to
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_spawn_3)); // answer pattern + start goal
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_spawn_3)); // id
  locpc += builtin_spawn_3_len;

  // Parent id
  p1 = (dlong *)args(areg1); deref(p1);

  if (! is_smallint(p1)) goto error_spawn_3;
  p2 = (dlong *)args(areg2); deref(p2);
  p3 = (dlong *)args(areg3); deref(p3);
  if (!is_ref(p3)) goto error_spawn_3;

  parent_id = get_smallint(p1);
  if (parent_id == -1) parent_id = make_engine_id(thisengine->index);
  else
    if (parent_id == -2) {parent_id = make_engine_id(thisengine->index); link = 1;}
  else link = 1;

  // ???
  //  if (! is_engine_id(parent_id)) goto error_spawn_3;
  //  if (link && (get_engine_index(parent_id) != thisengine->index)) goto error_spawn_3;

  engine_index = builtin_spawn(p2, parent_id, link, thisengine);

  if (engine_index < 0) goto error_spawn_3;

  trail(p3,bh,loctrreg);
  build_smallint(p3, make_engine_id(engine_index));

  goto_more_exec;
}

// Get the atom representing the starting goal for the current engine.
hcase(builtin_engine_get_start_goal_low_1)
builtin_engine_get_start_goal_low_1_label:
profile(builtin_engine_get_start_goal_low_1);
{
  dlong areg1;
  dlong *p1;
  dlong ret;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_engine_get_start_goal_low_1));
  p1 = (dlong *) args(areg1); deref(p1);
  locpc += builtin_engine_get_start_goal_low_1_len;

  if (!is_ref(p1))
    goto error_engine_get_start_goal_low_1;

  dlong start_goal_size = builtin_engine_start_goal_size(thisengine);
  if (start_goal_size < 0)
    goto error_engine_get_start_goal_low_1;
  if (lochreg + start_goal_size > heap_limit)
    macro_deal_with_heap_overflow(1,start_goal_size);

  p1 = (dlong *) args(areg1);
  deref(p1);

  trail(p1,bh,loctrreg);
  ret = builtin_engine_start_goal(p1, thisengine);
  if (!ret)
    goto error_engine_get_start_goal_low_1;

  goto_more_exec;
}

// Stop the engine with the given ID
hcase(builtin_stop_1)
builtin_stop_1_label:
profile(builtin_stop_1);
{
  dlong areg1;
  dlong *p1;
  dlong id;
  dlong ret;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_stop_1));
  locpc += builtin_stop_1_len;

  // Engine/hub id
  p1 = (dlong *)args(areg1); deref(p1);

  if (! is_smallint(p1)) goto error_stop_1;
  id = get_smallint(p1);

  if (is_engine_id(id))
    ret = builtin_stop_engine(get_engine_index(id), 1, thisengine);
  else
    ret = builtin_stop_hub(get_hub_index(id),&locmach, thisengine);

  if (!ret)
    goto error_stop_1;

  goto_more_exec;
}

/* - Logic engines: messaging- */
// Push a string to a given inbox
hcase(builtin_low_send_2)
builtin_low_send_2_label:
profile(builtin_low_send_2);
{
  dlong areg1, areg2;
  dlong *p1, *p2;
  dlong id;
  dlong ret;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_low_send_2));
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_low_send_2));
  locpc += builtin_low_send_2_len;

  // Engine/hub ID
  p1 = (dlong *)args(areg1); deref(p1);
  if (is_smallint(p1))
    id = get_smallint(p1);
  else if (is_ref(p1))
    id = thisengine->parent_id;
  else
    goto error_low_send_2;

  // Message to push
  p2 = (dlong *)args(areg2); deref(p2);

  if (is_engine_id(id))
    ret = builtin_engine_send(get_engine_index(id), p2, thisengine);
  else
    ret = builtin_hub_send(get_hub_index(id), p2, thisengine);
  // Message could not be sent (invalid engine id?)
  if (!ret)
    goto error_low_send_2;

  goto_more_exec;
}

// Pop a string from a given engine from the current engine's inbox.
// Use from_engine_index == unbound to pop a message from any sender.
hcase(builtin_low_receive_3)
builtin_low_receive_3_label:
profile(builtin_low_receive_3);
{
  dlong areg1, areg2, areg3;
  dlong *p1, *p2, *p3;
  dlong hub_id;
  dlong from_id;
  dlong ret;

  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_low_receive_3));
  areg2 = *(areg_type*)(arg_place(locpc,2,builtin_low_receive_3));
  areg3 = *(areg_type*)(arg_place(locpc,3,builtin_low_receive_3));
  locpc += builtin_low_receive_3_len;

  // Inbox ID:
  // - if unbound or not a hub ID, pop a message from our own inbox
  // - if positive, pop a message from the given hub's inbox
  p1 = (dlong *)args(areg1); deref(p1);
  if (is_ref(p1))
    hub_id = -1;
  else if (is_smallint(p1))
    hub_id = get_smallint(p1);
  else
    goto error_low_receive_3;

  // Sender engine ID:
  // - if unbound or not an engine ID, don't filter by sender
  // - if positive, filter by sender engine
  p2 = (dlong *)args(areg2); deref(p2);
  if (is_ref(p2))
    from_id = -1;
  else if (is_smallint(p2))
    from_id = get_smallint(p2);
  else
    goto error_low_receive_3;

  // Message variable
  p3 = (dlong *) args(areg3); deref(p3);
  if (!is_ref(p3))
    goto error_low_receive_3;

  struct message* msg;
  // Get a message from our own inbox
  if (hub_id < 0 || !is_hub_id(hub_id))
    {
      // Filter by sender engine
      if (from_id >= 0 && is_engine_id(from_id))
	msg = builtin_engine_next_msg(get_engine_index(from_id), thisengine);
      // Don't filter by sender engine
      else
	msg = builtin_engine_next_msg(-1, thisengine);
    }
  // Get a message from a hub's inbox
  else
    {
      // Filter by sender engine
      if (from_id >= 0 && is_engine_id(from_id))
	msg = builtin_hub_next_msg(get_hub_index(hub_id), get_engine_index(from_id), thisengine);
      // Don't filter by sender engine
      else
	msg = builtin_hub_next_msg(get_hub_index(hub_id), -1, thisengine);
    }

  if (msg == NULL)
    goto error_low_receive_3;

  if (lochreg + msg->msg->size > heap_limit)
    macro_deal_with_heap_overflow(3,msg->msg->size);

  p3 = (dlong *) args(areg3);
  deref(p3);

  trail(p3,bh,loctrreg);
  ret = builtin_receive(p3, msg, thisengine);
  if (!ret)
    goto error_low_receive_3;


  if (is_ref(p2))
    {
      trail(p2,bh,loctrreg);
      build_smallint(p2, make_engine_id(msg->from_engine_index));
    }

  goto_more_exec;
}

hcase(builtin_running_threads_1)
builtin_running_threads_1_label:
profile(builtin_running_threads_1);
{
  dlong areg1;
  dlong *p1 ;
  
  areg1 = *(areg_type*)(arg_place(locpc,1,builtin_running_threads_1));
  p1 = (dlong *)args(areg1); deref(p1);
  
  *p1 = make_smallint( nr_of_running_threads );
  
  locpc += builtin_running_threads_1_len;
  goto_more_exec;
}

hcase(builtin_lock_1)
builtin_lock_1_label:
profile(builtin_lock_1);
{
  dlong *p1;
  dlong i;
  
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nb_setval_2)));
  
  locpc += builtin_lock_1_len;
  
  deref(p1);
  
  if (! is_atom(p1)) goto_fail_label;
  i = get_atom_index(p1);
  pthread_mutex_lock(&(symtab[i].flock));
  
  goto_more_exec;
}

hcase(builtin_unlock_1)
builtin_unlock_1_label:
profile(builtin_unlock_1);
{
  dlong *p1;
  dlong i;
  
  p1 = (dlong *)args(*(areg_type *)(arg_place(locpc,1,builtin_nb_setval_2)));
  
  locpc += builtin_lock_1_len;
  
  deref(p1);
  
  if (! is_atom(p1)) goto_fail_label;
  i = get_atom_index(p1);
  pthread_mutex_unlock(&(symtab[i].flock));
  
  goto_more_exec;
  
}

#else
hcase(builtin_running_threads_1)
builtin_running_threads_1_label:
profile(builtin_running_threads_1);

hcase(builtin_lock_1)
builtin_lock_1_label:
profile(builtin_lock_1);

hcase(builtin_unlock_1)
builtin_unlock_1_label:
profile(builtin_unlock_1);

hcase(builtin_pid_1)
builtin_pid_1_label:
profile(builtin_pid_1);
hcase(builtin_hub_1)
builtin_hub_1_label:
profile(builtin_hub_1);
hcase(builtin_spawn_3)
builtin_spawn_3_label:
profile(builtin_spawn_3);
hcase(builtin_engine_get_start_goal_low_1)
builtin_engine_get_start_goal_low_1_label:
profile(builtin_engine_get_start_goal_low_1);
hcase(builtin_stop_1)
builtin_stop_1_label:
profile(builtin_stop_1);
hcase(builtin_low_send_2)
builtin_low_send_2_label:
profile(builtin_low_send_2);
hcase(builtin_low_receive_3)
builtin_low_receive_3_label:
profile(builtin_low_receive_3);
{
  fprintf(stderr,"System was made without multi-threading !\n");
  exit(1);
}
#endif
