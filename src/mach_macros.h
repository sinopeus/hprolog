#define breg machp->B
#define ereg machp->E
#define trreg machp->TR
#define hreg machp->H
#define preg machp->P
#define contpreg machp->CONTP
#define tos machp->TOS
#define bh machp->BH
#define heap_limit machp->shadow_limit_heap
#define ls_limit machp->limit_ls
#define begin_ls machp->begin_ls
#define cp_limit machp->limit_cp
#define trail_limit machp->limit_trail
#define args(i) machp->Areg[i]
#define start_heap machp->begin_heap
#define current_out machp->openfiles[machp->file_out_nr].fp
#define current_in machp->openfiles[machp->file_in_nr].fp
#define current_out_nr machp->file_out_nr
#define current_in_nr machp->file_in_nr

