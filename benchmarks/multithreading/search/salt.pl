% each state is represented by a compound term with four arguments: (Acumulator, Measure1, Measure2, Step)
initial_state((0, 0, 0, all_empty)).

% the intended salt quantity must end up on the acumulator
goal_state((Liters, _, _), (Liters, _, _, _)).
% state transitions:

% emptying a measure into the accumulator
next_state(_, (Acc, X, Y, _), (NewAcc, 0, Y, transfer(m1, acc)), 1) :-
  X > 0,
  NewAcc is Acc + X.
next_state(_, (Acc, X, Y, _), (NewAcc, X, 0, transfer(m2, acc)), 1) :-
  Y > 0,
  NewAcc is Acc + Y.

% filling up of one of the measures
next_state((_, MaxX, _), (Acc, X, Y, Step), (Acc, MaxX, Y, fill(m1)), 1) :-
  X < MaxX,
  Step \= empty(m1).
next_state((_, _, MaxY), (Acc, X, Y, Step), (Acc, X, MaxY, fill(m2)), 1) :-
  Y < MaxY,
  Step \= empty(m2).

% either pouring of a measure into the other till it is filled up
% or all content of a measure into the other one
next_state((_, MaxX, _), (Acc, X, Y, _), (Acc, W, Z, transfer(m2, m1)), 1) :-
  Y > 0,
  X < MaxX,
  (X + Y >= MaxX ->
    W = MaxX,
    Z is Y - (MaxX - X)
    ;
    W is X + Y,
    Z = 0
   ).
next_state((_, _, MaxY), (Acc, X, Y, _), (Acc, W, Z, transfer(m1, m2)), 1) :-
  X > 0,
  Y < MaxY,
  (X + Y >= MaxY ->
    W is X - (MaxY - Y),
    Z = MaxY
    ;
    W = 0,
    Z is X + Y
   ).

% throwing out the contents of a measure; does not afect the accumulator
next_state(_, (Acc, X, Y, Step), (Acc, 0, Y, empty(m1)), 1) :-
  X > 0,
  Step \= fill(m1).
next_state(_, (Acc, X, Y, Step), (Acc, X, 0, empty(m2)), 1) :-
  Y > 0,
  Step \= fill(m2).

heuristic((Acc, _, _), (Acc, Acc, _, _), 0.1) :-
  !.
heuristic((Acc, _, _), (Acc, _, Acc, _), 0.1) :-
  !.
heuristic((Acc, _, _), (Acc, X, Y, _), 0.2) :-
  Acc is abs(X - Y),
  !.
heuristic((Acc, _, _), (Acc, X, _, _), 0.3) :-
  (	X mod Acc =:= 0 ->
    Cost is X // Acc
  ;	Acc mod X =:= 0 ->
    Cost is Acc // X
  ),
  !.
heuristic((Acc, _, _), (Acc, _, Y, _), 0.3) :-
  (	Y mod Acc =:= 0 ->
    Cost is Y // Acc
  ;	Acc mod Y =:= 0 ->
    Cost is Acc // Y
  ),
  !.
heuristic((Acc, _, _), (Acc, X, Y, _), 0.4) :-
  Diff is abs(X - Y),
  (	Diff mod Acc =:= 0 ->
    Cost is Diff // Acc
  ;	Acc mod Diff =:= 0 ->
    Cost is Acc // Diff
  ),
  !.
heuristic(_, (_, _, _, _), 0.5).

member_path((Acc, X, Y, _), [(Acc, X, Y, _)| _]) :-
  !.
member_path(State, [_| Path]) :-
  member_path(State, Path).

print_state((Acc, X, Y, Step)) :-
  write('('), write((Acc, X, Y)), write(')    '), write(Step), nl.
