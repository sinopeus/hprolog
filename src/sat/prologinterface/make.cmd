@ECHO OFF

copy pl-crypminisat.cpp ..\cryptominisat-2.5.1\Solver\pl-crypminisat.cpp
cd ..\cryptominisat-2.5.1\Solver

swipl-ld -O2 -fomit-frame-pointer -s -shared -fpic -o ..\..\pl-crypminisat.dll ^
pl-crypminisat.cpp ^
Clause.cpp ^
ClauseCleaner.cpp ^
FailedVarSearcher.cpp ^
FindUndef.cpp ^
PackedRow.cpp ^
PartFinder.cpp ^
PartHandler.cpp ^
RestartTypeChooser.cpp ^
SmallPtr.cpp ^
Solver.cpp ^
StateSaver.cpp ^
Subsumer.cpp ^
VarReplacer.cpp ^
XorFinder.cpp ^
XorSubsumer.cpp ^
-I"..\mtl" -I"..\MTRand" -I"..\Solver"

cd ..\..

rem del *.obj
del pl-crypminisat.lib
del pl-crypminisat.dll.manifest
del pl-crypminisat.exp
 
GOTO endComments
----------------------------------
            Comments
----------------------------------
To compile slim CryptoMinisat
(a version with no logger and no gaussian support)
replace the line *.cpp ^" with the following:
pl-crypminisat.cpp ^
Clause.cpp ^
ClauseCleaner.cpp ^
FailedVarSearcher.cpp ^
FindUndef.cpp ^
PackedRow.cpp ^
PartFinder.cpp ^
PartHandler.cpp ^
RestartTypeChooser.cpp ^
SmallPtr.cpp ^
Solver.cpp ^
StateSaver.cpp ^
Subsumer.cpp ^
VarReplacer.cpp ^
XorFinder.cpp ^
XorSubsumer.cpp ^

To enable Logger:
1. edit the file: "cryptominisat-2.5.1/config.h"
   and replace line 79 "/* #undef STATS_NEEDED */" with "#define STATS_NEEDED 1"
2. add the following line to the swipl-ld call above:
Logger.cpp ^
   
   
To enable Gaussian elimination:
1. edit the file: "cryptominisat-2.5.1/solver/constants.h"
   and remove the '//' from the begining of line 37 "#define USE_GAUSS"
2. add the following lines to the swipl-ld call above:
Gaussian.cpp ^
MatrixFinder.cpp ^

If you want to enable both you can use only the following line:
*.cpp ^
instead of specific calls to each cpp file.

endComments:
