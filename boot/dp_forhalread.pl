%% This file contains the definitions for the predicates I can't define
%% in HAL for native_hal_read - hProlog version
%% 
%% Bart Demoen Wed Dec  4 23:35:50 CET 2002

%% this version is for hProlog with builtin string (and char) type
%% Mon Jan 20 20:09:03 CET 2003


% :- typedef string.

%---------------------------------------------------------------------------%
%% :- pred input_stream_name(string,T,T).
%% :- mode input_stream_name(out,in,out) is det.
%---------------------------------------------------------------------------%
(io):input_stream_name(F,L,L) :-
	seeing(File),
	name(File,FLi),
	ilist_to_string(FLi,F).



%---------------------------------------------------------------------------%
%% :- pred get_line_number(int,T,T).
%% :- mode get_line_number(out,in,out) is det.
%---------------------------------------------------------------------------%
(io):get_line_number(X,L,L) :-
	nb_getval('$linenumber',N),
	sysh:get_line_number(Y),
	(N == [] ->
	    X = Y
	;
	    X is N + Y
	).



%---------------------------------------------------------------------------%
%% :- pred read_char_code(okchar,T,T).
%% :- mode read_char_code(out,in,out) is det.
%---------------------------------------------------------------------------%
(io):read_char_code(Out,L,L) :-
	(get0(Char) ->
	    (Char == -1 ->
		Out = eof
	    ;
		(Char = 13 ->
		    Out = ok(0' )
		;
		    Out = ok(Char)
		)
	    )
	;
	    error("get_char_code failed")
	).


%---------------------------------------------------------------------------%
%% :- pred set_line_number(int,T,T).
%% :- mode set_line_number(in,in,out) is det.
%---------------------------------------------------------------------------%
(io):set_line_number(X,L,L) :-
	sysh:get_line_number(CN),
	Diff is X - CN,
	nb_setval('$linenumber',Diff).


%---------------------------------------------------------------------------%
%% :- pred putback_char(int,T,T).
%% :- mode putback_char(in,in,out) is det.
%---------------------------------------------------------------------------%
(io):putback_char(C,L,L) :-
	sysh:ungetichar(C).


%---------------------------------------------------------------------------%
%% :- pred list:rev(list(T), list(T),list(T)).
%% :- mode list:rev(in,in,out) is det.
%---------------------------------------------------------------------------%
% rev/3 only internally needed
% 
%---------------------------------------------------------------------------%

list:rev([],I,I).
list:rev([X|R],I,O) :- list:rev(R,[X|I],O).

%---------------------------------------------------------------------------%
%% :- pred string:append(string, string, string).
%% :- mode string:append(in,in,out) is det.
%---------------------------------------------------------------------------%
% string:append/3
% 
%---------------------------------------------------------------------------%

string:append(S1,S2,S3) :-
	string_to_ilist(S1,Il1),
	string_to_ilist(S2,Il2),
	list:app(Il1,Il2,Il3),
	ilist_to_string(Il3,S3).

list:app([],S,S).
list:app([X|R],S,[X|T]) :- list:app(R,S,T).

%---------------------------------------------------------------------------%
%% :- pred string:rev_int_list_to_float_lowlevel(list(int), float).
%% :- mode string:rev_int_list_to_float_lowlevel(in, out) is semidet.
%---------------------------------------------------------------------------%

string:rev_int_list_to_float_lowlevel(L,F) :-
	list:rev(L,[],R),
	atomic2list(F1,R,0),
	float(F1),
	F = F1.

%---------------------------------------------------------------------------%
%% :- pred string:rev_int_list_to_string(list(int), string).
%% :- mode string:rev_int_list_to_string(in, out) is det.
%---------------------------------------------------------------------------%

string:rev_int_list_to_string(L,S) :-
	list:rev(L,[],Lrev),
	ilist_to_string(Lrev,S).

%---------------------------------------------------------------------------%
%% :- pred string:int_list_to_string(list(int), string).
%% :- mode string:int_list_to_string(in, out) is det.
%---------------------------------------------------------------------------%

string:int_list_to_string(Ilist,String) :- ilist_to_string(Ilist,String).

%---------------------------------------------------------------------------%
%% :- pred compiler_interface:make_empty_dict(dict(int,list(name-int)))
%% :- compiler_interface:mode make_empty_dict(out) is det.
%---------------------------------------------------------------------------%
% make_empty_dict/1
% 
%---------------------------------------------------------------------------%

compiler_interface:make_empty_dict(dict(0,[])).


%---------------------------------------------------------------------------%
%% :- pred compiler_interface:add_name(name,varset,varset,int).
%% :- mode compiler_interface:add_name(in,in,out,out) is det.
%---------------------------------------------------------------------------%
% add_name/4
% 
%---------------------------------------------------------------------------%

compiler_interface:add_name(Name,dict(N,OldAssoc),NewAssoc,M) :-
	M is N + 1,
	NewAssoc = dict(M,[Name - M|OldAssoc]).


%---------------------------------------------------------------------------%
%% :- pred compiler_interface:get_first_variable_with_name(varset,name,int).
%% :- mode compiler_interface:get_first_variable_with_name(in,in,out) is det.
%---------------------------------------------------------------------------%
% get_first_variable_with_name/3
% 
%---------------------------------------------------------------------------%

compiler_interface:get_first_variable_with_name(dict(_,Assoc),Name,N) :-
	my_memb(Name-N,Assoc).

my_memb(X,[X|_]).
my_memb(X,[_|R]) :- my_memb(X,R).


%---------------------------------------------------------------------------%
%% :- pred ops:dynamic_op_table(string, category, specifier, int).
%% :- mode ops:dynamic_op_table(in, in, out, out) is semidet.
%---------------------------------------------------------------------------%

ops:dynamic_op_table(Op,Cat,Spec,Prec) :-
	nb_getval('$operators',DynOps),
	DynOps = [op_table(Op1,Cat1,Spec1,Prec1)|DynOpsRest],
	opmember(Op,Cat,Spec,Prec,Op1,Cat1,Spec1,Prec1,DynOpsRest),
	!. % this cut should not be there

opmember(Op,Cat,Spec,Prec,Op,Cat,Spec,Prec,_).
opmember(Op,Cat,Spec,Prec,_,_,_,_,[op_table(Op1,Cat1,Spec1,Prec1)|DynOpsRest]) :-
	opmember(Op,Cat,Spec,Prec,Op1,Cat1,Spec1,Prec1,DynOpsRest).

