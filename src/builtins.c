#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include "toinclude.h"
#include "mach_macros.h"
#include "builtins.h"
#include <dlfcn.h>
#include <unistd.h>


#include <stdio.h>

extern dlong peep_spec;
extern dlong dump_reset_profiling(char *filename);
extern dlong bigint_to_string(dlong *p, char **pstring, struct machine* machp, dlong base);
extern void bigint_free_string(char *pc, struct machine* machp);
extern type_opcode next_instruction(codep);
extern void fatal_message(char *);

#ifdef WITH_READLINE
#include <readline/readline.h>
#include <readline/history.h>

static char *readlinebuf = 0;
static char *currentreadlinechar = 0;

static dlong reasonable_readlinebuf(char *b)
{
  dlong i;
  if (! b) return(0);
  i = strlen(b);
  while (*b == ' ') { b++; i--; }
  if (i == 0) return(0);
  if ((*b == ';') && (i == 1)) return(0);
  return(1);
} /* reasonable_readlinebuf */

static dlong get0readline(char *pr)
{
  dlong i;

  if (! readlinebuf)
    {
      readlinebuf = readline(pr);
      if (reasonable_readlinebuf(readlinebuf))
	add_history(readlinebuf);
      currentreadlinechar = readlinebuf;
    }

  if (! currentreadlinechar)
    return(-1);

  i = *currentreadlinechar;
  if (i == 0)
    {
      free(readlinebuf);
      readlinebuf = 0;
      return(10); /* nl hopefully */
    }

  currentreadlinechar++;
  return(i);

} /* get0readline */

#define has_readline 1

#else
static dlong get0readline(char *pr)
{
  return(0);
} /* get0readline */

#define has_readline 0

#endif

/* prolog_flag/3
   argument 1: atom that indicates about which flag we are talking
               current possibilities:
	               prompt
                       verbose
		       very_verbose
   argument 2: unified with old value of the flag
   argument 3: new value is set to this value
*/

extern void warn_unknown();

extern dlong print_instr;

extern dlong atom_verbose, atom_very_verbose, atom_prompt,
  symbol_dot_2, atom_repshar;
extern dlong atom_yes, atom_no;
extern dlong precise_profile_call_site_opt;

dlong Cprolog_flag(dlong *p1, dlong *p2, dlong *p3, struct machine *machp)
{
  dlong oldvalue;
  dlong *newvalue;

  if (! is_atom(p1)) return(0);
  if (! is_ref(p2)) return(0);

  if ((dlong)p1 == atom_prompt)
    { 
      *p2 = value_prompt; deref(p3);
      if (! is_atom(p3))
	return(0);
      value_prompt = (dlong)p3;
      return(1);
    }

  if ((dlong)p1 == atom_verbose)
    { 
      if (verbose_mode)
	*p2 = atom_yes;
      else
	*p2 = atom_no;
      deref(p3);
      if ((dlong)p3 == atom_yes)
	{
	  verbose_mode = 1;
	  return(1);
	}
      if ((dlong)p3 == atom_no)
	{
	  verbose_mode = 0;
	  return(1);
	}
	return(0);
    }


  if ((dlong)p1 == atom_very_verbose)
    { 
      if (very_verbose_mode)
	*p2 = atom_yes;
      else
	*p2 = atom_no;
      deref(p3);
      if ((dlong)p3 == atom_yes)
	{
	  verbose_mode = very_verbose_mode = 1;
	  return(1);
	}
      if ((dlong)p3 == atom_no)
	{
	  verbose_mode = very_verbose_mode = 0;
	  return(1);
	}
	return(0);
    }


  if ((dlong)p1 == atom_repshar)
    { 
      *p2 = make_smallint(machp->intro_representation_sharing);
      deref(p3);
      if (! is_smallint(p3)) return(0);
      machp->intro_representation_sharing = get_smallint(p3);
      return(1);
    }

  return(0);
} /* Cprolog_flag */



dlong builtin_2C(char **mes, struct machine *machp)
{
  dlong *p1, *p2, *p3, *p4;
  dlong i;
  dlong index;
  void *ref;

  p1 = (dlong *)args(1); deref(p1);
  p2 = (dlong *)args(2); deref(p2);
  p3 = (dlong *)args(3); deref(p3);
  p4 = (dlong *)args(4); deref(p4);

  i = get_smallint(p1);
  if (i == 0) /* cinter(0,+ObjectName,-Reference,666) */
    { /* dlopen */ 
      index = get_atom_index(p2);
      ref = dlopen(symtab[index].name, RTLD_NOW);    /* aig */
      if (!ref)
	{
	  *mes = dlerror();
	  printf("not ok: %s\n",*mes);
	  return(0);
	}
      build_smallint(p3, (dlong)ref);
      return(1);
    }

  if (i == 1) /* cinter(1,+Predicate,+FunctionName,+Reference) */
    { /* dlsym */
      dlong (*c_function)(struct machine*);
      char *error;
      if (! is_struct(p2))
	{
	  *mes = "predicate not a compound term";
	  return (0); /* later also atoms */
	}
      p2 = get_struct_pointer(p2);
      if (!is_atom(p3))
	{
	  *mes = "function name not an atom";
	  return (0);
	}
      index = get_atom_index(p3);
      ref = (void *)get_smallint(p4);
      c_function = dlsym(ref,  symtab[index].name);    /* aig */
      if ((error = dlerror()) != NULL)
	{
	  *mes = error;
	  return (0);
	}
      make_foreign_pred(*p2,(dlong *)c_function);
      return(1);
    }

  return(0);
} /* builtin_2C */


codep builtin_metacall(dlong *p, struct machine *machp)
{
  dlong arity;
  dlong symbol1;
  codep pc;

  if (is_struct(p))
    {
      p = get_struct_pointer(p);
      symbol1 = get_funct_symb(*p);
      pc = main_entrypoint(symbol1);
      arity = symtab[symbol1].arity;
      // arity = get_funct_arity(*p);
      if (arity < 6)
        { dlong *q = machp->Areg;
          q[1] = p[1];
          q[2] = p[2];
          q[3] = p[3];
          q[4] = p[4];
          q[5] = p[5];
          return(pc);
        }

      p += arity;
      while (arity)
	{
	  args(arity) = *p;
	  p--;
	  arity--;
	}
      return(pc);
    }

  if (is_atom(p))
    symbol1 = get_atom_index(p);
  else if (is_list(p))
    {
      p = get_list_pointer(p);
      args(1) = *p;
      args(2) = *(p+1);
      symbol1 = symbol_dot_2;
    }
  else
    return(0);

  pc = main_entrypoint(symbol1);

  return(pc);
} /* builtin_metacall */



dlong htoggles_2_builtin(dlong areg1, dlong areg2,struct machine *machp)
{
  dlong *p1, *p2;
  dlong mode, index;
  codep cp;

  p1 = (dlong *)args(areg1); deref(p1);
  p2 = (dlong *)args(areg2); deref(p2);

  mode = get_smallint(p1);
  switch (mode)
    {
    case 17:
      precise_profile_call_site_opt = 1;
      return(1);
    case 22:
      precise_profile_call_site_opt = 0;
      return(1);
    case 23:
      return(dump_reset_profiling(symtab[get_atom_index(p2)].name));
    case 25:
      /* one more overloading of htoggles/2 */
      print_instr = 1;
      return(1);
    case 26:
      /* one more overloading of htoggles/2 */
      warn_unknown();
      return(1);
    case 27:
      /* one more overloading of htoggles/2 */
      {dlong i = get_smallint(p2);
	if ((i < 0) || (i > 3)) return(0);
	peep_spec = i;
	return(1);
      }
    case 66:
      /* one more overloading of htoggles/2 */
      {
	dlong i;
	if (is_struct(p2))
	  {
	    p2 = get_struct_pointer(p2);
	    i = get_funct_symb(*p2);
	    if (is_protected(i)) return(1);
	    return(0);
	  }
	if (is_atom(p2))
	  {
	    i = get_atom_index(p2);
	    if (is_protected(i)) return(1);
            return(0);
	  }
	if (is_list(p2)) return(1);
	return(0);
      }
    }
  return(1);
} /* builtin_htoggles_2 */


/********************************************************/
/*                                                      */
/*  Some IO predicates                                  */
/*                                                      */
/*                                                      */
/********************************************************/

/*-------------------------------------------------------------------------*/
static dlong find_free_file_slot(struct machine *machp)
{
  /* strategy is:
     if there is a free slot at nr_open_files, give that
     otherwise look for a free slot starting from 0
  */

  dlong i = machp->nr_open_files;

  if (i < MAX_NR_OPEN_FILES)
    return(i);

  /* i == MAX_NR_OPEN_FILES */
  while (i > 0)
    {
      i--;
      if (0 == machp->openfiles[i].fp) return(i);
    }

  return(-1);
} /*find_free_file_slot */

dlong low_open_stream_3(dlong *p1, dlong *p2, struct machine *machp)
{
  dlong i, j;
  char *c, *filename;
  FILE *f;

  deref(p1); deref(p2);
  filename = symtab[get_atom_index(p1)].name;
  i = get_smallint(p2);
  if (i == 0) c = "r";
  else if (i == 1) c = "w";
  else if (i == 2) c = "a";
  else if (i == 4) c = "R";
  else return(-1);
  //  else if (i == 5) c = "W";

  j = find_free_file_slot(machp);
  if (j < 0)
    return(-1);
  if (j == machp->nr_open_files)
    machp->nr_open_files += 1;
  if (i < 3)
    {
      f = fopen(filename, c);
      if (! f) return(-1);
      machp->openfiles[j].fp = f;
    }
  else
    machp->openfiles[j].fp = (FILE *)1;
  machp->openfiles[j].colnr = 0;
  machp->openfiles[j].line_number = 0;
  machp->openfiles[j].f_symbol = get_atom_index((dlong)p1);
  machp->openfiles[j].ungetnr = 0;
  machp->openfiles[j].opentype = i;
  machp->openfiles[j].endoffile = 0;
  return(j);

} /* low_open_stream_3 */

dlong low_close_stream1(dlong *p1, struct machine *machp)
{
  dlong j;
  FILE *f;
  dlong tip;

  deref(p1);
  if (! is_smallint(p1)) return(0);
  j = get_smallint(p1);

  if ((j < 0) || (j >= machp->nr_open_files)) return(0);

  f = machp->openfiles[j].fp;
  tip = machp->openfiles[j].opentype;
  if (tip != FILE_READ_ATOM)
    fclose(f);

  machp->openfiles[j].ungetnr = 0;
  machp->openfiles[j].opentype = 0;
  machp->openfiles[j].fp = 0;
  machp->openfiles[j].f_symbol = 0;
  machp->openfiles[j].line_number = 0;
  if ((j+1) == machp->nr_open_files)
    machp->nr_open_files = j;
  return(1);
} /* low_close_stream1 */

void write_with_quotes(char *pc, FILE *fp)
{
  fprintf(fp,"'");
  while (*pc)
    put_with_escape(fp,*(pc++));
  fprintf(fp,"'");
} /* write_with_quotes */

static void low_write_atomic(dlong *term, dlong *how, FILE *fp, dlong doubleform, struct machine* machp)
{
  dlong index;
  deref(term); deref(how);

  if (is_atom(term))
    {
      char *pc;
      dlong index;

      index = get_atom_index(term);
      pc = symtab[index].name;
      if ((dlong)how == atom_on)
	{
	  fprintf(fp,"'");
	  while (*pc)
	    put_with_escape(fp,*(pc++));
	  fprintf(fp,"'");
	}
      else
        fprintf(fp,"%s",pc);
      return;
    }

  if (is_smallint(term))
    {
      dlong i;
      
      i = get_smallint(term);
      fprintf(fp,"%ld",i);
      return;
    }

  if (is_real(term))
    {
      dfloat f;
      
      f = get_real(term);
      if (doubleform == nil_atom)
#ifdef BITS32
	fprintf(fp,"%.17e",f);
#else
        fprintf(fp,"%.17Le",f);
#endif
      else
	{
	  char *s;
	  s = symtab[get_atom_index(doubleform)].name;
	  fprintf(fp,s,f);
	}
      return;
    }

  if (is_char(term))
    {
      char c = get_character(term);
      if ((dlong)how == atom_on)
	{
	  fprintf(fp,"'");
	  put_with_escape(fp,c);
	  fprintf(fp,"'");
	}
      else
        fprintf(fp,"%c",c);
      return;
    }

  if (is_string(term))
    {
      char *s = get_string_val(term);
      dlong l = get_string_length(term);
      
      if ((dlong)how == atom_on)
	{
	  fprintf(fp,"\"");
	  while (l--)
	    put_with_escape(fp,*(s++));
	  fprintf(fp,"\"");
	}
      else
        while (l--)
          fprintf(fp,"%c",*(s++));
      return;
    }

  if (is_bigint(term))
    {
      dlong i;
      char *pc;
      if (bigint_to_string(term, &pc, machp, 10))
	{
	  fprintf(fp,"%s",pc);
	  bigint_free_string(pc,machp);
	}
      else
	fprintf(fp," <unprintably long bigint :-(\n");
      return;
    }

  fprintf(fp," unprintable<%ld> ",(dlong)term);
} /* low_write_atomic */

void low_write_atomic3(dlong *p1, dlong *p2, struct machine *machp, dlong doubleform)
{
  low_write_atomic(p1,p2,machp->openfiles[machp->file_out_nr].fp,doubleform, machp);
} /* low_write_atomic3 */


/*-------------------------------------------------------------------------*/
static INLINE dlong matching_open_mode(dlong m1, dlong m2)
{
  if (m1 == FILE_ANY)
    return(1);
  if (m1 == FILE_READ) 
    return((m2 == FILE_READ));
  return((m2 != FILE_READ));
} /*matching_open_mode */


dlong file2stream(dlong filename, dlong openmode, struct machine *machp)
{
  dlong i;
  i = machp->nr_open_files;
  while (i > 0)
    {
      i--;
      if ((machp->openfiles[i].f_symbol == filename) && 
	  (matching_open_mode(openmode,machp->openfiles[i].opentype)))
	return(i);
    }
  return(-1);
} /* file2stream */

static INLINE dlong get_file(dlong *p, dlong how, struct machine *machp, FILE **fp)
{
  /* p should be an integer and indicate a "good" file pointer */
  dlong i;

  if (! is_smallint(p)) return(0);
  i = get_smallint(p);
  if ((i < 0) || (i >= machp->nr_open_files))
    return(0);
  if (how == FILE_WRITE)
    {
      /* it has to be WRITE or APPEND */
      if ((machp->openfiles[i].opentype != FILE_WRITE) &&
          (machp->openfiles[i].opentype != FILE_APPEND)) return(0);
    }
  else
    /* how == FILE_READ */
    if ((machp->openfiles[i].opentype != FILE_READ) 
	&& (machp->openfiles[i].opentype != FILE_READ_ATOM))
      return(0);

  *fp = machp->openfiles[i].fp;
  return(1);
} /* get_file */

dlong low_write_atomic4(dlong *p1, dlong *p2, dlong *p3, struct machine *machp, dlong doubleform)
{
  FILE *f;
  dlong ret;

  deref(p1);
  ret = get_file(p1,FILE_WRITE,machp,&f);
  if (! ret) return(0);
  low_write_atomic(p2,p3,f,doubleform, machp);
  return(1);
} /* low_write_atomic4 */


dlong low_write_var(dlong *p, struct machine *machp, FILE *f)
{
  if (is_ref(p))
    {
      dlong index;

      index = p-machp->begin_heap;
      fprintf(f,"_%ld",index);
      return(1);
    }

  if (is_attributed(p))
    {
      dlong index;
      
      p = get_attvar_pointer(p);
      index = p-machp->begin_heap;
      fprintf(f,"AttVar%ld",index);
      return(1);
    }

  return(0);
} /* low_write_var */

dlong low_write_var1(dlong *p, struct machine *machp)
{
  return(low_write_var(p, machp, machp->openfiles[machp->file_out_nr].fp));
} /* low_write_var */


dlong low_write_var2(dlong *p1, dlong *p2, struct machine *machp)
{
  FILE *f;
  dlong ret;
  ret = get_file(p1,FILE_WRITE,machp,&f);
  if (! ret) return(0);
  return(low_write_var(p2, machp, f));
} /* low_write_var */


static dlong write_prompt = 1;

/*-------------------------------------------------------------------------*/
static INLINE void zero_file(struct file_struct *p)
{
  p->fp = 0;
  p->f_symbol = 0;
} /*zero_file */


/*-------------------------------------------------------------------------*/
static INLINE dlong low_get0_from_atom(dlong fi, struct machine *machp)
{
  dlong l,cn;
  dlong i;
  char *pc;

  i = machp->openfiles[fi].ungetnr;
  if (i)
    {
      machp->openfiles[fi].ungetnr = i-1;
      i = machp->openfiles[fi].c1;
      machp->openfiles[fi].c1 = machp->openfiles[fi].c2;
      if (i == EOF)
        i = -1;
      return(i);
    }

  pc = symtab[machp->openfiles[fi].f_symbol].name;
  cn = machp->openfiles[fi].colnr;
  l = strlen(pc);
  if (cn >= l) return(-1);
  l = pc[cn];
  (machp->openfiles[fi].colnr)++;
  return(l);
} /*low_get0_from_atom */

/*-------------------------------------------------------------------------*/
dlong low_current_eof0(dlong curin, struct machine *machp)
{
  return(machp->openfiles[curin].endoffile);
} /*low_current_eof0 */

/*-------------------------------------------------------------------------*/
static INLINE dlong low_get0(dlong curin, FILE *f, struct machine *machp)
{
  dlong i, scr;
  char *pr;

  i = machp->openfiles[curin].ungetnr;
  if (i)
    {
      machp->openfiles[curin].ungetnr = i-1;
      i = machp->openfiles[curin].c1;
      machp->openfiles[curin].c1 = machp->openfiles[curin].c2;
      if (i == EOF)
	{
	  i = -1;
	  machp->openfiles[curin].endoffile = 1;
	}
      return(i);
    }

  scr = isatty(fileno(f));
  pr = symtab[get_atom_index(value_prompt)].name;
  if (has_readline && scr)
    i = get0readline(pr);
  else
    {
      if (scr && write_prompt)
	{
	  fprintf(stdout,"%s",pr);
	  write_prompt = 0;
	}
      i = getc(f);
    }

  if (i == EOF)
    {
      i = -1;
      machp->openfiles[curin].endoffile = 1;
    }
  else
    if (i == '\n')
      {
        machp->openfiles[curin].line_number ++;
        value_prompt = atom_default_prompt;
	if (scr)
	  write_prompt = 1;
      }

  return(i);
} /* low_get0 */

dlong low_get0_1(dlong curin, struct machine *machp)
{
  dlong i;
  FILE *f;

  if (machp->openfiles[curin].opentype == FILE_READ_ATOM)
    return(low_get0_from_atom(curin,machp));
  f = machp->openfiles[curin].fp;
  return(low_get0(curin,f,machp));
} /* low_get0_1 */

dlong low_get0_stream_2(dlong *str, struct machine *machp)
{
  FILE *f;
  dlong ret;

  ret = get_file(str,FILE_READ,machp,&f);

  if (! ret) return(-2);
  ret = get_smallint(str);
  if (machp->openfiles[ret].opentype == FILE_READ_ATOM)
    return(low_get0_from_atom(ret,machp));
  return(low_get0(ret,f,machp));
} /* low_get0_stream_2 */


dlong low_get_open_stream3(dlong *p1, dlong *p2, dlong *p3, struct machine *machp)
{
  dlong i;
  deref(p1);
  if ((!is_smallint(p1)) || (!is_ref(p2)) || (!is_ref(p3)))
    return(-1);
  i = get_smallint(p1);
  if (i < 0) return(-1);
  while (i < machp->nr_open_files)
    {
      if (machp->openfiles[i].fp)
	return(i);
      i++;
    }
  return(-1);
} /* low_get_open_stream3 */

dlong low_put2(dlong *p1, dlong *p2, struct machine *machp)
{
  dlong i,ret;
  FILE *f;

  deref(p1); deref(p2);
  if (! is_smallint(p2))
    return(0);
  i = get_smallint(p2);
  ret = get_file(p1,FILE_WRITE,machp,&f);
  if (! ret) return(0);
  fputc(i,f);
  return(1);
} /* low_put2 */


/*-------------------------------------------------------------------------*/
dlong low_see_1(dlong *p, struct machine *machp)
{
  FILE *F;
  dlong symbol, i;

  deref(p);
  if (is_atom(p))
    {
      symbol = get_atom_index(p);

      i = file2stream(symbol,FILE_READ,machp);
      if (i < 0)
	{
	  i = find_free_file_slot(machp);
	  if (i < 0) return(0);
	  F = fopen(symtab[symbol].name,"r");
	  if (F == 0) return(0);
	  machp->openfiles[i].opentype = FILE_READ;
	  machp->openfiles[i].fp = F;
	  machp->openfiles[i].f_symbol = symbol;
	  machp->openfiles[i].line_number = 1;
	  machp->openfiles[i].ungetnr = 0;
	  machp->openfiles[i].endoffile = 0;
	  current_in_nr = i;
	  if (i == machp->nr_open_files)
	    machp->nr_open_files++;
	  return(1);
	}

      current_in_nr = i;
      return(1);
    }

  if (! is_smallint(p))
    return(0);

  /* see/1 was called with a stream as argument - it must be open and for read */

  i = get_smallint(p);
  if ((i < 0) || (i >= machp->nr_open_files))
    return(0);
  if ((machp->openfiles[i].opentype != FILE_READ)
      && (machp->openfiles[i].opentype != FILE_READ_ATOM))
    return(0);
  current_in_nr = i;
  return(1);
} /*low_see_1 */

/*-------------------------------------------------------------------------*/
dlong low_tell_1(dlong *p, struct machine *machp, char* type)
{
  FILE *F;
  dlong symbol, i;

  deref(p);
  if (is_atom(p))
    {
      symbol = get_atom_index(p);

      i = file2stream(symbol,FILE_WRITE,machp);
      if (i < 0)
	{
	  i = find_free_file_slot(machp);
	  if (i < 0) return(0);
	  F = fopen(symtab[symbol].name,type);
	  if (F == 0) return(0);
	  machp->openfiles[i].opentype = FILE_WRITE;
	  machp->openfiles[i].fp = F;
	  machp->openfiles[i].f_symbol = symbol;
	  current_out_nr = i;
	  if (i == machp->nr_open_files)
	    machp->nr_open_files++;
	  return(1);
	}

      current_out_nr = i;
      return(1);
    }

  if (! is_smallint(p))
    return(0);

  /* tell/1 was called with a stream as argument - it must be open and for write */

  i = get_smallint(p);
  if ((i < 0) || (i >= machp->nr_open_files))
    return(0);
  if (machp->openfiles[i].opentype != FILE_WRITE)
    return(0);
  current_out_nr = i;
  return(1);
} /*low_tell_1 */

/*-------------------------------------------------------------------------*/
void low_seen_0(struct machine *machp)
{
  dlong i;
  if (current_in == stdin)
    return;

  i = current_in_nr;
  fclose(current_in);
  current_in_nr = 0;  /* 0 = stdin */
  if (i == (machp->nr_open_files-1))
    machp->nr_open_files -= 1;
  zero_file(&(machp->openfiles[i]));
} /*low_seen_0 */


/*-------------------------------------------------------------------------*/
void low_told_0(struct machine *machp)
{
  dlong i;
  if (current_out == stdout)
    return;

  i = current_out_nr;
  fclose(current_out);
  current_out_nr = 1;  /* 1 = stdout */
  if (i == (machp->nr_open_files-1))
    machp->nr_open_files -= 1;
  zero_file(&(machp->openfiles[i]));
} /*low_told_0 */


/*-------------------------------------------------------------------------*/
dlong low_close1(dlong *p, struct machine *machp)
{
  dlong symbol, i;
  deref(p);
  if (! is_atom(p))
    return(0);
  symbol = get_atom_index(p);
  i = file2stream(symbol,FILE_ANY,machp);
  if (i < 3) /* we don't want std* to be closed */
    return(0);
  fclose(machp->openfiles[i].fp);
  if (i == current_in_nr)
    current_in_nr = 0; /* stdin */
  else
    if (i == current_out_nr)
      current_out_nr = 1;  /* stdout */
  if (i == (machp->nr_open_files-1))
    machp->nr_open_files -= 1;
  zero_file(&(machp->openfiles[i]));
  return(1);
} /*low_close1 */

/*-------------------------------------------------------------------------*/
void show_info_cp(struct machine *machp, dlong *upto)
{
  codep alt;
  dlong *b;
  dlong at, ar, m;

  b = breg;
  if (upto > machp->begin_cp)
    upto = machp->begin_cp;
  if (b < upto)
    fprintf(stderr,"/* choice points are left by */\n");

  while (b < upto)
    {
      alt = ((CP_P) b)->failurecont;
      b = ((CP_P) b)->b;
      if (b > upto) return;
      find_pred_index(alt,&at);
      m = symtab[at].module;
      if (m)
	fprintf(stderr,"%s:",symtab[m].name);
      fprintf(stderr,"%s/%ld",symtab[at].name,symtab[at].arity);
      if (points_to_or_retry_trust(alt))
	fprintf(stderr," in disjunction");
      fprintf(stderr,".\n");
    }

} /*show_info_cp */


/*-------------------------------------------------------------------------*/
void back_trace(struct machine *machp)
{
  dlong a, ar;
  dlong *e;
  codep cp;
  char *s;
  dlong *startls;

  startls = begin_ls;

  e = machp->E;
  cp = machp->CONTP;

  while (e != startls)
    {
      find_name_arity(cp,&a,&ar);
      s = symtab[get_atom_index(a)].name;
      fprintf(stderr,"%s/%ld\n",s,ar);
      cp = (codep)(*(e+1));
      e = (dlong *)*e;
    }

} /* back_trace */

/*-------------------------------------------------------------------------*/
dlong ancestors(struct machine *machp)
{
  dlong a, ar;
  dlong *e;
  codep cp;
  char *s;
  dlong *startls;
  dlong *h, *lim;
  dlong retval;

  startls = begin_ls;

  e = machp->E;
  cp = machp->CONTP;
  h = machp->H;
  retval = make_list(h);
  lim = machp->shadow_limit_heap;

  while (e != startls)
    {
      find_name_arity(cp,&a,&ar);
      //      *h = a; h++; *h = make_list(h+1); h++; *h = a; h++; make_list(h+1); h++;
      *h = a; h++; *h = make_list(h+1); h++; *h = make_smallint(ar); h++; *h = make_list(h+1); h++;
      cp = (codep)(*(e+1));
      e = (dlong *)*e;
      if (h > lim) break;
    }

  h[-1] = nil_atom;
  machp->H = h;
  return(retval);
} /* ancestors */

/*-------------------------------------------------------------------------*/
dlong check_bindings(struct machine *machp, dlong *upto)
{
  dlong *trnow, *trthen;
  dlong *hthen;

  trnow = trreg;
  trthen = ((CP_P)upto)->tr;
  hthen = ((CP_P)upto)->h;

  while (trnow > trthen)
    {
      dlong *trailed_place;
      trnow -=2;
      trailed_place = (dlong *)trnow[1];
      if (trailed_place < hthen) return(0);
    }
  
  return(1);
} /*check_bindings */


