%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%   ____          _
%%  / ___|___   __| | ___
%% | |   / _ \ / _` |/ _ \
%% | |__| (_) | (_| |  __/
%%  \____\___/ \__,_|\___|
%% 
%%  ____                            _
%% |  _ \  ___ _ __   ___ _ __   __| | ___ _ __   ___ _   _
%% | | | |/ _ \ '_ \ / _ \ '_ \ / _` |/ _ \ '_ \ / __| | | |
%% | |_| |  __/ |_) |  __/ | | | (_| |  __/ | | | (__| |_| |
%% |____/ \___| .__/ \___|_| |_|\__,_|\___|_| |_|\___|\__, |
%%            |_|                                     |___/
% 
%% author: Tom Schrijvers, K.U.Leuven
%% 
%% * deadcode(File,PredicateSpecs)
%% 
%%     prints the predicate specs of the predicates defined in File
%%     that are not used (directly or indirectly) by the given predicates
%%     (only based on syntactic dependencies)
%%
%% * missingcode(File)
%% 
%%     prints the predicate specs of the predicates that are called
%%     but neither defined nor imported
%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% To Do:
%%
%%     - properly deal with module qualified calls
%%
%%     - deal with regularly imported modules
%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(dependency,[deadcode/2,missingcode/1]).

:- use_module(library(lists)).
:- use_module(library(pairlist)).

:- ['language/dcg.pl'].

deadcode(File,Starts) :-
	readfile(File,Clauses),
	exported_predicates(Clauses,Exports),
	findall(C, ( member(C,Clauses), C \= (:- _) , C \= (?- _)), Cs),
	findall(F/A - L, ( member(X2,Cs), dependency:hb(X2,T,B), functor(T,F,A), dependency:extract_predicates(B,L,[])), Fs),
	pairup(Keys,_Values,Fs), !,
	remove_duplicates(Keys,Predicates),
	append(Exports,Starts,Roots),
	scan_deadcode(Roots,Fs,Predicates,Unused),
	sort(Unused,Us),
	findall(_,(member(X,Us), write(X), nl), _).

missingcode(File) :-
	readfile(File,Clauses),
	findall(Import,( member((:- use_module(library(Lib))), Clauses), dependency:exported_lib_predicates(Lib,Import)), ImportLists),
	flatten(ImportLists,Imports),
	findall(C, ( member(C,Clauses), C \= (:- _) , C \= (?- _)), Cs),
	findall(F/A - L, ( member(X2,Cs), dependency:hb(X2,T,B), functor(T,F,A), dependency:extract_predicates(B,L,[])), Fs),
	pairup(Keys,Values,Fs), !,
	remove_duplicates(Keys,LocalPredicates),
	flatten(Values,Calls),
	append(LocalPredicates,Imports,Predicates),
	scan_missing(Calls,Predicates,Missing),
	sort(Missing,SortedMissing),
	remove_duplicates(SortedMissing,UniqueMissing),
	findall(_,(member(X,UniqueMissing), write(X), nl), _).
	
exported_lib_predicates(Lib,Exports) :-
	sysh:make_absolute_file_name(0,Lib,AFile),
	atom_concat(AFile,'.pl', LibFile),
	readfile(LibFile,LibClauses),
	exported_predicates(LibClauses,Exports).

exported_predicates(Clauses,Exports) :-
	( member( (:- module(_, Exports)), Clauses) ->
		true
	;
		Exports = []
	).

hb(C,H,B) :-
	( C = (H :- B) ->
		true
	;
		C = H,
		B = true
	).

scan_deadcode([],_,Unused,Unused).
scan_deadcode([FA|FAs],Dict,Ps,Unused) :-
	( member(FA,Ps) ->
		delete_first(Ps,FA,NPs),	
		findall(X, lookup_any(Dict,FA,X), Deps),
		flatten(Deps,D),
		append(D,FAs,NFAs)
	;
		NPs = Ps,
		NFAs = FAs
	),
	scan_deadcode(NFAs,Dict,NPs,Unused).

scan_missing([],_Dict,[]).
scan_missing([FA|FAs],Dict,Missing) :-
	( member(FA,Dict) ->
		Missing = T
	;
		Missing = [FA|T]
	),
	scan_missing(FAs,Dict,T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
extract_predicates(!,L,L) :- ! .
extract_predicates(_ < _,L,L) :- ! .
extract_predicates(_ = _,L,L) :- ! .
extract_predicates(_ =.. _ ,L,L) :- ! .
extract_predicates(_ =:= _,L,L) :- ! .
extract_predicates(_ == _,L,L) :- ! .
extract_predicates(_ > _,L,L) :- ! .
extract_predicates(_ \= _,L,L) :- ! .
extract_predicates(_ \== _,L,L) :- ! .
extract_predicates(_ is _,L,L) :- ! .
extract_predicates(arg(_,_,_),L,L) :- ! .
extract_predicates(atom_concat(_,_,_),L,L) :- ! .
extract_predicates(atomic(_),L,L) :- ! .
extract_predicates(b_getval(_,_),L,L) :- ! .
extract_predicates(call(_),L,L) :- ! .
extract_predicates(compound(_),L,L) :- ! .
extract_predicates(copy_term(_,_),L,L) :- ! .
extract_predicates(del_attr(_,_),L,L) :- ! .
extract_predicates(fail,L,L) :- ! .
extract_predicates(functor(_,_,_),L,L) :- ! .
extract_predicates(get_attr(_,_,_),L,L) :- ! .
extract_predicates(length(_,_),L,L) :- ! .
extract_predicates(nb_setval(_,_),L,L) :- ! .
extract_predicates(nl,L,L) :- ! .
extract_predicates(nonvar(_),L,L) :- ! .
extract_predicates(once(G),L,T) :- !,
	( nonvar(G) ->
		extract_predicates(G,L,T)
	;
		L = T
	).
extract_predicates(op(_,_,_),L,L) :- ! .
extract_predicates(prolog_flag(_,_),L,L) :- ! .
extract_predicates(prolog_flag(_,_,_),L,L) :- ! .
extract_predicates(put_attr(_,_,_),L,L) :- ! .
extract_predicates(read(_),L,L) :- ! .
extract_predicates(see(_),L,L) :- ! .
extract_predicates(seen,L,L) :- ! .
extract_predicates(setarg(_,_,_),L,L) :- ! .
extract_predicates(tell(_),L,L) :- ! .
extract_predicates(term_variables(_,_),L,L) :- ! .
extract_predicates(told,L,L) :- ! .
extract_predicates(true,L,L) :- ! .
extract_predicates(var(_),L,L) :- ! .
extract_predicates(write(_),L,L) :- ! .
extract_predicates((G1,G2),L,T) :- ! ,
	extract_predicates(G1,L,T1),
	extract_predicates(G2,T1,T).
extract_predicates((G1->G2),L,T) :- !,
	extract_predicates(G1,L,T1),
	extract_predicates(G2,T1,T).
extract_predicates((G1;G2),L,T) :- !,
	extract_predicates(G1,L,T1),
	extract_predicates(G2,T1,T).
extract_predicates(\+ G, L, T) :- !,
	extract_predicates(G, L, T).
extract_predicates(findall(_,G,_),L,T) :- !,
	extract_predicates(G,L,T).
extract_predicates(bagof(_,G,_),L,T) :- !,
	extract_predicates(G,L,T).
extract_predicates(_^G,L,T) :- !,
	extract_predicates(G,L,T).
extract_predicates(_:Call,L,T) :- !,
	extract_predicates(Call,L,T).
extract_predicates(Call,L,T) :-
	( var(Call) ->
		L = T
	;
		functor(Call,F,A),
		L = [F/A|T]
	).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% File Reading

readfile(File,Declarations) :-
	see(File),
	prolog_flag(verbose,X),
	( X == yes ->
		write(' * loading file '),
		write(File),
		write('...'),
		nl
	;
		true
	),
	( readcontent(Declarations) ->
		seen,
		( X == yes ->
			write('   finished loading file '),
			write(File),
			nl
		;
			true
		)
	;
		seen,
		( X == yes ->
			write('   ERROR loading file '),
			write(File),
			nl
		;
			true
		),
		fail
	).

readcontent(C) :-
	read(X),
	( X = (:- op(Prec,Fix,Op)) ->
		op(Prec,Fix,Op)
	;
		true
	),
	( dcg_rule(X,NX) ->
		true
	;
		X = NX
	),
	( X == end_of_file ->
		C = []
	;
		C = [NX | Xs],
		readcontent(Xs)
	).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

