?- ['sat/satsolver.pl'].

?- Unary=[A,B,C,D], Cnf=[[A,-B],[B,-C],[C,-D],[A,B,Z],[D,C,-Z],[-A,-D]], satsolver:sat(Cnf).
