% Author: Amit Metodi
% Last Updated: 11/01/2012

:- module(atLeastOne, [ ]).
:- use_module('auxLiterals').


atLeastOneSimplify(Pbits,NewPbits,FoundOne,Changed):-!,
    atLeastOneSimplify(Pbits,TPbits,FoundOne,Changed,NeedSort),
    (FoundOne==1 -> NewPbits=[] ;
    (NeedSort==1 ->
        sort(TPbits,TPbits2),
        Changed=1,
        atLeastOneSimplify(TPbits2,NewPbits,FoundOne,_) ;
    NewPbits=TPbits
    )).

atLeastOneSimplify([(X1,Xop1)|PWbits],NewPWbits,FoundOne,Changed,NeedSort):-!,
    (ground(X1) ->
        Changed=1,
        (X1*Xop1 =:= 1 ->
            FoundOne=1
        ;
            Changed=1,
            atLeastOneSimplify(PWbits,NewPWbits,FoundOne,1,NeedSort)
        ) ;
    (var(X1) ->
        atLeastOneSimplify(PWbits,(X1,Xop1),NewPWbits,FoundOne,Changed,NeedSort) ;
    lit2plit(X1,X1n,X1op),
    Xop1n is Xop1 * X1op,
    atLeastOneSimplify(PWbits,(X1n,Xop1n),NewPWbits,FoundOne,Changed,NeedSort)
    )).
atLeastOneSimplify([],[],_,_,_):-!.

atLeastOneSimplify([(X2,X2op)|PWbits],(X1,X1op),NewPWbits,FoundOne,Changed,NeedSort):-!,
    (ground(X2) ->
        Changed=1,
        (X2*X2op =:= 1 ->
            FoundOne=1
        ;
            atLeastOneSimplify(PWbits,(X1,X1op),NewPWbits,FoundOne,1,NeedSort)
        ) ;
    (var(X2) ->
        (X2 == X1 ->
             Changed=1,
             (X2op==X1op ->
                  atLeastOneSimplify(PWbits,(X1,X1op),NewPWbits,FoundOne,1,NeedSort)
             ;
                  FoundOne=1
             )
        ;
            (X2 @< X1 ->
                  NeedSort=1,
                  NewPWbits=[(X2,X2op)|MorePWbits],
                  atLeastOneSimplify(PWbits,(X1,X1op),MorePWbits,FoundOne,Changed,NeedSort)
            ;
                  NewPWbits=[(X1,X1op)|MorePWbits],
                  atLeastOneSimplify(PWbits,(X2,X2op),MorePWbits,FoundOne,Changed,NeedSort)
            )
        )
    ;
        lit2plit(X2,X2n,X2opb),
        X2opn is X2op*X2opb,
        atLeastOneSimplify([(X2n,X2opn)|PWbits],(X1,X1op),NewPWbits,FoundOne,Changed,NeedSort)
    )).

atLeastOneSimplify([],PWx,[PWx],_,_,_):-!.
