\chapter{Finite Atom Domains Constraint Solver}

\section{Introduction}

This finite domain constraint solver is a simple solver,
defined over domains of atoms and functors,
with the notion of equality and inequality constraints.

It will keep track of the domains of variables presented to it using its constraints,
and unifies variables with their only possible value
if such a unique value can be determined from the constraint store.
Alternatively, it will fail when it detects inconsistencies.
Note that it does not detect all inconsistencies.
For an explanation, see section \ref{currentissues}.

It has been implemented for hProlog, using its attributed variables,
in the context of the author's graduation paper.

\section{Usage and Syntax}

The usage of this solver is analogous to other (h)Prolog packages,
by invoking \texttt{use\_module(lib / solvers / fdcs)}.

This allows the user to define constraints over variables using \texttt{domain/2},
\texttt{not\_equal/2} and \texttt{all\_different/1}.

The current state of the variables involved in the constraint store can be checked
by using \texttt{get\_current\_domain/2} and \texttt{label/1}.

\subsection{domain/2}

Syntax: \texttt{domain(?X,+Domain)}

This constraint allows the user the define a variable's initial domain of possible values.
It can be called more than once for the same variable, which will constraint
the resulting domain to be the intersection of both input domains.

It is allowed to call the constraint with ground variables, since the solver can ground variables
without the user explicitly being aware of this. Domain must be a list of non-variable terms.

Users should keep in mind that this defines a constraint,
and therefor should not be used to inspect to current domain of a variable.
\texttt{get\_current\_domain/2} can be used for this purpose.

\subsection{not\_equal/2}

Syntax: \texttt{not\_equal(?X,?Y)}

This constrains both arguments to be different.
In effect, this means that if one of those variables is,
or becomes, ground, it is removed from the domain of the other.

The user should keep in mind that no consistency checking is done by this constraint
while both variables stay unground.

\subsection{all\_different/1}

Syntax: \texttt{all\_different(+List)}

This constrains all elements of list to be pairwise different.

This is, in its current implementation, simply a shorthand for
\texttt{not\_equal/2} constraints over all elements of List.
List can contain both variables and ground terms.

Efficient algorithms for consistency checking concerning only the variables in List exist,
but are currently not implemented.

\subsection{get\_current\_domain/2}

Syntax: \texttt{get\_current\_domain(-X,-Domain)}

This allows a user to inspect the current resulting domain of a variable, by unifying it with Domain.

It will fail if X is a non-variable or no initial domain has yet been defined for X.

\subsection{label/1}

Syntax: \texttt{label(+List)}

This constrains all the variables in list to be ground to
a legal value with respect to all the previously called constraints.

This assigns to each variable in List a value in its domain,
and backtracks over each possible combination of values.

It will fail if List contains a variable for which no initial domain
has been defined yet.

Users should keep in mind that the values assigned are only guaranteed to be consistent
with respect to the other variables in List.% For a more thorough explanation, see
% section \ref{currentissues}.

\section{Example of Usage}

The use of fdcs is fairly straightforward. As an example,
what follows is a simpler version of the ZEBRA problem.

Consider three pets next to each other: Spot the cat, Porthos the dog and Livingston the fish.
They each like one of three foods: cheese, salmon and flakes.
They each belong to either Picard, Archer or Data. The problem is
to assign to each pet the right owner and food based on a number of clues.

\begin{enumerate}

\item Livingston is no cannibal, he doesn't eat salmon.
\item Data is Spot's owner.
\item Picard's pet is located to the right of Data's.
\item Cats are allergic to cheese, dogs are allergic to flakes.
\item The pet that likes flakes is to the right of the one that likes cheese.

\end{enumerate}

A pet is represented by a three membered list [name,owner,food].
They are stored in another three membered list which implicitly contains their
orientation.

The clues can be divided in two lists, those that contain direct information (1,2,4)
and those that will give rise to backtracking (3,5).

First we must initialize the matrix using \texttt{domain/2} and \texttt{all\_different/1}.
After that, we can first apply the direct clues, and backtrack over the
remaining possibilities with the other rules.

\begin{verbatim}

solve(Pets) :-
    Pets = [ [      spot,      SpotOwner,      SpotFood],
             [   porthos,   PorthosOwner,   PorthosFood],
             [livingston,LivingstonOwner,LivingstonFood] ],
    
    owners(SpotOwner,PorthosOwner,LivingstonOwner),
     foods( SpotFood, PorthosFood, LivingstonFood),
    
    SpotOwner = data,
    
    not_equal(LivingstonFood,salmon),
    not_equal(      SpotFood,cheese),
    not_equal(   PorthosFood,flakes),
    
    left_right([_,data,     _],[_,picard,     _],Pets),
    left_right([_,   _,cheese],[_,     _,flakes],Pets).

owners([X,Y,Z]) :-
    Domain = [picard,data,archer],
    domain(X,Domain),
    domain(Y,Domain),
    domain(Z,Domain),
    all_different([X,Y,Z]).

foods([X,Y,Z]) :-
    Domain = [cheese,salmon,flakes],
    domain(X,Domain),
    domain(Y,Domain),
    domain(Z,Domain),
    all_different([X,Y,Z]).

% left_right(L, R, X) is true when L is to the immediate left of R in list X

left_right(L, R, [L, R | _]).
left_right(L, R, [_ | X]) :- left_right(L, R, X).

\end{verbatim}

\section{Current Issues} \label{currentissues}

As noted several times before, this finite domain constraint solver
has some issues with consistency checking. What follows is a short
attempt to clarify what this means, and an brief explanation of
why this is so.

\subsection{Example of inconsistency}

As an example, consider the following situation:
\begin{verbatim}
domain([X,Y,Z],[a,b,c]),
domain(T,[a,b,c,d]),
all_different([X,Y,Z,T]).
\end{verbatim}

For these constraints to be consistent, the solver would have to deduce
that T must be d.

It was also noted that \texttt{label/1} is guaranteed only to be consistent
with respect to the variables it is actually labelling. For example, when,
in the above situation, labelling only T will allow it to take any of the four
values. Labelling all variables will constrain T to always be d.

In truth, labelling T and one of the other variables also correctly constrains
T to be d, since, when T takes one of the other values, in combination with the second
variable taking a value, only one value remains for the two other variables remains,
and they will both attempt to unify with this value, which will fail.

\subsection{Explanation}

The main reason for not checking consistency is simply because no
efficient algorithms are known to keep a full set of the constraints
presented above consistent.

Algorithms the make the domains of variables involved in a single
\texttt{all\_different/1} constraint exist, and are fairly efficient.
However, it is perfectly possible to create a situation similar to
\texttt{all\_different/1} by using \texttt{not\_equal/2}. Simply
detecting such situations is a daunting task in itself, especially
considering that it has to be rechecked every time a new constraint
is presented to the solver.

The choice not to implement the algorithm for \texttt{all\_different/1}
was made because of several reasons.
\begin{itemize}
\item To keep the domain of the involved variables truly consistent,
the algorithm must be rerun every time a change is made to one of its variables.
\item In many cases, the algorithm will perform superfluous work since,
in essence, all it does is bring the information about the inconsistency up
early. Of no combination of valuations for its variables exist, eventually, none will
be found later on when handling other constraints.
\item It seems wrong to force the user to use the more expensive
consistency checking \texttt{all\_different/1}. An inexpensive implementation must
be presented in case the user knows for certain no inconsistencies are present (as is
the case in the example of usage presented above).
\end{itemize}

Of course, such arguments merely dictate the existence of a non-checking \texttt{all\_different/1}.
There is no real reason not to implement one that does constrain the domains to be consistent,
and, time permitting, it will surely be implemented.
