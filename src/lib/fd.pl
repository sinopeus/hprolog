:- module(fd,
	  [
	   domain/2,
	   alldifferent/1, all_different/1,
	   'abs(X-Y)#<A'/3,
	   labeling/1
	  ]).

domain(X,Dom) :-
	(
	  var(X) ->
	  domain2([X],Dom)
	;
	  X = [_|_] ->
	  domain2(X,Dom)
	;
	  del2(Dom,X,_)
	).


domain2([],_).
domain2([X|R],Dom) :-
	(var(X) ->
	    (get_attr(X,domain,OldDom) ->
		intersect(OldDom,Dom,NewDom),
		NewDom = [F|T],
		(T == [] ->
		    X = F
		;
		    put_attr(X,domain,NewDom)
		)
	    ;
		put_attr(X,domain,Dom)
	    )
	;
	    del2(Dom,X,_)
	),
	domain2(R,Dom).

intersect(I1,I2,O) :-
	(I1 == [] ->
	    O = []
	;
	    I2 == [] ->
	    O = []
	;
	    I1 = [A1|R1],
	    I2 = [A2|R2],
	    (A1 == A2 ->
		O = [A1|OR],
		intersect(R1,R2,OR)
	    ;
		(A1 < A2 ->
		    intersect(R1,I2,O)
		;
		    intersect(I1,R2,O)
		)
	    )
	).

alldifferent(L) :- alldifferent(L,[]).
all_different(L) :- alldifferent(L,[]).

alldifferent([],_).
alldifferent([X|R],S) :-
%%	freeze(X,fd:notin(X,R,S)),
	(var(X) ->
	    (get_attr(X,alldifferent,Old) ->
		New = conj(notin(R,S),Old),
		put_attr(X,alldifferent,New)
	    ;
		put_attr(X,alldifferent,notin(R,S))
	    )
	;
	    notin(X,R,S)
	),
	alldifferent(R,[X|S]).

alldifferent:attr_unify_hook(notin(R,S),X) :-
	notin(R,X), notin(S,X).
alldifferent:attr_unify_hook(conj(A,B),X) :-
	alldifferent:attr_unify_hook(A,X),
	alldifferent:attr_unify_hook(B,X).

notin(X,R,S) :- notin(R,X), notin(S,X).

notin([],_).
notin([A|R],X) :-
	exclude(X,A),
	notin(R,X).

exclude(X,A) :-
	(attvar(A) ->
	    get_attr(A,domain,Dom),
	    del1(Dom,X,NewDom),
	    NewDom = [F|T],
	    (T == [] ->
		A = F
	    ;
		put_attr(A,domain,NewDom)
	    )
	;
	    (A == X -> fail ; true)
	).

domain:attr_unify_hook(Dom,Val) :- member(Val,Dom), !.

del1(In,X,Out) :- del2(In,X,Out), !.
del1(In,_,In).

del2([A|R],X,O) :-
	(A == X ->
	    O = R
	;
	    O = [A|O1],
	    del2(R,X,O1)
	).

labeling([]).
labeling([X|R]) :-
	(attvar(X) ->
	    get_attr(X,domain,D),
	    member(X,D)
	;
	    true
	),
	labeling(R).


'abs(X-Y)#<A'(A,X,Y) :-
        (
	  nonvar(X) ->
	  absXYgtAfix(A,X,Y)
	;
	  nonvar(Y) ->
	  absXYgtAfix(A,Y,X)
	;
	  Goal = fd:absXYgtA(A,X,Y,_),
	  freeze(X,Goal), freeze(Y,Goal)
	).

absXYgtA(A,X,Y,Done) :-
	(var(Done) ->
	    % X and/or Y is fixed now
	    Done = 1,
	    (
	      nonvar(X) ->
	      absXYgtAfix(A,X,Y)
	    ;
	      nonvar(Y) ->
	      absXYgtAfix(A,Y,X)
	    ;
	      abs(X-Y) > A
	    )
	;
	    true
	).

absXYgtAfix(A,X,Y) :- % X is fixed, Y could be
	(
	  var(Y) ->
	  get_attr(Y,domain,YDom),
	  reduce_absXYgtAfix(A,X,YDom,NewYDom),
	  NewYDom = [F|Rest],
	  (Rest == [] ->
	      Y = F
	  ;
	      put_attr(Y,domain,NewYDom)
	  )
	;
	  abs(X-Y) > A
	).

reduce_absXYgtAfix(A,X,YDom,NewYDom) :-
	findall(Y,(member(Y,YDom),abs(X-Y) > A),NewYDom).


/*--------------------------------------------------------------*/

'aX=bY+c'(A,X,B,Y,C) :-
         (nonvar(X) ->
	     Y is (A*X-C)//B
	 ;
	     nonvar(Y) ->
	     X is (B*Y+C)//A
	 ;
	     Goal = fd:'aX=bY+c'(A,X,B,Y,C,_),
	     freeze(X,Goal), freeze(Y,Goal)
	 ).

'aX=bY+c'(A,X,B,Y,C,Done) :-
	(var(Done) ->
	    Done = 1,
	    (var(X) ->
		X is (B*Y+C)//A
	    ;
		Y is (A*X-C)//B
	    )
	;
	    true
	).

