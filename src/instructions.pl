%% gen_opcodes/0 generates dp_numbering.pl - only the numbers of the instructions
%%                                                  for compiler.pl
%%                       opcodes.h  - with numbers of instructions
%%                                    lengths of all possible instructions
%%                                    length of every instruction
%%                                    offsets of all arguments - per instruction
%% 
%%                       instr_names.c - an array with the instr names
%%                       instr_labels.c - an array with adresses for labels

%% gen_loading/0 generates loading.h with the cases of the switch statement
%%               for loading the instructions - atoms, structs and floats are done
%% 	         properly, lables will be done in fill_labels

%% gen_builtins/0 generates builtinnumbers.h and builtinnumbers.pl
%%                 because there must be correspondance between the compiler
%% 	           written in Prolog and the C emulator
%%                 the basis is the predicate inline_builtin_forgen

%% gen_fill_labels/0 generates fill_labels.h for inclusion in symtab.c
%%                   relocates the labels


%% gen_empty_mach/0 generates an empty emulator in new_machine.h
%%                  only the arguments are gotten
%%                  the rest of the code must be filled by hand

%% gen_lowlevel_builtins/0 generates the "self-recursive" definitions
%%                         one must be able to meta call them AND
%%                         they cannot be inlined ever
%%                         call/1 cannot be inlined either
%%                         file dp_lowlevel_builtins.pl
%%                         for the builtins written in C
%% 		           I had forgotten too often to do them by hand :-)

%% gen_profiling_calls/0 generates the switch needed to pass over the
%%                       code of a predicate and print out call-site
%% 		      related profiling info


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


write_additional_string_len(I,PC) :-
	write_additional_string_len(I,PC,PC).

write_additional_string_len(I,PC1,PC2) :-
	needs_additional_string_len(I),
	writel(['        ', PC1, ' += extra_strlen_len_', I, '( ', PC2, ');', nl]).
write_additional_string_len(I,PC1,PC2) :-
	needs_additional_bigint_len(I),
	writel(['        ', PC1, ' += extra_bigint_len_', I, '( ', PC2, ');', nl]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gen_profiling_calls :-
	tell('profile.h'),
	write_header,
	instr(Instr,_Args,_Bytes),
	imem(I,Instr),
	write_case(I),
	(I == profile_instr ->
	    writel(['         countlow = *(count_type*)(arg_place(startcode_pred,1,profile_instr));']), nl,
	    writel(['         counthigh = *(count_type*)(arg_place(startcode_pred,2,profile_instr));']), nl,
	    writel(['         *(count_type*)(arg_place(startcode_pred,1,profile_instr)) = 0;']), nl,
	    writel(['         *(count_type*)(arg_place(startcode_pred,2,profile_instr)) = 0;']), nl
	;
	    true
	),
	(I == switchonbound ->
	    writel(['         startcode_pred += sizeof(dlong) * (dlong)*(short*)(arg_place(startcode_pred,2,switchonbound)); ',nl])
	;   
	    true
	),
	(
	  write_additional_string_len(I,startcode_pred), fail
	;
	  true
	),
	writel(['             startcode_pred += ',I,'_len;',nl]),
	(is_followed_by_active_yvars(I) ->
	    writel(['             startcode_pred += active_yvar_len;', nl])
	;
	    true
	),
	writel(['             break;',nl]),
	fail.

gen_profiling_calls :-
	told, fail.
gen_profiling_calls.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gen_lowlevel_builtins :-
 	tell('dp_lowlevel_builtins.pl'),
	write_header,
	local_write(':- sysmodule(sysh,['),nl,
	inline_builtin_forgen(PName,_CName,Arity,Info),
	once(local_member(module(Mod),Info)),
	Mod = user,
	local_write('                ('), local_write(PName), local_write(')/'), local_write(Arity), local_write(','),nl,
	fail.
gen_lowlevel_builtins :-
	local_write('                (! /0)'), nl,
	fail.
gen_lowlevel_builtins :-
	local_write('               ]).'),nl,nl,
	fail.
gen_lowlevel_builtins :-
	inline_builtin_forgen(PName,_CName,Arity, Info),
	\+(fake_builtin(PName,Arity)),
	write_rec_def(PName,Arity,Info),
	fail.
gen_lowlevel_builtins :-
        told,
	fail.
gen_lowlevel_builtins.


write_rec_def(PName,Arity,Info) :-
	once(local_member(module(Mod),Info)),
	local_write(PName),
	(Mod == user ->
	    true
	;
	    local_write('__M'), local_write('_'), local_write(Mod)
	),
	(Arity > 0 ->
	    local_write('('), writeargs(Arity,') :- ',0'A)
	;
	    local_write(' :- ')
	),
	local_write(''''),
	local_write(PName),
	((Mod == sysh ; local_member(unsafeheap,Info)) ->
	    local_write('__M'), local_write('_sysh')
	;
	    true
	),
	(Arity > 0 ->
	    local_write('''('),
	    writeargs(Arity,').',0'A)
	;
	    local_write('''.')
	),
	nl.

writeargs(N,End,Var) :-
	put(Var), 
	(N == 1 ->
	    local_write(End)
	;
	    M is N - 1,
	    W is Var+1,
	    local_write(','),
	    writeargs(M,End,W)
	).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_builtin_errors :-
	tell('builtin_errors.c'),
	write_header,
	inline_builtin_forgen(_PName,CName,Arity),
	local_write('error_'), local_write(CName), local_write('_'), local_write(Arity), local_write(':'),nl,
	local_write('{'),nl,
	local_write('  dlong a1, a2, a3, a4;'),nl,
	local_write('  locpc -= builtin_'), local_write(CName), local_write('_'), local_write(Arity), local_write('_len;'), nl,
	(Arity > 0 ->
	    local_write('  a1 = args(*(areg_type *)(arg_place(locpc,1,builtin_'),
	    local_write(CName), local_write('_'), local_write(Arity), local_write(')));'),nl
	;
	    true
	),
	(Arity > 1 ->
	    local_write('  a2 = args(*(areg_type *)(arg_place(locpc,2,builtin_'),
	    local_write(CName), local_write('_'), local_write(Arity), local_write(')));'),nl
	;
	    true
	),
	(Arity > 2 ->
	    local_write('  a3 = args(*(areg_type *)(arg_place(locpc,3,builtin_'),
	    local_write(CName), local_write('_'), local_write(Arity), local_write(')));'),nl
	;
	    true
	),
	(Arity > 3 ->
	    local_write('  a4 = args(*(areg_type *)(arg_place(locpc,4,builtin_'),
	    local_write(CName), local_write('_'), local_write(Arity), local_write(')));'),nl
	;
	    true
	),
	(Arity > 0 ->
	    local_write('  args(1) = a1;'), nl
	;
	    true
	),
	(Arity > 1 ->
            local_write('  args(2) = a2;'), nl
	;
	    true
	),
	(Arity > 2 ->
            local_write('  args(3) = a3;'), nl
	;
	    true
	),
	(Arity > 3 ->
            local_write('  args(4) = a4;'), nl
	;
	    true
	),
	local_write('  glob_nrbuiltin = '), local_write(CName), local_write('_'), local_write(Arity),
	local_write(';'), nl,
	local_write('  glob_arity = '), local_write(Arity), local_write(';'), nl,
	local_write('  goto error_builtin;'),nl,
	local_write('}'),nl,nl,
	fail.
gen_builtin_errors :-
	specialized_inline_builtin_forgen(_,Arity,CName,OrigCName,SpecArgs),
	local_write('error_'), local_write(CName), local_write('_'), local_write(Arity), local_write(':'),nl,
	local_write('{'),nl,
	local_write('  dlong a1, a2, a3, a4;'),nl,
	local_write('  locpc -= builtin_'), local_write(CName), local_write('_'), local_write(Arity), local_write('_len;'), nl,
	(Arity > 0 ->
	    (checkspecial(1,SpecArgs) ->
		true
	    ;
		local_write('  a1 = args(*(areg_type *)(arg_place(locpc,1,builtin_'),
		local_write(CName), local_write('_'), local_write(Arity), local_write(')));'),nl
	    )
	;
	    true
	),
	(Arity > 1 ->
	    (checkspecial(2,SpecArgs) ->
		true
	    ;
		local_write('  a2 = args(*(areg_type *)(arg_place(locpc,2,builtin_'),
		local_write(CName), local_write('_'), local_write(Arity), local_write(')));'),nl
	    )
	;
	    true
	),
	(Arity > 2 ->
	    (checkspecial(3,SpecArgs) ->
		true
	    ;
		local_write('  a3 = args(*(areg_type *)(arg_place(locpc,3,builtin_'),
		local_write(CName), local_write('_'), local_write(Arity), local_write(')));'),nl
	    )
	;
	    true
	),
	(Arity > 3 ->
	    (checkspecial(4,SpecArgs) ->
		true
	    ;
		local_write('  a4 = args(*(areg_type *)(arg_place(locpc,4,builtin_'),
		local_write(CName), local_write('_'), local_write(Arity), local_write(')));'),nl
	    )
	;
	    true
	),
	(Arity > 0 ->
	    local_write('  args(1) = a1;'), nl
	;
	    true
	),
	(Arity > 1 ->
            local_write('  args(2) = a2;'), nl
	;
	    true
	),
	(Arity > 2 ->
            local_write('  args(3) = a3;'), nl
	;
	    true
	),
	(Arity > 3 ->
            local_write('  args(4) = a4;'), nl
	;
	    true
	),
	local_write('  glob_nrbuiltin = '), local_write(OrigCName), local_write('_'), local_write(Arity),
	local_write(';'), nl,
	local_write('  glob_arity = '), local_write(Arity), local_write(';'), nl,
	local_write('  goto error_builtin;'),nl,
	local_write('}'),nl,nl,
	fail.
gen_builtin_errors :-
        told,
        fail.
gen_builtin_errors.

checkspecial(N,SpecArgs) :-
	    local_member(N,SpecArgs), !,
	    local_write('  a'), local_write(N), local_write(' = *lochreg = (dlong)lochreg; lochreg++;\n').

gen_builtins :-
	tell('builtinnumbers.h'),
	findall(CName-Arity,(inline_builtin_forgen(_,CName,Arity);
			     specialized_inline_builtin_forgen(_,Arity,CName,_)),L),
	write_builtin_h(L,0),
	told,
	fail.
gen_builtins.


write_builtin_arity([A|R],N) :-
	writel(['           ',A,',',nl]),
	NN is N + 1,
	write_builtin_arity(R,NN).

write_builtin_h([],_).
write_builtin_h([Name-Arity|R],N) :-
	writel(['#define ',Name, '_', Arity, '      ', N, nl]),
	M is N + 1,
	write_builtin_h(R,M).

write_builtin_pl([],_).
write_builtin_pl([Name-Arity-Info|R],N) :-
	local_write('inline_builtin('),
	((local_member(module(sysh),Info) ; local_member(unsafeheap,Info)) ->
	    Mod2 = sysh
	;
	    Mod2 = user
	),
	write_with_module(Name,Mod2),
	local_write(','), local_write(Arity), local_write(','), local_write(N), local_write(').'), nl,
	M is N + 1,
	write_builtin_pl(R,M).

/*======================================================================*/
/* inline_builtin_forgen: prolog name, C name, arity, extra info
/*======================================================================*/

%% normal inlined builtins like var/2 are exported - belong to user -
%% and must have a recursive definition so that they can be metacalled
%% builtins whose heap usage is known at compile time are like that, for
%% instance: functor/3, =../2

%% some builtins are unsafe for gc, i.e. one can't know at compile time
%% how much heap they will need - examples are sort/2, atomic2list, ilist_to_string/2 ...
%% such builtins should not be inlined, because that would need activexvars
%% (or other modifications to the compiler)
%% the solution is as follows: sort/2 is a builtin, but unsafe for gc;
%% then make sure that user:sort/2 is visible and that it is defined as
%% user:sort(A,B) :- somemod:sort(A,B).
%% and that somemod:sort/2 is the inlined one - no other code should use somemod:sort/2
%% the C-implementation of sort/2 can now count on Areg[1..2]  being the only live
%% argument registers and no live perm vars
%% we encode this in the extra info as unsafeheap
%% if we also add a definition somemod:sort(A,B) :- somemod:sort(A,B).
%% we can even metacall somemod:sort/2
%% the meta call is in this category because of CP
%% 
%% some inlined builtins should not be in user, perhaps because they should not
%% be in the manual and/or because they exist only for the implementation of
%% visible builtins; examples are load_file/2, write_var/1 ...
%% say bar/2 is such a builtin
%% we can have a definition
%% somemod:bar(X,Y) :- somemod:bar(X,Y). so that it can be metacalled
%% everywhere it is called (apart from the file the above clause is in)
%% must explicitly modlue qualify it
%% we encode this in the extra info as module(somemod)
%% 
%% we have chosen sysh for the somemod
%% 
%% the two attributes are in a list, so that it will be easy to use them
%% and extend them
%%

inline_builtin_forgen(PrologName, CName, Arity) :-
		     inline_builtin_forgen(PrologName, CName, Arity,_).

inline_builtin_forgen(traverselist_rev, traverselist_rev, 0, [module(sysh)]).
inline_builtin_forgen(traverselist_rev1, traverselist_rev1, 0, [module(sysh)]).


inline_builtin_forgen(load_file,load_file,2, [module(sysh)]).
%% inline_builtin_forgen(make_absolute_file_name,make_absolute_file_name,4, [module(sysh)]).
inline_builtin_forgen(getenv, getenv, 2, [module(sysh)]).
inline_builtin_forgen(write_var, write_var,1, [module(sysh)]).
inline_builtin_forgen(write_var, write_var,2, [module(sysh)]).
inline_builtin_forgen(write_atomic,write_atomic, 3, [module(sysh)]).
inline_builtin_forgen(write_atomic,write_atomic, 4, [module(sysh)]).

inline_builtin_forgen(call,call,1, [module(user), unsafeheap]).
inline_builtin_forgen(call, call, 2, [module(user), unsafeheap]).
inline_builtin_forgen(call, call, 3, [module(user), unsafeheap]).
inline_builtin_forgen(call, call, 4, [module(user), unsafeheap]).
inline_builtin_forgen(call, call, 5, [module(user), unsafeheap]).
inline_builtin_forgen(call, call, 6, [module(user), unsafeheap]).
inline_builtin_forgen(call, call, 7, [module(user), unsafeheap]).
inline_builtin_forgen(call, call, 8, [module(user), unsafeheap]).
inline_builtin_forgen(call, call, 9, [module(user), unsafeheap]).
inline_builtin_forgen(call, call,10, [module(user), unsafeheap]).

inline_builtin_forgen(print_heap, print_heap, 0, [module(user)]).

inline_builtin_forgen(mapcall, mapcall, 3, [module(user), unsafeheap]).

inline_builtin_forgen(get_line_number, get_line_number, 1, [module(sysh)]).
inline_builtin_forgen(ungetichar, ungetichar, 1, [module(sysh)]).
inline_builtin_forgen(internalstat, internalstat, 5, [module(sysh)]).

inline_builtin_forgen(reset_cont, reset_cont, 0, [module(sysh)]).
inline_builtin_forgen(new_call_continuation, new_call_continuation, 0, [module(sysh)]).

inline_builtin_forgen(wln, wln, 1, [module(sysh)]).


inline_builtin_forgen(current_reg, current_reg, 2,  [module(sysh)]).
inline_builtin_forgen(setEinB, setEinB, 0,  [module(sysh)]).
inline_builtin_forgen(end_new_shift, end_new_shift, 3, [module(sysh)]).

inline_builtin_forgen(unwind_stack_callcc, unwind_stack_callcc, 0, [module(sysh)]).
inline_builtin_forgen(after_callcc, after_callcc, 0, [module(sysh)]).

inline_builtin_forgen(after_intercept, after_intercept, 0, [module(sysh)]).
inline_builtin_forgen(find_intercept, find_intercept, 2, [module(sysh)]).

inline_builtin_forgen(nextEP, nextEP, 3, [module(sysh)]).
inline_builtin_forgen(get_one_tailbody, get_one_tailbody, 3, [module(sysh), unsafeheap]).
inline_builtin_forgen(points_to_callcc, points_to_callcc, 1, [module(sysh)]).
inline_builtin_forgen(call_tailbody, call_tailbody, 1, [module(sysh)]).

inline_builtin_forgen(construct_delcont, construct_delcont, 1, [module(sysh), unsafeheap]).


inline_builtin_forgen(asm, asm, 1, [module(sysh)]).

inline_builtin_forgen(unwind_stack, unwind_stack, 0, [module(sysh)]).
inline_builtin_forgen(end_catch, end_catch, 1, [module(sysh)]).
inline_builtin_forgen(cut1fail, cut1fail, 0, [module(sysh)]).

inline_builtin_forgen(copy_term, copy_term, 2, [module(user), unsafeheap]).
inline_builtin_forgen(copy_term_nat, copy_term_nat, 2, [module(user), unsafeheap]).
inline_builtin_forgen(back_trace, back_trace, 0, [module(sysh), unsafeheap]).
inline_builtin_forgen(ancestors, ancestors, 1, [module(sysh)]).


inline_builtin_forgen(varfreeze, varfreeze, 2, [module(sysh)]).
inline_builtin_forgen(put_attr_lowlevel_init, put_attr_lowlevel_init, 2, [module(sysh)]).
inline_builtin_forgen(put_attr_lowlevel, put_attr_lowlevel, 2, [module(sysh)]).
inline_builtin_forgen(put_attr_lowlevel, put_attr_lowlevel, 3, [module(sysh)]).
inline_builtin_forgen(get_attr_lowlevel, get_attr_lowlevel, 2, [module(sysh)]).
inline_builtin_forgen(get_attr_lowlevel, get_attr_lowlevel, 3, [module(sysh)]).

inline_builtin_forgen(add_attr, add_attr, 4, [module(sysh)]).

inline_builtin_forgen(term_variables, term_variables, 3, [module(user), unsafeheap]).
inline_builtin_forgen(all_term_variables, all_term_variables, 3, [module(user), unsafeheap]).
inline_builtin_forgen(findall_init, findall_init, 2, [module(sysh)]).
inline_builtin_forgen(findall_add, findall_add,2, [module(sysh)]).
inline_builtin_forgen(findall_get_solutions, findall_get_solutions,3, [module(sysh), unsafeheap]).
inline_builtin_forgen(findall_cleanup, findall_cleanup,1, [module(sysh)]).

inline_builtin_forgen(set_copy_heap_barrier, set_copy_heap_barrier, 1, [module(user)]).
inline_builtin_forgen(current_heap, current_heap, 1, [module(user)]).

inline_builtin_forgen(internal_key_sort, internal_key_sort, 2, [module(sysh), unsafeheap]).
inline_builtin_forgen(internal_sort, internal_sort, 2, [module(sysh), unsafeheap]).
inline_builtin_forgen(internal_numbervars, internal_numbervars, 2, [module(sysh)]).
inline_builtin_forgen(garbage_collect, garbage_collect, 0, [module(user), unsafeheap]).
inline_builtin_forgen(internal_prolog_flag, internal_prolog_flag, 3, [module(sysh)]).

inline_builtin_forgen(ilist_to_string, ilist_to_string, 2, [module(user),unsafeheap]).
inline_builtin_forgen(string_to_ilist, string_to_ilist, 2, [module(user),unsafeheap]).

inline_builtin_forgen(new_symbol, new_symbol, 1, [module(sysh)]).

inline_builtin_forgen(file_time_stamp, file_time_stamp, 3, [module(sysh)]).
inline_builtin_forgen(needs_recompile, needs_recompile, 2, [module(sysh)]).
inline_builtin_forgen(file_exists, file_exists, 1, [module(user)]).

inline_builtin_forgen(nonnumber,nonnumber, 1, [module(sysh)]).
inline_builtin_forgen(disable_cp, disable_cp, 1, [module(sysh)]).
inline_builtin_forgen(smash_cp, smash_cp, 3, [module(sysh)]).

%% inline_builtin_forgen(warn_var,warn_var, 1, [module(user)]).
%% inline_builtin_forgen(warn_nonvar,warn_nonvar, 1, [module(user)]).
%% inline_builtin_forgen(warn_atom,warn_atom, 1, [module(user)]).
%% inline_builtin_forgen(warn_integer,warn_integer, 1, [module(user)]).

inline_builtin_forgen(var,var, 1, [module(user)]).
inline_builtin_forgen(nonvar,nonvar, 1, [module(user)]).
inline_builtin_forgen(atom,atom, 1, [module(user)]).
inline_builtin_forgen(integer,integer, 1, [module(user)]).
inline_builtin_forgen(bigint, bigint, 1, [module(user)]).
inline_builtin_forgen(bigint_print, bigint_print, 1, [module(user)]).
inline_builtin_forgen(float,float, 1, [module(user)]).
inline_builtin_forgen(number,number, 1, [module(user)]).
inline_builtin_forgen(atomic,atomic, 1, [module(user)]).
inline_builtin_forgen(compound,compound, 1, [module(user)]).

inline_builtin_forgen(term_hash, term_hash, 2, [module(user)]).

inline_builtin_forgen(compare,compare,3, [module(user)]).
inline_builtin_forgen(has_a_definition,has_a_definition, 1, [module(user)]).
inline_builtin_forgen(callable,callable, 1, [module(user)]).
inline_builtin_forgen(ground, ground, 1, [module(user)]).
inline_builtin_forgen(pred_erase,pred_erase, 1, [module(user)]).
inline_builtin_forgen(low_is_list, low_is_list, 1, [module(user)]).
inline_builtin_forgen(functor,functor, 3, [module(user),unsafeheap]).
inline_builtin_forgen(mgu_variables,mgu_variables, 3, [module(sysh)]).
inline_builtin_forgen(mgu,mgu, 3, [module(sysh)]).
inline_builtin_forgen(nocheck_arg,nocheck_arg, 3, [module(user)]).
inline_builtin_forgen(arg,arg, 3, [module(user)]).
inline_builtin_forgen(setarg,setarg,3, [module(user)]).
inline_builtin_forgen(nb_setarg,nb_setarg,3, [module(user)]).
inline_builtin_forgen(=..,univ, 2, [module(user),unsafeheap]).



inline_builtin_forgen(classify_goal, classify_goal, 2, [module(sysh)]).

inline_builtin_forgen(norepshar,norepshar, 1, [module(user)]).

inline_builtin_forgen(exclude_from_domain,exclude_from_domain, 2, [module(user)]).


inline_builtin_forgen(add_module, add_module, 3, [module(user)]).

inline_builtin_forgen(low_put, low_put, 2, [module(sysh)]).
inline_builtin_forgen(put, put, 1, [module(user)]).
inline_builtin_forgen(cputime, cputime,1, [module(user)]).
inline_builtin_forgen(unixtime, unixtime,1, [module(user)]).
inline_builtin_forgen(random, random,1, [module(user)]).
inline_builtin_forgen(module_call,module_call,2, [module(user)]).

inline_builtin_forgen(delete_file, delete_file, 1, [module(user)]).

inline_builtin_forgen(atomic2list, atomic2list, 3, [module(user),unsafeheap]).

inline_builtin_forgen(stream_property, stream_property, 4, [module(sysh)]).
inline_builtin_forgen(open_stream, open_stream, 3, [module(sysh)]).
inline_builtin_forgen(low_get_open_stream, low_get_open_stream, 3, [module(sysh)]).
inline_builtin_forgen(low_close_stream, low_close_stream, 1, [module(sysh)]).
inline_builtin_forgen(get0,get0,1, [module(user)]).
inline_builtin_forgen(current_eof, current_eof, 0, [module(user)]).
inline_builtin_forgen(get0_stream,get0_stream,2, [module(user)]).
inline_builtin_forgen(see,see, 1, [module(user)]).
inline_builtin_forgen(seen,seen, 0, [module(user)]).
inline_builtin_forgen(seeing,seeing, 2, [module(user)]).
inline_builtin_forgen(tell,tell, 1, [module(user)]).
inline_builtin_forgen(append,append, 1, [module(user)]).
inline_builtin_forgen(telling,telling, 2, [module(user)]).
inline_builtin_forgen(told,told, 0, [module(user)]).
inline_builtin_forgen(close,close,1, [module(user)]).
inline_builtin_forgen(htoggles, htoggles, 2, [module(user)]).
inline_builtin_forgen(print_code,print_code,1, [module(sysh)]).
inline_builtin_forgen(count_call,count_call,2, [module(user)]).
inline_builtin_forgen(b_setval, b_setval, 2, [module(user)]).
inline_builtin_forgen(b_getval, b_getval, 2, [module(user)]).
inline_builtin_forgen(nb_setval, nb_setval, 2, [module(user)]).
inline_builtin_forgen(nb_getval, nb_getval, 2, [module(user)]).
inline_builtin_forgen(attvar, attvar, 1, [module(user)]).
inline_builtin_forgen(make_new_attvar, make_new_attvar, 1, [module(user)]).
inline_builtin_forgen(bind_attvar, bind_attvar, 2, [module(user)]).
inline_builtin_forgen(cyclic_list, cyclic_list, 1, [module(user)]).
inline_builtin_forgen(timeout, timeout, 1, [module(user)]).
inline_builtin_forgen(cinter, cinter, 4, [module(user)]).
inline_builtin_forgen(argv, argv, 2, [module(user)]).
inline_builtin_forgen(codegc, codegc, 0, [module(user)]).
inline_builtin_forgen(string, string, 1, [module(user)]).
inline_builtin_forgen(string_length, string_length, 2, [module(user)]).
inline_builtin_forgen(char_to_int, char_to_int, 2, [module(user)]).
inline_builtin_forgen(int_to_char, int_to_char, 2, [module(user)]).
inline_builtin_forgen(char, char, 1, [module(user)]).
inline_builtin_forgen(atom_index, atom_index, 2, [module(user)]).

inline_builtin_forgen(info_cp, info_cp, 1, [module(user)]).

inline_builtin_forgen(notbadmetagoal, notbadmetagoal, 2, [module(user)]).

inline_builtin_forgen(term_type, term_type, 2, [module(user)]).



inline_builtin_forgen(shell, shell, 2, [module(user)]).
inline_builtin_forgen(halt, halt, 1, [module(user)]).
inline_builtin_forgen(waar, waar, 0, [module(user)]).

inline_builtin_forgen(pickup, pickup, 1, [module(user)]).
inline_builtin_forgen(drop, drop, 1, [module(user)]).

inline_builtin_forgen(assign_areg, assign_areg, 2, [module(sysh)]).
inline_builtin_forgen(drop_areg, drop_areg, 2, [module(sysh)]).


% Logic engines: misc
inline_builtin_forgen(sleep, sleep, 1, [module(sysh)]).
inline_builtin_forgen(flush_output, flush_output, 1, [module(sysh)]).

% Logic engines: creation & termination
inline_builtin_forgen(pid, pid, 1, [module(user)]).
inline_builtin_forgen(hub, hub, 1, [module(user)]).
inline_builtin_forgen(spawn, spawn, 3, [module(sysh)]).
inline_builtin_forgen(engine_get_start_goal_low, engine_get_start_goal_low, 1,
		      [module(sysh),unsafeheap]).
inline_builtin_forgen(stop, stop, 1, [module(user)]).

% Logic engines: messaging
inline_builtin_forgen(low_send, low_send, 2, [module(sysh)]).
inline_builtin_forgen(low_receive, low_receive, 3, [module(sysh), unsafeheap]).

% Logic engines: nr_of_threads
inline_builtin_forgen(running_threads, running_threads, 1, [module(user)]).
inline_builtin_forgen(lock, lock, 1, [module(user)]).
inline_builtin_forgen(unlock, unlock, 1, [module(user)]).




inline_builtin_forgen(create_sat_instance,create_sat_instance,0,[module(user),unsafeheap]).
inline_builtin_forgen(delete_sat_instance,delete_sat_instance,0,[module(user),unsafeheap]).
inline_builtin_forgen(solve_sat,solve_sat,0,[module(user),unsafeheap]).
inline_builtin_forgen(add_sat_clause,add_sat_clause,1,[module(user),unsafeheap]).
inline_builtin_forgen(add_xor_clause,add_xor_clause,1,[module(user),unsafeheap]).
inline_builtin_forgen(sat_get_var_assignment,sat_get_var_assignment,2,[module(user),unsafeheap]).
inline_builtin_forgen(sat_get_model,sat_get_model,1,[module(user),unsafeheap]).
inline_builtin_forgen(sat_assign_model,sat_assign_model,1,[module(user),unsafeheap]).
inline_builtin_forgen(sat_nvars,sat_nvars,1,[module(user),unsafeheap]).

fake_builtin(assign_areg,2).
fake_builtin(drop_areg,2).
fake_builtin(traverselist_rev,0).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% inline_builtin_forgen(floop, floop, 2, [module(user)]).
%% 
%% special_builtin_args(floop,[dlong(Symb), dlong(I)],
%% 		     [builtin_floop_2, % as usual
%% 		      symbol(Symb), dlong(I)]). % bedoeling: length(Symb) == I

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/* The following declarations will results in specialized calls
   to builtins, where the specified arguments are called with
      uninitialized registers.
   The arguments should be given in order!
      The third argument is c-name for the builtin.
*/
% specialized_inline_builtin_forgen(_,_,_,_) :- fail.

% specialized_inline_builtin_forgen(PName, Arity, CName, OrigCName, SpecArgs)
	
specialized_inline_builtin_forgen(pickup,1, pickup_spec, pickup, [new]).

specialized_inline_builtin_forgen(arg,3, arg_int_spec, arg, [smallposint,?,new]).
specialized_inline_builtin_forgen(arg,3, arg_int, arg, [smallposint,?,?]).
specialized_inline_builtin_forgen(arg,3, arg_spec, arg, [?,?,new]).

specialized_inline_builtin_forgen(nocheck_arg,3, nocheck_arg_int_spec, nocheck_arg, [smallposint,?,new]).
specialized_inline_builtin_forgen(nocheck_arg,3, nocheck_arg_int, nocheck_arg, [smallposint,?,?]).
specialized_inline_builtin_forgen(nocheck_arg,3, nocheck_arg_spec, nocheck_arg, [?,?,new]).

specialized_inline_builtin_forgen(functor, 3, functor_spec, functor, [?,new,new]).
specialized_inline_builtin_forgen(nb_getval, 2, nb_getval_spec, nb_getval, [?,new]).
specialized_inline_builtin_forgen(b_getval, 2, b_getval_spec, b_getval, [?,new]).
specialized_inline_builtin_forgen(compare, 3, compare_spec, compare, [new,?,?]).
specialized_inline_builtin_forgen(PName, Arity, CName, SpecArgs) :-
	specialized_inline_builtin_forgen(PName, Arity, CName, _, SpecArgs).


/* inlined builtins that possibly could call the heap garbage
   collector during their call
   currently none
*/
unlimited_heap_inline_builtin_forgen(user, functor, 3) :- fail.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_empty_mach :-
	tell('new_machine.h'),
	gem,
	told.

gem :-
	all_instr(I,Args,LayOut),
	writel(['hcase(', I, ')', nl]),
	writel([I, '_label:', nl]),
	writel(['profile(', I, ');', nl]),
	writel(['{', nl,nl]),
	invent_var_names(LayOut,0),
        declare_varnames(Args),
	nl,
	LayOut = [opcode|RL],
	write_code(RL,I,1),
        writel([nl, 'fprintf(stderr,"',I,' not yet implemented\\n"); return;', nl]),
	(
	  write_additional_string_len(I,locpc), fail
	;
	  true
	),
	writel(['  locpc += ', I, '_len;', nl]),
	writel(['  goto_more_exec;', nl]),
	writel(['}', nl, nl]),
	fail.
gem.

write_code([],_,_).
write_code([Arg|R],I,N) :-
	arg(1,Arg,Name),
	basic_type(Arg,Type),
	writel(['         ',Name,' = *(',Type,'*)(arg_place(locpc,',N,',',I,'));',nl]),
	M is N + 1,
	write_code(R,I,M).

declare_varnames([]).
declare_varnames([What|R]) :-
	What =.. [_,Name],
	(What = float(_) ->
	    writel(['             float ',Name,';',nl])
	;
	    writel(['             dlong ',Name,';',nl])
	),
	declare_varnames(R).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_with_module(Name,Mod) :-
	local_write('\''),
	local_write(Name),
	(Mod == user ->
	    true
	;
	    local_write('__'), local_write('M_'), local_write(Mod)
	),
	local_write('\'').
	

gen_opcodes :-
        (
	  findall(I,all_instr(I),L),
	  mylen(L,1,LenL),
	  (
 	    tell('dp_numbering.pl'),
	    write_header,
	    write_opcode_pl(L,0),
	    findall(PName-Arity-Info,
		    (inline_builtin_forgen(PName,_,Arity,Info) ;
			specialized_inline_builtin_forgen(_,Arity,PName,_,_),
			Info=[module(user)]),LB),
	    write_builtin_pl(LB,0),
	    (
	      inline_builtin_forgen(N1,N2,A,Info),
	      ((local_member(module(sysh),Info) ; local_member(unsafeheap,Info)) ->
		  Mod = sysh
	      ;
		  Mod = user
	      ),
	      local_write('inline_builtin_naming('),
	      write_with_module(N1,Mod), local_write(','),
	      local_write(N2), local_write(','), local_write(A),
	      local_write(').'), nl, fail
	    ;
	      true
	    ),
	    findall(N1, (specialized_inline_builtin_forgen(N1,A,N2,_),
			    local_write(inline_builtin_naming(N2,N2,A)),
			    local_write('.'), nl), _),
	    findall(Arity,
		    (specialized_inline_builtin_forgen(Name,Arity,SName, Args ),
			local_write('specialized_inline_builtin("'),
			local_write(Name), local_write('",'), local_write(Arity),
			local_write(',"'), local_write(SName), local_write('",'),
			local_write(Args), local_write(').\n')),
		    Bos),
	    (Bos == [] ->
		local_write('specialized_inline_builtin(_,_,_,_) :- fail.'),nl
	    ;
		true
	    ),
	    findall(Arity,
		    (unlimited_heap_inline_builtin_forgen(M,Name,Arity),
			write_unlimited_heap_inline_builtin(M,Name,Arity)),
		    Bos2),
	    (Bos2 == [] ->
		local_write('unlimited_heap_inline_builtin(_,_) :- fail.'),nl
	    ;
		true
	    ),
	    told,
	    fail
	  ;
	    (BITS = 32, tell('opcodes_32.h') ; BITS = 64, tell('opcodes_64.h')),
	    (
	      write_header,
	      write_types, nl,
	      local_write('#define NR_OF_OPCODES '), local_write(LenL), nl,
	      local_write('#define type_opcode short '), nl,
	      nl,
	      write_opcode_h(L,0),
	      fail
	    ;
	      write_instr_len_defines(BITS),
	      fail
	    ;
	      write_additional_strlen_defines,
	      fail
	    ;
	      write_additional_bigintlen_defines,
	      fail
	    ;
	      told,
	      fail
	    )
	  )
	;
	  tell('instr_names.c'),
	  write_header,
	  writel([nl,'char *instr_name[] = {',nl]),
	  all_instr(I),
	  writel(['           "',I,'",',nl]),
	  fail
	;
	  writel(['           0};',nl]),
	  told,
	  fail
	;
%% 	  tell('instr_labels.c'),
%% 	  write_header,
%% 	  writel(['static void *local_instr_labels[] = {',nl]),
%% 	  instr(Instr,_,_),
%% 	  imem(I,Instr),
%% 	  writel(['           &&',I,'_label,',nl]),
%% 	  fail
%% 	;
%% 	  writel(['           0};',nl]),
%% 	  told,
%% 	  fail
%% 	;
	  tell('instr_labels.c'),
	  write_header,
	  writel(['#ifndef PLAIN_SWITCH',nl]),
	  instr(Instr,_,_),
	  imem(I,Instr),
	  writel(['instr_labels[',I,'] = &&',I,'_label;',nl]),
	  fail
	;
	  writel(['#endif',nl]),
	  told
	).

write_unlimited_heap_inline_builtin(M,N,A):-
	local_write('unlimited_heap_inline_builtin('),
	local_write(N),
	(M == user ->
	    true
	;
	    local_write('__M'), local_write('_'), local_write(M)
	),
	local_write(','),
	local_write(A), local_write(').\n').

mylen([],I,I).
mylen([_|R],I,O) :- II is I + 1, mylen(R,II,O).

write_types :-
	which_type(X,Y),
	(X == Y ->
	    true
	;
	    writel(['#define ',X,' ',Y,nl])
	),
	fail.
write_types.


all_instr(I) :-
        all_instr(I,_,_).

all_instr(I,A,B) :- 
        instr(Is,A,B),
        imem(I,Is).
all_instr(I,[],[opcode]) :-
	pseudo_ops(Is),
	imem(I,Is).



write_opcode_h([],_).
write_opcode_h([I|R],N) :-
	M is N + 1,
	local_write('#define '), local_write(I), local_write('   '), local_write(N), nl,
	write_opcode_h(R,M).

write_opcode_pl([],_).
write_opcode_pl([I|R],N) :-
        M is N + 1,
        local_write('opcode('), local_write(I), local_write(','), local_write(N), local_write(').'), nl,
        write_opcode_pl(R,M).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_print_instrs :-
	tell('print_instrs.h'),
	write_header,
	instr(Il,_Args,LayOut),
	imem(I,Il),
	writel(['    case ', I, ':', nl]),
	writel(['    ', I, '_label:', nl]),
	writel(['      {', nl,nl]),
	LayOut = [opcode|RL],
% 	writel(['        printf("',I,'  ");',nl]),
	
	write_print_code(RL,I,1),
	( I = switchonbound ->
	    writel(['          locpc += sizeof(dlong) * (dlong)*(short*)(arg_place(locpc,2,switchonbound)); ',nl])
	;   
	    true
	),
	(
	  write_additional_string_len(I,locpc), fail
	;
	  true
	),
	writel(['        locpc += ', I, '_len;', nl]),
	(is_followed_by_active_yvars(I) ->
	    writel(['        locpc += active_yvar_len;', nl])
	;
	    true
	),
	(I == call_unkn ->
	    writel(['        return;',nl])
	;
	    true
	),
	writel(['        break;', nl]),
	writel(['      }', nl, nl]),
	fail.
gen_print_instrs :-
	close('print_instrs.h').

write_print_code([],_,_) :-
	writel(['        printf("\\n");',nl]).
write_print_code([Arg|R],I,N) :-
	basic_type(Arg,Type),
	Arg = symbol(_), !,
	writel(['  {     dlong temp, temp2;',nl]),
	writel(['        temp = (dlong)*(',Type,'*)(arg_place(locpc,',N,',',I,'));',nl]),
	writel(['        temp2 = get_atom_index(module_from_index(temp));',nl]),
	writel(['        printf("%s/%ld (%s) ",symtab[temp].name,symtab[temp].arity,', nl]),
	writel(['                        symtab[temp2].name); }',nl]),
	M is N + 1,
	write_print_code(R,I,M).
write_print_code([dstring(_)|R],I,N) :-
	!,
	MM is N - 1,
	writel(['        print_string(',nl]),
  	writel(['                     (dlong)*(str_len*)(arg_place(locpc,' , MM , ' , ', I, ')),',nl]),
 	writel(['                     (char *)(arg_place(locpc,',N,' , ',I, ')));',nl]),
	M is N + 1,
	write_print_code(R,I,M).
write_print_code([dbigint(_)|R],I,N) :-
	!,
        MM is N - 1,
	writel(['        print_bigint(',nl]),
	writel(['                     (dlong)*(bigint_len*)(arg_place(locpc,' , MM , ' , ', I, ')),',nl]),
	writel(['                     (dlong *)(arg_place(locpc,',N,' , ',I, ')));',nl]),
	M is N + 1,
	write_print_code(R,I,M).
write_print_code([Arg|R],I,N) :-
	basic_type(Arg,Type),
	writel(['        printf("']),
	(Arg = areg(_) ->
	    ToPrint = ['A%ld ",(dlong)*(',Type,'*)(arg_place(locpc,',N,',',I,')));']
	;
	 Arg = yvar(_) ->
	    ToPrint = ['Y%ld ",(dlong)*(',Type,'*)(arg_place(locpc,',N,',',I,')));']
	;
	 Arg = arity(_) ->
	    ToPrint = ['arity = %ld ",(dlong)*(',Type,'*)(arg_place(locpc,',N,',',I,')));']
	;
	 Arg = atom(_) ->
	    ToPrint = ['%s ",get_atom_name_carefull((dlong)*(',Type,'*)(arg_place(locpc,',N,',',I,'))));']
	;
	 Arg = struct(_) ->
	    ToPrint = ['%s/%ld ",get_funct_name((dlong)*(',Type,'*)(arg_place(locpc,',N,',',I,'))),get_funct_arity((dlong)*(',Type,'*)(arg_place(locpc,',N,',',I,'))));']
	;
	 Arg = label(_) ->
	    ToPrint = ['label: %ld ",(dlong)(((codep)*(',Type,'*)(arg_place(locpc,',N,',',I,'))) - codezone));']
	;
	 Arg = dfloat(_) ->
	    ToPrint = ['float(not yet) ");']
	;
	 Arg = dchar(_) ->
	    ToPrint = ['%c",get_character(*(dchar *)(arg_place(locpc,',N,',',I,'))));']
	;
	    ToPrint = ['%ld ",(dlong)*(',Type,'*)(arg_place(locpc,',N,',',I,')));']
	),
	writel(ToPrint), writel([nl]),
	M is N + 1,
	write_print_code(R,I,M).

is_followed_by_active_yvars(putpvarcall).
is_followed_by_active_yvars(getpvarcall).
is_followed_by_active_yvars(putpvarvalvalcall).
is_followed_by_active_yvars(call).
is_followed_by_active_yvars(fast_call).
is_followed_by_active_yvars(dummy_call).
is_followed_by_active_yvars(trymeorelse).
is_followed_by_active_yvars(getpbreg_trymeorelse).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_loading :-
	tell('loading.h'),
	write_header,
	instr(Instr,Args,Bytes),
	imem(I,Instr),
	write_case(I),
	invent_var_names(Args,0),
	writel(['           {',nl]),
	declare_varnames_load(Args),
	write_scan(Args,1),
	write_bytes(Bytes,1,I),
	(I == switchonbound ->
	    local_write('             if (!deal_with_loading_switchonbound(&topofcode,hashtable,size,hashsize,'),
	    nl,
	    local_write('                           pred_arity,index_fail_label,tempindextable,&nrlabs,dlong0)) '),nl,
	    local_write('                 return(0);'), nl
	;
	    I = active_yvar ->
	    local_write('             *(dlong *)topofcode = deal_with_loading_active_yvar(f,topofcode);'),
	    nl
	;
	    I = select_functor2 ->
	    local_write('             deal_with_loading_select_struct2(dlong0, dlong1, dlong3, topofcode);'), nl
	;
	    I = select_functor3 ->
	    local_write('             deal_with_loading_select_struct3(dlong0, dlong1, dlong3, dlong5, topofcode);'), nl
	;
	    I = select_functor4 ->
	    local_write('             deal_with_loading_select_struct4(dlong0, dlong1, dlong3, dlong5, dlong7, topofcode);'), nl
	;
	    I = select_functor5 ->
	    local_write('             deal_with_loading_select_struct5(dlong0, dlong1, dlong3, dlong5, dlong7, dlong9, topofcode);'), nl
	;
	    true
	),
	(
	  write_additional_string_len(I,topofcode), fail
	;
	  true
	),
	(I == switchonbound ->
	    true
	;
	    writel(['             topofcode += ',I,'_len;',nl])
	),
	writel(['           }',nl]),
	local_write('           break;'),nl,nl,
	fail.
gen_loading :-
	told.


declare_varnames_load([]).
declare_varnames_load([What|R]) :-
	What =.. [_,Name],
	(What = dfloat(_) ->
	    writel(['             dfloat ',Name,';',nl])
	;
	    writel(['             dlong ',Name,';',nl])
	),
	declare_varnames_load(R).

invent_var_names([],_).
invent_var_names([What|R],N) :-
	(What = opcode ->
	    true
	;
	    What =.. [ItsName,Name],
	    (ItsName == strlength ->
		Name = len
	    ;
		atomconcat([ItsName,N],Name)
	    )
	),
	M is N + 1,
	invent_var_names(R,M).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

gen_fill_labels :-
	tell('fill_labels.h'),
	write_header,
	instr(Instr,_,Bytes),
	imem(I,Instr),
	write_case(I),
	write_treat_labels(Bytes,I),
	local_write('           break;'),nl,nl,
	fail.
gen_fill_labels :-
        told.


write_header :-
	local_write('/* Generated by instructions.pl */'), nl,
	local_write('/* Change at own risk           */'), nl,
	local_write('/* Bart Demoen                  */'), nl, nl.


write_treat_labels(Bytes,I) :-
	contains_labels(Bytes),
	write_treat_labels1(Bytes,I,0),
	fail.
write_treat_labels(_,I) :-
	(
	  I = switchonbound ->
	  writel(['           deal_with_labels_switchonbound(pc);',nl]),
	  writel(['           ','{ dlong size;',nl]),
	  writel(['           ','size =  (dlong)*(short *)(arg_place(pc,2,switchonbound));',nl]),
	  writel(['           ','pc +=  size   * sizeof(dlong);',nl]),
	  writel(['           ','}',nl])
	;
	    true
	),
	(
	  write_additional_string_len(I,pc), fail
	;
	  true
	),
	(is_followed_by_active_yvars(I) ->
	    writel(['           pc += active_yvar_len;', nl])
	;
	    true
	),
	writel(['           pc += ', I, '_len;', nl]).

needs_additional_string_len(put_string).
needs_additional_string_len(get_string).
needs_additional_string_len(put_stringv).
needs_additional_string_len(get_stringv).
needs_additional_string_len(unify_string_untagged).
needs_additional_string_len(bld_string).
needs_additional_string_len(ifequal_string_goto).

needs_additional_bigint_len(put_bigint).
needs_additional_bigint_len(put_bigintv).

contains_labels(L) :-
	local_member(label(_),L), ! .

write_treat_labels1([],_,_).
write_treat_labels1([label(_)|_],I,N) :-
	writel(['           lab = *(dlong *)(arg_place(locpc,',N,',',I,'));', nl]),
	(I == jump ->
	    writel(['           lab = end_of_chain(*(dlong *)(arg_place(locpc,1,jump)),profile_extra);',nl])
	;
	    true
	),
	writel(['           if (lab < 0) *(dlong *)(arg_place(locpc,',N,',',I,')) = (dlong)labeltabel[0];',nl]),
	writel(['           else *(dlong *)(arg_place(locpc,',N,',',I,')) = (dlong)labeltabel[lab] + profile_extra;',nl]),
	fail.
write_treat_labels1([_|R],I,N) :-
	M is N + 1,
	write_treat_labels1(R,I,M).

write_bytes([],_,_).
write_bytes([B|RB],N,I) :-
	(B = opcode ->
	    M = N
	;
	    treat_b(B,N,I),
	    M is N + 1
	),
	write_bytes(RB,M,I).



% The following is machine dependent

size(32,X,Y) :- size_32(X,Y).
alignment(32,X,Y) :- alignment_32(X,Y).
opcodelen(32,X,Y) :- opcodelen_32(X,Y).
total_alignment(32,X,Y) :- total_alignment_32(X,Y).

size(64,X,Y) :- size_64(X,Y).
alignment(64,X,Y) :- alignment_64(X,Y).
opcodelen(64,X,Y) :- opcodelen_64(X,Y).
total_alignment(64,X,Y) :- total_alignment_64(X,Y).


% for Intel 32 bit

size_32(byte,1).
size_32(dlong,4).
size_32(short,2).
size_32(dfloat,8).
size_32(dstring,0).  % a lie - it is derived from the str_len
size_32(dbigint,0).   % a lie - it is derived from the bigint length

alignment_32(byte,1).
alignment_32(dlong,4).
alignment_32(short,2).
alignment_32(dfloat,4).
alignment_32(dstring,4). % 1 instead of 4 would be ok, but it makes _len
				% computations more difficult
alignment_32(dbigint,4).

opcodelen_32('THREADED',4).
opcodelen_32('SWITCH',2).

total_alignment_32('THREADED',4).
total_alignment_32('SWITCH',4).



% for Intel 64 bit

size_64(byte,1).
size_64(dlong,8).
size_64(short,2).
size_64(dfloat,16).
size_64(dstring,0).  % a lie - it is derived from the str_len
size_64(dbigint,0).   % a lie - it is derived from the bigint length

alignment_64(byte,1).
alignment_64(dlong,8).
alignment_64(short,2).
alignment_64(dfloat,16).
alignment_64(dstring,8). % 1 instead of 4 would be ok, but it makes _len
				% computations more difficult
alignment_64(dbigint,8).

opcodelen_64('THREADED',8).
opcodelen_64('SWITCH',2).

total_alignment_64('THREADED',8).
total_alignment_64('SWITCH',8).



% end  machine dependent

which_type(count_type,dlong).
which_type(areg_type,byte).
which_type(yvar_type,short).
which_type(arity_type,byte).
which_type(size_type,short).
which_type(byte,byte).
which_type(short,short).
which_type(dlong,dlong).
which_type(label_type,dlong).
which_type(dfloat,dfloat).
which_type(str_len,short).
which_type(bigint_len,dlong).
which_type(dstring, dstring).  % will need to be caught specially
which_type(dbigint,dbigint).   % will need to be caught specially
which_type(tagged_dlong_type,dlong).
which_type(symbol_type,dlong).
which_type(dlong_type,dlong).
which_type(atom_type,dlong).
which_type(dchar, dlong).
which_type(struct_type,dlong).

basic_type(dchar(_), dchar) :- ! .
basic_type(dstring(_), dstring) :- ! .
basic_type(dbigint(_), dbigint) :- ! .
basic_type(str_len(_),str_len) :- ! .
basic_type(bigint_len(_),bigint_len) :- ! .
basic_type(count(_),count_type) :- ! .
basic_type(areg(_),areg_type) :- ! .
basic_type(yvar(_),yvar_type) :- ! .
basic_type(arity(_),arity_type) :- ! .
basic_type(atom(_),dlong) :- ! .
basic_type(struct(_),dlong) :- ! .
basic_type(label(_),label_type) :- ! .
basic_type(dfloat(_),dfloat) :- ! .
basic_type(dlong(_),dlong) :- ! .
basic_type(tagged_dlong(_),tagged_dlong_type) :- ! .
basic_type(strlength(_),dlong) :- ! .
basic_type(size(_),size_type) :- ! .
basic_type(symbol(_),symbol_type) :- ! .
basic_type(X,_) :- local_write(no_basic_type(X)),nl.


treat_b(What,N,I) :-
        arg(1,What,A),
        basic_type(What,Type),
        ( Type = dfloat ->
            writel(['             store_double_arg(arg_place(locpc,', N, ',', I, '), ', A, ');', nl])
	;
	    Type = dstring ->
	    writel(['             store_string((char *)arg_place(locpc,', N, ',', I, '), len, f);', nl])
	;
	    Type = dbigint ->
	    writel(['             store_bigint((char *)arg_place(locpc,', N, ',', I, '), dlong1, f);', nl])
        ;
            writel(['             *(', Type, ' *)(arg_place(locpc,', N, ',', I,  ')) = ']),
            ( What = atom(_) ->
                writel(['make_atom(tempsymbtab[', A, ']);', nl])
            ;
                What = struct(_) ->
                writel(['make_funct(tempsymbtab[', A, ']);', nl])
            ;
                What = symbol(_) ->
                writel(['tempsymbtab[', A, '];', nl])
	    ;
	        What = tagged_dlong(_) ->
		writel(['make_smallint(', A, ');', nl])
	    ;
		What = count(_) ->
		writel(['0;',nl])
	    ;
		What = dchar(_) ->
		writel(['make_character(',A,');',nl])
	    ;
                writel(['(', Type,')', A, ';', nl])
            )
        ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

write_scan([],_).
write_scan([Arg|Args], Nr):-
	write_scan_arg(Arg, Nr), !,
	Nr1 is Nr +1,
	write_scan(Args, Nr1).

write_scan_arg(strlength(I), _) :-
	writel(['             if (fscanf(f," %ld",&',I,') != 1) goto invalid_input;',nl]).
write_scan_arg(dlong(I), _) :-
	writel(['             if (fscanf(f," %ld",&',I,') != 1) goto invalid_input;',nl]).
write_scan_arg(dchar(C), _) :-
	writel(['             if (fscanf(f," %ld",&',C,') != 1) goto invalid_input;',nl]).
write_scan_arg(dstring(_), _) :-
	writel(['             /* postpone scanning the string until filling it out */',nl]).
write_scan_arg(bigint(_), _) :-
	writel(['             /* postpone scanning the bigint until filling it out */',nl]).
write_scan_arg(dfloat(I),_) :-
	writel(['             {',nl]),
        writel(['               char s[100];',nl]),
        writel(['               if (fscanf(f," %s ",s)!= 1) goto invalid_input;',nl]),
        writel(['               ',I,' = atof(s);',nl]),
        writel(['             }',nl]).

write_scan_arg(count(_), _).  % indeed - do not scan, because not generated, but transformed from call instruction
write_scan_arg(atom(I), _) :-
	writel(['             if (fscanf(f," %ld",&',I,') != 1) goto invalid_input;',nl]).
write_scan_arg(struct(I), _) :-
	writel(['             if (fscanf(f," %ld",&',I,') != 1) goto invalid_input;',nl]).
write_scan_arg(a_s_i_val(_I), _Nr) :-
	writel(['             goto invalid_input;',nl]).


write_case([]) :-
	! .
write_case([A|L]) :-
	!,
	write_case(A),
	write_case(L).
write_case(N) :-
	local_write('        case '),
	local_write(N),
	local_write(':'),
	nl.

local_member(I,[J|R]) :-
	(
	    I = J
        ;
	    local_member(I,R)
	).

imem(I,[I|_]).
imem(I,[_|R]) :- !, imem(I,R).
imem(_,[]) :- !, fail.
imem(I,I).

same(A,B) :-
	instr(A,A1,A2),
	instr(B,B1,B2),
	\+(A = B),
	numbervars(A1-A2,1,N),
	numbervars(B1-B2,1,N),
	A1-A2 = B1-B2.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% defines for instruction lengths: each memory need results in a define


write_additional_strlen_defines :-
	instr(Is,_,Mems),
	local_member(I,Is),
	nth(Mems,str_len(_),0,N),
	local_write('#define extra_strlen_len_'),
	local_write(I),
	local_write('(p) add_str_len(*(str_len *)arg_place(p,'),
	local_write(N),local_write(' , '), local_write(I), local_write('))'),
	nl,
	fail.

write_additional_bigintlen_defines :-
	instr(Is,_,Mems),
	local_member(I,Is),
	nth(Mems,bigint_len(_),0,N),
	local_write('#define extra_bigint_len_'),
	local_write(I),
	local_write('(p) add_bigint_len(*(bigint_len *)arg_place(p,'),
	local_write(N),local_write(' , '), local_write(I), local_write('))'),
	nl,
	fail.

nth([X|_],X,N,N) :- ! .
nth([_|R],X,N,M) :- NN is N + 1, nth(R,X,NN,M).


write_instr_len_defines(BITS) :-
	setof(MemNeed,(I,R)^instr(I,R,MemNeed),MemNeeds),
	write_mem_need_defines(MemNeeds,'THREADED',BITS),
	(write_instruction_args('THREADED',BITS), fail ; true),
	write_mem_need_defines(MemNeeds,'SWITCH',BITS),
	(write_instruction_args('SWITCH',BITS), fail ; true),
	writel(['#define end_clauses_len nop_len',nl]),
	instr(Is,_,MemNeed),
	imem(I,Is),
	make_mem_need_define(MemNeed,[],0,'THREADED',_,Name,BITS),
				% name is independent of How, but THREADED to prevent backtracking
	writel(['#define ',I,'_len',' ',Name,nl]), fail.

write_instruction_args(How,BITS) :-
        writel(['#ifdef ',How,nl]),
	instr(Instrs,_,MemNeed),
	translate_to_offsets(MemNeed,OffSets,0,How,BITS),
	imem(I,Instrs),
	write_arg_offsets(OffSets,I,1),
	fail.
write_instruction_args(_,_) :-
        writel(['#endif',nl,nl]).


translate_to_offsets([],[],_,_,_).
translate_to_offsets([What|RW],Out,N,How,BITS) :-
	(What = opcode ->
	    opcodelen(BITS,How,L),
	    M is N + L,
	    Out = Os
	;
	    Out = [O|Os],
	    basic_type(What,Type1), which_type(Type1,Type),
	    alignment(BITS,Type,Align),
	    padding(N,Align,O),
	    size(BITS,Type,TypeSize),
	    M is O + TypeSize
	),
	translate_to_offsets(RW,Os,M,How,BITS).


write_arg_offsets([],_,_).
write_arg_offsets([O|Os],I,N) :-
	writel(['#define ',I,'_arg_',N,'       ',O,nl]),
	M is N + 1,
	write_arg_offsets(Os,I,M).


write_mem_need_defines(MemNeeds,How,BITS) :-
	writel(['#ifdef ',How,nl]),
	(
	    local_member(M,MemNeeds),
	    make_mem_need_define(M,[],0,How,Len,Name,BITS),
	    writel(['#define ',Name,'      ',Len,nl]),
	    
	    fail
        ;
	    true
        ),
	writel(['#endif',nl,nl]).






make_mem_need_define([],NameL,Len,How,NewLen,Name,BITS) :-
	total_alignment(BITS,How,TotAlign),
	padding(Len,TotAlign,NewLen),
	reverse(NameL,NewNameL),
	atomconcat([len_|NewNameL],Name).
make_mem_need_define([A|R],NameL,LenIn,How,NewLen,Name,BITS) :-
	mem_info(A,Prefix,LenIn,LenOut,How,BITS),
	make_mem_need_define(R,[Prefix|NameL],LenOut,How,NewLen,Name,BITS).

mem_info(What,Prefix,LenIn,LenOut,How,BITS) :-
	(What = opcode ->
	    opcodelen(BITS,How,L),
	    LenOut is LenIn + L
	;
	    basic_type(What,Type1), which_type(Type1,Type),
	    size(BITS,Type,Size),
	    padding(LenIn,Size,LI1),
	    LenOut is LI1 + Size
	),
	mem_info(What,Prefix).


mem_info(count(_), c_) :- ! .
mem_info(areg(_), x_) :- ! .
mem_info(yvar(_), y_) :- ! .
mem_info(arity(_), ari_) :- ! .
mem_info(opcode,  o_) :- ! .
mem_info(atom(_), at_) :- ! .
mem_info(dfloat(_), f_) :- ! .
mem_info(dlong(_), i_) :- ! .
mem_info(tagged_dlong(_), ti_) :- ! .
mem_info(label(_), l_) :- ! .
mem_info(size(_), size_) :- ! .
mem_info(struct(_), str_) :- ! .
mem_info(symbol(_), sy_) :- ! .
mem_info(str_len(_), sl_) :- ! .
mem_info(bigint_len(_), bl_) :- ! .
mem_info(dchar(_), dchar_) :- ! .
mem_info(dstring(_), string_) :- ! .
mem_info(dbigint(_), bigint_) :- ! .
mem_info(A,_) :- local_write(notfound(mem_info(A))),nl,fail.

padding(A,N,B) :-
	(N =:= 0 ->
	    B = A
	;
	    M is A mod N,
	    (M = 0 ->
		B = A
	    ;   B is A + N - M
	    )
	).


%% instr/3: arg1: opcode or list of opcodes
%%          arg2: list of arguments: dlong(_) (could become later other stuff)
%%          arg3: what the instruction in memory needs
%%                [opcode,yvar(X),areg(Y)] as example for getpvar

%% the idea is to have it separately specified how this is layed out in
%% memory



%% pseudo-ops are treated differently

%% instr(end_clauses,[],[]).
%% instr(label,[dlong(Label)],[label(Label)]).
%% instr(cases,[dlong(L),dlong(Arity),dlong(Size)],[label(Label),arity(Arity),dlong(Size)]).
%% instr(arglabel,[

pseudo_ops([end_clauses,label,cases,arglabel]).

instr([getpvarputpvar],
      [dlong(X),dlong(Y),dlong(XX),dlong(YY)],
      [opcode,areg(Y),yvar(X),areg(YY),yvar(XX)]).

instr([getpvar,getpval,putpval,putpvar],
        [dlong(X),dlong(Y)],
        [opcode,yvar(X),areg(Y)]).

instr([getpvalv, move_var],
      [dlong(X), dlong(Y)],
      [opcode, yvar(X), yvar(Y)]).

instr([move_var2],
      [dlong(X1), dlong(Y1), dlong(X2), dlong(Y2)],
      [opcode, yvar(X1), yvar(Y1), yvar(X2), yvar(Y2)]).

instr([move_var3],
      [dlong(X1), dlong(Y1), dlong(X2), dlong(Y2), dlong(X3), dlong(Y3)],
      [opcode, yvar(X1), yvar(Y1), yvar(X2), yvar(Y2), yvar(X3), yvar(Y3)]).

instr([gettval,movreg,gettval_proceed],
	[dlong(X),dlong(Y)],
	[opcode,areg(X),areg(Y)]).

instr([movreg2],
	[dlong(X),dlong(Y),dlong(A),dlong(B)],
	[opcode,areg(X),areg(Y),areg(A),areg(B)]).

instr([add_integer, idiv_integer, modulo_integer],
        [dlong(X), dlong(Y), dlong(I)],
        [opcode, areg(X), areg(Y), dlong(I)]).
instr([integer_sub],
        [dlong(X), dlong(I), dlong(Y)],
        [opcode, areg(X), areg(Y), dlong(I)]).
instr([add1, sub1],
        [dlong(X)],
        [opcode, areg(X)]).


instr([float_sub],
        [dlong(X), dfloat(I), dlong(Y)],
        [opcode, areg(X), areg(Y), dfloat(I)]).

instr([add_float],
        [dlong(X), dlong(Y), dfloat(I)],
        [opcode, areg(X), areg(Y), dfloat(I)]).

instr([add,subtr,bit_and,bit_or,bit_xor,idivision,logshiftl,logshiftr,
       multiply,division,maximum,minimum,c_modulo,power],
        [dlong(X), dlong(Y), dlong(I)],
        [opcode, areg(X), areg(Y), areg(I)]).

instr([float2intfloor, float2intceil, float2inttrunc, mostsigbit, negate, min1, mysqrt,
       myatan, mysin, mycos, getlist_tvar, getlist_tval,
%%        getlist_tvar_tvar_same,
       myabs,
       round2int, mylog, myexp, mysign, listmax1, listmin1, listlen1, listsum1],
      [dlong(X), dlong(Y)],
      [opcode, areg(X), areg(Y)]).

instr([get_atom, put_atom],
	[dlong(A),dlong(Sym)],
	[opcode,areg(A),atom(Sym)]).

instr([get_structure,put_structure],
	[dlong(A),dlong(Sym)],
	[opcode,areg(A),struct(Sym)]).

instr([set_functor, unify_structure],
      [dlong(Sym)],
      [opcode, struct(Sym)]).

instr([set_struct, set_float, set_string],
      [dlong(O)],
      [opcode, dlong(O)]).

instr([getstrv, put_structurev],
      [dlong(A),dlong(Sym)],
      [opcode,yvar(A),struct(Sym)]).

instr([unipvar,unipval,bldpvar,bldpval,getpbreg,putpbreg,allocate,getlistv,
       putlistv],
      [dlong(I)],
      [opcode,yvar(I)]).

instr([allocategetpvar],
      [dlong(N), dlong(Y2), dlong(A2)],
      [opcode,yvar(N), yvar(Y2), areg(A2)]).

instr([bldpval2],
      [dlong(I1), dlong(I2)],
      [opcode,yvar(I1),yvar(I2)]).

instr([unipvarpvar],
      [dlong(I),dlong(J)],
      [opcode,yvar(I),yvar(J)]).

instr([unify_list_tvar,unitvar,unitval,getlist,putlist,bldtvar,
	bldtval,gettbreg,puttbreg],
	[dlong(I)],
	[opcode,areg(I)]).

instr([swap],
      [dlong(I1),dlong(I2)],
      [opcode,areg(I1),areg(I2)]).


instr([bldtvar2],
      [dlong(I1),dlong(I2)],
      [opcode,areg(I1),areg(I2)]).

instr([bldtval2],
      [dlong(I1),dlong(I2)],
      [opcode,areg(I1),areg(I2)]).

instr([bldtval3],
      [dlong(I1),dlong(I2),dlong(I3)],
      [opcode,areg(I1),areg(I2),areg(I3)]).


instr([nop,uni_void, uni_void_2, uni_void_3, set_void, set_void2, set_void3, set_void4, unify_list],[],[opcode]).

instr([unify_list_untagged, uni_void_n], [dlong(I)], [opcode, dlong(I)]).

instr([get_list_untagged], [dlong(A), dlong(I)], [opcode, areg(A), dlong(I)]).

instr([get_structure_untagged], [dlong(A), dlong(Sym), dlong(I)],
                                [opcode, areg(A), struct(Sym), dlong(I)]).

instr([get_listv_untagged], [dlong(A), dlong(I)], [opcode, areg(A), dlong(I)]).

instr([get_structurev_untagged], [dlong(A), dlong(Sym), dlong(I)],
                                [opcode, yvar(A), struct(Sym), dlong(I)]).

instr([unify_string_untagged],
      [dlong(I), strlength(Len), dstring(S)],
      [opcode, dlong(I), str_len(Len), dstring(S)]).
				% other order is better for plain
				% switch code, but for generating
				% things, it is nice to have the
				% length always just before the string

instr([unify_float_untagged],
      [dfloat(F), dlong(I)],
      [opcode, dfloat(F), dlong(I)]).

instr([unify_int],
      [dlong(I)],
      [opcode, tagged_dlong(I)]).


instr([unify_int2],
      [dlong(I), dlong(J)],
      [opcode, tagged_dlong(I), tagged_dlong(J)]).

instr([build_list], [dlong(I)], [opcode,dlong(I)]).

instr([build_list_int],
      [dlong(I), dlong(N)],
      [opcode,dlong(I),tagged_dlong(N)]).

instr([putpvarcall, getpvarcall],
      [dlong(X),dlong(Y),dlong(Symb),dlong(Allocated)],
      [opcode,yvar(X),areg(Y),symbol(Symb),size(Allocated)]).

instr([putpvarvalvalcall],
      [dlong(A1),dlong(A2),dlong(A3),dlong(Y1),dlong(Y2),dlong(Y3),dlong(Symb),dlong(Allocated)],
      [opcode,areg(A1),areg(A2),areg(A3),yvar(Y1),yvar(Y2),yvar(Y3),symbol(Symb),size(Allocated)]).

instr([fast_execute],
      [dlong(Label),dlong(Symb)],
      [opcode,label(Label),symbol(Symb)]).

instr([execute],
      [dlong(Symb1),dlong(Symb2)],
      [opcode,symbol(Symb1),symbol(Symb2)]).

instr([call,call_unkn],
	[dlong(Symb),dlong(Allocated)],
	[opcode,symbol(Symb),size(Allocated)]).

instr([dummy_call,deallex],
	[dlong(Symb)],
	[opcode,symbol(Symb)]).

instr([profile_instr],
      [count(High),count(Low)],
      [opcode,count(High),count(Low)]).

instr([build_atom,unify_atom],
	[dlong(Symb)],
	[opcode,atom(Symb)]).

instr([build_char,unify_char],
	[dchar(Symb)],
	[opcode,dchar(Symb)]).

instr([getlist_tvar_tvar, getlist_tval_tvar
       , getlist_tval_tvar_same],
	[dlong(A),dlong(B),dlong(C)],
	[opcode,areg(A),areg(B),areg(C)]).

instr([get_int,put_int],
	[dlong(R),dlong(I)],
	[opcode,areg(R),tagged_dlong(I)]).

instr([get_int2,put_int2],
	[dlong(R1),dlong(I1),dlong(R2),dlong(I2)],
	[opcode,areg(R1),areg(R2),tagged_dlong(I1),tagged_dlong(I2)]).

instr([get_int3,put_int3],
	[dlong(R1),dlong(I1),dlong(R2),dlong(I2),dlong(R3),dlong(I3)],
	[opcode,areg(R1),areg(R2),areg(R3),
	 tagged_dlong(I1),tagged_dlong(I2),tagged_dlong(I3)]).

instr([put_int4],
	[dlong(R1),dlong(I1),dlong(R2),dlong(I2),dlong(R3),dlong(I3),dlong(R4),dlong(I4)],
	[opcode,areg(R1),areg(R2),areg(R3),areg(R4),
	 tagged_dlong(I1),tagged_dlong(I2),tagged_dlong(I3),tagged_dlong(I4)]).

instr([get_int_proceed],
	[dlong(R),dlong(I)],
	[opcode,areg(R),tagged_dlong(I)]).

instr([get_intv, put_intv],
	[dlong(R),dlong(I)],
	[opcode,yvar(R),tagged_dlong(I)]).

instr([get_float,put_float],
	[dlong(R),dfloat(I)],
	[opcode,areg(R),dfloat(I)]).

instr([get_string,put_string],
	[dlong(R), strlength(Len), dstring(S)],
	[opcode, areg(R), str_len(Len), dstring(S)]).

instr([put_bigint],
	[dlong(R), dlong(S), bigint(I)],
	[opcode, areg(R), bigint_len(S), dbigint(I)]).

instr([put_bigintv],
	[dlong(R), dlong(S), bigint(I)],
	[opcode, yvar(R), bigint_len(S), dbigint(I)]).

instr([get_char,put_char],
	[dlong(R), dchar(C)],
	[opcode, areg(R), dchar(C)]).

instr([get_stringv,put_stringv],
	[dlong(R), strlength(Len), dstring(S)],
	[opcode,yvar(R), str_len(Len),dstring(S)]).

instr([get_floatv,put_floatv],
	[dlong(R),dfloat(I)],
	[opcode,yvar(R),dfloat(I)]).

instr([get_atomv, put_atomv],
	[dlong(R),dlong(I)],
	[opcode,yvar(R),atom(I)]).

instr([get_charv, put_charv],
	[dlong(R),dchar(I)],
	[opcode,yvar(R),dchar(I)]).

instr([try,retry,trust,try_me_else, gettbreg_try_me_else, retry_me_else],
	[dlong(Arity),dlong(Alt)],
	[opcode,arity(Arity),label(Alt)]).

instr([getpbreg_trymeorelse],
      [dlong(A),dlong(Alt)],
      [opcode,yvar(A),label(Alt)]).

instr([trymeelse5, gettbreg_try_me_else5, retrymeelse5,
       trymeelse4, gettbreg_try_me_else4, retrymeelse4,
       trymeelse3, gettbreg_try_me_else3, retrymeelse3,
       trymeelse2, gettbreg_try_me_else2, retrymeelse2,
       trymeelse1, gettbreg_try_me_else1, retrymeelse1,
       try2, trust2,
       retry3],
	[dlong(Alt)],
	[opcode,label(Alt)]).

instr([trust_me_else],
	[dlong(Arity)],
	[opcode,arity(Arity)]).

instr([trustmeelse5,trustmeelse4,trustmeelse3,trustmeelse2,trustmeelse1],
	[],
	[opcode]).

instr([switchonterm,switchonlist, switchonlist_skip],
	[dlong(Reg),dlong(L1),dlong(L2), dlong(L3)],
	[opcode,areg(Reg),label(L1),label(L2), label(L3)]).

instr([switchonlist_append],
	[dlong(Reg),dlong(Areg3), dlong(Areg4), dlong(L1),dlong(L2), dlong(L3)],
	[opcode,areg(Reg),areg(Areg3), areg(Areg4), label(L1),label(L2), label(L3)]).

instr([switchonlist_skip_alloc],
      [dlong(Reg),dlong(A),dlong(L1),dlong(L2), dlong(L3)],
      [opcode,areg(Reg),yvar(A),label(L1),label(L2), label(L3)]).

instr(switchonbound,
	[dlong(R),dlong(Size), dlong(Dummy), dlong(L)],
	[opcode,areg(R),size(Size),dlong(Dummy),label(L)]).

instr([select_functor2],
	[dlong(_What),
	 dlong(F1),dlong(Label1),dlong(F2),dlong(Label2),
	 dlong(Var),dlong(Fail)],
	[opcode,
	 struct(F1),label(Label1),struct(F2),label(Label2),
	 label(Var),label(Fail)]).

instr([select_functor3],
	[dlong(_What),
	 dlong(F1),dlong(Label1),dlong(F2),dlong(Label2),
	 dlong(F3),dlong(Label3),
	 dlong(Var),dlong(Fail)],
	[opcode,
	 struct(F1),label(Label1),struct(F2),label(Label2),
	 struct(F3),label(Label3),
	 label(Var),label(Fail)]).

instr([select_functor4],
	[dlong(_What),
	 dlong(F1),dlong(Label1),dlong(F2),dlong(Label2),
	 dlong(F3),dlong(Label3),dlong(F4),dlong(Label4),
	 dlong(Var),dlong(Fail)],
	[opcode,
	 struct(F1),label(Label1),struct(F2),label(Label2),
	 struct(F3),label(Label3),struct(F4),label(Label4),
	 label(Var),label(Fail)]).

instr([select_functor5],
	[dlong(_What),
	 dlong(F1),dlong(Label1),dlong(F2),dlong(Label2),
	 dlong(F3),dlong(Label3),dlong(F4),dlong(Label4),
	 dlong(F5),dlong(Label5),
	 dlong(Var),dlong(Fail)],
	[opcode,
	 struct(F1),label(Label1),struct(F2),label(Label2),
	 struct(F3),label(Label3),struct(F4),label(Label4),
	 struct(F5),label(Label5),
	 label(Var),label(Fail)]).

instr([trymeorelse,retrymeorelse,jump],
	[dlong(B)],
	[opcode,label(B)]).

instr([choicepoint_reuse],
      [dlong(N), dlong(L1), dlong(L2)],
      [opcode,areg(N),label(L1),label(L2)]).

instr([test_smaller, test_smaller_or_equal, test_smaller_or_equal_spec,
       test_inequal, test_equal,
       test_toplevel_equal, test_toplevel_smaller,
       test_term_smaller, test_term_smaller_or_equal, test_term_equal,
       test_term_not_equal, test_term_not_unify],
      [dlong(R1), dlong(R2), dlong(Lab)],
      [opcode, areg(R1), areg(R2), label(Lab)]).

instr([test_smaller_or_equaltp],
      [dlong(P1),dlong(P2), dlong(Lab)],
      [opcode,areg(P1),yvar(P2), label(Lab)]).

instr([test_inequalpp, test_smaller_or_equalpp],
      [dlong(P1),dlong(P2), dlong(Lab)],
      [opcode,yvar(P1),yvar(P2), label(Lab)]).

instr([test_term_equal_atom],
      [dlong(R1), dlong(Sym), dlong(Lab)],
      [opcode, areg(R1), atom(Sym), label(Lab)]).

instr([test_term_equal_integer],
      [dlong(R1), dlong(I), dlong(Lab)],
      [opcode, areg(R1), tagged_dlong(I), label(Lab)]).

instr([test_var,test_atom,test_atomic,test_number,test_attvar,test_compound,test_nonvar,test_integer, test_callable, test_points_to_callcc],
      [dlong(R1), dlong(Lab)],
      [opcode, areg(R1), label(Lab)]).

instr([test_nonvar_p,test_var_p,test_integer_p],
      [dlong(A), dlong(Y), dlong(Lab)],
      [opcode, areg(A), yvar(Y), label(Lab)]).

instr([add_integer_p],
      [dlong(Dest), dlong(Yvar), dlong(Help), dlong(I)],
      [opcode, areg(Dest), yvar(Yvar), areg(Help), dlong(I)]).

instr([get_int_test],
      [dlong(R1), dlong(R2), dlong(Lab)],
      [opcode, areg(R1), tagged_dlong(R2), label(Lab)]).
instr([get_atom_test],
      [dlong(R1), dlong(R2), dlong(Lab)],
      [opcode, areg(R1), atom(R2), label(Lab)]).
instr([get_intv_test],
      [dlong(R1), dlong(R2), dlong(Lab)],
      [opcode, yvar(R1), tagged_dlong(R2), label(Lab)]).
instr([get_atomv_test],
      [dlong(R1), dlong(R2), dlong(Lab)],
      [opcode, yvar(R1), atom(R2), label(Lab)]).



instr([fail,dealloc_proceed,proceed,trustmeorelsefail],
	[],
	[opcode]).

instr([bld_float],
	[dfloat(N)],
        [opcode,dfloat(N)]).

instr([bld_string],
	[strlength(Len),dstring(S)],
        [opcode,str_len(Len), dstring(S)]).

instr([build_int],
	[dlong(N)],
        [opcode,tagged_dlong(N)]).

% ifequal_*_goto is generated at load-time, not a compile_time

instr([ifequal_int_goto,ifequal_atom_struct_goto],
	[dlong(A),dlong(L),dlong(I)],
	[opcode,areg(A),label(L),dlong(I)]).

instr([ifequal_float_goto],
        [dlong(A),dlong(L),dfloat(F)],
        [opcode,areg(A),label(L),dfloat(F)]).

instr([ifequal_char_goto],
        [dlong(A), dchar(C), dlong(L)],
        [opcode,areg(A), dchar(C), label(L)]).

instr([ifequal_string_goto],
        [dlong(A), dlong(L), strlength(Len), dstring(S)],
        [opcode,areg(A), label(L), str_len(Len), dstring(S)]).

instr([ifequal_list_goto],
        [dlong(A),dlong(L)],
        [opcode,areg(A),label(L)]).

instr([putpvarvar, putpvarval, putpvalval],
	[dlong(A1),dlong(A2),dlong(P1),dlong(P2)],
	[opcode,areg(A1),areg(A2),yvar(P1),yvar(P2)]).

instr([putpvarvarvar, putpvarvarval, putpvarvalval, putpvalvalval],
	[dlong(A1),dlong(A2),dlong(A3),dlong(P1),dlong(P2),dlong(P3)],
	[opcode,areg(A1),areg(A2),areg(A3),yvar(P1),yvar(P2),yvar(P3)]).


instr([putpvalvalvalval],
      [dlong(A1),dlong(A2),dlong(A3),dlong(A4),
       dlong(P1),dlong(P2),dlong(P3),dlong(P4)],
      [opcode,areg(A1),areg(A2),areg(A3),areg(A4),
       yvar(P1),yvar(P2),yvar(P3),yvar(P4)]).

instr([put4pval],
      [dlong(P1),dlong(P2),dlong(P3),dlong(P4)],
      [opcode,yvar(P1),yvar(P2),yvar(P3),yvar(P4)]).

instr([getpvar2],
	[dlong(A1),dlong(A2),dlong(P1),dlong(P2)],
	[opcode,areg(A1),areg(A2),yvar(P1),yvar(P2)]).

instr([getpvar3],
	[dlong(A1),dlong(A2),dlong(A3),dlong(P1),dlong(P2),dlong(P3)],
	[opcode,areg(A1),areg(A2),areg(A3),yvar(P1),yvar(P2),yvar(P3)]).

instr([getpvar4],
      [dlong(A1),dlong(A2),dlong(A3),dlong(A4),
       dlong(P1),dlong(P2),dlong(P3),dlong(P4)],
      [opcode,areg(A1),areg(A2),areg(A3),areg(A4),
       yvar(P1),yvar(P2),yvar(P3),yvar(P4)]).

instr([getstr_tvar],
	[dlong(A1),dlong(A2),dlong(Sym)],
	[opcode,areg(A1),areg(A2),struct(Sym)]).

instr([getstr_tvar_tvar],
	[dlong(A1),dlong(A2),dlong(A3),dlong(Sym)],
	[opcode,areg(A1),areg(A2),areg(A3),struct(Sym)]).


instr([uni_tvar_tvar,uni_tvar_tval,uni_tval_tvar,uni_tval_tval],
        [dlong(A1),dlong(A2)],
        [opcode,areg(A1),areg(A2)]).

instr([check_free_heap_head],
      [dlong(A1), dlong(A2)],
      [opcode, areg(A1), dlong(A2)]).

instr([check_free_heap_body],
      [dlong(A1)],
      [opcode, dlong(A1)]).

instr([initperm],
      [dlong(I)],
      [opcode, dlong(I)]).


instr([test_smaller_float, test_larger_float, test_smaller_or_equal_float,
       test_larger_or_equal_float, test_equal_float, test_inequal_float],
      [dlong(R1), dfloat(F2), dlong(Lab3)],
      [opcode, areg(R1), dfloat(F2), label(Lab3)]).

instr([test_smaller_int , test_larger_int, test_smaller_or_equal_int,
       test_larger_or_equal_int, test_equal_int, test_inequal_int ],
      [dlong(R1), dlong(I2), dlong(Lab3)],
      [opcode, areg(R1), dlong(I2), label(Lab3)]).


instr([unify_structure_untagged], [struct(Sym), dlong(I)],
      [opcode, struct(Sym), dlong(I)]).

instr([member_entry, member_retry, append_entry, last_entry, length_entry, reverse_entry],[], [opcode]).

instr(error_message, [dlong(Nr)], [opcode,dlong(Nr)]).

instr(active_yvar,[],[opcode]). % this is a complete lie - I want 4 bytes that's all

instr(call_c,[dlong(Function),dlong(FunctorIndex)],
      [opcode,dlong(Function),dlong(FunctorIndex)]).

instr(Name, Reading, Memory):-
	inline_builtin_forgen(_, Name_builtin, Arity),
	atom_concat('builtin_', Name_builtin, P1),
	underscore(U),
	atom_concat(P1, U, P2),
	atom_concat(P2, Arity, Name),
	Memory = [opcode|M1],
	gen_args(Arity, Reading, M1).

instr(Name, Reading, Memory):-
	specialized_inline_builtin_forgen(_, Arity, Name_builtin, _),
	atom_concat('builtin_', Name_builtin, P1),
	underscore(U),
	atom_concat(P1, U, P2),
%% 	number_codes(Arity, Codes),
%% 	atom_codes(ArAtom, Codes),
%% 	atom_concat(P2, ArAtom, Name),
	atom_concat(P2, Arity, Name),
	Memory = [opcode|M1],
	gen_args(Arity, Reading, M1).


gen_args(0, [], []):- ! .
gen_args(X, [dlong(V)|R], [areg(V)|M]):-
	X1 is X -1,
	gen_args(X1, R, M).


%%%%%%%%%%%%%%%%%%%%%%%%

underscore(U) :-
	name(U,[0'_]).

writel([]).
writel([X|R]) :-
	(X = nl ->
	    nl
	;   X = tab ->
	      local_write('        ')
	;
	      local_write(X)
        ),
	writel(R).


reverse(A,B) :- rev(A,[],B).
rev([],L,L).
rev([X|R],A,B) :- rev(R,[X|A],B).


atomconcat(L,N) :-
	atomconcat(L,'',N).

atomconcat([],N,N).
atomconcat([A|R],In,Out) :-
	atomconcat(R,In,In1),
	name(A,AL),
	name(In1,In1L),
	app(AL,In1L,OutL),
	name(Out,OutL).

app([],L,L).
app([X|R],S,[X|T]) :- app(R,S,T).



%%%%%%%%%%%% things for moving code while adding profiling instructions

gen_move_instructions :-
	tell('move_instructions_profiling.h'),
	write_header,
	instr(Instr,_Args,_Bytes),
	imem(I,Instr),
	write_case(I),
	writel(['           ','{',nl]),
	writel(['           ','  dlong size = 0;',nl]),
	(I == switchonbound ->
	    writel(['           ','  size = (dlong)((*(short*)(arg_place(pc,2,switchonbound))));',nl]),
	    writel(['           ','  size = ',I,'_len + sizeof(codep *)*size;',nl])
	;
	    (
	      write_additional_string_len(I,size,pc), fail
	    ;
	      true
	    ),
	    writel(['             size += ',I,'_len;',nl]),
	    (is_followed_by_active_yvars(I) ->
		writel(['             size += active_yvar_len;', nl])
	    ;
		true
	    )
	),
	writel(['           ','  memcpy(topofcode,pc,size);',nl]),
	writel(['           ','  pc += size;',nl]),
	writel(['           ','  topofcode += size;',nl]),
	writel(['           ','}',nl]),
	local_write(  '             break;'),nl,nl,
	fail.
gen_move_instructions :-
	told.

%%%%%%%%%%%% things for the code gc

gen_codegc_instructions :-
	tell('codegc_instructions.h'),
	write_header,
	instr(Instr,_Args,Bytes),
	imem(I,Instr),
	write_case(I),
	(
	  I == switchonbound ->
	  writel(['           ','{',nl]),
	  writel(['           ','  dlong size;',nl]),
	  write_relloc_instr(Bytes,1,I),
	  writel(['           ','  size = (dlong)((*(short*)(arg_place(startcode_pred,2,switchonbound))));',nl]),
	  writel(['             startcode_pred += ',I,'_len;',nl]),
	  writel(['           ','  while (size--)',nl]),
	  writel(['           ','    {',nl]),
	  writel(['           ','      relocate_label(offset, (codep *)startcode_pred);',nl]),
	  writel(['           ','      startcode_pred = (codep)((codep *)startcode_pred + 1);',nl]),
	  writel(['           ','    }',nl]),
	  writel(['           ','}',nl])
	;
	  I == fast_execute ->
	  writel(['             ','fill_opcode(startcode_pred,execute);',nl]),
	  writel(['             ','*(dlong *)(arg_place(startcode_pred,1,execute)) = *(dlong *)(arg_place(startcode_pred,2,execute));',nl]),
	  writel(['             startcode_pred += ',I,'_len;',nl])
	;
	  write_relloc_instr(Bytes,1,I),
	  (
	    write_additional_string_len(I,startcode_pred), fail
	  ;
	    true
	  ),
	  writel(['             startcode_pred += ',I,'_len;',nl]),
	  (is_followed_by_active_yvars(I) ->
	      writel(['             startcode_pred += active_yvar_len;', nl])
	  ;
	      true
	  )
	),
	(I == call_unkn ->
	    writel(['             return;',nl])
	;
	    true
	),
	local_write(  '             break;'),nl,nl,
	fail.
gen_codegc_instructions :-
	told.


write_relloc_instr([],_,_).
write_relloc_instr([B|RB],N,I) :-
	(B == opcode ->
	    M = N
	;
	    (B = label(_) ->
		writel(['             relocate_label(offset, (codep *)(arg_place(startcode_pred,', N, ',', I, ')));', nl])
	    ;
		true
	    ),
	    M is N + 1
	),
	write_relloc_instr(RB,M,I).

%% ?- op(0,fy,trust).
%% ?- op(0,xfx,(=..)).

local_write(X) :-
	atom(X), !,
	telling(_,CurrentOutStream),
	sysh:write_atomic(CurrentOutStream,X,off,[]).
local_write(X) :-
	write(X).

%%%%%%%%%%%%%%%%
%% specialization
%%%%%%%%%%%%%%%%


specialize(try,[arity(Arity),label(Alt)],
	   [spec([Arity == 2], try2, [label(Alt)])]).
specialize(trust,[arity(Arity),label(Alt)],
	   [spec([Arity == 2], trust2, [label(Alt)])]).

specialize(retry,[arity(Arity),label(Alt)],
	   [spec([Arity == 3], retry3, [label(Alt)])]).

specialize(try_me_else,[arity(Arity),label(Alt)],
	   [spec([Arity == 1], trymeelse1, [label(Alt)]),
	    spec([Arity == 2], trymeelse2, [label(Alt)]),
	    spec([Arity == 3], trymeelse3, [label(Alt)]),
	    spec([Arity == 4], trymeelse4, [label(Alt)]),
	    spec([Arity == 5], trymeelse5, [label(Alt)])]).
specialize(retry_me_else,[arity(Arity),label(Alt)],
	   [spec([Arity == 1], retrymeelse1, [label(Alt)]),
	    spec([Arity == 2], retrymeelse2, [label(Alt)]),
	    spec([Arity == 3], retrymeelse3, [label(Alt)]),
	    spec([Arity == 4], retrymeelse4, [label(Alt)]),
	    spec([Arity == 5], retrymeelse5, [label(Alt)])]).
specialize(trust_me_else,[arity(Arity)],
	   [spec([Arity == 1], trustmeelse1, []),
	    spec([Arity == 2], trustmeelse2, []),
	    spec([Arity == 3], trustmeelse3, []),
	    spec([Arity == 4], trustmeelse4, []),
	    spec([Arity == 5], trustmeelse5, [])]).
specialize(add_integer,[areg(X), areg(Y), dlong(I)],
	   [spec([X == Y, I == 1], add1, [areg(X)]),
	    spec([X == Y, I == -1], sub1, [areg(X)])]).


%% specialize(putpvalvalvalval,[areg(A1),areg(A2),areg(A3),areg(A4),
%% 	                     yvar(P1),yvar(P2),yvar(P3),yvar(P4)],
%% 	   [spec([A1==1,A2==2,A3==3,A4==4],
%% 	         put4pval,
%% 		 [yvar(P1),yvar(P2),yvar(P3),yvar(P4)])]).


gen_specialize :-
	tell('specialize.h'),
	write_header,
	specialize,
	fail.
gen_specialize :-
	close('specialize.h').

specialize :-
	specialize(Iold,Argold,Specs),
	wl(["      case ", Iold, colon, nl]),
	wl(["        {",nl]),
	B = "          ",
	gen_decl(B, Argold,1,1),
	stepback(B, Iold),
	gen_getargs(B, Iold,Argold),
	gen_spec(Specs,Iold,B),
	wl(["        }",nl]),
	wl(["        break;", nl, nl]),
	fail.

gen_spec([],Iold,B) :-
	wl([B, "update_last_opcode(",Iold,");",nl]).
gen_spec([spec(Conds,Inew,Argsnew)|R],Iold,B) :-
	wl([B,"if ("]),
	wconds(Conds),
	wl([")", nl]),
	wl([B,"  {",nl]),
	gen_fillargs([B,"    "],Inew,Argsnew,1),
	wl([B, "    update_last_opcode(",Inew,");",nl]),
	wl([B,"  }",nl]),
	wl([B,"else",nl]),
	gen_spec(R,Iold,B).

wconds([C|Conds]) :-
	C = (f(I,N) == f(I1,N1)), !,
	wl(["(i", I, "_", N, " == i", I1, "_", N1, ")"]),
	(Conds == [] ->
	    true
	;
	    write(" && "),
	    wconds(Conds)
	).
wconds([C|Conds]) :-
	C = (f(I,N) == F),
	wl(["(i", I, "_", N, " == ", F, ")"]),
	(Conds == [] ->
	    true
	;
	    write(" && "),
	    wconds(Conds)
	).

	    
%%%%%%%%%%%%%%%%
%% compression
%%%%%%%%%%%%%%%%

% compress2: extra arg = transformation from one set of args to new set
% only assignment and + is allowed for now

compress2(uni_void, uni_void, uni_void_2,
	  [],[],[], []).
compress2(uni_void_2, uni_void, uni_void_3,
	  [],[],[], []).
compress2(uni_void_3, uni_void, uni_void_n,
	  [],[],[dlong(I)], [I = 4]).
compress2(uni_void_n, uni_void, uni_void_n,
	  [dlong(I)], [], [dlong(J)], [J is I + 1]).

compress2(unipvar, unipvar, unipvarpvar,
	  [yvar(A1)], [yvar(A2)],
	  [yvar(A3), yvar(A4)],[A3 = A1, A4 = A2]).

compress2(unitvar, unitvar, uni_tvar_tvar,
	  [areg(A1)], [areg(A2)],
	  [areg(A3), areg(A4)],[A3 = A1, A4 = A2]).
compress2(unitvar, uni_void, uni_tvar_tvar,
	  [areg(A1)], [],
	  [areg(A3), areg(A2)],[A2 = 0, A3 = A1]).
compress2(uni_void, unitvar, uni_tvar_tvar,
	  [], [areg(A2)],
	  [areg(A1), areg(A3)],[A1 = 0, A3 = A2]).
compress2(uni_void, uni_void, uni_tvar_tvar,
	  [], [],
	  [areg(A1), areg(A2)],[A1 = 0, A2 = 0]).

compress2(unitval, unitval, uni_tval_tval,
	  [areg(A1)], [areg(A2)],
	  [areg(A3), areg(A4)],[A3 = A1, A4 = A2]).

compress2(unitval, unitvar, uni_tval_tvar,
	  [areg(A1)], [areg(A2)],
	  [areg(A3), areg(A4)],[A3 = A1, A4 = A2]).
compress2(unitval, uni_void, uni_tval_tvar,
	  [areg(A1)], [],
	  [areg(A3), areg(A4)],[A3 = A1, A4 = 0]).

compress2(unitvar, unitval, uni_tvar_tval,
	  [areg(A1)], [areg(A2)],
	  [areg(A3), areg(A4)],[A3 = A1, A4 = A2]).
compress2(uni_void, unitval, uni_tvar_tval,
	  [], [areg(A2)],
	  [areg(A3), areg(A4)],[A3 = 0, A4 = A2]).




compress2(getlist, unitvar, getlist_tvar,
	  [areg(A1)], [areg(B1)],
	  [areg(A2),areg(B2)], [A2 = A1, B2 = B1]).
compress2(getlist, uni_void, getlist_tvar,
	  [areg(A1)], [],
	  [areg(A2),areg(B2)], [A2 = A1, B2 = 0]).
compress2(getlist, unitval, getlist_tval,
	  [areg(A1)], [areg(B1)],
	  [areg(A2),areg(B2)], [A2 = A1, B2 = B1]).


%% compress2(getlist_tvar, unitvar, getlist_tvar_tvar_same,
%% 	  [areg(A1), areg(B1)], [areg(C1)],
%% 	  [areg(A2),areg(B2)],
%% 	  [C1 == A1, A2 = A1, B2 = B1]).

compress2(getlist_tvar, unitvar, getlist_tvar_tvar,
	  [areg(A1), areg(B1)], [areg(C1)],
	  [areg(A2),areg(B2), areg(C2)], [A2 = A1, B2 = B1, C2 = C1]).
compress2(getlist_tvar, uni_void, getlist_tvar_tvar,
	  [areg(A1), areg(B1)], [],
	  [areg(A2),areg(B2), areg(C2)], [A2 = A1, B2 = B1, C2 = 0]).

compress2(getlist_tval, unitvar, getlist_tval_tvar_same,
	  [areg(A1), areg(B1)], [areg(C1)],
	  [areg(A2),areg(B2), areg(C2)],
	  [C1 == A1, A2 = A1, B2 = B1, C2 = C1]).

compress2(getlist_tval, unitvar, getlist_tval_tvar,
	  [areg(A1), areg(B1)], [areg(C1)],
	  [areg(A2),areg(B2), areg(C2)],
	  [A2 = A1, B2 = B1, C2 = C1]).

compress2(getlist_tvar, uni_void, getlist_tvar_tvar,
	  [areg(A1), areg(B1)], [],
	  [areg(A2),areg(B2), areg(C2)], [A2 = A1, B2 = B1, C2 = 0]).

compress2(getlist_tval, uni_void, getlist_tval_tvar,
	  [areg(A1), areg(B1)], [],
	  [areg(A2),areg(B2), areg(C2)], [A2 = A1, B2 = B1, C2 = 0]).


compress2(get_structure, unitvar, getstr_tvar,
	  [areg(A1),atom(Sym1)], [areg(B1)],
	  [areg(A2), areg(B2), atom(Sym2)], [A2 = A1, B2 = B1, Sym2 = Sym1]).
compress2(get_structure, uni_void, getstr_tvar,
	  [areg(A1),atom(Sym1)], [],
	  [areg(A2), areg(B2), atom(Sym2)], [A2 = A1, B2 = 0, Sym2 = Sym1]).

compress2(getstr_tvar, unitvar, getstr_tvar_tvar,
	  [areg(A1), areg(B1), atom(Sym1)], [areg(C1)],
	  [areg(A2), areg(B2), areg(C2),struct(Sym2)],
	  [A2 = A1, B2 = B1, C2 = C1, Sym2 = Sym1]).
compress2(getstr_tvar, uni_void, getstr_tvar_tvar,
	  [areg(A1), areg(B1), atom(Sym1)], [],
	  [areg(A2), areg(B2), areg(C2),struct(Sym2)],
	  [A2 = A1, B2 = B1, C2 = 0, Sym2 = Sym1]).


compress2(unify_list, unitvar, unify_list_tvar,
	  [], [areg(B1)],
	  [areg(B2)], [B2 = B1]).
compress2(unify_list, uni_void, unify_list_tvar,
	  [], [],
	  [areg(I)], [I = 0]).

compress2(putpval, test_nonvar, test_nonvar_p,
	 [yvar(Y1), areg(X1)], [areg(X2), label(Lab2)],
	 [areg(X3), yvar(Y3), label(Lab3)], [X1 == X2, X3 = X1, Y3 = Y1, Lab3 = Lab2]).

compress2(putpval, test_var, test_var_p,
	 [yvar(Y1), areg(X1)], [areg(X2), label(Lab2)],
	 [areg(X3), yvar(Y3), label(Lab3)], [X1 == X2, X3 = X1, Y3 = Y1, Lab3 = Lab2]).

compress2(putpvar, test_var, putpvar,
	 [yvar(Y1), areg(X1)], [areg(X2), label(_Lab2)],
	 [yvar(Y3), areg(X3)], [X1 == X2, X3 = X1, Y3 = Y1]).

compress2(putpval, test_integer, test_integer_p,
	 [yvar(Y1), areg(X1)], [areg(X2), label(Lab2)],
	 [areg(X3), yvar(Y3), label(Lab3)], [X1 == X2, X3 = X1, Y3 = Y1, Lab3 = Lab2]).

compress2(putpval, add_integer, add_integer_p,
	  [yvar(Y1), areg(A1)], [areg(A2), areg(B2), dlong(I2)],
	  [areg(A3), yvar(Y3), areg(C3), dlong(I3)],
	  [A1 == B2, A3 = A2, Y3 = Y1, C3 = A1, I3 = I2]).






% compress: just transfer of arguments - no special things needed
% but could be formulated in terms of compress2

compress(putpval, builtin_end_catch_1, builtin_end_catch_1,
	 [yvar(Y1),areg(_A1)], [areg(_A2)],
	 [areg(Y1)]).  % dangerous !


compress(gettval, proceed, gettval_proceed,
	[areg(X),areg(Y)], [],
	[areg(X),areg(Y)]).

compress(bldtval, bldtval, bldtval2,
	 [areg(A1)], [areg(A2)],
	 [areg(A1), areg(A2)]).

compress(bldtvar, bldtvar, bldtvar2,
	 [areg(A1)], [areg(A2)],
	 [areg(A1), areg(A2)]).

compress(bldtval2, bldtval, bldtval3,
	 [areg(A1), areg(A2)], [areg(A3)],
	 [areg(A1), areg(A2), areg(A3)]).

compress(get_int, proceed, get_int_proceed,
	 [areg(R1),tagged_dlong(I1)], [],
	 [areg(R1),tagged_dlong(I1)]).

compress(set_void, set_void, set_void2,[],[],[]).
compress(set_void2, set_void, set_void3,[],[],[]).
compress(set_void3, set_void, set_void4,[],[],[]).

compress(getpvar, getpvar, getpvar2,
	 [yvar(Y1), areg(A1)], [yvar(Y2), areg(A2)],
	 [areg(A1), areg(A2), yvar(Y1), yvar(Y2)]).

compress(getpvar2, getpvar, getpvar3,
	 [areg(A1), areg(A2), yvar(Y1), yvar(Y2)], [yvar(Y3), areg(A3)],
	 [areg(A1), areg(A2), areg(A3), yvar(Y1), yvar(Y2), yvar(Y3)]).


compress(getpvar3, getpvar, getpvar4,
	 [areg(A1), areg(A2), areg(A3), yvar(Y1), yvar(Y2), yvar(Y3)], [yvar(Y4), areg(A4)],
	 [areg(A1), areg(A2), areg(A3), areg(A4), yvar(Y1), yvar(Y2), yvar(Y3), yvar(Y4)]).


%%%%

compress(getpvar, putpvar, getpvarputpvar,
	 [yvar(Y1),areg(A1)], [yvar(Y2),areg(A2)],
	 [areg(A1), yvar(Y1), areg(A2), yvar(Y2)]).

%%%%
% putpva* +  putpva* -> putpva*pva*
%%%%

compress(putpvar, putpvar, putpvarvar,
	 [yvar(Y1),areg(A1)], [yvar(Y2),areg(A2)],
	 [areg(A1), areg(A2), yvar(Y1), yvar(Y2)]).

compress(putpval, putpvar, putpvarval,
	 [yvar(Y1),areg(A1)], [yvar(Y2),areg(A2)],
	 [areg(A2), areg(A1), yvar(Y2), yvar(Y1)]).

compress(putpvar, putpval, putpvarval,
	 [yvar(Y1),areg(A1)], [yvar(Y2),areg(A2)],
	 [areg(A1), areg(A2), yvar(Y1), yvar(Y2)]).


compress(putpval, putpval, putpvalval,
	 [yvar(Y1),areg(A1)], [yvar(Y2),areg(A2)],
	 [areg(A1), areg(A2), yvar(Y1), yvar(Y2)]).


%%%%
% putpva*pva* +  putpva* -> putpva*pva*pva*
%%%%


compress(putpvarvar, putpvar, putpvarvarvar,
	 [areg(A1),areg(A3),yvar(Y1),yvar(Y3)], [yvar(Y2),areg(A2)],
	 [areg(A1),areg(A2),areg(A3),yvar(Y1),yvar(Y2),yvar(Y3)]).

compress(putpvarvar, putpval, putpvarvarval,
	 [areg(A1),areg(A3),yvar(Y1),yvar(Y3)],
	 [yvar(Y2),areg(A2)], [areg(A1),areg(A3),areg(A2),yvar(Y1),yvar(Y3),yvar(Y2)]).

compress(putpvarval, putpvar, putpvarvarval,
	 [areg(A1),areg(A3),yvar(Y1),yvar(Y3)],
	 [yvar(Y2),areg(A2)], [areg(A1),areg(A2),areg(A3),yvar(Y1),yvar(Y2),yvar(Y3)]).

compress(putpvarval, putpval, putpvarvalval,
	 [areg(A1),areg(A3),yvar(Y1),yvar(Y3)],
	 [yvar(Y2),areg(A2)], [areg(A1),areg(A2),areg(A3),yvar(Y1),yvar(Y2),yvar(Y3)]).

compress(putpvalval, putpvar, putpvarvalval,
	 [areg(A1),areg(A3),yvar(Y1),yvar(Y3)],
	 [yvar(Y2),areg(A2)], [areg(A2),areg(A1),areg(A3),yvar(Y2),yvar(Y1),yvar(Y3)]).


compress(putpvalval, putpval, putpvalvalval,
	 [areg(A1),areg(A2),yvar(Y1),yvar(Y2)],
	 [yvar(Y3),areg(A3)],
	 [areg(A1),areg(A2),areg(A3),yvar(Y1),yvar(Y2),yvar(Y3)]).

%%%
% putpvalvalval + putpval -> putpvalvalvalval
%%%

compress(putpvalvalval, putpval, putpvalvalvalval,
	 [areg(A1),areg(A2),areg(A3),yvar(Y1),yvar(Y2),yvar(Y3)],
	 [yvar(Y4),areg(A4)],
	 [areg(A1),areg(A2),areg(A3),areg(A4), yvar(Y1),yvar(Y2),yvar(Y3),yvar(Y4)]).

%%%%%

compress(allocate, getpvar, allocategetpvar,
	 [yvar(N)], [yvar(Y2), areg(A2)],
	 [yvar(N), yvar(Y2), areg(A2)]).


%%%%
% * + call -> *call
%%%%

compress(getpvar,call,getpvarcall,
	 [yvar(X),areg(Y)], [symbol(Symb),size(Allocated)],
	 [yvar(X),areg(Y),symbol(Symb),size(Allocated)]).

compress(putpvar, call, putpvarcall,
	 [yvar(X),areg(Y)], [symbol(Symb),size(Allocated)],
	 [yvar(X),areg(Y),symbol(Symb),size(Allocated)]).

compress(putpvarvalval, call, putpvarvalvalcall,
	 [areg(A1),areg(A2),areg(A3),yvar(Y1),yvar(Y2),yvar(Y3)], [symbol(Symb),size(Allocated)],
	 [areg(A1),areg(A2),areg(A3),yvar(Y1),yvar(Y2),yvar(Y3),symbol(Symb),size(Allocated)]).


%%%%
% get*breg + try* -> get*breg_try*
%%%%

compress(getpbreg, trymeorelse, getpbreg_trymeorelse,
	 [yvar(I)], [label(B)], [yvar(I),label(B)]).

compress(gettbreg,try_me_else,gettbreg_try_me_else,
	 [areg(_)],[dlong(Arity),dlong(Alt)],[dlong(Arity),dlong(Alt)]).
compress(gettbreg,trymeelse1,gettbreg_try_me_else1,
	 [areg(_)],[dlong(Alt)],[dlong(Alt)]).
compress(gettbreg,trymeelse2,gettbreg_try_me_else2,
	 [areg(_)],[dlong(Alt)],[dlong(Alt)]).
compress(gettbreg,trymeelse3,gettbreg_try_me_else3,
	 [areg(_)],[dlong(Alt)],[dlong(Alt)]).
compress(gettbreg,trymeelse4,gettbreg_try_me_else4,
	 [areg(_)],[dlong(Alt)],[dlong(Alt)]).
compress(gettbreg,trymeelse5,gettbreg_try_me_else5,
	 [areg(_)],[dlong(Alt)],[dlong(Alt)]).

compress(movreg,movreg,movreg2,
	 [areg(X1),areg(Y1)],[areg(X2),areg(Y2)],[areg(X1),areg(Y1),areg(X2),areg(Y2)]).
compress(unify_int,unify_int,unify_int2,
	 [tagged_dlong(I1)],[tagged_dlong(I2)],[tagged_dlong(I1),tagged_dlong(I2)]).
compress(bldpval,bldpval,bldpval2,
	 [yvar(I1)],[yvar(I2)],[yvar(I1),yvar(I2)]).
compress(build_list,build_int,build_list_int,
	 [dlong(I)],[tagged_dlong(N)], [dlong(I), tagged_dlong(N)]).

compress(get_int,get_int,get_int2,
	 [areg(R1),tagged_dlong(I1)], [areg(R2),tagged_dlong(I2)],
	 [areg(R1),areg(R2),tagged_dlong(I1),tagged_dlong(I2)]).

compress(get_int2,get_int,get_int3,
	 [areg(R1),areg(R2),tagged_dlong(I1),tagged_dlong(I2)],
	 [areg(R3),tagged_dlong(I3)],
	 [areg(R1),areg(R2),areg(R3),
	  tagged_dlong(I1),tagged_dlong(I2),tagged_dlong(I3)]).

compress(put_int,put_int,put_int2,
	 [areg(R1),tagged_dlong(I1)], [areg(R2),tagged_dlong(I2)],
	 [areg(R1),areg(R2),tagged_dlong(I1),tagged_dlong(I2)]).

compress(put_int2,put_int,put_int3,
	 [areg(R1),areg(R2),tagged_dlong(I1),tagged_dlong(I2)],
	 [areg(R3),tagged_dlong(I3)],
	 [areg(R1),areg(R2),areg(R3),
	  tagged_dlong(I1),tagged_dlong(I2),tagged_dlong(I3)]).

compress(put_int3,put_int,put_int4,
	 [areg(R1),areg(R2),areg(R3),tagged_dlong(I1),tagged_dlong(I2),tagged_dlong(I3)],
	 [areg(R4),tagged_dlong(I4)],
	 [areg(R1),areg(R2),areg(R3),areg(R4),
	  tagged_dlong(I1),tagged_dlong(I2),tagged_dlong(I3),tagged_dlong(I4)]).

compress_info(I2,Infos) :-
	findall(I2,compress(_,I2,_,_,_,_),I2s),
	sort(I2s,I2sbis),
	local_member(I2,I2sbis),
	findall(compress_with(I1,I2,I3,D1,D2,D3),
		compress(I1,I2,I3,D1,D2,D3),Infos).


gen_compress :-
	tell('compress.h'),
        write_header,
	(
	  gen_compress_code
	;
	  gen_compress_code2
	),
	fail.
gen_compress :-
	close('compress.h').

gen_compress_code :-
	compress_info(I2,Infos),
	wl(["      case ", I2, colon, nl]),
	wl(["        {",nl]),
	gen_compress_code(Infos),
	wl(["        }", nl]),
	wl(["        break;", nl, nl]),
	fail.

gen_compress_code2 :-
	compress_info2(I2,Infos),
	wl(["      case ", I2, colon, nl]),
	wl(["        {",nl]),
	gen_compress_code2(Infos),
	wl(["        }", nl]),
	wl(["        break;", nl, nl]),
	fail.

compress_info2(I2,Infos) :-
	findall(I2,compress2(_,I2,_,_,_,_,_),I2s),
	sort(I2s,I2sbis),
	local_member(I2,I2sbis),
	findall(compress_with(I1,I2,I3,D1,D2,D3,A),
		compress2(I1,I2,I3,D1,D2,D3,A),Infos).


gen_compress_code2([]).
gen_compress_code2([compress_with(I1,I2,I3,D1,D2,D3,A)|R]) :-
	gen_compress_code2(I1,I2,I3,D1,D2,D3,A),
	(R == [] ->
	    true
	;
	    wl(["          else", nl]),
	    gen_compress_code2(R)
	).

gen_compress_code2(I1,I2,I3,D1,D2,D3,A) :-
	wl(["          if (opcodeminus1 == ", I1, ")", nl]),
	wl(["            {", nl]),
	wl(["              /* ", I1, " + ", I2, " -> ", I3, " */", nl]),
	gen_decl("              ",D1,1,1), 
	gen_decl("              ",D2,2,1),
	gen_decl("              ",D3,3,1), 
	stepback("              ",I2),
	gen_getargs("              ",I2,D2),
	stepback("              ",I1),
	gen_getargs("              ",I1,D1),

	gen_condition(A,"              "),
	wl(["              {",nl]),
	transfoargs(A,"                "),
	gen_fillargs("                ",I3,D3,1),
	wl(["                update_opcodes(", I3, ");", nl]),
	wl(["              }",nl]),
	wl(["            else {"]),
	stepforward("",I1),
	stepforward("                  ",I2),
	wl(["                 }",nl]),
	wl(["            }",nl]).

gen_condition(L,I) :-
	wl([I,"if ("]),
	gen_conditions(L),
	wl([" 1)",nl]).

gen_conditions([]).
gen_conditions([A|R]) :-
	(A = (X == Y) ->
	   wl(["("]), wrx(X), wl([" == "]), wrx(Y), wl([") && "])
	;
	   true
	),
	gen_conditions(R).

transfoargs([],_).
transfoargs([A|As],B) :-
	transfoarg(A,B),
	transfoargs(As,B).

transfoarg(X = Y,B) :- !,
	write(B),
	wrx(X),
	wl([" = "]),
	wrx(Y),
	wl([";", nl]).
transfoarg(X is Y + Z,B) :- !,
	write(B),
	wrx(X),
	wl([" = "]),
	wrx(Y),
	wl([" + "]),
	wrx(Z),
	wl([";", nl]).
transfoarg(_ == _,_) :- ! .


wrx(X) :- number(X), !, write(X).
wrx(f(I,N)) :- wl(["i", I, "_", N]).


gen_compress_code([]).
gen_compress_code([compress_with(I1,I2,I3,D1,D2,D3)|R]) :-
	gen_compress_code(I1,I2,I3,D1,D2,D3),
	(R == [] ->
	    true
	;
	    wl(["          else", nl]),
	    gen_compress_code(R)
	).

gen_compress_code(I1,I2,I3,D1,D2,D3) :-
	wl(["          if (opcodeminus1 == ", I1, ")", nl]),
	wl(["            {", nl]),
	wl(["              /* ", I1, " + ", I2, " -> ", I3, " */", nl]),
	gen_decl("              ",D1,1,1),
	gen_decl("              ",D2,2,1),
	stepback("              ",I2),
	gen_getargs("              ",I2,D2),
	stepback("              ",I1),
	gen_getargs("              ",I1,D1),
	gen_fillargs("              ",I3,D3,1),
	wl(["              update_opcodes(", I3, ");", nl]),
	wl(["            }",nl]).

gen_decl(B,D,I,N) :-
	(D == [] ->
	    true
	;
	    D = [T|DR],
	    arg(1,T,f(I,N)),
	    wl([B, "dlong i", I, "_", N, ";", nl]),
	    M is N + 1,
	    gen_decl(B,DR,I,M)
	).

stepback(B,I) :- wl([B, "locpc -= ", I, "_len;", nl]).
stepforward(B,I) :- wl([B, "locpc += ", I, "_len;", nl]).

gen_getargs(B,Instr,D) :-
	(D == [] ->
	    true
	;
	    D = [T|DR],
	    arg(1,T,f(I,N)),
	    wl([B, "i", I, "_", N, " = "]),
	    argument(Instr,T,N),
	    wl([";", nl]),
	    gen_getargs(B,Instr,DR)
	).

argument(Instr,T,I) :-
	functor(T,Name,_),
	wl(["*(", Name, "_type *)(arg_place(locpc,", I, ",", Instr, "))"]).

gen_fillargs(B,Instr,D,A) :-
	(D == [] ->
	    true
	;
	    D = [T|DR],
	    arg(1,T,f(I,N)),
	    wl([B]),
	    argument(Instr,T,A),
	    wl([" = i", I, "_", N, ";", nl]),
	    AA is A + 1,
	    gen_fillargs(B,Instr,DR,AA)
	).


wl([]).
wl([X|R]) :-
	wl1(X),
	wl(R).

wl1(L) :- L = [_|_], !, wl(L).
wl1(nl) :- !, nl.
wl1(colon) :- !, write(":").
wl1(X) :- local_write(X).


%%%%%%%%%%%%%%%%
/*
some instructions have the same action and layout of arguments -
only the type of some arg(s) differs
example: get_atom, get_int, get_char

keeping these as separate instructions, makes compression less powerfull,
so we declare that after loading, get_atom and get_char are turned into
get_int

same for put unify build (and others ?)

from this info, we generate a C function:

type_opcode equivalent_opcode(type_opcode opc)

switch (opc)
{
 case get_atom:
 case get_char: return(get_int);
 ...
}

*/

make_into(get_atom, get_int).
make_into(get_char, get_int).

make_into(get_atomv, get_intv).
make_into(get_charv, get_intv).

make_into(unify_atom, unify_int).
make_into(unify_char, unify_int).

make_into(put_atom, put_int).
make_into(put_char, put_int).

make_into(put_atomv, put_intv).
make_into(put_charv, put_intv).

make_into(build_atom, build_int).
make_into(build_char, build_int).


gen_equivalent :-
	tell('equivalent_opcode.h'),
	write_header,
	writel(['type_opcode equivalent_opcode(type_opcode opc)',nl,'{',nl]),
	writel(['switch (opc)', nl, '{',nl]),
	(
	  make_into(X,Y),
	  writel(['case ', X, ': return(', Y, ');', nl]),
	  fail
	;
	  writel(['default: return(opc);',nl])
	),
	writel(['}',nl, '} /* equivalent_opcode */',nl]),
	fail.
gen_equivalent :-
	close('equivalent_opcode.h').

	



%%%%%%%%%%%%%%%%


%% ?- gen_lowlevel_builtins.
%% ?- gen_builtin_errors.
%% ?- gen_opcodes.
%% ?- gen_loading.
%% ?- gen_builtins.
%% ?- gen_fill_labels.
%% ?- gen_empty_mach.
%% ?- gen_print_instrs.
%% ?- gen_codegc_instructions.
%% ?- gen_profiling_calls.
%% ?- halt.

