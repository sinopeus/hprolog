/*
  A small binding between hProlog and Cryptominisat. Note that this
  binding is stateful and different sequences of calls will result in
  different results.
*/

#include <stdio.h>
#include <assert.h>
#include <stdint.h>

#include "Solver.h"

#define dlong long

Solver *s = NULL; // Initialise the solver instance to null
vec<Lit> lits; // Global variable to keep literals to pass to Cryptominisat

/*
  Deletes the previous instance, clears the global literals variable, and creates a new solver instance. Returns true if all of this succeeds.
*/
extern "C" int minisat_new_solver()
{
  if (s) {
    delete s;
    s = NULL;
  }
  s = new Solver;
  lits.clear();
  return (1);
}


/*
  Deletes the previous instance and clears the global literals variable. Returns true if all of this succeeds.
*/
extern "C" int minisat_delete_solver()
{
  if (s) {
    delete s;
    s = NULL;
  }
  lits.clear();
  return (1);
}


/*
  Given an integer, constructs a Lit object based on the integer and
  increases the amount of variables in the instance accordingly.
*/
static inline Lit pl2lit(int lit)
{
  int var = abs(lit) - 1;
  while (var >= s->nVars()) s->newVar();
  return Lit(var,!(lit > 0));
}


/*
  Adds the current global literals vector to the solver instance as a
  clause.
*/
extern "C" void minisat_add_clause()
{
  vec<Lit> temp = lits;
  s->addClause(temp);
  lits.clear();
}


/*
  Adds the current global literals vector to the solver instance as a
  XOR clause.
*/
extern "C" void minisat_add_xorclause()
{
  bool xor_clause_inverted = false;
  for (uint32_t i = 0; i < lits.size(); i++)
    xor_clause_inverted ^= lits[i].sign();
    
  vec<Lit> temp = lits;
  s->addXorClause(temp, xor_clause_inverted);
  lits.clear();
}


/*
  Adds the provided integer to the literals vector as a Literal
  object.
*/
extern "C" void push_literal(dlong lit)
{
  lits.push(pl2lit(lit));
}


/*
  Asks the solver instance to solve the current problem state.
*/
extern "C" int minisat_solve() {
  lbool x=s->solve();
  return (x==l_True);
}

/*
  Returns whether the variable at the given index is true.
*/
extern "C" int check_model_index(int index) {
  return (s->model[index] == l_True);
}

/*
  Returns the number of variables in the current instance.
*/
extern "C" dlong minisat_nvars()
{
  return s->nVars();
}

/*
  Returns the current value of the variable with index i in the
  current solver instance. Will return false if there is no such
  variable.
*/
extern "C" int val(int i) {
  if(s->model[i] != l_Undef) {
    if (s->model[i]==l_True)
      return i + 1;
    else 
      return -1 * (i + 1);
  }
  else 
    return 0;
}
