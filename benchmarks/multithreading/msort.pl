data_msort(Size, List) :-
  gen_list_qsort(Size, List).

gen_list_qsort(0, []) :- !.
gen_list_qsort(Count, [Elem|Result]) :-
  NewCount is Count - 1,
  random(Elem),
  gen_list_qsort(NewCount, Result).

run_msort(Threads, _, List) :-
  mt_msort(Threads, List, _).

run_msort_native(Threads, _, List) :-
  mt_msort_native(Threads, List, _).

mt_msort(1, List, Sorted) :-
  !,
  st_msort(List, Sorted).
mt_msort(Threads, List, Sorted) :-
  Threads2 is Threads//2,
  split(List, List1, List2),
  threaded((
    mt_msort(Threads2, List1, Sorted1),
    mt_msort(Threads2, List2, Sorted2)
  )),
  merge(Sorted1, Sorted2, Sorted).

mt_msort_native(1, List, Sorted) :-
  !,
  st_msort(List, Sorted).
mt_msort_native(Threads, List, Sorted) :-
  Threads2 is Threads//2,
  split(List, List1, List2),
  hub(Hub),
  spawn_link(Hub, Sorted1, mt_msort_native(Threads2, List1, Sorted1), T1),
  spawn_link(Hub, Sorted2, mt_msort_native(Threads2, List2, Sorted2), T2),
  receive(Hub, T1, the(Sorted1)),
  receive(Hub, T2, the(Sorted2)),
  stop(Hub),
  merge(Sorted1, Sorted2, Sorted).

st_msort([], []) :- !.
st_msort([X], [X]) :- !.
st_msort([X, Y| Xs], Ys) :-
  split([X, Y| Xs], X1s, X2s),
  st_msort(X1s, Y1s),
  st_msort(X2s, Y2s),
  merge(Y1s, Y2s, Ys).

split([], [], []).
split([X| Xs], [X| Ys], Zs) :-
  split(Xs, Zs, Ys).

merge([X| Xs], [Y| Ys], [X| Zs]) :-
  X =< Y, !,
  merge(Xs, [Y| Ys], Zs).
merge([X| Xs], [Y| Ys], [Y| Zs]) :-
  X > Y, !,
  merge([X | Xs], Ys, Zs).
merge([], Xs, Xs) :- !.
merge(Xs, [], Xs).

