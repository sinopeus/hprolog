#include "toinclude.h"

#ifdef MULTI_THREADED

#include "shared_memory.h"

dlong nr_of_running_threads = 1 ;
static pthread_mutex_t running_threads_lock ;

extern void change_nr_of_running_threads(dlong nr){
   pthread_mutex_lock(&running_threads_lock);
   nr_of_running_threads += nr ;
   pthread_mutex_unlock(&running_threads_lock);	
}

#endif
