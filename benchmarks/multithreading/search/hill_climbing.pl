% Threshold = max_depth 14

hill_search(Space, State, Threshold, Solution, Cost) :-
  pid(Pid),
  hill(Space, State, Threshold, [], Path, 0, Cost),
  reverse(Path, Solution).

hill(Space, State, _, Path, [State| Path], Cost, Cost) :-
  goal_state(Space, State).
hill(Space, State, Threshold, Path, Solution, SoFar, Total) :-
  findall(
    (Estimate, Cost, Next),
    (next_state(Space, State, Next, Cost),
           \+ member_path(Next, [State| Path]),
           heuristic(Space, Next, Guess),
           Estimate is Guess + Cost),
    States),
  sort(States, SortedStates),
  member((_, Cost2, Next2), SortedStates),
  SoFar2 is SoFar + Cost2,
  SoFar2 =< Threshold,
  hill(Space, Next2, Threshold, [State| Path], Solution, SoFar2, Total).
