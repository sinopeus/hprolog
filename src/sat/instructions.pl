%% inline_builtin_forgen(create_sat_instance,0,[module(sat)]).
%% inline_builtin_forgen(delete_sat_instance,0,[module(sat)]).
%% inline_builtin_forgen(solve_sat,0,[module(sat)]).
%% inline_builtin_forgen(add_sat_clause,1,[module(sat)]).
%% inline_builtin_forgen(add_xor_clause,1,[module(sat)]).
%% inline_builtin_forgen(sat_get_var_assignment,1,[module(sat)]).
%% inline_builtin_forgen(sat_get_model,1,[module(sat)]).
%% inline_builtin_forgen(sat_assign_model,1,[module(sat)]).
%% inline_builtin_forgen(sat_nvars,1,[module(sat)]).

%% inline_builtin_forgen(create_sat_instance,0).
%% inline_builtin_forgen(delete_sat_instance,0).
%% inline_builtin_forgen(solve_sat,0).
%% inline_builtin_forgen(add_sat_clause,1).
inline_builtin_forgen(add_xor_clause,1).
inline_builtin_forgen(sat_get_var_assignment,1).
inline_builtin_forgen(sat_get_model,1).
inline_builtin_forgen(sat_assign_model,1).
inline_builtin_forgen(sat_nvars,1).
