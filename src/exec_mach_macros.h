#define breg locmach.B
#define ereg locmach.E
#define trreg locmach.TR
#define hreg locmach.H
#define preg locmach.P
#define contpreg locmach.CONTP
#define tos locmach.TOS
#define bh locmach.BH
#define heap_limit locmach.shadow_limit_heap
#define ls_limit locmach.limit_ls
#define cp_limit locmach.limit_cp
#define trail_limit locmach.limit_trail
#define args(i) locmach.Areg[i]
#define start_heap locmach.begin_heap
#define current_out locmach.openfiles[locmach.file_out_nr].fp
#define current_in locmach.openfiles[locmach.file_in_nr].fp
#define current_in_nr locmach.file_in_nr
#define current_out_nr locmach.file_out_nr
#define cutb locmach.CUTB
#define fbuffer locmach.global_charbuf

#ifdef PLAIN_SWITCH
#define hcase(x) case x:
type_opcode opc = 0;
#else
#define hcase(x)
#endif
