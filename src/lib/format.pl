%% begin Sat Sep 20 10:13:53 CEST 2003
%% Bart Demoen
%% end   Mon Sep 22 20:46:50 CEST 2003
/*
format/2: as specified by SICStus Prolog (see below, which is an
excerpt from the manual) but without column boundaries

should be refactored :-)
*/

/*
     The general format of a control sequence is ~NC. The character C
     determines the type of the control sequence. N is an optional numeric
     argument. An alternative form of N is *. * implies that the next
     argument in Arguments should be used as a numeric argument in the
     control sequence. Example:

               | ?- format('Hello~4cworld!', [0'x]).


     and

               | ?- format('Hello~*cworld!', [4,0'x]).


     both produce

               Helloxxxxworld!


     The following control sequences are available.
     ~a
          The argument is an atom. The atom is printed without quoting.
     ~Nc
          (Print character.) The argument is a number that will be
          interpreted as a character code. N defaults to one and is
          interpreted as the number of times to print the character.
     ~Ne
     ~NE
     ~Nf
     ~Ng
     ~NG
          (Print float). The argument is a float. The float and N will be
          passed to the C printf() function as

                         printf("%.Ne", Arg)
                         printf("%.NE", Arg)
                         printf("%.Nf", Arg)
                         printf("%.Ng", Arg)
                         printf("%.NG", Arg)


          respectively.

          If N is not supplied the action defaults to

                         printf("%e", Arg)
                         printf("%E", Arg)
                         printf("%f", Arg)
                         printf("%g", Arg)
                         printf("%G", Arg)


          respectively.
     ~Nd
          (Print decimal.) The argument is an integer. N is interpreted as
          the number of digits after the decimal point. If N is 0 or
          missing, no decimal point will be printed. Example:

                         | ?- format('Hello ~1d world!', [42]).
                         Hello 4.2 world!

                         | ?- format('Hello ~d world!', [42]).
                         Hello 42 world!


     ~ND
          (Print decimal.) The argument is an integer. Identical to ~Nd
          except that , will separate groups of three digits to the left of
          the decimal point. Example:

                         | ?- format('Hello ~1D world!', [12345]).
                         Hello 1,234.5 world!


     ~Nr
          (Print radix.) The argument is an integer. N is interpreted as a
          radix, 2 =< N =< 36. If N is missing the radix defaults to 8. The
          letters a-z will denote digits larger than 9. Example:

                         | ?- format('Hello ~2r world!', [15]).
                         Hello 1111 world!

                         | ?- format('Hello ~16r world!', [15]).
                         Hello f world!


     ~NR
          (Print radix.) The argument is an integer. Identical to ~Nr except
          that the letters A-Z will denote digits larger than 9. Example:

                         | ?- format('Hello ~16R world!', [15]).
                         Hello F world!


     ~Ns
          (Print string.) The argument is a list of character codes. Exactly
          N characters will be printed. N defaults to the length of the
          string. Example:

                         | ?- format('Hello ~4s ~4s!', ["new","world"]).
                         Hello new  worl!

                         | ?- format('Hello ~s world!', ["new"]).
                         Hello new world!


     ~i
          (Ignore.) The argument, which may be of any type, is ignored.
          Example:

                         | ?- format('Hello ~i~s world!', ["old","new"]).
                         Hello new world!


     ~k
          (Print canonical.) The argument may be of any type. The argument
          will be passed to write_canonical/1 (see Term I/O). Example:

                         | ?- format('Hello ~k world!', [[a,b,c]]).
                         Hello .(a,.(b,.(c,[]))) world!


     ~p
          (Print.) The argument may be of any type. The argument will be
          passed to print/1 (see Term I/O). Example:

                         | ?- assert((portray([X|Y]) :- print(cons(X,Y)))).
                         | ?- format('Hello ~p world!', [[a,b,c]]).
                         Hello cons(a,cons(b,cons(c,[]))) world!


     ~q
          (Print quoted.) The argument may be of any type. The argument will
          be passed to writeq/1 (see Term I/O). Example:

                         | ?- format('Hello ~q world!', [['A','B']]).
                         Hello ['A','B'] world!


     ~w
          (Write.) The argument may be of any type. The argument will be
          passed to write/1 (see Term I/O). Example:

                         | ?- format('Hello ~w world!', [['A','B']]).
                         Hello [A,B] world!


     ~@
          (Call.) The argument is a goal, which will be called and expected
          to print on the current output stream. If the goal performs other
          side-effects or does not succeed deterministically, the behavior
          is undefined. Example:

                         | ?- format('Hello ~@ world!', [write(new)]).
                         Hello new world!


     ~~
          (Print tilde.) Takes no argument. Prints ~. Example:

                         | ?- format('Hello ~~ world!', []).
                         Hello ~ world!


     ~Nn
          (Print newline.) Takes no argument. Prints N newlines. N defaults
          to 1. Example:

                         | ?- format('Hello ~n world!', []).
                         Hello
                          world!


     ~N
          (Print Newline.) Prints a newline if not at the beginning of a
          line.

     column boundaries are not implemented
*/

:- module(format,[format/2, format/3]).

:- use_module(library(lists)).

format(_,Format,Args) :-
	% works only for user at this point
	seeing(F),
	seen,
	format(Format,Args),
	see(F).

format(Atom,Args) :-
	atom(Atom), !,
	name(Atom,L),
	format2(L,Args).
format(String,Args) :-
	string(String), !,
	string_to_ilist(String,L),
	format2(L,Args).
format(X,L) :-
	throw(bad_format(X,L)).



format2([],[]).
format2([C|F],Args) :-
	(C == 0'~ ->
	    format2tilde(F,Args)
	;
	    put(C),
	    format2(F,Args)
	).

format2tilde([],[]).
format2tilde([X|R],Args) :-
	((0'0 =< X , X =< 0'9) ->
	    Y is X - 0'0,
	    format2tilden(R,Y,Args)
	;
	    (X == 0'* ->
		format2tildestar(R,Args)
	    ;
		format2tildex(X,R,Args)
	    )
	).

format2tildex(0'a,R,[Arg|Args]) :- !, atom(Arg), write(Arg), format2(R,Args).
format2tildex(0'c,R,[Arg|Args]) :- !, integer(Arg), put(Arg), format2(R,Args).
format2tildex(0'd,R,[Arg|Args]) :- !, integer(Arg), write(Arg), format2(R,Args).
format2tildex(0'D,R,[Arg|Args]) :- !, integer(Arg), writeintD(Arg), format2(R,Args).
format2tildex(0'i,R,[_|Args]) :- !, format2(R,Args).
format2tildex(0'k,R,[Arg|Args]) :- !, write(Arg), format2(R,Args).
format2tildex(0'p,R,[Arg|Args]) :- !, write(Arg), format2(R,Args).
format2tildex(0'q,R,[Arg|Args]) :- !, writeq(Arg), format2(R,Args).
format2tildex(0'r,R,[Arg|Args]) :- !, writebase(Arg,8,small), format2(R,Args).
format2tildex(0'R,R,[Arg|Args]) :- !, writebase(Arg,8,capital), format2(R,Args).
format2tildex(0'n,R,Args)       :- !, nl, format2(R,Args).
format2tildex(0's,R,[Arg|Args]) :- !, string(Arg), write(Arg), format2(R,Args).
format2tildex(0'w,R,[Arg|Args]) :- !, write(Arg), format2(R,Args).

format2tildex(0'e,R,As) :- !, format2tildefloat('%.e',As,R).
format2tildex(0'E,R,As) :- !, format2tildefloat('%.E',As,R).
format2tildex(0'f,R,As) :- !, format2tildefloat('%.f',As,R).
format2tildex(0'g,R,As) :- !, format2tildefloat('%.g',As,R).
format2tildex(0'G,R,As) :- !, format2tildefloat('%.G',As,R).

format2tildex(0'@,R,[Arg|Args]) :- !, call(Arg), format2(R,Args).

format2tildex(0'~,R,Args) :- !, put(0'~), format2(R,Args).


format2tildex(X,  _, _        ) :- throw(unsupported_format(X)).

format2tildefloat(F,[Arg|Args],R) :-
	float(Arg),
	sysh:write_atomic(Arg,off,F),
	format2(R,Args).


format2tilden([X|R],In,Args) :-
	((0'0 =< X , X =< 0'9) ->
	    NewIn is 10*In + (X - 0'0),
	    format2tilden(R,NewIn,Args)
	;
	    format2tildenx(X,In,R,Args)
	).

format2tildenx(0'c,L,R,[Arg|Args]) :- !, integer(Arg), putn(L,Arg), format2(R,Args).
format2tildenx(0'd,L,R,[Arg|Args]) :- !, integer(Arg), writelint(L,Arg), format2(R,Args).
format2tildenx(0'D,L,R,[Arg|Args]) :- !, integer(Arg), writelintD(L,Arg), format2(R,Args).
format2tildenx(0'n,L,R,Args) :- !, writennls(L), format2(R,Args).

format2tildenx(0'r,L,R,[Arg|Args]) :- !, integer(Arg), writebase(Arg,L,small), format2(R,Args).
format2tildenx(0'R,L,R,[Arg|Args]) :- !, integer(Arg), writebase(Arg,L,capital), format2(R,Args).

format2tildenx(0's,L,R,[Arg|Args]) :- !, string(Arg), string_to_ilist(Arg,NArg), writel(L,NArg), format2(R,Args).

format2tildenx(F,L,R,As) :- F = 0'e, !, format2tildefloatn(F,L,As,R).
format2tildenx(F,L,R,As) :- F = 0'E, !, format2tildefloatn(F,L,As,R).
format2tildenx(F,L,R,As) :- F = 0'f, !, format2tildefloatn(F,L,As,R).
format2tildenx(F,L,R,As) :- F = 0'g, !, format2tildefloatn(F,L,As,R).
format2tildenx(F,L,R,As) :- F = 0'G, !, format2tildefloatn(F,L,As,R).


format2tildefloatn(G,N,[Arg|Args],R) :-
	float(Arg),
	name(N,Nl),
	append([0'%,0'.|Nl],[G],Ff),
	name(Ffatom,Ff),
	sysh:write_atomic(Arg,off,Ffatom),
	format2(R,Args).

format2tildestar([F|Fs],[Arg|Args]) :- integer(Arg), format2tildenx(F,Arg,Fs,Args).

writelint(N,Int1) :-
	writesign(Int1,Int),
	(N == 0 ->
	    write(Int)
	;
	    chopupint(Int,[],Lint),
	    length(Lint,Len),
	    (Len > N ->
		P is Len - N,
		putfirstn(P,Lint,RestLint),
		put(0'.),
		putl(RestLint)
	    ;
		P is N - Len,
		put(0'0), put(0'.),
		putn(P,0'0),
		write(Int)
	    )
	).

putfirstn(N,Li,Lo) :-
	(N =:= 0 ->
	    Lo = Li
	;
	    Li = [A|R],
	    put(A),
	    M is N - 1,
	    putfirstn(M,R,Lo)
	).
	
putn(N,A) :-
	(N > 0 ->
	    put(A),
	    M is N - 1,
	    putn(M,A)
	;
	    true
	).


writennls(N) :-
	(N > 0 ->
	    nl,
	    M is N - 1,
	    writennls(M)
	;
	    true
	).

writel(N,L) :-
	(N > 0 ->
	    (L == [] ->
		put(0' ),
		R = L
	    ;
		L = [X|R],
		put(X)
	    ),
	    NN is N - 1,
	    writel(NN,R)
	;
	    true
	).

putl([]).
putl([X|R]) :- put(X), putl(R).

writesign(I,O) :-
	(I < 0 ->
	    put(0'-),
	    O is -I
	;
	    O = I
	).

writelintD(L,Int1) :-
	writesign(Int1,Int),
	(L == 0 ->
	    writeintD(Int)
	;
	    chopupint(Int,[],Chops),
	    AfterDot = L,
	    writeintD(Chops,AfterDot)
	).

writeintD(Int1) :-
	writesign(Int1,Int),
	(Int < 1000 ->
	    write(Int)
	;
	    chopupint(Int,[],Chops),
	    AfterDot = 0,
	    writeintD(Chops,AfterDot)
	).

writeintD(Chops,AfterDot) :-
	length(Chops,L),
	(AfterDot == 0 ->
	    writeintD(L,Chops,_)
	;
	    (L > AfterDot ->
		Before is L - AfterDot,
		writeintD(Before,Chops,RestChops),
		put(0'.),
		putl(RestChops)
	    ;
		put(0'0), put(0'.),
		Zeroes is AfterDot - L,
		putn(Zeroes,0'0),
		putl(Chops)
	    )
	).

writeintD(Before1,Chops1,RestChops) :-
	chopofzero(Chops1,Before1,Chops,Before),
	(Before > 3 ->
	    M is ((Before-1) mod 3) + 1,
	    (M == 1 ->
		Chops = [A|R],
		put(A)
	    ;
		M == 2 ->
		Chops = [A,B|R],
		put(A), put(B)
	    ;
		M == 3 ->
		Chops = [A,B,C|R],
		put(A), put(B), put(C)
	    ),
	    NewB is Before - M,
	    consumecomma(NewB,R,RestChops)
	;
	    (Before == 1 ->
		Chops = [A|RestChops], put(A)
	    ;
		Before == 2 ->
		Chops = [A,B|RestChops], put(A), put(B)
	    ;
		Chops = [A,B,C|RestChops], put(A), put(B), put(C)
	    )
	).

chopofzero([],_,[0'0],1).
chopofzero([X|R],N,Lo,No) :-
	(X == 0'0 ->
	    NN is N - 1,
	    chopofzero(R,NN,Lo,No)
	;
	    Lo = [X|R],
	    No = N
	).


consumecomma(B,In,Out) :-
	(B == 0 ->
	    Out = In
	;
	    BB is B - 3,
	    In = [X,Y,Z|R],
	    put(0',),
	    put(X), put(Y), put(Z),
	    consumecomma(BB,R,Out)
	).


% chopupint is the poor mans implementation for name/2 on integers
% that might be too big for name/2 to handle ...

chopupint(I,Li,Lo) :-
	(I < 10000 ->
	    name(I,Lint1),
	    extend(Lint1,4,Lint),
	    append(Lint,Li,Lo)
	;
	    II is I // 10000,
	    J is I mod 10000,
	    name(J,Lint1),
	    extend(Lint1,4,Lint),
	    append(Lint,Li,NewLi),
	    chopupint(II,NewLi,Lo)
	).

extend(L1,N,L2) :-
	length(L1,M),
	D is N - M,
	extend2(D,L1,L2).

extend2(D,L1,L2) :-
	(D > 0 ->
	    DD is D - 1,
	    L2 = [0'0|L3],
	    extend2(DD,L1,L3)
	;
	    L2 = L1
	).


writebase(Int1,Base,Capital) :-
	writesign(Int1,Int),
	chopupbase(Int,Base,[],Chops),
	writebasechops(Chops,Capital).

chopupbase(I,Base,Li,Lo) :-
	(I < Base ->
	    Lo = [I|Li]
	;
	    II is I // Base,
	    J is I mod Base,
	    NewLi = [J|Li],
	    chopupbase(II,Base,NewLi,Lo)
	).

writebasechops([],_).
writebasechops([C|Chops],Capital) :-
	writebasechop(C,Capital),
	writebasechops(Chops,Capital).

writebasechop(C,Capital) :-
	(C < 10 ->
	    write(C)
	;
	    (Capital == capital ->
		CC is (C - 10) + 0'A
	    ;
		CC is (C - 10) + 0'a
	    ),
	    put(CC)
	).
