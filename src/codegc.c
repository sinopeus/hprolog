/* *********************************************************************
 * code garbage collection is called during a consult or a make_pred_*
 * that's out of the reach of the emulator, so we have no other choice
 * than to use globalmach
 *
 * UPDATE: Tue Dec  7 13:43:33 CET 2010 Leuven
 * code_gc() is only called inside machine.h in cinter/4, codegc/0 and
 * load_file/2. All those builtins have a reference to locmach,
 * so we don't need to use globalmach.
 *
 * general idea: before loading a pred, the length should be known
 * if not enough code space: collect
 * if still not enough code space (needed): expand
 * there is no need to look at active yvar entries - we must skip them
 *
 * start: Wed Aug 28 08:42:14 CEST 2002  Maratea
 * first time tested: Wed Sep  4 11:27:27 CEST 2002 Leuven
 * first time satisfied: Fri Sep  6 13:07:27 CEST 2002 Leuven
 * most recent bug on  : Sun Sep  8 01:41:19 CEST 2002 Leuven
 **********************************************************************
 * 6 sept: works when called from builtin predicate codegc/0
 *         todo: expansion if needed
 *
 * ********************************************************************/



#include "toinclude.h"
#include <stdlib.h>
#include <string.h>

extern void fatal_message(char *);

/*
code gc:

	maak parallelle array voor new
	markeer de predstarts die nog in gebruik zijn door
		- overloop functortable
		- overloop stacks
		- gebruik 2 bits in de oude
	verschuif code - sliding upwards/reloceer
		reloceer code pointers
	markeer in //: 	nieuwe predstart
	overloop functortable/stacks:
		gebruik diff oud/nieuw als offset
	copy // naar predstarts
*/

extern dlong *predstarts;
extern dlong nrofpredstarts;
extern dlong numb_symbs;
extern type_opcode next_instruction(codep);

#if 0
static dlong find_pred_start_lin(dlong i, dlong where[])
{
  dlong p;
  dlong j;

  if ((p = (dlong)entries[i]))
    {
      p = (codep)p - codezone;
      p = p<<2;
      for (j = 0; j < nrofpredstarts; j++) // linear - should be log
	{
	  if (p == where[j])
	    return(j);
	}
    }
  return(-1);
} /* find_pred_start_lin */

static dlong find_approx_pred_start_lin(codep cp, dlong where[])
{
  dlong p, j;


  p = cp-codezone;
  p = p<<2;

  for (j = 0; j < nrofpredstarts; j++) // linear - should be log
    {
      if (p < where[j])
	return(j-1);
    }
  return(nrofpredstarts-1);
} /* find_approx_pred_start_lin */
#endif


/* binary search */

static dlong find_pred_start_bin(dlong i, dlong where[])
{
  dlong p,q;
  dlong first, last, mid;

  p = (dlong)entries[i];
  if (! p) return(-1);
  p = (codep)p - codezone;
  p = p<<2;

  first = 0; last = nrofpredstarts-1;

 loop:
  mid = (last+first)/2;
  q = where[mid];
  if (p < q)
    {
      last = mid-1;
      if (last<first)
	return(-1);
      goto loop;
    }
  if (p > q)
    {
      first = mid+1;
      if (first > last)
	return(-1);
      goto loop;
    }
  return(mid);
} /* find_pred_start_bin */

static dlong find_approx_pred_start_bin(codep cp, dlong where[])
{
  dlong p,q;
  dlong first, last, mid;

  p = cp-codezone;
  p = p<<2;

  first = 0; last = nrofpredstarts-1;

 loop:
  mid = (last+first)/2;
  q = where[mid];
  if (p < q)
    {
      last = mid-1;
      if (last<first)
	return(-1);
      goto loop;
    }
  if (p > q)
    {
      if (p < where[mid+1])
	return(mid);
      first = mid+1;
      if (first > last)
	return(-1);
      goto loop;
    }
  return(mid);
} /* find_approx_pred_start_bin */

#if 0
static dlong find_pred_start_both(dlong i, dlong where[])
{
  dlong l,b;
  l = find_pred_start_lin(i,where);
  b = find_pred_start_bin(i,where);
  if (l != b)
    fprintf(stderr,"scream %ld %ld\n",l,b);
  return(l);
} /* find_pred_start */

static dlong find_approx_pred_start_both(codep cp, dlong where[])
{
  dlong l,b;
  l = find_approx_pred_start_lin(cp,where);
  b = find_approx_pred_start_bin(cp,where);
  if (l != b)
    fprintf(stderr,"scream approx %ld %ld\n",l,b);
  return(l);
} /* find_approx_pred_start */
#endif

#define find_pred_start(a,b) find_pred_start_bin(a,b)
#define find_approx_pred_start(a,b) find_approx_pred_start_bin(a,b)


static void mark_predstart(dlong j)
{
  if (j >= 0)
    predstarts[j] = predstarts[j] | 0x1;
} /* mark_predstart */

#define predstart_marked(j) ((dlong)predstarts[j] & 0x1)

static void relocate_label(dlong offset, codep *p)
{
  if (*p)
    *p = *p + offset;
} /* relocate_label */

static void relocate_pred_code(dlong offset, codep startcode_pred)
{
  type_opcode opc, prevopc;

  if (! offset) return;
  opc = 0;
  while (1)
    {
      prevopc = opc;
      opc = next_instruction(startcode_pred);

      switch (opc)
	{
	case end_clauses:
	  return;

#include "codegc_instructions.h"

	default:
	  fprintf(stderr,"previous %d %s\n",prevopc,instr_name[prevopc]);
	  fprintf(stderr,"current ??? %d\n",opc);
	  fprintf(stderr,"current ??? %s\n",instr_name[opc]);
	  exit(0); 
	}
    }
} /* relocate_pred_code */

extern codep topofcode;
extern dlong CODEZONEL;

#define CODE_INCREMENT 15000000

static dlong nrofcodeexps = 0;

static dlong code_expansion(struct machine* machp)
{
  dlong i, j, startoffset, endoffset, offset;
  dlong *startls, *stopls, *p, *q, *b;
  codep startcode_pred, endcode_pred, newcodezone, oldcodezone, oldtopofcode;
  dlong t1,t2;

  t1 = get_cpu_time();
  nrofcodeexps++;

  if (verbose_mode)
      printf("\nCode expansion %ld start\n",nrofcodeexps);

  newcodezone = (codep)realloc(codezone,(CODEZONEL+CODE_INCREMENT)*sizeof(byte));
  if (! newcodezone)
    {
      fatal_message("code expansion failed - no malloc possible");
      return(0);
    }
  
  CODEZONEL += CODE_INCREMENT;
  
  offset = newcodezone-codezone;
  oldcodezone = codezone;
  codezone = newcodezone;
  oldtopofcode = topofcode;
  topofcode += offset;
  
  if (offset == 0)
    goto code_expansion_end_message;


  /* relocate external code pointers */
  for (i = 0; i < (nrofpredstarts-1); i++)
    {
      startoffset = predstarts[i] >> 2;
      startcode_pred = newcodezone + startoffset;
      relocate_pred_code(offset,startcode_pred);
    }

  /* relocate root pointers */

  /* relocate symbol table */

  for (i = 0; i < numb_symbs; i++)
    {
      if (entries[i])
	entries[i] += offset;
    }

  /* relocate environments */

  startls = machp->begin_ls;
  stopls = machp->TOS;

  for (p = startls; p > stopls; p--)
    {
      q = (dlong *)*p;
      if (is_ref(q) && (oldcodezone <= (codep)q) && ((codep)q <= oldtopofcode))
	  *p = (dlong)((codep)q + offset);
    }

  /* relocate choice points */

  for (b = machp->B; (b && (b < machp->begin_cp)); b = ((CP_P)b)->b)
    {
      ((CP_P)b)->failurecont += offset;

      ((CP_P)b)->cp += offset;
    }

  /* relocate P */

  machp->P += offset;

  /* relocate CONTP */

  machp->CONTP += offset;


 code_expansion_end_message:
  t2 = get_cpu_time();
  t2 = t2 - t1;

  if (verbose_mode)
    printf("Code expansion (offset %ld) - time taken: %ld milliseconds\n",offset,t2);

  return(1);
} /* code_expansion */

static dlong nrcodegcs = 0;

dlong code_gc(dlong expand, struct machine* machp)
{
  dlong *newpredstarts;
  dlong i, j, newnrofpredstarts, startoffset, endoffset, offset;
  dlong *startls, *stopls, *p, *q, *b;
  codep startcode, startcode_pred, endcode_pred;
  dlong t1,t2;

  if (expand)
    return(code_expansion(machp));

  nrcodegcs++;

  t1 = get_cpu_time();

  if (verbose_mode)
    printf("\nCode gc %ld - start size = %ld bytes\n",nrcodegcs,(dlong)(topofcode-codezone));

  newpredstarts = (dlong *)malloc(nrofpredstarts*sizeof(dlong));
  if (! newpredstarts)
    fatal_message("initial allocation during code gc failed");

  /* mark from symbol table */

  for (i = 0; i < numb_symbs; i++)
    {
      j = find_pred_start(i,predstarts);
      if (j >= 0)
	mark_predstart(j);
    }

  /* mark from environments */

  startls = machp->begin_ls;
  stopls = machp->TOS;

  for (p = startls; p > stopls; p--)
    {
      q = (dlong *)*p;
      if (is_ref(q) && (codezone <= (codep)q) && ((codep)q <= topofcode))
	{
	  j = find_approx_pred_start((codep)q,predstarts);
	  mark_predstart(j);
	}
    }

  /* mark from choice points */

  for (b = machp->B; (b && (b < machp->begin_cp)); b = ((CP_P)b)->b)
    {
      j = find_approx_pred_start(((CP_P)b)->failurecont,predstarts);
      mark_predstart(j);

      j = find_approx_pred_start(((CP_P)b)->cp,predstarts);
      mark_predstart(j);
    }

  /* mark from current P */

  j = find_approx_pred_start(machp->P,predstarts);
  mark_predstart(j);

  /* mark from current CONTP */

  j = find_approx_pred_start(machp->CONTP,predstarts);
  mark_predstart(j);



  /* slide */

  // fprintf(stderr,"old nrofpredstarts %ld old topofcode %ld\n",nrofpredstarts,predstarts[nrofpredstarts-1]);
  newnrofpredstarts = 0;
  startcode = codezone;
  for (i = 0; i < (nrofpredstarts-1); i++)
    {
      if (predstart_marked(i))
	{
	  startoffset = predstarts[i] >> 2;
	  startcode_pred = codezone + startoffset;
	  endoffset = predstarts[i+1] >> 2;
	  endcode_pred = codezone + endoffset;
	  newpredstarts[newnrofpredstarts] = predstarts[i] & ~0x1;
	  predstarts[newnrofpredstarts] = (startcode-codezone)<<2;
	  offset = startcode-startcode_pred;
	  relocate_pred_code(offset,startcode_pred);
	  memmove(startcode,startcode_pred,endcode_pred-startcode_pred);
	  startcode = startcode + (endcode_pred-startcode_pred);
	  newnrofpredstarts++;
	}
      //   else
      // fprintf(stderr,"%ld not marked - skipped %ld\n",i,(predstarts[i+1] >> 2) - (predstarts[i] >> 2));
    }

  newpredstarts[newnrofpredstarts] = (topofcode-codezone)<<2;
  predstarts[newnrofpredstarts] = (startcode-codezone)<<2;
  // fprintf(stderr,"nrofpredstarts = newnrofpredstarts ? %ld %ld\n",nrofpredstarts,newnrofpredstarts);
  nrofpredstarts = newnrofpredstarts+1;
  
  // fprintf(stderr,"new nrofpredstarts %ld new topofcode %ld\n",nrofpredstarts,predstarts[nrofpredstarts-1]);

  /* relocate root pointers */

  /* relocate symbol table */

  for (i = 0; i < numb_symbs; i++)
    {
      j = find_pred_start(i,newpredstarts);
      if (j >= 0)
	entries[i] = codezone + (predstarts[j]>>2);
      // if (newpredstarts[j] != predstarts[j])
      // fprintf(stderr,"old-new entries: %ld %ld %ld\n",
      //                 j,(newpredstarts[j]>>2),(predstarts[j]>>2));
    }

  /* relocate environments */

  startls = machp->begin_ls;
  stopls = machp->TOS;

  for (p = startls; p > stopls; p--)
    {
      q = (dlong *)*p;
      if (is_ref(q) && (codezone <= (codep)q) && ((codep)q <= topofcode))
	{
	  j = find_approx_pred_start((codep)q,newpredstarts);
	  offset = (predstarts[j]>>2)-(newpredstarts[j]>>2);
	  //  	  fprintf(stderr,"Code gc - offset env = %ld %ld\n",offset,j);
	  *p = (dlong)((codep)q + offset);
	}
    }

  /* relocate choice points */

  for (b = machp->B; (b && (b < machp->begin_cp)); b = ((CP_P)b)->b)
    {
      j = find_approx_pred_start(((CP_P)b)->failurecont,newpredstarts);
      offset = (predstarts[j]>>2)-(newpredstarts[j]>>2);
      //            fprintf(stderr,"Code gc - offset cp alt= %ld\n",offset);
      ((CP_P)b)->failurecont += offset;

      j = find_approx_pred_start(((CP_P)b)->cp,newpredstarts);
      offset = (predstarts[j]>>2)-(newpredstarts[j]>>2);
      //            fprintf(stderr,"Code gc - offset cp cont= %ld\n",offset);
      ((CP_P)b)->cp += offset;
    }

  /* relocate P */

  j = find_approx_pred_start(machp->P,newpredstarts);
  offset = (predstarts[j]>>2)-(newpredstarts[j]>>2);
  //    fprintf(stderr,"Code gc - offset P = %ld\n",offset);
  machp->P += offset;

  /* relocate CONTP */

  j = find_approx_pred_start(machp->CONTP,newpredstarts);
  offset = (predstarts[j]>>2)-(newpredstarts[j]>>2);
  //    fprintf(stderr,"Code gc - offset CONTP = %ld\n",offset);
  machp->CONTP += offset;

  topofcode = codezone + (predstarts[nrofpredstarts-1]>>2);

  free(newpredstarts);

  t2 = get_cpu_time();
  t2 = t2 - t1;

  if (verbose_mode)
    {
      printf("Code gc - time taken: %ld milliseconds\n",t2);
      printf("Code gc - end size = %ld bytes\n",(dlong)(topofcode-codezone));
    }

  return(1);

} /* code_gc */
