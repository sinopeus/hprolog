run_fib(Threads, P) :-
  mt_fib(Threads, P, _),
  !.
run_fib_native(Threads, P) :-
  mt_fib_native(Threads, P, _),
  !.

mt_fib(_, 0, 1) :- !.
mt_fib(_, 1, 1) :- !.
mt_fib(1, N, F) :- !,
  st_fib(N, F).
mt_fib(Threads, N, F) :-
  Threads > 1,
  Threads2 is Threads//2,
  N > 1,
  N1 is N - 1,
  N2 is N - 2,
  threaded((
    mt_fib(Threads2, N1, F1),
    mt_fib(Threads2, N2, F2)
  )),
  F is F1 + F2.

mt_fib_native(_, 0, 1) :- !.
mt_fib_native(_, 1, 1) :- !.
mt_fib_native(1, N, F) :- !,
  st_fib(N, F).
mt_fib_native(Threads, N, F) :-
  Threads > 1,
  Threads2 is Threads//2,
  N > 1,
  N1 is N - 1,
  N2 is N - 2,
  hub(Hub),
  spawn_link(Hub, F1, mt_fib_native(Threads2, N1, F1), T1),
  spawn_link(Hub, F2, mt_fib_native(Threads2, N2, F2), T2),
  receive(Hub, T1, the(F1)),
  receive(Hub, T2, the(F2)),
  stop(Hub),
  F is F1 + F2.

st_fib(0, 1) :- !.
st_fib(1, 1) :- !.
st_fib(N, F) :-
  N > 1,
  N1 is N - 1,
  N2 is N - 2,
  st_fib(N1, F1),
  st_fib(N2, F2),
  F is F1 + F2.

