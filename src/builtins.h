extern void set_scope_marker(codep);
extern void unwind_stack(struct machine *);

extern dlong * builtin_copy_term(dlong *, struct machine *);
extern dlong * builtin_copy_term_nat(dlong *, struct machine *);

extern dlong copy_out(dlong *, struct machine *);
extern void copy_in(dlong *, dlong *, dlong, struct machine *);

extern void fatal_message(char *);

extern dlong module_from_index(dlong);

extern void find_name_arity(codep, dlong *, dlong *);
extern void find_pred_index(codep, dlong *);

// extern dlong find_symbol_from_codep(codep);

extern dlong findall_init_c(dlong, struct machine *);

extern dlong findall_add(dlong, dlong, struct machine *);

extern dlong get_findall_size(dlong, struct machine *);
extern dlong findall_get_solutions(dlong, dlong, dlong, struct machine *, dlong);

extern void findall_free(int, findall_solution_list*, struct machine *);

extern dlong do_sort(dlong *, dlong, dlong *, struct machine*);

