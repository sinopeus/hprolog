/*
  Too many bugs in my own bigint.c after support for 64 bits
  And much slower than gmp
  Trying out gmp now
  Mon Aug  8 11:43:30 CEST 2011
*/

#ifdef USE_GMP

#include "toinclude.h"
#include <gmp.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mach_macros.h"

extern void fatal_message(char *);

extern dlong alloc_new_bigint(dlong, struct machine *);

/*-------------------------------------------------------------------------*/
static INLINE void hbig_to_gbig(dlong *p, mpz_t gbig, struct machine *machp)
{
  p = get_number_pointer(p) + 1;

  gbig->_mp_alloc = bigint_size(*p);
  if (bigint_positive(*p))
    gbig->_mp_size = gbig->_mp_alloc;
  else
    gbig->_mp_size = -(gbig->_mp_alloc);

  if (bigint_onheap(*p))
    gbig->_mp_d = p+1;
  else
    gbig->_mp_d = (machp->excess_bigints)[*(p+1)];

} /* hbig_to_gbig */

/*-------------------------------------------------------------------------*/
void bigint_free_string(char *pc, struct machine *machp)
{
  if (pc == machp->global_charbuf) return;
  free(pc);
} /* bigint_free_string */

/*-------------------------------------------------------------------------*/
dlong bigint_to_string(dlong *p1, char **pstring, struct machine* machp, dlong base)
{
  mpz_t big1;

  hbig_to_gbig(p1,big1,machp);

  *pstring = mpz_get_str(0,base,big1);

  return(1);
} /*bigint_to_string */

/*-------------------------------------------------------------------------*/

dlong bigint_low_msb(dlong *p1, struct machine *machp)
{
  /* will return -1 if something is wrong */
  /* p is dereffed allready */

  mpz_t big1;
  int toclear = 0;
  dlong size;

  if (is_smallint(p1))
    { 
      size = get_smallint(p1);
      if (size < 0) return(-1);
      mpz_init_set_si(big1, size);
      toclear = 1;
    }
  else if (is_bigint(p1))
    {
      hbig_to_gbig(p1,big1,machp);
      if (mpz_sgn(big1) < 0) return(-1);
    }
  else return(-1);

  size = mpz_sizeinbase(big1,2);

  if (toclear) mpz_clear(big1);

  if (! in_smallint_range(size)) return(-1);

  return(size-1);

} /* bigint_low_msb */

/*-------------------------------------------------------------------------*/
dlong get_float_from_number(dlong *p1, dfloat *f, struct machine *machp)
{
  mpz_t big1;
  if (is_smallint(p1)) { *f = get_smallint(p1); return(1); }
  if (is_real(p1)) { *f = get_real(p1); return(1); }
  if (! is_bigint(p1)) return(0);
  hbig_to_gbig(p1,big1,machp);
  *f = mpz_get_d(big1);
  return(1);
} /* get_float_from_number */

/*-------------------------------------------------------------------------*/

static INLINE void gbig_to_hbig(mpz_t gbig, struct machine *machp)
{
  dlong size = gbig->_mp_size;
  dlong pos;
  dlong *h = lochreg;

  if (size > 0) pos = 1; else {pos = 0; size = -size; }

  *h = BIGINT_HEADER; h++;
  if ((h+size+1) > machp->limit_heap)
    {
      dlong *p;
      dlong i;
      *h = bigint_info_notonheap(pos,size); h++;
      i = alloc_new_bigint(size,machp);
      *h = i;
      p = (machp->excess_bigints)[i];
      heap_limit = (dlong *)1;
      memcpy(p,gbig->_mp_d,size*sizeof(dlong));
      lochreg = h+1;
      return;
    }

  *h = bigint_info_onheap(pos,size); h++;
  memcpy(h,gbig->_mp_d,size*sizeof(dlong));
  lochreg = h+size;

} /* gbig_to_hbig */

/*-------------------------------------------------------------------------*/

dlong bigint_result(mpz_t gbig, struct machine *machp)
{
  if ((mpz_cmp_si(gbig,MAXSMALLINT) > 0) || (mpz_cmp_si(gbig,MINSMALLINT) < 0))
    {
      // it remains a bigint - copy to heap
      dlong *resp = lochreg;
      gbig_to_hbig(gbig,machp);
      mpz_clear(gbig);
      return(make_number_pointer(resp));
    }
  else
    {
      // it is a smallint
      dlong i;
      i = mpz_get_si(gbig);
      mpz_clear(gbig);
      return(make_smallint(i));
    }
} /* bigint_result */

/*-------------------------------------------------------------------------*/

/*
  both should be integers, one could be a smallint
  the return should be either a smallint
                          or a number pointer to a bigint (on the heap or not)
			  or 0 in case of failure
  
/*-------------------------------------------------------------------------*/

dlong addsub_two_inumbers(dlong *p1, dlong *p2, dlong addition, struct machine *machp)
{
  mpz_t res, big1, big2;

  if ((!is_inumber(p1)) || (! is_inumber(p2))) return(0);

  mpz_init(res);

  if (is_smallint(p1))
    {
      dlong i = get_smallint(p1);
      hbig_to_gbig(p2,big2,machp);
      if (i >= 0)
	{ if (addition) mpz_add_ui(res, big2, i); else mpz_ui_sub(res, i, big2); }
      else
	{
	  i = -i;
	  if (addition) mpz_sub_ui(res, big2, i); else { mpz_add_ui(res, big2, i); mpz_neg(res,res);}
	}
    }
  else if (is_smallint(p2))
    {
      dlong i = get_smallint(p2);
      hbig_to_gbig(p1,big1,machp);
      if (i >= 0)
	{ if (addition) mpz_add_ui(res, big1, i); else mpz_sub_ui(res, big1, i); }
      else
	{
	  i = -i;
	  if (addition) mpz_sub_ui(res, big1, i); else { mpz_add_ui(res, big1, i); }
	}
    }
  else
    {
      hbig_to_gbig(p1,big1,machp);
      hbig_to_gbig(p2,big2,machp);
      if (addition) mpz_add(res, big1, big2); else mpz_sub(res, big1, big2);
    }

  return(bigint_result(res,machp));
} /* addsub_two_inumbers */

/*------------------------------------------------------------------------------*/

void add_ints(mpz_t *big1, dlong *p2, struct machine *machp)
{
  mpz_t big2;

  if (is_smallint(p2))
    {
      dlong i = get_smallint(p2);
      if (i >= 0)
	mpz_add_ui(*big1, *big1, i);
      else
	{
	  i = -i;
	  mpz_sub_ui(*big1, *big1, i);
	}
    }
  else
    {
      hbig_to_gbig(p2,big2,machp);
      mpz_add(*big1, *big1, big2);
    }
} /* add_ints */


/*-------------------------------------------------------------------------*/
dlong multiply_two_inumbers(dlong *p1, dlong *p2, struct machine *machp)
{
  mpz_t res, big1, big2;

  mpz_init(res);

  if (is_smallint(p1))
    {
      if (is_smallint(p2))
	mpz_init_set_si(big2, get_smallint(p2));
      else
	{
	  if (! is_bigint(p2)) return(0);
	  hbig_to_gbig(p2,big2,machp);
	}
      mpz_mul_si(res, big2, get_smallint(p1));
    }
  else
    {
      if (! is_bigint(p1)) return(0);
      hbig_to_gbig(p1,big1,machp);
      if (is_smallint(p2)) 
	mpz_mul_si(res, big1, get_smallint(p2));
      else 
	{
	  if (! is_bigint(p2)) return(0);
	  hbig_to_gbig(p2,big2,machp);
	  mpz_mul(res, big1, big2);
	}
    }

  return(bigint_result(res,machp));

} /* multiply_two_inumbers */


dlong shiftleft_two_inumbers(dlong *, dlong *, struct machine *);
/*-------------------------------------------------------------------------*/
dlong shiftright_two_inumbers(dlong *p1, dlong *p2, struct machine *machp)
{
  mpz_t res, big1;
  dlong i1, i2;

  deref(p1); deref(p2);
  if ((!is_inumber(p1)) || (! is_smallint(p2))) return(0);

  i2 = get_smallint(p2);
  if (i2 < 0) return(shiftleft_two_inumbers(p1,(dlong *)make_smallint(-i2),machp));
  if (i2 == 0) return((dlong)p1);

  mpz_init(res);

  if (is_smallint(p1))
    { 
      i1 = get_smallint(p1);
      mpz_init_set_si(big1, i1);
      mpz_tdiv_q_2exp(res, big1, i2);
      mpz_clear(big1);
      if ((i1 < 0) && (mpz_sgn(res) == 0)) return(make_smallint(-1));
      return(bigint_result(res,machp));  
    }

  hbig_to_gbig(p1,big1,machp);

  mpz_tdiv_q_2exp(res, big1, i2);
  if ((mpz_sgn(big1) < 0) && (mpz_sgn(res) == 0))
    {
      mpz_clear(res);
      return(make_smallint(-1));
    }

  return(bigint_result(res,machp));

} /*shiftright_two_inumbers */


/*-------------------------------------------------------------------------*/
dlong shiftleft_two_inumbers(dlong *p1, dlong *p2, struct machine *machp)
{
  mpz_t res, big1;
  dlong i2;

  deref(p1); deref(p2);
  if ((!is_inumber(p1)) || (! is_smallint(p2)))
    return(0);

  i2 = get_smallint(p2);
  if (i2 < 0) return(shiftright_two_inumbers(p1,(dlong *)make_smallint(-i2),machp));
  if (i2 == 0) return((dlong)p1);

  mpz_init(res);

  if (is_smallint(p1))
    {
      mpz_init_set_si(big1, get_smallint(p1));
      mpz_mul_2exp(res, big1, i2);
      mpz_clear(big1);
      return(bigint_result(res,machp));
    }

  hbig_to_gbig(p1,big1,machp);
  mpz_mul_2exp(res, big1, i2);
  return(bigint_result(res,machp));
} /*shiftleft_two_inumbers */

/*-------------------------------------------------------------------------*/
dlong bigint_low_abs(dlong *p1, struct machine *machp, dlong makepos)
{
  mpz_t res, big1;

  mpz_init(res);

  hbig_to_gbig(p1,big1,machp);

  if (makepos) mpz_abs(res, big1);
  else mpz_neg(res, big1);

  return(bigint_result(res,machp));
} /*bigint_low_abs */


/*-------------------------------------------------------------------------*/
dlong float_to_bigint(dfloat f, struct machine *machp)
{
  mpz_t res;

  mpz_init_set_d(res,f);

  return(bigint_result(res,machp));
} /*float_to_bigint */


/*-------------------------------------------------------------------------*/
dlong and_two_inumbers(dlong *p1, dlong *p2, struct machine *machp)
{
  mpz_t res, big1, big2;
  int toclear = 0;

  mpz_init(res);

  if (is_smallint(p1)) { toclear += 1; mpz_init_set_si(big1, get_smallint(p1));}
  else hbig_to_gbig(p1,big1,machp);

  if (is_smallint(p2)) { toclear += 2; mpz_init_set_si(big2, get_smallint(p2));}
  else hbig_to_gbig(p2,big2,machp);


  mpz_and(res, big1, big2);

  if (toclear & 0x1) mpz_clear(big1);
  if (toclear & 0x2) mpz_clear(big2);

  return(bigint_result(res,machp));
} /*and_two_inumbers */


/*-------------------------------------------------------------------------*/
dlong compare_two_inumbers(dlong *p1, dlong *p2, struct machine *machp, dlong *er)
{
  mpz_t big1, big2;
  int toclear = 0;
  int res;

  if ((!is_inumber(p1)) || (! is_inumber(p2)))
    {
      *er = 1;
      return(0);
    }

  *er = 0;
  if (is_smallint(p1)) { toclear += 1; mpz_init_set_si(big1, get_smallint(p1));}
  else hbig_to_gbig(p1,big1,machp);

  if (is_smallint(p2)) { toclear += 2; mpz_init_set_si(big2, get_smallint(p2));}
  else hbig_to_gbig(p2,big2,machp);

  res = mpz_cmp(big1, big2);

  if (toclear & 0x1) mpz_clear(big1);
  if (toclear & 0x2) mpz_clear(big2);

  return(res);
} /*compare_two_inumbers */


/*-------------------------------------------------------------------------*/
dlong or_two_inumbers(dlong *p1, dlong *p2, struct machine *machp)
{
  mpz_t res, big1, big2;
  int toclear = 0;

  mpz_init(res);

  if (is_smallint(p1)) { toclear += 1; mpz_init_set_si(big1, get_smallint(p1));}
  else hbig_to_gbig(p1,big1,machp);

  if (is_smallint(p2)) { toclear += 2; mpz_init_set_si(big2, get_smallint(p2));}
  else hbig_to_gbig(p2,big2,machp);


  mpz_ior(res, big1, big2);

  if (toclear & 0x1) mpz_clear(big1);
  if (toclear & 0x2) mpz_clear(big2);

  return(bigint_result(res,machp));

} /*or_two_inumbers */


/*-------------------------------------------------------------------------*/
dlong xor_two_inumbers(dlong *p1, dlong *p2, struct machine *machp)
{
  mpz_t res, big1, big2;
  int toclear = 0;

  mpz_init(res);

  if (is_smallint(p1)) { toclear += 1; mpz_init_set_si(big1, get_smallint(p1));}
  else hbig_to_gbig(p1,big1,machp);

  if (is_smallint(p2)) { toclear += 2; mpz_init_set_si(big2, get_smallint(p2));}
  else hbig_to_gbig(p2,big2,machp);


  mpz_xor(res, big1, big2);

  if (toclear & 0x1) mpz_clear(big1);
  if (toclear & 0x2) mpz_clear(big2);

  return(bigint_result(res,machp));

} /*xor_two_inumbers */


/*-------------------------------------------------------------------------*/

dlong bigint_divide(dlong *p1, dlong *p2, dlong tocompute, struct machine *machp)
{
  mpz_t res, big1, big2;
  int toclear = 0;

  mpz_init(res);

  if (is_smallint(p1)) { toclear += 1; mpz_init_set_si(big1, get_smallint(p1));}
  else if (is_bigint(p1)) hbig_to_gbig(p1,big1,machp);
  else return(0);

  if (is_smallint(p2)) { toclear += 2; mpz_init_set_si(big2, get_smallint(p2));}
  else if (is_bigint(p2)) hbig_to_gbig(p2,big2,machp);
  else return(0);


  if (DIVIDE == tocompute) mpz_tdiv_q(res, big1, big2);
  else mpz_tdiv_r(res, big1, big2);

  if (toclear & 0x1) mpz_clear(big1);
  if (toclear & 0x2) mpz_clear(big2);

  return(bigint_result(res,machp));
  
} /*bigint_divide */


/*-------------------------------------------------------------------------*/
/*
  bigint_print is there for lowlevel purposes only: it prints byte for
               byte all the ... bytes of the bigint
*/
/*-------------------------------------------------------------------------*/


void bigint_print(FILE *f, dlong *p, struct machine* machp)
{
  dlong len;

  p = get_number_pointer(p);
  p++;
  len = bigint_size(*p);
  if (bigint_positive(*p))
    fprintf(f,"%ld ",len);
  else
    fprintf(f,"%ld ",-len);
  if (bigint_onheap(*p))
    p = p+1;
  else
    p = ((machp->excess_bigints)[*(p+1)]);

  while (len--)
    fprintf(f,"%ld ",*p++);
} /* bigint_print */

/*-------------------------------------------------------------------------*/

void init_intsum(mpz_t *intsum)
{
  mpz_init_set_ui(*intsum,0);
} /* init_intsum */


#else

#include "toinclude.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mach_macros.h"

void bigint_free_string(char *pc, struct machine *machp){return;}

dlong bigint_to_string(dlong *p1, char **pstring, struct machine* machp, dlong base){return(0);}
dlong addsub_two_inumbers(dlong *p1, dlong *p2, dlong addition, struct machine *machp){return(0);}
dlong multiply_two_inumbers(dlong *p1, dlong *p2, struct machine *machp){return(0);}
dlong bigint_low_abs(dlong *p1, struct machine *machp, dlong makepos){return(0);}

dlong and_two_inumbers(dlong *p1, dlong *p2, struct machine *machp){return(0);}
dlong compare_two_inumbers(dlong *p1, dlong *p2, struct machine *machp, dlong *er){return(0);}
dlong or_two_inumbers(dlong *p1, dlong *p2, struct machine *machp){return(0);}
dlong xor_two_inumbers(dlong *p1, dlong *p2, struct machine *machp){return(0);}
dlong bigint_divide(dlong *p1, dlong *p2, dlong tocompute, struct machine *machp){return(0);}
void bigint_print(FILE *f, dlong *p, struct machine* machp){return;}

/*-------------------------------------------------------------------------*/
dlong shiftright_two_inumbers(dlong *p1, dlong *p2, struct machine *machp)
{
  dlong i1, i2;
  deref(p1); deref(p2);
  if ((!is_smallint(p1)) || (! is_smallint(p2)))
    return(0);
  i1 = get_smallint(p1);
  i2 = get_smallint(p2);

  if (i2 > 0) i1 = i1 >> i2;
  else i1 = i1 << (-i2);

  return(make_smallint(i1));
} /*shiftright_two_inumbers */

/*-------------------------------------------------------------------------*/
dlong shiftleft_two_inumbers(dlong *p1, dlong *p2, struct machine *machp)
{
  dlong i1, i2;
  deref(p1); deref(p2);
  if ((!is_smallint(p1)) || (! is_smallint(p2)))
    return(0);
  i1 = get_smallint(p1);
  i2 = get_smallint(p2);

  if (i2 > 0) i1 = i1 << i2;
  else i1 = i1 >> (-i2);

  return(make_smallint(i1));
} /*shiftleft_two_inumbers */

/*-------------------------------------------------------------------------*/
dlong float_to_bigint(dfloat f, struct machine *machp)
{
  dlong i = f;
  i = make_smallint(i);
  return(i);
} /*float_to_bigint */

/*-------------------------------------------------------------------------*/
dlong get_float_from_number(dlong *p1, dfloat *f, struct machine *machp)
{
  if (is_smallint(p1)) { *f = get_smallint(p1); return(1); }
  if (is_real(p1)) { *f = get_real(p1); return(1); }
  return(0);
} /* get_float_from_number */

/*-------------------------------------------------------------------------*/

dlong bigint_low_msb(dlong *p1, struct machine *machp)
{
  /* will return -1 if something is wrong */
  /* p is dereffed allready */

  dlong i, j;

  if (! is_smallint(p1)) return(-1);

  i = get_smallint(p1);
  if (i < 0) return(-1);
  if (i == 0) return(0);

  j = 0;

#ifdef BITS64
  if (i >= 0x100000000) { i >>= 32; j += 32;}
#endif
  if (i >= 0x10000) { i >>= 16; j += 16;}
  if (i >= 0x100) {i >>= 8; j += 8;}
  if (i >= 0x10) {i >>= 4; j += 4;}
  if (i >= 0x4) {i >>= 2; j += 2;}
  if (i >= 0x2) j++;

  return(j);

} /* bigint_low_msb */

/*-------------------------------------------------------------------------*/
dlong bigint_result(dlong *gbig, struct machine *machp)
{
  return((dlong)gbig);
} /* bigint_result */


void init_intsum(dlong **p)
{
  *p = (dlong *)make_smallint(0);
} /* init_intsum */

void add_ints(dlong **p1, dlong *p2, struct machine *machp)
{
  dlong i1, i2;
  i1 = get_smallint(*p1);
  i2 = get_smallint(p2);
  *p1 = (dlong *)(make_smallint(i1+i2));
} /* add_ints */


#endif // USE_GMP
