% Incremental toplevel
% Written by Bart Demoen
% Implementation started: 17-11-1993
% Last change: 22-11-1993
% Adapted for hProlog Tue Sep  3 17:16:53 CEST 2002

% stopped working when chars and strings were introduced :-(


% Loading this file, starts the incremental toplevel.
% You stay in it until you reply to a 'What next' prompt:
% 
% 		e: this brings you to the usual toplevel
% 	or
% 		^D: this brings you to UNIX
% 
% Type h in response to the 'What next' prompt and you will get some help.
% 
% At any time, the incremental toplevel can be restarted, by the goal: itop.

% The idea of incremental toplevel is originally by Van Emden back in 1985 (or so).
% Some people think it is great, I think I never needed it.
% Very few systems have it.
% A lot of people don't see what the fuzz is all about.


% A sample session: [newer versions of hProlog might name the variables differently]
% 
% #1 ?- functor(X,f,3) .
% *1
%   X = f(A_0,A_1,A_2)
% Yes 
% @1 What next ? l
% 
% #2 ?- A_0 = g(A_2) ; A_0 = a .
% *2
%   A_0 = g(A_2)
%   X = f(g(A_2),A_1,A_2)
% Yes 
% @2 What next ? ;
% *2
%   A_0 = a
%   X = f(a,A_1,A_2)
% Yes 
% @2 What next ? ;
% No - leaving level 2
%  
% @1 What next ? l
% 
% #2 ?- A_0 = h(A_2) .
% *2
%   A_0 = h(A_2)
%   X = f(h(A_2),A_1,A_2)
% Yes 
% @2 What next ? 
% 
% @1 What next ? 
% 
% #1 ?-

:- module(incremental_toplevel,[itop/0]) .


itop :- itop(1,[],[]) .
itop :- itop .

itop(N,VarList,QueryList) :-
	atomconcat(['##',N,' ?- '],P) ,
	nl_stderr,
	pvread(P,X,V), (X = end_of_file -> halt ; true) ,
	mergevars(V,VarList,V2) ,
	copy_term((X+V2),(CX+CV2)) ,
	instantvars(CV2) ,
	treat_itop_input(X,V2,N,[N,CX|QueryList]) .
itop(_,_,_) :- eof(stdin), halt .
itop(N,_,_) :- N \== 1  .

instantvars([]) .
instantvars([_name=_var|_rest]) :-
	_name \== '',
	var(_var),
	givevarname(_var,_name) , ! ,
	instantvars(_rest) .
instantvars([_x|_r]) :- instantvars(_r) .

mergevars([],V,V) .
mergevars([Atom = Var|Rest],V,O) :- putin(Atom,Var,V,V2) , mergevars(Rest,V2,O) .

putin(Atom,Var,[],[Atom = Var]) :- ! .
putin(Atom,Var,[Atom = Var|Rest],[Atom = Var|Rest]) :- ! .
putin(Atom,Var,[AV|Rest],[AV|O]) :- putin(Atom,Var,Rest,O) .

treat_itop_input(X,V,Level,QueryList) :-
	( X = (?-Query) ; X = Query ), !,
	treatQuery(Query,V,Level,QueryList) .

treatQuery(Goals,Varlist,Level,QueryList) :-
	call(Goals),
	showsolution(Varlist,Level,NewVarlist),
	whatnext(NewVarlist,Level,Wat,QueryList),
	newline(Z) ,
	(Wat = Z -> ! , fail ; fail) .
treatQuery(_,_,Level,_) :-
	write_stderr_list(['No - leaving level ',Level,nl]),
	fail.

whatnext(Varlist,Level,Wat,QueryList) :-
	write_stderr_list([nl, @ ,Level,' What next ? ']) ,
	getfirst(Firstchar),
	do(Varlist,Level,Firstchar,Wat,QueryList) .

do(V,N,h,W,QL) :- ! , (help_what_next , fail ; whatnext(V,N,W,QL)) .
do(V,N,s,W,QL) :- ! , (showsharsol(V,V,[]) , fail ; whatnext(V,N,W,QL)) .
do(_,_,b,_,_) :- ! , throw('back to toplevel').
do(_,_,e,_,_) :- ! , abort .
do(V,N,v,W,QL) :- ! , (showvar(V) , fail ; whatnext(V,N,W,QL)) .
do(V,N,g,W,QL) :- ! , (showlevelgoal(QL) , fail ; whatnext(V,N,W,QL)) .
do(V,N,l,W,QL) :- ! ,
	(M is N+1 , itop(M,V,QL) , fail
	;
	 whatnext(V,N,W,QL)
	) .
do(_,_,A,A,_) .

showlevelgoal(QL) :-
	write_stderr('Type level number of which you want to see the goal: ') ,
	read_stdin(X) ,
	integer(X) ,
	searchlevelgoal(X,QL,G) ,
	write_stderr(G) .

showvar(L) :-
	write_stderr('Type the variable you want to see: ') ,
	vread(X,Var) ,
	varname(Var,X,V) ,
	instantfreevars(L,_) ,
	showonevar(V,L) .

showonevar(V,[V = X|_]) :- write_var_loc(V = X) , ! .
showonevar(V,[_|R]) :- showonevar(V,R) .

varname([V = _],_,V) :- ! .
varname(_,V,V) :- atom(V) .


searchlevelgoal(X,[X,G|_],G) :- ! .
searchlevelgoal(X,[_,_|R],G) :- searchlevelgoal(X,R,G) .

help_what_next :-
	write_stderr_list(['possible input:',nl]) ,
	write_stderr_list(['        h: this help',nl]) ,
	write_stderr_list(['        s: show variable bindings (again)',nl]) ,
	write_stderr_list(['        e: back to usual toplevel',nl]) ,
	write_stderr_list(['        b: back to incremental toplevel #1',nl]) ,
	write_stderr_list(['        g: show goal of a particular level',nl]) ,
	write_stderr_list(['        v: show binding of a particular variable',nl]) ,
	write_stderr_list(['        l: go one level deeper',nl]) ,
	write_stderr_list(['        <CR>: go to previous level',nl]) ,
	write_stderr_list(['        ^D: leave Prolog',nl]) ,
	write_stderr_list(['        anything else: next solution; if none, back to previous level',nl]) ,
	write_stderr_list(['from usual toplevel to incremental toplevel: ?- itop .',nl] ).


getfirst(X) :-
	get0_stdin(Firstchar), ! , (Firstchar = -1 -> halt ; true) ,
	ascii(X,Firstchar) ,
	newline(Z) ,
	( X = Z , ! ; ascii(Z,Nl), skip_stdin(Nl) ) .
getfirst(_) :-
	eof(stdin), halt .

showsolution(L,Level,NewL) :-
	write_stderr_list([(*),Level,nl]) ,
	varlist(L,Allvars) ,
	determine_name(L,BName) ,
	makenewvarlist(Allvars,L,BName,0,NewL) ,
	(showsharsol(NewL,NewL,[]) , fail ; true) .

determine_name(V,N) :- determine_name(V,'A',O) , makebigger(O,N) .

determine_name([],Name,Name) .
determine_name([N = _|R],I,O) :- N @> I , ! , determine_name(R,N,O) .
determine_name([ _|R],I,O) :- determine_name(R,I,O) .

makebigger(N,O) :-
	atomtolist(N,NL) ,
	(NL = [A|_] , 'A' @=< A , A @=< 'Z' -> mkb(NL,OL) ,atomtolist(O,OL)
	;
	 O = 'A_'
	) .

mkb([],['_']) .
mkb(['Z'|R],['Z'|RO]) :- ! , mkb(R,RO) .
mkb([P|_],['A','_']) :- (P @< 'A' ; P @> 'Z') , ! .
mkb([P|_],[Q,'_']) :- ascii(P,AP) , AP2 is AP+1 , ascii(Q,AP2) .

makenewvarlist([],L,_,_,L) .
makenewvarlist([Var|Rest],L,BName,N,O) :-
	(varinvarlist(Var,L) -> makenewvarlist(Rest,L,BName,N,O)
	;
	 atomconcat(BName,N,Name) , O = [Name = Var|RO] ,
		M is N+1 , makenewvarlist(Rest,L,BName,M,RO)
	) .

varinvarlist(X,[_ = Y|_]) :- X == Y , ! .
varinvarlist(X,[_|R]) :- varinvarlist(X,R) .

instantfreevars([],[]) .
instantfreevars([_name=_var|_rest],_newrest) :-
	_name \== '',
	var(_var), !,
	givevarname(_var,_name),
	instantfreevars(_rest,_newrest) .
instantfreevars([_x|_r],[_x|_o]) :- instantfreevars(_r,_o) .

write_stderr_list([]) .
write_stderr_list([nl|R]) :- ! , newline(Z) , write_stderr(Z) , write_stderr_list(R) .
write_stderr_list([X|R]) :- write_stderr(X) , write_stderr_list(R) .



vread(X,V) :-
	pvread('',X,V).
pvread(P,X,V) :-
	write(P) ,
	sysh:hread(X,VB) ,
	makevarlistfromvardic(VB,V) .

makevarlistfromvardic([],[]).
makevarlistfromvardic([Var = Name|R],[Name = Var|S]) :-
	makevarlistfromvardic(R,S).

atomconcat(A,B,O) :- atomconcat([A,B],O) .

atomconcat(L,O) :- concatlistofatoms(L,O1) , name(O,O1) .

concatlistofatoms([],[]) .
concatlistofatoms([A|R],O) :-
		name(A,AL) ,
		makeopen(AL,O,Tail) ,
		concatlistofatoms(R,Tail) .

makeopen([],T,T) .
makeopen([A|R],[A|S],T) :- makeopen(R,S,T) .

givevarname(N,N) .

eof(_) :- fail . % no idea at the moment

read_stdin(X) :- read(X) .

write_stderr(X) :- write(X) .

get0_stdin(X) :- get0(X) .

skip_stdin(X) :- skip(X) .

nl_stderr :- nl .

ascii(X,Y) :- name(X,[Y]) .

atomtolist(X,Y) :- var(X) , ! , atomconcat(Y,X) .
atomtolist(X,Y) :- name(X,Z) , makechars(Z,Y) .

makechars([],[]) .
makechars([A|R],[B|S]) :- ascii(B,A) , makechars(R,S) .

varlist(A,B) :- varlist(A,[],B) .

varlist(Var,InV,OutV) :- var(Var) , ! , putvarinlist(Var,InV,OutV) .
varlist(Atomic,InV,InV) :- atomic(Atomic) , ! .
varlist([A|B],InV,OutV) :- ! ,
		varlist(A,InV,AV) ,
		varlist(B,AV,OutV) .
varlist(Term,InV,OutV) :-
		Term =.. [_|Args] , varlist(Args,InV,OutV) .

putvarinlist(V,I,I) :- varisin(V,I) , ! .
putvarinlist(V,I,[V|I]) .

varisin(V,[W|_]) :- V == W , ! .
varisin(V,[_|R]) :- varisin(V,R) .

showsharsol(_L,_NewL,[]) :- !,
	instantfreevars(_L,_newL),
	sys_show_solutions(_newL) ,
	write_stderr('Yes ') .

sys_show_solutions( [] ) .
sys_show_solutions( [_var|_vars] ) :-
	write_stderr_list(['  ',_var,nl]) ,
	sys_show_solutions( _vars ) .

newline('
') .	% this is supposed to be the atom of length one with only a carriage return

write_var_loc(X) :- write_stderr('  ') , write(X) .

